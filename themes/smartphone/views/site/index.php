<?php
	$url = new Url();
?>

<div class="container no-bottom">
	<h4 class="icon-heading">Tuyển tập truyện mới</h4>
	<p>Danh sách các truyện mới nhất vừa được cập nhật lên hệ thống Tủ Truyện!</p>
	<?php $i=0; foreach($data_posts_new as $row) {
		$i++;
		if($i == 4) break;
		foreach($data_category as $item){
			if($item['id'] == $row['category_id']){
				$category_alias = $item['alias'];
			}
		}
	?>
		<div class="recent-post">
			<p>
				<img src="<?php echo StringUtils::getImageUrl($row['post_image'], "post"); ?>" width="90" alt="img">
				<strong><a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>"><?php echo $row['post_title']; ?> <?php if($row['post_end'] == 1) { echo '[Hoàn thành]';  }?><?php if($row['post_is_full'] == 1) { echo '[Đang cập nhật]';  }?></a></strong><br/>
				<?php echo $row['post_description']; ?> <br/>
			</p>
		</div>
	<?php } ?>
</div>

<div class="decoration"></div>

<div class="container no-bottom">
	<h4 class="icon-heading">Tuyển tập truyện nhiều người đọc</h4>
	<p>Danh sách các truyện được nhiều người đọc nhất hệ thống Tủ Truyện!</p>
	<?php $i=0; foreach($data_posts_hot as $row) {
		$i++;
		if($i == 4) break;
		foreach($data_category as $item){
			if($item['id'] == $row['category_id']){
				$category_alias = $item['alias'];
			}
		}
		?>
		<div class="recent-post">
			<p>
				<img src="<?php echo StringUtils::getImageUrl($row['post_image'], "post"); ?>" width="90" alt="img">
				<strong><a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>"><?php echo $row['post_title']; ?> <?php if($row['post_end'] == 1) { echo '[Hoàn thành]';  }?><?php if($row['post_is_full'] == 1) { echo '[Đang cập nhật]';  }?></a></strong><br/>
				<?php echo $row['post_description']; ?> <br/>
			</p>
		</div>
	<?php } ?>
</div>

<div class="decoration"></div>

<div class="container no-bottom">
	<h4 class="icon-heading">Tuyển tập truyện nhiều người bình chọn</h4>
	<p>Danh sách các truyện được nhiều bạn đọc đánh giá là hay nhất hệ thống Tủ Truyện!</p>
	<?php $i=0; foreach($data_posts_vote as $row) {
		$i++;
		if($i == 4) break;
		foreach($data_category as $item){
			if($item['id'] == $row['category_id']){
				$category_alias = $item['alias'];
			}
		}
		?>
		<div class="recent-post">
			<p>
				<img src="<?php echo StringUtils::getImageUrl($row['post_image'], "post"); ?>" width="90" alt="img">
				<strong><a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>"><?php echo $row['post_title']; ?> <?php if($row['post_end'] == 1) { echo '[Hoàn thành]';  }?><?php if($row['post_is_full'] == 1) { echo '[Đang cập nhật]';  }?></a></strong><br/>
				<?php echo $row['post_description']; ?> <br/>
			</p>
		</div>
	<?php } ?>
</div>

<div class="decoration"></div>

<script>

	$(document).ready(function () {
		$('#page_title').html('Trang chủ');
	});

</script>

