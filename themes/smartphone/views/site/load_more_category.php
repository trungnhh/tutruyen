<?php
	$url = new Url();
?>

<?php for($i = 0; $i < count($data_category_hot); $i ++) { ?>
	<div id="category_<?php echo $data_category_hot[$i]['id']; ?>" data-lazy_images="true" data-lazy_horizontal_images="true" class="autogen_class_views_shared_carousels_document_carousel autogen_class_views_shared_carousels_carousel has_paddles carousel_widget">
		<div class="module_edge_right"></div>
		<div class="paddle page_left carousel_prev always_visible">
			<div class="sprite paddle_left"></div>
		</div>
		<div class="paddle page_right carousel_next always_visible">
			<div class="sprite paddle_right"></div>
		</div>
		<div class="carousel_title">
			<a href="<?php echo $url->createUrl("category/index", array("alias" => $data_category_hot[$i]['alias'])); ?>" class="title_link"><?php echo $data_category_hot[$i]['name']; ?></a>
		</div>
		<div class="carousel" data-jcarousel="true">
			<ul style="left: 0px; top: 0px;">
				<?php foreach($data_posts[$i] as $row) { ?>
					<li class="item">
						<div class="document_cell object_cell ">
							<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $data_category_hot[$i]['alias'], "post_alias" => $row['post_alias'])); ?>">
								<div
									style="background-image: url(&quot;<?php echo StringUtils::getImageUrl($row['post_image'], "post"); ?>&quot;);"
									class="thumb">
									<?php if($row['post_end'] == 1) { ?>
										<div class="flag purchase_only">HOÀN THÀNH</div>
									<?php } ?>
									<?php if($row['post_is_full'] == 1) { ?>
										<div class="flag sample">ĐANG CẬP NHẬT</div>
									<?php } ?>
									<!--<div class="geo_restricted flag">NOT AVAILABLE</div>-->

									<div class="overlay">
										<div class="sprite white_big_open_book">
										</div>
									</div>
								</div>
							</a>

							<div class="cell_data">
								<div class="document_title">
									<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $data_category_hot[$i]['alias'], "post_alias" => $row['post_alias'])); ?>">
										<?php echo $row['post_title']; ?>
									</a>
								</div>
								<div class="document_author">
									<div class="author_row">
										<a href="javascript:"><?php if($row['author_name'] == '') {
												echo "Sưu tầm";
											} else {
												echo $row['author_name'];
											} ?></a>
									</div>
								</div>
							</div>
						</div>
					</li>
				<?php } ?>
				<li class="carousel_separator item"></li>
			</ul>
		</div>
	</div>

	<script type="text/javascript">
		//&lt;![CDATA[

		new Scribd.UI.Carousel("#category_<?php echo $data_category_hot[$i]['id']; ?>")

		//]]&gt;
	</script>
<?php } ?>