<?php
	$url = new Url();
?>

<div class="container no-bottom">
	<h4 class="icon-heading"><?php echo $data_category['name']; ?></h4>
	<p></p>
	<?php foreach($data_posts as $row) {
		foreach($data_category as $item){
			if($item['id'] == $row['category_id']){
				$category_alias = $item['alias'];
			}
		}
		?>
		<div class="recent-post">
			<p>
				<img src="<?php echo StringUtils::getImageUrl($row['post_image'], "post"); ?>" width="90" alt="img">
				<strong><a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>"><?php echo $row['post_title']; ?> <?php if($row['post_end'] == 1) { echo '[Hoàn thành]';  }?><?php if($row['post_is_full'] == 1) { echo '[Đang cập nhật]';  }?></a></strong><br/>
				<?php echo $row['post_description']; ?> <br/>
			</p>
		</div>
	<?php } ?>
</div>

<div class="footer_row paging_smartphone">
	<div class="paginator">
		<ul class="paging">
			<?php
				echo $paging;
			?>
		</ul>
	</div>
</div>

<div class="decoration"></div>

<script>

	$(document).ready(function () {
		$('#page_title').html('<?php echo $data_category['name']; ?>');
	});

</script>
