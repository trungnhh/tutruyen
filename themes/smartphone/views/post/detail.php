<?php
	$url = new Url();
	$content = nl2br($data_post['post_content']);
?>
<div class="container">
	<div class="blog-post" id="storyText">
		<h3><?php echo $data_post['post_title']; ?></h3>

<!--		<p class="author">Tác giả: <span>--><?php //if($data_post['author_name'] == '') {
//					echo "Sưu tầm";
//				} else {
//					echo $data_post['author_name'];
//				} ?><!--</span></p>-->

		<div id="storyText">
			<?php
				echo $content;
			?>
		</div>

	</div>
</div>
<div class="fb-comments" data-href="<?php echo Helpers::getCurrentUrl(); ?>" data-width="100%" data-numposts="10" data-order-by="social" data-colorscheme="light"></div>
<?php
	if($data_all_chap != null) {
		for($i = 0; $i < count($data_all_chap); $i ++) {
			$a = 0;
			$a = $i - 1;
			$b = $i + 1;
			if($data_all_chap[$i]['post_id'] == $data_post['id']) {
				?>
				<div class="chap-control">
					<?php if($data_all_chap[$b] != null) { ?>
						<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $data_all_chap[$b]['alias'], "post_alias" => $data_all_chap[$b]['post_alias'])); ?>"><?php echo $data_all_chap[$b]['post_title']; ?> ››</a>
					<?php } ?>
				</div>
			<?php
			}
		}
	}
?>
<div class="decoration"></div>

<?php if($data_chap != null) { ?>
	<div class="container">
		<h3>Danh sách chương (<span><?php echo $count; ?></span>)</h3>

		<div class="chap-list">
			<ul>
				<?php foreach($data_chap as $row) { ?>
					<li>
						<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $row['alias'], "post_alias" => $row['post_alias'])); ?>"><?php echo $row['post_title'] ?></a> (<?php echo date('d/m/Y', $row['update_date']) ?>)
					</li>
				<?php } ?>
			</ul>
		</div>
		<div class="pager">
			<ul>
				<?php
					echo $paging;
				?>
			</ul>
		</div>
	</div>
	<div class="decoration"></div>
<?php } ?>

<div class="container">
	<h4 class="icon-heading">Tuyển tập truyện cùng chuyên mục</h4>
	<br>
	<?php $i = 0;
		foreach($data_post_random as $row) {
			$i ++;
			if($i == 4)
				break;
			?>
			<div class="recent-post">
				<p>
					<img src="<?php echo StringUtils::getImageUrl($row['post_image'], "post"); ?>" width="90" alt="img">
					<strong><a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>"><?php echo $row['post_title']; ?> <?php if($row['post_end'] == 1) {
								echo '[Hoàn thành]';
							} ?><?php if($row['post_is_full'] == 1) {
								echo '[Đang cập nhật]';
							} ?></a></strong><br/>
					<?php echo $row['post_description']; ?> <br/>
				</p>
			</div>
		<?php } ?>
</div>
<div class="decoration"></div>

<script>

	$(document).ready(function () {
		$('#page_title').html('<?php echo $data_post['post_title']; ?>');
	});

</script>