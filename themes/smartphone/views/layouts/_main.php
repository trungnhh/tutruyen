<?php
	$url = new Url();
	$controller = Yii::app()->controller->id;
	$action = Yii::app()->controller->action->id;
	$ac = $controller . '-' . $action;
	$fb_app_id = Helpers::getConfigurationValue('facebook_app_id');
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="Keywords" content="<?= isset($this->metaKeywords) ? $this->metaKeywords : Helpers::getconfigurationValue('meta_keyword'); ?>"/>
	<meta name="Description" content="<?= isset($this->metaDescription) ? $this->metaDescription :Helpers::getconfigurationValue('meta_desc'); ?>"/>
	<!--Favicon shortcut link-->
	<link type="image/x-icon" rel="shortcut icon" href="<?php echo Yii::app()->params["urlRs"]; ?>/favicon.ico"/>
	<link type="image/x-icon" rel="icon" href="<?php echo Yii::app()->params["urlRs"]; ?>/favicon.ico"/>
	<!--Declare page as mobile friendly -->
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
	<!-- Declare page as iDevice WebApp friendly -->
	<meta name="apple-mobile-web-app-capable" content="yes"/>

	<meta property="og:title" content="<?=isset($this->title) ? $this->title :  Helpers::getConfigurationValue('page_title') ?>">
	<meta property="og:description" content="<?=isset($this->metaDescription) ? $this->metaDescription : Helpers::getconfigurationValue('meta_desc') ?>">
	<meta property="og:image" content="<?php echo Helpers::getBaseUrl(); ?>images/tutruyen_cover.jpg">
	<meta property="og:url" content="<?php echo Helpers::getCurrentUrl(); ?>">
	<meta property="og:image:type" content="image/png">
	<meta property="og:image:width" content="200">
	<meta property="og:image:height" content="200">
	<?php if($fb_app_id && $fb_app_id != ''): ?>
		<meta property="fb:app_id" content="<?php echo $fb_app_id; ?>"/>
	<?php endif; ?>

	<!-- iDevice WebApp Splash Screen, Regular Icon, iPhone, iPad, iPod Retina Icons -->
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo Yii::app()->params["urlRs"]; ?>/images/smartphone/splash/splash-icon.png">
	<link rel="apple-touch-startup-image" href="<?php echo Yii::app()->params["urlRs"]; ?>/images/smartphone/splash/splash-screen.png" media="screen and (max-device-width: 320px)"/>
	<link rel="apple-touch-startup-image" href="<?php echo Yii::app()->params["urlRs"]; ?>/images/smartphone/splash/splash-screen_402x.png" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)"/>
	<link rel="apple-touch-startup-image" href="<?php echo Yii::app()->params["urlRs"]; ?>/images/smartphone/splash/splash-screen_403x.png" sizes="640x1096">

	<!-- Page Title -->
	<title><?= isset($this->title) ? $this->title : "Đọc Truyện Online - Truyện Ngắn - Truyện Cười - Truyện Tình Yêu" ?></title>

	<!-- Stylesheet Load -->
	<link href="<?php echo Yii::app()->params["urlRs"]; ?>/css/smartphone/style.css" rel="stylesheet" type="text/css">
	<link href="<?php echo Yii::app()->params["urlRs"]; ?>/css/smartphone/framework-style.css" rel="stylesheet" type="text/css">
	<link href="<?php echo Yii::app()->params["urlRs"]; ?>/css/smartphone/framework.css" rel="stylesheet" type="text/css">
	<link href="<?php echo Yii::app()->params["urlRs"]; ?>/css/smartphone/icons.css" rel="stylesheet" type="text/css">
	<!--<link href="<?php /*echo Yii::app()->params["urlRs"]; */?>/css/smartphone/retina.css" rel="stylesheet" type="text/css" media="only screen and (-webkit-min-device-pixel-ratio: 2)"/>-->
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRs"] ?>/css/smartphone/library.css" media="all" />

	<!--Page Scripts Load -->

	<script src="<?php echo Yii::app()->params["urlRs"]; ?>/js/smartphone/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->params["urlRs"]; ?>/js/smartphone/jquery-ui-min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->params["urlRs"]; ?>/js/smartphone/contact.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->params["urlRs"]; ?>/js/smartphone/swipe.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->params["urlRs"]; ?>/js/smartphone/klass.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->params["urlRs"]; ?>/js/smartphone/photoswipe.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->params["urlRs"]; ?>/js/smartphone/colorbox.js" type="text/javascript"></script>
	<!--<script src="<?php /*echo Yii::app()->params["urlRs"]; */?>/js/smartphone/twitter.js" type="text/javascript"></script>-->
	<!--<script src="<?php /*echo Yii::app()->params["urlRs"]; */?>/js/smartphone/retina.js" type="text/javascript"></script>-->
	<script src="<?php echo Yii::app()->params["urlRs"]; ?>/js/smartphone/custom.js" type="text/javascript"></script>
</head>

<body>
<?php if($fb_app_id && $fb_app_id != ''): ?>
	<!-- Facebook JavaScript SDK -->
	<div id="fb-root"></div>
	<script type="text/javascript">
		window.fbAsyncInit = function() {
			FB.init({appId: '<?php echo $fb_app_id; ?>', status: true, cookie: true,
				xfbml: true});
		};
		(function() {
			var e = document.createElement('script'); e.async = true;
			e.src = document.location.protocol +
				'//connect.facebook.net/en_US/all.js';
			document.getElementById('fb-root').appendChild(e);
		}());
	</script>
	<!-- End Facebook JavaScript SDK -->
<?php else: ?>
	<div id="fb-root"></div>
	<script type="text/javascript">
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
<?php endif; ?>
<div id="preloader">
	<div id="status">
		<p class="center-text">
			Đang tải nội dung...
			<em>Tốc độ tải trang phụ thuộc vào kết nối của bạn!</em>
		</p>
	</div>
</div>


<div class="page-content">
	<?php $this->widget("SmartHeader"); ?>

	<div class="content">
		<div class="decoration"></div>
		<?php echo $content; ?>
		<a href="javascript:void(0);" id="scroll" title="Scroll to Top" style="display: none;">Top<span></span></a>
		<?php $this->widget("SmartFooter"); ?>
	</div>
</div>

<?php $this->widget("Sidebar"); ?>

<script type="text/javascript">
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', '<?php echo Helpers::getConfigurationValue('ga'); ?>', '<?php echo Helpers::getBrandDomain();?>');
	ga('send', 'pageview');

</script>

</body>
</html>




