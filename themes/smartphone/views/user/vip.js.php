<script type="">

    function show_vip(){
        var choose_vip = $("input[name=choise_add]:checked").val();
        if(choose_vip == 1){
            $('#list-vip').slideUp('slow');
            $('#card-vip').slideDown('slow');
        }        
        if(choose_vip == 2){
            $('#list-vip').slideUp('slow');
            $('#sms-vip').slideDown('slow');
        }
    }
    
    function process_card(){        
        loadingAjax();
        var card_code = $('#card_code').val();
        var card_serial = $('#card_serial').val();
        var card_type = $("input[name=card_type]:checked").val();
        if(card_code == ''){
            show_error('Bạn chưa nhập mã thẻ!');
            return false;
        }
        if(card_serial == ''){
            show_error('Bạn chưa nhập số Seri!');
            return false;
        }
        $('#log_error').hide();
        var strUrl = '<?=$url->createUrl("user/ajaxProcessCard") ?>';        
        $.ajax({
            type: "POST",
            url: strUrl,
            data: {card_code:card_code,card_serial:card_serial,card_type:card_type},
            success: function(msg){
                closeLoadingAjax();
                show_error(msg);                                
            }
        });
    }
    
    function reset_vip(){
        $('#card-vip').slideUp('slow');
        $('#sms-vip').slideUp('slow');
        $('#list-vip').slideDown('slow');
    }
    
    function show_error(text){
        $('#log_error').show();
        $('#log_error').html(text);
    }

</script>
