<?php
	$url = new Url();
?>

<link href="<?php echo Yii::app()->params['urlRs']; ?>/css/content_list.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->params['urlRs']; ?>/css/recently_read.css" rel="stylesheet" type="text/css" />

<div class="autogen_class_views_recently_read_index autogen_class_views_shared_content_list_index autogen_class_views_shared_contained_page">
	<div class="home_container">
		<div class="autogen_class_views_recently_read_header content_list_header recently_read_header">
			<div class="relative_container">
				<h2 class="page_title">Truyện yêu thích <span class="count"><span>(</span><span class="value" id="item_count"><?php echo $count; ?></span><span>)</span></span></h2>
			</div>
		</div>
		<div class="items_container_placeholder">
			<div class="items_container">
				<div class="items">
					<?php foreach($data_posts as $row){ ?>

						<div class="item document" id="item_id_<?php echo $row['id']; ?>">
							<div class="preview">
								<a href="<?php echo $url->createUrl("post/detail",array("category_alias"=>$row['alias'],"post_alias"=>$row['post_alias'])); ?>">
									<img width="123" height="164" src="<?php echo StringUtils::getImageUrl($row['post_image'],"post"); ?>">
								</a>
							</div>
							<div class="details">
								<div class="item_header">
									<h3 class="title">
										<a href="<?php echo $url->createUrl("post/detail",array("category_alias"=>$row['alias'],"post_alias"=>$row['post_alias'])); ?>"><?php echo $row['post_title']; ?></a>
									</h3>

									<div class="author"></div>
								</div>
								<div class="main_column">
									<div class="ratings_row"></div>
									<div class="read_this_description">
										<?php
											$content = $row['post_content'];
											$content = StringUtils::cutstring($content,400,true);
											$content = nl2br($content);
											echo $content;
										?>
									</div>
									<!--<div class="uploader">Published by<span class="verified_badge"></span><a href="/WorkmanEBooks">Workman eBooks</a></div>-->
									<div class="author">Tác giả:
										<?php if($row['author_name'] != ''){ ?>
											<a class="userlink" href="javascript:"><?php echo $row['author_name']; ?></a>
										<?php }else{ ?>
											<a class="userlink" href="javascript:">Admin</a>
										<?php } ?>
									</div>
								</div>
								<div class="metadata">
									<ul class="metrics">
										<li><span class="label">Danh mục</span>:
											<a href="<?php echo $url->createUrl("category/index",array("alias"=>$row['alias'])); ?>" class="value"><?php echo $row['category_name']; ?></a>
										</li>
										<li><span class="label">Lượt đọc</span>: <span class="value"><?php echo $row['post_views']; ?></span></li>
										<li>Public</li>
									</ul>
								</div>
								<div class="action_date">Cập nhật ngày <?php if($row['update_date'] != '' && $row['update_date'] != 0){echo date('d/m/Y',$row['update_date']);}else{echo date('d/m/Y',$row['create_date']);} ?></div>
								<div class="hover_buttons">
									<div class="item_action delete_recently_read">
										<a href="javascript:" class="button" onclick="delete_item(<?php echo $row['id']; ?>)"><span class="sprite">Xoá</span></a>
									</div>
								</div>
							</div>
						</div>

					<?php } ?>
				</div>
				<?php if($count == 0){ ?>
					<div class="no_items active">
						<p>Bạn chưa đọc truyện nào!</p>
						<p>Khi bạn băt đầu đọc truyên, trang này sẽ lưu lại những truyện bạn đọc gần đây nhất.</p>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="footer_row">
			<div class="paginator">
				<ul class="paging">
					<?php
						echo $paging;
					?>
				</ul>
			</div>
		</div>
	</div>
</div>

<script>

	function delete_item(bookmark_id){
		var strUrl = '<?=$url->createUrl("user/ajaxDeleteBookmark") ?>';
		loadingAjax();
		$.ajax({
			type: "POST",
			url: strUrl,
			data: {bookmark_id:bookmark_id},
			success: function(msg){
				if(msg==0){
					//errorMessage('login_msg_err', 'Tài khoản hoặc mật khẩu sai!', '#username_field');
				}else{
					var item_count = parseInt($('#item_count').html());
					$('#item_id_'+vote_id).hide();
					$('#item_count').html(item_count-1)
				}
			}
		});
		closeLoadingAjax();
	}

</script>
