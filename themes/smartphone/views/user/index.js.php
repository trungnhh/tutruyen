<script type="">

    //-----------------------------------------------------------------------------------------------------------------//   

    var filename = "";
    var filepath = "";
    function getdata(data_json){              
        var json = $.parseJSON(data_json);
        var code = json.code;   

        if(code == '105')
            {
            filename = json.data["filename"];

            //filetype = json.data["filetype"];
            //filesize = json.data["filesize"];
            filepath = "/" + json.data["forder_up"]  + "/" + ""+filename;
            //path = "<?=Yii::app()->params['urlImage']?>/"+ json.data["forder_up"]  + "/" + ""+filename;
            path = "<?php echo Helpers::getImageBaseUrl();?>data/upload_data/"+ json.data["forder_up"]  + "/" + ""+filename;
            $("#txtFileName").val(filename);
            $("#thumbnails").html("<img style='max-height: 200px;max-width: 200px;' src='"+path+"'/>");                          
        }
        else
            {
            var msg = json.msg;                
            alert(msg);    
        }
    }

    //-----------------------------------------------------------------------------------------------------------------//

    var swfu;
    window.onload = function () {
        /*swfu = new SWFUpload({
            // Backend Settings
            upload_url: "<?=$link_url ?>",
            post_params: "resume_file",

            // File Upload Settings
            file_size_limit : "4 MB",
            file_types : "*.jpg;*.jpeg;*.png;",
            file_types_description : "Ảnh",
            file_upload_limit : "0",

            file_queue_error_handler : fileQueueError,
            file_dialog_complete_handler : fileDialogComplete,
            upload_progress_handler : uploadProgress,
            upload_error_handler : uploadError,
            upload_success_handler : uploadSuccess,
            upload_complete_handler : uploadComplete,

            // Button Settings
            //button_image_url : "<?php echo Yii::app()->params['urlRs']; ?>/js/swfupload/XPButtonUploadText_61x22.png",
            button_image_url : "http://images.tutruyen.vn/js/swfupload/XPButtonUploadText_61x22.png",
            button_placeholder_id : "spanButtonPlaceholder",
            button_width: 61,
            button_height: 22,

            // Flash Settings
            //flash_url : "<?php echo Yii::app()->params['urlRs']; ?>/js/swfupload/swfupload.swf",
            flash_url : "http://images.tutruyen.vn/js/swfupload/swfupload.swf",

            custom_settings : {
                upload_target : "divFileProgressContainer"
            },

            // Debug Settings
            //debug: true
        });*/
    };

    //-----------------------------------------------------------------------------------------------------------------//

    function update(){
        var password = $('#password').val();  
        var fullname = $('#fullname').val();  
        var email = $('#email').val();  
        var mobile = $('#mobile').val();  
        var yahoo = $('#yahoo').val();  
        var address = $('#address').val();
        //var image = $('#txtFileName').val();
        if(fullname == ''){            
            show_error('Bạn chưa nhập họ tên!');
            return false;
        }
        if(password == ''){            
            show_error('Bạn chưa nhập mật khẩu!');
            return false;
        }
        if(mobile == ''){            
            show_error('Bạn chưa nhập số điện thoại!');
            return false;
        }
        $('.error_message').hide('slow');
        var strUrl = '<?=$url->createUrl("user/ajaxSubmitUpdate") ?>';        
        $.ajax({
            type: "POST",
            url: strUrl,
            data: {password:password,mobile:mobile,yahoo:yahoo,fullname:fullname,email:email,address:address},
            success: function(msg){                
                if(msg==-3){                    
                    show_error('Tên tài khoản đã được sử dụng! Hãy sử dụng tên tài khoản khác!');
                }else{
	                $('.saved_message').slideDown('slow');
	                $('.saved_message').html('Thông tin tài khoản của bạn đã được cập nhật thành công!');
	                $('html, body').animate({ scrollTop: 0 }, 'slow');
                }                
            }
        });        
    }

    function show_error(text){
        $('.error_message').slideDown('slow');
        $('.error_message').html(text);
	    $('html, body').animate({ scrollTop: 0 }, 'slow');
    }
    
</script>