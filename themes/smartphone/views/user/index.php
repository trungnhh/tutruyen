<?php 
    $url = new Url();      
    //$link_url = Yii::app()->params['urlRs']."/upload/upload.php?forder_up=user";
    $link_url = Helpers::getImageBaseUrl()."upload/upload.php?forder_up=user";
    $url_img_thichtruyen = Helpers::getImageBaseUrl();
?> 

<link href="<?php echo Yii::app()->params['urlRs']; ?>/css/account_settings.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->params['urlRs']; ?>/js/swfupload/css/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo Yii::app()->params['urlRs']; ?>/js/swfupload/swfupload.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->params['urlRs']; ?>/js/swfupload/js/handlers.js"></script>

	<div class="autogen_class_views_account_settings_show account_settings_page">
		<div class="home_container">
			<div class="page_header">
				<h2 class="title">Tài khoản</h2>
				<?php $this->widget("UserMenu");?>
			</div>
			<div id="autogen_id_482978354" class="autogen_class_views_account_settings_tabs_personal autogen_class_views_account_settings_tabs_base">
				<div class="page_body">
						<div class="error_message" style="display: none"></div>
						<div class="saved_message" style="display: none"></div>
						<div>
							<div class="page_content">
								<section><h3 class="title">Thông tin đăng nhập</h3>

									<div class="column">
										<div class="row field">
											<label for="word_user_login">Tên đăng nhập</label><input type="text" value="<?php echo $data_user['username']; ?>" size="30" name="username" id="username" disabled>
										</div>
										<div class="row field email_addresses">
											<label for="primary_email">Mật khẩu</label><input type="password" value="shinhip215" name="password" id="password">
										</div>
									</div>
									<div class="column">
										<!--<div class="row field change_password">
											<label>Mật khẩu</label><a href="#">Thay đổi mật khẩu của bạn</a>
										</div>-->
										<div class="row facebook field"><label>Facebook</label>
											<button id="autogen_id_482981972" class="facebook_login_button home_btn facebook">Kết nối với tài khoản Facebook</button>
										</div>
									</div>
								</section>
								<section><h3 id="profile_info" class="title">Thông tin cá nhân</h3>

									<div class="column">
										<div class="field row">
											<label for="word_user_name">Họ tên</label><input type="text" size="30" value="<?php echo $data_user['fullname']; ?>" name="fullname" id="fullname">
										</div>
										<div class="location field">
											<label for="word_user_place">Điện thoại</label><input type="text" size="30" value="<?php echo $data_user['mobile']; ?>" name="mobile" id="mobile">
										</div>
										<div class="row field email_addresses">
											<label for="primary_email">Email</label><input type="text" value="<?php echo $data_user['email']; ?>" name="email" id="email">
										</div>
										<div class="website field">
											<label for="word_user_website">Địa chỉ</label><input type="text" size="30" value="<?php echo $data_user['address']; ?>" name="address" id="address">
										</div>
										<div class="website field">
											<label for="word_user_website">Nick yahoo</label><input type="text" size="30" value="<?php echo $data_user['yahoo']; ?>" name="yahoo" id="yahoo">
										</div>
									</div>
								</section>
							</div>
							<div class="bottom_menu">
								<button type="button" class="save_button home_btn darkblue" onclick="update()">Lưu</button>
							</div>
						</div>

				</div>
			</div>
		</div>
	</div>

<?php include "index.js.php"; ?>