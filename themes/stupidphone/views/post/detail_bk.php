<?php
	$url = new Url();
	$content = nl2br($data_post['post_content']);
?>

	<!-- BEGIN CONTENT -->
	<div class="page detail-page">
		<div class="block">
			<div class="info">
				<h2><?php echo $data_post['post_title']; ?></h2>
				<p class="author">Tác giả: <span><?php if($data_post['author_name'] == '') { echo "Sưu tầm"; }else{ echo $data_post['author_name']; } ?></span></p>
				<ul>
					<li class="first">Lượt đọc: <span><?php echo $data_post['post_views']; ?></span></li>
					<li class="last">Bình chọn: <span><?php echo $data_post['post_vote']; ?></span></li>
				</ul>
			</div>
			<div class="page-content">
				<div class="std">
					<?php
						echo $content;
					?>
				</div>
			</div>
			<div class="chap-area">

				<?php
					if($data_all_chap != null){
						for($i=0;$i<count($data_all_chap);$i++){
							$a = 0;
							$a = $i - 1;
							$b = $i + 1;
							if($data_all_chap[$i]['post_id'] ==  $data_post['id']){
								?>
								<div class="chap-control">
									<?php if($data_all_chap[$b] != null){ ?>
										<a href="<?php echo $url->createUrl("post/detail",array("category_alias"=>$data_all_chap[$b]['alias'],"post_alias"=>$data_all_chap[$b]['post_alias'])); ?>"><?php echo $data_all_chap[$b]['post_title']; ?> ››</a>
									<?php } ?>
									<p><a href="<?php echo $url->createUrl("post/detail",array("category_alias"=>$data_all_chap[$b]['alias'],"post_alias"=>$data_all_chap[$b]['post_alias'])); ?>">Danh sách chương ››</a></p>
								</div>
							<?php
							}
						}
					}
				?>

				<?php if($data_chap != null){  ?>
				<div class="chap-list" id="document_activity">
					<p>Danh sách chương (<span><?php echo $count; ?></span>)</p>
					<ul>
						<?php foreach($data_chap as $row){  ?>
						<li><a href="<?php echo $url->createUrl("post/detail",array("category_alias"=>$row['alias'],"post_alias"=>$row['post_alias'])); ?>"><?php echo $row['post_title'] ?></a> <span>| Cập nhật ngày <?php echo date('d/m/Y',$row['update_date']) ?></span></li>
						<?php } ?>
					</ul>
				</div>
				<div class="pager">
					<ul>
						<?php
							echo $paging;
						?>
					</ul>
				</div>
				<?php } ?>
			</div>
		</div>

		<div class="block truyen-moi">
			<div class="title">
				<h3><a href="javascript:">Truyện cùng danh mục</a></h3>
			</div>
			<div class="list">
				<ul>
					<?php $i=0; foreach($data_post_random as $row) {
						$i++;
						if($i == 6) break;
						?>
						<li><a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>"><?php echo $row['post_title']; ?> <?php if($row['post_end'] == 1) { echo '[Hoàn thành]';  }?><?php if($row['post_is_full'] == 1) { echo '[Đang cập nhật]';  }?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>

	</div>


	<!-- END CONTENT -->

<?php include "detail.js.php"; ?>