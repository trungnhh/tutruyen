<?php $url = new Url();$content = nl2br($data_post['post_content']);$post_title_no_sign = StringUtils::RemoveSign($data_post['post_title']); ?><!-- BEGIN CONTENT -->
<div class="page detail-page">
		<?php $this->widget("StupidHot"); ?>
		<?php $this->widget("StupidNotification"); ?>
		<div class="block">
			<div class="info">
				<h1><b><?php echo $data_post['post_title']; ?></b></h1>
<!--				<p class="author">Tác giả: <span>-->
<!--						--><?php //if($data_post['author_name'] == '') { echo "Sưu tầm"; }else{ ?>
<!--							<a href="--><?php //echo $url->createUrl("author/detail", array("author_alias" => $data_post['author_alias'])); ?><!--">--><?php //echo $data_post['author_name']; ?><!--</a>-->
<!--						--><?php //} ?>
<!--					</span></p>-->
				<ul>
					<li class="first">Lượt đọc: <span><?php echo $data_post['post_views']; ?></span></li>
					<li class="last">Bình chọn: <span><?php echo $data_post['post_vote']; ?></span></li>
				</ul>
				<!--<a class="save-post" href="#"><span>Đánh dấu chương này</span></a>-->
			</div>
				<?php
					if($data_all_chap != null){
						for($i=0;$i<count($data_all_chap);$i++){
							$a = 0;
							$a = $i - 1;
							$b = $i + 1;
							if($data_all_chap[$i]['post_id'] ==  $data_post['id']){
								?>
								<div class="chap-control top">
									<?php if($data_all_chap[$b] != null){ ?>
										<a href="<?php echo $url->createUrl("post/detail",array("category_alias"=>$data_all_chap[$b]['alias'],"post_alias"=>$data_all_chap[$b]['post_alias'])); ?>"><?php echo $data_all_chap[$b]['post_title']; ?> ››</a>
									<?php } ?>
								</div>
							<?php
							}
						}
					}
				?>
			<div class="page-content">
				<div class="std">
					<?php
						echo $content;
					?>
					<br>
					
				</div>
			</div>
			
			<div class="chap-area">

				<?php
					if($data_all_chap != null){
						for($i=0;$i<count($data_all_chap);$i++){
							$a = 0;
							$a = $i - 1;
							$b = $i + 1;
							if($data_all_chap[$i]['post_id'] ==  $data_post['id']){
								?>
								<div class="chap-control">
									<?php if($data_all_chap[$b] != null){ ?>
										<a href="<?php echo $url->createUrl("post/detail",array("category_alias"=>$data_all_chap[$b]['alias'],"post_alias"=>$data_all_chap[$b]['post_alias'])); ?>"><?php echo $data_all_chap[$b]['post_title']; ?> ››</a>
									<?php } ?>
								</div>
							<?php
							}
						}
					}
				?>

				<?php if($data_chap != null){  ?>
				<div class="chap-list">
					<p>Danh sách chương (<span><?php echo $count; ?></span>)</p>
					<ul>
						<?php foreach($data_chap as $row){  ?>
						<li><a href="<?php echo $url->createUrl("post/detail",array("category_alias"=>$row['alias'],"post_alias"=>$row['post_alias'])); ?>"><?php echo $row['post_title'] ?></a> <span>| Cập nhật ngày <?php echo date('d/m/Y',$row['update_date']) ?></span></li>
						<?php } ?>
					</ul>
				</div>
				
				<div class="pager">
					<ul>
						<?php
							echo $paging;
						?>
					</ul>
				</div>
				<?php } ?>
				<h2 class="tag">Tags : <a href="<?php echo $url->createUrl("category/index",array("alias"=>$category_alias)); ?>"><?php echo str_replace('-',' ',$category_alias); ?></a><?php if($data_post_parent != null){ ?> , <a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $data_post_parent['post_alias'])); ?>"><?php echo StringUtils::RemoveSign($data_post_parent['post_title']); ?></a>, <a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $data_post_parent['post_alias'])); ?>"><?php echo $data_post_parent['post_title']; ?></a> <?php } ?></h2>
				
			<!--<div class="comments">
				<h6>Hãy gửi bình luận cho chúng tôi để được phục vụ tốt nhất!</h6>
				<table class="write-comment">
					<tr>
						<td>Tên bạn</td>
						<td><input type="text"/></td>
					</tr>
					<tr>
						<td>Email</td>
						<td><input type="text"/></td>
					</tr>
					<tr>
						<td>Nhận xét</td>
						<td><textarea type="text"></textarea></td>
					</tr>
					<tr>
						<td></td>
						<td><button type="summit">Gửi</button></td>
					</tr>
				</table>
				<div class="cmt-list">
					<ul>
						<li>
							<p class="cmt-author">Kenvin - <span>10/1/2014</span></p>
							<p class="cmt-email">kenvin0208@gmail.com</p>
							<p class="cmt-post">Một câu truyện tuyệt vời.</p>
						</li>
					</ul>
				</div>
			</div>-->
			</div>
			
		</div>

		<div class="block truyen-moi">
			<div class="title">
				<h3><a href="javascript:">Truyện cùng danh mục</a></h3>
			</div>
			<div class="list">
				<ul>
					<?php $i=0; foreach($data_post_random as $row) {
						$i++;
						if($i == 6) break;
						?>
						<li><a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>"><?php echo $row['post_title']; ?> <?php if($row['post_end'] == 1) { echo '[Hoàn thành]';  }?><?php if($row['post_is_full'] == 1) { echo '[Đang cập nhật]';  }?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>

	</div>
	<!-- END CONTENT -->

<?php include "detail.js.php"; ?>