<?php
	$url = new Url();
?>

	<!-- BEGIN CONTENT -->
	<div class="page account-page login-page">

		<?php $this->widget("StupidUserMenu"); ?>

		<div class="block">
			<div class="page-title" id="user-messages">
				<h2>Tin nhắn</h2>
			</div>
			<div class="page-content">
				<?php foreach($data_messages as $row) { ?>
					<div class="item"><a href="<?php echo $url->createUrl("messages/detail", array("id" => $row['id'])); ?>"><?php echo $row['title']; ?> <?php if($row['status'] == 1) { echo '[Đã đọc]';  }else{ echo "[Chưa đọc]";}?></a>
						<p class="author"><?php echo $row['sender']; ?></p>
					</div>
				<?php } ?>
			</div>
			<div class="clearfix"></div>
			<div class="pager">
				<ul>
					<?php
						echo $paging;
					?>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- END CONTENT -->

<?php //include "index.js.php"; ?>