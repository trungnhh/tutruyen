<?php
	$url = new Url();
?>

<!-- BEGIN CONTENT -->
<div class="page detail-page">
	<div class="block">
		<div class="info">
			<h2><?php echo $data_message['title']; ?></h2>
			<p class="author">Người gửi: <span><?php echo $data_message['sender']; ?></span></p>
		</div>
		<div class="page-content">
			<div class="std">
				<?php
					echo $data_message['content'];
				?>
			</div>
		</div>

	</div>

</div>
<!-- END CONTENT -->