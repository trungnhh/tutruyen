<?php $url = new Url(); ?>
	<!-- BEGIN CONTENT -->
<div class="page detail-page">
		<?php $this->widget("StupidNotification"); ?>
		<div class="block">
			<div class="info">
				<h1><b><?php echo $data_new['news_title']; ?></b></h1>
				<p class="author">Tác giả: <span>Tủ Truyện</span></p>
			</div>
			<div class="page-content">
				<div class="std">
					<?php echo $data_new['news_content']; ?>
					<br>
					
				</div>
			</div>
		</div>

		<div class="block truyen-moi">
			<div class="title">
				<h3><a href="javascript:">Tin tức khác</a></h3>
			</div>
			<div class="list">
				<ul>
					<?php foreach($data_news_other as $row) { ?>
						<li><a href="<?php echo $url->createUrl("news/detail",array("news_id"=>$row['id'])); ?>"><?php echo $row['news_title']; ?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>

	</div>
	<!-- END CONTENT -->

<?php include "detail.js.php"; ?>