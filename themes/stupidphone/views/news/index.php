<?php
	$url = new Url();
?>

<!-- BEGIN CONTENT -->
<div class="page listing-page">
	<div class="block">
		<div class="page-title">
			<h1><b>Tin tức</b></h1>
		</div>
		<div class="page-content">
				<?php foreach($data_news as $row) { ?>
				<div class="item"><a href="<?php echo $url->createUrl("news/detail",array("news_id"=>$row['id'])); ?>"><?php echo $row['news_title']; ?></a>
					<p class="author">Tủ Truyện</p>
				</div>
				<?php } ?>
		</div>
		<div class="clearfix"></div>
		<div class="pager">
			<ul>
				<?php
					echo $paging;
				?>
			</ul>
		</div>
		<div class="clearfix"></div>
	</div>


</div>
<!-- END CONTENT -->