<?php
	$url = new Url();
	$controller = Yii::app()->controller->id;
	$action = Yii::app()->controller->action->id;
	$ac = $controller . '-' . $action;
?>

<!DOCTYPE html>
<html>
<head>
	<title><?=isset($this->title) ? $this->title : Helpers::getConfigurationValue('page_title'); ?></title>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8">
	<meta name="author" content="<?php echo Helpers::getBrandDomain();?>">
	<meta name="copyright" content="<?php echo Helpers::getBrandDomain();?>">
	<meta http-equiv="Reply-to" content="<?php echo Helpers::getConfigurationValue('info_email') ?>">
	<meta name="generator" content="<?php echo Helpers::getBrandDomain();?>">
	<meta name="creation-date" content="01/12/2012">
	<meta name="revisit-after" content="1 days">
	<meta name="description" content="<?=isset($this->metaDescription) ? $this->metaDescription : Helpers::getConfigurationValue('meta_desc') ?>"/>
	<meta name="keywords" content="<?=isset($this->metaKeywords) ? $this->metaKeywords : Helpers::getConfigurationValue('meta_keyword') ?>"/>
	<meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0" name="viewport">
	<link type="image/x-icon"  rel="shortcut icon" href="<?php echo Yii::app()->params["urlRs"]; ?>/favicon.ico" />
	<link type="image/x-icon"  rel="icon"		   href="<?php echo Yii::app()->params["urlRs"]; ?>/favicon.ico" />

	<meta property="og:title" content="<?= isset($this->title) ? $this->title : Helpers::getConfigurationValue('page_title') ?>">
	<meta property="og:description" content="<?= isset($this->metaDescription) ? $this->metaDescription : Helpers::getConfigurationValue('meta_desc') ?>">
	<meta property="og:image" content="<?php echo Helpers::getBaseUrl();?>images/ava-fb.jpg">
	<meta property="og:url" content="<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
	<meta property="og:image:type" content="image/png">
	<meta property="og:image:width" content="200">
	<meta property="og:image:height" content="200">

	<link href="<?= Yii::app()->params["urlRs"] ?>/css/stupidphone/styles.css" rel="stylesheet" media="all" type="text/css"/>
</head>
<body>
<div class="content-wrapper">
	<?php $this->widget("StupidHeader"); ?>
	<?php echo $content; ?><?php $this->widget("StupidFooter"); ?>
</div>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', '<?php echo Helpers::getConfigurationValue('ga'); ?>', '<?php echo Helpers::getBrandDomain();?>');
	ga('send', 'pageview');

</script>

</body>
</html>