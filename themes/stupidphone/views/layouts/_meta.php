<?php 
    $url = new Url();
    $controller = Yii::app()->controller->id;
    $action = Yii::app()->controller->action->id;
    $ac = $controller.'-'.$action;
	if($ac != "user-login"){
		$_SESSION['t186_link'] = StringUtils::getCurrentPageURL();
	}
?>
<title><?=isset($this->title) ? $this->title:"Đọc Truyện Online - Truyện Ngắn - Truyện Cười - Truyện Tình Yêu" ?></title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="canonical" href="/">

<meta http-equiv="Expires" content="Fri, Jan 01 2015 00:00:00 GMT">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta name="author" content="<?php echo Helpers::getBrandDomain();?>">
<meta name="copyright" content="<?php echo Helpers::getBrandDomain();?>">
<meta http-equiv="Reply-to" content="<?php echo Helpers::getConfigurationValue('info_email') ?>">
<meta name="generator" content="<?php echo Helpers::getBrandDomain();?>">
<meta name="description" content="<?=isset($this->metaDescription) ? $this->metaDescription : Helpers::getConfigurationValue('meta_desc') ?>"/>
<meta name="keywords" content="<?=isset($this->metaKeywords) ? $this->metaKeywords : Helpers::getConfigurationValue('meta_keyword') ?>"/>
<meta name="creation-date" content="12/09/2012">
<meta name="revisit-after" content="15 days">
<link href="<?php echo Yii::app()->params["urlRs"]; ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />

<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRs"] ?>/css/browse.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRs"] ?>/css/style.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRs"] ?>/css/library.css" media="all" />
<script src="<?=Yii::app()->params["urlRs"] ?>/js/chrome.js" type="text/javascript"></script>
<script src="<?=Yii::app()->params["urlRs"] ?>/js/browse.js" type="text/javascript"></script>
<script src="<?=Yii::app()->params["urlRs"] ?>/js/library.js" type="text/javascript"></script>
