<?php $url = new Url() ?>
<!--<div id="footer">
    <span>HocNhom ©2012 </span> |
    <a href="javascript:">Về chúng tôi</a> <span>|</span>
    <a href="javascript:" class="ft-blog">Blog</a> <span class="ft-blog">|</span>
    <a href="javascript:" class="ft-help">Trợ giúp</a> <span class="ft-help">|</span>
    <a href="javascript:">Tuyển dụng</a> <span>|</span>
    <a href="javascript:">Liên hệ</a> <span>|</span>
    <a href="javascript:">Tin tức</a> <span>|</span>
    <a href="javascript:">Điều khoản</a> <span>|</span>
    <a href="javascript:">Bảo mật</a> <span>|</span>
</div>  --> 
<footer class="global-footer" role="contentinfo">
    <div class="container">
        <ul id="footer-nav" class="horz-menu">
            <li><a href="javascript:">Về chúng tôi</a></li>
            <li><a href="javascript:" class="ft-blog">Blog</a></li>
            <li><a href="javascript:" class="ft-help">Trợ giúp</a></li>
            <li><a href="javascript:">Tuyển dụng</a></li>
            <li><a href="javascript:">Liên hệ</a></li>
            <li><a href="javascript:">Tin tức</a></li>
            <li><a href="javascript:">Điều khoản</a></li>            
            <li><a href="javascript:">Bảo mật</a></li>
        </ul>
        <ul id="copyright" class="horz-menu">
            <li class="first copyright-notice">© 2012 Edu.net</li>
            <li id="sales-phone-number">iColdStyle: 0-945-69-4499</li>
        </ul>
    </div>
</footer>