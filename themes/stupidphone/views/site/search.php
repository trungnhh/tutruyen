<?php
	$url = new Url();
?>
<div class="page search-page">

	<div class="block">
		<div class="title">
			<h3>Tìm kiếm</h3>
		</div>
		<form id="tuturyen_search_form" method="get" autocomplete="off" action="<?php echo $url->createUrl("site/search"); ?>">
			<table>
				<tbody>
				<tr>
					<td><input type="text" id="keyword" name="keyword" value="<?php echo $_GET['keyword']; ?>" autocomplete="off" placeholder="Tìm kiếm..." class="input-text p-search">
					</td>
					<td>
						<button type="submit" class="button-search"><span>Go</span></button>
					</td>
				</tr>
				</tbody>
			</table>
		</form>
	</div>

	<div class="block">
		<h3>Kết quả cho "<span><?php echo $_GET['keyword']; ?></span>"</h3>
		<div class="page-content">
			<ul>
				<?php foreach($data_posts as $row){ ?>
				<li><a href="<?php echo $url->createUrl("post/detail",array("category_alias"=>$row['alias'],"post_alias"=>$row['post_alias'])); ?>"><?php echo $row['post_title']; ?> <?php if($row['post_end'] == 1) { echo '[Hoàn thành]';  }?><?php if($row['post_is_full'] == 1) { echo '[Đang cập nhật]';  }?></a>
					<p class="author">
						<?php if($row['author_name'] != ''){ ?>
							<?php echo $row['author_name']; ?>
						<?php }else{ ?>
							Sưu tầm
						<?php } ?>
					</p>
				</li>
				<?php } ?>
			</ul>
		</div>
		<?php if($count > 0){ ?>
			<div class="pager">
				<ul>
					<?php
						echo $paging;
					?>
				</ul>
			</div>
		<?php } ?>
	</div>
</div>