<div id="autogen_id_17232583" data-lazy_images="true" data-lazy_horizontal_images="true"
     class="autogen_class_views_shared_carousels_document_carousel autogen_class_views_shared_carousels_carousel has_paddles carousel_widget">
<div class="module_edge_right"></div>
<div data-track="fiction___literature:previous" class="paddle page_left carousel_prev">
    <div class="sprite paddle_left"></div>
</div>
<div data-track="fiction___literature:next" class="paddle page_right carousel_next">
    <div class="sprite paddle_right"></div>
</div>
<div data-track="fiction___literature" class="carousel_title"><a href="http://www.scribd.com/browse/Books/Fiction"
                                                                 class="title_link">Fiction &amp; Literature</a><span
        class="sub_title">Subscription</span></div>
<div class="carousel" data-jcarousel="true">
<ul style="left: 0px; top: 0px;">
<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:1,&quot;doc_id&quot;:177086552,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="1" data-track_event="click_list" data-track="fiction___literature" data-object_position="1"
        data-object_list_title="fiction___literature" data-object_id="177086552" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/177086552/Slaughterhouse-Five">


            <div
                style="background-image: url('http://imgv2-1.scribdassets.com/img/word_document/177086552/original/fit_to_size/183x250/2ab1afcae2/1383610786')"
                data-thumb_url="" class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/177086552/Slaughterhouse-Five">
                    Slaughterhouse-Five
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/kurt0vonnegut">Kurt Vonnegut</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:2,&quot;doc_id&quot;:163613446,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="2" data-track_event="click_list" data-track="fiction___literature" data-object_position="2"
        data-object_list_title="fiction___literature" data-object_id="163613446" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163613446/Neverwhere">


            <div
                style="background-image: url('http://imgv2-3.scribdassets.com/img/word_document/163613446/original/fit_to_size/183x250/ed8b261ffe/1383609157')"
                data-thumb_url="" class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163613446/Neverwhere">
                    Neverwhere
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/neil8gaiman">Neil Gaiman</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:3,&quot;doc_id&quot;:163656139,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="3" data-track_event="click_list" data-track="fiction___literature" data-object_position="3"
        data-object_list_title="fiction___literature" data-object_id="163656139" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163656139/American-Gods">


            <div
                style="background-image: url('http://imgv2-4.scribdassets.com/img/word_document/163656139/original/fit_to_size/183x250/68aacebece/1383608192')"
                data-thumb_url="" class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163656139/American-Gods">
                    American Gods
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/neil8gaiman">Neil Gaiman</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:4,&quot;doc_id&quot;:168907455,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="4" data-track_event="click_list" data-track="fiction___literature" data-object_position="4"
        data-object_list_title="fiction___literature" data-object_id="168907455" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/168907455/The-Alchemist-10th-Anniversary-Edition">


            <div
                style="background-image: url('http://imgv2-4.scribdassets.com/img/word_document/168907455/original/fit_to_size/183x250/d5492438f2/1383609151')"
                data-thumb_url="" class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/168907455/The-Alchemist-10th-Anniversary-Edition">
                    The Alchemist - 10th Anniversary Edition
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/paulo_coelho_86">Paulo Coelho</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:5,&quot;doc_id&quot;:153598980,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="5" data-track_event="click_list" data-track="fiction___literature" data-object_position="5"
        data-object_list_title="fiction___literature" data-object_id="153598980" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/153598980/Water-for-Elephants-A-Novel">


            <div
                style="background-image: url('http://imgv2-2.scribdassets.com/img/word_document/153598980/original/fit_to_size/183x250/72b5fb9b00/1383608263')"
                data-thumb_url="" class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/153598980/Water-for-Elephants-A-Novel">
                    Water for Elephants: A Novel
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/sara9gruen">Sara Gruen</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:6,&quot;doc_id&quot;:163638327,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="6" data-track_event="click_list" data-track="fiction___literature" data-object_position="6"
        data-object_list_title="fiction___literature" data-object_id="163638327" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163638327/Good-Omens">


            <div
                style="background-image: url(&quot;http://imgv2-4.scribdassets.com/img/word_document/163638327/original/fit_to_size/183x250/878a68c0b5/1383602585&quot;);"
                data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/163638327/original/fit_to_size/183x250/878a68c0b5/1383602585"
                class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163638327/Good-Omens">
                    Good Omens
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/neil8gaiman">Neil Gaiman</a>, <a
                        href="http://www.scribd.com/terry7pratchett">Terry Pratchett</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:7,&quot;doc_id&quot;:163634842,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="7" data-track_event="click_list" data-track="fiction___literature" data-object_position="7"
        data-object_list_title="fiction___literature" data-object_id="163634842" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163634842/Wicked-Life-and-Times-of-the-Wicked-Witch-of-the-West">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/163634842/original/fit_to_size/183x250/dfc20d9eaf/1383608200"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163634842/Wicked-Life-and-Times-of-the-Wicked-Witch-of-the-West">
                    Wicked: Life and Times of the Wicked Witch of the West
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/gregory6maguire">Gregory Maguire</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:8,&quot;doc_id&quot;:163644810,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="8" data-track_event="click_list" data-track="fiction___literature" data-object_position="8"
        data-object_list_title="fiction___literature" data-object_id="163644810" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163644810/Stardust">


            <div style=""
                 data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/163644810/original/fit_to_size/183x250/adc0570710/1383602588"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163644810/Stardust">
                    Stardust
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/neil8gaiman">Neil Gaiman</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:9,&quot;doc_id&quot;:177086446,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="9" data-track_event="click_list" data-track="fiction___literature" data-object_position="9"
        data-object_list_title="fiction___literature" data-object_id="177086446" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/177086446/Cat-s-Cradle">


            <div style=""
                 data-thumb_url="http://imgv2-3.scribdassets.com/img/word_document/177086446/original/fit_to_size/183x250/38d3288cc4/1383607725"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/177086446/Cat-s-Cradle">
                    Cat's Cradle
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/kurt0vonnegut">Kurt Vonnegut</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:10,&quot;doc_id&quot;:163638703,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="10" data-track_event="click_list" data-track="fiction___literature" data-object_position="10"
        data-object_list_title="fiction___literature" data-object_id="163638703" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163638703/Anansi-Boys">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/163638703/original/fit_to_size/183x250/58c7061911/1383602590"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163638703/Anansi-Boys">
                    Anansi Boys
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/neil8gaiman">Neil Gaiman</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:11,&quot;doc_id&quot;:163584403,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="11" data-track_event="click_list" data-track="fiction___literature" data-object_position="11"
        data-object_list_title="fiction___literature" data-object_id="163584403" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163584403/Beowulf">


            <div style=""
                 data-thumb_url="http://imgv2-3.scribdassets.com/img/word_document/163584403/original/fit_to_size/183x250/b9faf599c4/1383589465"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163584403/Beowulf">
                    Beowulf
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/caitlin4r44kiernan">Caitlin R. Kiernan</a>, <a
                        href="http://www.scribd.com/neil8gaiman">Neil Gaiman</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:12,&quot;doc_id&quot;:163603389,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="12" data-track_event="click_list" data-track="fiction___literature" data-object_position="12"
        data-object_list_title="fiction___literature" data-object_id="163603389" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163603389/A-Prayer-for-Owen-Meany-A-Novel">


            <div style=""
                 data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/163603389/original/fit_to_size/183x250/9fa87fb2e8/1383545586"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163603389/A-Prayer-for-Owen-Meany-A-Novel">
                    A Prayer for Owen Meany: A Novel
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/john2irving">John Irving</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:13,&quot;doc_id&quot;:163599595,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="13" data-track_event="click_list" data-track="fiction___literature" data-object_position="13"
        data-object_list_title="fiction___literature" data-object_id="163599595" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163599595/Cryptonomicon">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/163599595/original/fit_to_size/183x250/2ffa03d14f/1383582357"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163599595/Cryptonomicon">
                    Cryptonomicon
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/neal6stephenson">Neal Stephenson</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:14,&quot;doc_id&quot;:177086575,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="14" data-track_event="click_list" data-track="fiction___literature" data-object_position="14"
        data-object_list_title="fiction___literature" data-object_id="177086575" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/177086575/Breakfast-of-Champions">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/177086575/original/fit_to_size/183x250/0022f68aad/1383607733"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/177086575/Breakfast-of-Champions">
                    Breakfast of Champions
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/kurt0vonnegut">Kurt Vonnegut</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:15,&quot;doc_id&quot;:163565859,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="15" data-track_event="click_list" data-track="fiction___literature" data-object_position="15"
        data-object_list_title="fiction___literature" data-object_id="163565859" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163565859/The-Vampire-Diaries-The-Awakening">


            <div style=""
                 data-thumb_url="http://imgv2-3.scribdassets.com/img/word_document/163565859/original/fit_to_size/183x250/5f884d6abf/1383610780"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163565859/The-Vampire-Diaries-The-Awakening">
                    The Vampire Diaries: The Awakening
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/l11j11smith">L. J. Smith</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:16,&quot;doc_id&quot;:163639633,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="16" data-track_event="click_list" data-track="fiction___literature" data-object_position="16"
        data-object_list_title="fiction___literature" data-object_id="163639633" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163639633/Their-Eyes-Were-Watching-God">


            <div style=""
                 data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/163639633/original/fit_to_size/183x250/52b107fd62/1383608208"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163639633/Their-Eyes-Were-Watching-God">
                    Their Eyes Were Watching God
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/zora4neale4hurston">Zora Neale Hurston</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:17,&quot;doc_id&quot;:163649424,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="17" data-track_event="click_list" data-track="fiction___literature" data-object_position="17"
        data-object_list_title="fiction___literature" data-object_id="163649424" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163649424/Confessions-Of-An-Ugly-Stepsister">


            <div style=""
                 data-thumb_url="http://imgv2-3.scribdassets.com/img/word_document/163649424/original/fit_to_size/183x250/66635853f0/1383610776"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163649424/Confessions-Of-An-Ugly-Stepsister">
                    Confessions Of An Ugly Stepsister
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/gregory6maguire">Gregory Maguire</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:18,&quot;doc_id&quot;:150477245,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="18" data-track_event="click_list" data-track="fiction___literature" data-object_position="18"
        data-object_list_title="fiction___literature" data-object_id="150477245" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/150477245/Wizard-s-First-Rule">


            <div style=""
                 data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/150477245/original/fit_to_size/183x250/ddb9d855dd/1383476923"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/150477245/Wizard-s-First-Rule">
                    Wizard's First Rule
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/terry5goodkind">Terry Goodkind</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:19,&quot;doc_id&quot;:163657166,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="19" data-track_event="click_list" data-track="fiction___literature" data-object_position="19"
        data-object_list_title="fiction___literature" data-object_id="163657166" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163657166/A-Tree-Grows-in-Brooklyn">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/163657166/original/fit_to_size/183x250/b7ee94ced4/1383476918"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163657166/A-Tree-Grows-in-Brooklyn">
                    A Tree Grows in Brooklyn
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/betty9smith">Betty Smith</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:20,&quot;doc_id&quot;:150477787,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="20" data-track_event="click_list" data-track="fiction___literature" data-object_position="20"
        data-object_list_title="fiction___literature" data-object_id="150477787" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/150477787/Rama-II">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/150477787/original/fit_to_size/183x250/0b198f4d15/1383544011"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/150477787/Rama-II">
                    Rama II
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/arthur1c11clarke">Arthur C. Clarke</a>, <a
                        href="http://www.scribd.com/gentry0lee">Gentry Lee</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:21,&quot;doc_id&quot;:163583806,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="21" data-track_event="click_list" data-track="fiction___literature" data-object_position="21"
        data-object_list_title="fiction___literature" data-object_id="163583806" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163583806/A-Thief-of-Time">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/163583806/original/fit_to_size/183x250/df76b7063d/1383577138"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163583806/A-Thief-of-Time">
                    A Thief of Time
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/tony8hillerman">Tony Hillerman</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:22,&quot;doc_id&quot;:163585877,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="22" data-track_event="click_list" data-track="fiction___literature" data-object_position="22"
        data-object_list_title="fiction___literature" data-object_id="163585877" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163585877/Quicksilver">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/163585877/original/fit_to_size/183x250/cec274d855/1383581839"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163585877/Quicksilver">
                    Quicksilver
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/neal6stephenson">Neal Stephenson</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:23,&quot;doc_id&quot;:163654132,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="23" data-track_event="click_list" data-track="fiction___literature" data-object_position="23"
        data-object_list_title="fiction___literature" data-object_id="163654132" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163654132/Divine-Secrets-of-the-Ya-Ya-Sisterhood-Novel-A">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/163654132/original/fit_to_size/183x250/dc679cb3bd/1383609359"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163654132/Divine-Secrets-of-the-Ya-Ya-Sisterhood-Novel-A">
                    Divine Secrets of the Ya-Ya Sisterhood: Novel, A
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/rebecca2wells">Rebecca Wells</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:24,&quot;doc_id&quot;:163558726,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="24" data-track_event="click_list" data-track="fiction___literature" data-object_position="24"
        data-object_list_title="fiction___literature" data-object_id="163558726" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163558726/Mystic-River">


            <div style=""
                 data-thumb_url="http://imgv2-3.scribdassets.com/img/word_document/163558726/original/fit_to_size/183x250/a5508c4c48/1383602610"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163558726/Mystic-River">
                    Mystic River
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/dennis7lehane">Dennis Lehane</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:25,&quot;doc_id&quot;:163642803,&quot;list&quot;:&quot;fiction___literature&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="25" data-track_event="click_list" data-track="fiction___literature" data-object_position="25"
        data-object_list_title="fiction___literature" data-object_id="163642803" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163642803/Smoke-and-Mirrors">


            <div style=""
                 data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/163642803/original/fit_to_size/183x250/ad71957822/1383602615"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163642803/Smoke-and-Mirrors">
                    Smoke and Mirrors
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/neil8gaiman">Neil Gaiman</a>
                </div>

            </div>
        </div>
    </div>
</li>


<li class="carousel_separator item"></li>
</ul>
</div>
<p class="carousel_pagination"></p></div>
<script type="text/javascript">
    //&lt;![CDATA[

    new Scribd.UI.Carousel("#autogen_id_17232583")


    //]]&gt;
</script>
<div id="autogen_id_17233362" data-lazy_images="true" data-lazy_horizontal_images="true"
     class="autogen_class_views_shared_carousels_document_carousel autogen_class_views_shared_carousels_carousel has_paddles carousel_widget">
<div class="module_edge_right"></div>
<div data-track="new___noteworthy:previous" class="paddle page_left carousel_prev">
    <div class="sprite paddle_left"></div>
</div>
<div data-track="new___noteworthy:next" class="paddle page_right carousel_next">
    <div class="sprite paddle_right"></div>
</div>
<div data-track="new___noteworthy" class="carousel_title"><a href="http://www.scribd.com/collections/4370719"
                                                             class="title_link">New &amp; Noteworthy</a><span
        class="sub_title">Subscription</span></div>
<div class="carousel" data-jcarousel="true">
<ul style="left: 0px; top: 0px;">
<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:1,&quot;doc_id&quot;:170779888,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="1" data-track_event="click_list" data-track="new___noteworthy" data-object_position="1"
        data-object_list_title="new___noteworthy" data-object_id="170779888" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/170779888/Indian-Killer-A-Novel">


            <div
                style="background-image: url(&quot;http://imgv2-1.scribdassets.com/img/word_document/170779888/original/fit_to_size/183x250/f630766097/1383331478&quot;);"
                data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/170779888/original/fit_to_size/183x250/f630766097/1383331478"
                class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/170779888/Indian-Killer-A-Novel">
                    Indian Killer: A Novel
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/sherman7alexie">Sherman Alexie</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:2,&quot;doc_id&quot;:171519431,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="2" data-track_event="click_list" data-track="new___noteworthy" data-object_position="2"
        data-object_list_title="new___noteworthy" data-object_id="171519431" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171519431/Classified">


            <div
                style="background-image: url(&quot;http://imgv2-2.scribdassets.com/img/word_document/171519431/original/fit_to_size/183x250/51feee15dc/1383364549&quot;);"
                data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/171519431/original/fit_to_size/183x250/51feee15dc/1383364549"
                class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171519431/Classified">
                    Classified
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/fern0michaels">Fern Michaels</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:3,&quot;doc_id&quot;:154387907,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="3" data-track_event="click_list" data-track="new___noteworthy" data-object_position="3"
        data-object_list_title="new___noteworthy" data-object_id="154387907" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/154387907/A-Ranger-s-Tale-Tallenmere-Book-One">


            <div
                style="background-image: url(&quot;http://imgv2-4.scribdassets.com/img/word_document/154387907/original/fit_to_size/183x250/e2e026565c/1383611340&quot;);"
                data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/154387907/original/fit_to_size/183x250/e2e026565c/1383611340"
                class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/154387907/A-Ranger-s-Tale-Tallenmere-Book-One">
                    A Ranger's Tale (Tallenmere: Book One)
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/melange7books">Melange Books</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:4,&quot;doc_id&quot;:171532300,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="4" data-track_event="click_list" data-track="new___noteworthy" data-object_position="4"
        data-object_list_title="new___noteworthy" data-object_id="171532300" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171532300/The-Morning-After">


            <div
                style="background-image: url(&quot;http://imgv2-2.scribdassets.com/img/word_document/171532300/original/fit_to_size/183x250/f93264dbe1/1383336005&quot;);"
                data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/171532300/original/fit_to_size/183x250/f93264dbe1/1383336005"
                class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171532300/The-Morning-After">
                    The Morning After
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/lisa3jackson">Lisa Jackson</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:5,&quot;doc_id&quot;:149783356,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="5" data-track_event="click_list" data-track="new___noteworthy" data-object_position="5"
        data-object_list_title="new___noteworthy" data-object_id="149783356" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/149783356/The-Pitcher">


            <div
                style="background-image: url(&quot;http://imgv2-4.scribdassets.com/img/word_document/149783356/original/fit_to_size/183x250/de4c64fb05/1383342308&quot;);"
                data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/149783356/original/fit_to_size/183x250/de4c64fb05/1383342308"
                class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/149783356/The-Pitcher">
                    The Pitcher
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/william4hazelgrove">William Hazelgrove</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:6,&quot;doc_id&quot;:155741877,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="6" data-track_event="click_list" data-track="new___noteworthy" data-object_position="6"
        data-object_list_title="new___noteworthy" data-object_id="155741877" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/155741877/The-Adventures-of-Mr-Clackworthy-8-Classic-Tales-of-the-Con-Man">


            <div
                style="background-image: url(&quot;http://imgv2-4.scribdassets.com/img/word_document/155741877/original/fit_to_size/183x250/fc391ab298/1383497113&quot;);"
                data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/155741877/original/fit_to_size/183x250/fc391ab298/1383497113"
                class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/155741877/The-Adventures-of-Mr-Clackworthy-8-Classic-Tales-of-the-Con-Man">
                    The Adventures of Mr. Clackworthy: 8 Classic Tales of the...
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/christopher9b99booth">Christopher B. Booth</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:7,&quot;doc_id&quot;:171523935,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="7" data-track_event="click_list" data-track="new___noteworthy" data-object_position="7"
        data-object_list_title="new___noteworthy" data-object_id="171523935" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171523935/No-Mercy">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/171523935/original/fit_to_size/183x250/5bc86704df/1383335140"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171523935/No-Mercy">
                    No Mercy
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/john2gilstrap">John Gilstrap</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:8,&quot;doc_id&quot;:160874447,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="8" data-track_event="click_list" data-track="new___noteworthy" data-object_position="8"
        data-object_list_title="new___noteworthy" data-object_id="160874447" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/160874447/Follow-the-Rabbit-Proof-Fence">


            <div style=""
                 data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/160874447/original/fit_to_size/183x250/6243c46404/1383286433"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/160874447/Follow-the-Rabbit-Proof-Fence">
                    Follow the Rabbit-Proof Fence
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/doris3garimara3pilki">Doris Garimara Pilkington</a>, <a
                        href="http://www.scribd.com/doris5pilkington">Doris Pilkington</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:9,&quot;doc_id&quot;:149772971,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="9" data-track_event="click_list" data-track="new___noteworthy" data-object_position="9"
        data-object_list_title="new___noteworthy" data-object_id="149772971" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/149772971/Supplement-Your-Perscription">


            <div style=""
                 data-thumb_url="http://imgv2-3.scribdassets.com/img/word_document/149772971/original/fit_to_size/183x250/f04825cfaf/1383507608"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/149772971/Supplement-Your-Perscription">
                    Supplement Your Perscription
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/hyla1cass1m1d1">Hyla Cass M.D.</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:10,&quot;doc_id&quot;:171103430,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="10" data-track_event="click_list" data-track="new___noteworthy" data-object_position="10"
        data-object_list_title="new___noteworthy" data-object_id="171103430" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171103430/The-Color-of-Love">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/171103430/original/fit_to_size/183x250/9fda169790/1383333719"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171103430/The-Color-of-Love">
                    The Color of Love
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/sandra5kitt">Sandra Kitt</a>, <a
                        href="http://www.scribd.com/sandra3kitt">Sandra Kitt</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:11,&quot;doc_id&quot;:171098246,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="11" data-track_event="click_list" data-track="new___noteworthy" data-object_position="11"
        data-object_list_title="new___noteworthy" data-object_id="171098246" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171098246/Whose-Body">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/171098246/original/fit_to_size/183x250/68e466a5ea/1383369195"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171098246/Whose-Body">
                    Whose Body?
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/dorothy7l77sayers">Dorothy L. Sayers</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:12,&quot;doc_id&quot;:171107510,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="12" data-track_event="click_list" data-track="new___noteworthy" data-object_position="12"
        data-object_list_title="new___noteworthy" data-object_id="171107510" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171107510/When-Next-We-Love">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/171107510/original/fit_to_size/183x250/14b998a196/1383333675"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171107510/When-Next-We-Love">
                    When Next We Love
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/heather9graham">Heather Graham</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:13,&quot;doc_id&quot;:171106877,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="13" data-track_event="click_list" data-track="new___noteworthy" data-object_position="13"
        data-object_list_title="new___noteworthy" data-object_id="171106877" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171106877/Zipporah-s-Daughter">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/171106877/original/fit_to_size/183x250/45805acd48/1383521974"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171106877/Zipporah-s-Daughter">
                    Zipporah's Daughter
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/philippa1carr">Philippa Carr</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:14,&quot;doc_id&quot;:171531074,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="14" data-track_event="click_list" data-track="new___noteworthy" data-object_position="14"
        data-object_list_title="new___noteworthy" data-object_id="171531074" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171531074/Sugar-Rush">


            <div style=""
                 data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/171531074/original/fit_to_size/183x250/144590f211/1383336150"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171531074/Sugar-Rush">
                    Sugar Rush
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/donna8kauffman">Donna Kauffman</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:15,&quot;doc_id&quot;:160772240,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="15" data-track_event="click_list" data-track="new___noteworthy" data-object_position="15"
        data-object_list_title="new___noteworthy" data-object_id="160772240" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/160772240/Traditional-Foods-Are-Your-Best-Medicine">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/160772240/original/fit_to_size/183x250/6652985b02/1383485066"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/160772240/Traditional-Foods-Are-Your-Best-Medicine">
                    Traditional Foods Are Your Best Medicine
                </a>
            </div>
            <div class="document_author">


                <a href="http://www.scribd.com/InnerTraditions">Inner Traditions</a>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:16,&quot;doc_id&quot;:171580147,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="16" data-track_event="click_list" data-track="new___noteworthy" data-object_position="16"
        data-object_list_title="new___noteworthy" data-object_id="171580147" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171580147/If-I-m-So-Wonderful-Why-Am-I-Still-Single">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/171580147/original/fit_to_size/183x250/36a944bb17/1383336967"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171580147/If-I-m-So-Wonderful-Why-Am-I-Still-Single">
                    If I'm So Wonderful, Why Am I Still Single?
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/susan7page">Susan Page</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:17,&quot;doc_id&quot;:160772246,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="17" data-track_event="click_list" data-track="new___noteworthy" data-object_position="17"
        data-object_list_title="new___noteworthy" data-object_id="160772246" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/160772246/Aspects-in-Astrology">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/160772246/original/fit_to_size/183x250/592d321386/1383292130"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/160772246/Aspects-in-Astrology">
                    Aspects in Astrology
                </a>
            </div>
            <div class="document_author">


                <a href="http://www.scribd.com/InnerTraditions">Inner Traditions</a>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:18,&quot;doc_id&quot;:171080208,&quot;list&quot;:&quot;new___noteworthy&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="18" data-track_event="click_list" data-track="new___noteworthy" data-object_position="18"
        data-object_list_title="new___noteworthy" data-object_id="171080208" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171080208/The-Fatal-Crown-A-Novel">


            <div style=""
                 data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/171080208/original/fit_to_size/183x250/c62f7a9d42/1383331957"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171080208/The-Fatal-Crown-A-Novel">
                    The Fatal Crown: A Novel
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/ellen7jones">Ellen Jones</a>
                </div>

            </div>
        </div>
    </div>
</li>


<li class="carousel_separator item"></li>
</ul>
</div>
<p class="carousel_pagination"></p></div>
<script type="text/javascript">
    //&lt;![CDATA[

    new Scribd.UI.Carousel("#autogen_id_17233362")


    //]]&gt;
</script>
<div id="autogen_id_17234728" data-lazy_images="true" data-lazy_horizontal_images="true"
     class="autogen_class_views_shared_carousels_document_carousel autogen_class_views_shared_carousels_carousel has_paddles carousel_widget">
<div class="module_edge_right"></div>
<div data-track="recently_added:previous" class="paddle page_left carousel_prev">
    <div class="sprite paddle_left"></div>
</div>
<div data-track="recently_added:next" class="paddle page_right carousel_next">
    <div class="sprite paddle_right"></div>
</div>
<div data-track="recently_added" class="carousel_title"><a href="http://www.scribd.com/collections/4371965"
                                                           class="title_link">Recently Added</a><span class="sub_title">Subscription</span>
</div>
<div class="carousel" data-jcarousel="true">
<ul style="left: 0px; top: 0px;">
<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:1,&quot;doc_id&quot;:177086552,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="1" data-track_event="click_list" data-track="recently_added" data-object_position="1"
        data-object_list_title="recently_added" data-object_id="177086552" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/177086552/Slaughterhouse-Five">


            <div
                style="background-image: url(&quot;http://imgv2-1.scribdassets.com/img/word_document/177086552/original/fit_to_size/183x250/2ab1afcae2/1383610786&quot;);"
                data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/177086552/original/fit_to_size/183x250/2ab1afcae2/1383610786"
                class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/177086552/Slaughterhouse-Five">
                    Slaughterhouse-Five
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/kurt0vonnegut">Kurt Vonnegut</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:2,&quot;doc_id&quot;:177086512,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="2" data-track_event="click_list" data-track="recently_added" data-object_position="2"
        data-object_list_title="recently_added" data-object_id="177086512" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/177086512/The-7-Habits-of-Highly-Effective-People">


            <div
                style="background-image: url(&quot;http://imgv2-3.scribdassets.com/img/word_document/177086512/original/fit_to_size/183x250/77ac7d7a98/1383485101&quot;);"
                data-thumb_url="http://imgv2-3.scribdassets.com/img/word_document/177086512/original/fit_to_size/183x250/77ac7d7a98/1383485101"
                class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/177086512/The-7-Habits-of-Highly-Effective-People">
                    The 7 Habits of Highly Effective People
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/stephen6r66covey">Stephen R. Covey</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:3,&quot;doc_id&quot;:175951202,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="3" data-track_event="click_list" data-track="recently_added" data-object_position="3"
        data-object_list_title="recently_added" data-object_id="175951202" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/175951202/We-Promise-Not-To-Tell">


            <div
                style="background-image: url(&quot;http://imgv2-4.scribdassets.com/img/word_document/175951202/original/fit_to_size/183x250/be68218c41/1383340019&quot;);"
                data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/175951202/original/fit_to_size/183x250/be68218c41/1383340019"
                class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/175951202/We-Promise-Not-To-Tell">
                    We Promise Not To Tell
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/albert5able">Albert Able</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:4,&quot;doc_id&quot;:177086466,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="4" data-track_event="click_list" data-track="recently_added" data-object_position="4"
        data-object_list_title="recently_added" data-object_id="177086466" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/177086466/The-Millionaire-Mind">


            <div
                style="background-image: url(&quot;http://imgv2-3.scribdassets.com/img/word_document/177086466/original/fit_to_size/183x250/df981b50ad/1383341069&quot;);"
                data-thumb_url="http://imgv2-3.scribdassets.com/img/word_document/177086466/original/fit_to_size/183x250/df981b50ad/1383341069"
                class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/177086466/The-Millionaire-Mind">
                    The Millionaire Mind
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/thomas7j77stanley7ph">Thomas J. Stanley Ph.D.</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:5,&quot;doc_id&quot;:177086692,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="5" data-track_event="click_list" data-track="recently_added" data-object_position="5"
        data-object_list_title="recently_added" data-object_id="177086692" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/177086692/Hocus-Pocus">


            <div
                style="background-image: url(&quot;http://imgv2-3.scribdassets.com/img/word_document/177086692/original/fit_to_size/183x250/e30d6b2ae9/1383340619&quot;);"
                data-thumb_url="http://imgv2-3.scribdassets.com/img/word_document/177086692/original/fit_to_size/183x250/e30d6b2ae9/1383340619"
                class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/177086692/Hocus-Pocus">
                    Hocus Pocus
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/kurt7vonnegut">Kurt Vonnegut</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:6,&quot;doc_id&quot;:177086680,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="6" data-track_event="click_list" data-track="recently_added" data-object_position="6"
        data-object_list_title="recently_added" data-object_id="177086680" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/177086680/The-Millionaire-Next-Door">


            <div
                style="background-image: url(&quot;http://imgv2-2.scribdassets.com/img/word_document/177086680/original/fit_to_size/183x250/3658d17a77/1383491154&quot;);"
                data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/177086680/original/fit_to_size/183x250/3658d17a77/1383491154"
                class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/177086680/The-Millionaire-Next-Door">
                    The Millionaire Next Door
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/thomas7j77stanley7ph">Thomas J. Stanley Ph.D.</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:7,&quot;doc_id&quot;:177086575,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="7" data-track_event="click_list" data-track="recently_added" data-object_position="7"
        data-object_list_title="recently_added" data-object_id="177086575" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/177086575/Breakfast-of-Champions">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/177086575/original/fit_to_size/183x250/0022f68aad/1383607733"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/177086575/Breakfast-of-Champions">
                    Breakfast of Champions
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/kurt0vonnegut">Kurt Vonnegut</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:8,&quot;doc_id&quot;:166896361,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="8" data-track_event="click_list" data-track="recently_added" data-object_position="8"
        data-object_list_title="recently_added" data-object_id="166896361" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/166896361/The-State-Is-Out-of-Date-We-Can-Do-It-Better">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/166896361/original/fit_to_size/183x250/f82aa7367a/1383350142"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/166896361/The-State-Is-Out-of-Date-We-Can-Do-It-Better">
                    The State Is Out of Date: We Can Do It Better
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/gregory_sams">Gregory Sams</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:9,&quot;doc_id&quot;:175088944,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="9" data-track_event="click_list" data-track="recently_added" data-object_position="9"
        data-object_list_title="recently_added" data-object_id="175088944" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/175088944/Sally-Discovers-New-York">


            <div style=""
                 data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/175088944/original/fit_to_size/183x250/958a6c0d24/1383339188"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/175088944/Sally-Discovers-New-York">
                    Sally Discovers New York
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/sally3and3friends">Sally and Friends</a>, <a
                        href="http://www.scribd.com/stephen1huneck">Stephen Huneck</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:10,&quot;doc_id&quot;:177086446,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="10" data-track_event="click_list" data-track="recently_added" data-object_position="10"
        data-object_list_title="recently_added" data-object_id="177086446" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/177086446/Cat-s-Cradle">


            <div style=""
                 data-thumb_url="http://imgv2-3.scribdassets.com/img/word_document/177086446/original/fit_to_size/183x250/38d3288cc4/1383607725"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/177086446/Cat-s-Cradle">
                    Cat's Cradle
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/kurt0vonnegut">Kurt Vonnegut</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:11,&quot;doc_id&quot;:174856266,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="11" data-track_event="click_list" data-track="recently_added" data-object_position="11"
        data-object_list_title="recently_added" data-object_id="174856266" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/174856266/Pushed">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/174856266/original/fit_to_size/183x250/2461433081/1383339077"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/174856266/Pushed">
                    Pushed
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/corrine1jackson">Corrine Jackson</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:12,&quot;doc_id&quot;:170464112,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="12" data-track_event="click_list" data-track="recently_added" data-object_position="12"
        data-object_list_title="recently_added" data-object_id="170464112" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/170464112/The-Whole-Guy-Thing-What-Every-Girl-Needs-to-Know-about-Crushes-Friendship-Relating-and-Dating">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/170464112/original/fit_to_size/183x250/2cc5a725d4/1383338964"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/170464112/The-Whole-Guy-Thing-What-Every-Girl-Needs-to-Know-about-Crushes-Friendship-Relating-and-Dating">
                    The Whole Guy Thing: What Every Girl Needs to Know about ...
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/nancy0rue">Nancy Rue</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:13,&quot;doc_id&quot;:174856281,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="13" data-track_event="click_list" data-track="recently_added" data-object_position="13"
        data-object_list_title="recently_added" data-object_id="174856281" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/174856281/A-Not-So-Model-Home">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/174856281/original/fit_to_size/183x250/b6ccff4426/1383339049"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/174856281/A-Not-So-Model-Home">
                    A Not So Model Home
                </a>
            </div>
            <div class="document_author">


                <a href="http://www.scribd.com/kensingtonbooks">Kensington Books</a>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:14,&quot;doc_id&quot;:174856152,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="14" data-track_event="click_list" data-track="recently_added" data-object_position="14"
        data-object_list_title="recently_added" data-object_id="174856152" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/174856152/Secrets-on-Cedar-Key">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/174856152/original/fit_to_size/183x250/02b036f487/1383339038"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/174856152/Secrets-on-Cedar-Key">
                    Secrets on Cedar Key
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/terri6dulong">Terri DuLong</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:15,&quot;doc_id&quot;:175199724,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="15" data-track_event="click_list" data-track="recently_added" data-object_position="15"
        data-object_list_title="recently_added" data-object_id="175199724" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/175199724/We-Love-to-Sew-28-Pretty-Things-to-Make-Jewelry-Headbands-Softies-T-shirts-Pillows-Bags-More">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/175199724/original/fit_to_size/183x250/704021efb0/1383339720"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/175199724/We-Love-to-Sew-28-Pretty-Things-to-Make-Jewelry-Headbands-Softies-T-shirts-Pillows-Bags-More">
                    We Love to Sew: 28 Pretty Things to Make: Jewelry, Headba...
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/annabel0wrigley">Annabel Wrigley</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:16,&quot;doc_id&quot;:173571819,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="16" data-track_event="click_list" data-track="recently_added" data-object_position="16"
        data-object_list_title="recently_added" data-object_id="173571819" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/173571819/Still-a-Kid-at-Heart-My-Life-in-Baseball-and-Beyond">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/173571819/original/fit_to_size/183x250/3b42b00f4f/1383338218"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/173571819/Still-a-Kid-at-Heart-My-Life-in-Baseball-and-Beyond">
                    Still a Kid at Heart: My Life in Baseball and Beyond
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/johnny6bench">Johnny Bench</a>, <a
                        href="http://www.scribd.com/gary1carter">Gary Carter</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:17,&quot;doc_id&quot;:175462001,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="17" data-track_event="click_list" data-track="recently_added" data-object_position="17"
        data-object_list_title="recently_added" data-object_id="175462001" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/175462001/The-Front-Nine-Golf-s-9-All-Time-Greatest-Shots">


            <div style=""
                 data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/175462001/original/fit_to_size/183x250/90019da707/1383339640"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/175462001/The-Front-Nine-Golf-s-9-All-Time-Greatest-Shots">
                    The Front Nine: Golf's 9 All-Time Greatest Shots
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/barry1lebrock">Barry LeBrock</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:18,&quot;doc_id&quot;:175464949,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="18" data-track_event="click_list" data-track="recently_added" data-object_position="18"
        data-object_list_title="recently_added" data-object_id="175464949" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/175464949/The-Making-of-The-Wizard-of-Oz">


            <div style=""
                 data-thumb_url="http://imgv2-3.scribdassets.com/img/word_document/175464949/original/fit_to_size/183x250/f9867b9849/1383341128"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/175464949/The-Making-of-The-Wizard-of-Oz">
                    The Making of The Wizard of Oz
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/margaret8hamilton">Margaret Hamilton</a>, <a
                        href="http://www.scribd.com/aljean3harmetz">Aljean Harmetz</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:19,&quot;doc_id&quot;:170710548,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="19" data-track_event="click_list" data-track="recently_added" data-object_position="19"
        data-object_list_title="recently_added" data-object_id="170710548" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/170710548/Twelve-Times-Blessed">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/170710548/original/fit_to_size/183x250/bab82d8a3e/1383331163"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/170710548/Twelve-Times-Blessed">
                    Twelve Times Blessed
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/jacquelyn6mitchard">Jacquelyn Mitchard</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:20,&quot;doc_id&quot;:170710509,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="20" data-track_event="click_list" data-track="recently_added" data-object_position="20"
        data-object_list_title="recently_added" data-object_id="170710509" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/170710509/The-Last-Goodbye">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/170710509/original/fit_to_size/183x250/13ca129515/1383331098"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/170710509/The-Last-Goodbye">
                    The Last Goodbye
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/reed9arvin">Reed Arvin</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:21,&quot;doc_id&quot;:170710586,&quot;list&quot;:&quot;recently_added&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="21" data-track_event="click_list" data-track="recently_added" data-object_position="21"
        data-object_list_title="recently_added" data-object_id="170710586" data-object_type="document"
        class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/170710586/The-Gospel-of-Anarchy-A-Novel">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/170710586/original/fit_to_size/183x250/07a96dffb2/1383331055"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/170710586/The-Gospel-of-Anarchy-A-Novel">
                    The Gospel of Anarchy: A Novel
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/justin0taylor_1">Justin Taylor</a>
                </div>

            </div>
        </div>
    </div>
</li>


<li class="carousel_separator item"></li>
</ul>
</div>
<p class="carousel_pagination"></p></div>
<script type="text/javascript">
    //&lt;![CDATA[

    new Scribd.UI.Carousel("#autogen_id_17234728")


    //]]&gt;
</script>
<div id="autogen_id_17235057" data-lazy_images="true" data-lazy_horizontal_images="true"
     class="autogen_class_views_shared_carousels_document_carousel autogen_class_views_shared_carousels_carousel has_paddles carousel_widget">
<div class="module_edge_right"></div>
<div data-track="editor_s_picks___food_fiction:previous" class="paddle page_left carousel_prev">
    <div class="sprite paddle_left"></div>
</div>
<div data-track="editor_s_picks___food_fiction:next" class="paddle page_right carousel_next">
    <div class="sprite paddle_right"></div>
</div>
<div data-track="editor_s_picks___food_fiction" class="carousel_title"><a
        href="http://www.scribd.com/collections/4377752" class="title_link">Editor's Picks - Food Fiction</a></div>
<div class="carousel" data-jcarousel="true">
<ul style="left: 0px; top: 0px;">
<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:1,&quot;doc_id&quot;:163653887,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="1" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="1" data-object_list_title="editor_s_picks___food_fiction" data-object_id="163653887"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163653887/The-Recipe-Club-A-Tale-of-Food-and-Friendship">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/163653887/original/fit_to_size/183x250/64423d2b19/1383390415"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163653887/The-Recipe-Club-A-Tale-of-Food-and-Friendship">
                    The Recipe Club: A Tale of Food and Friendship
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/nancy6garfinkel">Nancy Garfinkel</a>, <a
                        href="http://www.scribd.com/andrea8israel">Andrea Israel</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:2,&quot;doc_id&quot;:163646863,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="2" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="2" data-object_list_title="editor_s_picks___food_fiction" data-object_id="163646863"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163646863/Five-Quarters-of-the-Orange">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/163646863/original/fit_to_size/183x250/2195da9718/1383297175"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163646863/Five-Quarters-of-the-Orange">
                    Five Quarters of the Orange
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/joanne5harris">Joanne Harris</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:3,&quot;doc_id&quot;:163587429,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="3" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="3" data-object_list_title="editor_s_picks___food_fiction" data-object_id="163587429"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163587429/How-to-Eat-a-Cupcake-A-Novel">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/163587429/original/fit_to_size/183x250/5953497182/1383561713"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163587429/How-to-Eat-a-Cupcake-A-Novel">
                    How to Eat a Cupcake: A Novel
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/meg8donohue">Meg Donohue</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:4,&quot;doc_id&quot;:171531304,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="4" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="4" data-object_list_title="editor_s_picks___food_fiction" data-object_id="171531304"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171531304/Chocolate-Chip-Cookie-Murder">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/171531304/original/fit_to_size/183x250/aa6c84811e/1383373553"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171531304/Chocolate-Chip-Cookie-Murder">
                    Chocolate Chip Cookie Murder
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/joanne5fluke">Joanne Fluke</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:5,&quot;doc_id&quot;:163650257,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="5" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="5" data-object_list_title="editor_s_picks___food_fiction" data-object_id="163650257"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163650257/The-Baker-s-Apprentice">


            <div style=""
                 data-thumb_url="http://imgv2-3.scribdassets.com/img/word_document/163650257/original/fit_to_size/183x250/90c0a2a830/1383497098"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163650257/The-Baker-s-Apprentice">
                    The Baker's Apprentice
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/judith2r22hendricks">Judith R. Hendricks</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:6,&quot;doc_id&quot;:163565867,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="6" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="6" data-object_list_title="editor_s_picks___food_fiction" data-object_id="163565867"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163565867/Bread-Alone-A-Novel">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/163565867/original/fit_to_size/183x250/910ef7f43a/1383395287"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163565867/Bread-Alone-A-Novel">
                    Bread Alone: A Novel
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/judith2r22hendricks">Judith R. Hendricks</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:7,&quot;doc_id&quot;:163586448,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="7" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="7" data-object_list_title="editor_s_picks___food_fiction" data-object_id="163586448"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163586448/Sweet-Revenge">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/163586448/original/fit_to_size/183x250/2af2bb962b/1383294661"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163586448/Sweet-Revenge">
                    Sweet Revenge
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/diane9mott9davidson">Diane Mott Davidson</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:8,&quot;doc_id&quot;:171533309,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="8" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="8" data-object_list_title="editor_s_picks___food_fiction" data-object_id="171533309"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171533309/A-Catered-Thanksgiving">


            <div style=""
                 data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/171533309/original/fit_to_size/183x250/c381756b46/1383336199"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171533309/A-Catered-Thanksgiving">
                    A Catered Thanksgiving
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/isis6crawford">Isis Crawford</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:9,&quot;doc_id&quot;:163647631,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="9" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="9" data-object_list_title="editor_s_picks___food_fiction" data-object_id="163647631"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163647631/Friendship-Cake">


            <div style=""
                 data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/163647631/original/fit_to_size/183x250/9ed585c5ec/1383297642"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163647631/Friendship-Cake">
                    Friendship Cake
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/lynne3hinton">Lynne Hinton</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:10,&quot;doc_id&quot;:171532290,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="10" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="10" data-object_list_title="editor_s_picks___food_fiction" data-object_id="171532290"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171532290/Death-of-a-Kitchen-Diva">


            <div style=""
                 data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/171532290/original/fit_to_size/183x250/4c83c3e9e0/1383441619"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171532290/Death-of-a-Kitchen-Diva">
                    Death of a Kitchen Diva
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/lee3hollis">Lee Hollis</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:11,&quot;doc_id&quot;:171519734,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="11" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="11" data-object_list_title="editor_s_picks___food_fiction" data-object_id="171519734"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171519734/The-Chocolate-Kiss">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/171519734/original/fit_to_size/183x250/e8fc48b8e0/1383413904"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171519734/The-Chocolate-Kiss">
                    The Chocolate Kiss
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/laura3florand">Laura Florand</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:12,&quot;doc_id&quot;:163565635,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="12" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="12" data-object_list_title="editor_s_picks___food_fiction" data-object_id="163565635"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163565635/Deep-Dish">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/163565635/original/fit_to_size/183x250/c8bb82d0bf/1383292704"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163565635/Deep-Dish">
                    Deep Dish
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/mary1kay1andrews">Mary Kay Andrews</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:13,&quot;doc_id&quot;:171534196,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="13" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="13" data-object_list_title="editor_s_picks___food_fiction" data-object_id="171534196"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171534196/A-Catered-Murder">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/171534196/original/fit_to_size/183x250/bc2bcd2f18/1383396218"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171534196/A-Catered-Murder">
                    A Catered Murder
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/isis6crawford">Isis Crawford</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:14,&quot;doc_id&quot;:171528347,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="14" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="14" data-object_list_title="editor_s_picks___food_fiction" data-object_id="171528347"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171528347/A-Decadent-Way-To-Die">


            <div style=""
                 data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/171528347/original/fit_to_size/183x250/a2f1a7bfd4/1383335940"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171528347/A-Decadent-Way-To-Die">
                    A Decadent Way To Die
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/g88a88mckevett">G. A. McKevett</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:15,&quot;doc_id&quot;:171535178,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="15" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="15" data-object_list_title="editor_s_picks___food_fiction" data-object_id="171535178"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171535178/Wicked-Craving">


            <div style=""
                 data-thumb_url="http://imgv2-3.scribdassets.com/img/word_document/171535178/original/fit_to_size/183x250/83f7632a1b/1383399020"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171535178/Wicked-Craving">
                    Wicked Craving
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/g88a88mckevett">G. A. McKevett</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:16,&quot;doc_id&quot;:171538730,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="16" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="16" data-object_list_title="editor_s_picks___food_fiction" data-object_id="171538730"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171538730/Poisoned-Tarts">


            <div style=""
                 data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/171538730/original/fit_to_size/183x250/735e6ce667/1383336657"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171538730/Poisoned-Tarts">
                    Poisoned Tarts
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/g88a88mckevett">G. A. McKevett</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:17,&quot;doc_id&quot;:171525489,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="17" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="17" data-object_list_title="editor_s_picks___food_fiction" data-object_id="171525489"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171525489/Murder-A-La-Mode">


            <div style=""
                 data-thumb_url="http://imgv2-3.scribdassets.com/img/word_document/171525489/original/fit_to_size/183x250/090513cc0d/1383398672"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171525489/Murder-A-La-Mode">
                    Murder A' La Mode
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/g88a88mckevett">G. A. McKevett</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:18,&quot;doc_id&quot;:163624532,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="18" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="18" data-object_list_title="editor_s_picks___food_fiction" data-object_id="163624532"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163624532/Crunch-Time-A-Novel-of-Suspense">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/163624532/original/fit_to_size/183x250/e446c335e6/1383295730"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163624532/Crunch-Time-A-Novel-of-Suspense">
                    Crunch Time: A Novel of Suspense
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/diane9mott9davidson">Diane Mott Davidson</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:19,&quot;doc_id&quot;:163592050,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="19" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="19" data-object_list_title="editor_s_picks___food_fiction" data-object_id="163592050"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163592050/Fatally-Flaky-A-Novel">


            <div style=""
                 data-thumb_url="http://imgv2-3.scribdassets.com/img/word_document/163592050/original/fit_to_size/183x250/f331b03e57/1383294506"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163592050/Fatally-Flaky-A-Novel">
                    Fatally Flaky: A Novel
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/diane9mott9davidson">Diane Mott Davidson</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:20,&quot;doc_id&quot;:171531197,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="20" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="20" data-object_list_title="editor_s_picks___food_fiction" data-object_id="171531197"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171531197/Death-of-a-Country-Fried-Redneck">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/171531197/original/fit_to_size/183x250/81d6022814/1383421159"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171531197/Death-of-a-Country-Fried-Redneck">
                    Death of a Country Fried Redneck
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/lee3hollis">Lee Hollis</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:21,&quot;doc_id&quot;:171537246,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="21" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="21" data-object_list_title="editor_s_picks___food_fiction" data-object_id="171537246"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171537246/Pepperoni-Pizza-Can-Be-Murder">


            <div style=""
                 data-thumb_url="http://imgv2-1.scribdassets.com/img/word_document/171537246/original/fit_to_size/183x250/74c41d4270/1383413170"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171537246/Pepperoni-Pizza-Can-Be-Murder">
                    Pepperoni Pizza Can Be Murder
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/chris7cavender">Chris Cavender</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:22,&quot;doc_id&quot;:163618608,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="22" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="22" data-object_list_title="editor_s_picks___food_fiction" data-object_id="163618608"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/163618608/Girls-Dinner-Club">


            <div style=""
                 data-thumb_url="http://imgv2-2.scribdassets.com/img/word_document/163618608/original/fit_to_size/183x250/be10fbf7d6/1383417560"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/163618608/Girls-Dinner-Club">
                    Girls Dinner Club
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/jessie5elliot">Jessie Elliot</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:23,&quot;doc_id&quot;:171522511,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="23" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="23" data-object_list_title="editor_s_picks___food_fiction" data-object_id="171522511"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171522511/A-Pizza-To-Die-For">


            <div style=""
                 data-thumb_url="http://imgv2-4.scribdassets.com/img/word_document/171522511/original/fit_to_size/183x250/1019a90ebf/1383412465"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171522511/A-Pizza-To-Die-For">
                    A Pizza To Die For
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/chris7cavender">Chris Cavender</a>
                </div>

            </div>
        </div>
    </div>
</li>

<li class="item">


    <div
        data-track_rats_value="{&quot;context&quot;:&quot;home&quot;,&quot;position&quot;:24,&quot;doc_id&quot;:171526005,&quot;list&quot;:&quot;editor_s_picks___food_fiction&quot;,&quot;event_name&quot;:&quot;click_list&quot;}"
        data-track_value="24" data-track_event="click_list" data-track="editor_s_picks___food_fiction"
        data-object_position="24" data-object_list_title="editor_s_picks___food_fiction" data-object_id="171526005"
        data-object_type="document" class="document_cell object_cell ">
        <a href="http://www.scribd.com/doc/171526005/Gingerbread-Cookie-Murder">


            <div style=""
                 data-thumb_url="http://imgv2-3.scribdassets.com/img/word_document/171526005/original/fit_to_size/183x250/28e4e308d3/1383379901"
                 class="thumb">
                <div class="flag purchase_only">PURCHASE ONLY</div>
                <div class="flag sample">SAMPLE</div>
                <div class="geo_restricted flag">NOT AVAILABLE</div>

                <div class="overlay">
                    <div class="sprite white_big_open_book">
                    </div>
                </div>
            </div>
        </a>

        <div class="cell_data">
            <div class="document_title">
                <a href="http://www.scribd.com/doc/171526005/Gingerbread-Cookie-Murder">
                    Gingerbread Cookie Murder
                </a>
            </div>
            <div class="document_author">

                <div class="author_row">


                    <a href="http://www.scribd.com/leslie7meier">Leslie Meier</a>, <a
                        href="http://www.scribd.com/joanne5fluke">Joanne Fluke</a>
                </div>

            </div>
        </div>
    </div>
</li>


<li class="carousel_separator item"></li>
</ul>
</div>
<p class="carousel_pagination"></p></div>
<script type="text/javascript">
    //&lt;![CDATA[

    new Scribd.UI.Carousel("#autogen_id_17235057")


    //]]&gt;
</script>