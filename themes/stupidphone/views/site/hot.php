<?php
	$url = new Url();
?>

<!-- BEGIN CONTENT -->
<div class="page listing-page">

	<div class="block">
		<div class="page-title">
			<h2><?php echo $title; ?></h2>
		</div>
		<div class="page-content">
				<?php foreach($data_posts as $row) {
					foreach($data_category as $item){
						if($item['id'] == $row['category_id']){
							$category_alias = $item['alias'];
						}
					}
					?>
				<div class="item"><a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>"><?php echo $row['post_title']; ?> <?php if($row['post_end'] == 1) { echo '[Hoàn thành]';  }?><?php if($row['post_is_full'] == 1) { echo '[Đang cập nhật]';  }?></a>
					<p class="author"><?php if($row['author_name'] == '') {echo "Sưu tầm";} else { ?>
						<a href='<?php echo $url->createUrl("author/detail", array("author_alias" => $row['author_alias'])); ?>'><?php echo $row['author_name']; ?></a>
						<?php } ?></p>
				</div>
				<?php } ?>
		</div>
		<div class="clearfix"></div>
		<div class="pager">
			<ul>
				<?php
					echo $paging;
				?>
			</ul>
		</div>
		<div class="clearfix"></div>
	</div>


</div>
<!-- END CONTENT -->