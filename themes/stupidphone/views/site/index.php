<?php
	$url = new Url();
?>
<!-- BEGIN CONTENT -->
<div class="page">
	<?php $this->widget("StupidHot"); ?>
	<?php $this->widget("StupidNotification"); ?>
	<div class="block truyen-moi">
		<div class="title">
			<h2><a href="<?php echo $url->createUrl("site/new"); ?>">Truyện mới</a></h2>
		</div>
		<div class="list">
			<ul>
				<?php $i=0; foreach($data_posts_new as $row) {
					$i++;
					if($i == 6) break;
					foreach($data_category as $item){
						if($item['id'] == $row['category_id']){
							$category_alias = $item['alias'];
						}
					}
				?>
				<li><a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>"><?php echo $row['post_title']; ?> <?php if($row['post_end'] == 1) { echo '[Hoàn thành]';  }?><?php if($row['post_is_full'] == 1) { echo '[Đang cập nhật]';  }?></a></li>
				<?php } ?>
			</ul>
		</div>
		<p class="learn-more"><a href="<?php echo $url->createUrl("site/new"); ?>">Xem tiếp...</a></p>
	</div>
	<div class="block truyen-hot">
		<div class="title">
			<h2><a href="<?php echo $url->createUrl("site/hot"); ?>">Truyện hot</a></h2>
		</div>
		<div class="list">
			<ul>
				<?php $i=0; foreach($data_posts_hot as $row) {
					$i++;
					if($i == 6) break;
					foreach($data_category as $item){
						if($item['id'] == $row['category_id']){
							$category_alias = $item['alias'];
						}
					}
					?>
					<li><a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>"><?php echo $row['post_title']; ?> <?php if($row['post_end'] == 1) { echo '[Hoàn thành]';  }?><?php if($row['post_is_full'] == 1) { echo '[Đang cập nhật]';  }?></a></li>
				<?php } ?>
			</ul>
		</div>
		<p class="learn-more"><a href="<?php echo $url->createUrl("site/hot"); ?>">Xem tiếp...</a></p>
	</div>
	<div class="block truyen-binhchon">
		<div class="title">
			<h2><a href="<?php echo $url->createUrl("site/vote"); ?>">Tuyển tập truyện hay</a></h2>
		</div>
		<div class="list">
			<ul>
				<?php $i=0; foreach($data_posts_vote as $row) {
					$i++;
					if($i == 6) break;
					foreach($data_category as $item){
						if($item['id'] == $row['category_id']){
							$category_alias = $item['alias'];
						}
					}
					?>
					<li><a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>"><?php echo $row['post_title']; ?> <?php if($row['post_end'] == 1) { echo '[Hoàn thành]';  }?><?php if($row['post_is_full'] == 1) { echo '[Đang cập nhật]';  }?></a></li>
				<?php } ?>
			</ul>
		</div>
		<p class="learn-more"><a href="<?php echo $url->createUrl("site/vote"); ?>">Xem tiếp...</a></p>
	</div>
	<?php for($i = 0; $i < count($data_category_hot); $i ++) { ?>
		<div class="block truyen-moi">
			<div class="title">
				<h2><a href="<?php echo $url->createUrl("category/index", array("alias" => $data_category_hot[$i]['alias'])); ?>"><?php echo $data_category_hot[$i]['name']; ?></a></h2>
			</div>
			<div class="list">
				<ul>
					<?php $a=0; foreach($data_posts[$i] as $row) {
						$a++;
						if($a == 6) break;
						?>
						<li><a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $data_category_hot[$i]['alias'], "post_alias" => $row['post_alias'])); ?>"><?php echo $row['post_title']; ?> <?php if($row['post_end'] == 1) { echo '[Hoàn thành]';  }?><?php if($row['post_is_full'] == 1) { echo '[Đang cập nhật]';  }?></a></li>
					<?php } ?>
				</ul>
			</div>
			<p class="learn-more"><a href="<?php echo $url->createUrl("category/index", array("alias" => $data_category_hot[$i]['alias'])); ?>">Xem tiếp...</a></p>
		</div>
	<?php } ?>
</div>
<!-- END CONTENT -->