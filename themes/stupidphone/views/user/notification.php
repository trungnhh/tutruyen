<?php 
    $url = new Url();    
?>
	<link href="<?php echo Yii::app()->params['urlRs']; ?>/css/account_settings.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo Yii::app()->params['urlRs']; ?>/css/notifications.css" rel="stylesheet" type="text/css" />

	<div class="autogen_class_views_notifications_index account_settings_page">
		<div class="home_container">
			<div class="page_header">
				<h2 class="title">Thông báo</h2>
				<?php $this->widget("UserMenu");?>
			</div>
			<div id="autogen_id_482978354" class="autogen_class_views_account_settings_tabs_personal autogen_class_views_account_settings_tabs_base">
				<div class="page_body">
					<div class="error_message" style="display: none"></div>
					<div class="saved_message" style="display: none"></div>
					<div>
						<div class="page_content">
							<div id="autogen_id_612924306" class="notifications_container">
								<div class="no_notifications">Hiện tại bạn vẫn chưa nhận được thông báo nào.</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

<?php include "messege.js.php"; ?>