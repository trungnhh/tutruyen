<?php 
    $url = new Url();
?>

	<!-- BEGIN CONTENT -->
	<div class="page account-page login-page">

		<?php $this->widget("StupidUserMenu"); ?>

		<div class="block">
			<div class="page-title">
				<h2>Thông tin tài khoản</h2>
			</div>
			<form accept-charset="UTF-8" id="user-index" method="post" action="<?=$url->createUrl("user/formSubmitUpdate") ?>">
				<div>
					<div class="form-item form-type-textfield form-item-name">
						<label for="edit-name">Tên truy cập</label>
						<input type="text" class="input-text required" value="<?php echo $data_user['username']; ?>" name="name" id="edit-name" disabled>

						<label for="edit-name">Họ và tên</label>
						<input type="text" class="input-text" value="<?php echo $data_user['fullname']; ?>" name="fullname" id="fullname">

						<label for="edit-name">Điện thoại</label>
						<input type="text" class="input-text" value="<?php echo $data_user['mobile']; ?>" name="mobile" id="mobile">

						<label for="edit-name">Email</label>
						<input type="text" class="input-text" value="<?php echo $data_user['email']; ?>" name="email" id="email">
					</div>
					<div id="update-actions" class="form-actions form-wrapper"><input type="submit" class="form-submit" value="Cập nhật" name="op" id="update-submit"></div>
					<p>Điền thông tin của bạn và chọn cập nhật.</p>
				</div>
			</form>

		</div>
	</div>
	<!-- END CONTENT -->

<?php //include "index.js.php"; ?>