<?php 
    $url = new Url();
?>

	<!-- BEGIN CONTENT -->
	<div class="page account-page login-page">

		<?php $this->widget("StupidUserMenu"); ?>

		<div class="block">
			<div class="page-title">
				<h2>Góp ý</h2>
			</div>
			<form accept-charset="UTF-8" id="user-note" method="post" action="<?=$url->createUrl("user/formSubmitNote") ?>">
				<div>
					<div class="form-item form-type-textfield form-item-name">
						<label for="edit-name">Họ tên <span title="Trường dữ liệu này là bắt buộc." class="form-required">*</span></label>
						<input type="text" class="input-text required" value="<?php echo $_SESSION["user_tt"]['fullname']; ?>" name="fullname" id="fullname">

						<label for="edit-name">Email</label>
						<input type="text" class="input-text required" value="<?php echo $_SESSION["user_tt"]['email']; ?>" name="email" id="email">

						<label for="edit-name">Điện thoại</label>
						<input type="text" class="input-text" value="<?php echo $_SESSION["user_tt"]['mobile']; ?>" name="mobile" id="mobile">

						<label for="edit-name">Nội dung góp ý</label>
						<textarea class="input-text" name="content" id="content"></textarea>
					</div>
					<div id="update-actions" class="form-actions form-wrapper"><input type="submit" class="form-submit" value="Gửi đóng góp" name="op" id="update-submit"></div>
					<?php if($_GET['success'] == 1){ ?>
						<div class="validate validate-fail">Góp ý của bạn đã được gửi cho chúng tôi :D Chúng tôi sẽ luôn cố gắng để bạn có được những trải nghiệm tốt nhất ở Tủ Truyện.</div>
					<?php } ?>
					<?php if($_GET['fail'] == 2){ ?>
						<div class="validate validate-fail">Bạn quên chưa điền nội dung góp ý rồi kìa.</div>
					<?php } ?>
				</div>
			</form>

		</div>
	</div>
	<!-- END CONTENT -->

<?php //include "index.js.php"; ?>