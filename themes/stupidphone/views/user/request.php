<?php 
    $url = new Url();
?>

	<!-- BEGIN CONTENT -->
	<div class="page account-page login-page">
		<div class="block">
			<div class="page-title">
				<h2>Yêu cầu truyện</h2>
			</div>
			<form accept-charset="UTF-8" id="user-request" method="post" action="<?=$url->createUrl("user/formSubmitRequest") ?>">
				<div>
					<div class="form-item form-type-textfield form-item-name">
						<label for="edit-name">Tiêu đề truyện yêu cầu <span title="Trường dữ liệu này là bắt buộc." class="form-required">*</span></label>
						<input type="text" class="input-text required" value="" name="request_name" id="request_name">

						<label for="edit-name">Thể loại</label>
						<select class="input-text" id="category_id" name="category_id">
							<option value="0">Không rõ</option>
							<?php foreach($data_category as $row){ ?>
								<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
							<?php } ?>
						</select>

						<label for="edit-name">Mô tả</label>
						<textarea class="input-text" name="request_content" id="request_content"></textarea>

						<label for="edit-name">Tác giả</label>
						<input type="text" class="input-text" value="" name="request_author" id="request_author">
					</div>
					<?php if(isset($_SESSION["user_tt"])){ ?>
						<div id="update-actions" class="form-actions form-wrapper"><input type="submit" class="form-submit" value="Gửi yêu cầu" name="op" id="update-submit"></div>
					<?php }else{ ?>
						<p>Bạn cần là thành viên để có thể yêu cầu truyện. Hãy click <a href="<?=$url->createUrl("user/login") ?>">vào đây</a> để đăng nhập hoặc đăng ký thành viên.</p>
					<?php } ?>
					<?php if($_GET['success'] == 1){ ?>
						<div class="validate validate-fail">Yêu cầu của bạn đã được gửi cho chúng tôi ;) Hãy quay lại Tủ Truyện thường xuyên trong thời gian tới để đọc truyện bạn vừa yêu cầu nhé.</div>
					<?php } ?>
					<?php if($_GET['fail'] == 2){ ?>
						<div class="validate validate-fail">Bạn quên chưa điền tên truyện rồi kìa.</div>
					<?php } ?>
				</div>
			</form>

		</div>
	</div>
	<!-- END CONTENT -->

<?php //include "index.js.php"; ?>