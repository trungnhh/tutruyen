<?php
	$url = new Url();
?>

	<!-- BEGIN CONTENT -->
	<div class="page account-page login-page">

		<?php $this->widget("StupidUserMenu"); ?>

		<div class="block">
			<div class="page-title">
				<h2>Truyện đánh dấu</h2>
			</div>
			<div class="page-content">
				<?php foreach($data_posts as $row) { ?>
					<div class="item"><a href="<?php echo $url->createUrl("post/detail",array("category_alias"=>$row['alias'],"post_alias"=>$row['post_alias'])); ?>"><?php echo $row['post_title']; ?> <?php if($row['post_end'] == 1) { echo '[Hoàn thành]';  }?><?php if($row['post_is_full'] == 1) { echo '[Đang cập nhật]';  }?></a>
						<p class="author"><?php if($row['author_name'] == '') {echo "Sưu tầm";} else { ?>
								<a href='<?php echo $url->createUrl("author/detail", array("author_alias" => $row['author_alias'])); ?>'><?php echo $row['author_name']; ?></a>
							<?php } ?></p>
					</div>
				<?php } ?>
			</div>
			<div class="clearfix"></div>
			<div class="pager">
				<ul>
					<?php
						echo $paging;
					?>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- END CONTENT -->

<?php //include "index.js.php"; ?>