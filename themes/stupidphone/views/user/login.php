<?php
	$url = new Url();
?>


	<!-- BEGIN CONTENT -->
	<div class="page login-page">
		<div class="block">
			<div class="page-title">
				<h2>Đăng nhập</h2>
			</div>
			<form accept-charset="UTF-8" id="user-login" method="post" action="<?=$url->createUrl("user/formSubmitLogin") ?>">
				<div>
					<div class="form-item form-type-textfield form-item-name">
						<label for="edit-name">Tên truy cập<span title="Trường dữ liệu này là bắt buộc." class="form-required">*</span></label>
						<input type="text" class="input-text required" value="" name="username" id="username" id="edit-name">
					</div>
					<div class="form-item form-type-password form-item-pass">
						<label for="edit-pass">Mật khẩu
							<span title="Trường dữ liệu này là bắt buộc." class="form-required">*</span></label>
						<input type="password" class="input-text required" name="password" id="password" id="edit-pass">
					</div>
					<div id="edit-actions" class="form-actions form-wrapper">
						<input type="submit" class="form-submit" value="Đăng nhập" name="op" id="edit-submit"></div>
					<?php if($_GET['fail'] == 1){ ?>
					<div class="validate validate-fail">Đăng nhập không thành công! Bạn hãy thử đăng nhập lại xem :D</div>
					<?php } ?>
				</div>
			</form>
		</div>
		<div class="block">
			<div class="page-title">
				<h2>Đăng ký</h2>
			</div>
			<form accept-charset="UTF-8" id="user-register" method="post" action="<?=$url->createUrl("user/formSubmitRegister") ?>">
				<div>
					<!--<div class="form-item form-type-textfield form-item-name">
						<label for="edit-name">Họ tên<span title="Trường dữ liệu này là bắt buộc." class="form-required">*</span></label>
						<input type="text" class="input-text required" value="" name="fullname" id="fullname" placeholder="Họ tên">
					</div>-->
					<div class="form-item form-type-textfield form-item-name">
						<label for="edit-name">Tên đăng nhập <span title="Trường dữ liệu này là bắt buộc." class="form-required">*</span></label>
						<input type="text" class="input-text required" value="" name="username" id="username" placeholder="Tên đăng nhập">
					</div>
					<div class="form-item form-type-password form-item-pass">
						<label for="edit-pass">Mật khẩu <span title="Trường dữ liệu này là bắt buộc." class="form-required">*</span></label>
						<input type="text" class="input-text required" name="password" id="password" placeholder="Mật khẩu">
						<!--<label for="edit-pass">Nhập lại mật khẩu
							<span title="Trường dữ liệu này là bắt buộc." class="form-required">*</span></label>
						<input type="text" class="input-text required" name="re_password" id="re_password" placeholder="Nhập lại mật khẩu">-->
					</div>
					<div class="form-item form-type-textfield form-item-email">
						<label for="edit-name">Điện thoại <span title="Trường dữ liệu này là bắt buộc." class="form-required">*</span></label>
						<input type="text" class="input-text required" value="" name="mobile" id="mobile" placeholder="Số điện thoại">
					</div>
					<div class="form-item form-type-textfield form-item-email">
						<label for="edit-name">Email</label>
						<input type="text" class="input-text required" value="" name="email" id="email" placeholder="Email">
					</div>
					<div id="create-actions" class="form-actions form-wrapper">
						<input type="submit" class="form-submit" value="Đăng ký" name="op" id="create-submit"></div>
					<?php if($_GET['fail'] == 2){ ?>
						<div class="validate validate-fail">Bạn chưa điền đủ thông tin rồi ;)</div>
					<?php } ?>
					<?php if($_GET['fail'] == 3){ ?>
						<div class="validate validate-fail">Tên đăng nhập của bạn chọn đã tồn tại mất rồi :) Hãy chọn lại tên khác bạn nhé.</div>
					<?php } ?>
					<?php if($_GET['fail'] == 4){ ?>
						<div class="validate validate-fail">Bạn hãy chọn lại tên đăng nhập của bạn nhé!</div>
					<?php } ?>
				</div>
			</form>
		</div>
	</div>
	</div>
	<!-- END CONTENT -->


<?php //include "login.js.php"; ?>