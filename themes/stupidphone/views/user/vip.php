<?php 
    $url = new Url();    
?> 

<div class="column_left">
    <div class="container left">
        <br>
        <ol style="margin: 0; padding: 0 0 0 30px;">
            <li style="display: inline-block; zoom: 1; *display: inline; list-style: none;" class="info_tabs info_tabs_selected" id="select_account">
                <span class="nohover">Nạp xu</span>
            </li>            
        </ol>
		
        <div id="descriptionText" class="textbody" style="margin-top: 0px;">
		<b><font color="#ff0000">Chú ý:</font></b> Nạp Xu qua thẻ cào sẽ được cộng nhiều Xu hơn
            <!--Để nạp xu vào tài khoản các bạn làm theo một trong 2 cách sau đây:
            <div>
                <b><font color="#000099">Nạp Xu qua tin nhắn (SMS)</font></b>
                <br>Đối với <font color="#ff0000">1SMS</font> bạn sẽ nhận <font color="#ff0000">150 Xu</font>&nbsp;
            </div>
            <div>
                Để nạp xu qua tin nhắn các bạn soạn tin theo cú pháp sau:
                <br><b><font color="#ff0000">UH <?php echo $_SESSION["user_t186"]['username']; ?></font></b> gửi <b><font color="#ff0000">8755</font></b>
            </div>            
            <div><i>Chỉ áp dụng cho 3 mạng <b>Viettel, Mobi</b> và <b>Vina&nbsp;</b></i><br>
                <div>
                    <b><font color="#000099">Nạp Xu qua thẻ cào (Card điện thoại)</font></b></div>
                <div>Đối với hình thức nạp bằng thẻ cào bạn sẽ được <b>số Xu gấp đôi</b> so với hình thức nạp bằng tin nhắn (<i>Khuyên bạn nên sử dụng hình thức này cho rẻ</i>) cụ thể như sau:</div>
                <div>Mệnh giá <font color="#ff0000">10.000 VNĐ = 200 Xu</font></div>
                <div>Mệnh giá <font color="#ff0000">20.000 VNĐ = 500 Xu</font></div>
                <div>Mệnh giá <font color="#ff0000">50.000 VNĐ = 1.500 Xu</font></div>
                <div>Mệnh giá <font color="#ff0000">100.000 VNĐ = 5.000 Xu</font></div>
                <div>Mệnh giá <font color="#ff0000">200.000 VNĐ</font> Bạn sẽ có <font color="#ff0000">Tài Khoản VIP Mãi Mãi</font></div>
                <div>Mệnh giá <font color="#ff0000">500.000 VNĐ</font> Bạn sẽ trở thành <font color="#ff0000">Nhà Tài Trợ</font> của ThichTruyen.VN</div>                
                <div>Sau khi có thẻ cào các bạn lấy <b>Mã thẻ</b> và số <b>Seri</b>&nbsp;trên thẻ cào để hoàn thành Form dưới đây:</div>
                <br>
                <div id="card-vip">                
                    <ul class="form2 bottom20"> 
                        <li class="clearfix" style="margin-bottom: 0px ;">
                            <label>Chọn loại thẻ cào : <span class="clred">(*)</span></label> 
                            <div class="filltext"> 
                                <ul class="list2 clearfix"> 
                                    <li> 
                                        <img src="<?php echo Yii::app()->params['urlRs'];?>/images/viettel.png"><br>
                                        <input checked="" type="radio" onclick="" name="card_type" value="107"> Viettel 
                                    </li> 
                                    <li> 
                                        <img src="<?php echo Yii::app()->params['urlRs'];?>/images/mobi.png"><br>
                                        <input type="radio" onclick="" name="card_type" value="92"> Mobifone 
                                    </li> 
                                    <li> 
                                        <img src="<?php echo Yii::app()->params['urlRs'];?>/images/vina.png"><br>
                                        <input type="radio" onclick="" name="card_type" value="93"> Vinaphone 
                                    </li> 
                                </ul> 
                            </div> 
                        </li>
                        <li class="clearfix">
                            <label>Nhập mã số thẻ : <span class="clred">(*)</span></label> 
                            <div class="filltext"> <input type="text" name="card_code" id="card_code" class="magb5" style="width:195px"><br> <span class="cl999 s12">Nhập đủ mã thẻ cào, viết liền. VD: 123456789012</span> </div> 
                        </li>
                        <li class="clearfix">
                            <label>Seri thẻ cào : <span class="clred">(*)</span></label> 
                            <div class="filltext"> <input type="text" name="card_serial" id="card_serial" class="magb5" style="width:195px"><br> <span class="cl999 s12">Nhập đủ seri thẻ cào</span> </div> 
                        </li>
                        <li id="result_add_money" class="clearfix"> </li> 
                    </ul>
                    <p style="text-align: center;display: none;" id="log_error" class="ajax_error"></p>
                    <p style="text-align: center;display: none;" id="log_success" class="ajax_success"></p>
                    <p style="text-align: center;">                        
                        <a onclick="process_card()" href="javascript:" id="account_save" class="b2 orange">Nạp tiền</a>
                    </p>
                </div>
                <br>
                <div><b><u><font color="#ff0000">Chú ý:</font></u></b> Mỗi truyện yêu cầu Xu bạn sẽ ko phải tiêu quá 10 Xu. Liên hệ với mình nếu bạn có bất cứ thắc mắc gì: 0973 055 398 (Đừng ngại)</div>
                <div><br></div>
                <div>Có thể bạn quan tâm:</div>                
                <div>- <a href="http://thichtruyen.vn/qua-tang-cuoc-song/vai-loi-buc-xuc/" target="_blank">Vì sao phải nạp Xu?</a></div>                
                <div>- <a href="http://truyenchu.info/?p=502" target="_blank">Hướng dẫn nạp Xu</a></div></div>-->
            
            
            <div><b><font color="#000099">&#187; Nạp Xu qua tin nhắn (SMS)</font></b>&nbsp;</div>
            <div><i><font color="#999999"></font></i></div>
            <div>Soạn tin:&nbsp;</div>
            <div><span style="font-weight: bold;color: red;">UH <?php echo $_SESSION["user_t186"]['username']; ?></span> gửi <span style="font-weight: bold;color: red;">8755</span></div>
            <div>Đối với <font color="#ff0000">1SMS</font> bạn sẽ nhận <font color="#ff0000">150 Xu</font>&nbsp;</div>
            <div><i>Chỉ áp dụng cho 3 mạng <b>Viettel</b>, <b>Mobi</b> và <b>Vina&nbsp;</b></i></div>
            <div><b><font color="#000099">Nạp Xu qua thẻ cào (Card điện thoại)</font></b></div>
			
            <div>Các bạn nên sử dụng hình thức này để được cộng nhiều Xu:</div>
            <div>Mệnh giá <font color="#ff0000">10.000 VNĐ</font> = <font color="#ff0000">200 Xu</font></div>
            
            <div>Mệnh giá <font color="#ff0000">20.000 VNĐ</font> = <font color="#ff0000">500 Xu</font></div>
            
            <div>Mệnh giá <font color="#ff0000">50.000 VNĐ</font> = <font color="#ff0000">1.500 Xu</font></div>
            
            <div>Mệnh giá <font color="#ff0000">100.000 VNĐ</font> = <font color="#ff0000">5.000 Xu</font></div>
            
            <div>Mệnh giá <font color="#ff0000">200.000 VNĐ</font> Bạn sẽ có <font color="#ff0000">Tài Khoản VIP Mãi Mãi</font></div>
            
            <div><font color="#999999"></font>Mệnh giá <font color="#ff0000">500.000 VNĐ</font> Bạn sẽ trở thành <font color="#ff0000">Nhà Tài Trợ</font> của ThichTruyen.VN</div>
            
            <div>Sau khi có thẻ cào các bạn lấy <b>Mã thẻ</b> và <b>số Seri</b> trên thẻ cào để hoàn thành Form dưới đây:</div>
            
            <div id="card-vip">                
                    <ul class="form2 bottom20"> 
                        <li class="clearfix" style="margin-bottom: 0px ;">
                            <label>Chọn loại thẻ cào : <span class="clred">(*)</span></label> 
                            <div class="filltext"> 
                                <ul class="list2 clearfix"> 
                                    <li> 
                                        <img src="<?php echo Yii::app()->params['urlRs'];?>/images/viettel.png"><br>
                                        <input checked="" type="radio" onclick="" name="card_type" value="107"> Viettel 
                                    </li> 
                                    <li> 
                                        <img src="<?php echo Yii::app()->params['urlRs'];?>/images/mobi.png"><br>
                                        <input type="radio" onclick="" name="card_type" value="92"> Mobifone 
                                    </li> 
                                    <li> 
                                        <img src="<?php echo Yii::app()->params['urlRs'];?>/images/vina.png"><br>
                                        <input type="radio" onclick="" name="card_type" value="93"> Vinaphone 
                                    </li> 
                                </ul> 
                            </div> 
                        </li>
                        <li class="clearfix">
                            <label>Nhập mã số thẻ : <span class="clred">(*)</span></label> 
                            <div class="filltext"> <input type="text" name="card_code" id="card_code" class="magb5" style="width:195px"><br> <span class="cl999 s12">Nhập đủ mã thẻ cào, viết liền. VD: 123456789012</span> </div> 
                        </li>
                        <li class="clearfix">
                            <label>Seri thẻ cào : <span class="clred">(*)</span></label> 
                            <div class="filltext"> <input type="text" name="card_serial" id="card_serial" class="magb5" style="width:195px"><br> <span class="cl999 s12">Nhập đủ seri thẻ cào</span> </div> 
                        </li>
                        <li id="result_add_money" class="clearfix"> </li> 
                    </ul>
                    <p style="text-align: center;display: none;" id="log_error" class="ajax_error"></p>
                    <p style="text-align: center;display: none;" id="log_success" class="ajax_success"></p>
                    <p style="text-align: center;">                        
                        <a onclick="process_card()" href="javascript:" id="account_save" class="b2 orange">Nạp tiền</a>
                    </p>
                </div>
            
            <br>
                <div><b><u><font color="#ff0000">Chú ý:</font></u></b> Các bạn chỉ mất duy nhất 10 Xu để đọc TRỌN BỘ TRUYỆN DÀI KỲ.</div>
             
				<div><b><font color="#ff0000">Hỗ trợ: 0973 055 398</font></b></div>
                <div><b><font color="#ff0000">Có thể bạn quan tâm:</font></b></div>                
                <div>- <a href="http://thichtruyen.vn/huong-dan/hay-tiet-kiem-mot-bua-an-sang-de-doc-truyen-thoa-thich/" target="_blank">Tiết kiệm một bữa sáng để đọc truyện thỏa thích</a></div>                
                <div>- <a href="http://m.thichtruyen.vn/huong-dan/huong-dan-cach-nap-xu-vao-tai-khoan" target="_blank">Hướng dẫn nạp Xu</a></div></div>
            
        </div>                                                                      
    </div>
</div>

<div class="column_right_bubble">    
    <?php $this->widget("CategoryRight");?>    
    <?php $this->widget("AdvRight");?>
    <?php $this->widget("FooterRight");?>    
 </div>
  
<?php include "vip.js.php"; ?>
