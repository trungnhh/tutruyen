<?php
    define('ROOT_DIR', getcwd());
	if(strstr($_SERVER['HTTP_HOST'],'www.')){
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: http://'.substr($_SERVER['HTTP_HOST'],4).$_SERVER['REQUEST_URI']);    
	}
    error_reporting(0);
    session_start();
    /*$yii=dirname(__FILE__).'/../yii/framework/yii.php';*/
	$yii=dirname(__FILE__).'/yii_ts247/framework/yii.php';        
    $config=dirname(__FILE__).'/protected/config/main.php';
    // remove the following lines when in production mode
    defined('YII_DEBUG') or define('YII_DEBUG',false);
    // specify how many levels of call stack should be shown in each log message
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
    require_once($yii);    
    Yii::createWebApplication($config)->run();

?>