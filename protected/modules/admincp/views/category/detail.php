<?php $url = new Url();?>
<div class="onecolumn" >
    <div class="header"><span><span class="ico gray window"></span>  Chỉnh sửa danh mục  </span></div>
    <div class="clear"></div>
    <div class="content" >

        <form id="validation_demo" action="#"> 
            <input type="hidden" id="category_id" name="category_id" value="<?php echo $data_category['id']; ?>">
            <div class="section ">
                <label> Tên danh mục<small></small></label>   
                <div> 
                    <input type="text" class="validate[required] large" name="category_name" id="category_name" value="<?php echo $data_category['name']; ?>">
                </div>                                
            </div>
            <div class="section ">
                <label> Thứ tự hiển thị<small></small></label>   
                <div> 
                    <input type="text" class="validate[required] small" name="category_order" id="category_order" value="<?php echo $data_category['order_number']; ?>">
                </div>                                
            </div>                            
            <div class="section ">
                <label> Hiển thị trang chủ<small></small></label>   
                <div>                     
                        <input type="checkbox" class="on_off_checkbox" name="category_is_hot" id="category_is_hot"  value="1" <?php if($data_category['is_hot'] == 1){ echo " checked='checked'";}  ?>  />
                </div>
            </div>
            <div class="section last">
                <div>                    
                    <a class="uibutton submit_btn" onclick="update_category()" >Sửa</a>
                </div>
            </div>
        </form> 

    </div>
</div>

<script type="">    

    function update_category(){                
        if($("form#validation_demo").validationEngine('validate')){
            var category_id = $('#category_id').val();            
            var category_name = $('#category_name').val();
            var category_order = $('#category_order').val();
            var category_is_hot = 0;
            if($('#category_is_hot').attr('checked') == "checked"){
                category_is_hot = 1;
            }
            loading('Xử lý',1);                                                
            var strUrl = '<?php echo $url->createUrl("category/ajaxEditCategory"); ?>';         
            $.ajax({
                type: "POST",
                url: strUrl,            
                data: {category_id:category_id,category_name:category_name,category_order:category_order,category_is_hot:category_is_hot},
                beforeSend: function() {

                },
                complete: function() {                    
                    setTimeout( "unloading()", 1000 );                           
                },
                success: function(msg){                    
                    if(msg == -1){                       
                        setTimeout('showError("Đường dẫn đã tồn tại!")',100);                                                    
                    }else{                        
                        window.location.href='<?php echo $url->createUrl("category/index"); ?>';
                    }
                }
            })
        }
    }

</script>