<?php $url = new Url();?>
<div class="onecolumn" >
    <div class="header"><span ><span class="ico  gray bookmark"></span>Quản lý danh mục</span> </div>
    <div class="clear"></div>
    <div class="content" >

        <div id="uploadTab">
            <ul class="tabs" >
                <li><a href="#tab2" id="3">  Danh sách danh mục  <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/new.gif" width="20" height="9" /></a></li>
                <li><a href="#tab1" id="2" style="display: none;">  validation  </a></li>                
            </ul>
            <div class="tab_container" >

                <div id="tab2" class="tab_content"> 
                    <div class="load_page">
                        <ul class="uibutton-group">
                            <li><span class="tip"><a class="uibutton icon add on_load"  name="#tab1"  title="Click để thêm danh mục">Thêm danh mục</a></span></li>                            
                        </ul>
                        <form class="tableName toolbar">

                            <h3>Danh mục truyện</h3>
                            <table class="display data_table2 " id="data_table">
                                <thead>
                                    <tr>                                        
                                        <th width="352" align="left">Tên</th>
                                        <th width="174" >Tên Url</th>
                                        <th width="100" >Trạng thái</th>
                                        <th width="150" >Nổi bật</th>
                                        <th width="199" >Quản lý</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php for($i=0;$i<count($data_category);$i++){ ?>

                                        <tr>                                            
                                            <td  align="left"><?php echo $data_category[$i]['name'];  ?></td>
                                            <td><?php echo $data_category[$i]['alias'];  ?></td>
                                            <td id="status_row_<?php echo $data_category[$i]['id']; ?>">
	                                            <a href="javascript:" onclick="change_status(<?php echo $data_category[$i]['id']; ?>,<?php echo $data_category[$i]['status']; ?>)"><span class="ico shadow checkmark2 <?php if($data_category[$i]['status'] == 1){echo "color";}else{echo "gray";} ?>"></span></a>
                                            </td>
                                            <td id="is_hot_row_<?php echo $data_category[$i]['id']; ?>">
                                                <a href="javascript:" onclick="change_is_hot(<?php echo $data_category[$i]['id']; ?>,<?php echo $data_category[$i]['is_hot']; ?>)"><span class="ico shadow checkmark2 <?php if($data_category[$i]['is_hot'] == 1){echo "color";}else{echo "gray";} ?>"></span></a>                                                
                                            </td>
                                            <td >
                                                <span class="tip" >
                                                    <a title="Sửa" href="<?php echo $url->createUrl("category/detail",array("category_id"=>$data_category[$i]['id'])); ?>" >
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/icon_edit.png" >
                                                    </a>
                                                </span> 
                                                <span class="tip" >
                                                    <a value="<?php echo $data_category[$i]['id']; ?>" class="Delete" name="<?php echo $data_category[$i]['name'];  ?>" title="Xóa"  >
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/icon_delete.png" >
                                                    </a>
                                                </span> 
                                            </td>
                                        </tr>

                                        <?php } ?>                            

                                </tbody>
                            </table>
                        </form>
                    </div>    
                    <div class="show_add" style=" display:none">
                        <ul class="uibutton-group" >
                            <li><span class="tip"><a class="uibutton icon prev on_prev "  id="on_prev_pro" name="#tab2" onClick="jQuery('#validation_demo').validationEngine('hideAll')" title="Quay trở lại">Quay lại danh sách danh mục</a></span></li>
                            <li><span class="tip"><a class="uibutton special" onClick="ResetForm()" title="Điền lại">Điền lại</a></span></li>
                        </ul>
                        <form id="validation_demo" action="#"> 

                            <div class="section ">
                                <label> Tên danh mục<small></small></label>   
                                <div> 
                                    <input type="text" class="validate[required] large" name="category_name" id="category_name">
                                </div>                                
                            </div>
                            <div class="section ">
                                <label> Thứ tự hiển thị<small></small></label>   
                                <div> 
                                    <input type="text" class="validate[required] small" name="category_order" id="category_order">
                                </div>                                
                            </div>
                            <div class="section last">
                                <div>
                                    <!--<a class="uibutton submit_form" >Thêm mới</a>-->
                                    <a class="uibutton submit_btn" onclick="insert_category()" >Thêm mới</a>
                                </div>
                            </div>
                        </form>
                    </div>    
                </div>

                <div id="tab1" class="tab_content"> 
                    
                </div>                

            </div>
        </div>
        <div class="clear"/></div>                  
</div>
</div>

<script>

    $(".Delete").live('click',function() {         
        var category_id = $(this).attr("value");        
        var row=$(this).parents('tr');
        var dataSet=$(this).parents('form');
        var id = $(this).attr("id");
        var name = $(this).attr("name");        
        var data ='id='+id;
        Delete(data,name,row,0,dataSet,category_id);
    });

    function Delete(data,name,row,type,dataSet,category_id){
        var loadpage = dataSet.hdata(0);
        var url = dataSet.hdata(1);
        var table = dataSet.hdata(2);
        var data = data+"&tabel="+table;
        $.confirm({
            'title': 'Xóa dữ liệu','message': " <strong>Bạn có muốn xóa </strong><br /><font color=red>' "+ name +" ' </font> ",'buttons': {'Chấp nhận': {'class': 'special',
                    'action': function(){
                        loading('Checking');
                        delete_category(category_id);
                        $('#preloader').html('Đang xóa');
                        if(type==0){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        if(type==1){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        setTimeout("unloading();",900);          
                }},'Hủy bỏ'    : {'class'    : ''}}});}

    function delete_category(category_id){
        
        var strUrl = '<?php echo $url->createUrl("category/ajaxDeleteCategory"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {category_id:category_id}, 
            beforeSend: function() {

            },
            complete: function() {
                            
            },
            success: function(msg){                
                
            }
        })
    }                

    function insert_category(){                
        if($("form#validation_demo").validationEngine('validate')){
            var category_name = $('#category_name').val();
            var category_order = $('#category_order').val();
            loading('Xử lý',1);                                                
            var strUrl = '<?php echo $url->createUrl("category/ajaxAddCategory"); ?>';         
            $.ajax({
                type: "POST",
                url: strUrl,            
                data: {category_name:category_name,category_order:category_order},
                beforeSend: function() {

                },
                complete: function() {
                    setTimeout( "unloading()", 1000 );                    
                },
                success: function(msg){                        
                    if(msg != 0){                       
                        window.location.reload(); 
                    }
                }
            })
        }
    }

    function change_is_hot(category_id,is_hot){
	    var change_status = 0;
        var is_hot_status = 1;
        var text_color = "color";
        if(is_hot == 1){
            is_hot_status = 0;
            text_color = "gray";
        }
        loading('Xử lý',1);                                                
        var strUrl = '<?php echo $url->createUrl("category/ajaxChangeStatus"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {category_id:category_id,is_hot_status:is_hot_status,change_status:change_status},
            beforeSend: function() {

            },
            complete: function() {
                setTimeout( "unloading()", 1000 );                    
            },
            success: function(msg){                
                if(msg == 1){                       
                    $('#is_hot_row_'+category_id).html('<a href="javascript:" onclick="change_is_hot('+category_id+','+is_hot_status+')"><span class="ico shadow checkmark2 '+text_color+'"></span></a>');
                }
            }
        })
    }

    function change_status(category_id,cur_status){
	    var change_status = 1;
	    var status = 1;
	    var text_color = "color";
	    if(cur_status == 1){
		    status = 0;
		    text_color = "gray";
	    }
	    loading('Xử lý',1);
	    var strUrl = '<?php echo $url->createUrl("category/ajaxChangeStatus"); ?>';
	    $.ajax({
		    type: "POST",
		    url: strUrl,
		    data: {category_id:category_id,status:status,change_status:change_status},
		    beforeSend: function() {

		    },
		    complete: function() {
			    setTimeout( "unloading()", 1000 );
		    },
		    success: function(msg){
			    if(msg == 1){
				    $('#status_row_'+category_id).html('<a href="javascript:" onclick="change_status('+category_id+','+status+')"><span class="ico shadow checkmark2 '+text_color+'"></span></a>');
			    }
		    }
	    })
    }

</script>