<?php 
    $url = new Url();      
    $tong_nap = 0;
    $tong_tieu = 0;
    foreach($data as $row){
        if($row['transaction_type'] == 1){
            $tong_nap = $tong_nap + ($row['funds_after'] - $row['funds_before']);
        }
        if($row['transaction_type'] == 0){
            $tong_tieu = $tong_tieu + ($row['funds_before'] - $row['funds_after']);
        }
    } 
?>

<div class="formEl_b">    
    <form id="validation" action="#" method="get"> 
        <fieldset >
            <legend>Tìm kiếm giao dịch</legend>
            <div class="section ">
                <label> Tên người dùng<small></small></label>   
                <div> 
                    <input type="text" class="validate[required] large" name="keyword" id="keyword" value="<?php echo $_GET['keyword'];?>">
                </div>                
            </div>            
            <div class="section ">
                <label> Loại</label>   
                <div>
                    <select data-placeholder="Chọn loại" class="chzn-select" tabindex="2" id="type_id" onchange="reload_page()" >
                        <option value=""></option> 
                        <option value="1" <?php if($_GET['type_id'] == 1){ echo "selected";} ?> title="1" >Nạp</option> 
                        <option value="2" <?php if($_GET['type_id'] == 2){ echo "selected";} ?> title="2" >Tiêu</option> 
                        <!--<option value="0" <?php if($_GET['type_id'] == 0){ echo "selected";} ?> title="0" >Tất cả</option>-->
                    </select>
                </div>                
            </div>
            <div class="section last">
                <div>
                    <a class="uibutton submit_form" >Tìm kiếm</a>
                    <a class="uibutton special"  onClick="ResetForm()">Huỷ </a>
                </div>
            </div>
        </fieldset>
    </form>
</div>

<div class="onecolumn" >

    <!--<form action="" id="dempo">                
    <div class="section">            
    <div style="font-size: 14px;font-weight: bold;">  Tổng nạp : <?php echo StringUtils::formatNumber($tong_nap); ?> Xu | Tổng tiêu : <?php echo StringUtils::formatNumber($tong_tieu); ?> Xu</div>
    <div style="font-size: 14px;font-weight: bold;">  Tổng thu quy ra vê nờ đờ : <?php echo StringUtils::formatNumber($tong_nap*40); ?> VNĐ</div>
    </div>
    <div class="section">                                
    <div>
    <select data-placeholder="Chọn danh mục" class="chzn-select" tabindex="2" id="type_id" onchange="reload_page()" >
    <option value=""></option> 
    <option value="1" <?php if($_GET['type_id'] == 1){ echo "selected";} ?> title="1" >Nạp</option> 
    <option value="2" <?php if($_GET['type_id'] == 2){ echo "selected";} ?> title="2" >Tiêu</option>                     
    </select>
    </div>
    </div>  
    </form>    -->

    <div class="header"><span ><span class="ico  gray bookmark"></span>Quản lý giao dịch</span> </div>
    <div class="clear"></div>
    <div class="content" >

        <div id="uploadTab">
            <ul class="tabs" >
                <li><a href="#tab2" id="3">  Danh sách giao dịch  <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/new.gif" width="20" height="9" /></a></li>
                <li><a href="#tab1" id="2" style="display: none;">  validation  </a></li>                
            </ul>
            <div class="tab_container" >

                <div id="tab2" class="tab_content"> 
                    <div class="load_page">                        
                        <form class="tableName toolbar">
                            <!--<h3>Danh sách giao dịch</h3>-->
                            <table class="display static " id="static">                            
                                <thead>
                                    <tr>                                        
                                        <th width="35" ><input type="checkbox" id="checkAll1"  class="checkAll"/></th>
                                        <th width="180" >Tài khoản</th>
                                        <th width="100" >Xu thay đổi</th>                                        
                                        <th width="150" >Loại</th>                                        
                                        <th width="">Nội dung</th>
                                        <th width="130" >Thời gian</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php for($i=0;$i<count($data);$i++){ ?>

                                        <tr>               
                                            <td width="35" ><input type="checkbox" name="checkbox[]" class="chkbox"  id="check<?php echo ($i+1); ?>"/></td>                             
                                            <td><?php echo $data[$i]['username'];  ?></td>
                                            <td><?php echo ($data[$i]['funds_after'] - $data[$i]['funds_before']);  ?></td>
                                            <td>
                                                <?php if($data[$i]['transaction_type'] == 1){
                                                        switch ($data[$i]['type']){
                                                            case 1:
                                                                echo "Nạp:SMS";
                                                                break;
                                                            case 2:
                                                                echo "Nạp:Thẻ cào";
                                                                break;
                                                            default:
                                                                break;
                                                        }
                                                }  ?>
                                                <?php if($data[$i]['transaction_type'] == 0){
                                                        switch ($data[$i]['type']){
                                                            case 1:
                                                                echo "Tiêu:Mua truyện";
                                                                break;
                                                            case 2:
                                                                echo "Nạp:Thẻ cào";
                                                                break;
                                                            default:
                                                                break;
                                                        }
                                                }  ?>
                                            </td>
                                            <td><?php echo $data[$i]['content'];  ?></td>                                                                                        
                                            <td><?php echo date('H:i d/m/Y',$data[$i]['transaction_date']);  ?></td>                                                                                        
                                        </tr>

                                        <?php if($i == 3000) break; } ?>

                                </tbody>
                            </table>
                        </form>

                        <div class="clearfix">
                            <ul class="paging fr">
                                <?php
                                    echo $paging;
                                ?>
                            </ul>
                        </div>

                    </div>    

                </div>

                <div id="tab1" class="tab_content"> 

                </div>                

            </div>
        </div>
        <div class="clear"></div>                  
    </div>
</div>

<script>

    function reload_page(){
        var type_id = $("#type_id option:selected").attr("title");        
        window.location.href='<?php echo $url->createUrl("transaction/index"); ?>/type_id/'+type_id;
    }

    $(".Delete").live('click',function() {         
        var admin_id = $(this).attr("value");        
        var row=$(this).parents('tr');
        var dataSet=$(this).parents('form');
        var id = $(this).attr("id");
        var name = $(this).attr("name");        
        var data ='id='+id;
        Delete(data,name,row,0,dataSet,admin_id);
    });

    function Delete(data,name,row,type,dataSet,admin_id){
        var loadpage = dataSet.hdata(0);
        var url = dataSet.hdata(1);
        var table = dataSet.hdata(2);
        var data = data+"&tabel="+table;
        $.confirm({
            'title': 'Xóa dữ liệu','message': " <strong>Bạn có muốn xóa </strong><br /><font color=red>' "+ name +" ' </font> ",'buttons': {'Chấp nhận': {'class': 'special',
                    'action': function(){
                        loading('Checking');
                        delete_admin(admin_id);
                        $('#preloader').html('Đang xóa');
                        if(type==0){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        if(type==1){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        setTimeout("unloading();",900);          
                }},'Hủy bỏ'    : {'class'    : ''}}});}

    function delete_admin(admin_id){

        var strUrl = '<?php echo $url->createUrl("admin/ajaxDelete"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {admin_id:admin_id}, 
            beforeSend: function() {

            },
            complete: function() {

            },
            success: function(msg){                

            }
        })
    }                    

</script>
