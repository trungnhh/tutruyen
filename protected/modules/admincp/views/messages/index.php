<?php 
    $url = new Url(); 
?>    
<div class="onecolumn" >
    <div class="header"><span ><span class="ico  gray bookmark"></span>Quản lý tin nhắn</span> </div>
    <div class="clear"></div>
    <div class="content" >    
        <div id="uploadTab">
            <ul class="tabs" >
                <li><a href="#tab2" id="3">  Danh sách tin nhắn  <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/new.gif" width="20" height="9" /></a></li>
            </ul>
            <div class="tab_container">

                <div id="tab2" class="tab_content"> 
                    <div class="load_page">
                        <ul class="uibutton-group">
                            <li><span class="tip"><a class="uibutton icon add" title="Click để gửi tin nhắn" href="<?php echo $url->createUrl("messages/add"); ?>">Nhắn tin cho toàn bộ user</a></span></li>
                        </ul>
                        <form class="tableName toolbar">

                            <h3>Danh sách tin nhắn</h3>
                            <table class="display data_table2" id="data_table">
                                <thead>
                                    <tr>
                                        <th width="250">Tiêu đề</th>
                                        <th width="" >Nội dung</th>
                                        <th width="120" >Người gửi</th>
                                        <th width="120" >Người nhận</th>
                                        <th width="60" >Trạng thái</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php for($i=0;$i<count($data_messages);$i++){ ?>

                                        <tr>
                                            <td><?php echo $data_messages[$i]['title'];  ?></td>
	                                        <td><?php echo $data_messages[$i]['content'];  ?></td>
	                                        <td><?php echo $data_messages[$i]['sender'];  ?></td>
	                                        <td><?php if($data_messages[$i]['type'] == 0){ echo $data_messages[$i]['receiver']; }else{ echo "Tất cả"; }   ?></td>
	                                        <td><?php if($data_messages[$i]['status'] == 1){ echo "Đã đọc"; }else{ echo "Chưa đọc"; }  ?></td>
                                        </tr>

                                        <?php } ?>                            

                                </tbody>
                            </table>
                        </form>
                    </div>    
                </div>                

            </div> 
        </div>
        <div class="clear"/></div>                   
</div>
</div>

<script>

    $(".Delete").live('click',function() {         
        var author_id = $(this).attr("value");                
        var row=$(this).parents('tr');
        var dataSet=$(this).parents('form');
        var id = $(this).attr("id");
        var name = $(this).attr("name");        
        var data ='id='+id;        
        Delete(data,name,row,0,dataSet,author_id);
    });

    function Delete(data,name,row,type,dataSet,author_id){
        var loadpage = dataSet.hdata(0);
        var url = dataSet.hdata(1);
        var table = dataSet.hdata(2);
        var data = data+"&tabel="+table;
        $.confirm({
            'title': 'Xóa dữ liệu','message': " <strong>Bạn có muốn xóa </strong><br /><font color=red>' "+ name +" ' </font> ",'buttons': {'Chấp nhận': {'class': 'special',
                    'action': function(){
                        loading('Checking');
                        delete_author(author_id);
                        $('#preloader').html('Đang xóa');
                        if(type==0){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        if(type==1){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        setTimeout("unloading();",900);          
                }},'Hủy bỏ'    : {'class'    : ''}}});}

    function delete_author(author_id){

        var strUrl = '<?php echo $url->createUrl("news/ajaxDeleteAuthor"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {author_id:author_id}, 
            beforeSend: function() {

            },
            complete: function() {

            },
            success: function(msg){                

            }
        })
    }    

    function change_is_hot(post_id,is_hot){
        var is_hot_status = 1;
        var text_color = "color";
        if(is_hot == 1){
            is_hot_status = 0;
            text_color = "gray";
        }
        loading('Xử lý',1);                                                
        var strUrl = '<?php echo $url->createUrl("news/ajaxChangeStatus"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {post_id:post_id,is_hot_status:is_hot_status}, 
            beforeSend: function() {

            },
            complete: function() {
                setTimeout( "unloading()", 1000 );                    
            },
            success: function(msg){                
                if(msg == 1){                       
                    $('#is_hot_row_'+post_id).html('<a href="javascript:" onclick="change_is_hot('+post_id+','+is_hot_status+')"><span class="ico shadow checkmark2 '+text_color+'"></span></a>');
                }
            }
        })
    }

</script>