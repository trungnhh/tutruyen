<?php 
    $url = new Url();          
?> 
<div class="onecolumn" >
    <div class="header"><span><span class="ico gray window"></span>  Gửi tin nhắn  </span></div>
    <div class="clear"></div>
    <div class="content" >

        <form id="validation_demo" action="#">
	        <input type="hidden" id="user_id" name="user_id" value="<?php echo $data_user['id']; ?>">
	        <input type="hidden" id="username" name="username" value="<?php echo $data_user['username']; ?>">
	        <input type="hidden" id="type" name="type" value="<?php if($data_user == null){ echo 1; }else{ echo 0;} ?>">
	        <div class="section ">
		        <label> Thành viên
			        <small></small>
		        </label>

		        <div>
			        <?php if($data_user['username'] != ''){ echo $data_user['username']; }else{ echo "Thành viên Tủ Truyện"; }  ?>&nbsp;
		        </div>
	        </div>
            <div class="section ">
                <label> Tiêu đề<small></small></label>   
                <div> 
                    <input type="text" class="validate[required] large" name="message_title" id="message_title" value="">
                </div>                                
            </div>                   
            <div class="section ">
                <label> Nội dung<small></small></label>
	            <div>
		            <textarea name="request_message" id="request_message" class="request_message" cols="59" rows="4"></textarea>
	            </div>
            </div>                                    
            <div class="section last">
                <div>                                    
                    <a class="uibutton submit_btn" onclick="send_message()" >Gửi tin nhắn</a>
                </div>
            </div>
        </form>

    </div>
</div>

<script type="">

	$(document).ready(function() {
		$("#request_message").cleditor({
			width:        "80%", // width not including margins, borders or padding
			height:       200, // height not including margins, borders or padding
			controls:     // controls to add to the toolbar
				"bold italic underline strikethrough subscript superscript | font size " +
					"style | color highlight removeformat | bullets numbering | outdent " +
					"indent | alignleft center alignright justify | undo redo | " +
					"rule image link unlink | cut copy paste pastetext | print source",
			colors:       // colors in the color popup
				"FFF FCC FC9 FF9 FFC 9F9 9FF CFF CCF FCF " +
					"CCC F66 F96 FF6 FF3 6F9 3FF 6FF 99F F9F " +
					"BBB F00 F90 FC6 FF0 3F3 6CC 3CF 66C C6C " +
					"999 C00 F60 FC3 FC0 3C0 0CC 36F 63F C3C " +
					"666 900 C60 C93 990 090 399 33F 60C 939 " +
					"333 600 930 963 660 060 366 009 339 636 " +
					"000 300 630 633 330 030 033 006 309 303",
			fonts:        // font names in the font popup
				"Times New Roman,Arial,Arial Black,Comic Sans MS,Courier New,Narrow,Garamond," +
					"Georgia,Impact,Sans Serif,Serif,Tahoma,Trebuchet MS,Verdana",
			sizes:        // sizes in the font size popup
				"1,2,3,4,5,6,7",
			styles:       // styles in the style popup
				[["Paragraph", "<p>"], ["Header 1", "<h1>"], ["Header 2", "<h2>"],
					["Header 3", "<h3>"],  ["Header 4","<h4>"],  ["Header 5","<h5>"],
					["Header 6","<h6>"]],
			useCSS:       false, // use CSS to style HTML when possible (not supported in ie)
			docType:      // Document type contained within the editor
				'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
			docCSSFile:   // CSS file used to style the document contained within the editor
				"",
			bodyStyle:    // style to assign to document body contained within the editor
				"margin:4px; font:20px; cursor:text;text-align:justify"
		});
	});

    function send_message() {

	    var receiver = $('#username').val();
	    var content = $('#request_message').val();
	    var title = $('#message_title').val();
	    var type = $('#type').val();
	    var sender = 'admin';

	    if(title == ''){
		    setTimeout('showError("Còn cái tiêu đề kìa!")', 100);
		    return false;
	    }

	    if(content == ''){
		    setTimeout('showError("Êu, nội dung k đc để trống!")', 100);
		    return false;
	    }

	    loading('Xử lý', 1);
	    var strUrl = '<?php echo $url->createUrl("messages/ajaxAddMessage"); ?>';
	    $.ajax({
		    type: "POST",
		    url: strUrl,
		    data: {receiver: receiver, content: content, sender: sender, title: title, type: type},
		    beforeSend: function () {

		    },
		    complete: function () {
			    setTimeout("unloading()", 1000);
		    },
		    success: function (msg) {
			    if (msg == -1) {
				    setTimeout('showError("Tên truyện đã tồn tại!")', 100);
			    } else {
				    setTimeout('showSuccess("Đã gửi tin nhắn cho người dùng!")', 100);
				    //window.location.href = '<?php echo $url->createUrl("message/index"); ?>';
			    }
		    }
	    })

    }

</script>