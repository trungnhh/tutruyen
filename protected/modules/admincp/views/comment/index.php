<?php $url = new Url();?>
<div class="onecolumn" >    
    <div class="header"><span ><span class="ico  gray bookmark"></span>Quản lý comment</span> </div>
    <div class="clear"></div>
    <div class="content" >    
        <div id="uploadTab">
            <ul class="tabs" >
                <li><a href="#tab2" id="3">  Danh sách comment  <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/new.gif" width="20" height="9" /></a></li>
                <li><a href="#tab1" id="2" style="display: none;">  validation  </a></li>                
            </ul>
            <div class="tab_container" >

                <div id="tab2" class="tab_content"> 
                    <div class="load_page">                        
                        <form class="tableName toolbar">                    
                        
                            <table class="display static " id="static">
                                <thead>
                                    <tr>
                                        <th width="35" ><input type="checkbox" id="checkAll1"  class="checkAll"/></th>
                                        <th width="" align="left">Nội dung</th>
                                        <th width="150" >Thành viên</th>   
                                        <th width="200" >Truyện</th>
                                        <th width="100" >Thời gian</th>
                                        <th width="80" >Quản lý</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($data_comment);$i++){ ?>
                                        <tr>
                                            <td width="35" ><input type="checkbox" name="checkbox[]" class="chkbox"  id="check<?php echo ($i+1); ?>"/></td>
                                            <td align="left"><?php echo $data_comment[$i]['comment'];  ?></td>
                                            <td><?php echo $data_comment[$i]['username'];  ?></td>
                                            <td><?php echo $data_comment[$i]['post_title'];  ?></td>                                            
                                            <td><?php echo date('d/m/Y',$data_comment[$i]['create_date']);  ?></td>                                            
                                            <td>                                                
                                                <span class="tip" >
                                                    <?php 
                                                        foreach($data_category as $row){
                                                            if($row['id'] == $data_comment[$i]['category_id']){
                                                                $category_alias = $row['alias'];
                                                            }
                                                        }
                                                    ?>
                                                    <a title="Xem" href="http://<?php echo Yii::app()->params['domain'];?><?php echo $category_alias;  ?>/<?php echo $data_comment[$i]['post_alias'];  ?>/" target="_blank" >
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/color_18/eye.png" >
                                                    </a>
                                                </span>&nbsp;
                                                <span class="tip" >
                                                    <a value="<?php echo $data_comment[$i]['id']; ?>" class="Delete" name="<?php echo $data_comment[$i]['comment'];  ?>" title="Xóa"  >
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/icon_delete.png" >
                                                    </a>
                                                </span>
                                            </td> 
                                        </tr>
                                        <?php } ?>

                                </tbody>
                            </table>                            

                        </form>

                        <div class="clearfix">
                            <ul class="paging fr">
                                <?php
                                    echo $paging;
                                ?>
                            </ul>
                        </div>

                    </div>    

                </div>

                <div id="tab1" class="tab_content"> 

                </div>                

            </div>
        </div>
        <div class="clear"/></div>                  
</div>
</div>

<script>

    $(".Delete").live('click',function() {         
        var comment_id = $(this).attr("value");        
        var row=$(this).parents('tr');
        var dataSet=$(this).parents('form');
        var id = $(this).attr("id");
        var name = $(this).attr("name");        
        var data ='id='+id;        
        Delete(data,name,row,0,dataSet,comment_id);
    });

    function Delete(data,name,row,type,dataSet,comment_id){
        var loadpage = dataSet.hdata(0);
        var url = dataSet.hdata(1);
        var table = dataSet.hdata(2);
        var data = data+"&tabel="+table;
        $.confirm({
            'title': 'Xóa dữ liệu','message': " <strong>Bạn có muốn xóa </strong><br /><font color=red>' "+ name +" ' </font> ",'buttons': {'Chấp nhận': {'class': 'special',
                    'action': function(){
                        loading('Checking');
                        delete_comment(comment_id);
                        $('#preloader').html('Đang xóa');
                        if(type==0){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        if(type==1){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        setTimeout("unloading();",900);          
                }},'Hủy bỏ'    : {'class'    : ''}}});}

    function delete_comment(comment_id){

        var strUrl = '<?php echo $url->createUrl("comment/ajaxDeleteComment"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {comment_id:comment_id}, 
            beforeSend: function() {

            },
            complete: function() {

            },
            success: function(msg){                

            }
        })
    }    

</script>