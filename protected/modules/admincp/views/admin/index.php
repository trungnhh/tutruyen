<?php $url = new Url();?>
<div class="onecolumn" >
    <div class="header"><span ><span class="ico  gray bookmark"></span>Quản lý mod</span> </div>
    <div class="clear"></div>
    <div class="content" >

        <div id="uploadTab">
            <ul class="tabs" >
                <li><a href="#tab2" id="3">  Danh sách Mod  <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/new.gif" width="20" height="9" /></a></li>
                <li><a href="#tab1" id="2" style="display: none;">  validation  </a></li>                
            </ul>
            <div class="tab_container" >

                <div id="tab2" class="tab_content"> 
                    <div class="load_page">
                        <ul class="uibutton-group">
                            <li><span class="tip"><a class="uibutton icon add on_load"  name="#tab1"  title="Click để thêm mod">Thêm mod</a></span></li>
                        </ul>
                        <form class="tableName toolbar">

                            <h3>Danh sách mod</h3>
                            <table class="display data_table2 " id="data_table">
                                <thead>
                                    <tr>                                        
                                        <th width="352" align="left">Tên</th>
                                        <th width="174" >Tài khoản</th>
                                        <th width="100" >Email</th>
                                        <th width="150" >Trạng thái</th>
                                        <th width="199" >Quản lý</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php for($i=0;$i<count($data_admin);$i++){ ?>

                                        <tr>                                            
                                            <td  align="left"><?php echo $data_admin[$i]['fullname'];  ?></td>
                                            <td><?php echo $data_admin[$i]['username'];  ?></td>
                                            <td><?php echo $data_admin[$i]['email'];  ?></td>
                                            <td id="is_hot_row_<?php echo $data_admin[$i]['id']; ?>">
                                                <a href="javascript:" onclick="change_status(<?php echo $data_admin[$i]['id']; ?>,<?php echo $data_admin[$i]['status']; ?>)"><span class="ico shadow checkmark2 <?php if($data_admin[$i]['status'] == 1){echo "color";}else{echo "gray";} ?>"></span></a>                                                
                                            </td>
                                            <td >
                                                <span class="tip" >
                                                    <a title="Sửa" href="<?php echo $url->createUrl("admin/detail",array("admin_id"=>$data_admin[$i]['id'])); ?>" >
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/icon_edit.png" >
                                                    </a>
                                                </span> 
                                                <span class="tip" >
                                                    <a href="<?php echo $url->createUrl("admin/report",array("admin_id"=>$data_admin[$i]['id'])); ?>" value="<?php echo $data_admin[$i]['id']; ?>" name="<?php echo $data_admin[$i]['fullname'];  ?>" title="Thống kê">
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/color_18/stats_bars.png" >
                                                    </a>
                                                </span>
                                            </td>
                                        </tr>

                                        <?php } ?>                            

                                </tbody>
                            </table>
                        </form>
                    </div>    
                    <div class="show_add" style=" display:none">
                        <ul class="uibutton-group" >
                            <li><span class="tip"><a class="uibutton icon prev on_prev "  id="on_prev_pro" name="#tab2" onClick="jQuery('#validation_demo').validationEngine('hideAll')" title="Quay trở lại">Quay lại danh sách Mod</a></span></li>
                            <li><span class="tip"><a class="uibutton special" onClick="ResetForm()" title="Điền lại">Điền lại</a></span></li>
                        </ul>
                        <form id="validation_demo" action="#"> 

                            <div class="section ">
                                <label> Tên Mod<small></small></label>
                                <div> 
                                    <input type="text" class="validate[required] large" name="fullname" id="fullname">
                                </div>                                
                            </div>
                            <div class="section ">
                                <label> Tài khoản<small></small></label>   
                                <div> 
                                    <input type="text" class="validate[required] large" name="username" id="username">
                                </div>                                
                            </div>
                            <div class="section ">
                                <label> Mật khẩu<small></small></label>   
                                <div> 
                                    <input type="password" class="validate[required] large" name="password" id="password">
                                </div>                                
                            </div>
                            <div class="section ">
                                <label> Email<small></small></label>   
                                <div> 
                                    <input type="text" class="validate[required] large" name="email" id="email">
                                </div>                                
                            </div>                                                        
                            <div class="section last">
                                <div>                                    
                                    <a class="uibutton submit_btn" onclick="insert_admin()" >Thêm mới</a>
                                </div>
                            </div>
                        </form>
                    </div>    
                </div>

                <div id="tab1" class="tab_content"> 
                    
                </div>                

            </div>
        </div>
        <div class="clear"/></div>                  
</div>
</div>

<script>

    $(".Delete").live('click',function() {         
        var admin_id = $(this).attr("value");        
        var row=$(this).parents('tr');
        var dataSet=$(this).parents('form');
        var id = $(this).attr("id");
        var name = $(this).attr("name");        
        var data ='id='+id;
        Delete(data,name,row,0,dataSet,admin_id);
    });

    function Delete(data,name,row,type,dataSet,admin_id){
        var loadpage = dataSet.hdata(0);
        var url = dataSet.hdata(1);
        var table = dataSet.hdata(2);
        var data = data+"&tabel="+table;
        $.confirm({
            'title': 'Xóa dữ liệu','message': " <strong>Bạn có muốn xóa </strong><br /><font color=red>' "+ name +" ' </font> ",'buttons': {'Chấp nhận': {'class': 'special',
                    'action': function(){
                        loading('Checking');
                        delete_admin(admin_id);
                        $('#preloader').html('Đang xóa');
                        if(type==0){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        if(type==1){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        setTimeout("unloading();",900);          
                }},'Hủy bỏ'    : {'class'    : ''}}});}

    function delete_admin(admin_id){
        
        var strUrl = '<?php echo $url->createUrl("admin/ajaxDelete"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {admin_id:admin_id}, 
            beforeSend: function() {

            },
            complete: function() {
                            
            },
            success: function(msg){                
                
            }
        })
    }                

    function insert_admin(){                
        if($("form#validation_demo").validationEngine('validate')){
            var fullname = $('#fullname').val();
            var username = $('#username').val();            
            var password = $('#password').val();            
            var email = $('#email').val();            
            loading('Xử lý',1);                                                
            var strUrl = '<?php echo $url->createUrl("admin/ajaxAdd"); ?>';
            $.ajax({
                type: "POST",
                url: strUrl,            
                data: {fullname:fullname,username:username,password:password,email:email}, 
                beforeSend: function() {

                },
                complete: function() {
                    setTimeout( "unloading()", 1000 );                    
                },
                success: function(msg){                        
                    if(msg != 0){                       
                        window.location.reload(); 
                    }
                }
            })
        }
    }

    function change_status(admin_id,status_value){
        var status = 1;
        var text_color = "color";
        if(status_value == 1){
            status = 0;
            text_color = "gray";
        }
        loading('Xử lý',1);                                                
        var strUrl = '<?php echo $url->createUrl("admin/ajaxChangeStatus"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {admin_id:admin_id,status:status}, 
            beforeSend: function() {

            },
            complete: function() {
                setTimeout( "unloading()", 1000 );                    
            },
            success: function(msg){                
                if(msg == 1){                       
                    $('#is_hot_row_'+admin_id).html('<a href="javascript:" onclick="change_status('+admin_id+','+status+')"><span class="ico shadow checkmark2 '+text_color+'"></span></a>');
                }
            }
        })
    }

</script>