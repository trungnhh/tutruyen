<?php $url = new Url();?>
<div class="onecolumn" >
    <div class="header"><span><span class="ico gray administrator"></span>Thông tin cá nhân</span></div>
    <div class="clear"></div>
    <div class="content" >
        <div class="boxtitle min">Chụp ảnh bằng Webcam</div>
        <form id="validation_demo" action="#"> 
            <input type="hidden" id="admin_id" name="admin_id" value="<?php echo $data_admin['id']; ?>">
            <div  class="grid1">
                <div class="profileSetting" >
                    <div class="avartar"><img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/avartarB.jpg" width="200" height="200" alt="avatar" /></div>
                    <div class="avartar">
                        <input type="file" class="fileupload" />
                        <p align="center"><span> Hoặc </span>Chụp ảnh bằng <a class="takeWebcam">Webcam</a></p>
                    </div>
                </div>
            </div>
            <div  class="grid3">
                <div class="section webcam">
                    <label> Chụp ảnh<small>bằng  webcam</small></label>   
                    <div> 
                        <div id="screen"></div>
                        <div class="buttonPane">
                            <a id="takeButton" class="uibutton">Chụp</a> <a id="closeButton" class="uibutton special">Đóng</a>
                        </div>
                        <div class="buttonPane hideme">
                            <a id="uploadAvatar" class="uibutton confirm">Upload Avartar</a> <a id="retakeButton" class="uibutton special">Chụp lại</a> 
                        </div>
                    </div>
                </div>

                <div class="section ">
                    <label> Tên </label>   
                    <div> 
                        <input type="text" class="validate[required] large" name="fullname" id="fullname" value="<?php echo $data_admin['fullname']; ?>">
                    </div>
                </div>
                <div class="section">
                    <label> Tài khoản </label>
                    <div>
                        <input type="text"  placeholder="Tài khoản" name="username" id="username"  class="validate[required,minSize[3],maxSize[20],] medium" disabled="disabled" value="<?php echo $data_admin['username']; ?>"  />
                        <span class="f_help"> Tài khoản để đăng nhập. <br />Nên có từ 3 đến 20 ký tự.</span> 
                    </div>
                    <div>
                        <input type="password" placeholder="Mật khẩu" class="validate[required,minSize[3]] medium"  name="password" id="password" value=""  />
                    </div>
                </div>                
                <div class="section ">
                    <label> Email</label>   
                    <div> 
                        <input type="text" class="validate[required,custom[email]]  large" name="email" id="email" value="<?php echo $data_admin['email']; ?>">
                    </div>
                </div>


                <div class="section last">
                    <div>
                        <a class="uibutton" onclick="update_admin()"  >Cập nhật</a>
                    </div>
                </div>

            </div>
        </form>
        <div class="clear"></div>


    </div>
</div> 
<script type="">    

    function update_admin(){                
        if($("form#validation_demo").validationEngine('validate')){
            var admin_id = $('#admin_id').val();            
            var username = $('#username').val();
            var fullname = $('#fullname').val();            
            var password = $('#password').val();            
            var email = $('#email').val();            
            loading('Xử lý',1);                                                
            var strUrl = '<?php echo $url->createUrl("admin/ajaxUpdate"); ?>';
            $.ajax({
                type: "POST",
                url: strUrl,            
                data: {admin_id:admin_id,username:username,fullname:fullname,password:password,email:email}, 
                beforeSend: function() {

                },
                complete: function() {                    
                    setTimeout( "unloading()", 1000 );                           
                },
                success: function(msg){                    
                    if(msg == -1){                       
                        setTimeout('showError("Đường dẫn đã tồn tại!")',100);                                                    
                    }else{                        
                        window.location.href='<?php echo $url->createUrl("admin/profile"); ?>';
                    }
                }
            })
        }
    }

</script>
