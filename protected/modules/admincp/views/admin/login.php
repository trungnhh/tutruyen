<?php $url = new Url();?>
<div id="alertMessage" class="error"></div>
<div id="successLogin"></div>
<div class="text_success"><img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/loadder/loader_green.gif"  alt="SauTruyenAdmin" /><span>Chờ một tẹo thôi</span></div>

<div id="login" >
    <div class="ribbon"></div>
    <div class="inner">
        <div class="logo">
            <img style="display: none" src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/logo/logo_login.png" alt="SauTruyenAdmin" />
            <h1 id="main-logo"><?php echo Helpers::getBrandDomain();?>&trade; - Quản trị Website</h1>
        </div>
        <div class="userbox"></div>
        <div class="formLogin">            
            <div class="tip">
                <input name="username" type="text" id="username_id" title="Tài khoản" />                    
            </div>
            <div class="tip">
                <input name="password" type="password" id="password" title="Mật khẩu" />                    
            </div>                

            <div style="padding:20px 0px 0px 0px ;">
                <div style="float:left; padding:0px 0px 2px 0px ;">
                    <input type="checkbox" id="on_off" name="remember" class="on_off_checkbox"  value="1"   />
                    <span class="f_help">Lưu đăng nhập</span>
                </div>
                <div style="float:right;padding:2px 0px ;">
                    <div> 
                        <ul class="uibutton-group">
                            <li><a class="uibutton normal" href="javascript:"  id="but_login" >Đăng nhập</a></li>
                            <li><a class="uibutton normal" href="javascript:" id="forgetpass">Quên mật khẩu</a></li>
                        </ul>
                    </div>
                </div>
            </div>            
        </div>
    </div>
    <div class="clear"></div>
    <div class="shadow"></div>
</div>

<script type="">

    $('#but_login').click(function(e){ 
        login_process();
    });    

    $('#password').bind('keypress', function(e) {
        if(e.keyCode==13){
            login_process();
        }
    });

    function login_process(){
        var username = $('#username_id').val();
        var pass = $('#password').val();        
        var remember = $('.iPhoneCheckHandle').css("left");
        if(remember == '40px'){
            remember = 1;   
        }else{
            remember = 0;
        }
        if(username == "" || pass == ""){
            showError("Hãy nhập Tài khoản / Mật khẩu");
            $('.inner').jrumble({ x: 4,y: 0,rotation: 0 });    
            $('.inner').trigger('startRumble');
            setTimeout('$(".inner").trigger("stopRumble")',500);
            setTimeout('hideTop()',5000);
            return false;
        }        
        hideTop();
        loading('Kiểm tra',1);                                        

        var strUrl = '<?php echo $url->createUrl("admin/ajaxSubmitLogin"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {username:username,pass:pass,remember:remember}, 
            beforeSend: function() {

            },
            complete: function() {
                setTimeout( "unloading()", 400 );    
            },
            success: function(msg){                
                if(msg == 1){                       
                    setTimeout( "Login()", 2500 );
                }else{                    
                    //showError("Tài khoản / Mật khẩu sai rồi!");
                    setTimeout('showError("Tài khoản / Mật khẩu sai rồi!")',700);
                    $('.inner').jrumble({ x: 4,y: 0,rotation: 0 });    
                    $('.inner').trigger('startRumble');
                    setTimeout('$(".inner").trigger("stopRumble")',800);
                    setTimeout('hideTop()',5000);                     
                }
            }
        })            

    }

    function Login(){    

        $("#login").animate({   opacity: 1,top: '49%' }, 200,function(){
            $('.userbox').show().animate({ opacity: 1 }, 500);
            $("#login").animate({   opacity: 0,top: '60%' }, 500,function(){
                $(this).fadeOut(200,function(){
                    $(".text_success").slideDown();
                    $("#successLogin").animate({opacity: 1,height: "200px"},500);                    
                });                              
            })    
        })    
        setTimeout( "window.location.href='<?php echo $url->createUrl("site/index"); ?>'", 3000 );
    }

</script>