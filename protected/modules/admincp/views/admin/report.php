<?php
	$url = new Url();
?>

<div class="onecolumn" >
	<div class="header"><span ><span class="ico  gray bookmark"></span>Quản lý truyện được post bởi <?php echo $data_admin['fullname']; ?></span> </div>
	<div class="clear"></div>
	<div class="content" >
		<div id="uploadTab">
			<ul class="tabs" >
				<li><a href="#tab2" id="3">  Danh sách truyện được post bởi <?php echo $data_admin['fullname']; ?> <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/new.gif" width="20" height="9" /></a></li>
				<li><a href="#tab1" id="2" style="display: none;">  validation  </a></li>
			</ul>
			<div class="tab_container" >

				<div id="tab2" class="tab_content">
					<div class="load_page">
						<form class="tableName toolbar">

							<h3>Danh sách truyện được post bởi <?php echo $data_admin['fullname']; ?></h3>
							<table class="display data_table2" id="data_table">
								<thead>
								<tr>
									<th width="" align="left">Tiêu đề</th>
									<th width="170" >Tên Url</th>
									<th width="170" >Số view</th>
									<th width="140" >Nổi bật</th>
									<th width="140" >Quản lý</th>
								</tr>
								</thead>
								<tbody>

								<?php for($i=0;$i<count($data_post);$i++){ ?>

									<tr>
										<td  align="left"><?php echo $data_post[$i]['post_title'];  ?></td>
										<td><?php echo $data_post[$i]['post_alias'];  ?></td>
										<td><?php echo $data_post[$i]['post_views'];  ?></td>
										<td id="is_hot_row_<?php echo $data_post[$i]['id']; ?>">
											<a href="javascript:" onclick="change_is_hot(<?php echo $data_post[$i]['id']; ?>,<?php echo $data_post[$i]['post_is_hot']; ?>)"><span class="ico shadow checkmark2 <?php if($data_post[$i]['post_is_hot'] == 1){echo "color";}else{echo "gray";} ?>"></span></a>
										</td>
										<td >
                                                <span class="tip" >
                                                    <a title="Xem" href="<?=Helpers::getBaseUrl();?><?php echo $data_post[$i]['category_alias'];  ?>/<?php echo $data_post[$i]['post_alias'];  ?>/" target="_blank" >
	                                                    <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/color_18/eye.png" >
                                                    </a>
                                                </span>&nbsp;
                                                <span class="tip" >
                                                    <a title="Sửa" href="<?php echo $url->createUrl("post/detail",array("post_id"=>$data_post[$i]['id'])); ?>" >
	                                                    <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/icon_edit.png" >
                                                    </a>
                                                </span>
										</td>
									</tr>

								<?php } ?>

								</tbody>
							</table>
						</form>
					</div>
					<div class="show_add" style=" display:none">
						<ul class="uibutton-group" >
							<li><span class="tip"><a class="uibutton icon prev on_prev "  id="on_prev_pro" name="#tab2" onClick="jQuery('#validation_demo').validationEngine('hideAll')" title="Quay trở lại">Quay lại danh sách truyện</a></span></li>
							<li><span class="tip"><a class="uibutton special" onClick="ResetForm()" title="Điền lại">Điền lại</a></span></li>
						</ul>
						<form id="validation_demo" action="#">

							<div class="section ">
								<label> Tên truyện<small></small></label>
								<div>
									<input type="text" class="validate[required] large" name="post_title" id="post_title">
								</div>
							</div>
							<!--<div class="section ">
								<label> Tên URL<small></small></label>
								<div>
									<input type="text" class="validate[required] large" name="post_alias" id="post_alias">
								</div>
							</div>-->
							<div class="section ">
								<label> Nội dung truyện<small></small></label>
								<div>
									<div > <textarea name="post_content" id="post_content"  class="editor"  cols="" rows=""></textarea></div>
								</div>
							</div>
							<div class="section ">
								<label> Danh mục<small></small></label>
								<div>
									<select data-placeholder="Chọn danh mục" class="chzn-select" tabindex="2" id="category_id_add">
										<option value=""></option>
										<?php foreach($data_category as $row){ ?>
											<option value="<?php echo $row['name']; ?>" <?php if($category_id == $row['id']){ echo "selected";} ?> title="<?php echo $row['id']; ?>" ><?php echo $row['name']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<!--<div class="section ">
								<label> Truyện VIP<small></small></label>
								<div>
									<input type="checkbox" class="on_off_checkbox" name="category_is_hot" id="post_is_vip"  value="1" />
								</div>
							</div>-->
							<div class="section last">
								<div>
									<a class="uibutton submit_btn" onclick="insert_post()" >Thêm mới</a>
								</div>
							</div>
						</form>
					</div>
				</div>

				<div id="tab1" class="tab_content">

				</div>

			</div>
		</div>
		<div class="clear"/></div>
</div>
</div>

<div class="onecolumn" >
	<div class="header"><span ><span class="ico gray stats_lines"></span>Thống kê mod : <?php echo $data_admin['fullname']; ?></span></div>
	<div class="clear"></div>
	<div class="content" >
		<br  class="clear"/>
		<div class="grid4">
			<div  style="width:98%;height:415px; margin-left:25px">
				<table class="chart_mod" style="width : 100%;">
					<thead>
					<tr>
						<th width="10%"></th>
						<th width="25%" style="color : #bedd17;">Số lượng bài post</th>
					</tr>
					</thead>
					<tbody>
					<?php $temp =0 ; foreach($data_chart_post as $row){ $temp++; ?>
						<tr>
							<th><?php echo $temp; ?></th>
							<td><?php echo $row['count']; ?></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script>

	var d = new Date();
	$("table.chart_mod").each(function() {
		var colors = [];
		$("table.chart thead th:not(:first)").each(function() {
			colors.push($(this).css("color"));
		});
		$(this).graphTable({
			series: 'columns',
			position: 'replace',
			width: '100%',
			height: '423px',
			colors: colors
		}, {
			xaxis: {
				tickSize: 1,
				ticks: [[1,getDateForChart(13)],[2,getDateForChart(12)],[3,getDateForChart(11)],[4,getDateForChart(10)],[5,getDateForChart(9)],[6,getDateForChart(8)],[7,getDateForChart(7)],[8,getDateForChart(6)],[9,getDateForChart(5)],[10,getDateForChart(4)],[11,getDateForChart(3)],[12,getDateForChart(2)],[13,getDateForChart(1)],[14, d.getDate() + '/' + d.getMonth()]]
			},
			yaxis: {
				max: 300,
				min: 0,
			},	series: {
				points: {show: true },
				lines: { show: true, fill: true, steps: false },
			}
		});
	});

	function getDateForChart(day_minus){
		var today = new Date();
		today.setDate(today.getDate() - day_minus);
		return today.getDate() + '/' + today.getMonth();
	}

</script>