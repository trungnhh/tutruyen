<?php $url = new Url();?>
<div class="onecolumn" >
    <div class="header"><span><span class="ico gray window"></span>  Chỉnh sửa admin  </span></div>
    <div class="clear"></div>
    <div class="content" >

        <form id="validation_demo" action="#"> 
            <input type="hidden" id="admin_id" name="admin_id" value="<?php echo $data_admin['id']; ?>">
            <div class="section ">
                <label> Tên admin<small></small></label>   
                <div> 
                    <input type="text" class="validate[required] large" name="fullname" id="fullname" value="<?php echo $data_admin['fullname']; ?>">
                </div>                                
            </div>
            <div class="section ">
                <label> Tài khoản<small></small></label>   
                <div> 
                    <input type="text" class="validate[required] large" name="username" id="username" value="<?php echo $data_admin['username']; ?>">
                </div>                                
            </div>
            <div class="section ">
                <label> Mật khẩu<small></small></label>   
                <div> 
                    <input type="password" class="validate[required] large" name="password" id="password" value="">
                </div>                                
            </div>
            <div class="section ">
                <label> Email<small></small></label>   
                <div> 
                    <input type="text" class="validate[required] large" name="email" id="email" value="<?php echo $data_admin['email']; ?>">
                </div>                                
            </div>                                                        
            <div class="section last">
                <div>                                    
                    <a class="uibutton submit_btn" onclick="update_admin()" >Sửa</a>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="">    

    function update_admin(){                
        if($("form#validation_demo").validationEngine('validate')){
            var admin_id = $('#admin_id').val();            
            var username = $('#username').val();
            var fullname = $('#fullname').val();            
            var password = $('#password').val();            
            var email = $('#email').val();            
            loading('Xử lý',1);                                                
            var strUrl = '<?php echo $url->createUrl("admin/ajaxUpdate"); ?>';
            $.ajax({
                type: "POST",
                url: strUrl,            
                data: {admin_id:admin_id,username:username,fullname:fullname,password:password,email:email}, 
                beforeSend: function() {

                },
                complete: function() {                    
                    setTimeout( "unloading()", 1000 );                           
                },
                success: function(msg){                    
                    if(msg == -1){                       
                        setTimeout('showError("Đường dẫn đã tồn tại!")',100);                                                    
                    }else{                        
                        window.location.href='<?php echo $url->createUrl("admin/index"); ?>';
                    }
                }
            })
        }
    }

</script>
