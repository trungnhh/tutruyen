<?php 
    $url = new Url(); 
?>    
<div class="onecolumn" >
    <div class="header"><span ><span class="ico  gray bookmark"></span>Quản lý truyện yêu cầu</span> </div>
    <div class="clear"></div>
    <div class="content" >    
        <div id="uploadTab">
            <ul class="tabs" >
                <li><a href="#tab2" id="3">  Danh sách truyện yêu cầu  <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/new.gif" width="20" height="9" /></a></li>
            </ul>
            <div class="tab_container">

                <div id="tab2" class="tab_content"> 
                    <div class="load_page">
                        <form class="tableName toolbar">

                            <h3>Danh sách truyện yêu cầu</h3>
                            <table class="display data_table2" id="data_table">
                                <thead>
                                    <tr>
                                        <th width="120">Tên</th>
                                        <th width="" >Nội dung</th>
                                        <th width="120" >Trạng thái</th>
                                        <th width="100">Thành viên</th>
	                                    <th width="80" ></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php for($i=0;$i<count($data_request);$i++){ ?>

                                        <tr>
                                            <td><?php echo $data_request[$i]['request_name'];  ?></td>
                                            <td><?php $content = $data_request[$i]['request_content'];$content = StringUtils::cutstring($content,200,true);$content = nl2br($content);  echo $content;  ?></td>
	                                        <td id="status_row_<?php echo $data_request[$i]['id']; ?>">
		                                        <a href="javascript:" onclick="change_status(<?php echo $data_request[$i]['id']; ?>,<?php echo $data_request[$i]['status']; ?>)"><span class="ico shadow checkmark2 <?php if($data_request[$i]['status'] == 1){echo "color";}else{echo "gray";} ?>"></span></a>
	                                        </td>
	                                        <td><?php echo $data_request[$i]['username'];  ?></td>
	                                        <td>
		                                        <span class="tip" >
                                                    <a title="Chi tiết" href="<?php echo $url->createUrl("request/detail",array("id"=>$data_request[$i]['id'])); ?>">
	                                                    <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/color_18/eye.png" >
                                                    </a>
                                                </span>&nbsp;
	                                        </td>
                                        </tr>

                                        <?php } ?>                            

                                </tbody>
                            </table>
                        </form>
                    </div>    
                </div>                

            </div> 
        </div>
        <div class="clear"/></div>                   
</div>
</div>

<script>

    $(".Delete").live('click',function() {         
        var author_id = $(this).attr("value");                
        var row=$(this).parents('tr');
        var dataSet=$(this).parents('form');
        var id = $(this).attr("id");
        var name = $(this).attr("name");        
        var data ='id='+id;        
        Delete(data,name,row,0,dataSet,author_id);
    });

    function Delete(data,name,row,type,dataSet,author_id){
        var loadpage = dataSet.hdata(0);
        var url = dataSet.hdata(1);
        var table = dataSet.hdata(2);
        var data = data+"&tabel="+table;
        $.confirm({
            'title': 'Xóa dữ liệu','message': " <strong>Bạn có muốn xóa </strong><br /><font color=red>' "+ name +" ' </font> ",'buttons': {'Chấp nhận': {'class': 'special',
                    'action': function(){
                        loading('Checking');
                        delete_author(author_id);
                        $('#preloader').html('Đang xóa');
                        if(type==0){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        if(type==1){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        setTimeout("unloading();",900);          
                }},'Hủy bỏ'    : {'class'    : ''}}});}

    function delete_author(author_id){

        var strUrl = '<?php echo $url->createUrl("news/ajaxDeleteAuthor"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {author_id:author_id}, 
            beforeSend: function() {

            },
            complete: function() {

            },
            success: function(msg){                

            }
        })
    }

    function change_status(request_id,cur_status){
	    var change_status = 1;
	    var status = 1;
	    var text_color = "color";
	    if(cur_status == 1){
		    status = 0;
		    text_color = "gray";
	    }
	    loading('Xử lý',1);
	    var strUrl = '<?php echo $url->createUrl("request/ajaxChangeStatus"); ?>';
	    $.ajax({
		    type: "POST",
		    url: strUrl,
		    data: {request_id:request_id,status:status,change_status:change_status},
		    beforeSend: function() {

		    },
		    complete: function() {
			    setTimeout( "unloading()", 1000 );
		    },
		    success: function(msg){
			    if(msg == 1){
				    $('#status_row_'+request_id).html('<a href="javascript:" onclick="change_status('+request_id+','+status+')"><span class="ico shadow checkmark2 '+text_color+'"></span></a>');
			    }
		    }
	    })
    }

</script>