<?php
	$url = new Url();
?>

<div class="onecolumn">
	<div class="header"><span><span class="ico gray window"></span>  Chi tiết Yêu cầu  </span></div>
	<div class="clear"></div>
	<div class="content">

		<form id="validation_demo" action="#">
			<input type="hidden" id="request_id" name="request_id" value="<?php echo $data_request['id']; ?>">
			<input type="hidden" id="user_id" name="user_id" value="<?php echo $data_request['user_id']; ?>">
			<input type="hidden" id="username" name="username" value="<?php echo $data_request['username']; ?>">

			<div class="section ">
				<label> Tiêu đề
					<small></small>
				</label>

				<div>
					<?php echo $data_request['request_name']; ?>&nbsp;
				</div>
			</div>
			<div class="section ">
				<label> Nội dung
					<small></small>
				</label>

				<div>
					<?php echo $data_request['request_content']; ?>&nbsp;
				</div>
			</div>
			<div class="section ">
				<label> Tác giả
					<small></small>
				</label>

				<div>
					<?php echo $data_request['request_author']; ?>&nbsp;
				</div>
			</div>
			<div class="section ">
				<label> Danh mục
					<small></small>
				</label>

				<div>
					<?php
						foreach($data_category as $row) {
							if($row['id'] == $data_request['category_id']) {
								echo $row['name'];
							}
						}
					?>
				</div>
			</div>
			<div class="section ">
				<label> Thành viên yêu cầu
					<small></small>
				</label>

				<div>
					<?php echo $data_request['username']; ?>&nbsp;
				</div>
			</div>
			<div class="section ">
				<label> Ngày yêu cầu
					<small></small>
				</label>

				<div>
					<?php echo date('H:i d/m/Y', $data_request['create_date']); ?>&nbsp;
				</div>
			</div>
			<div class="section ">
				<label> Tiêu đề tin nhắn
					<small></small>
				</label>
				<div>
					<input type="text" class="large" value="" id="message_title" name="message_title">
				</div>
			</div>
			<div class="section ">
				<label> Tin nhắn
					<small></small>
				</label>

				<div>
					<textarea name="request_message" id="request_message" class="request_message" cols="59" rows="4"></textarea>
				</div>
			</div>
			<div class="section last">
				<div>
					<a class="uibutton submit_btn" onclick="send_message()">Gửi tin nhắn cho người dùng</a>
				</div>
			</div>
		</form>

	</div>
</div>

<script type="">

	function send_message() {

		var receiver = $('#username').val();
		var content = $('#request_message').val();
		var title = $('#message_title').val();
		var sender = 'admin';

		if(title == ''){
			setTimeout('showError("Còn cái tiêu đề kìa!")', 100);
			return false;
		}

		if(content == ''){
			setTimeout('showError("Êu, nội dung k đc để trống!")', 100);
			return false;
		}

		loading('Xử lý', 1);
		var strUrl = '<?php echo $url->createUrl("messages/ajaxAddMessage"); ?>';
		$.ajax({
			type: "POST",
			url: strUrl,
			data: {receiver: receiver, content: content, sender: sender, title: title},
			beforeSend: function () {

			},
			complete: function () {
				setTimeout("unloading()", 1000);
			},
			success: function (msg) {
				if (msg == -1) {
					setTimeout('showError("Tên truyện đã tồn tại!")', 100);
				} else {
					setTimeout('showSuccess("Đã gửi tin nhắn cho người dùng!")', 100);
					//window.location.href = '<?php echo $url->createUrl("message/index"); ?>';
				}
			}
		})

	}

</script>