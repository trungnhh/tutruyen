<?php 
    $url = new Url();    
?>
<div class="onecolumn" >
    <div class="header"><span><span class="ico gray window"></span>  Chỉnh sửa truyện  </span></div>
    <div class="clear"></div>
    <div class="content" >

        <form id="validation_demo" action="#"> 
            <input type="hidden" id="user_id" name="user_id" value="<?php echo $data_user['id']; ?>">
            <div class="section ">
                <label> ID<small></small></label>   
                <div> 
                    <input type="text" class="small" value="<?php echo $data_user['id']; ?>" disabled="disabled">
                </div>                                
            </div>

            <div class="section ">
                <label> Tên tài khoản<small></small></label>   
                <div> 
                    <input type="text" class="large" disabled="disabled" value='<?php echo $data_user['username']; ?>'>
                </div>                                
            </div>                    
            <div class="section ">
                <label> Tên người dùng<small></small></label>   
                <div> 
                    <input type="text" class="small" name="fullname" id="fullname" value="<?php echo $data_user['fullname']; ?>">
                </div>                                
            </div>
            <div class="section ">
                <label> Điện thoại<small></small></label>   
                <div> 
                    <input type="text" class="small" name="mobile" id="mobile" value="<?php echo $data_user['mobile']; ?>">
                </div>                                
            </div>
            <div class="section ">
                <label> Email<small></small></label>   
                <div> 
                    <input type="text" class="small" name="email" id="email" value="<?php echo $data_user['email']; ?>">
                </div>                                
            </div>
            <div class="section ">
                <label> Số xu<small></small></label>
                <div> 
                    <input type="text" class="small" name="funds" id="funds" value="<?php echo $data_user['funds']; ?>">
                </div>                                
            </div>

            <div class="section last">
                <div>                                    
                    <a class="uibutton submit_btn" onclick="update_user()" >Cập nhật</a>
                    <a class="uibutton submit_btn" onclick="reset_password()" >Reset mật khẩu</a>
                </div>
            </div>
        </form>

    </div>
</div>

<script type="">

    $(document).ready(function() {
        //$("#post_content").cleditor()[0].focus();        

        $('#post_description').keyup(function(){  
            //get the limit from maxlength attribute  
            var limit = 160;  
            //get the current text inside the textarea  
            var text = $(this).val();  
            //count the number of characters in the text  
            var chars = text.length;  

            //check if there are more characters then allowed  
            if(chars > limit){  
                //and if there are use substr to get the text before the limit  
                var new_text = text.substr(0, limit);  

                //and change the current text with the new text  
                $(this).val(new_text);  
            }  
        });

        $(document).ready(function() {
            $("#post_content").cleditor({
                width:        "100%", // width not including margins, borders or padding
                height:       600, // height not including margins, borders or padding
                controls:     // controls to add to the toolbar
                "bold italic underline strikethrough subscript superscript | font size " +
                "style | color highlight removeformat | bullets numbering | outdent " +
                "indent | alignleft center alignright justify | undo redo | " +
                "rule image link unlink | cut copy paste pastetext | print source",
                colors:       // colors in the color popup
                "FFF FCC FC9 FF9 FFC 9F9 9FF CFF CCF FCF " +
                "CCC F66 F96 FF6 FF3 6F9 3FF 6FF 99F F9F " +
                "BBB F00 F90 FC6 FF0 3F3 6CC 3CF 66C C6C " +
                "999 C00 F60 FC3 FC0 3C0 0CC 36F 63F C3C " +
                "666 900 C60 C93 990 090 399 33F 60C 939 " +
                "333 600 930 963 660 060 366 009 339 636 " +
                "000 300 630 633 330 030 033 006 309 303",    
                fonts:        // font names in the font popup
                "Times New Roman,Arial,Arial Black,Comic Sans MS,Courier New,Narrow,Garamond," +
                "Georgia,Impact,Sans Serif,Serif,Tahoma,Trebuchet MS,Verdana",
                sizes:        // sizes in the font size popup
                "1,2,3,4,5,6,7",
                styles:       // styles in the style popup
                [["Paragraph", "<p>"], ["Header 1", "<h1>"], ["Header 2", "<h2>"],
                ["Header 3", "<h3>"],  ["Header 4","<h4>"],  ["Header 5","<h5>"],
                ["Header 6","<h6>"]],
                useCSS:       false, // use CSS to style HTML when possible (not supported in ie)
                docType:      // Document type contained within the editor
                '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
                docCSSFile:   // CSS file used to style the document contained within the editor
                "", 
                bodyStyle:    // style to assign to document body contained within the editor
                "margin:4px; font:20px,Arial,Verdana; cursor:text"
            });
        });      

    }); 

    function update_user(){                
        if($("form#validation_demo").validationEngine('validate')){
            var user_id = $('#user_id').val();
            var fullname = $('#fullname').val();            
            var mobile = $('#mobile').val();               
            var email = $('#email').val();                           
            var funds = $('#funds').val();             

            loading('Xử lý',1);                                                
            var strUrl = '<?php echo $url->createUrl("user/ajaxEditUser"); ?>';         
            $.ajax({
                type: "POST",
                url: strUrl,            
                data: {user_id:user_id,fullname:fullname,mobile:mobile,email:email,funds:funds},
                beforeSend: function() {

                },
                complete: function() {                    
                    setTimeout( "unloading()", 1000 );                           
                },
                success: function(msg){                    
                    if(msg == -1){                       
                        setTimeout('showError("Đường dẫn đã tồn tại!")',100);                                                    
                    }else{                                           
                        //window.location.href='<?php echo $url->createUrl("post/index/category_id/"); ?>'+'/'+category_id;
                        //location.reload(true); 
                    }
                }
            })
        }
    }

    function reset_password(){
        var user_id = $('#user_id').val();
        //loading('Xử lý',1);                                                
        var strUrl = '<?php echo $url->createUrl("user/ajaxResetPasswordUser"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {user_id:user_id},
            beforeSend: function() {

            },
            complete: function() {                    
                //setTimeout( "unloading()", 1000 );                           
            },
            success: function(msg){                    
                if(msg == -1){                       
                    setTimeout('showError("Đường dẫn đã tồn tại!")',100);                                                    
                }else{                                           
                    alert('Mật khẩu của user đã được reset về mật khẩu mặc định : 123456');
                }
            }
        })
    }

</script>