<?php 
    $url = new Url();  
?>

<div class="formEl_b">    
    <form id="validation" method="get"> 
        <fieldset >            
            <div class="section ">
                <label> Tài khoản thành viên<small></small></label>   
                <div> 
                    <input type="hidden" value="1" id="page" name="page">
                    <input type="text" class="large" name="keyword" id="keyword" value="<?php echo $_GET['keyword'];?>">
                </div>                
            </div>            
            <div class="section last">
                <div>
                    <a class="uibutton submit_form" >Tìm kiếm</a>                    
                </div>
            </div>
        </fieldset>
    </form>
</div>

<div class="clear"></div>        

<div class="onecolumn" >
    <div class="header"><span ><span class="ico  gray bookmark"></span>Quản lý thành viên</span> </div>
    <div class="clear"></div>
    <div class="content" >

        <div id="uploadTab">
            <ul class="tabs" >
                <li><a href="#tab2" id="3">  Danh sách thành viên  <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/new.gif" width="20" height="9" /></a></li>                
            </ul>
            <div class="tab_container" >
            
                <div id="tab2" class="tab_content"> 
                    <div class="load_page">                        
                        <form class="tableName toolbar">                                                

                            <table class="display static " id="static">
                                <thead>
                                    <tr>                                        
                                        <th align="left" width="150">Tên</th>
                                        <th width="">Tài khoản</th>   
                                        <th width="140" >Thời gian</th>
                                        <th width="50" >Xu</th>
                                        <th width="80" >Trạng thái</th>
                                        <th width="80" >Quản lý</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($data_user);$i++){ ?>
                                        <tr>                                            
                                            <td align="left"><?php echo $data_user[$i]['fullname'];  ?></td>
                                            <td><?php echo $data_user[$i]['username'];  ?></td>
                                            <td><?php echo date("H:i d/m/Y",$data_user[$i]['create_date']);  ?></td>
                                            <td><?php echo $data_user[$i]['funds'];  ?></td>
                                            <td id="is_hot_row_<?php echo $data_user[$i]['id']; ?>">                                            
                                                <a href="javascript:" onclick="change_status(<?php echo $data_user[$i]['id']; ?>,<?php echo $data_user[$i]['status']; ?>)"><span class="ico shadow checkmark2 <?php if($data_user[$i]['status'] == 1){echo "color";}else{echo "gray";} ?>"></span></a>                                                
                                            </td>
                                            <td>
                                                <span class="tip" >
                                                    <a title="Sửa" href="<?php echo $url->createUrl("user/detail",array("user_id"=>$data_user[$i]['id'])); ?>" >
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/icon_edit.png" >
                                                    </a>
                                                </span>&nbsp;
                                                <span class="tip" >
                                                    <a title="Gửi tin nhắn" href="<?php echo $url->createUrl("messages/add",array("user_id"=>$data_user[$i]['id'])); ?>" >
	                                                    <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/color_18/mail.png" >
                                                    </a>
                                                </span>
                                            </td> 
                                        </tr>
                                        <?php } ?>

                                </tbody>
                            </table>                            

                        </form>

                        <div class="clearfix">
                            <ul class="paging fr">
                                <?php
                                    echo $paging;
                                ?>
                            </ul>
                        </div>

                    </div>    

                </div>

            </div>
        </div>
        <div class="clear"></div>                  
    </div>
</div>

<script>

    $(".Delete").live('click',function() {         
        var admin_id = $(this).attr("value");        
        var row=$(this).parents('tr');
        var dataSet=$(this).parents('form');
        var id = $(this).attr("id");
        var name = $(this).attr("name");        
        var data ='id='+id;
        Delete(data,name,row,0,dataSet,admin_id);
    });

    function Delete(data,name,row,type,dataSet,admin_id){
        var loadpage = dataSet.hdata(0);
        var url = dataSet.hdata(1);
        var table = dataSet.hdata(2);
        var data = data+"&tabel="+table;
        $.confirm({
            'title': 'Xóa dữ liệu','message': " <strong>Bạn có muốn xóa </strong><br /><font color=red>' "+ name +" ' </font> ",'buttons': {'Chấp nhận': {'class': 'special',
                    'action': function(){
                        loading('Checking');
                        delete_admin(admin_id);
                        $('#preloader').html('Đang xóa');
                        if(type==0){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        if(type==1){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        setTimeout("unloading();",900);          
                }},'Hủy bỏ'    : {'class'    : ''}}});}

    function delete_admin(admin_id){

        var strUrl = '<?php echo $url->createUrl("admin/ajaxDelete"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {admin_id:admin_id}, 
            beforeSend: function() {

            },
            complete: function() {

            },
            success: function(msg){                

            }
        })
    }                

    function insert_admin(){                
        if($("form#validation_demo").validationEngine('validate')){
            var fullname = $('#fullname').val();
            var username = $('#username').val();            
            var password = $('#password').val();            
            var email = $('#email').val();            
            loading('Xử lý',1);                                                
            var strUrl = '<?php echo $url->createUrl("admin/ajaxAdd"); ?>';
            $.ajax({
                type: "POST",
                url: strUrl,            
                data: {fullname:fullname,username:username,password:password,email:email}, 
                beforeSend: function() {

                },
                complete: function() {
                    setTimeout( "unloading()", 1000 );                    
                },
                success: function(msg){                        
                    if(msg != 0){                       
                        window.location.reload(); 
                    }
                }
            })
        }
    }

    function change_status(user_id,status_value){
        var status = 1;
        var text_color = "color";
        if(status_value == 1){
            status = 0;
            text_color = "gray";
        }
        loading('Xử lý',1);                                                
        var strUrl = '<?php echo $url->createUrl("user/ajaxChangeStatus"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {user_id:user_id,status:status}, 
            beforeSend: function() {

            },
            complete: function() {
                setTimeout( "unloading()", 1000 );                    
            },
            success: function(msg){                
                if(msg == 1){                       
                    $('#is_hot_row_'+user_id).html('<a href="javascript:" onclick="change_status('+user_id+','+status+')"><span class="ico shadow checkmark2 '+text_color+'"></span></a>');
                }
            }
        })
    }

</script>
