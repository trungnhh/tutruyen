<?php $url = new Url(); ?>
<?php
$categories = explode(',', $category_id);
?>
<div class="onecolumn">
	<div class="header"><span><span class="ico  gray connect"></span>Thêm mới Chap </span></div>
	<div class="clear"></div>
	<div class="content">
		<div class="boxtitle">
			<span class="ico gray audio_knob"></span> Là 1 phần của truyện
			<span class="netip"><a href="<?php echo $url->createUrl("post/detail", array("post_id" => $data_parent_post['id'])); ?>" class="red" title="<?php echo $data_parent_post['post_title']; ?>"> <?php echo $data_parent_post['post_title']; ?>  </a><img src="<?php echo Yii::app()->params['urlRsAdmin']; ?>/images/icon/link.png" alt="link"/></span>
		</div>
		<div id="uploadTab">
			<ul class="tabs">
				<li><a href="#tab1" id="2"> Thêm mới </a></li>
				<li><a href="#tab2" id="3"> Ghép truyện đã có </a></li>
			</ul>
			<div class="tab_container">

				<div id="tab1" class="tab_content">
					<div class="load_page">
						<form id="validation_demo" action="#">
							<input type="hidden" id="parent_post_id" name="parent_post_id" value="<?php echo $parent_post_id; ?>">
							<input type="hidden" id="author_id" name="author_id" value="<?php echo $data_parent_post['author_id']; ?>">

							<div class="section ">
								<label> Tên truyện
									<small></small>
								</label>

								<div>
									<input type="text" class="validate[required] large" name="post_title" id="post_title" value="">
								</div>
							</div>
							<div class="section ">
								<label> Nội dung truyện
									<small></small>
								</label>

								<div>
									<div>&nbsp;</div>
								</div>
								<div style="margin-left : 0px ;">
									<textarea name="post_content" id="post_content" class="post_content" cols="" rows=""></textarea>
								</div>
							</div>
							<div class="section ">
								<label> Danh mục
									<small></small>
								</label>

								<div>
									<select data-placeholder="Chọn danh mục" class="chzn-select" tabindex="2" id="category_id_add" disabled="disabled" multiple>
										<option value=""></option>
										<?php foreach($data_category as $row) { ?>
											<option value="<?php echo $row['name']; ?>" title="<?php echo $row['id']; ?>" <?php if(in_array($row['id'], $categories)) {
												echo "selected='selected'";
											} ?> ><?php echo $row['name']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="section ">
								<label> Số lượt xem
									<small></small>
								</label>

								<div>
									<input type="text" class="small" name="post_views" id="post_views" value="1">
								</div>
							</div>
							<div class="section ">
								<label> Số thứ tự
									<small></small>
								</label>

								<div>
									<input type="text" class="small" name="order" id="order" value="">
								</div>
							</div>
							<div class="section ">
								<label> Giá
									<small></small>
								</label>

								<div>
									<input type="text" class="small" name="post_cost" id="post_cost" value="">
								</div>
							</div>
							<div class="section last">
								<div>
									<a class="uibutton submit_btn" onclick="add_post_chap()">Đăng truyện</a>
								</div>
							</div>
						</form>
					</div>
				</div>


				<div id="tab2" class="tab_content">
					<div class="load_page">
						<form id="validation_demo_2" action="#">
							<div class="section ">
								<label> ID truyện
									<small></small>
								</label>

								<div>
									<input type="text" class="validate[required] large" name="post_id" id="post_id" value="">
								</div>
							</div>

							<div class="section ">
								<label> Số thứ tự
									<small></small>
								</label>

								<div>
									<input type="text" class="small" name="order_2" id="order_2" value="">
								</div>
							</div>

							<div class="section ">
								<label> Giữ truyện gốc
									<small></small>
								</label>

								<div>
									<div>
										<input type="checkbox" name="keep_post" id="keep_post" value="1" class="ck" />
									</div>
								</div>
							</div>

							<div class="section last">
								<div>
									<a class="uibutton submit_btn" onclick="add_post_link_chap()">Đăng truyện</a>
								</div>
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>
		<div class="clear"/>
	</div>
</div>

</div>

<script type="">

	$(document).ready(function () {
		//$("#post_content").cleditor()[0].focus();

		$(document).ready(function () {
			$("#post_content").cleditor({
				width: "100%", // width not including margins, borders or padding
				height: 600, // height not including margins, borders or padding
				controls:     // controls to add to the toolbar
					"bold italic underline strikethrough subscript superscript | font size " +
						"style | color highlight removeformat | bullets numbering | outdent " +
						"indent | alignleft center alignright justify | undo redo | " +
						"rule image link unlink | cut copy paste pastetext | print source",
				colors:       // colors in the color popup
					"FFF FCC FC9 FF9 FFC 9F9 9FF CFF CCF FCF " +
						"CCC F66 F96 FF6 FF3 6F9 3FF 6FF 99F F9F " +
						"BBB F00 F90 FC6 FF0 3F3 6CC 3CF 66C C6C " +
						"999 C00 F60 FC3 FC0 3C0 0CC 36F 63F C3C " +
						"666 900 C60 C93 990 090 399 33F 60C 939 " +
						"333 600 930 963 660 060 366 009 339 636 " +
						"000 300 630 633 330 030 033 006 309 303",
				fonts:        // font names in the font popup
					"Times New Roman,Arial,Arial Black,Comic Sans MS,Courier New,Narrow,Garamond," +
						"Georgia,Impact,Sans Serif,Serif,Tahoma,Trebuchet MS,Verdana",
				sizes:        // sizes in the font size popup
					"1,2,3,4,5,6,7",
				styles:       // styles in the style popup
					[
						["Paragraph", "<p>"],
						["Header 1", "<h1>"],
						["Header 2", "<h2>"],
						["Header 3", "<h3>"],
						["Header 4", "<h4>"],
						["Header 5", "<h5>"],
						["Header 6", "<h6>"]
					],
				useCSS: false, // use CSS to style HTML when possible (not supported in ie)
				docType:      // Document type contained within the editor
					'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
				docCSSFile:   // CSS file used to style the document contained within the editor
					"",
				bodyStyle:    // style to assign to document body contained within the editor
					"margin:4px; font:20px; cursor:text;text-align:justify"
			});
		});

	});

	function add_post_chap() {
		if ($("form#validation_demo").validationEngine('validate')) {
			var parent_post_id = $('#parent_post_id').val();
			var order = $('#order').val();
			var post_title = $('#post_title').val();
			var post_content = $('#post_content').val();
			var category_id = $("#category_id_add option:selected").attr("title");
			var post_views = $('#post_views').val();
			var post_cost = $('#post_cost').val();
			var author_id = $('#author_id').val();

			loading('Xử lý', 1);
			var strUrl = '<?php echo $url->createUrl("post/ajaxAddPostChap"); ?>';
			$.ajax({
				type: "POST",
				url: strUrl,
				data: {post_title: post_title, post_content: post_content, category_id: category_id, post_views: post_views, post_cost: post_cost, parent_post_id: parent_post_id, author_id: author_id, order: order},
				beforeSend: function () {

				},
				complete: function () {
					setTimeout("unloading()", 1000);
				},
				success: function (msg) {
					if (msg == -1) {
						setTimeout('showError("Tên truyện đã tồn tại!")', 100);
					} else {
						location.reload(true);
					}
				}
			})
		}
	}

	function add_post_link_chap() {
		if ($("form#validation_demo_2").validationEngine('validate')) {
			var parent_post_id = $('#parent_post_id').val();
			var order = $('#order_2').val();
			var post_id = $('#post_id').val();
			var category_id = $("#category_id_add option:selected").attr("title");
			var keep_post = $('.checker:first').hasClass('checked');
			if(keep_post == true){
				keep_post = 1;
			}else{
				keep_post = 0;
			}
			loading('Xử lý', 1);
			var strUrl = '<?php echo $url->createUrl("post/ajaxAddPostChapLink"); ?>';
			$.ajax({
				type: "POST",
				url: strUrl,
				data: {parent_post_id: parent_post_id, post_id: post_id, order: order,keep_post:keep_post},
				beforeSend: function () {

				},
				complete: function () {
					setTimeout("unloading()", 1000);
				},
				success: function (msg) {
					if (msg == -1) {
						setTimeout('showError("Đường dẫn đã tồn tại!")', 100);
					} else {
						location.reload(true);
					}
				}
			})
		}
	}

</script>
