<?php 
    $url = new Url();
    foreach($data_category as $row){
        if($category_id == $row['id']){ 
            $category_alias = $row['alias'];
        }
    }
?>
<div class="onecolumn" >
    <form action="">
        <div class="section">                                
            <div>
                <select data-placeholder="Chọn danh mục" class="chzn-select" tabindex="2" id="category_id" onchange="reload_page()" multiple>
                    <option value=""></option> 
                    <?php foreach($data_category as $row){ ?>
                        <option value="<?php echo $row['name']; ?>" <?php if($category_id == $row['id']){ echo "selected";} ?> title="<?php echo $row['id']; ?>" ><?php echo $row['name']; ?></option> 
                        <?php } ?>
                        <option value="chua-hoan-thanh-1" <?php if($category_id == 100){ echo "selected";} ?> title="100" >Truyện chưa hoàn thành - Phần 1</option> 
                        <option value="chua-hoan-thanh-2" <?php if($category_id == 101){ echo "selected";} ?> title="101" >Truyện chưa hoàn thành - Phần 2</option> 
                        <option value="chua-hoan-thanh-3" <?php if($category_id == 102){ echo "selected";} ?> title="102" >Truyện chưa hoàn thành - Phần 3</option> 
                        <option value="chua-hoan-thanh-4" <?php if($category_id == 103){ echo "selected";} ?> title="103" >Truyện chưa hoàn thành - Phần 4</option> 
                        <option value="chua-hoan-thanh-5" <?php if($category_id == 104){ echo "selected";} ?> title="104" >Truyện chưa hoàn thành - Phần 5</option> 
                        <option value="chua-hoan-thanh-6" <?php if($category_id == 105){ echo "selected";} ?> title="105" >Truyện chưa hoàn thành - Phần 6</option> 
                        <option value="truyen-ngan-1" <?php if($category_id == 106){ echo "selected";} ?> title="106" >Truyện ngắn - Phần 1</option> 
                        <option value="truyen-ngan-2" <?php if($category_id == 107){ echo "selected";} ?> title="107" >Truyện ngắn - Phần 2</option> 
                        <option value="truyen-ngan-2" <?php if($category_id == 108){ echo "selected";} ?> title="108" >Truyện ngắn - Phần 3</option> 
                </select>
            </div>
        </div>  
    </form>
    <div class="header"><span ><span class="ico  gray bookmark"></span>Quản lý truyện</span> </div>
    <div class="clear"></div>
    <div class="content" >    
        <div id="uploadTab">
            <ul class="tabs" >
                <li><a href="#tab2" id="3">  Danh sách truyện  <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/new.gif" width="20" height="9" /></a></li>
                <li><a href="#tab1" id="2" style="display: none;">  validation  </a></li>                
            </ul>
            <div class="tab_container" >

                <div id="tab2" class="tab_content"> 
                    <div class="load_page">
                        <ul class="uibutton-group">
                            <li><span class="tip"><a class="uibutton icon add" title="Click để thêm truyện" href="<?php echo $url->createUrl("post/add"); ?>">Thêm truyện</a></span></li>
                            <!--<li><span class="tip"><a class="uibutton icon add on_load"  name="#tab1"  title="Click để thêm truyện">Thêm truyện</a></span></li>-->
                            <!--<li><span class="tip"><a class="uibutton special DeleteAll" title="Xóa truyện">Xóa</a></span></li>-->
                        </ul>
                        <form class="tableName toolbar">

                            <h3>Danh sách truyện</h3>                            
                            <table class="display data_table2" id="data_table">
                                <thead>
                                    <tr>
                                        <!--<th width="35" ><input type="checkbox" id="checkAll1"  class="checkAll"/></th>-->
                                        <th width="" align="left">Tiêu đề</th>                                        
                                        <th width="170" >Tên Url</th>
                                        <th width="140" >Số view</th>
                                        <th width="120" >Nổi bật</th>
                                        <th width="160" >Quản lý</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php for($i=0;$i<count($data_post);$i++){ ?>

                                        <tr>
                                            <!--<td  width="35" ><input type="checkbox" name="checkbox[]" class="chkbox"  id="check<?php echo $i; ?>"/></td>-->
                                            <td  align="left"><?php echo $data_post[$i]['post_title'];  ?></td>                                            
                                            <td><?php echo $data_post[$i]['post_alias'];  ?></td>
                                            <td><?php echo $data_post[$i]['post_views'];  ?></td>
                                            <td id="is_hot_row_<?php echo $data_post[$i]['id']; ?>">
                                                <a href="javascript:" onclick="change_is_hot(<?php echo $data_post[$i]['id']; ?>,<?php echo $data_post[$i]['post_is_hot']; ?>)"><span class="ico shadow checkmark2 <?php if($data_post[$i]['post_is_hot'] == 1){echo "color";}else{echo "gray";} ?>"></span></a>
                                            </td>                                            
                                            <td >
                                                <span class="tip" >
                                                    <a title="Xem" href="http://<?php echo Yii::app()->params['domain'];?><?php echo $category_alias;  ?>/<?php echo $data_post[$i]['post_alias'];  ?>/" target="_blank" >
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/color_18/eye.png" >
                                                    </a>
                                                </span>&nbsp;
                                                <span class="tip" >
                                                    <a title="Sửa" href="<?php echo $url->createUrl("post/detail",array("post_id"=>$data_post[$i]['id'])); ?>" >
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/icon_edit.png" >
                                                    </a>
                                                </span>&nbsp;
                                                <span class="tip" >
                                                    <a title="Thêm chap" href="<?php echo $url->createUrl("post/addchap",array("parent_post_id"=>$data_post[$i]['id'],"category_id"=>$category_id)); ?>">
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/color_18/copy.png" >
                                                    </a>
                                                </span>&nbsp;
                                                <span class="tip" >
                                                    <a value="<?php echo $data_post[$i]['id']; ?>" class="Delete" name="<?php echo $data_post[$i]['post_title'];  ?>" title="Xóa"  >
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/icon_delete.png" >
                                                    </a>
                                                </span>
                                            </td> 
                                        </tr>

                                        <?php } ?>                            

                                </tbody>
                            </table>
                        </form>
                    </div>    
                    <div class="show_add" style=" display:none">
                        <ul class="uibutton-group" >
                            <li><span class="tip"><a class="uibutton icon prev on_prev "  id="on_prev_pro" name="#tab2" onClick="jQuery('#validation_demo').validationEngine('hideAll')" title="Quay trở lại">Quay lại danh sách truyện</a></span></li>
                            <li><span class="tip"><a class="uibutton special" onClick="ResetForm()" title="Điền lại">Điền lại</a></span></li>
                        </ul>
                        <form id="validation_demo" action="#"> 

                            <div class="section ">
                                <label> Tên truyện<small></small></label>   
                                <div> 
                                    <input type="text" class="validate[required] large" name="post_title" id="post_title">
                                </div>                                
                            </div>
                            <!--<div class="section ">
                                <label> Tên URL<small></small></label>   
                                <div> 
                                    <input type="text" class="validate[required] large" name="post_alias" id="post_alias">
                                </div>                                
                            </div>-->
                            <div class="section ">
                                <label> Nội dung truyện<small></small></label>   
                                <div>                                     
                                    <div > <textarea name="post_content" id="post_content"  class="editor"  cols="" rows=""></textarea></div>      
                                </div>                                
                            </div>
                            <div class="section ">
                                <label> Danh mục<small></small></label>   
                                <div>                                     
                                    <select data-placeholder="Chọn danh mục" class="chzn-select" tabindex="2" id="category_id_add">
                                        <option value=""></option> 
                                        <?php foreach($data_category as $row){ ?>
                                            <option value="<?php echo $row['name']; ?>" <?php if($category_id == $row['id']){ echo "selected";} ?> title="<?php echo $row['id']; ?>" ><?php echo $row['name']; ?></option> 
                                            <?php } ?>
                                    </select>
                                </div>                                
                            </div>
                            <!--<div class="section ">
                                <label> Truyện VIP<small></small></label>   
                                <div>                                     
                                    <input type="checkbox" class="on_off_checkbox" name="category_is_hot" id="post_is_vip"  value="1" />
                                </div>
                            </div>-->
                            <div class="section last">
                                <div>                                    
                                    <a class="uibutton submit_btn" onclick="insert_post()" >Thêm mới</a>
                                </div>
                            </div>
                        </form>
                    </div>    
                </div>

                <div id="tab1" class="tab_content"> 

                </div>                

            </div>
        </div>
        <div class="clear"/></div>                  
</div>
</div>

<script>    

    $(document).ready(function() {
        //$("#post_content").cleditor()[0].focus();
        
        $(document).ready(function() {
            $("#post_content").cleditor({
                width:        500, // width not including margins, borders or padding
                height:       250, // height not including margins, borders or padding
                controls:     // controls to add to the toolbar
                "bold italic underline strikethrough subscript superscript | font size " +
                "style | color highlight removeformat | bullets numbering | outdent " +
                "indent | alignleft center alignright justify | undo redo | " +
                "rule image link unlink | cut copy paste pastetext | print source",
                colors:       // colors in the color popup
                "FFF FCC FC9 FF9 FFC 9F9 9FF CFF CCF FCF " +
                "CCC F66 F96 FF6 FF3 6F9 3FF 6FF 99F F9F " +
                "BBB F00 F90 FC6 FF0 3F3 6CC 3CF 66C C6C " +
                "999 C00 F60 FC3 FC0 3C0 0CC 36F 63F C3C " +
                "666 900 C60 C93 990 090 399 33F 60C 939 " +
                "333 600 930 963 660 060 366 009 339 636 " +
                "000 300 630 633 330 030 033 006 309 303",    
                fonts:        // font names in the font popup
                "Arial,Arial Black,Comic Sans MS,Courier New,Narrow,Garamond," +
                "Georgia,Impact,Sans Serif,Serif,Tahoma,Trebuchet MS,Verdana",
                sizes:        // sizes in the font size popup
                "1,2,3,4,5,6,7",
                styles:       // styles in the style popup
                [["Paragraph", "<p>"], ["Header 1", "<h1>"], ["Header 2", "<h2>"],
                ["Header 3", "<h3>"],  ["Header 4","<h4>"],  ["Header 5","<h5>"],
                ["Header 6","<h6>"]],
                useCSS:       false, // use CSS to style HTML when possible (not supported in ie)
                docType:      // Document type contained within the editor
                '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
                docCSSFile:   // CSS file used to style the document contained within the editor
                "", 
                bodyStyle:    // style to assign to document body contained within the editor
                "margin:4px; font:10pt Arial,Verdana; cursor:text"
            });
        });
        
    });

    function reload_page(){
        var category_id = $("#category_id option:selected").attr("title");
        window.location.href='<?php echo $url->createUrl("post/chap"); ?>/category_id/'+category_id;
    }

    $(".Delete").live('click',function() {         
        var post_id = $(this).attr("value");        
        var row=$(this).parents('tr');
        var dataSet=$(this).parents('form');
        var id = $(this).attr("id");
        var name = $(this).attr("name");        
        var data ='id='+id;        
        Delete(data,name,row,0,dataSet,post_id);
    });

    function Delete(data,name,row,type,dataSet,post_id){
        var loadpage = dataSet.hdata(0);
        var url = dataSet.hdata(1);
        var table = dataSet.hdata(2);
        var data = data+"&tabel="+table;
        $.confirm({
            'title': 'Xóa dữ liệu','message': " <strong>Bạn có muốn xóa </strong><br /><font color=red>' "+ name +" ' </font> ",'buttons': {'Chấp nhận': {'class': 'special',
                    'action': function(){
                        loading('Checking');
                        delete_post(post_id);
                        $('#preloader').html('Đang xóa');
                        if(type==0){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        if(type==1){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        setTimeout("unloading();",900);          
                }},'Hủy bỏ'    : {'class'    : ''}}});}

    function delete_post(post_id){

        var strUrl = '<?php echo $url->createUrl("post/ajaxDeletePost"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {post_id:post_id}, 
            beforeSend: function() {

            },
            complete: function() {

            },
            success: function(msg){                

            }
        })
    }

    function insert_post(){                
        if($("form#validation_demo").validationEngine('validate')){
            var post_title = $('#post_title').val();
            /*var post_alias = $('#post_alias').val();*/
            var post_content = $('#post_content').val();            
            var category_id = $("#category_id_add option:selected").attr("title");
            /*var category_is_hot = 0;
            if($('#category_is_hot').attr('checked') == "checked"){
                category_is_hot = 1;
            }*/
            loading('Xử lý',1);                                                
            var strUrl = '<?php echo $url->createUrl("post/ajaxAddPost"); ?>';         
            $.ajax({
                type: "POST",
                url: strUrl,            
                data: {post_title:post_title,post_content:post_content,category_id:category_id},
                beforeSend: function() {

                },
                complete: function() {                    
                    setTimeout( "unloading()", 1000 );                           
                },
                success: function(msg){    
                    alert(msg);return false;                  
                    if(msg == -1){                       
                        setTimeout('showError("Đường dẫn đã tồn tại!")',100);                                                    
                    }else{                        
                        window.location.reload();                                                 
                    }
                }
            })
        }
    }

    function change_is_hot(post_id,is_hot){
        var is_hot_status = 1;
        var text_color = "color";
        if(is_hot == 1){
            is_hot_status = 0;
            text_color = "gray";
        }
        loading('Xử lý',1);                                                
        var strUrl = '<?php echo $url->createUrl("post/ajaxChangeStatus"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {post_id:post_id,is_hot_status:is_hot_status}, 
            beforeSend: function() {

            },
            complete: function() {
                setTimeout( "unloading()", 1000 );                    
            },
            success: function(msg){                
                if(msg == 1){                       
                    $('#is_hot_row_'+post_id).html('<a href="javascript:" onclick="change_is_hot('+post_id+','+is_hot_status+')"><span class="ico shadow checkmark2 '+text_color+'"></span></a>');
                }
            }
        })
    }

</script>