<?php 
    $url = new Url();
?>

<div class="formEl_b">    
    <form id="validation" action="#" method="get"> 
        <fieldset >
            <legend>Tìm kiếm truyện</legend>
            <div class="section ">
                <label> Tên truyện<small></small></label>   
                <div> 
                    <input type="text" class="large" name="keyword" id="keyword" value="<?php echo $_GET['keyword'];?>">
                </div>                
            </div>            
            <div class="section ">
                <label> Danh mục truyện</label>   
                <div>
                    <select data-placeholder="Chọn danh mục" class="chzn-select" tabindex="2" id="category_id" name="category_id">
                    <option value=""></option> 
                    <?php foreach($data_category as $row){ ?>
                        <option value="<?php echo $row['id']; ?>" <?php if($_GET['category_id'] == $row['id']){ echo "selected";} ?> title="<?php echo $row['id']; ?>" ><?php echo $row['name']; ?></option>
                        <?php } ?>                    
                </select>
                </div>
	            <label> Tác giả</label>
	            <div>
		            <select data-placeholder="Chọn tác giả" class="chzn-select" tabindex="3" id="author_id" name="author_id">
			            <option value=""></option>
			            <?php foreach($data_author as $row){ ?>
				            <option value="<?php echo $row['author_id']; ?>" <?php if($_GET['author_id'] == $row['author_id']){ echo "selected";} ?> title="<?php echo $row['author_id']; ?>" ><?php echo $row['author_name']; ?></option>
			            <?php } ?>
		            </select>
	            </div>
	            <label> Người post</label>
	            <div>
		            <select data-placeholder="Chọn người post" class="chzn-select" tabindex="3" id="create_user" name="create_user">
			            <option value=""></option>
			            <?php foreach($data_mod as $row){ ?>
				            <option value="<?php echo $row['id']; ?>" <?php if($_GET['create_user'] == $row['id']){ echo "selected";} ?> title="<?php echo $row['fullname']; ?>" ><?php echo $row['fullname']; ?></option>
			            <?php } ?>
		            </select>
	            </div>
                <label> Truyện Vip</label>   
                <div>
                    <select class="small" name="is_vip" id="is_vip">
                        <option value="0" <?php if($_GET['is_vip'] == 0){ echo "selected";} ?>>Tất cả</option>
                        <option value="1" <?php if($_GET['is_vip'] == 1){ echo "selected";} ?>>Có</option>
                        <option value="2" <?php if($_GET['is_vip'] == 2){ echo "selected";} ?>>Không</option>                        
                    </select>       
                </div>                
                <label> Truyện Chưa hoàn thành</label>   
                <div>
                    <select class="small" name="is_full" id="is_full">
                        <option value="0" <?php if($_GET['is_full'] == 0){ echo "selected";} ?>>Tất cả</option>
                        <option value="1" <?php if($_GET['is_full'] == 1){ echo "selected";} ?>>Có</option>
                        <option value="2" <?php if($_GET['is_full'] == 2){ echo "selected";} ?>>Không</option>                        
                    </select>       
                </div>
	            <label> Truyện gốc</label>
	            <div>
		            <select class="small" name="post_parent" id="post_parent">
			            <option value="1" <?php if($_GET['post_parent'] == 1){ echo "selected";} ?>>Có</option>
			            <option value="3" <?php if($_GET['post_parent'] == 3){ echo "selected";} ?>>Tất cả</option>
			            <option value="2" <?php if($_GET['post_parent'] == 2){ echo "selected";} ?>>Không</option>
		            </select>
	            </div>
	            <label>Ngày post</label>
	            <div>
		            <input type="text" name="post_date" class="datepicker" id="post_date" size="10">
	            </div>
	            <label> Sắp xếp</label>
	            <div>
		            <select class="large" name="post_sort" id="post_sort">
			            <option value="1" <?php if($_GET['post_sort'] == 1){ echo "selected";} ?>>Thời gian</option>>
			            <option value="2" <?php if($_GET['post_sort'] == 2){ echo "selected";} ?>>Lượt view</option>
		            </select>
	            </div>
            </div>
            <div class="section last">
                <div>
                    <a class="uibutton submit_form" onclick="submit_form()" >Tìm kiếm</a>
                    <a class="uibutton special"  onClick="ResetForm()">Huỷ </a>
                </div>
            </div>
        </fieldset>
    </form>
</div>

<div class="clear"></div>                      

<?php if($data_post != null){ ?>

<div class="onecolumn" >
    <div class="header"><span ><span class="ico  gray bookmark"></span>Danh sách truyện</span> </div>
    <div class="clear"></div>
    <div class="content" >    
        <div id="uploadTab">
            <ul class="tabs" >
                <li><a href="#tab2" id="3">  Danh sách truyện  <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/new.gif" width="20" height="9" /></a></li>
                <li><a href="#tab1" id="2" style="display: none;">  validation  </a></li>                
            </ul>
            <div class="tab_container" >

                <div id="tab2" class="tab_content"> 
                    <div class="load_page">
                        <ul class="uibutton-group">
                            <li><span class="tip"><a class="uibutton icon add" title="Click để thêm truyện" href="<?php echo $url->createUrl("post/add"); ?>">Thêm truyện</a></span></li>                            
                        </ul>
                        <form class="tableName toolbar">                                                

                            <table class="display data_table2 " id="data_table">
                                <thead>
                                    <tr>                                        
                                        <th width="" align="left">Tiêu đề</th>
                                        <th width="170" >Tên Url</th>   
                                        <th width="140" >Số view</th>
                                        <th width="120" >Nổi bật</th>
                                        <th width="160" >Quản lý</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0;$i<count($data_post);$i++){ ?>
                                        <tr>                                            
                                            <td align="left"><?php echo $data_post[$i]['post_title'];  ?></td>
                                            <td><?php echo $data_post[$i]['post_alias'];  ?></td>
                                            <td><?php echo $data_post[$i]['post_views'];  ?></td>
                                            <td id="is_hot_row_<?php echo $data_post[$i]['id']; ?>">
                                                <a href="javascript:" onclick="change_is_hot(<?php echo $data_post[$i]['id']; ?>,<?php echo $data_post[$i]['post_is_hot']; ?>)"><span class="ico shadow checkmark2 <?php if($data_post[$i]['post_is_hot'] == 1){echo "color";}else{echo "gray";} ?>"></span></a>
                                            </td>
                                            <td>
                                                <span class="tip" >
                                                    <a title="Xem" href="http://<?php echo Yii::app()->params['domain'];?><?php echo $data_post[$i]['category_alias']; ?>/<?php echo $data_post[$i]['post_alias'];  ?>/" target="_blank" >
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/color_18/eye.png" >
                                                    </a>
                                                </span>&nbsp;
                                                <span class="tip" >
                                                    <a title="Sửa" href="<?php echo $url->createUrl("post/detail",array("post_id"=>$data_post[$i]['id'])); ?>" >
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/icon_edit.png" >
                                                    </a>
                                                </span>&nbsp;
                                                <span class="tip" >
                                                    <a title="Thêm chap" href="<?php echo $url->createUrl("post/addchap",array("parent_post_id"=>$data_post[$i]['id'],"category_id"=>$data_post[$i]['category_id'])); ?>">
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/color_18/copy.png" >
                                                    </a>
                                                </span>&nbsp;
                                                <span class="tip" >
                                                    <a value="<?php echo $data_post[$i]['id']; ?>" class="Delete" name="<?php echo $data_post[$i]['post_title'];  ?>" title="Xóa"  >
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/icon_delete.png" >
                                                    </a>
                                                </span>
                                            </td> 
                                        </tr>
                                        <?php } ?>

                                </tbody>
                            </table>                            

                        </form>

                        <div class="clearfix">
                            <ul class="paging fr">
                                <?php
                                    echo $paging;
                                ?>
                            </ul>
                        </div>

                    </div>    

                </div>

                <div id="tab1" class="tab_content"> 

                </div>                

            </div>
        </div>
        <div class="clear"></div>                  
    </div>
</div>

<?php }else{
    echo "Không có dữ liệu";
} ?>

<script>    

    function submit_form(){
        /*var keyword = $('#keyword').val();
        if(keyword != ''){
            $('#search_form').submit();   
        }*/
	    $('#search_form').submit();
    }

    $(document).ready(function() {
        //$("#post_content").cleditor()[0].focus();

        $(document).ready(function() {
            $("#post_content").cleditor({
                width:        500, // width not including margins, borders or padding
                height:       250, // height not including margins, borders or padding
                controls:     // controls to add to the toolbar
                "bold italic underline strikethrough subscript superscript | font size " +
                "style | color highlight removeformat | bullets numbering | outdent " +
                "indent | alignleft center alignright justify | undo redo | " +
                "rule image link unlink | cut copy paste pastetext | print source",
                colors:       // colors in the color popup
                "FFF FCC FC9 FF9 FFC 9F9 9FF CFF CCF FCF " +
                "CCC F66 F96 FF6 FF3 6F9 3FF 6FF 99F F9F " +
                "BBB F00 F90 FC6 FF0 3F3 6CC 3CF 66C C6C " +
                "999 C00 F60 FC3 FC0 3C0 0CC 36F 63F C3C " +
                "666 900 C60 C93 990 090 399 33F 60C 939 " +
                "333 600 930 963 660 060 366 009 339 636 " +
                "000 300 630 633 330 030 033 006 309 303",    
                fonts:        // font names in the font popup
                "Arial,Arial Black,Comic Sans MS,Courier New,Narrow,Garamond," +
                "Georgia,Impact,Sans Serif,Serif,Tahoma,Trebuchet MS,Verdana",
                sizes:        // sizes in the font size popup
                "1,2,3,4,5,6,7",
                styles:       // styles in the style popup
                [["Paragraph", "<p>"], ["Header 1", "<h1>"], ["Header 2", "<h2>"],
                ["Header 3", "<h3>"],  ["Header 4","<h4>"],  ["Header 5","<h5>"],
                ["Header 6","<h6>"]],
                useCSS:       false, // use CSS to style HTML when possible (not supported in ie)
                docType:      // Document type contained within the editor
                '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
                docCSSFile:   // CSS file used to style the document contained within the editor
                "", 
                bodyStyle:    // style to assign to document body contained within the editor
                "margin:4px; font:10pt Arial,Verdana; cursor:text"
            });
        });

    });    

    $(".Delete").live('click',function() {         
        var post_id = $(this).attr("value");        
        var row=$(this).parents('tr');
        var dataSet=$(this).parents('form');
        var id = $(this).attr("id");
        var name = $(this).attr("name");        
        var data ='id='+id;        
        Delete(data,name,row,0,dataSet,post_id);
    });

    function Delete(data,name,row,type,dataSet,post_id){
        var loadpage = dataSet.hdata(0);
        var url = dataSet.hdata(1);
        var table = dataSet.hdata(2);
        var data = data+"&tabel="+table;
        $.confirm({
            'title': 'Xóa dữ liệu','message': " <strong>Bạn có muốn xóa </strong><br /><font color=red>' "+ name +" ' </font> ",'buttons': {'Chấp nhận': {'class': 'special',
                    'action': function(){
                        loading('Checking');
                        delete_post(post_id);
                        $('#preloader').html('Đang xóa');
                        if(type==0){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        if(type==1){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        setTimeout("unloading();",900);          
                }},'Hủy bỏ'    : {'class'    : ''}}});}

    function delete_post(post_id){

        var strUrl = '<?php echo $url->createUrl("post/ajaxDeletePost"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {post_id:post_id}, 
            beforeSend: function() {

            },
            complete: function() {

            },
            success: function(msg){                

            }
        })
    }

    function change_is_hot(post_id,is_hot){
        var is_hot_status = 1;
        var text_color = "color";
        if(is_hot == 1){
            is_hot_status = 0;
            text_color = "gray";
        }
        loading('Xử lý',1);                                                
        var strUrl = '<?php echo $url->createUrl("post/ajaxChangeStatus"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {post_id:post_id,is_hot_status:is_hot_status}, 
            beforeSend: function() {

            },
            complete: function() {
                setTimeout( "unloading()", 1000 );                    
            },
            success: function(msg){                
                if(msg == 1){                       
                    $('#is_hot_row_'+post_id).html('<a href="javascript:" onclick="change_is_hot('+post_id+','+is_hot_status+')"><span class="ico shadow checkmark2 '+text_color+'"></span></a>');
                }
            }
        })
    }

</script>