<?php 
    $url = new Url();      
    //$link_url = Yii::app()->params['urlRs']."/upload/upload.php?forder_up=author";
    $url_img_thichtruyen = Helpers::getImageUrl();
    $link_url = $url_img_thichtruyen."upload/upload.php?forder_up=author";
?> 

<link href="<?php echo Yii::app()->params['urlRs']; ?>/js/swfupload/css/default.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="<?php echo Yii::app()->params['urlRs']; ?>/js/swfupload/swfupload.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->params['urlRs']; ?>/js/swfupload/js/handlers.js"></script>
<div class="onecolumn" >
    <div class="header"><span><span class="ico gray window"></span>  Thêm mới tác giả  </span></div>
    <div class="clear"></div>
    <div class="content" >

        <form id="validation_demo" action="#">       
            <input type="hidden" id="author_id" name="author_id" value="<?php echo $data_author['author_id']; ?>">      
            <div class="section ">
                <label> Tên tác giả<small></small></label>   
                <div> 
                    <input type="text" class="validate[required] large" name="author_name" id="author_name" value="<?php echo $data_author['author_name']; ?>">
                </div>                                
            </div>       
            <div class="section ">
                <label> Ảnh tác giả<small></small></label>   
                <div>
                    <div>
                        <input type="text" id="txtFileName" disabled="disabled" style="background-color: #FFFFFF;height: 20px;" size="30" value="<?php echo $data_author['author_image']; ?>" />                            
                        <span id="spanButtonPlaceholder"></span>
                    </div>                        
                    <div class="flash" id="divFileProgressContainer"></div>
                    <div id="thumbnails"></div>
                </div>
            </div>                         
            <div class="section ">
                <label> Giới thiệu tác giả<small></small></label>   
                <div>                                     
                    <div>&nbsp;</div>      
                </div>                        
                <div style="margin-left : 0px ;"> <textarea name="author_content" id="author_content"  class="author_content" cols="" rows=""><?php echo $data_author['author_description']; ?></textarea></div>
            </div>                                    
            <div class="section last">
                <div>                                    
                    <a class="uibutton submit_btn" onclick="update_author()" >Sửa tác giả</a>
                </div>
            </div>
        </form>

    </div>
</div>

<script type="">

    //-----------------------------------------------------------------------------------------------------------------//   

    var filename = "";
    var filepath = "";
    function getdata(data_json){              
        var json = $.parseJSON(data_json);
        var code = json.code;   

        if(code == '105')
            {
            filename = json.data["filename"];

            //filetype = json.data["filetype"];
            //filesize = json.data["filesize"];
            filepath = "/" + json.data["forder_up"]  + "/" + ""+filename;
            //path = "<?=Yii::app()->params['urlImage']?>/"+ json.data["forder_up"]  + "/" + ""+filename;
            path = "<?php echo $url_img_thichtruyen; ?>data/upload_data/"+ json.data["forder_up"]  + "/" + ""+filename;
            $("#txtFileName").val(filename);
            $("#thumbnails").html("<img style='max-height: 200px;max-width: 200px;' src='"+path+"'/>");                          
        }
        else
            {
            var msg = json.msg;                
            alert(msg);    
        }
    }

    //-----------------------------------------------------------------------------------------------------------------//

    var swfu;
    window.onload = function () {
        swfu = new SWFUpload({
            // Backend Settings
            upload_url: "<?=$link_url ?>",
            post_params: "resume_file",

            // File Upload Settings
            file_size_limit : "4 MB",
            file_types : "*.jpg;*.jpeg;*.png;",
            file_types_description : "Ảnh",
            file_upload_limit : "0",

            file_queue_error_handler : fileQueueError,
            file_dialog_complete_handler : fileDialogComplete,
            upload_progress_handler : uploadProgress,
            upload_error_handler : uploadError,
            upload_success_handler : uploadSuccess,
            upload_complete_handler : uploadComplete,

            // Button Settings
            button_image_url : "<?php echo Yii::app()->params['urlRs']; ?>/js/swfupload/XPButtonUploadText_61x22.png",
            button_image_url : "<?php echo $url_img_thichtruyen; ?>js/swfupload/XPButtonUploadText_61x22.png",
            button_placeholder_id : "spanButtonPlaceholder",
            button_width: 61,
            button_height: 22,

            // Flash Settings
            //flash_url : "<?php echo Yii::app()->params['urlRs']; ?>/js/swfupload/swfupload.swf",
            flash_url : "<?php echo $url_img_thichtruyen; ?>js/swfupload/swfupload.swf",

            custom_settings : {
                upload_target : "divFileProgressContainer"
            },

            // Debug Settings
            //debug: true
        });
    };

    //-----------------------------------------------------------------------------------------------------------------//

    $(document).ready(function() {
        //$("#post_content").cleditor()[0].focus();        

        $(document).ready(function() {
            $("#author_content").cleditor({
                width:        "100%", // width not including margins, borders or padding
                height:       600, // height not including margins, borders or padding
                controls:     // controls to add to the toolbar
                "bold italic underline strikethrough subscript superscript | font size " +
                "style | color highlight removeformat | bullets numbering | outdent " +
                "indent | alignleft center alignright justify | undo redo | " +
                "rule image link unlink | cut copy paste pastetext | print source",
                colors:       // colors in the color popup
                "FFF FCC FC9 FF9 FFC 9F9 9FF CFF CCF FCF " +
                "CCC F66 F96 FF6 FF3 6F9 3FF 6FF 99F F9F " +
                "BBB F00 F90 FC6 FF0 3F3 6CC 3CF 66C C6C " +
                "999 C00 F60 FC3 FC0 3C0 0CC 36F 63F C3C " +
                "666 900 C60 C93 990 090 399 33F 60C 939 " +
                "333 600 930 963 660 060 366 009 339 636 " +
                "000 300 630 633 330 030 033 006 309 303",    
                fonts:        // font names in the font popup
                "Times New Roman,Arial,Arial Black,Comic Sans MS,Courier New,Narrow,Garamond," +
                "Georgia,Impact,Sans Serif,Serif,Tahoma,Trebuchet MS,Verdana",
                sizes:        // sizes in the font size popup
                "1,2,3,4,5,6,7",
                styles:       // styles in the style popup
                [["Paragraph", "<p>"], ["Header 1", "<h1>"], ["Header 2", "<h2>"],
                ["Header 3", "<h3>"],  ["Header 4","<h4>"],  ["Header 5","<h5>"],
                ["Header 6","<h6>"]],
                useCSS:       false, // use CSS to style HTML when possible (not supported in ie)
                docType:      // Document type contained within the editor
                '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
                docCSSFile:   // CSS file used to style the document contained within the editor
                "", 
                bodyStyle:    // style to assign to document body contained within the editor
                "margin:4px; font:20px; cursor:text;text-align:justify"
            });
        });

    }); 

    function update_author(){                
        if($("form#validation_demo").validationEngine('validate')){
            var author_id = $('#author_id').val();            
            var author_name = $('#author_name').val();            
            var author_image = $('#txtFileName').val();                            
            var author_content = $('#author_content').val();            

            /*if(author_image == ''){
            alert('Bạn chưa chọn danh mục!');
            return false;
            }*/

            loading('Xử lý',1);                                                
            var strUrl = '<?php echo $url->createUrl("author/ajaxEditAuthor"); ?>';         
            $.ajax({
                type: "POST",
                url: strUrl,            
                data: {author_id:author_id,author_name:author_name,author_image:author_image,author_content:author_content}, 
                beforeSend: function() {

                },
                complete: function() {                    
                    setTimeout( "unloading()", 1000 );                           
                },
                success: function(msg){                    
                    if(msg == -1){                       
                        setTimeout('showError("Tên truyện đã tồn tại!")',100);                                                    
                    }else{                        
                        window.location.href='<?php echo $url->createUrl("author/index"); ?>';
                    }
                }
            })
        }
    }

</script>