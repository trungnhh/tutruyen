<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php require_once ("meta.php");?>
    </head>
    <body class="dashborad">        
        <div id="alertMessage" class="error"></div> 

        <?php $this->widget("AdminHeader");?>

        <div id="shadowhead"></div>
        <div id="hide_panel"> 
            <a class="butAcc" rel="0" id="show_menu"></a>
            <a class="butAcc" rel="1" id="hide_menu"></a>
            <a class="butAcc" rel="0" id="show_menu_icon"></a>
            <a class="butAcc" rel="1" id="hide_menu_icon"></a>
        </div>           

        <?php $this->widget("AdminLeftMenu");?>

        <div id="content">
            <div class="inner">

                <?php $this->widget("AdminTopMenu");?>

                <?php echo $content;?>                             

                <?php $this->widget("AdminFooter");?>

            </div>
        </div>
    </body>

</html>
