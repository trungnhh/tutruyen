<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?php echo Helpers::getBrandName();?>&trade; Quản trị website phiên bản 1.5 - <?php echo Helpers::getBrandDomain();?></title>
        <link href="<?php echo Yii::app()->params["urlRs"]; ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />  
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <link href="<?php echo Yii::app()->params['urlRsAdmin'];?>/css/zice.style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->params['urlRsAdmin'];?>/css/icon.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->params['urlRsAdmin'];?>/components/tipsy/tipsy.css" media="all"/>
        <style type="text/css">
            html {
                background-image: none;
            }
            #versionBar {
                background-color:#212121;
                position:fixed;
                width:100%;
                height:35px;
                bottom:0;
                left:0;
                text-align:center;
                line-height:35px;
            }
            .copyright{
                text-align:center; font-size:10px; color:#CCC;
            }
            .copyright a{
                color:#A31F1A; text-decoration:none
            }    
            #main-logo {
                color: #3D698E;
                font-family: Verdana;
                font-size: 18px;
                font-weight: normal;
                margin: 0 0 10px;
                text-align: center;
                border-bottom: none;
            }
        </style>
        <script type="text/javascript" src="<?php echo Yii::app()->params['urlRsAdmin'];?>/js/jquery.min.js"></script>
    </head>
    <body >
    
        <?php echo $content;?>
    
        <!--Login div-->
        <div class="clear"></div>
        <div id="versionBar" >
            <div class="copyright" > &copy; Copyright 2016  All Rights Reserved <span class="tip"><a target="_blank" href="<?php echo Helpers::getBaseUrl(); ?>" title="SauTruyen Admin" ><?php echo Helpers::getBrandName(); ?></a> </span> </div>
            <!-- // copyright-->
        </div>
        <!-- Link JScript-->        
        <script type="text/javascript" src="<?php echo Yii::app()->params['urlRsAdmin'];?>/components/effect/jquery-jrumble.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->params['urlRsAdmin'];?>/components/ui/jquery.ui.min.js"></script>     
        <script type="text/javascript" src="<?php echo Yii::app()->params['urlRsAdmin'];?>/components/tipsy/jquery.tipsy.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->params['urlRsAdmin'];?>/components/checkboxes/iphone.check.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->params['urlRsAdmin'];?>/js/login.js"></script>
    </body>
</html>