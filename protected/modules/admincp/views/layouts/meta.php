<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />

<title><?php echo Helpers::getBrandName(); ?>&trade; Quản trị website phiên bản 1.5 - <?php echo Helpers::getBrandDomain(); ?></title>
<link href="<?php echo Yii::app()->params["urlRs"]; ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />  
<!-- Link css-->
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/css/zice.style.css"/>
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/css/icon.css"/>
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/css/ui-custom.css"/>
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/css/timepicker.css"  />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/components/colorpicker/css/colorpicker.css"  />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/components/elfinder/css/elfinder.css" />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/components/datatables/dataTables.css"  />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/components/validationEngine/validationEngine.jquery.css" />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/components/jscrollpane/jscrollpane.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/components/fancybox/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/components/tipsy/tipsy.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/components/editor/jquery.cleditor.css"  />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/components/chosen/chosen.css" />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/components/confirm/jquery.confirm.css" />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/components/sourcerer/sourcerer.css"/>
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/components/fullcalendar/fullcalendar.css"/>
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/components/Jcrop/jquery.Jcrop.css"  />

<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/flot/excanvas.min.js"></script><![endif]-->

<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/ui/jquery.ui.min.js"></script> 
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/ui/jquery.autotab.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/ui/timepicker.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/colorpicker/js/colorpicker.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/checkboxes/iphone.check.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/elfinder/js/elfinder.full.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/datatables/dataTables.min.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/scrolltop/scrolltopcontrol.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/jscrollpane/mousewheel.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/jscrollpane/mwheelIntent.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/jscrollpane/jscrollpane.min.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/spinner/ui.spinner.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/tipsy/jquery.tipsy.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/editor/jquery.cleditor.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/chosen/chosen.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/confirm/jquery.confirm.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/validationEngine/jquery.validationEngine.js" ></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/validationEngine/jquery.validationEngine-en.js" ></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/vticker/jquery.vticker-min.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/sourcerer/sourcerer.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/fullcalendar/fullcalendar.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/flot/flot.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/flot/flot.pie.min.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/flot/flot.resize.min.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/flot/graphtable.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/uploadify/swfobject.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/uploadify/uploadify.js"></script>        
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/checkboxes/customInput.jquery.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/effect/jquery-jrumble.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/filestyle/jquery.filestyle.js" ></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/placeholder/jquery.placeholder.js" ></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/Jcrop/jquery.Jcrop.js" ></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/imgTransform/jquery.transform.js" ></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/webcam/webcam.js" ></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/rating_star/rating_star.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/dualListBox/dualListBox.js"  ></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/components/smartWizard/jquery.smartWizard.min.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/js/jquery.cookie.js"></script>
<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/js/zice.custom.js"></script>


<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/js/cleditor/jquery.cleditor.js"></script>
<!--<script type="text/javascript" src="<?=Yii::app()->params["urlRsAdmin"] ?>/js/cleditor/jquery.cleditor.bbcode.min.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRsAdmin"] ?>/js/cleditor/jquery.cleditor.css"  />
