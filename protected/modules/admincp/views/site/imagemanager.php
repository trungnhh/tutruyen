<div class="onecolumn" >
    <div class="header">
        <span ><span class="ico gray pictures_folder"></span>GALLERY MANAGER</span>
        <div id="buttom_top">
            <ul class="uibutton-group">
                <li><a class="uibutton icon add pop_box"   href="modalalbum.html"  >New Album</a></li>
                <li><div id="uploadButtondisable"></div><a  class="uibutton disable icon secure pop_box"   id="uploadAlbum">upload Pic to Album</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="clear"></div>     
    <div class="content" >

        <div id="albumsList" >
            <div class="load_page" style="padding:0">
                <div class="album load" id="1">
                    <div class="preview">
                        <img width="130" id="photo1" class="stackphotos" src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/23.jpg" alt="Thumbnail" />
                        <img width="130" id="photo2" class="stackphotos" src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/27.jpg" alt="Thumbnail" />
                        <img width="130" id="photo3" class="stackphotos" src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/25.jpg" alt="Thumbnail" />
                        <div style="clear:both"></div>
                    </div>
                    <div class="title">02/20/2012</div>
                    <div class="stats">Images: <span class="picCount">15</span></div>
                    <div class="clear"></div>
                </div>
                <div class="album load" id="2">
                    <div class="preview">
                        <img width="130" id="photo1" class="stackphotos" src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/06.jpg" alt="Thumbnail" />
                        <img width="130" id="photo3" class="stackphotos" src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/30.jpg" alt="Thumbnail" />
                        <div style="clear:both"></div>
                    </div>
                    <div class="title">02/15/2012</div>
                    <div class="stats">Images: <span class="picCount">2</span></div>
                    <div class="clear"></div>
                </div>  
                <div class="album load" id="3">
                    <div class="preview">
                        <img width="130" id="p1" class="stackphotos" src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/17.jpg" alt="Thumbnail" />
                        <div style="clear:both"></div>
                    </div>
                    <div class="title">29/01/2012</div>
                    <div class="stats">Images: <span class="picCount">1</span></div>
                    <div class="clear"></div>
                </div>
                <!--                                         <div class="album load" id="4">
                <div class="preview">
                <img width="130" id="photo1" class="stackphotos" src="exampic/25.jpg" alt="Thumbnail" />
                <img width="130" id="photo2" class="stackphotos" src="exampic/25.jpg" alt="Thumbnail" />
                <img width="130" id="photo3" class="stackphotos" src="exampic/25.jpg" alt="Thumbnail" />
                <div style="clear:both"></div>
                </div>
                <div class="title">15/01/2012</div>
                <div class="stats">Images: <span class="picCount">15</span></div>
                <div class="clear"></div>
                </div>
                <div class="album load" id="5">
                <div class="preview">
                <img width="130" id="photo1" class="stackphotos" src="exampic/25.jpg" alt="Thumbnail" />
                <img width="130" id="photo2" class="stackphotos" src="exampic/25.jpg" alt="Thumbnail" />
                <img width="130" id="photo3" class="stackphotos" src="exampic/25.jpg" alt="Thumbnail" />
                <div style="clear:both"></div>
                </div>
                <div class="title">28/12/2011</div>
                <div class="stats">Images: <span class="picCount">15</span></div>
                <div class="clear"></div>
                </div>-->
            </div>
        </div>

        <div class="screen-msg"><span class="icon"><img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/dialog.png" alt="dialog"/></span>Click album to more any images.</div>

        <div class="albumImagePreview">
            <div class="album_edit" style="display:none">
                <form name="Save_form"  id="Save_form" action="#">
                    <h1>Edit  Album</h1>

                    <div class="picPreview"><img id="image-albumPreview" title="Drop Image Here"  src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/25.jpg" alt="Image Preview"  /></div>
                    <div class="clear"></div>
                    <div class="hr"></div>

                    <input type="hidden" name="id_edit" id="id_edit"  value="1"/>
                    <input type="hidden" name="thumbPreview" id="thumbPreview"  />
                    <div class="tip">
                        <input type="text" name="name" id="name"   title="Album name" style="width:146px" value="02/20/2012" maxlength="35" />
                    </div>
                    <div class="hr"></div>
                    <ul class="uibutton-group">
                        <a class="uibutton save_form normal loading"  title="Saving" rel="1">save</a>
                        <a class="uibutton  albumDelete normal " id="1" name="02/20/2012" >Delete </a>
                    </ul>
                    <div class="hr"></div>
                </form>
                <div class="deletezone small" > Drop Images To Delete</div>
                <div class="hr"></div>    

                <div class="clear"></div>
            </div>

            <div class="boxtitle" ><span class="texttip">double click to viwe large images // </span>
                <span class="tip"><a id="editAlbum"  class="editOn" title="Click here to edit  Album">edit album</a></span> or 
                <span class="tip"><a class="albumDelete red" id="1" name="02/20/2012"   title="Click here to Delete  Album">Delete </a></span>
            </div>        


            <div class="albumpics">

                <ul id="sortable"  >

                    <li class="albumImage">
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/05.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/05.jpg"  title="Drag This Photo" />
                            <div class="picTitle">DSC00249.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>10</li><!--// This id images-->
                                <li>DSC00249.jpg</li><!--// This name images-->
                            </ul>   
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/12.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/12.jpg"   title="Drag This Photo" />
                            <div class="picTitle">DSC00250.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>11</li><!--// This id images-->
                                <li>DSC00250.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>

                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/23.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/23.jpg"   title="Drag This Photo" />
                            <div class="picTitle">DSC00251.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>12</li><!--// This id images-->
                                <li>DSC00251.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/28.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/28.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/21.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/21.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/27.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/27.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/30.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/30.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/09.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/09.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/10.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/10.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/13.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/13.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/01.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/01.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/02.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/02.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/03.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/03.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/16.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/16.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/19.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/19.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/06.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/06.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/17.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/17.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/15.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/15.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/11.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/11.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/14.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/14.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/07.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/07.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/08.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/08.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/18.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/18.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/04.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/04.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/22.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/22.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="exampic/b/24.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/24.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/25.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/25.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/20.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/20.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/b/26.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/26.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>
                    <li class="albumImage" >
                        <div class="picHolder">
                            <span class="image_highlight"></span>
                            <a href="images/demo-size3.jpg" rel='glr'></a>    
                            <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/exampic/29.jpg"    title="Drag Image to delete"/>
                            <div class="picTitle">DSC00252.jpg</div>
                            <ul class="dataImg"><!--// This data images with your call to php or SQL -->
                                <li>13</li><!--// This id images-->
                                <li>DSC00252.jpg</li><!--// This name images-->
                            </ul>  
                        </div>
                    </li>


                </ul>
            </div> 
        </div>

        <div class="clear"></div>
    </div>
</div>