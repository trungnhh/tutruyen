<?php 
    $url = new Url(); 
    $color_chart[0] = "#92B927";
    $color_chart[1] = "#A0CA2B";
    $color_chart[2] = "#AAD435";
    $color_chart[3] = "#B8DB57";
    $color_chart[4] = "#BFDF68";
    $color_chart[5] = "#C6E278";
?>
<div class="onecolumn" >
    <div class="header"> <span ><span class="ico gray home"></span> Quản lý website</span> </div>
    <div class="clear"></div>
    <div class="content" >
        <div class="boxtitle min">Thống kê website</div>
        <div  class="grid2">
            <table class="chart-pie" style="width : 100%;">
                <thead>
                    <tr>
                        <th></th>
                        <?php for($i=0;$i<count($data_chart);$i++){ ?>
                            <th style="color : <?php echo $color_chart[$i]; ?>;"><?php echo $data_chart[$i]['name']; ?></th>
                            <?php } ?>                        
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th></th>
                        <?php for($i=0;$i<count($data_chart);$i++){ ?>
                            <td><?php echo $data_chart[$i]['count']; ?></td>
                            <?php } ?>                        
                    </tr>
                </tbody>
            </table>
            <div class="chart-pie-shadow" ></div>
            <div class="chart_title"><span>Số truyện trong các danh mục được post trong 1 tuần</span></div>
        </div>
        <div  class="grid2">
            <div class="inner">
                <form action="#">                    
                    <div class="section">
                        <label> Tiêu đề Website <small>Tủ Truyện</small></label>
                        <div>
                            <input type="text" name="page_title" id="page_title" class="full" value="<?php echo $data_term[0]['page_title']; ?>" />
                        </div>
                    </div>
                    <div class="section">
                        <label> Mô tả Website <small>Tủ Truyện</small></label>
                        <div>
                            <input type="text" name="page_description" id="page_description"  class="full"  value="<?php echo $data_term[0]['page_description']; ?>" />
                        </div>
                    </div>
                    <!--<div class="section">
                        <label> Thông báo SMS VIP <small>Tủ Truyện</small></label>
                        <div>                            
                            <div>
                                <textarea name="editor" id="vip_messege"  class="vip_messege"  cols="" rows=""><?php /*echo $data_term[0]['vip_messege']; */?></textarea>
                            </div>      
                        </div>
                    </div>-->
                    <div class="section">
                        <label> Thông báo HOT <small>Tủ Truyện</small></label>
                        <div>                            
                            <div>
                                <textarea name="editor" id="hot_messege"  class="hot_messege"  cols="" rows=""><?php echo $data_term[0]['hot_messege']; ?></textarea>
                            </div>      
                        </div>
                    </div>
                    <!--<div class="section">
                    <label> Trạng thái <small>Truyện186</small></label>
                    <div> 
                    <input type="checkbox" id="online" name="online"   class="online"  value="1"   checked="checked" />
                    </div>
                    </div>-->
                    <!--<div class="section">
                    <label> SEO metaTag <small>Truyện186</small></label>
                    <div>
                    <select  class="chzn-select"  multiple="multiple" tabindex="4">
                    <option value=""></option>
                    <option value="" selected="selected">webstie </option>
                    <option value="" selected="selected">data manager</option>
                    <option value="" selected="selected">shopping</option>
                    <option value="" selected="selected">appication</option>
                    <option value="" >webdesign</option>
                    <option value="" >truyen186</option>
                    <option value="" >icoldstyle</option>
                    <option value="">your tag</option>
                    </select>
                    </div>
                    </div>-->
                    <div class="section last">
                        <div> 
                            <a class="uibutton loading"  title="Lưu"  rel="1" onclick="update_term()" > Lưu</a>                             
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>                

<div class="onecolumn" >
    <div class="header"><span ><span class="ico gray stats_lines"></span>Thống kê website</span></div>
    <div class="clear"></div>
    <div class="content" > <br  class="clear"/>
        <div class="grid1 rightzero">
            <div class="shoutcutBox"> <span class="ico color chat-exclamation"></span> <strong><?php echo $count_post; ?></strong> <em>Truyện</em> </div>
            <div class="breaks"><span></span></div>
            <!-- // breaks -->
            <div class="shoutcutBox" > <span class="ico color item"></span> <strong><?php echo $count_post_new; ?></strong> <em> Truyện mới</em> </div>
            <div class="shoutcutBox"> <span class="ico color group"></span> <strong><?php echo $count_user; ?></strong> <em>Thành viên</em> </div>
            <div class="shoutcutBox"> <span class="ico color emoticon_grin"></span> <strong><?php echo $count_user_new; ?></strong> <em>Thành viên mới</em> </div>
            <div class="breaks"><span></span></div>
            <!-- // breaks -->
            <div class="shoutcutBox"> <span class="ico color emoticon_in_love"></span> <strong><?php echo $count_bookmark; ?></strong> <em>Lượt đánh dấu</em> </div>
            <div class="shoutcutBox last"> <span class="ico color connect"></span> <strong></strong> <em>&nbsp;</em> </div>
        </div>
        <div class="grid3">
            <div  style="width:100%;height:415px; margin-left:25px">
                <table class="chart" style="width : 100%;">
                    <thead>
                        <tr>
                            <th width="10%"></th>
                            <th width="25%" style="color : #bedd17;">Số lượng bài post</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $temp =0 ; foreach($data_chart_post as $row){ $temp++; ?>
                        <tr>
                            <th><?php echo $temp; ?></th>
                            <td><?php echo $row['count']; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<script>

    $(document).ready(function() {
        $("#vip_messege,#hot_messege").cleditor({
            width:        "100%", // width not including margins, borders or padding
            height:       180, // height not including margins, borders or padding
            controls:     // controls to add to the toolbar
            "bold italic underline | font size " +
            "| color | bullets | " +            
            "link | paste pastetext | source"            
        });

    });

    var d = new Date();
    $("table.chart").each(function() {
	    var colors = [];
	    $("table.chart thead th:not(:first)").each(function() {
		    colors.push($(this).css("color"));
	    });
	    $(this).graphTable({
		    series: 'columns',
		    position: 'replace',
		    width: '96%',
		    height: '423px',
		    colors: colors
	    }, {
		    xaxis: {
			    tickSize: 1,
			    ticks: [[1,getDateForChart(13)],[2,getDateForChart(12)],[3,getDateForChart(11)],[4,getDateForChart(10)],[5,getDateForChart(9)],[6,getDateForChart(8)],[7,getDateForChart(7)],[8,getDateForChart(6)],[9,getDateForChart(5)],[10,getDateForChart(4)],[11,getDateForChart(3)],[12,getDateForChart(2)],[13,getDateForChart(1)],[14, getDateForChart(0)]]
		    },
		    yaxis: {
			    max: 600,
			    min: 0,
		    },
		    series: {
			    points: {show: true },
			    lines: { show: true, fill: true, steps: false },
		    }
	    });
    });

    function getDateForChart(day_minus){
	    var today = new Date();
	    today.setDate(today.getDate() - day_minus);
	    //today.setTime(today.getTime() - day_minus * 24 * 60 * 60 * 1000);
	    return today.getDate() + '/' + parseInt(today.getMonth() + 1);
    }

    function update_term(){
        var page_title = $('#page_title').val();
        var page_description = $('#page_description').val();            
        var vip_messege = $('#vip_messege').val();                           
        var hot_messege = $('#hot_messege').val();                           

        var strUrl = '<?php echo $url->createUrl("site/ajaxUpdateTerm"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {page_title:page_title,page_description:page_description,vip_messege:vip_messege,hot_messege:hot_messege}, 
            beforeSend: function() {

            },
            complete: function() {                    

            },
            success: function(msg){                    

            }
        })
    }

</script>