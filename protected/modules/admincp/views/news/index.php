<?php 
    $url = new Url(); 
?>    
<div class="onecolumn" >
    <div class="header"><span ><span class="ico  gray bookmark"></span>Quản lý tin tức</span> </div>
    <div class="clear"></div>
    <div class="content" >    
        <div id="uploadTab">
            <ul class="tabs" >
                <li><a href="#tab2" id="3">  Danh sách tin tức  <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/new.gif" width="20" height="9" /></a></li>                
            </ul>
            <div class="tab_container">

                <div id="tab2" class="tab_content"> 
                    <div class="load_page">
                        <ul class="uibutton-group">
                            <li><span class="tip"><a class="uibutton icon add" title="Click để thêm tin tức" href="<?php echo $url->createUrl("news/add"); ?>">Thêm tin tức</a></span></li>
                        </ul>
                        <form class="tableName toolbar">

                            <h3>Danh sách tin tức</h3>                            
                            <table class="display data_table2" id="data_table">
                                <thead>
                                    <tr>
                                        <!--<th width="35" ><input type="checkbox" id="checkAll1"  class="checkAll"/></th>-->
                                        <th width="20" >ID</th>
                                        <th width="250" align="left">Tiêu đề</th>                                                                                
                                        <th width="" >Nội dung</th>
                                        <th width="120" >Trạng thái</th>
                                        <th width="120" >Quản lý</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php for($i=0;$i<count($data_news);$i++){ ?>

                                        <tr>
                                            <td><?php echo $data_news[$i]['id'];  ?></td>
                                            <td align="left"><?php echo $data_news[$i]['news_title'];  ?></td>                                                                                    
                                            <td><?php $content = $data_news[$i]['news_content'];$content = StringUtils::cutstring($content,200,true);$content = nl2br($content);  echo $content;  ?></td>
                                            <td id="is_hot_row_<?php echo $data_news[$i]['id']; ?>">
                                                <a href="javascript:"><span class="ico shadow checkmark2 <?php if($data_news[$i]['news_status'] == 1){echo "color";}else{echo "gray";} ?>"></span></a>
                                            </td>                                            
                                            <td >                                                
                                                <span class="tip" >
                                                    <a title="Sửa" href="<?php echo $url->createUrl("news/detail",array("id"=>$data_news[$i]['id'])); ?>" >
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/icon_edit.png" >
                                                    </a>                                                                
                                                </span>&nbsp;
                                                <span class="tip" >
                                                    <a value="<?php echo $data_author[$i]['author_id']; ?>" class="Delete" name="<?php echo $data_news[$i]['news_title'];  ?>" title="Xóa"  >
                                                        <img src="<?php echo Yii::app()->params['urlRsAdmin'];?>/images/icon/icon_delete.png" >
                                                    </a>
                                                </span>                                                                                    
                                            </td> 
                                        </tr>

                                        <?php } ?>                            

                                </tbody>
                            </table>
                        </form>
                    </div>    
                </div>                

            </div> 
        </div>
        <div class="clear"/></div>                   
</div>
</div>

<script>    

    $(document).ready(function() {
        //$("#post_content").cleditor()[0].focus();
        
        $(document).ready(function() {
            $("#post_content").cleditor({
                width:        500, // width not including margins, borders or padding
                height:       250, // height not including margins, borders or padding
                controls:     // controls to add to the toolbar
                "bold italic underline strikethrough subscript superscript | font size " +
                "style | color highlight removeformat | bullets numbering | outdent " +
                "indent | alignleft center alignright justify | undo redo | " +
                "rule image link unlink | cut copy paste pastetext | print source",
                colors:       // colors in the color popup
                "FFF FCC FC9 FF9 FFC 9F9 9FF CFF CCF FCF " +
                "CCC F66 F96 FF6 FF3 6F9 3FF 6FF 99F F9F " +
                "BBB F00 F90 FC6 FF0 3F3 6CC 3CF 66C C6C " +
                "999 C00 F60 FC3 FC0 3C0 0CC 36F 63F C3C " +
                "666 900 C60 C93 990 090 399 33F 60C 939 " +
                "333 600 930 963 660 060 366 009 339 636 " +
                "000 300 630 633 330 030 033 006 309 303",    
                fonts:        // font names in the font popup
                "Arial,Arial Black,Comic Sans MS,Courier New,Narrow,Garamond," +
                "Georgia,Impact,Sans Serif,Serif,Tahoma,Trebuchet MS,Verdana",
                sizes:        // sizes in the font size popup
                "1,2,3,4,5,6,7",
                styles:       // styles in the style popup
                [["Paragraph", "<p>"], ["Header 1", "<h1>"], ["Header 2", "<h2>"],
                ["Header 3", "<h3>"],  ["Header 4","<h4>"],  ["Header 5","<h5>"],
                ["Header 6","<h6>"]],
                useCSS:       false, // use CSS to style HTML when possible (not supported in ie)
                docType:      // Document type contained within the editor
                '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
                docCSSFile:   // CSS file used to style the document contained within the editor
                "", 
                bodyStyle:    // style to assign to document body contained within the editor
                "margin:4px; font:10pt Arial,Verdana; cursor:text"
            });
        });
        
    });    

    $(".Delete").live('click',function() {         
        var author_id = $(this).attr("value");                
        var row=$(this).parents('tr');
        var dataSet=$(this).parents('form');
        var id = $(this).attr("id");
        var name = $(this).attr("name");        
        var data ='id='+id;        
        Delete(data,name,row,0,dataSet,author_id);
    });

    function Delete(data,name,row,type,dataSet,author_id){
        var loadpage = dataSet.hdata(0);
        var url = dataSet.hdata(1);
        var table = dataSet.hdata(2);
        var data = data+"&tabel="+table;
        $.confirm({
            'title': 'Xóa dữ liệu','message': " <strong>Bạn có muốn xóa </strong><br /><font color=red>' "+ name +" ' </font> ",'buttons': {'Chấp nhận': {'class': 'special',
                    'action': function(){
                        loading('Checking');
                        delete_author(author_id);
                        $('#preloader').html('Đang xóa');
                        if(type==0){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        if(type==1){ row.slideUp(function(){   showSuccess('Thành công',5000); unloading(); }); return false;}
                        setTimeout("unloading();",900);          
                }},'Hủy bỏ'    : {'class'    : ''}}});}

    function delete_author(author_id){

        var strUrl = '<?php echo $url->createUrl("news/ajaxDeleteAuthor"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {author_id:author_id}, 
            beforeSend: function() {

            },
            complete: function() {

            },
            success: function(msg){                

            }
        })
    }    

    function change_is_hot(post_id,is_hot){
        var is_hot_status = 1;
        var text_color = "color";
        if(is_hot == 1){
            is_hot_status = 0;
            text_color = "gray";
        }
        loading('Xử lý',1);                                                
        var strUrl = '<?php echo $url->createUrl("news/ajaxChangeStatus"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {post_id:post_id,is_hot_status:is_hot_status}, 
            beforeSend: function() {

            },
            complete: function() {
                setTimeout( "unloading()", 1000 );                    
            },
            success: function(msg){                
                if(msg == 1){                       
                    $('#is_hot_row_'+post_id).html('<a href="javascript:" onclick="change_is_hot('+post_id+','+is_hot_status+')"><span class="ico shadow checkmark2 '+text_color+'"></span></a>');
                }
            }
        })
    }

</script>