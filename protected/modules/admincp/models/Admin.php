<?php
class Admin extends CActiveRecord{

    public static function model($className = __CLASS__) {
        return parent::model ( $className );
    }

    // dat luat cho model
    public function rules(){}         

    // goi den bang can ket noi   
    public function tableName() {                  
        return 't186_admin';
    }
    
    public function validatePassword($password){
        return $this->hashPassword($password)===$this->password;
    }
 
    public function hashPassword($password){
        return md5($password);
    }

    // nhan cac thuong tinh 
    public function attributeLabels(){}

    public function getAllRows(){        
        $row = array();
        $sql = "SELECT * FROM t186_admin";
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);
        $row = $command->queryAll();
        return $row;
    }   
    
    public function getAllMod(){        
        $row = array();
        $sql = "SELECT * FROM t186_admin WHERE is_admin = 0";
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);
        $row = $command->queryAll();
        return $row;
    }

    public function getRowsByUsername($title){

        $row = array();        
        $sql = "SELECT * FROM t186_admin WHERE `username`='".$title."'";
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);            
        $row = $command->queryRow();                        
        return $row;
    }

    public function getRowsById($id){
	    $row = array();
	    $cache = Yii::app()->cache;
	    if($cache != null){
		    $cacheService = new CacheService("Admin","getRowsById",$id);
		    $key = $cacheService->createKey();
		    $dependency = $cacheService->createDependency();
		    $cache = Yii::app ()->cache->get ( $key );
	    }
	    //$cache=false;
	    if($cache == false){
		    $sql = "SELECT * FROM t186_admin WHERE `id` = '".$id."'";
		    $connect = Yii::app()->db;
		    $command = $connect->createCommand($sql);
		    $row = $command->queryRow();
		    Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
	    }else{
		    $row = $cache;
	    }
	    return $row;
    }

    public function getRowsByUserPaging($begin,$end){        
        $row = array();        
        $sql = "SELECT * FROM t186_admin LIMIT ".$begin.",".$end;            
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);
        $row = $command->queryAll();            
        return $row;
    }

    public function getRowsByTitleUserPaging($title,$begin,$end){        
        $row = array();        
        $sql = "SELECT * FROM t186_admin WHERE `username` LIKE '%".$title."%'  LIMIT ".$begin.",".$end;            
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);            
        $row = $command->queryAll();                        
        return $row;
    }

    public function deleteRowById($id){
        $row = array();                        
        $sql = "DELETE FROM t186_admin WHERE `id` = ".$id." ";
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);
        $row = $command->execute();            
        return $row;
    }    

    public function updateRowById($id,$password){
        $row = array();                        
        $sql = "UPDATE t186_admin SET `password`='".$password."' WHERE `id` = ".$id." ";
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);
        $row = $command->execute();            
        return $row;
    }
    
    public function insertRow($username,$password,$fullname,$user_create){   
        $pass = AdminLogin::hashPassword($password);     
        $date = date('Y-m-d',time());
        $row = array();              
        $sql = "INSERT INTO t186_admin (username,password,fullname,create_date,create_user) VALUES ('".$username."','".$pass."','".$fullname."','".$date."','".$user_create."')";
        //echo $sql;die;
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);
        $result = $command->execute();            
        return $result;
    }
       
    public function insert($data){
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);        
        $command->insert('t186_admin', $data);    
        $record_id=Yii::app()->db->getLastInsertID();  
        return $record_id;
    }
    
    public function update($id,$data){
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);                
        return $command->update('t186_admin', $data,"id=".$id);        
    }
    
    public function delete($where){
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);                
        return $command->delete('t186_admin', $where);        
    }       
       
}
