<?php
class AdminOptions extends CActiveRecord
{
    protected $_tableName = 't186_options';
    protected $_connect = null;
    protected $_configurationKey = '__configuration';

    public static function model($className = __CLASS__)
    {
        return parent::model ( $className );
    }

    /**
     * Get database model instance
     * @return null
     */
    protected function _getConnect()
    {
        if(!$this->_connect){
            $this->_connect = Yii::app()->db;
        }
        return $this->_connect;
    }

    /**
     * Get table name from database
     * @return string
     */
    public function tableName()
    {
        return $this->_tableName;
    }

    /**
     * Get option key of Configuration value
     * @return string
     */
    public function getConfigurationKey()
    {
        return $this->_configurationKey;
    }

    /**
     * Get Configuration value
     * @return bool|mixed
     */
    public function getConfiguration()
    {
        if(!empty($this->getRowValueByName($this->getConfigurationKey()))){
            $configuration = $this->getRowValueByName($this->getConfigurationKey());
            return unserialize($configuration['option_value']);
        }
        return false;
    }

    /**
     * Update Configuration value
     * @param $data
     * @return bool
     */
    public function updateConfiguration($data)
    {
        $insert_data = array(
            'option_name'     =>  $this->getConfigurationKey(),
            'option_value'    =>  serialize($data)
        );
        if(!$this->getConfiguration()){
            $this->insertObject($insert_data);
            return true;
        }
        $this->updateObject($insert_data, 'option_name', $this->getConfigurationKey());
        return true;
    }

    /**
     * Get all records of options
     * @return mixed
     */
    public function getOptions(){
        $sql = "SELECT * FROM {$this->tableName()}";
        $command = $this->_getConnect()->createCommand($sql);
        $rows = $command->queryAll();
        return $rows;
    }

    /**
     * Get a record by option_name
     * @param $option_name
     * @return array
     */
    public function getRowByName($option_name)
    {
        $row = array();
        $sql = "SELECT `".$this->tableName()."`.* FROM `".$this->tableName()."` WHERE `".$this->tableName()."`.`option_name` = '" . $option_name . "'";
        $command = $this->_getConnect()->createCommand($sql);
        $row = $command->queryRow();
        return $row;
    }

    /**
     * Get option_value by option_name
     * @param $option_name
     * @return array
     */
    public function getRowValueByName($option_name)
    {
        $row = array();
        $sql = "SELECT `".$this->tableName()."`.`option_value` FROM `".$this->tableName()."` WHERE `".$this->tableName()."`.`option_name` = '" . $option_name . "'";
        $command = $this->_getConnect()->createCommand($sql);
        $row = $command->queryRow();
        return $row;
    }

    /**
     * Insert a record to database
     * @param $array_input
     * @return mixed
     */
    public function insertObject($array_input){
        $sql = '';

        foreach($array_input as $key=>$value){
            $sql .= $key."='".$value."',";
        }
        $sql = 'INSERT INTO `'.$this->tableName().'` SET '.$sql;
        $sql = rtrim($sql,',');
        $command = $this->_getConnect()->createCommand($sql);
        $command->execute();
        $record_id = Yii::app()->db->getLastInsertID();

        return $record_id;
    }

    /**
     * Update a exist record
     * @param $array_input
     * @param $key_id
     * @param $key_value
     * @return int
     */
    public function updateObject($array_input, $key_id, $key_value)
    {
        $sql='';
        foreach($array_input as $key => $value)
        {
            $sql.=$key."='".$value."',";
        }
        $sql = rtrim($sql, ',');

        if($sql != ''){
            $sql = 'UPDATE `'.$this->tableName().'` SET '.$sql.' WHERE '.$key_id.'="'.$key_value.'"';
            $command = $this->_getConnect()->createCommand($sql);
            $a = $command->execute();
            return $a;
        }
        return -1;
    }

    public function deleteObject($array_input){
        $sql = ' 1 ';

        foreach($array_input as $key => $value){
            $sql.=" AND ".$key."='".$value."'";
        }
        $sql = rtrim($sql,',');

        if($sql!=''){
            $sql = 'delete from `'.$this->tableName().'` where "'.$sql.'"';
            $command = $this->_getConnect()->createCommand($sql);
            $a = $command->execute();
            if($a) return 1;
            return 0;
        }
        return 0;
    }

    public function insert($data)
    {
        $command = $this->_getConnect()->createCommand();
        $command->insert($this->tableName(), $data);
        $record_id = Yii::app()->db->getLastInsertID();
        return $record_id;
    }

    public function update($id, $data)
    {
        $command = $this->_getConnect()->createCommand();
        return $command->update($this->tableName(), $data,"id=".$id);
    }

    public function delete($where)
    {
        $command = $this->_getConnect()->createCommand();
        return $command->delete($this->tableName(), $where);
    }
}