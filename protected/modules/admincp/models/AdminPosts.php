<?php

    class AdminPosts extends CActiveRecord{
        public static function model($className = __CLASS__) {
            return parent::model ( $className );
        }

        // dat luat cho model
        public function rules(){}         

        // goi den bang can ket noi   
        public function tableName() {                  
            return 't186_posts';
        }

        // nhan cac thuong tinh 
        public function attributeLabels(){}    

        public function getRowById($id){
            $id = intval($id);
            // $cache = false;
            $row = array();            
            $sql = "SELECT t186_posts.* FROM t186_posts WHERE t186_posts.id =".$id;
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryRow();            
            return $row;
        }

        public function getRowForDel($id){
            $id = intval($id);
            // $cache = false;
            $row = array();            
            $sql = "SELECT post_title,post_content,post_image,post_alias,post_status,post_is_hot,post_is_vip,post_is_chap,post_views,category_id FROM t186_posts WHERE t186_posts.id =".$id;
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryRow();            
            return $row;
        }

        public function getRowByAlias($post_alias){        
            // $cache = false;
            $row = array();            
            $sql = "SELECT t186_posts.* FROM t186_posts INNER JOIN WHERE t186_posts.post_alias = '".$post_alias."'";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryRow();            
            return $row;
        }

        public function getDataByCategoryId($category_id){        
            $category_id = intval($category_id);
            // $cache = false;
            $row = array();            
            $sql = "SELECT id,post_title,post_alias,post_is_hot,post_views FROM t186_posts WHERE post_is_chap = 0 AND post_status = 1 AND category_id like '%" . $category_id.",%' ORDER BY id DESC";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryAll();            
            return $row;
        }

	    public function countPostsByDate($date){
		    $row = array();
		    $cache = Yii::app()->cache;
		    $start_time = strtotime($date);
		    $end_time = strtotime('+1 day', strtotime($date));
		    if($cache != null){
			    $cacheService = new CacheService("AdminPosts","countPostsByDate",$start_time.$end_time);
			    $key = $cacheService->createKey();
			    $dependency = $cacheService->createDependency();
			    $cache = Yii::app ()->cache->get ( $key );
		    }
		    //$cache=false;
		    if($cache == false){
			    $sql = "SELECT count(*) FROM t186_posts WHERE update_date > " . $start_time." AND update_date < " . $end_time;
			    $connect = Yii::app()->db;
			    $command = $connect->createCommand($sql);
			    $row = $command->queryScalar();
			    Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
		    }else{
			    $row = $cache;
		    }
		    return $row;
	    }

	    public function countPostsByMod($date,$mod_id){
		    $row = array();
		    $start_time = strtotime($date);
		    $end_time = strtotime('+1 day', strtotime($date));
		    $sql = "SELECT count(*) FROM t186_posts WHERE create_user = ".$mod_id." AND update_date > " . $start_time." AND update_date < " . $end_time;
		    $connect = Yii::app()->db;
		    $command = $connect->createCommand($sql);
		    $row = $command->queryScalar();
		    return $row;
	    }

        public function getDataByModCategoryId($category_id,$mod_id){        
            $category_id = intval($category_id);
            // $cache = false;
            $row = array();            
            $sql = "SELECT id,post_title,post_alias,post_is_hot,post_views FROM t186_posts WHERE create_user = '".$mod_id."' AND post_is_chap = 0 AND post_status = 1 AND category_id = " . $category_id." ORDER BY id DESC";            
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryAll();            
            return $row;
        }

        public function getChapByCategoryId($category_id){        
            $category_id = intval($category_id);
            // $cache = false;
            $row = array();            
            $sql = "SELECT id,post_title,post_alias,post_is_hot,post_views FROM t186_posts WHERE post_is_chap = 1 AND category_id = '%" . $category_id.",%' ORDER BY id DESC";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryAll();            
            return $row;
        }

        public function getPostsVipByCategoryId($category_id){        
            $category_id = intval($category_id);            
            $row = array();            
            if($category_id == 0){
                $sql = "SELECT t186_posts.id,post_title,post_alias,post_is_hot,post_views FROM t186_posts WHERE post_status = 1 AND post_cost > 0 ORDER BY t186_posts.id DESC";
            }else{
                $sql = "SELECT t186_posts.id,post_title,post_alias,post_is_hot,post_views FROM t186_posts WHERE post_status = 1 AND category_id like '%" . $category_id.",%' AND post_cost > 0 ORDER BY t186_posts.id DESC";
            }            
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryAll();        
            return $row;
        }

        public function getPostsFeaturedByCategoryId($category_id){        
            $category_id = intval($category_id);            
            $row = array();            
            if($category_id == 0){
                $sql = "SELECT t186_posts.id,post_title,post_alias,post_is_hot,post_views FROM t186_posts WHERE post_status = 1 AND post_featured = 1 ORDER BY t186_posts.id DESC";
            }else{
                $sql = "SELECT t186_posts.id,post_title,post_alias,post_is_hot,post_views FROM t186_posts WHERE post_status = 1 AND category_id like '%" . $category_id.",%' AND post_featured = 1 ORDER BY t186_posts.id DESC";
            }            
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryAll();            
            return $row;
        }

	    public function getPostsHotByCategoryId($category_id){
		    $category_id = intval($category_id);
		    $row = array();
		    if($category_id == 0){
			    $sql = "SELECT t186_posts.id,post_title,post_alias,post_is_hot,post_views FROM t186_posts WHERE post_status = 1 AND post_is_hot = 1 ORDER BY t186_posts.id DESC";
		    }else{
			    $sql = "SELECT t186_posts.id,post_title,post_alias,post_is_hot,post_views FROM t186_posts WHERE post_status = 1 AND category_id like '%" . $category_id.",%' AND post_is_hot = 1 ORDER BY t186_posts.id DESC";
		    }
		    $connect = Yii::app()->db;
		    $command = $connect->createCommand($sql);
		    $row = $command->queryAll();
		    return $row;
	    }

        public function getPostsGoing(){                            
            $row = array();            
            $sql = "SELECT t186_posts.id,post_title,post_alias,post_is_hot,post_is_full,post_views,category_id FROM t186_posts WHERE post_status = 1 AND post_is_full = 1 AND post_is_chap = 0 ORDER BY t186_posts.id DESC";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryAll();            
            return $row;
        }

        public function countDataByCategoryId($category_id){
            $category_id = intval($category_id);            
            $row = array();            
            $sql = "SELECT COUNT(*) FROM t186_posts WHERE category_id like '%" . $category_id . ",%'";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryScalar();                
            return $row;
        }

        public function countDataListByCategoryId($category_id){
            $category_id = intval($category_id);            
            $row = array();            
            $sql = "SELECT COUNT(*) FROM t186_posts WHERE post_is_chap = 0 AND post_status = 1 AND category_id like '%" . $category_id . ",%'";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryScalar();                
            return $row;
        }

        public function getDataByCategoryIdPaging($category_id,$begin,$end){            
            $row = array();            
            $sql = "SELECT id,post_title,post_alias,post_is_hot,post_views FROM t186_posts WHERE post_is_chap = 0 AND post_status = 1 AND category_id like '%" . $category_id . ",%'";
            $sql .= " ORDER BY id DESC LIMIT ".$begin.",".$end;                              
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);            
            $row = $command->queryAll();                        
            return $row;
        }

        public function getDataByModCategoryIdPaging($category_id,$begin,$end,$mod_id){            
            $row = array();            
            $sql = "SELECT id,post_title,post_alias,post_is_hot,post_views FROM t186_posts WHERE create_user = '".$mod_id."' AND post_is_chap = 0 AND post_status = 1 AND category_id like '%" . $category_id . ",%'";
            $sql .= " ORDER BY id DESC LIMIT ".$begin.",".$end;                            
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);            
            $row = $command->queryAll();                        
            return $row;
        }

        public function getAllRows(){
            $rows = array();            
            $sql = "SELECT t186_posts.id,t186_posts.post_title,t186_posts.post_alias,t186_posts.post_is_hot FROM t186_posts WHERE category_id like '%1,%' AND post_status = 1 ORDER BY id DESC";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryAll();
            return $rows;
        }

        public function getSearch($where){            
            $sql = "SELECT t186_posts.id,t186_posts.post_title,t186_posts.post_alias,t186_posts.post_is_hot,t186_posts.post_views FROM t186_posts WHERE post_status = 1 ". $where . " LIMIT 0,500";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryAll();        
            return $rows;
        }

        public function countSearch($where){            
            $sql = "SELECT COUNT(*) FROM t186_posts FROM t186_posts WHERE post_status = 1 AND " . $where;
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryScalar();        
            return $row;
        }

        public function getAllPostsHot(){
            $rows = array();            
            $sql = "SELECT t186_posts.id,post_title,post_image,post_alias,post_views,create_user,create_date FROM t186_posts WHERE post_is_hot = 1";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryAll();
            return $rows;
        }

        public function insertObject($array_input){
            $sql='';
            foreach($array_input as $key=>$value)
            {
                $sql.=$key."='".$value."',"; 
            }
            $sql='INSERT INTO t186_posts SET '.$sql;
            $sql=rtrim($sql,',');
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $command->execute();
            $record_id=Yii::app()->db->getLastInsertID();  
            return $record_id;
        }

        public function updateObject($array_input,$key_id,$key_value){
            $sql='';
            foreach($array_input as $key=>$value)
            {
                $sql.=$key."='".$value."',"; 
            }
            $sql=rtrim($sql,',');
            if($sql!='')
            {
                $sql='UPDATE t186_posts SET '.$sql.' WHERE '.$key_id.'='.$key_value;
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $a=$command->execute();
                return $a;
            }
            return -1;
        }    

        public function deleteObject($array_input){
            $sql=' 1 ';
            foreach($array_input as $key=>$value)
            {
                $sql.=" AND ".$key."='".$value."'"; 
            }
            $sql=rtrim($sql,',');
            if($sql!='')
            {
                $sql='delete from t186_posts where '.$sql.'';
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $a=$command->execute();
                if($a) return 1;
                else return 0;
            }
            else return 0;

        }

        public function insert($data){
            $connect = Yii::app()->db;
            $command = $connect->createCommand();
            $command->insert('t186_posts', $data);    
            $record_id=Yii::app()->db->getLastInsertID();  
            return $record_id;
        }

        public function update($id,$data){
            $connect = Yii::app()->db;
            $command = $connect->createCommand();
            return $command->update('t186_posts', $data,"id=".$id);        
        }

        public function delete($where){
            $connect = Yii::app()->db;
            $command = $connect->createCommand();
            return $command->delete('t186_posts', $where);        
        }

        public function getCountPostsByCategory($date){
            $rows = array();
            $sql = "SELECT COUNT(*) AS 'count',category_id,t186_category.name FROM t186_posts INNER JOIN t186_category ON t186_category.id = t186_posts.category_id WHERE update_date > $date GROUP BY category_id ORDER BY `count` DESC";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryAll();
            return $rows;
        }

        public function getCountPost($where){
            $rows = array();        
            $sql = "SELECT COUNT(*) AS 'count' FROM t186_posts WHERE " . $where;
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryScalar();
            return $rows;
        }

        public function getDataForImage($begin,$end){        
            // $cache = false;
            $row = array();            
            $sql = "SELECT t186_posts.* FROM t186_posts WHERE post_is_chap = 0 ORDER BY id DESC LIMIT ".$begin.",".$end;            
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryAll();            
            return $row;
        }

    }

?>