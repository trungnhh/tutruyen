<?php
class AdminBookmark extends CActiveRecord{
    public static function model($className = __CLASS__) {
        return parent::model ( $className );
    }

    // dat luat cho model
    public function rules(){}         

    // goi den bang can ket noi   
    public function tableName() {                  
        return 't186_bookmark';
    }

    // nhan cac thuong tinh 
    public function attributeLabels(){}

    public function getRowByUsername($username){
        //$id = intval($id);        
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("User","getRowByUsername",$username);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key );
            //echo $dependency;  
        }
        //$cache=false;
        if($cache == false){ 
            $sql = "SELECT * FROM t186_users WHERE username='".$username."'";           
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryRow();
            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }    

    public function getRowById($id){
        $id = intval($id);        
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("Category","getRowById",$id);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }
        $cache=false;
        if($cache == false){ 
            $sql = "SELECT * FROM t186_bookmark WHERE id=".$id;                      
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryRow();
            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }

    public function getRowByAlias($category_alias){        
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("Category","getRowByAlias",$category_alias);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }
        //$cache=false;
        if($cache == false){ 
            $sql = "SELECT * FROM t186_bookmark WHERE alias='".$category_alias."'";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryRow();
            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }
    
    public function getAllRows(){
        $rows = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("Category","getAllRows","");
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }
        $cache=false;
        if($cache == false){ 
            $sql = "SELECT * FROM t186_bookmark WHERE status = 1 ORDER BY order_number";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryAll();

            Yii::app ()->cache->set ( $key, $rows, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $rows = $cache;
        }
        return $rows;
    }    

    public function insertObject($array_input){
        $sql='';
        foreach($array_input as $key=>$value)
        {
            $sql.=$key."='".$value."',"; 
        }
        $sql='INSERT INTO t186_bookmark SET '.$sql;
        $sql=rtrim($sql,',');
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);
        $command->execute();
        $record_id=Yii::app()->db->getLastInsertID();  
        return $record_id;
    }

    public function updateObject($array_input,$key_id,$key_value){
        $sql='';
        foreach($array_input as $key=>$value)
        {
            $sql.=$key."='".$value."',"; 
        }
        $sql=rtrim($sql,',');
        if($sql!='')
        {
            $sql='UPDATE t186_bookmark SET '.$sql.' WHERE '.$key_id.'='.$key_value;
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $a=$command->execute();
            return $a;
        }
        return -1;
    }    

    public function deleteObject($array_input){
        $sql=' 1 ';
        foreach($array_input as $key=>$value)
        {
            $sql.=" AND ".$key."='".$value."'"; 
        }
        $sql=rtrim($sql,',');
        if($sql!='')
        {
            $sql='delete from t186_bookmark where '.$sql.'';
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $a=$command->execute();
            if($a) return 1;
            else return 0;
        }
        else return 0;

    }

    public function insert($data){
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);        
        $command->insert('t186_bookmark', $data);    
        $record_id=Yii::app()->db->getLastInsertID();  
        return $record_id;
    }
    
    public function update($id,$data){
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);                
        return $command->update('t186_bookmark', $data,"id=".$id);        
    }
    
    public function delete($where){
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);                
        return $command->delete('t186_bookmark', $where);        
    }
    
    public function getCountBookmark($where){
            $rows = array();        
            $sql = "SELECT COUNT(*) AS 'count' FROM t186_bookmark WHERE " . $where;
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryScalar();
            return $rows;
        }

}

?>