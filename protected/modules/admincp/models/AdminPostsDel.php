<?php
class AdminPostsDel extends CActiveRecord{
    public static function model($className = __CLASS__) {
        return parent::model ( $className );
    }

    // dat luat cho model
    public function rules(){}         

    // goi den bang can ket noi   
    public function tableName() {                  
        return 't186_posts_del';
    }

    // nhan cac thuong tinh 
    public function attributeLabels(){}    

    public function getRowById($id){
        $id = intval($id);
        // $cache = false;
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("Posts","getRowById",$id);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }        
        $cache = false;
        if($cache == false){ 
            $sql = "SELECT t186_posts_del.*,t186_category.name AS 'category_name' FROM t186_posts_del INNER JOIN t186_category ON t186_posts_del.category_id = t186_category.id WHERE t186_posts_del.id =".$id;                      
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryRow();
            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }

    public function getRowByAlias($post_alias){        
        // $cache = false;
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("Posts","getRowByAlias",$post_alias);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }        
        $cache = false;
        if($cache == false){ 
            $sql = "SELECT t186_posts_del.*,t186_category.name AS 'category_name' FROM t186_posts_del INNER JOIN t186_category ON t186_posts_del.category_id = t186_category.id WHERE t186_posts_del.post_alias = '".$post_alias."'";            
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryRow();
            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }

    public function getDataByCategoryId($category_id){        
        $category_id = intval($category_id);
        // $cache = false;
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("Posts","getDataByCategoryId",$category_id);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }                
        $cache = false;
        if($cache == false){             
            $sql = "SELECT id,post_title,post_alias,post_is_hot,post_views FROM t186_posts_del WHERE post_is_chap = 0 AND post_status = 1 AND category_id = " . $category_id." ORDER BY id DESC";            
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryAll();
            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }
    
    public function getPostsVipByCategoryId($category_id){        
        $category_id = intval($category_id);
        // $cache = false;
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("Posts","getPostsVipByCategoryId",$category_id);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }                
        $cache = false;
        if($cache == false){       
            if($category_id == 0){
                $sql = "SELECT t186_posts_del.id,post_title,post_alias,post_is_hot,post_views,t186_category.alias as 'category_alias' FROM t186_posts_del INNER JOIN t186_category ON t186_posts_del.category_id = t186_category.id WHERE post_status = 1 ORDER BY t186_posts_del.id DESC";
            }else{
                $sql = "SELECT t186_posts_del.id,post_title,post_alias,post_is_hot,post_views,t186_category.alias as 'category_alias' FROM t186_posts_del INNER JOIN t186_category ON t186_posts_del.category_id = t186_category.id WHERE post_status = 1 AND category_id = " . $category_id." ORDER BY t186_posts_del.id DESC";
            }            
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryAll();
            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }

    public function countDataByCategoryId($category_id){
        $category_id = intval($category_id);
        // $cache = false;
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("Posts","countDataByCategoryId",$category_id);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }                
        $cache = false;
        if($cache == false){             
            $sql = "SELECT COUNT(*) FROM t186_posts_del WHERE category_id = " . $category_id;            
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryScalar();
            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }

    public function getDataByCategoryIdPaging($category_id,$begin,$end){
        //$cache = false;
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("Posts","getDataByCategoryIdPaging",$category_id.$begin.$end);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }
        $cache=false;
        if($cache == false){             
            $sql = "SELECT id,post_title,post_alias,post_is_hot,post_views FROM t186_posts_del WHERE post_is_chap = 0 AND post_status = 1 AND category_id = " . $category_id;
            $sql .= " ORDER BY id DESC LIMIT ".$begin.",".$end;                              
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);            
            $row = $command->queryAll();            
            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }

    public function getAllRows(){
        $rows = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("Posts","getAllRows","");
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }
        $cache=false;
        if($cache == false){ 
            $sql = "SELECT t186_posts_del.id,t186_posts_del.post_title,t186_posts_del.post_alias,t186_posts_del.post_is_hot,t186_posts_del.category_id,t186_category.name AS 'category_name' FROM t186_posts_del INNER JOIN t186_category ON t186_posts_del.category_id = t186_category.id WHERE category_id = 1 AND post_status = 1 ORDER BY id DESC";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryAll();

            Yii::app ()->cache->set ( $key, $rows, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $rows = $cache;
        }
        return $rows;
    }

    public function getAllPostsHot(){
        $rows = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("Posts","getAllPostsHot","");
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }
        //$cache=false;
        if($cache == false){ 
            $sql = "SELECT t186_posts_del.id,post_title,post_image,post_alias,post_views,create_user,create_date,t186_category.alias AS 'category_alias' FROM t186_posts_del INNER JOIN t186_category ON t186_posts_del.category_id = t186_category.id WHERE post_is_hot = 1";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryAll();

            Yii::app ()->cache->set ( $key, $rows, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $rows = $cache;
        }
        return $rows;
    }

    public function insertObject($array_input){
        $sql='';
        foreach($array_input as $key=>$value)
        {
            $sql.=$key."='".$value."',"; 
        }
        $sql='INSERT INTO t186_posts_del SET '.$sql;
        $sql=rtrim($sql,',');
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);
        $command->execute();
        $record_id=Yii::app()->db->getLastInsertID();  
        return $record_id;
    }

    public function updateObject($array_input,$key_id,$key_value){
        $sql='';
        foreach($array_input as $key=>$value)
        {
            $sql.=$key."='".$value."',"; 
        }
        $sql=rtrim($sql,',');
        if($sql!='')
        {
            $sql='UPDATE t186_posts_del SET '.$sql.' WHERE '.$key_id.'='.$key_value;
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $a=$command->execute();
            return $a;
        }
        return -1;
    }    

    public function deleteObject($array_input){
        $sql=' 1 ';
        foreach($array_input as $key=>$value)
        {
            $sql.=" AND ".$key."='".$value."'"; 
        }
        $sql=rtrim($sql,',');
        if($sql!='')
        {
            $sql='delete from t186_posts_del where '.$sql.'';
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $a=$command->execute();
            if($a) return 1;
            else return 0;
        }
        else return 0;

    }

    public function insert($data){
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);        
        $command->insert('t186_posts_del', $data);    
        $record_id=Yii::app()->db->getLastInsertID();  
        return $record_id;
    }
    
    public function update($id,$data){
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);                
        return $command->update('t186_posts_del', $data,"id=".$id);        
    }
    
    public function delete($where){
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);                
        return $command->delete('t186_posts_del', $where);        
    }    

}
