<?php
class AdminRequest extends CActiveRecord{
    public static function model($className = __CLASS__) {
        return parent::model ( $className );
    }

    // dat luat cho model
    public function rules(){}         

    // goi den bang can ket noi   
    public function tableName() {                  
        return 't186_request';
    }

    // nhan cac thuong tinh 
    public function attributeLabels(){}    

    public function getRowById($id){
        $id = intval($id);        
        $row = array();        
        $sql = "SELECT t186_request.*,t186_users.username FROM t186_request INNER JOIN t186_users ON t186_request.user_id = t186_users.id WHERE t186_request.id=".$id;
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);
        $row = $command->queryRow();        
        return $row;
    }

    public function getRowByAlias($category_alias){        
        $row = array();        
        $sql = "SELECT * FROM t186_request WHERE alias='".$category_alias."'";
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);
        $row = $command->queryRow();        
        return $row;
    }

    public function getAllRows(){
        $rows = array();        
        $sql = "SELECT t186_request.*,t186_users.username FROM t186_request INNER JOIN t186_users ON t186_request.user_id = t186_users.id ORDER BY id DESC";
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);
        $rows = $command->queryAll();
        return $rows;
    }    

    public function insertObject($array_input){
        $sql='';
        foreach($array_input as $key=>$value)
        {
            $sql.=$key."='".$value."',"; 
        }
        $sql='INSERT INTO t186_request SET '.$sql;
        $sql=rtrim($sql,',');
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);
        $command->execute();
        $record_id=Yii::app()->db->getLastInsertID();  
        return $record_id;
    }

    public function updateObject($array_input,$key_id,$key_value){
        $sql='';
        foreach($array_input as $key=>$value)
        {
            $sql.=$key."='".$value."',"; 
        }
        $sql=rtrim($sql,',');
        if($sql!='')
        {
            $sql='UPDATE t186_request SET '.$sql.' WHERE '.$key_id.'='.$key_value;
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $a=$command->execute();
            return $a;
        }
        return -1;
    }    

    public function deleteObject($array_input){
        $sql=' 1 ';
        foreach($array_input as $key=>$value)
        {
            $sql.=" AND ".$key."='".$value."'"; 
        }
        $sql=rtrim($sql,',');
        if($sql!='')
        {
            $sql='delete from t186_request where '.$sql.'';
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $a=$command->execute();
            if($a) return 1;
            else return 0;
        }
        else return 0;

    }

    public function insert($data){
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);        
        $command->insert('t186_request', $data);    
        $record_id=Yii::app()->db->getLastInsertID();  
        return $record_id;
    }

    public function update($id,$data){
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);                
        return $command->update('t186_request', $data,"id=".$id);        
    }

    public function delete($where){
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);                
        return $command->delete('t186_request', $where);        
    }

}
