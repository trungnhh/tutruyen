<?php
class AdminTransactionOut extends CActiveRecord{
    public static function model($className = __CLASS__) {
        return parent::model ( $className );
    }

    // dat luat cho model
    public function rules(){}         

    // goi den bang can ket noi   
    public function tableName() {                  
        return 't186_transaction_out';
    }

    // nhan cac thuong tinh 
    public function attributeLabels(){}    
    
    public function getRowById($id){
        $id = intval($id);
        // $cache = false;
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("transaction_out","getRowById",$id);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }
        $cache=false;
        if($cache == false){ 
            $sql = "SELECT * FROM t186_transaction_out WHERE id=".$id;                      
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryRow();
            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }        
    
    public function getAllRows(){
        $rows = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("transaction_in","getAllRows","");
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }
        $cache=false;
        if($cache == false){ 
            $sql = "SELECT * FROM t186_transaction_out";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryAll();

            Yii::app ()->cache->set ( $key, $rows, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $rows = $cache;
        }
        return $rows;
    }        
    
    public function getSearch($where,$begin,$end){
        $rows = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("transaction_out","getSearch",$where);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }
        $cache=false;
        if($cache == false){ 
            $sql = "SELECT *,0 AS 'transaction_type' FROM t186_transaction_out WHERE 1 = 1 ".$where." ORDER BY t186_transaction_out.id DESC";            
            $sql .= " LIMIT ".$begin.",".$end;                                          
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryAll();

            Yii::app ()->cache->set ( $key, $rows, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $rows = $cache;
        }
        return $rows;
    }
    
    public function insert($data){
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);        
        $command->insert('t186_transaction_out', $data);    
        $record_id=Yii::app()->db->getLastInsertID();  
        return $record_id;
    }
    
    public function update($id,$data){
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);                
        return $command->update('t186_transaction_out', $data,"id=".$id);        
    }
    
}
