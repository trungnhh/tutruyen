<?php

    class AdminNews extends CActiveRecord{
        public static function model($className = __CLASS__) {
            return parent::model ( $className );
        }

        // dat luat cho model
        public function rules(){}         

        // goi den bang can ket noi   
        public function tableName() {                  
            return 't186_news';
        }

        // nhan cac thuong tinh 
        public function attributeLabels(){}    

        public function getRowById($id){
            $id = intval($id);            
            $row = array();
            $sql = "SELECT * FROM t186_news WHERE t186_news.id =".$id;                      				
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);           
            $row = $command->queryRow(); 
            return $row;
        }        

        public function getDataByCategoryId($category_id){        
            $category_id = intval($category_id);
            // $cache = false;
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","getDataByCategoryId",$category_id);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }                
            $cache = false;
            if($cache == false){             
                $sql = "SELECT id,post_title,post_alias,post_is_hot,post_views FROM t186_news WHERE post_is_chap = 0 AND post_status = 1 AND category_id = " . $category_id." ORDER BY id DESC";            
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryAll();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }                

        public function countDataByCategoryId($category_id){
            $category_id = intval($category_id);
            // $cache = false;
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","countDataByCategoryId",$category_id);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }                
            $cache = false;
            if($cache == false){             
                $sql = "SELECT COUNT(*) FROM t186_news WHERE category_id = " . $category_id;            
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryScalar();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }

        public function countDataListByCategoryId($category_id){
            $category_id = intval($category_id);
            // $cache = false;
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","countDataListByCategoryId",$category_id);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }                
            $cache = false;
            if($cache == false){             
                $sql = "SELECT COUNT(*) FROM t186_news WHERE post_is_chap = 0 AND post_status = 1 AND category_id = " . $category_id;            
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryScalar();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }

        public function getDataByCategoryIdPaging($category_id,$begin,$end){
            //$cache = false;
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","getDataByCategoryIdPaging",$category_id.$begin.$end);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }
            $cache=false;
            if($cache == false){             
                $sql = "SELECT id,post_title,post_alias,post_is_hot,post_views FROM t186_news WHERE post_is_chap = 0 AND post_status = 1 AND category_id = " . $category_id;
                $sql .= " ORDER BY id DESC LIMIT ".$begin.",".$end;                              
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);            
                $row = $command->queryAll();            
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }        

        public function getAllRows(){
            $rows = array();            
            $sql = "SELECT * FROM t186_news ORDER BY id DESC";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryAll();        
            return $rows;
        }

        public function getSearch($where){            
            $sql = "SELECT t186_news.id,t186_news.post_title,t186_news.post_alias,t186_news.post_is_hot,t186_news.category_id,t186_category.alias as 'category_alias',t186_category.name AS 'category_name' FROM t186_news INNER JOIN t186_category ON t186_news.category_id = t186_category.id WHERE post_status = 1 ".$where." ORDER BY id DESC";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryAll();        
            return $rows;
        }

        public function countSearch($where){            
            $sql = "SELECT COUNT(*) FROM t186_news FROM t186_news INNER JOIN t186_category ON t186_news.category_id = t186_category.id WHERE post_status = 1 AND " . $where;            
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryScalar();        
            return $row;
        }        

        public function insertObject($array_input){
            $sql='';
            foreach($array_input as $key=>$value)
            {
                $sql.=$key."='".$value."',"; 
            }
            $sql='INSERT INTO t186_news SET '.$sql;
            $sql=rtrim($sql,',');
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $command->execute();
            $record_id=Yii::app()->db->getLastInsertID();  
            return $record_id;
        }

        public function updateObject($array_input,$key_id,$key_value){
            $sql='';
            foreach($array_input as $key=>$value)
            {
                $sql.=$key."='".$value."',"; 
            }
            $sql=rtrim($sql,',');
            if($sql!='')
            {
                $sql='UPDATE t186_news SET '.$sql.' WHERE '.$key_id.'='.$key_value;
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $a=$command->execute();
                return $a;
            }
            return -1;
        }    

        public function deleteObject($array_input){
            $sql=' 1 ';
            foreach($array_input as $key=>$value)
            {
                $sql.=" AND ".$key."='".$value."'"; 
            }
            $sql=rtrim($sql,',');
            if($sql!='')
            {
                $sql='delete from t186_news where '.$sql.'';
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $a=$command->execute();
                if($a) return 1;
                else return 0;
            }
            else return 0;

        }

        public function insert($data){
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);        
            $command->insert('t186_news', $data);    
            $record_id=Yii::app()->db->getLastInsertID();  
            return $record_id;
        }

        public function update($id,$data){
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);                
            return $command->update('t186_news', $data,"id=".$id);        
        }

        public function delete($where){
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);                
            return $command->delete('t186_news', $where);        
        }

        public function getCountPostsByCategory(){
            $rows = array();        
            $sql = "SELECT COUNT(*) AS 'count',category_id,t186_category.name FROM t186_news INNER JOIN t186_category ON t186_category.id = t186_news.category_id GROUP BY category_id ORDER BY `count` DESC";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryAll();
            return $rows;
        }

        public function getCountPost($where){
            $rows = array();        
            $sql = "SELECT COUNT(*) AS 'count' FROM t186_news WHERE " . $where;
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryScalar();
            return $rows;
        }

        public function getDataForImage($begin,$end){        
            // $cache = false;
            $row = array();            
            $sql = "SELECT t186_news.* FROM t186_news WHERE post_is_chap = 0 ORDER BY id DESC LIMIT ".$begin.",".$end;            
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryAll();            
            return $row;
        }

    }

?>