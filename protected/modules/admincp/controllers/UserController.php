<?php

class UserController extends Controller
{
    //public $layout='column1';
    //public $defaultAction = 'login';

    public function actionIndex(){                    
        /*$data_user = User::getAllRows();                       
        $this->render('index',array('data_user'=>$data_user));*/

        $url = new Url();                               
        $page = isset($_GET['page']) ? intval($_GET ['page']):1;
        $keyword = isset($_GET['keyword']) ? $_GET ['keyword']:'';
        $rows_per_page = 20;

        $begin = ($page - 1)*$rows_per_page;                        
        $end = $rows_per_page;
        $where = "";
        if($keyword != ""){
            $where = " AND username LIKE '%".$keyword."%'";
        }        
        $count = AdminUser::getCountUser($where);            
        if($count % $rows_per_page == 0)        
        {
            $totalpage = floor($count/$rows_per_page);
        }
        else
        {
            $totalpage = floor($count/$rows_per_page) + 1;                             
        }                    

        $data_user = AdminUser::getRowsByUserPaging($where,$begin,$end);            

        $util = new Paging();                 
        $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("user/index").'/',"?keyword=".$keyword);            
        $this->render('index',array('data_user'=>$data_user,'paging'=>$paging));

    }

    public function actionDetail(){        
        $user_id = isset($_GET["user_id"])?mysql_escape_string($_GET["user_id"]):"1";            
        $data_user = AdminUser::getRowById($user_id);                    
        $this->render('detail',array('data_user'=>$data_user));
    }

    public function actionCreate(){        
        if(isset($_GET['edit']))
        {         
            $edit = 1;
            $id = $_GET['edit'];
            $model = new AdminUser();
            $data =  $model->getRowsById($id);               

            $this->render("create",array('edit'=>$edit,'data'=>$data));
        }
        else
        {
            $edit = 0;
            $this->render("create",array('edit'=>$edit));   
        }        
    }

    public function actionSubmitDelete(){                            
        $id = $_POST['id'];                                
        $model = new AdminUser();
        $data = $model->deleteRowById($id);
        if(count($data)==0)
        {
            echo 0;
        }
        else
        {
            echo 1;
        }
    }    

    public function actionSubmitInsert(){   
        $username = $_POST['username'];                                
        $fullname = $_POST['fullname'];                                
        $email = $_POST['email'];               
        $mobile = $_POST['mobile'];               
        $address = $_POST['address'];               
        $xu = $_POST['xu'];                               
        $password = md5($_POST['password']);        
        $model = new AdminUser();
        $check =  $model->getRowsByTitleUser($username);
        if(count($check)==1)
        {
            echo 2;die;
        }
        $data = $model->insertRow($username,$fullname,$email,$mobile,$address,$xu,$password);        
        if(count($data)==0)
        {
            echo 0;
        }
        else
        {
            echo 1;
        }
    }    

    public function actionAjaxChangeStatus(){          
        $url = new Url();                          
        $user_id = isset($_POST["user_id"])?mysql_escape_string($_POST["user_id"]):"";
        $data_update['status'] = isset($_POST["status"])?mysql_escape_string($_POST["status"]):"";        
        echo User::update($user_id,$data_update);
    }

    public function actionAjaxEditUser(){                    
        $user_id = isset($_POST["user_id"])?mysql_escape_string($_POST["user_id"]):"";
        $data_update['fullname'] = isset($_POST["fullname"])?$_POST["fullname"]:"";            
        $data_update['mobile'] = isset($_POST["mobile"])?mysql_escape_string($_POST["mobile"]):"";
        $data_update['email'] = isset($_POST["email"])?mysql_escape_string($_POST["email"]):"";
        $data_update['funds'] = isset($_POST["funds"])?mysql_escape_string($_POST["funds"]):"";            
        echo User::update($user_id,$data_update);
    }

    public function actionAjaxResetPasswordUser(){                    
        $user_id = isset($_POST["user_id"])?mysql_escape_string($_POST["user_id"]):"";    
        $default_password = "123456";
        $data_update['password'] = User::getMd5($default_password);
        echo User::update($user_id,$data_update);
    }        

}
