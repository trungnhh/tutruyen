<?php

class RequestController extends Controller
{
    //public $layout='column1';	

    /**
    * Declares class-based actions.
    */
    public function actions(){

    }

    /**
    * This is the action to handle external exceptions.
    */
    public function actionError(){
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
    * Displays the login page
    */
    public function actionIndex(){		        
        $data_request = AdminRequest::getAllRows();
	    $data_category = AdminCategory::getAllRows();
        $this->render('index',array('data_request'=>$data_request,'data_category'=>$data_category));
    }

    public function actionAdd(){             
        $data_category = AdminCategory::getAllRows();
        $this->render('add',array('data_category'=>$data_category));
    }

    public function actionDetail(){        
        $request_id = isset($_GET["id"])?mysql_escape_string($_GET["id"]):"1";
        $data_request = AdminRequest::getRowById($request_id);
	    $data_category = AdminCategory::getAllRows();
        $this->render('detail',array('data_request'=>$data_request,'data_category'=>$data_category));
    }

    public function actionAjaxChangeStatus(){          
        $url = new Url();                          
        $request_id = isset($_POST["request_id"])?mysql_escape_string($_POST["request_id"]):"";
        $data_update['status'] = isset($_POST["status"])?mysql_escape_string($_POST["status"]):"";
        echo AdminRequest::update($request_id,$data_update);
    }

    public function actionAjaxEditRequest(){
        $url = new Url();     
        $news_id = isset($_POST["news_id"])?mysql_escape_string($_POST["news_id"]):"";
        $data_insert['news_title'] = isset($_POST["news_title"])?mysql_escape_string($_POST["news_title"]):"";                
        $data_insert['news_content'] = isset($_POST["news_content"])?$_POST["news_content"]:"";        
        echo AdminNews::update($news_id,$data_insert);
    }

    public function actionAjaxDeleteRequest(){
        $url = new Url();                          
        $author_id = isset($_POST["author_id"])?mysql_escape_string($_POST["author_id"]):"";                                
        echo AdminAuthor::delete("author_id = " . $author_id);
    }

}
