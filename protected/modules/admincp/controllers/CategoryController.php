<?php

class CategoryController extends Controller
{
	//public $layout='column1';	

	/**
	 * Declares class-based actions.
	 */
	public function actions(){
		
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError(){
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the login page
	 */
	public function actionIndex(){		
        $data_category = AdminCategory::getAllRows();        
		$this->render('index',array('data_category'=>$data_category));
	}
	
    public function actionDetail(){        
        $category_id = isset($_GET["category_id"])?mysql_escape_string($_GET["category_id"]):"1";
        $data_category = AdminCategory::getRowById($category_id);
        $this->render('detail',array('data_category'=>$data_category));
    }
    
    public function actionAjaxChangeStatus(){          
        $url = new Url();                          
        $category_id = isset($_POST["category_id"])?mysql_escape_string($_POST["category_id"]):"";
	    $change_status = isset($_POST["change_status"])?mysql_escape_string($_POST["change_status"]):1;
	    if($change_status == 1){
		    $data_update['status'] = isset($_POST["status"])?mysql_escape_string($_POST["status"]):1;
	    }else{
		    $data_update['is_hot'] = isset($_POST["is_hot_status"])?mysql_escape_string($_POST["is_hot_status"]):"";
	    }
        echo AdminCategory::update($category_id,$data_update);
    }
    
    public function actionAjaxAddCategory(){                  
        $url = new Url();                          
        $data_insert['name'] = isset($_POST["category_name"])?mysql_escape_string($_POST["category_name"]):"";
        $alias = StringUtils::removeTitle($data_insert['name']);        
        $data_insert['alias'] = strtolower($alias);
        $data_insert['order_number'] = isset($_POST["category_order"])?mysql_escape_string($_POST["category_order"]):"";
        
        echo AdminCategory::insert($data_insert);
    }
    
    public function actionAjaxEditCategory(){                  
        $url = new Url();     
        $category_id = isset($_POST["category_id"])?mysql_escape_string($_POST["category_id"]):"";
        $data_insert['name'] = isset($_POST["category_name"])?mysql_escape_string($_POST["category_name"]):"";
        $alias = StringUtils::removeTitle($data_insert['name']);        
        $data_insert['alias'] = strtolower($alias);
        $data_insert['order_number'] = isset($_POST["category_order"])?mysql_escape_string($_POST["category_order"]):"";
        $data_insert['is_hot'] = isset($_POST["category_is_hot"])?mysql_escape_string($_POST["category_is_hot"]):"";
        
        echo AdminCategory::update($category_id,$data_insert);
    }
    
    public function actionAjaxDeleteCategory(){                  
        $url = new Url();                          
        $category_id = isset($_POST["category_id"])?mysql_escape_string($_POST["category_id"]):"";                
        $data_update['status'] = 0;
        echo AdminCategory::update($category_id,$data_update);
    }
    
}
