<?php

class MessagesController extends Controller
{
    //public $layout='column1';	

    /**
    * Declares class-based actions.
    */
    public function actions(){

    }

    /**
    * This is the action to handle external exceptions.
    */
    public function actionError(){
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
    * Displays the login page
    */
    public function actionIndex(){		        
        $data_messages = AdminMessages::getAllRows();
        $this->render('index',array('data_messages'=>$data_messages));
    }

    public function actionAdd(){
	    $user_id = isset($_GET["user_id"])?mysql_escape_string($_GET["user_id"]):"0";
	    $data_user = array();
	    if($user_id != 0){
		    $data_user = AdminUser::getRowById($user_id);
	    }
        $this->render('add',array('data_user'=>$data_user));
    }

    public function actionDetail(){        
        $request_id = isset($_GET["id"])?mysql_escape_string($_GET["id"]):"1";
        $data_request = AdminRequest::getRowById($request_id);
	    $data_category = AdminCategory::getAllRows();
        $this->render('detail',array('data_request'=>$data_request,'data_category'=>$data_category));
    }

    public function actionAjaxChangeStatus(){          
        $url = new Url();                          
        $category_id = isset($_POST["category_id"])?mysql_escape_string($_POST["category_id"]):"";
        $data_update['is_hot'] = isset($_POST["is_hot_status"])?mysql_escape_string($_POST["is_hot_status"]):"";        
        echo AdminCategory::update($category_id,$data_update);
    }

    public function actionAjaxAddMessage(){
        $url = new Url();                          
        $data_insert['title'] = isset($_POST["title"])?mysql_escape_string($_POST["title"]):"";
        $data_insert['content'] = isset($_POST["content"])?mysql_escape_string($_POST["content"]):"";
        $data_insert['sender'] = isset($_POST["sender"])?$_POST["sender"]:"";
        $data_insert['receiver'] = isset($_POST["receiver"])?$_POST["receiver"]:"";
        $data_insert['type'] = isset($_POST["type"])?$_POST["type"]:0;
        $data_insert['create_date'] = time();
        echo AdminMessages::insert($data_insert);
    }

    public function actionAjaxEditMessage(){
        $url = new Url();     
        $news_id = isset($_POST["news_id"])?mysql_escape_string($_POST["news_id"]):"";
        $data_insert['news_title'] = isset($_POST["news_title"])?mysql_escape_string($_POST["content"]):"";
        $data_insert['news_content'] = isset($_POST["news_content"])?$_POST["news_content"]:"";        
        echo AdminNews::update($news_id,$data_insert);
    }

    public function actionAjaxDeleteMessage(){
        $url = new Url();                          
        $author_id = isset($_POST["author_id"])?mysql_escape_string($_POST["author_id"]):"";                                
        echo AdminAuthor::delete("author_id = " . $author_id);
    }

}
