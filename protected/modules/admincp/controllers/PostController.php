<?php

    class PostController extends Controller{
        //public $layout='column1';	

        /**
        * Declares class-based actions.
        */
        public function actions(){

        }

        /**
        * This is the action to handle external exceptions.
        */
        public function actionError(){
            if($error=Yii::app()->errorHandler->error)
            {
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->render('error', $error);
            }
        }

        public function actionIndex(){		
            $category_id = isset($_GET["category_id"])?mysql_escape_string($_GET["category_id"]):"3";
            $data_category = AdminCategory::getAllRows();        
            if($category_id != 100 && $category_id != 101 && $category_id != 102 && $category_id != 103 && $category_id != 104 && $category_id != 105){            

                if($category_id == 106 || $category_id == 107 || $category_id == 108){                                                
                    $begin = ($category_id - 106) * 500;
                    $end = 500;
                    $data_post = AdminPosts::getDataByCategoryIdPaging(8,$begin,$end);
                }else{                                                             
                    $data_post = AdminPosts::getDataByCategoryId($category_id);        
                }

            }else{            
                $begin = ($category_id - 100) * 800;
                $end = 800;
                $data_post = AdminPosts::getDataByCategoryIdPaging(13,$begin,$end);
            }
            $this->render('index',array('data_post'=>$data_post,'data_category'=>$data_category,'category_id'=>$category_id));
        }

        public function actionList(){
            $url = new Url();                   
            $category_id = isset($_GET["category_id"])?mysql_escape_string($_GET["category_id"]):"3";
            $page = isset($_GET['page']) ? intval($_GET ['page']):1;
            $rows_per_page = 10;
            $begin = ($page - 1)*$rows_per_page;                        
            $end = $rows_per_page;
            $count = AdminPosts::countDataListByCategoryId($category_id);
            if($count % $rows_per_page == 0)        
            {
                $totalpage = floor($count/$rows_per_page);
            }
            else
            {
                $totalpage = floor($count/$rows_per_page) + 1;                             
            }                    

            $data_post = AdminPosts::getDataByCategoryIdPaging($category_id,$begin,$end);                                                                               

            $util = new Paging();                 
            $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("post/list",array("category_id"=>$category_id)).'/',"");

            $data_category = AdminCategory::getAllRows();                                
            /*$data_post = AdminPosts::getDataByCategoryId($category_id);*/
            $this->render('list',array('data_post'=>$data_post,'data_category'=>$data_category,'category_id'=>$category_id,'paging'=>$paging));
        }

        public function actionSearch(){        
            $url = new Url();                   
            $category_id = isset($_GET["category_id"])?mysql_escape_string($_GET["category_id"]):"0";        
            $keyword = isset($_GET["keyword"])?mysql_escape_string($_GET["keyword"]):"null";
            $is_vip = isset($_GET["is_vip"])?mysql_escape_string($_GET["is_vip"]):"";        
            $category_id = isset($_GET["category_id"])?mysql_escape_string($_GET["category_id"]):"";
            $author_id = isset($_GET["author_id"])?mysql_escape_string($_GET["author_id"]):"";
            $create_user = isset($_GET["create_user"])?mysql_escape_string($_GET["create_user"]):"";
            $is_full = isset($_GET["is_full"])?mysql_escape_string($_GET["is_full"]):"";
            $post_parent = isset($_GET["post_parent"])?mysql_escape_string($_GET["post_parent"]):1;
            $post_date = isset($_GET["post_date"])?mysql_escape_string($_GET["post_date"]):"";
            $post_sort = isset($_GET["post_sort"])?mysql_escape_string($_GET["post_sort"]):1;
            /*$count = AdminPosts::countDataByCategoryId($category_id);*/
	        $where = "";
            if($keyword != ""){
                $where .= " AND post_title LIKE '%".$keyword."%'";
            }
	        if($category_id != ""){
		        $where .= " AND category_id = ".$category_id;
	        }
            if($is_full == 1){
                $where .= " AND post_is_full = 1";
            }
            if($is_full == 2){
                $where .= " AND post_is_full = 0";
            }
	        if($post_parent == 1){
		        $where .= " AND post_is_chap = 0";
	        }
	        if($post_parent == 2){
		        $where .= " AND post_is_chap = 1";
	        }
	        if($post_date != ""){
		        $start_date = strtotime($post_date);
		        $end_date = strtotime("+1 days", $start_date);
		        $where .= " AND create_date > " . $post_date . " AND create_date < " . $end_date;
	        }
	        if($author_id != ""){
		        $where .= " AND author_id = " . $author_id;
	        }
	        if($create_user != ""){
		        $where .= " AND create_user = " . $create_user;
	        }
	        if($post_sort == 1){
		        $where .= " ORDER BY id DESC";
	        }else{
		        $where .= " ORDER BY post_views DESC";
	        }
            if($keyword != "null"){
                $data_post = AdminPosts::getSearch($where);            
            }else{
                $data_post = array();                
            }                        
            //var_dump($data_post);die;
            $data_category = AdminCategory::getAllRows();
	        $data_author = AdminAuthor::getAllRows();
	        $data_mod = Admin::getAllRows();
            $this->render('search',array('data_post'=>$data_post,'data_category'=>$data_category,'post_parent'=>$post_parent,'data_author'=>$data_author,'data_mod'=>$data_mod));
        }

        public function actionMod(){        
            $category_id = isset($_GET["category_id"])?mysql_escape_string($_GET["category_id"]):"3";
            $mod_id = Yii::app()->user->id;             
            $data_category = AdminCategory::getAllRows();        
            if($category_id != 100 && $category_id != 101 && $category_id != 102 && $category_id != 103 && $category_id != 104 && $category_id != 105){                            
                if($category_id == 106 || $category_id == 107 || $category_id == 108){                           
                    $begin = ($category_id - 106) * 500;
                    $end = 500;
                    $data_post = AdminPosts::getDataByModCategoryIdPaging(8,$begin,$end,$mod_id);                    
                }else{                                                                                 
                    $data_post = AdminPosts::getDataByModCategoryId($category_id,$mod_id);        
                }

            }else{            
                $begin = ($category_id - 100) * 800;
                $end = 800;
                $data_post = AdminPosts::getDataByModCategoryIdPaging(13,$begin,$end,$mod_id);
            }            
            $this->render('mod',array('data_post'=>$data_post,'data_category'=>$data_category,'category_id'=>$category_id));
        }

        public function actionChap(){        
            $category_id = isset($_GET["category_id"])?mysql_escape_string($_GET["category_id"]):"5";
            $data_category = AdminCategory::getAllRows();        
            $data_post = AdminPosts::getChapByCategoryId($category_id);        
            $this->render('chap',array('data_post'=>$data_post,'data_category'=>$data_category,'category_id'=>$category_id));
        }

        public function actionVip(){
            $category_id = isset($_GET["category_id"])?mysql_escape_string($_GET["category_id"]):"0";
            $data_category = AdminCategory::getAllRows();        
            $data_post = AdminPosts::getPostsVipByCategoryId($category_id);
            $this->render('vip',array('data_post'=>$data_post,'data_category'=>$data_category,'category_id'=>$category_id));
        }

        public function actionFeatured(){
            $category_id = isset($_GET["category_id"])?mysql_escape_string($_GET["category_id"]):"0";
            $data_category = AdminCategory::getAllRows();        
            $data_post = AdminPosts::getPostsFeaturedByCategoryId($category_id);
            $this->render('featured',array('data_post'=>$data_post,'data_category'=>$data_category,'category_id'=>$category_id));
        }

	    public function actionHot(){
		    $category_id = isset($_GET["category_id"])?mysql_escape_string($_GET["category_id"]):"0";
		    $data_category = AdminCategory::getAllRows();
		    $data_post = AdminPosts::getPostsHotByCategoryId($category_id);
		    $this->render('hot',array('data_post'=>$data_post,'data_category'=>$data_category,'category_id'=>$category_id));
	    }

        public function actionGoing(){
            $category_id = isset($_GET["category_id"])?mysql_escape_string($_GET["category_id"]):"5";
            $data_category = AdminCategory::getAllRows();        
            $data_post = AdminPosts::getPostsGoing();
            $this->render('going',array('data_post'=>$data_post,'data_category'=>$data_category));
        }

        public function actionAdd(){             
            $data_category = AdminCategory::getAllRows();
            $data_author = AdminAuthor::getAllRows();
            $this->render('add',array('data_category'=>$data_category,'data_author'=>$data_author));
        }

        public function actionAddChap(){        
            $parent_post_id = isset($_GET["parent_post_id"])?mysql_escape_string($_GET["parent_post_id"]):"1";
            $category_id = isset($_GET["category_id"])?mysql_escape_string($_GET["category_id"]):"1";
            $data_category = AdminCategory::getAllRows();
            $data_parent_post = AdminPosts::getRowById($parent_post_id);
            $this->render('add_chap',array('data_category'=>$data_category,'parent_post_id'=>$parent_post_id,'category_id'=>$category_id,'data_parent_post'=>$data_parent_post));
        }

        public function actionDetail(){        
            $post_id = isset($_GET["post_id"])?mysql_escape_string($_GET["post_id"]):"1";
            $data_category = AdminCategory::getAllRows();
            $data_author = AdminAuthor::getAllRows();
            $data_post = AdminPosts::getRowById($post_id);

            $data_chap = AdminPostsLink::getRowByParentPostId($post_id);
	        $data_post_chap = AdminPostsLink::getRowByPostId($post_id);

            $this->render('detail',array('data_post'=>$data_post,'data_category'=>$data_category,'data_chap'=>$data_chap,'data_author'=>$data_author,'data_post_chap'=>$data_post_chap));
        }

        public function actionAjaxChangeStatus(){          
            $url = new Url();                          
            $post_id = isset($_POST["post_id"])?mysql_escape_string($_POST["post_id"]):"";
            $data_update['post_is_hot'] = isset($_POST["is_hot_status"])?mysql_escape_string($_POST["is_hot_status"]):"";        
            echo AdminPosts::update($post_id,$data_update);    
        }

        public function actionAjaxChangeFull(){
            $url = new Url();                          
            $post_id = isset($_POST["post_id"])?mysql_escape_string($_POST["post_id"]):"";
            $data_update['post_is_full'] = 0;
            $data_update['post_end'] = 1;
            echo AdminPosts::update($post_id,$data_update);
        }

        public function actionAjaxAddPost(){
            $url = new Url();               
            $data_insert['post_title'] = isset($_POST["post_title"])?$_POST["post_title"]:"";
            $alias = StringUtils::removeTitle($data_insert['post_title']);
            $data_insert['post_alias'] = strtolower($alias);
            $data_insert['post_content'] = isset($_POST["post_content"])?mysql_escape_string($_POST["post_content"]):"";
            $data_insert['post_description'] = isset($_POST["post_description"])?mysql_escape_string($_POST["post_description"]):"";
            $data_insert['category_id'] = isset($_POST["category_id"])?mysql_escape_string(implode(',',$_POST["category_id"]).','):"";
            $data_insert['post_views'] = isset($_POST["post_views"])?mysql_escape_string($_POST["post_views"]):0;            
            $data_insert['post_cost'] = isset($_POST["post_cost"])?mysql_escape_string($_POST["post_cost"]):0;
            $data_insert['post_is_full'] = isset($_POST["post_is_full"])?mysql_escape_string($_POST["post_is_full"]):0;
            $data_insert['post_end'] = isset($_POST["post_end"])?mysql_escape_string($_POST["post_end"]):0;
            $data_insert['post_featured'] = isset($_POST["post_is_featured"])?mysql_escape_string($_POST["post_is_featured"]):0;            
            $data_insert['post_image'] = isset($_POST["post_image"])?$_POST["post_image"]:"";
            $data_insert['author_id'] = isset($_POST["author_id"])?$_POST["author_id"]:0;
            $data_insert['create_user'] = Yii::app()->user->id;
            $data_insert['create_date'] = time();
            $data_insert['update_date'] = time();

            $content = $data_insert['post_content'];        
            $content = str_replace('\n','',$content);
            $content = str_replace('\"','"',$content);        

            $title = $data_insert['post_title'];                
            $title = str_replace('\"','"',$title);        

            $data_insert['post_title'] = $title;
            $data_insert['post_content'] = $content;        

            $data_check = AdminPosts::getRowByAlias($data_insert['post_alias']);            

            if($data_check == null){
                $chap_id = AdminPosts::insert($data_insert);                
            }else{            
                if($data_check['category_id'] == $data_insert['category_id']){
                    echo -1;die;                    
                }else{
                    $chap_id = AdminPosts::insert($data_insert);                
                }
            }
            echo 1;die;
        }

        public function actionAjaxAddPostChap(){                  
            $url = new Url();               
            $parent_post_id = isset($_POST["parent_post_id"])?mysql_escape_string($_POST["parent_post_id"]):"";
            $order = isset($_POST["order"])?mysql_escape_string($_POST["order"]):"";
            $data_insert['post_title'] = isset($_POST["post_title"])?$_POST["post_title"]:"";
            $alias = StringUtils::removeTitle($data_insert['post_title']);                
            $data_insert['post_alias'] = strtolower($alias);
            $data_insert['post_is_chap'] = 1;
            $data_insert['post_content'] = isset($_POST["post_content"])?mysql_escape_string($_POST["post_content"]):"";
            $data_insert['category_id'] = isset($_POST["category_id"])?mysql_escape_string(implode(',',$_POST["category_id"]).','):"";
            $data_insert['post_views'] = isset($_POST["post_views"])?mysql_escape_string($_POST["post_views"]):0;
            $data_insert['post_cost'] = isset($_POST["post_cost"])?mysql_escape_string($_POST["post_cost"]):0;
	        $data_insert['author_id'] = isset($_POST["author_id"])?$_POST["author_id"]:0;
            $data_insert['create_user'] = Yii::app()->user->id;
            $data_insert['create_date'] = time();
            $data_insert['update_date'] = time();


            $content = $data_insert['post_content'];        
            $content = str_replace('\n','',$content);
            $content = str_replace('\"','"',$content);        

            $title = $data_insert['post_title'];                
            $title = str_replace('\"','"',$title);        

            $data_insert['post_title'] = $title;
            $data_insert['post_content'] = $content;

            $data_check = AdminPosts::getRowByAlias($data_insert['post_alias']);

            if($data_check == null){
                $chap_id = AdminPosts::insert($data_insert);                
            }else{            
                if($data_check['category_id'] == $data_insert['category_id']){
                    echo -1;die;                    
                }else{
                    $chap_id = AdminPosts::insert($data_insert);                
                }
            }        

            $data_parent_update['update_date'] = time();
            AdminPosts::update($parent_post_id,$data_parent_update);

            $data_insert_chap['post_parent'] = $parent_post_id;
            $data_insert_chap['post_id'] = $chap_id;
            $data_insert_chap['order'] = $order;
            echo AdminPostsLink::insert($data_insert_chap);       
        }

        public function actionAjaxAddPostChapLink(){                  
            $url = new Url();                               
            $data_insert_chap['post_parent'] = isset($_POST["parent_post_id"])?mysql_escape_string($_POST["parent_post_id"]):"";
            $data_insert_chap['post_id'] = isset($_POST["post_id"])?mysql_escape_string($_POST["post_id"]):"";
            $data_insert_chap['order'] = isset($_POST["order"])?mysql_escape_string($_POST["order"]):"";
            $keep_post = isset($_POST["keep_post"])?$_POST["keep_post"]:0;
	        $data_update['post_is_chap'] = 1;
	        if($keep_post == 1){
		        $data_update['post_is_chap'] = 0;
	        }
	        AdminPosts::update($data_insert_chap['post_id'],$data_update);
            echo AdminPostsLink::insert($data_insert_chap);
        }

        public function actionAjaxEditPost(){
            $post_id = isset($_POST["post_id"])?mysql_escape_string($_POST["post_id"]):"";
            $data_insert['post_title'] = isset($_POST["post_title"])?$_POST["post_title"]:"";
            $alias = StringUtils::removeTitle($data_insert['post_title']);                
            $data_insert['post_alias'] = strtolower($alias);
            $data_insert['post_content'] = isset($_POST["post_content"])?mysql_escape_string($_POST["post_content"]):"";            
            $data_insert['post_image'] = isset($_POST["post_image"])?$_POST["post_image"]:"";
            $data_insert['post_description'] = isset($_POST["post_description"])?mysql_escape_string($_POST["post_description"]):"";            
            $data_insert['post_cost'] = isset($_POST["post_cost"])?mysql_escape_string($_POST["post_cost"]):0;
            $data_insert['post_views'] = isset($_POST["post_views"])?mysql_escape_string($_POST["post_views"]):"";
            $data_insert['post_is_full'] = isset($_POST["post_is_full"])?mysql_escape_string($_POST["post_is_full"]):"";
            $data_insert['post_end'] = isset($_POST["post_end"])?mysql_escape_string($_POST["post_end"]):0;
            $data_insert['post_status'] = isset($_POST["post_status"])?mysql_escape_string($_POST["post_status"]):1;
            $data_insert['post_featured'] = isset($_POST["post_is_featured"])?mysql_escape_string($_POST["post_is_featured"]):"";
            $data_insert['update_date'] = time();

	        $post_order = isset($_POST["post_order"])?mysql_escape_string($_POST["post_order"]):"";
	        $post_link_id = isset($_POST["post_link_id"])?mysql_escape_string($_POST["post_link_id"]):"";

            if($_POST["category_id"] != 0){                
                $data_insert['category_id'] = isset($_POST["category_id"])?mysql_escape_string(implode(',',$_POST["category_id"]).','):"";
            }
            if($_POST["author_id"] != 0){                
                $data_insert['author_id'] = isset($_POST["author_id"])?mysql_escape_string($_POST["author_id"]):"";
            }
            if($_POST["post_image"] != 0){                
                $data_insert['post_image'] = isset($_POST["post_image"])?mysql_escape_string($_POST["post_image"]):"";
            }

            $content = $data_insert['post_content'];        
            $content = str_replace('\n','',$content);
            $content = str_replace('\"','"',$content);        

            $title = $data_insert['post_title'];                
            $title = str_replace('\"','"',$title);            

            $data_insert['post_title'] = $title;
            $data_insert['post_content'] = $content;            

			$data_child_post = PostsLink::getRowByParentPostId($post_id);
	        if($data_child_post != null){
		        $data_update['category_id'] = $data_insert['category_id'];
		        $data_update['author_id'] = $data_insert['author_id'];
		        foreach($data_child_post as $row){
			        $post_child_id = $row['post_id'];
			        AdminPosts::update($post_child_id,$data_update);
		        }

	        }

	        if($post_order != ''){
		        $data_post_chap_update['order'] = $post_order;
		        AdminPostsLink::update($post_link_id,$data_post_chap_update);
	        }

            echo AdminPosts::update($post_id,$data_insert);
        }

        public function actionAjaxDeletePost(){                  
            $url = new Url();                          
            $post_id = isset($_POST["post_id"])?mysql_escape_string($_POST["post_id"]):"";                
            $data_post = AdminPosts::getRowForDel($post_id);
            $data_post['create_date'] = time();
            AdminPostsDel::insert($data_post);
            /*$data_update['post_status'] = 0;
            echo AdminPosts::update($post_id,$data_update);*/
            echo AdminPosts::delete("id=".$post_id);
        }

        public function actionAjaxDeletePostLink(){
            $post_link_id = isset($_POST["post_link_id"])?mysql_escape_string($_POST["post_link_id"]):"";
            $data_post_link = AdminPostsLink::getRowById($post_link_id);
            $post_id = $data_post_link['post_id'];
            $data_post = AdminPosts::getRowForDel($post_id);
            $data_post['create_date'] = time();
            AdminPostsDel::insert($data_post);        
            AdminPosts::delete("id=".$post_id);
            echo AdminPostsLink::delete("id=".$post_link_id);
        }

    }

?>