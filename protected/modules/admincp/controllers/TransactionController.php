<?php

class TransactionController extends Controller
{
    //public $layout='column1';
    //public $defaultAction = 'login';

    public function actionIndex(){      
        $url = new Url();                        
        $type_id = isset($_GET["type_id"])?mysql_escape_string($_GET["type_id"]):"1";
        $keyword = isset($_GET["keyword"])?$_GET["keyword"]:"";
        $page = isset($_GET['page']) ? intval($_GET ['page']):1;
        $rows_per_page = 10;
        $have_content = 0;
        $begin = ($page - 1)*$rows_per_page;                        
        $end = $rows_per_page;
        $where = "";
        if($keyword != ''){
            $where = " AND username LIKE '%".$keyword."%'";            
        }
        switch($type_id){
            case 0:
                $in     = AdminTransactionIn::getSearch($where,$begin,$end);
                $out    = AdminTransactionOut::getSearch($where,$begin,$end);
                $data = $in;
                $count = count($in);
                foreach($out as $key=>$value){          
                    $data[$count] = $value; 
                    $count++;  
                }
                $count_data = count($data);
                for($i = 0; $i < $count_data; $i++){
                    for($j = 0; $j < $count_data - 1 -$i; $j++){
                        if($data[$j+1]['transaction_date'] > $data[$j]['transaction_date']){
                            $tmp1 = $data[$j];
                            $tmp2 = $data[$j+1];
                            $data[$j] = $tmp2;
                            $data[$j+1] = $tmp1;                                     
                        }
                    }
                }
                break;
            case 1:
                $table = "t186_transaction_in";
                $count = AdminTransactionIn::countDataSearch($table,$where);
                $data = AdminTransactionIn::getSearch($where,$begin,$end);
                break;
            case 2:
                $table = "t186_transaction_out";
                $count = AdminTransactionIn::countDataSearch($table,$where);                    
                $data = AdminTransactionOut::getSearch($where,$begin,$end);
                break;
            default:
                break;
        }

        if($count % $rows_per_page == 0)        
        {
            $totalpage = floor($count/$rows_per_page);
        }
        else
        {
            $totalpage = floor($count/$rows_per_page) + 1;                             
        }                            
        $util = new Paging();                         
        if($keyword != ''){
            $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("transaction/index",array("type_id"=>$type_id,"keyword"=>$keyword)).'/',"");                    
        }else{
            $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("transaction/index",array("type_id"=>$type_id)).'/',"");                        
        }
        
        $this->render('index',array('data'=>$data,'paging'=>$paging));
    }    

}
