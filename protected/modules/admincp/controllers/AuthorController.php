<?php

class AuthorController extends Controller
{
    //public $layout='column1';	

    /**
    * Declares class-based actions.
    */
    public function actions(){

    }

    /**
    * This is the action to handle external exceptions.
    */
    public function actionError(){
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
    * Displays the login page
    */
    public function actionIndex(){		
        $data_author = AdminAuthor::getAllRows();          
        $this->render('index',array('data_author'=>$data_author));
    }

    public function actionAdd(){             
        $data_category = AdminCategory::getAllRows();
        $this->render('add',array('data_category'=>$data_category));
    }

    public function actionDetail(){        
        $author_id = isset($_GET["author_id"])?mysql_escape_string($_GET["author_id"]):"1";
        $data_author = AdminAuthor::getRowById($author_id);
        $this->render('detail',array('data_author'=>$data_author));
    }

    public function actionAjaxChangeStatus(){          
        $url = new Url();                          
        $category_id = isset($_POST["category_id"])?mysql_escape_string($_POST["category_id"]):"";
        $data_update['is_hot'] = isset($_POST["is_hot_status"])?mysql_escape_string($_POST["is_hot_status"]):"";        
        echo AdminCategory::update($category_id,$data_update);
    }

    public function actionAjaxAddAuthor(){                  
        $url = new Url();                          
        $data_insert['author_name'] = isset($_POST["author_name"])?mysql_escape_string($_POST["author_name"]):"";        
        $data_insert['author_image'] = isset($_POST["author_image"])?mysql_escape_string($_POST["author_image"]):"";
        $data_insert['author_description'] = isset($_POST["author_content"])?$_POST["author_content"]:"";

        echo AdminAuthor::insert($data_insert);
    }

    public function actionAjaxEditAuthor(){                  
        $url = new Url();     
        $author_id = isset($_POST["author_id"])?mysql_escape_string($_POST["author_id"]):"";
        $data_insert['author_name'] = isset($_POST["author_name"])?mysql_escape_string($_POST["author_name"]):"";        
        $data_insert['author_image'] = isset($_POST["author_image"])?mysql_escape_string($_POST["author_image"]):"";
	    $alias = StringUtils::removeTitle($data_insert['author_name']);
	    $data_insert['author_alias'] = strtolower($alias);
        $data_insert['author_description'] = isset($_POST["author_content"])?$_POST["author_content"]:"";

        echo AdminAuthor::update($author_id,$data_insert);
    }

    public function actionAjaxDeleteAuthor(){                  
        $url = new Url();                          
        $author_id = isset($_POST["author_id"])?mysql_escape_string($_POST["author_id"]):"";                                
        echo AdminAuthor::delete("author_id = " . $author_id);
    }

}
