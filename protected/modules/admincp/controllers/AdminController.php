<?php

    class AdminController extends Controller{
        //public $layout='column1';
        //public $defaultAction = 'login';

        public function actionIndex(){    
            $data_admin = Admin::getAllMod();
            $this->render('index',array('data_admin'=>$data_admin));
        }

        public function actionDetail(){        
            $admin_id = isset($_GET["admin_id"])?mysql_escape_string($_GET["admin_id"]):"1";
            $data_admin = Admin::getRowsById($admin_id);
            $this->render('detail',array('data_admin'=>$data_admin));
        }

	    public function actionReport(){
		    $admin_id = isset($_GET["admin_id"])?mysql_escape_string($_GET["admin_id"]):"1";
		    $data_admin = Admin::getRowsById($admin_id);
		    $a = 0;
		    for($i=13;$i>=0;$i--){
			    $str_date = '- '.$i.' day';
			    $date = date('Y-m-d', strtotime($str_date));
			    $count_post_by_day = AdminPosts::countPostsByMod($date,$admin_id);
			    $data_chart_post[$a]['date'] = $date;
			    $data_chart_post[$a]['count'] = $count_post_by_day;
			    $a++;
		    }

		    $where = " AND post_is_chap = 0 AND create_user = " . $admin_id;
		    $data_post = AdminPosts::getSearch($where);

		    $this->render('report',array('data_admin'=>$data_admin,'data_chart_post'=>$data_chart_post,'data_post'=>$data_post));
	    }

        public function actionProfile(){        
            $admin_id = Yii::app()->user->id;
            $data_admin = Admin::getRowsById($admin_id);
            $this->render('profile',array('data_admin'=>$data_admin));
        }

        public function actionAjaxChangeStatus(){          
            $url = new Url();                          
            $admin_id = isset($_POST["admin_id"])?mysql_escape_string($_POST["admin_id"]):"";
            $data_update['status'] = isset($_POST["status"])?mysql_escape_string($_POST["status"]):"";        
            echo Admin::update($admin_id,$data_update);
        }

        public function actionAjaxDelete(){                  
            $url = new Url();                          
            $admin_id = isset($_POST["admin_id"])?mysql_escape_string($_POST["admin_id"]):"";                        
            echo Admin::deleteRowById($admin_id);
        }

        public function actionAjaxAdd(){                  
            $url = new Url();                          
            $data_insert['username'] = isset($_POST["username"])?mysql_escape_string($_POST["username"]):"";            
            $data_insert['fullname'] = isset($_POST["fullname"])?mysql_escape_string($_POST["fullname"]):"";            
            $data_insert['email'] = isset($_POST["email"])?mysql_escape_string($_POST["email"]):"";
            $data_insert['password'] =md5(isset($_POST["password"])?mysql_escape_string($_POST["password"]):"");

            echo Admin::insert($data_insert);
        }

        public function actionAjaxUpdate(){
            $url = new Url();     
            $admin_id = isset($_POST["admin_id"])?mysql_escape_string($_POST["admin_id"]):"";
            $data_insert['username'] = isset($_POST["username"])?mysql_escape_string($_POST["username"]):"";        
            $data_insert['fullname'] = isset($_POST["fullname"])?mysql_escape_string($_POST["fullname"]):"";
            $data_insert['password'] = md5(isset($_POST["password"])?mysql_escape_string($_POST["password"]):"");
            $data_insert['email'] = isset($_POST["email"])?mysql_escape_string($_POST["email"]):"";               

            echo Admin::update($admin_id,$data_insert);
        }

        public function actionLogin(){
            $url = new Url();
            $this->layout="column_login";
            $model = new AdminLogin();
            if(isset($_POST["AdminLogin"])){
                $model->username = $_POST["AdminLogin"]["username"];
                $model->password = $_POST["AdminLogin"]["password"];
                $model->remember = $_POST["AdminLogin"]["remember"];
                if($model->validate()){
                    $identity=new UserIdentity($model->username,$model->password);
                    if($identity->authenticate()){
                        Yii::app()->user->login($identity);
                        $this->redirect($url->createUrl("home/index"));
                    }
                    else{
                        echo $identity->errorMessage;  
                    }
                }
            }
            $this->render("login",array("model"=>$model));
        }

        public function actionAjaxSubmitLogin(){          
            $url = new Url();                      
            $model = new AdminLogin();
            $model->username = isset($_POST["username"])?mysql_escape_string($_POST["username"]):"";
            $model->password = isset($_POST["pass"])?mysql_escape_string($_POST["pass"]):"";        
            $model->remember = isset($_POST["remember"])?mysql_escape_string($_POST["remember"]):1;                
            $remember = isset($_POST["remember"])?mysql_escape_string($_POST["remember"]):1;                    
            if($model->validate()){
                $identity=new UserIdentity($model->username,$model->password);            
                if($identity->authenticate()){                
                    Yii::app()->user->login($identity,3600*24*7);
                    if($remember == 1){
                        $myDomain = ereg_replace('^[^\.]*\.([^\.]*)\.(.*)$', '\1.\2', $_SERVER['HTTP_HOST']);  
                        $setDomain = ($_SERVER['HTTP_HOST']) != "localhost" ? ".$myDomain" : false;  
                        setcookie ("admin_thichtryen_un", $username, time()+3600*24*(15), '/', "$setDomain", 0 );  
                        setcookie ("admin_thichtruyen_pw", $pass, time()+3600*24*(15), '/', "$setDomain", 0 );  
                    }
                    echo 1;
                    //$this->redirect($url->createUrl("home/index"));
                }
                else{
                    echo "fail";die;
                    echo $identity->errorMessage;  
                }
            }else{
                echo 0;die;
                //var_dump($model->getErrors());
            }
        }           

        public function actionLogout(){
            $url = new Url();
            Yii::app()->user->logout();
            $this->redirect($url->createUrl("admin/login"));
        }

        public function actionError(){

        }


    }

?>