<?php

    class CommentController extends Controller{
        //public $layout='column1';	

        /**
        * Declares class-based actions.
        */
        public function actions(){

        }

        /**
        * This is the action to handle external exceptions.
        */
        public function actionError(){
            if($error=Yii::app()->errorHandler->error)
            {
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->render('error', $error);
            }
        }        

        public function actionIndex(){        
            $url = new Url();                               
            $page = isset($_GET['page']) ? intval($_GET ['page']):1;
            $rows_per_page = 20;
            $have_content = 0;
            $begin = ($page - 1)*$rows_per_page;                        
            $end = $rows_per_page;
            $count = AdminComment::countData();
            if($count % $rows_per_page == 0)        
            {
                $totalpage = floor($count/$rows_per_page);
            }
            else
            {
                $totalpage = floor($count/$rows_per_page) + 1;                             
            }                    

            $data_comment = AdminComment::getDataPaging($begin,$end);
            $data_category = AdminCategory::getAllRows();
            
            $util = new Paging();                 
            $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("comment/index").'/',"");
                        
            $this->render('index',array('data_comment'=>$data_comment,'paging'=>$paging,'data_category'=>$data_category));
        }

        public function actionAjaxDeleteComment(){                  
            $url = new Url();                          
            $comment_id = isset($_POST["comment_id"])?mysql_escape_string($_POST["comment_id"]):"";            
            echo AdminComment::delete("id=".$comment_id);
        }        

    }

?>