<?php

class NewsController extends Controller
{
    //public $layout='column1';	

    /**
    * Declares class-based actions.
    */
    public function actions(){

    }

    /**
    * This is the action to handle external exceptions.
    */
    public function actionError(){
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
    * Displays the login page
    */
    public function actionIndex(){		        
        $data_news = AdminNews::getAllRows();        
        $this->render('index',array('data_news'=>$data_news));
    }

    public function actionAdd(){             
        $data_category = AdminCategory::getAllRows();
        $this->render('add',array('data_category'=>$data_category));
    }

    public function actionDetail(){        
        $news_id = isset($_GET["id"])?mysql_escape_string($_GET["id"]):"1";
        $data_new = AdminNews::getRowById($news_id);        
        $this->render('detail',array('data_new'=>$data_new));
    }

    public function actionAjaxChangeStatus(){          
        $url = new Url();                          
        $category_id = isset($_POST["category_id"])?mysql_escape_string($_POST["category_id"]):"";
        $data_update['is_hot'] = isset($_POST["is_hot_status"])?mysql_escape_string($_POST["is_hot_status"]):"";        
        echo AdminCategory::update($category_id,$data_update);
    }

    public function actionAjaxAddNew(){                  
        $url = new Url();                          
        $data_insert['news_title'] = isset($_POST["news_title"])?mysql_escape_string($_POST["news_title"]):"";                
        $data_insert['news_content'] = isset($_POST["news_content"])?$_POST["news_content"]:"";
        $data_insert['create_date'] = time();

        echo AdminNews::insert($data_insert);
    }

    public function actionAjaxEditNew(){                  
        $url = new Url();     
        $news_id = isset($_POST["news_id"])?mysql_escape_string($_POST["news_id"]):"";
        $data_insert['news_title'] = isset($_POST["news_title"])?mysql_escape_string($_POST["news_title"]):"";                
        $data_insert['news_content'] = isset($_POST["news_content"])?$_POST["news_content"]:"";        
        echo AdminNews::update($news_id,$data_insert);
    }

    public function actionAjaxDeleteNew(){                  
        $url = new Url();                          
        $author_id = isset($_POST["author_id"])?mysql_escape_string($_POST["author_id"]):"";                                
        echo AdminAuthor::delete("author_id = " . $author_id);
    }

}
