<?php
class ConfigurationController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions(){}

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the login page
     */
    public function actionIndex()
    {
        try{
            $admin_options = new AdminOptions();
            $configuration_data = $admin_options->getConfiguration();
            $this->render('index',array('configuration_data'=>$configuration_data));
        }catch (Exception $e)
        {
//            var_dump($e->getMessage());die;
        }
    }

    public function actionAjaxUpdateOptions()
    {
        $post_data = $_POST;
        if(!empty($post_data)){
            $admin_options = new AdminOptions();
            $admin_options->updateConfiguration($post_data);
        }
        echo 1;
    }
}