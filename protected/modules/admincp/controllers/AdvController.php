<?php

    class AdvController extends Controller
    {
        //public $layout='column1';	

        /**
        * Declares class-based actions.
        */
        public function actions(){

        }

        /**
        * This is the action to handle external exceptions.
        */
        public function actionError(){
            if($error=Yii::app()->errorHandler->error)
            {
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->render('error', $error);
            }
        }

        /**
        * Displays the login page
        */
        public function actionIndex(){		            
            $data_adv = AdminAdv::getSearch(" AND type = 1");
            $this->render('index',array('data_adv'=>$data_adv));
        }	
        
        public function actionPc(){        
            $data_adv = AdminAdv::getSearch(" AND type = 2");
            $this->render('pc',array('data_adv'=>$data_adv));
        }

        public function actionAjaxEditAdv(){
            $url = new Url();     
            $adv_1 = isset($_POST["adv_1"])?$_POST["adv_1"]:"";
            $adv_2 = isset($_POST["adv_2"])?$_POST["adv_2"]:"";
            $adv_3 = isset($_POST["adv_3"])?$_POST["adv_3"]:"";
            $adv_4 = isset($_POST["adv_4"])?$_POST["adv_4"]:"";
                            
            $update_content['content'] = $adv_1;            
            AdminAdv::update(1,$update_content);
            
            $update_content['content'] = $adv_2;            
            AdminAdv::update(2,$update_content);

            $update_content['content'] = $adv_3;            
            AdminAdv::update(3,$update_content);
            
            $update_content['content'] = $adv_4;            
            AdminAdv::update(9,$update_content);
            
            echo 1;
        }

        public function actionAjaxEditAdvPc(){
            $url = new Url();     
            $adv_1 = isset($_POST["adv_1"])?$_POST["adv_1"]:"";
            $adv_2 = isset($_POST["adv_2"])?$_POST["adv_2"]:"";
            $adv_3 = isset($_POST["adv_3"])?$_POST["adv_3"]:"";
            $adv_4 = isset($_POST["adv_4"])?$_POST["adv_4"]:"";
            $adv_5 = isset($_POST["adv_5"])?$_POST["adv_5"]:"";
                               
            $update_content['content'] = $adv_1;            
            AdminAdv::update(4,$update_content);
            
            $update_content['content'] = $adv_2;            
            AdminAdv::update(5,$update_content);

            $update_content['content'] = $adv_3;            
            AdminAdv::update(6,$update_content);
            
            $update_content['content'] = $adv_4;            
            AdminAdv::update(7,$update_content);
            
            $update_content['content'] = $adv_5;            
            AdminAdv::update(8,$update_content);
            
            echo 1;
        }
        
    }

?>
