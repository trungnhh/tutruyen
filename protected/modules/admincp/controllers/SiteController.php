<?php

class SiteController extends Controller
{
    //public $layout='column1';	

    /**
    * Declares class-based actions.
    */
    public function actions(){

    }

    /**
    * This is the action to handle external exceptions.
    */
    public function actionError(){
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
    * Displays the login page
    */
    public function actionIndex(){
	    $cur_date_string = strtotime('last week');
        $data_count = AdminPosts::getCountPostsByCategory($cur_date_string);
        $total_other = 0;        
        for($i=4;$i<count($data_count);$i++){
            $total_other = $total_other + $data_count[$i]['count'];            
        }        
        $data_chart[0] = $data_count[0];
        $data_chart[1] = $data_count[1];
        $data_chart[2] = $data_count[2];
        $data_chart[3] = $data_count[3];
        $data_chart[4] = $data_count[4];
        $data_chart[4]['name'] = "Còn lại";
        $data_chart[4]['count'] = $total_other;        

        $data_term = AdminTerm::getAllRows();        

        $where = "1=1";
        $count_post = AdminPosts::getCountPost($where);

        $cur_date_string = strtotime('last week');        
        $where = "update_date > " . $cur_date_string;
        $count_post_new  = AdminPosts::getCountPost($where);

        $where = " AND 1=1";
        $count_user = AdminUser::getCountUser($where);

        $cur_date_string = strtotime('last week');        
        $where = " AND create_date > " . $cur_date_string;
        $count_user_new  = AdminUser::getCountUser($where);

        $where = "1=1";
        $count_bookmark = AdminBookmark::getCountBookmark($where);

	    $a = 0;
		for($i=13;$i>=0;$i--){
			$str_date = '- '.$i.' day';
			$date = date('Y-m-d', strtotime($str_date));
			$count_post_by_day = AdminPosts::countPostsByDate($date);
			$data_chart_post[$a]['date'] = $date;
			$data_chart_post[$a]['count'] = $count_post_by_day;
			$a++;
		}

        $this->render('index',array('data_chart'=>$data_chart,'data_term'=>$data_term,'count_post'=>$count_post,'count_post_new'=>$count_post_new,'count_user'=>$count_user,'count_user_new'=>$count_user_new,'count_bookmark'=>$count_bookmark,'data_chart_post'=>$data_chart_post));
    }

    public function actionAjaxUpdateTerm(){                  
        $url = new Url();             
        $data_update['page_title'] = isset($_POST["page_title"])?mysql_escape_string($_POST["page_title"]):"";
        $data_update['page_description'] = isset($_POST["page_description"])?mysql_escape_string($_POST["page_description"]):"";                       
        $data_update['vip_messege'] = isset($_POST["vip_messege"])?mysql_escape_string($_POST["vip_messege"]):"";                       
        $data_update['hot_messege'] = isset($_POST["hot_messege"])?mysql_escape_string($_POST["hot_messege"]):"";                       

        $content = $data_update['vip_messege'];
        $content = str_replace('\n','<br>',$content);
        $content = str_replace('\"','"',$content);        

        $data_update['vip_messege'] = $content;

        $content = $data_update['hot_messege'];
        $content = str_replace('\n','<br>',$content);
        $content = str_replace('\"','"',$content);        

        $data_update['hot_messege'] = $content;

        echo AdminTerm::update(1,$data_update);
    }

    public function actionAjaxDeleteCache(){        
        $type = isset($_POST["type"])?mysql_escape_string($_POST["type"]):1;
        if($type == 1){
            $files = glob('protected/runtime/cache/*'); // get all file names        
            //echo "Count file:".count($files)."\n";
            $i=0;
            foreach($files as $file){   
                if(is_file($file)){
                    unlink($file); 
                    $i++;    
                }                
            }
            echo "Deleted cache pc version $i file.";
	        die;
        }else{
            $files = glob('/home/thichtruyenvn/m.thichtruyen.vn/protected/runtime/cache/*'); // get all file names                
            $i=0;
            foreach($files as $file){   
                if(is_file($file)){
                    unlink($file); 
                    $i++;    
                }                
            }
            echo "Deleted cache mobile version $i file.";
            die;    
        }        
    }

    public function actionFileManager(){                
        $this->render('filemanager',array());
    }

    public function actionImageManager(){                
        $this->render('imagemanager',array());
    }

    public function actionGetImage(){      
        $url = new Url();
        $page = isset($_GET['page']) ? intval($_GET ['page']):1;
        $rows_per_page = 500;  
        $begin = ($page - 1)*$rows_per_page;                        
        $end = $rows_per_page;
        $first_img = '';
        ob_start();
        ob_end_clean();
        set_time_limit(0);
        ini_set('max_execution_time',0);
        $data_post = AdminPosts::getDataForImage($begin,$end);        
        $data_image = array();
        $i = 0;
        foreach($data_post as $row){
            $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $row['post_content'], $matches);    
            if(!empty($matches[1][0])){
                $data_image[$i]['post_id'] = $row['id'];
                $data_image[$i]['image_url'] = $matches[1][0];
                $i++;
            }            
        }
        $count = 0;
        foreach($data_image as $row){
            $data_update['post_image'] = $row['image_url'];
            AdminPosts::update($row['post_id'],$data_update);                
            $count++;
        }
        echo "Trang : " . $page . "<br>";
        echo "Số dữ liệu đã tìm : " . ($begin+500) . "<br>";
        echo "Số post đã được insert ảnh : " . $count . "<br>";
        echo 'Click tiếp <a href="'.$url->createUrl("site/getImage",array("page"=>($page+1))).'">vào đây<a> để insert tiếp<br>';        
    }

}
