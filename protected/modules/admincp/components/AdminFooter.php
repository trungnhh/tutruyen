<?php
Yii::import('zii.widgets.CPortlet');

class AdminFooter extends CPortlet
{
    protected function renderContent(){

        $this->render("admin_footer",array());         
    }
}