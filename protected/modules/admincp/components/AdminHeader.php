<?php
Yii::import('zii.widgets.CPortlet');

class AdminHeader extends CPortlet
{
    protected function renderContent(){
        $admin_id = Yii::app()->user->id;
        $data_admin = Admin::getRowsById($admin_id);
        $this->render("admin_header",array("data_admin"=>$data_admin));         
    }
}