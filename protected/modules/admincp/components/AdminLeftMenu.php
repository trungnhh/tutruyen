<?php
Yii::import('zii.widgets.CPortlet');

class AdminLeftMenu extends CPortlet
{
    protected function renderContent(){
        
        $this->render("admin_leftmenu",array());         
    }
}