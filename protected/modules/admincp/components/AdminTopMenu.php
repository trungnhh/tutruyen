<?php
Yii::import('zii.widgets.CPortlet');

class AdminTopMenu extends CPortlet
{
    protected function renderContent(){
        if(isset($_COOKIE['admin_thichtryen_un'])){            
            $model = new AdminLogin();
            $model->username = $_COOKIE['admin_thichtryen_un'];
            $model->password = $_COOKIE['admin_thichtruyen_pw'];        
            if($model->validate()){
                $identity=new UserIdentity($model->username,$model->password);            
                if($identity->authenticate()){                
                    Yii::app()->user->login($identity);                    
                }
            }
        }
        $this->render("admin_topmenu",array());         
    }
}