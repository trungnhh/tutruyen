<?php 
    $url = new Url();
    $controller = Yii::app()->controller->id;
    $action = Yii::app()->controller->action->id;
    $ac = $controller.'-'.$action;    
?>
<div id="left_menu">
    <ul id="main_menu" class="main_menu">
        <li class="limenu <?php if($controller == "site"){echo "select";} ?>" ><a href="<?php echo $url->createUrl("site/index"); ?>"><span class="ico gray shadow home" ></span><b>Bảng điều khiển</b></a></li>
        <?php if(StringUtils::checkSuperAdmin() == 1){ ?>        
            <li class="limenu <?php if($controller == "category"){echo "select";} ?>" ><a href="<?php echo $url->createUrl("category/index"); ?>"><span class="ico gray shadow  bookmark"></span><b>Danh mục</b> </a></li>
            <li class="limenu <?php if($controller == "post"){echo "select";} ?>" ><a href="<?php echo $url->createUrl("post/list"); ?>"><span class="ico gray shadow  pen"></span><b>Truyện</b> </a>
                <ul>
                    <li ><a href="<?php echo $url->createUrl("post/search"); ?>"> Tìm kiếm </a></li>
                    <li ><a href="<?php echo $url->createUrl("post/list"); ?>"> Danh sách </a></li>
                    <li ><a href="<?php echo $url->createUrl("post/vip"); ?>"> Truyện VIP </a></li>                
                    <li ><a href="<?php echo $url->createUrl("post/hot"); ?>"> Truyện HOT </a></li>
                    <li ><a href="<?php echo $url->createUrl("post/featured"); ?>"> Truyện Đề cử </a></li>
                    <li ><a href="<?php echo $url->createUrl("post/going"); ?>"> Truyện Chưa Hoàn Thành </a></li>
                    <li ><a href="<?php echo $url->createUrl("post/add"); ?>"> Đăng truyện </a></li>                
                </ul>
            </li>            
            <li class="limenu <?php if($controller == "news"){echo "select";} ?>" ><a href="<?php echo $url->createUrl("news/index"); ?>"><span class="ico gray shadow  diary"></span><b>Tin tức</b> </a></li>
            <li class="limenu <?php if($controller == "author"){echo "select";} ?>" ><a href="<?php echo $url->createUrl("author/index"); ?>"><span class="ico gray shadow  paragraph_align_left"></span><b>Tác giả</b> </a></li>
            <li class="limenu" ><a target="_blank" href="<?=Helpers::getImageBaseUrl();?>filemanager2.php"><span class="ico gray shadow  file"></span><b>Quản lý file</b> </a></li>
            <li class="limenu <?php if($controller == "user"){echo "select";} ?>" ><a href="<?php echo $url->createUrl("user/index"); ?>"><span class="ico gray shadow  group"></span><b>Thành viên</b> </a></li>
            <li class="limenu <?php if($controller == "admin"){echo "select";} ?>" ><a href="<?php echo $url->createUrl("admin/index"); ?>"><span class="ico gray shadow  administrator"></span><b>Mod</b> </a></li>
            <li class="limenu <?php if($controller == "comment"){echo "select";} ?>" ><a href="<?php echo $url->createUrl("comment/index"); ?>"><span class="ico gray shadow  pictures_folder"></span><b>Comment</b> </a></li>
            <li class="limenu <?php if($controller == "adv"){echo "select";} ?>" ><a href="<?php echo $url->createUrl("adv/index"); ?>"><span class="ico gray shadow  location"></span><b>Quảng cáo</b> </a>
                <ul>
                    <li ><a href="<?php echo $url->createUrl("adv/index"); ?>"> Quảng cáo Mobile </a></li>                                                            
                    <li ><a href="<?php echo $url->createUrl("adv/pc"); ?>"> Quảng cáo PC </a></li>                
                </ul>
            </li>
            <li class="limenu <?php if($controller == "transaction"){echo "select";} ?>" ><a href="<?php echo $url->createUrl("transaction/index"); ?>"><span class="ico gray shadow  coin"></span><b>Giao dịch</b> </a></li>            
            <li class="limenu <?php if($controller == "messages"){echo "select";} ?>" ><a href="<?php echo $url->createUrl("messages/index"); ?>"><span class="ico gray shadow  mail"></span><b>Tin nhắn</b> </a></li>
            <li class="limenu <?php if($controller == "request"){echo "select";} ?>" ><a href="<?php echo $url->createUrl("request/index"); ?>"><span class="ico gray shadow  location"></span><b>Yêu cầu truyện</b> </a></li>
            <li class="limenu <?php if($controller == "note"){echo "select";} ?>" ><a href="<?php echo $url->createUrl("note/index"); ?>"><span class="ico gray shadow  mail"></span><b>Góp ý</b> </a></li>
            <li class="limenu <?php if($controller == "configuration"){echo "select";} ?>" ><a href="<?php echo $url->createUrl("configuration/index"); ?>"><span class="ico gray shadow  gear"></span><b>Cấu hình</b> </a></li>
            <?php }else{ ?>
            <li class="limenu <?php if($controller == "post"){echo "select";} ?>" ><a href="<?php echo $url->createUrl("post/mod"); ?>"><span class="ico gray shadow  pen"></span><b>Truyện</b> </a>
                <ul>
	                <li ><a href="<?php echo $url->createUrl("post/search"); ?>"> Tìm kiếm </a></li>
                    <li ><a href="<?php echo $url->createUrl("post/mod"); ?>"> Danh sách </a></li>
	                <li ><a href="<?php echo $url->createUrl("post/vip"); ?>"> Truyện VIP </a></li>
	                <li ><a href="<?php echo $url->createUrl("post/hot"); ?>"> Truyện HOT </a></li>
	                <li ><a href="<?php echo $url->createUrl("post/featured"); ?>"> Truyện Đề cử </a></li>
	                <li ><a href="<?php echo $url->createUrl("post/going"); ?>"> Truyện Chưa Hoàn Thành </a></li>
                    <li ><a href="<?php echo $url->createUrl("post/add"); ?>"> Đăng truyện </a></li>                
                </ul>
            </li>
	        <li class="limenu <?php if($controller == "author"){echo "select";} ?>" ><a href="<?php echo $url->createUrl("author/index"); ?>"><span class="ico gray shadow  paragraph_align_left"></span><b>Tác giả</b> </a></li>
            <li class="limenu" ><a target="_blank" href="<?=Helpers::getImageBaseUrl();?>filemanager2.php"><span class="ico gray shadow  file"></span><b>Quản lý file</b> </a></li>
            <?php } ?>

    </ul>
</div>
