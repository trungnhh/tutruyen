<?php 
    $url = new Url();
    $controller = Yii::app()->controller->id;
    $action = Yii::app()->controller->action->id;
    $ac = $controller.'-'.$action;    
?>
<div id="header" >
    <div id="account_info"> 
        <img src="<?=Yii::app()->params["urlRsAdmin"] ?>/images/avatar.png" alt="Online" class="avatar" />
        <div class="setting" title="Hồ sơ cá nhân"><b>Chào mừng,</b> <b class="red"><?php echo $data_admin['fullname']; ?></b><img src="<?=Yii::app()->params["urlRsAdmin"] ?>/images/gear.png" class="gear" alt="Hồ sơ cá nhân" ></div>
        <div class="logout" title="Ngắt kết nối"><b>Thoát</b> <img src="<?=Yii::app()->params["urlRsAdmin"] ?>/images/connect.png" name="connect" class="disconnect" alt="disconnect" ></div> 
    </div>
</div>

<script type="">

    // logout  
    $('.logout').live('click',function() { 
        var str="Logout"; 
        var overlay="1"; 
        loading(str,overlay);
        setTimeout("unloading()",1500);
        setTimeout( "window.location.href='<?php echo $url->createUrl("admin/logout"); ?>'", 2000 );
    });
    
    $('.setting').live('click',function() { 
        window.location.href='<?php echo $url->createUrl("admin/profile"); ?>';
    });

</script>