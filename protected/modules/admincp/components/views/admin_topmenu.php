<?php 
    $url = new Url();
    $controller = Yii::app()->controller->id;
    $action = Yii::app()->controller->action->id;
    $ac = $controller.'-'.$action;
?>
<div class="topcolumn">
    <!--<div class="logo"></div>-->
    <ul  id="shortcut">
        <li> <a target="_blank" href="<?php echo Yii::app()->params['urlRs'];?>/" title="SauTruyen"> <img src="<?=Yii::app()->params["urlRsAdmin"] ?>/images/icon/shortcut/home.png" alt="Trang chủ"/><strong>SauTruyen</strong> </a> </li>
        <li> <a href="<?php echo $url->createUrl("site/index"); ?>" title="Bảng điều khiển"> <img src="<?=Yii::app()->params["urlRsAdmin"] ?>/images/icon/shortcut/graph.png" alt="Bảng điều khiển"/><strong>Biểu đồ</strong> </a> </li>
        <li> <a href="<?php echo $url->createUrl("admin/profile"); ?>" title="Cài đặt" > <img src="<?=Yii::app()->params["urlRsAdmin"] ?>/images/icon/shortcut/setting.png" alt="Cài đặt" /><strong>Cài đặt</strong></a> </li> 
        <?php if(StringUtils::checkSuperAdmin() == 1){ ?>        
        <!--<li> <a href="javascript:" title="Xoá Cache" onclick="remove_cache(1)"> <img src="<?/*=Yii::app()->params["urlRsAdmin"] */?>/images/icon/shortcut/empty.png" alt="Xoá Cache" /><strong>Xoá Cache</strong></a><div class="notification" >10</div></li>-->
        <?php  } ?>
	    <li> <a href="javascript:" title="Xoá Cache" onclick="remove_cache(1)"> <img src="<?=Yii::app()->params["urlRsAdmin"] ?>/images/icon/shortcut/empty.png" alt="Xoá Cache" /><strong>Xoá Cache</strong></a><div class="notification" >10</div></li>
    </ul>
</div>
<div class="clear"></div>

<script>

    function remove_cache(type){
        loading('Xử lý',1);                                                
        var strUrl = '<?php echo $url->createUrl("site/ajaxDeleteCache"); ?>';         
        $.ajax({
            type: "POST",
            url: strUrl,            
            data: {type:type}, 
            beforeSend: function() {

            },
            complete: function() {
                setTimeout( "unloading()", 1000 );
            },
            success: function(msg){                

            }
        })
    }    

</script>