<?php

class AdmincpModule extends CWebModule
{
    public $defaultController = 'site';

    private $_assetsUrl;

    public function init()
    {
        // echo Yii::getPathOfAlias('admin.assets');die; 
        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        // import the module-level models and components
        $this->setImport(array(
        'admincp.models.*',
        'admincp.components.*',
        ));

        $this->setComponents(array(
        //'errorHandler' => array('errorAction' => 'customer/default/error'),
        'user' => array(
        'class' => 'CWebUser',             
        'loginUrl' => Yii::app()->createUrl('admincp/admin/login'),
        )
        ));

        //Yii::app()->user->setStateKeyPrefix('_admin');
    }

    public function beforeControllerAction($controller, $action)
    {
        if(parent::beforeControllerAction($controller, $action))
        {
            $route = $controller->id . '/' . $action->id;

            $publicPages = array(
                'admin/login',
                'admin/ajaxSubmitLogin',
                'admin/error',
            );
            $controller->layout = 'column_admin';
            if(Yii::app()->getModule('admincp')->user->isGuest && !in_array($route, $publicPages)){
                Yii::app()->getModule('admincp')->user->loginRequired();
            }
            return true;
        }
        else
            return false;
    }

    /**
    * @return string the base URL that contains all published asset files of this module.
    */
    public function getAssetsUrl()
    {
        if($this->_assetsUrl===null)
            $this->_assetsUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('admincp.assets'));

        return $this->_assetsUrl;
    }

    /**
    * @param string the base URL that contains all published asset files of this module.
    */
    public function setAssetsUrl($value)
    {
        $this->_assetsUrl=$value;
    }

    public function registerCss($file, $media='all')
    {
        $href = $this->getAssetsUrl().'/css/'.$file;
        return '<link rel="stylesheet" type="text/css" href="'.$href.'" media="'.$media.'" />';
    }

    public function registerJs($file)
    {
        $href = $this->getAssetsUrl().'/js/'.$file;
        return '<script type="text/javascript" src="'.$href.'"></script>';
    }

    public function registerImage($file)
    {
        return $this->getAssetsUrl().'/images/'.$file;
    }
}
