<?php
	class Comment extends CActiveRecord{
		public static function model($className = __CLASS__) {
			return parent::model ( $className );
		}

		// dat luat cho model
		public function rules(){}

		// goi den bang can ket noi
		public function tableName() {
			return 't186_comment';
		}

		// nhan cac thuong tinh
		public function attributeLabels(){}

		public function getRowById($id){
			$id = intval($id);
			$row = array();
			$cache = Yii::app()->cache;
			if($cache != null){
				$cacheService = new CacheService("Comment","getRowById",$id);
				$key = $cacheService->createKey();
				$dependency = $cacheService->createDependency();
				$cache = Yii::app ()->cache->get ( $key );
			}
			//$cache=false;
			if($cache == false){
				$sql = "SELECT * FROM t186_comment WHERE id=".$id;
				$connect = Yii::app()->db;
				$command = $connect->createCommand($sql);
				$row = $command->queryRow();
				Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
			}else{
				$row = $cache;
			}
			return $row;
		}

		public function getDataComment($post_id,$comment_parent,$type){
			$row = array();
			$cache = Yii::app()->cache;
			if($cache != null){
				$cacheService = new CacheService("Comment","getDataComment",$post_id.$comment_parent.$type);
				$key = $cacheService->createKey();
				$dependency = $cacheService->createDependency();
				$cache = Yii::app ()->cache->get ( $key );
			}
			$cache=false;
			if($cache == false){
				if($comment_parent == 0){
					$sql = "SELECT t186_comment.*,t186_users.username,t186_users.image FROM t186_comment INNER JOIN t186_users ON t186_comment.user_id = t186_users.id WHERE post_id = ".$post_id ." AND comment_parent = ".$comment_parent." AND type = ".$type." ORDER BY id DESC";
				}else{
					$sql = "SELECT t186_comment.*,t186_users.username,t186_users.image FROM t186_comment INNER JOIN t186_users ON t186_comment.user_id = t186_users.id WHERE post_id = ".$post_id ." AND comment_parent <> 0 AND type = ".$type." ORDER BY id ASC";
				}
				$connect = Yii::app()->db;
				$command = $connect->createCommand($sql);
				$row = $command->queryAll();
				Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
			}else{
				$row = $cache;
			}
			return $row;
		}

		public function getDataCommentPaging($post_id,$comment_parent,$type,$begin,$end){
			$row = array();
			$cache = Yii::app()->cache;
			if($cache != null){
				$cacheService = new CacheService("Comment","getDataCommentPaging",$post_id.$comment_parent.$type.$begin.$end);
				$key = $cacheService->createKey();
				$dependency = $cacheService->createDependency();
				$cache = Yii::app ()->cache->get ( $key );
			}
			$cache=false;
			if($cache == false){
				if($comment_parent == 0){
					$sql = "SELECT t186_comment.*,t186_users.username,t186_users.image FROM t186_comment INNER JOIN t186_users ON t186_comment.user_id = t186_users.id WHERE post_id = ".$post_id ." AND comment_parent = ".$comment_parent." AND type = ".$type." ORDER BY id DESC LIMIT ".$begin.",".$end;
				}else{
					$sql = "SELECT t186_comment.*,t186_users.username,t186_users.image FROM t186_comment INNER JOIN t186_users ON t186_comment.user_id = t186_users.id WHERE post_id = ".$post_id ." AND comment_parent <> 0 AND type = ".$type." ORDER BY id ASC";
				}
				$connect = Yii::app()->db;
				$command = $connect->createCommand($sql);
				$row = $command->queryAll();
				Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
			}else{
				$row = $cache;
			}
			return $row;
		}

		public function getAllRows(){
			$rows = array();
			$cache = Yii::app()->cache;
			if($cache != null){
				$cacheService = new CacheService("Comment","getAllRows","");
				$key = $cacheService->createKey();
				$dependency = $cacheService->createDependency();
				$cache = Yii::app ()->cache->get ( $key );
			}
			//$cache=false;
			if($cache == false){
				$sql = "SELECT * FROM t186_comment";
				$connect = Yii::app()->db;
				$command = $connect->createCommand($sql);
				$rows = $command->queryAll();

				Yii::app ()->cache->set ( $key, $rows, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
			}else{
				$rows = $cache;
			}
			return $rows;
		}

		public function insertObject($array_input){
			$sql='';
			foreach($array_input as $key=>$value)
			{
				$sql.=$key."='".$value."',";
			}
			$sql='INSERT INTO t186_comment SET '.$sql;
			$sql=rtrim($sql,',');
			$connect = Yii::app()->db;
			$command = $connect->createCommand($sql);
			$command->execute();
			$record_id=Yii::app()->db->getLastInsertID();
			return $record_id;
		}

		public function updateObject($array_input,$key_id,$key_value){
			$sql='';
			foreach($array_input as $key=>$value)
			{
				$sql.=$key."='".$value."',";
			}
			$sql=rtrim($sql,',');
			if($sql!='')
			{
				$sql='UPDATE t186_comment SET '.$sql.' WHERE '.$key_id.'='.$key_value;
				$connect = Yii::app()->db;
				$command = $connect->createCommand($sql);
				$a=$command->execute();
				return $a;
			}
			return -1;
		}

		public function deleteObject($array_input){
			$sql=' 1 ';
			foreach($array_input as $key=>$value)
			{
				$sql.=" AND ".$key."='".$value."'";
			}
			$sql=rtrim($sql,',');
			if($sql!='')
			{
				$sql='delete from t186_comment where '.$sql.'';
				$connect = Yii::app()->db;
				$command = $connect->createCommand($sql);
				$a=$command->execute();
				if($a) return 1;
				else return 0;
			}
			else return 0;

		}

		public function insert($data){
			$connect = Yii::app()->db;
			$command = $connect->createCommand($sql);
			$command->insert('t186_comment', $data);
			$record_id=Yii::app()->db->getLastInsertID();
			return $record_id;
		}

		public function getDataByUserId($user_id){
			$user_id = intval($user_id);
			// $cache = false;
			$row = array();
			$cache = Yii::app()->cache;
			if($cache != null){
				$cacheService = new CacheService("Comment","getDataByUserId",$user_id);
				$key = $cacheService->createKey();
				$dependency = $cacheService->createDependency();
				$cache = Yii::app ()->cache->get ( $key );
			}
			//$cache = false;
			if($cache == false){
				$sql = "SELECT t186_comment.*,t186_category.alias,t186_posts.post_title FROM t186_comment INNER JOIN t186_posts ON t186_comment.post_id = t186_posts.id INNER JOIN t186_category ON t186_posts.category_id = t186_category.id WHERE post_status = 1 AND user_id = " . $user_id;
				$connect = Yii::app()->db;
				$command = $connect->createCommand($sql);
				$row = $command->queryAll();
				Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_3600, new CGlobalStateCacheDependency ( $dependency ) );
			}else{
				$row = $cache;
			}
			return $row;
		}

		public function delete($where){
			$connect = Yii::app()->db;
			$command = $connect->createCommand($sql);
			return $command->delete('t186_comment', $where);
		}

	}