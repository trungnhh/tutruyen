<?php
class Vote extends CActiveRecord{
    public static function model($className = __CLASS__) {
        return parent::model ( $className );
    }

    // dat luat cho model
    public function rules(){}         

    // goi den bang can ket noi   
    public function tableName() {                  
        return 't186_vote';
    }

    // nhan cac thuong tinh 
    public function attributeLabels(){}

    public function getRowById($id){
        $id = intval($id);        
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("Vote","getRowById",$id);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }
        //$cache=false;
        if($cache == false){ 
            $sql = "SELECT * FROM t186_vote WHERE id=".$id;                      
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryRow();
            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }    
    
    public function getRowByCheckVote($post_id,$user_id){        
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("Vote","getRowByCheckVote",$post_id.$user_id);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }
        $cache=false;
        if($cache == false){ 
            $sql = "SELECT * FROM t186_vote WHERE user_id=".$user_id." AND post_id =".$post_id;
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryRow();
            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }
    
    public function getAllRows(){
        $rows = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("Vote","getAllRows","");
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }
        //$cache=false;
        if($cache == false){ 
            $sql = "SELECT * FROM t186_vote";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryAll();

            Yii::app ()->cache->set ( $key, $rows, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $rows = $cache;
        }
        return $rows;
    }    
    
    public function insertObject($array_input){
        $sql='';
        foreach($array_input as $key=>$value)
        {
            $sql.=$key."='".$value."',"; 
        }
        $sql='INSERT INTO t186_vote SET '.$sql;
        $sql=rtrim($sql,',');
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);
        $command->execute();
        $record_id=Yii::app()->db->getLastInsertID();  
        return $record_id;
    }

    public function updateObject($array_input,$key_id,$key_value){
        $sql='';
        foreach($array_input as $key=>$value)
        {
            $sql.=$key."='".$value."',"; 
        }
        $sql=rtrim($sql,',');
        if($sql!='')
        {
            $sql='UPDATE t186_vote SET '.$sql.' WHERE '.$key_id.'='.$key_value;
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $a=$command->execute();
            return $a;
        }
        return -1;
    }    

    public function deleteObject($array_input){
        $sql=' 1 ';
        foreach($array_input as $key=>$value)
        {
            $sql.=" AND ".$key."='".$value."'"; 
        }
        $sql=rtrim($sql,',');
        if($sql!='')
        {
            $sql='delete from t186_vote where '.$sql.'';
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $a=$command->execute();
            if($a) return 1;
            else return 0;
        }
        else return 0;

    }

    public function insert($data){
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);        
        $command->insert('t186_vote', $data);    
        $record_id=Yii::app()->db->getLastInsertID();  
        return $record_id;
    }

    public function getDataByUserId($user_id){
        $user_id = intval($user_id);
        // $cache = false;
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("Vote","getDataByUserId",$user_id);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }                
        $cache = false;
        if($cache == false){             
            $sql = "SELECT t186_vote.*,t186_category.alias,t186_posts.post_title FROM t186_vote INNER JOIN t186_posts ON t186_vote.post_id = t186_posts.id INNER JOIN t186_category ON t186_posts.category_id = t186_category.id WHERE post_status = 1 AND user_id = " . $user_id;
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryAll();
            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }
    
    public function getDataByUserIdPaging($user_id,$begin,$end){
        //$cache = false;
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("Vote","getDataByCategoryIdPaging",$user_id.$begin.$end);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }
        $cache=false;
        if($cache == false){          
            $sql = "SELECT t186_vote.*,t186_category.alias,t186_category.name as 'category_name',t186_posts.post_title,t186_posts.post_alias,t186_posts.post_content,t186_posts.post_image,t186_posts.post_views,post_vote,t186_posts.post_comments,t186_author.author_name FROM t186_vote INNER JOIN t186_posts ON t186_vote.post_id = t186_posts.id INNER JOIN t186_category ON t186_posts.category_id = t186_category.id LEFT JOIN t186_author ON t186_posts.author_id = t186_author.author_id WHERE post_status = 1 AND user_id = " . $user_id;
            $sql .= " ORDER BY t186_vote.id DESC LIMIT ".$begin.",".$end;                        
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);            
            $row = $command->queryAll();            
            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }
    
    public function delete($where){
        $connect = Yii::app()->db;
        $command = $connect->createCommand($sql);                
        return $command->delete('t186_vote', $where);        
    }
    
}
