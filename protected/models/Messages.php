<?php

    class Messages extends CActiveRecord{
        public static function model($className = __CLASS__) {
            return parent::model ( $className );
        }

        // dat luat cho model
        public function rules(){}         

        // goi den bang can ket noi   
        public function tableName() {                  
            return 't186_messages';
        }

        // nhan cac thuong tinh 
        public function attributeLabels(){}

	    public function countUnreadMessage($username){
		    // $cache = false;
		    $row = array();
		    $cache = Yii::app()->cache;
		    if($cache != null){
			    $cacheService = new CacheService("Messages","countUnreadMessage",$username);
			    $key = $cacheService->createKey();
			    $dependency = $cacheService->createDependency();
			    $cache = Yii::app ()->cache->get ( $key );
		    }
		    $cache = false;
		    if($cache == false){
			    $sql = "SELECT COUNT(*) FROM t186_messages LEFT JOIN t186_messages_check ON t186_messages.id = t186_messages_check.message_id WHERE t186_messages_check.id IS NULL AND t186_messages.type = 1 OR (t186_messages.receiver = '".$username."' AND t186_messages_check.status IS NULL)";
			    $connect = Yii::app()->db;
			    $command = $connect->createCommand($sql);
			    $row = $command->queryScalar();
			    Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
		    }else{
			    $row = $cache;
		    }
		    return $row;
	    }

        public function getRowById($id){
            $id = intval($id);        
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Messages","getRowById",$id);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }
            //$cache=false;
            if($cache == false){ 
                $sql = "SELECT t186_messages.*, t186_messages_check.status FROM t186_messages LEFT JOIN t186_messages_check ON t186_messages.id = t186_messages_check.message_id WHERE t186_messages.id=".$id;
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryRow();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }

        public function getAllRows(){
            $rows = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Messages","getAllRows","");
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }
            //$cache=false;
            if($cache == false){ 
                $sql = "SELECT * FROM t186_messages";
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $rows = $command->queryAll();

                Yii::app ()->cache->set ( $key, $rows, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $rows = $cache;
            }
            return $rows;
        }    

        public function insertObject($array_input){
            $sql='';
            foreach($array_input as $key=>$value)
            {
                $sql.=$key."='".$value."',"; 
            }
            $sql='INSERT INTO t186_messages SET '.$sql;
            $sql=rtrim($sql,',');
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $command->execute();
            $record_id=Yii::app()->db->getLastInsertID();  
            return $record_id;
        }

        public function updateObject($array_input,$key_id,$key_value){
            $sql='';
            foreach($array_input as $key=>$value)
            {
                $sql.=$key."='".$value."',"; 
            }
            $sql=rtrim($sql,',');
            if($sql!='')
            {
                $sql='UPDATE t186_messages SET '.$sql.' WHERE '.$key_id.'='.$key_value;
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $a=$command->execute();
                return $a;
            }
            return -1;
        }    

        public function deleteObject($array_input){
            $sql=' 1 ';
            foreach($array_input as $key=>$value)
            {
                $sql.=" AND ".$key."='".$value."'"; 
            }
            $sql=rtrim($sql,',');
            if($sql!='')
            {
                $sql='delete from t186_messages where '.$sql.'';
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $a=$command->execute();
                if($a) return 1;
                else return 0;
            }
            else return 0;

        }

        public function insert($data){
            $connect = Yii::app()->db;
            $command = $connect->createCommand();
            $command->insert('t186_messages', $data);    
            $record_id=Yii::app()->db->getLastInsertID();  
            return $record_id;
        }

	    public function update($id,$data){
		    $connect = Yii::app()->db;
		    $command = $connect->createCommand();
		    return $command->update('t186_messages', $data,"id=".$id);
	    }

        public function delete($where){
            $connect = Yii::app()->db;
            $command = $connect->createCommand();
            return $command->delete('t186_messages', $where);        
        }
        
        public function countDataSearch($where){            
            // $cache = false;
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Messages","countDataSearch",$where);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }                
            $cache = false;
            if($cache == false){             
                $sql = "SELECT COUNT(*) FROM t186_messages WHERE 1 = 1 ". $where;
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryScalar();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }
        
        public function getSearch($where,$begin,$end){
            $rows = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Messages","getSearch",$where.$begin.$end);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }
            $cache=false;
            if($cache == false){ 
                $sql = "SELECT t186_messages.*, t186_messages_check.status FROM t186_messages LEFT JOIN t186_messages_check ON t186_messages.id = t186_messages_check.message_id WHERE 1 = 1 ".$where." ORDER BY t186_messages.id DESC";
                $sql .= " LIMIT ".$begin.",".$end;
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $rows = $command->queryAll();

                Yii::app ()->cache->set ( $key, $rows, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $rows = $cache;
            }
            return $rows;
        }

    }

?>