<?php
class Options extends CActiveRecord
{
    protected $_tableName = 't186_options';
    protected $_connect = null;
    protected $_configurationKey = '__configuration';

    public static function model($className = __CLASS__) {
        return parent::model ( $className );
    }

    public function rules(){}

    public function attributeLabels(){}

    /**
     * Get database model instance
     * @return null
     */
    protected function _getConnect()
    {
        if(!$this->_connect){
            $this->_connect = Yii::app()->db;
        }
        return $this->_connect;
    }

    /**
     * Get table name from database
     * @return string
     */
    public function tableName()
    {
        return $this->_tableName;
    }

    /**
     * Get option key of Configuration value
     * @return string
     */
    public function getConfigurationKey()
    {
        return $this->_configurationKey;
    }

    /**
     * Get Configuration value
     * @return bool|mixed
     */
    public function getConfiguration()
    {
        if(!empty($this->getRowValueByName($this->getConfigurationKey()))){
            $configuration = $this->getRowValueByName($this->getConfigurationKey());
            return unserialize($configuration['option_value']);
        }
        return false;
    }

    /**
     * Get all records of options
     * @return mixed
     */
    public function getOptions(){
        $sql = "SELECT * FROM {$this->tableName()}";
        $command = $this->_getConnect()->createCommand($sql);
        $rows = $command->queryAll();
        return $rows;
    }

    /**
     * Get a record by option_name
     * @param $option_name
     * @return array
     */
    public function getRowByName($option_name)
    {
        $row = array();
        $sql = "SELECT `".$this->tableName()."`.* FROM `".$this->tableName()."` WHERE `".$this->tableName()."`.`option_name` = '" . $option_name . "'";
        $command = $this->_getConnect()->createCommand($sql);
        $row = $command->queryRow();
        return $row;
    }

    /**
     * Get option_value by option_name
     * @param $option_name
     * @return array
     */
    public function getRowValueByName($option_name)
    {
        $row = array();
        $sql = "SELECT `".$this->tableName()."`.`option_value` FROM `".$this->tableName()."` WHERE `".$this->tableName()."`.`option_name` = '" . $option_name . "'";
        $command = $this->_getConnect()->createCommand($sql);
        $row = $command->queryRow();
        return $row;
    }
}