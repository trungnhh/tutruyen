<?php

    class PostsLink extends CActiveRecord{
        public static function model($className = __CLASS__) {
            return parent::model ( $className );
        }

        // dat luat cho model
        public function rules(){}         

        // goi den bang can ket noi   
        public function tableName() {                  
            return 't186_posts_link';
        }

        // nhan cac thuong tinh 
        public function attributeLabels(){}    

        public function getRowById($id){
            $id = intval($id);            
            $row = array();
            $cache = Yii::app()->cache;            
            if($cache != null){
                $cacheService = new CacheService("posts_link","getRowById",$id."test");
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }                    
            $cache = false;
            if($cache == false){ 
                $sql = "SELECT * FROM t186_posts_link WHERE id =".$id;                      
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryRow();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }

        public function getRowByPostId($post_id){            
            $row = array();
            $cache = Yii::app()->cache;             
            if($cache != null){                
                $cacheService = new CacheService("posts_link","getRowByPostId",$post_id."test");
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();                
                $cache = Yii::app()->cache->get($key);                
            }                                 
            if($cache == false){ 
                $sql = "SELECT * FROM t186_posts_link WHERE post_id = ".$post_id;
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryRow();
                Yii::app()->cache->set($key,$row,ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency($dependency));    
                /*if($row == null){                    
                    Yii::app()->cache->set($key,0,ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency($dependency));
                }else{
                    Yii::app()->cache->set($key,$row,ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency($dependency));    
                }*/
            }else{
                $row = $cache;
            }                        
            return $row;
        }

        public function countRowByParentPostId($parent_post_id){
            $parent_post_id = intval($parent_post_id);        
            $row = array();
            $cache = Yii::app()->cache;            
            if($cache != null){
                $cacheService = new CacheService("posts_link","countRowByParentPostId",$parent_post_id);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();                                                
                $cache = Yii::app ()->cache->get ( $key );
            }                        
            //$cache = false;
            if($cache == false){ 
                $sql = "SELECT count(*) FROM t186_posts_link INNER JOIN t186_posts ON t186_posts_link.post_id = t186_posts.id WHERE post_parent = ".$parent_post_id . " Order by t186_posts.id ASC";                
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryScalar();                
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );                
            }else{
                $row = $cache;
            }
            return $row;
        }

        public function getRowByParentPostId($parent_post_id){
            $parent_post_id = intval($parent_post_id);        
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("posts_link","getRowByParentPostId",$parent_post_id);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }                
            if($cache == false){ 
                $sql = "SELECT t186_posts_link.*,t186_posts.post_alias,t186_posts.post_title,t186_category.alias FROM t186_posts_link INNER JOIN t186_posts ON t186_posts_link.post_id = t186_posts.id INNER JOIN t186_category ON t186_category.id = t186_posts.category_id WHERE post_parent = ".$parent_post_id . " Order by t186_posts.id ASC";
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryAll();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }

        public function getRowByParentPostIdPaging($parent_post_id,$begin,$end){
            $parent_post_id = intval($parent_post_id);
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("posts_link","getRowByParentPostIdPaging",$parent_post_id.$begin.$end);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key );
            }            
            if($cache == false){
                $sql = "SELECT t186_posts_link.*,t186_posts.post_alias,t186_posts.post_title,t186_posts.update_date,t186_category.alias FROM t186_posts_link INNER JOIN t186_posts ON t186_posts_link.post_id = t186_posts.id INNER JOIN t186_category ON t186_category.id = t186_posts.category_id WHERE post_parent = ".$parent_post_id . " Order by t186_posts_link.order,t186_posts.id ASC LIMIT ".$begin.",".$end;
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryAll();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }

        public function getAllRows(){
            $rows = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("posts_link","getAllRows","");
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }            
            if($cache == false){ 
                $sql = "SELECT * FROM t186_posts_link";
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $rows = $command->queryAll();

                Yii::app ()->cache->set ( $key, $rows, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $rows = $cache;
            }
            return $rows;
        }    

        public function insertObject($array_input){
            $sql='';
            foreach($array_input as $key=>$value)
            {
                $sql.=$key."='".$value."',"; 
            }
            $sql='INSERT INTO t186_posts_link SET '.$sql;
            $sql=rtrim($sql,',');
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $command->execute();
            $record_id=Yii::app()->db->getLastInsertID();  
            return $record_id;
        }

        public function updateObject($array_input,$key_id,$key_value){
            $sql='';
            foreach($array_input as $key=>$value)
            {
                $sql.=$key."='".$value."',"; 
            }
            $sql=rtrim($sql,',');
            if($sql!='')
            {
                $sql='UPDATE t186_posts_link SET '.$sql.' WHERE '.$key_id.'='.$key_value;
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $a=$command->execute();
                return $a;
            }
            return -1;
        }    

        public function deleteObject($array_input){
            $sql=' 1 ';
            foreach($array_input as $key=>$value)
            {
                $sql.=" AND ".$key."='".$value."'"; 
            }
            $sql=rtrim($sql,',');
            if($sql!='')
            {
                $sql='delete from t186_posts_link where '.$sql.'';
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $a=$command->execute();
                if($a) return 1;
                else return 0;
            }
            else return 0;

        }

        public function insert($data){
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);        
            $command->insert('t186_posts_link', $data);    
            $record_id=Yii::app()->db->getLastInsertID();  
            return $record_id;
        }

        public function update($id,$data){
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);                
            return $command->update('t186_posts_link', $data,"id=".$id);        
        }

        public function delete($where){
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);                
            return $command->delete('t186_posts_link', $where);        
        }

    }

?>