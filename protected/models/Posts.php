<?php

    class Posts extends CActiveRecord{
        public static function model($className = __CLASS__) {
            return parent::model ( $className );
        }

        // dat luat cho model
        public function rules(){}         

        // goi den bang can ket noi   
        public function tableName() {                  
            return 't186_posts';
        }

        // nhan cac thuong tinh 
        public function attributeLabels(){}    

        public function getRowById($id){
            $id = intval($id);
            // $cache = false;
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","getRowById",$id);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }        
            //$cache = false;
            if($cache == false){ 
                $sql = "SELECT t186_posts.*,t186_category.name AS 'category_name' FROM t186_posts LEFT JOIN t186_category ON t186_posts.category_id = t186_category.id WHERE t186_posts.id =".$id;                      
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryRow();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }

        public function getRowByAlias($post_alias){                    
            $row = array();            
            $cache = Yii::app()->cache;                         
            if($cache != null){
                $cacheService = new CacheService("Posts","getRowByAlias",$post_alias);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }        
            //$cache = false;
            if($cache == false){ 
                $sql = "SELECT t186_posts.*,t186_category.name AS 'category_name',t186_author.author_name,t186_author.author_image,t186_author.author_alias FROM t186_posts LEFT JOIN t186_category ON t186_posts.category_id = t186_category.id LEFT JOIN t186_author ON t186_posts.author_id = t186_author.author_id WHERE t186_posts.post_status = 1 AND t186_posts.post_alias = '".$post_alias."'  ORDER BY id DESC";
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryRow();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }

            return $row;
        }

        public function getStaticDataByAlias($post_alias){
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","getStaticDataByAlias",$post_alias);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key );
            }
            $cache = false;
            if($cache == false){
                $sql = "SELECT post_views FROM t186_posts WHERE t186_posts.post_status = 1 AND t186_posts.post_alias = '".$post_alias."'  ORDER BY id DESC";
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryRow();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }

            return $row;
        }

        public function getDataByCategoryId($category_id){        
            $category_id = intval($category_id);
            // $cache = false;
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","getDataByCategoryId",$category_id);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }                
            //$cache = false;
            if($cache == false){             
                $sql = "SELECT id,post_title,post_image,post_alias,post_views,create_user,create_date FROM t186_posts WHERE post_status = 1 AND category_id = " . $category_id;
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryAll();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }

        public function getPostByCategoryIndex($category_id){        
            $category_id = intval($category_id);
            // $cache = false;
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","getPostByCategoryIndex",$category_id);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }                
            //$cache = false;
            if($cache == false){             
                $sql = "SELECT t186_posts.id,post_title,post_image,post_alias,post_views,create_user,create_date,t186_category.alias AS 'category_alias' FROM t186_posts INNER JOIN t186_category ON t186_posts.category_id = t186_category.id WHERE post_is_chap = 0 AND post_status = 1 AND post_is_hot = 1 AND category_id = " . $category_id . "  ORDER BY t186_posts.id DESC LIMIT 0,5";
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryAll();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }

        public function getPostRandomByCategory($category_id){        
            $category_id = intval($category_id);
            // $cache = false;
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","getPostRandomByCategory",$category_id);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }        
            //$cache = false;                    
            if($cache == false){             
                $sql = "SELECT t186_posts.id,t186_posts.id AS 't186p_id',post_title,post_image,post_alias,post_description,post_content,post_views,post_vote,post_comments,create_user,create_date,update_date,t186_category.alias,t186_author.author_name FROM t186_posts INNER JOIN t186_category ON t186_posts.category_id = t186_category.id LEFT JOIN t186_author ON t186_posts.author_id = t186_author.author_id WHERE post_is_chap = 0 AND post_status = 1 AND category_id = " . $category_id . " LIMIT 0,30";
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryAll();                                
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );                
            }else{
                $row = $cache;
            }
            return $row;
        }

        public function getPostOtherByCategory($category_id,$post_id){        
            $category_id = intval($category_id);
            // $cache = false;
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","getPostOtherByCategory",$category_id.$post_id);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }                
            //$cache = false;
            if($cache == false){             
                $sql = "SELECT t186_posts.id,post_title,post_image,post_alias,t186_category.alias AS 'category_alias' FROM t186_posts INNER JOIN t186_category ON t186_posts.category_id = t186_category.id WHERE post_is_chap = 0 AND post_status = 1 AND category_id = " . $category_id . " AND t186_posts.id < ".$post_id."  ORDER BY t186_posts.id DESC LIMIT 0,8";
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryAll();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }

        public function countDataByCategoryId($category_id,$sort_complete){
            $category_id = intval($category_id);
            // $cache = false;
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","countDataByCategoryId",$category_id.'s'.$sort_complete);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }                
            //$cache = false;
            if($cache == false){
	            if($sort_complete == 0){
		            $sql = "SELECT COUNT(*) FROM t186_posts WHERE post_status = 1 AND post_is_chap = 0 AND category_id = " . $category_id;
	            }else{
		            $sql = "SELECT COUNT(*) FROM t186_posts WHERE post_status = 1 AND post_is_chap = 0 AND post_end = 1 AND category_id like '%" . $category_id . ",%'";
	            }
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryScalar();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }

        public function getDataByCategoryIdPaging($category_id,$begin,$end,$sort_complete,$order){
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","getDataByCategoryIdPaging",$category_id.'a'.$begin.'a'.$end.'a'.$sort_complete.'a'.$order);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }
            //$cache=false;
            if($cache == false){
	            //$select_column = "id,id AS 't186p_id',post_title,category_id,post_image,post_alias,post_views,post_vote,post_end,post_is_full,post_comments,create_user,create_date,update_date,t186_author.author_name,(SELECT COUNT(*) FROM t186_posts_link WHERE post_parent = `t186p_id`) AS 'count_chap'";
	            $select_column = "id,id AS 't186p_id',post_title,category_id,post_image,post_alias,post_description,post_views,post_vote,post_end,post_is_full,post_comments,create_user,create_date,update_date,t186_author.author_name,t186_author.author_alias";
	            if($category_id == 0){
		            $sql = "SELECT ". $select_column . " FROM t186_posts LEFT JOIN t186_author ON t186_posts.author_id = t186_author.author_id WHERE post_status = 1 AND post_is_chap = 0 AND category_id not like '%1,%' AND category_id not like '%4,%' AND category_id not like '%22,%'";
	            }else{
		            $sql = "SELECT ". $select_column . " FROM t186_posts LEFT JOIN t186_author ON t186_posts.author_id = t186_author.author_id WHERE post_status = 1 AND post_is_chap = 0";
	            }
	            if($category_id != 0){
		            $sql .= " AND category_id like '%" . $category_id . ",%'";
	            }
	            if($sort_complete != 0){
		            $sql .= " AND post_end = 1";
	            }
                switch($order){
                    case "date":
                        $sql .= " ORDER BY update_date DESC, id DESC LIMIT ".$begin.",".$end;                                
                        break;
                    case "view":
                        $sql .= " ORDER BY post_views DESC, id DESC LIMIT ".$begin.",".$end;                                
                        break;
                    case "vote":
                        $sql .= " ORDER BY post_vote DESC, id DESC LIMIT ".$begin.",".$end;                                
                        break;
	                case "hot":
		                $sql .= " AND post_is_hot = 1 ORDER BY update_date DESC, id DESC LIMIT ".$begin.",".$end;
		                break;
                    default:
                        $sql .= " ORDER BY id DESC LIMIT ".$begin.",".$end;
                        break;
                }
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);            
                $row = $command->queryAll();            
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }

        public function getAllRows(){
            $rows = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","getAllRows","");
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }
            //$cache=false;
            if($cache == false){ 
                $sql = "SELECT * FROM t186_posts WHERE post_status = 1";
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $rows = $command->queryAll();

                Yii::app ()->cache->set ( $key, $rows, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $rows = $cache;
            }
            return $rows;
        }

        public function getSearch($where,$begin,$end){
            $rows = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","getSearch",$where.$begin.$end);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }
            //$cache=false;
            if($cache == false){ 
                $sql = "SELECT t186_posts.id,t186_posts.id AS 't186p_id',post_title,post_image,post_alias,post_description,post_content,post_end,post_is_full,post_views,post_vote,post_comments,create_user,create_date,update_date,t186_category.name as category_name,t186_category.alias,t186_author.author_name FROM t186_posts INNER JOIN t186_category ON t186_posts.category_id = t186_category.id LEFT JOIN t186_author ON t186_posts.author_id = t186_author.author_id WHERE post_status = 1 ".$where." ORDER BY t186_posts.id DESC";
                $sql .= " LIMIT ".$begin.",".$end;
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $rows = $command->queryAll();

                Yii::app ()->cache->set ( $key, $rows, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $rows = $cache;
            }
            return $rows;
        }

        public function countDataSearch($where){            
            // $cache = false;
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","countDataSearch",$where);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }                
            //$cache = false;
            if($cache == false){             
                $sql = "SELECT COUNT(*) FROM t186_posts WHERE post_status = 1 ". $where;            
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryScalar();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }

        public function getPostsMostViews(){
            $rows = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","getAllPostsHot","");
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }
            //$cache=false;
            if($cache == false){ 
                $sql = "SELECT t186_posts.id,post_title,post_image,post_alias,post_views,create_user,create_date,t186_category.alias AS 'category_alias' FROM t186_posts INNER JOIN t186_category ON t186_posts.category_id = t186_category.id WHERE post_status = 1 ORDER BY post_views DESC LIMIT 0,5";
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $rows = $command->queryAll();

                Yii::app ()->cache->set ( $key, $rows, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $rows = $cache;
            }
            return $rows;
        }

        public function getRanPostFeaturedByCategory($category_id){        
            $category_id = intval($category_id);
            // $cache = false;
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","getRanPostFeaturedByCategory",$category_id);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key );                                 
            }                                        
            //$cache = false;
            if($cache == false){                 
                if($category_id != 0){
                    $sql = "SELECT t186_posts.author_id,t186_posts.category_id,t186_posts.id,t186_posts.post_alias,t186_posts.post_description,t186_posts.post_content,t186_posts.post_image,t186_posts.post_title,t186_author.author_name,t186_author.author_alias,t186_category.alias FROM t186_posts INNER JOIN t186_category ON t186_posts.category_id = t186_category.id LEFT JOIN t186_author ON t186_posts.author_id = t186_author.author_id WHERE post_featured = 1 AND post_status = 1 AND category_id = ".$category_id." LIMIT 0,30";
                }else{
                    $sql = "SELECT t186_posts.author_id,t186_posts.category_id,t186_posts.id,t186_posts.post_alias,t186_posts.post_description,t186_posts.post_content,t186_posts.post_image,t186_posts.post_title,t186_author.author_name,t186_author.author_alias,t186_category.alias FROM t186_posts INNER JOIN t186_category ON t186_posts.category_id = t186_category.id LEFT JOIN t186_author ON t186_posts.author_id = t186_author.author_id WHERE post_featured = 1 AND post_status = 1 AND t186_category.status = 1 LIMIT 0,30";
                }                            
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryAll();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }

        public function insertObject($array_input){
            $sql='';
            foreach($array_input as $key=>$value)
            {
                $sql.=$key."='".$value."',"; 
            }
            $sql='INSERT INTO t186_posts SET '.$sql;
            $sql=rtrim($sql,',');
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $command->execute();
            $record_id=Yii::app()->db->getLastInsertID();  
            return $record_id;
        }

        public function updateObject($array_input,$key_id,$key_value){
            $sql='';
            foreach($array_input as $key=>$value)
            {
                $sql.=$key."='".$value."',"; 
            }
            $sql=rtrim($sql,',');
            if($sql!='')
            {
                $sql='UPDATE t186_posts SET '.$sql.' WHERE '.$key_id.'='.$key_value;
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $a=$command->execute();
                return $a;
            }
            return -1;
        }    

        public function deleteObject($array_input){
            $sql=' 1 ';
            foreach($array_input as $key=>$value)
            {
                $sql.=" AND ".$key."='".$value."'"; 
            }
            $sql=rtrim($sql,',');
            if($sql!='')
            {
                $sql='delete from t186_posts where '.$sql.'';
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $a=$command->execute();
                if($a) return 1;
                else return 0;
            }
            else return 0;

        }

        public function insert($data){
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);        
            $command->insert('t186_posts', $data);    
            $record_id=Yii::app()->db->getLastInsertID();  
            return $record_id;
        }

        public function update($id,$data){
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);                
            return $command->update('t186_posts', $data,"id=".$id);        
        }

        public function updatePostViews($id){
            $sql='UPDATE t186_posts SET `post_views` = `post_views` + 1 WHERE id = '.$id;            
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $a=$command->execute();
            return $a;
        }

        public function countChapByListId($list_id){            
            // $cache = false;
            $row = array();
            $cache = Yii::app()->cache;
            if($cache != null){
                $cacheService = new CacheService("Posts","countChapByListId",$list_id);
                $key = $cacheService->createKey();
                $dependency = $cacheService->createDependency();
                $cache = Yii::app ()->cache->get ( $key ); 
            }                
            //$cache = false;
            if($cache == false){             
                $sql = "SELECT COUNT(*) FROM t186_posts WHERE post_status = 1 AND post_is_chap = 0 AND category_id = " . $category_id;            
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $row = $command->queryScalar();
                Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
            }else{
                $row = $cache;
            }
            return $row;
        }

    }

?>