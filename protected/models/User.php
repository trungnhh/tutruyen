<?php
class User extends CActiveRecord{
    public static function model($className = __CLASS__) {
        return parent::model ( $className );
    }

    // dat luat cho model
    public function rules(){}         

    // goi den bang can ket noi   
    public function tableName() {                  
        return 't186_users';
    }

    // nhan cac thuong tinh 
    public function attributeLabels(){}

    public function getRowByUsername($username){
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("User","getRowByUsername",$username);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key );
            //echo $dependency;  
        }
        $cache=false;
        if($cache == false){ 
            $sql = "SELECT * FROM t186_users WHERE username='".$username."'";                       
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryRow();
            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }    
    
    public function getRowById($id){
        $id = intval($id);
        // $cache = false;
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("User","getRowById",$id);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }
        $cache=false;
        if($cache == false){ 
            $sql = "SELECT * FROM t186_users WHERE id=".$id;                      
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryRow();
            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }
    
    public function getRowByMobile($mobile){
        //$id = intval($id);
        // $cache = false;
        $row = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("User","getRowByMobile",$mobile);
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }
        //$cache=false;
        if($cache == false){ 
            $sql = "SELECT * FROM user WHERE mobile='".$mobile."'";           
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $row = $command->queryRow();

            Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $row = $cache;
        }
        return $row;
    }

	public function getRowByFbId($id){
		$id = intval($id);
		// $cache = false;
		$row = array();
		$cache = Yii::app()->cache;
		if($cache != null){
			$cacheService = new CacheService("User","getRowByFbId",$id);
			$key = $cacheService->createKey();
			$dependency = $cacheService->createDependency();
			$cache = Yii::app ()->cache->get ( $key );
		}
		$cache=false;
		if($cache == false){
			$sql = "SELECT * FROM t186_users WHERE fb_id=".$id;
			$connect = Yii::app()->db;
			$command = $connect->createCommand($sql);
			$row = $command->queryRow();
			Yii::app ()->cache->set ( $key, $row, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
		}else{
			$row = $cache;
		}
		return $row;
	}

    public function getRowByEmailAndPass($username,$pass){
        $connect = Yii::app()->db;
        $row = array();
        $sql = "SELECT * FROM t186_users WHERE `status` = 1 AND username='".$username."' AND password ='".md5($pass)."'";
        $command = $connect->createCommand($sql);
        $row = $command->queryRow();
        return $row;
    }
    
    public function getMd5($value){
        $val = md5($value);
        return $val;
    }
    
    public function getAllRows(){
        $rows = array();
        $cache = Yii::app()->cache;
        if($cache != null){
            $cacheService = new CacheService("user","getAllRows","");
            $key = $cacheService->createKey();
            $dependency = $cacheService->createDependency();
            $cache = Yii::app ()->cache->get ( $key ); 
        }
        $cache=false;
        if($cache == false){ 
            $sql = "SELECT * FROM t186_users";
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $rows = $command->queryAll();

            Yii::app ()->cache->set ( $key, $rows, ConstantsUtil::TIME_CACHE_10800, new CGlobalStateCacheDependency ( $dependency ) );
        }else{
            $rows = $cache;
        }
        return $rows;
    }
    
    public function insertObject($array_input){
            $sql='';
            foreach($array_input as $key=>$value)
            {
                $sql.=$key."='".$value."',"; 
            }
            $sql='INSERT INTO tbl_user SET '.$sql;
            $sql=rtrim($sql,',');
            $connect = Yii::app()->db;
            $command = $connect->createCommand($sql);
            $command->execute();
            $record_id=Yii::app()->db->getLastInsertID();  
            return $record_id;
        }
    
    public function updateObject($array_input,$key_id,$key_value){
            $sql='';
            foreach($array_input as $key=>$value)
            {
                $sql.=$key."='".$value."',"; 
            }
            $sql=rtrim($sql,',');
            if($sql!='')
            {
                $sql='UPDATE t186_users SET '.$sql.' WHERE '.$key_id.'='.$key_value;
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $a=$command->execute();
                return $a;
            }
            return -1;
        }    
    
    public function deleteObject($array_input){
            $sql=' 1 ';
            foreach($array_input as $key=>$value)
            {
                $sql.=" AND ".$key."='".$value."'"; 
            }
            $sql=rtrim($sql,',');
            if($sql!='')
            {
                $sql='delete from pm_user where '.$sql.'';
                $connect = Yii::app()->db;
                $command = $connect->createCommand($sql);
                $a=$command->execute();
                if($a) return 1;
                else return 0;
            }
            else return 0;

        }
    
    public function insert($data){
        $connect = Yii::app()->db;
        $command = $connect->createCommand();
        $command->insert('t186_users', $data);
        $record_id=Yii::app()->db->getLastInsertID();  
        return $record_id;
    }
    
    public function update($id,$data){
        $connect = Yii::app()->db;
        $command = $connect->createCommand();
        return $command->update('t186_users', $data,"id=".$id);        
    }
    
}
