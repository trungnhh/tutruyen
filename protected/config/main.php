<?php

return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'sautruyen | SauTruyen Team',
    'theme'=>'classic',
    // preloading 'log' component
    'preload'=>array('log'),
    // autoloading model and component classes
    'import'=>array(
        'application.models.*',
        'application.components.*',
        'application.extensions.url.*',
        'application.utilities.*',
        'zii.widgets.CPortlet',
    ),
    'defaultController'=>'Site',
    'modules' => array(
        'admincp',
    ),

    'onBeginRequest'=>create_function('$event', 'return ob_start("ob_gzhandler");'),
    'onEndRequest'=>create_function('$event', 'return ob_end_flush();'),

    'components'=>array(
        'user'=>array(
            'allowAutoLogin'=>true,
            'loginUrl' => array('/admincp/admin/login'),
            'returnUrl' =>"site/admincp",
        ),


        // uncomment the following to enable URLs in path-format

        'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'rules'=>array(
                ''=>'site/index',
                '/danh-muc/<alias>/page/<page>/view/<sort_view>/complete/<sort_complete>/'=>'category/index',
                '/danh-muc/<alias>/'=>'category/index',
                '/danh-muc/<alias>/page/<page:\d+>/'=>'category/index',
                '/tac-gia/<author_alias>/'=>'author/detail',
                '/tac-gia/<author_alias>/page/<page:\d+>/'=>'author/detail',
                '/tai-khoan/'=>'user/index',
                '/danh-dau/'=>'user/bookmark',
                '/danh-dau/page/<page:\d+>/'=>'user/bookmark',
                '/vip/'=>'user/vip',
                '/doi-matkhau/'=>'user/changepass',
                '/lien-he/'=>'site/contact',
                '/dieu-khoan/'=>'site/policy',
                '/truyen-hot/'=>'site/hot',
                '/truyen-hot/page/<page:\d+>/'=>'site/hot',
                '/truyen-moi/'=>'site/new',
                '/truyen-moi/page/<page:\d+>/'=>'site/new',
                '/truyen-hay/'=>'site/vote',
                '/truyen-hay/page/<page:\d+>/'=>'site/vote',
                /*'/tim-kiem/'=>'site/search',*/
                '/dang-nhap/'=>'user/login',
                '/yeu-cau-truyen/'=>'user/request',
                '/gop-y/'=>'user/note',
                '/<category_alias:(?!user|site|post|admin|category|author|transaction|news|danh-muc|messages)(.*)>/<post_alias>/'=>'post/detail',
                '/<category_alias:(?!user|site|post|admin|category|author|transaction|news|danh-muc|messages)(.*)>/<post_alias>/<post_password>'=>'post/detail',
            ),
        ),

        'cache' => array (
            'class'=>'system.caching.CFileCache',
            //    		'class' => 'CMemCache',
            //   			'servers'=>array(
            //        	array(
            //            	'host'=>'localhost',
            //            	'port'=>11211,
            //        		'weight'=>100,
            //        		),
            //    		),
        ),

        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=tutruyen',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'class'=>'CDbConnectionRewrite',

        ),

        'authManager'=>array(
            'class'=>'CDbAuthManager',
            'connectionID'=>'db',
        ),


        'errorHandler'=>array(
            'errorAction'=>'site/error',
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
            ),
        ),
    ),
    'params'=>require(dirname(__FILE__).'/params.php'),
);

?>
