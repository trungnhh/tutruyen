<?php
    return array(
        'title'             =>  'Sâu Truyện',
        'adminEmail'        =>  'trungnh28@gmail.com',
        'defaultPerPage'    =>  10,
        'urlJs'             =>  '/js',
        'urlRs'             =>  '',
        'urlRsAdmin'        =>  '/admin_rs',
        'urlImage'          =>  'http://images.sautruyen.local/data/upload_data',
        'showsql'           =>  true,
        'domain'            =>  "sautruyen.local/",
        'brand_ame'         =>  "SauTruyen",
        'brand_domain'       =>  "SauTruyen.Com",
        'base_url'          =>  "http://sautruyen.local/",
        'image_url'         =>  'http://images.sautruyen.local/data/',
        'image_base_url'    =>  'http://images.sautruyen.local/'
    );
?>