<?php

    class NewsController extends Controller {
        public $metaDescription = null;
        public $metaKeywords = null;
        public $linkCanoncical = null;
        public $title = null;
        public $layout = 'column_main';        

        public function actionError(){                
            if($error=Yii::app()->errorHandler->error)
            {
                //echo $error['message'];die;
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->renderPartial('error', $error);
            }
        }        

        public function actionIndex(){
            $url = new Url();
            $page = isset($_GET['page']) ? intval($_GET ['page']):1;
            $rows_per_page = 10;
            $begin = ($page - 1)*$rows_per_page;                        
            $end = $rows_per_page;            
            $where = "";            
            $count = News::countDataSearch($where);            
            if($count % $rows_per_page == 0)        
            {
                $totalpage = floor($count/$rows_per_page);
            }
            else
            {
                $totalpage = floor($count/$rows_per_page) + 1;                             
            }                    
            $util = new Paging();

            $data_news = News::getSearch($where,$begin,$end);                                                
                        
            $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("news/index"),"");                        
            $this->render("index",array('data_news'=>$data_news,'paging'=>$paging));
        }                 
     
        public function actionDetail(){
            $news_id = isset($_GET['news_id']) ? intval($_GET ['news_id']):1;
            $data_new = News::getRowById($news_id);
	        $where = " AND id <> " .$news_id;
	        $data_news_other = News::getSearch($where,0,5);
            $this->render("detail",array('data_new'=>$data_new,'data_news_other'=>$data_news_other));
        }
        
    }

?>
