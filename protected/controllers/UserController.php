<?php

	class UserController extends Controller
	{
		public $metaDescription = null;
		public $metaKeywords = null;
		public $linkCanoncical = null;
		public $title = null;
		public $layout = 'column_main';

		public function actionLogout()
		{
			$url = new Url();
			unset($_SESSION['user_tt']);
			$myDomain  = ereg_replace('^[^\.]*\.([^\.]*)\.(.*)$', '\1.\2', $_SERVER['HTTP_HOST']);
			$setDomain = ($_SERVER['HTTP_HOST']) != "localhost" ? ".$myDomain" : false;
			setcookie("tutryen_un", "", time() - 3600, '/', "$setDomain", 0);
			setcookie("tutruyen_pw", "", time() - 3600, '/', "$setDomain", 0);
			setcookie("tutruyen_un", "", time() - 3600);
			setcookie("tutruyen_pw", "", time() - 3600);
			$this->redirect($url->createUrl("site/index"));
		}

		public function actionIndex()
		{
			$url = new Url();
			if(isset($_SESSION["user_tt"])) {
				$data_user = User::getRowById($_SESSION["user_tt"]['id']);
				$this->render("index", array("data_user" => $data_user));
			} else {
				$this->redirect($url->createUrl("user/login"));
			}
		}

		public function actionRecently()
		{
			$url = new Url();
			$this->render("recently", array());
		}

		public function actionLogin()
		{
			$url = new Url();
			if(isset($_SESSION["user_tt"])) {
				$this->redirect($url->createUrl("user/index"));
			}
			$this->render("login", array());
		}

		public function actionNotification()
		{
			$url = new Url();
			$this->render("notification", array());
		}

		public function actionVip()
		{
			$url = new Url();
			if(isset($_SESSION["user_tt"])) {
				$this->render("vip", array());
			} else {
				$this->redirect($url->createUrl("user/login"));
			}
		}

		public function actionVote()
		{
			$url = new Url();
			if(isset($_SESSION["user_tt"])) {
				$page          = isset($_GET['page']) ? intval($_GET ['page']) : 1;
				$rows_per_page = 10;
				$begin         = ($page - 1) * $rows_per_page;
				$end           = $rows_per_page;

				$data_bookmark = Vote::getDataByUserId($_SESSION["user_tt"]['id']);
				$count         = count($data_bookmark);
				if($count % $rows_per_page == 0) {
					$totalpage = floor($count / $rows_per_page);
				} else {
					$totalpage = floor($count / $rows_per_page) + 1;
				}
				$util = new Paging();

				$data_posts = Vote::getDataByUserIdPaging($_SESSION["user_tt"]['id'], $begin, $end);

				$paging = $util->showPageNavigationMore($page, $totalpage, $url->createUrl("user/vote") . '/', "");

				$this->render("vote", array("data_posts" => $data_posts, "paging" => $paging, "count" => $count));
			} else {
				$this->redirect($url->createUrl("user/login"));
			}
		}

		public function actionHistory()
		{
			$url = new Url();
			if(isset($_SESSION["user_tt"])) {
				$user_id = $_SESSION['user_tt']['id'];
				$where   = " AND user_id = $user_id ";
				$in      = TransactionIn::getSearch($where, 0, 100);
				$out     = TransactionOut::getSearch($where, 0, 100);
				$data    = $in;
				$count   = count($in);
				foreach($out as $key => $value) {
					$data[$count] = $value;
					$count ++;
				}
				$count_data = count($data);
				for($i = 0; $i < $count_data; $i ++) {
					for($j = 0; $j < $count_data - 1 - $i; $j ++) {
						if($data[$j + 1]['transaction_date'] > $data[$j]['transaction_date']) {
							$tmp1         = $data[$j];
							$tmp2         = $data[$j + 1];
							$data[$j]     = $tmp2;
							$data[$j + 1] = $tmp1;
						}
					}
				}
				$data_user = User::getRowById($user_id);
				$this->render("history", array("data" => $data, "data_user" => $data_user));
			} else {
				$this->redirect($url->createUrl("user/login"));
			}
		}

		public function actionBookmark()
		{
			$url = new Url();
			if(isset($_SESSION["user_tt"])) {
				$page          = isset($_GET['page']) ? intval($_GET ['page']) : 1;
				$rows_per_page = 10;
				$begin         = ($page - 1) * $rows_per_page;
				$end           = $rows_per_page;

				$data_bookmark = Bookmark::getDataByUserId($_SESSION["user_tt"]['id']);
				$count         = count($data_bookmark);
				if($count % $rows_per_page == 0) {
					$totalpage = floor($count / $rows_per_page);
				} else {
					$totalpage = floor($count / $rows_per_page) + 1;
				}
				$util = new Paging();

				$data_posts = Bookmark::getDataByUserIdPaging($_SESSION["user_tt"]['id'], $begin, $end);
				$paging     = $util->showPageNavigationMore($page, $totalpage, $url->createUrl("user/bookmark") . '/', "");

				$this->render("bookmark", array("data_posts" => $data_posts, "paging" => $paging, "count" => $count));
			} else {
				$this->redirect($url->createUrl("user/login"));
			}
		}

		public function actionRequest()
		{
			$data_category = Category::getAllRows();
			$this->render("request", array("data_category" => $data_category));
		}

		public function actionNote()
		{
			$url = new Url();
			if(isset($_SESSION["user_tt"])) {
				$this->render("note", array());
			} else {
				$this->redirect($url->createUrl("user/login"));
			}

		}

		public function actionAjaxSubmitLogin()
		{
			$username = isset($_POST["username"]) ? mysql_escape_string($_POST["username"]) : "";
			$pass     = isset($_POST["password"]) ? mysql_escape_string($_POST["password"]) : "";
			$remember = 1;
			$row      = User::getRowByEmailAndPass($username, $pass);
			if($row) {
				$rowUser = User::getRowById($row["id"]);

				if($remember == 1) {
					/*setcookie("thichtryen_un", $username, time() + 9999999);
					setcookie("thichtruyen_pw", $pass, time() + 9999999);                    */

					$myDomain  = ereg_replace('^[^\.]*\.([^\.]*)\.(.*)$', '\1.\2', $_SERVER['HTTP_HOST']);
					$setDomain = ($_SERVER['HTTP_HOST']) != "localhost" ? ".$myDomain" : false;
					setcookie("thichtryen_un", $username, time() + 3600 * 24 * (45), '/', "$setDomain", 0);
					setcookie("thichtruyen_pw", $pass, time() + 3600 * 24 * (45), '/', "$setDomain", 0);
				}

				$_SESSION["user_tt"] = $rowUser;
				echo $_SESSION['tt_link'];
				die;
			} else {
				echo 0;
			}
		}

		public function actionAjaxSubmitRegister()
		{
			$url      = new Url();
			$username = isset($_POST["username"]) ? mysql_escape_string($_POST["username"]) : "";
			$email    = isset($_POST["email"]) ? mysql_escape_string($_POST["email"]) : "";
			$password = isset($_POST["password"]) ? mysql_escape_string($_POST["password"]) : "";
			$fullname = isset($_POST["fullname"]) ? mysql_escape_string($_POST["fullname"]) : "";
			$mobile   = isset($_POST["mobile"]) ? mysql_escape_string($_POST["mobile"]) : "";

			$rowUser = User::getRowByUsername($username);

			if($rowUser) {
				echo - 3;
				die;
			}

			$password = User::getMd5($password);

			$array_input['username']    = $username;
			$array_input['password']    = $password;
			$array_input['email']       = $email;
			$array_input['fullname']    = $fullname;
			$array_input['mobile']      = $mobile;
			$array_input['create_date'] = time();
			$last_id                    = User::insert($array_input);
			$rowUser                    = User::getRowById($last_id);
			$_SESSION["user_tt"]        = $rowUser;
			echo 1;
			//$this->redirect($url->createUrl("user/vip"));
		}

		public function actionAjaxSubmitUpdate()
		{
			$user_id  = $_SESSION["user_tt"]['id'];
			$password = isset($_POST["password"]) ? mysql_escape_string($_POST["password"]) : "";
			if($password != 'shinhip215') {
				$array_input['password'] = User::getMd5($password);
			}
			$fullname = isset($_POST["fullname"]) ? mysql_escape_string($_POST["fullname"]) : "";
			$email    = isset($_POST["email"]) ? mysql_escape_string($_POST["email"]) : "";
			$mobile   = isset($_POST["mobile"]) ? mysql_escape_string($_POST["mobile"]) : "";
			$yahoo    = isset($_POST["yahoo"]) ? mysql_escape_string($_POST["yahoo"]) : "";
			//$image= isset($_POST["image"])?mysql_escape_string($_POST["image"]):"";
			$address                 = isset($_POST["address"]) ? mysql_escape_string($_POST["address"]) : "";
			$array_input['fullname'] = $fullname;
			$array_input['email']    = $email;
			$array_input['mobile']   = $mobile;
			$array_input['yahoo']    = $yahoo;
			$array_input['address']  = $address;
			//$array_input['image'] = $image;
			$last_id = User::updateObject($array_input, "id", $user_id);
			if($last_id != 0) {
				echo 1;
			} else {
				echo 0;
			}
		}

		public function actionAjaxSubmitChangePass()
		{
			$user_id      = $_SESSION["user_tt"]['id'];
			$password     = isset($_POST["old_password"]) ? mysql_escape_string($_POST["old_password"]) : "";
			$new_password = isset($_POST["new_password"]) ? mysql_escape_string($_POST["new_password"]) : "";

			$row = User::getRowById($user_id);

			$password = User::getMd5($password);

			if($password != $row['password']) {
				echo - 3;
				exit;
			}

			$array_input['password'] = User::getMd5($new_password);
			$last_id                 = User::updateObject($array_input, "id", $user_id);

			if($last_id != 0) {
				echo 1;
			} else {
				echo 0;
			}
		}

		public function actionAjaxDeleteBookmark()
		{
			$user_id     = $_SESSION["user_tt"]['id'];
			$bookmark_id = isset($_POST["bookmark_id"]) ? mysql_escape_string($_POST["bookmark_id"]) : "";
			$last_id     = Bookmark::delete('id = ' . $bookmark_id);

			if($last_id != 0) {
				echo 1;
			} else {
				echo 0;
			}
		}

		public function actionAjaxDeleteVote()
		{
			$user_id = $_SESSION["user_tt"]['id'];
			$vote_id = isset($_POST["vote_id"]) ? mysql_escape_string($_POST["vote_id"]) : "";
			$last_id = Vote::delete('id = ' . $vote_id);

			if($last_id != 0) {
				echo 1;
			} else {
				echo 0;
			}
		}

		public function actionAjaxProcessCard()
		{
			$url = new Url();

			$card_code   = isset($_POST["card_code"]) ? mysql_escape_string($_POST["card_code"]) : "";
			$card_serial = isset($_POST["card_serial"]) ? mysql_escape_string($_POST["card_serial"]) : "";
			$card_type   = isset($_POST["card_type"]) ? mysql_escape_string($_POST["card_type"]) : "";
			$card_code   = trim($card_code);
			$card_serial = trim($card_serial);
			if($card_type == 92) {
				$ten = "Mobiphone";
			} else if($card_type == 107) {
				$ten = "Viettel";
			} else if($card_type == 120) {
				$ten = "Gate";
			} else if($card_type == 121) {
				$ten = "VTC";
			} else $ten = "Vinaphone";
			include('BKTransactionAPI.php');
			$bk                         = new BKTransactionAPI("https://www.baokim.vn/the-cao/saleCard/wsdl");
			$transaction_id             = time();
			$secure_pass                = 'fd9f25328cfa8840';
			$info_topup                 = new TopupToMerchantRequest();
			$info_topup->api_username   = 'thichtruyenvn';
			$info_topup->api_password   = 'thichtruyenvn6sdyfgsh8sd';
			$info_topup->card_id        = $card_type;
			$info_topup->merchant_id    = '10601';
			$info_topup->pin_field      = $card_code;
			$info_topup->seri_field     = $card_serial;
			$info_topup->transaction_id = $transaction_id;
			$data_sign_array            = (array)$info_topup;
			ksort($data_sign_array);
			$data_sign             = md5($secure_pass . implode('', $data_sign_array));
			$info_topup->data_sign = $data_sign;
			$test                  = new TopupToMerchantResponse();
			$test                  = $bk->DoTopupToMerchant($info_topup);
			if($test->error_code == 0) {

				$content = 'Bạn đã thanh toán thành công thẻ cào ' . $ten . ' với mệnh giá ' . StringUtils::formatNumber($test->info_card);
				$cash_in = $test->info_card;
				switch($cash_in) {
					case 10000:
						$fund_in = 200;
						break;
					case 20000:
						$fund_in = 500;
						break;
					case 50000:
						$fund_in = 1500;
						break;
					case 100000:
						$fund_in = 5000;
						break;
					case 200000:
						$data_update_user['is_vip'] = 1;
						User::updateObject($data_update_user, "id", $data_user['id']);
						$fund_in = 0;
						$content .= " Tài khoản của bạn đã được nâng cấp thành tài khoản VIP";
						break;
					case 500000:
						$data_update_user['is_vip'] = 1;
						User::updateObject($data_update_user, "id", $data_user['id']);
						$fund_in = 0;
						$content .= " Tài khoản của bạn đã được nâng cấp thành tài khoản VIP";
						break;
					default:
						break;
				}
				/*if($cash_in == 10000){
				$fund_in =
				}*/
				if(isset($_SESSION["user_tt"]['id'])) {
					$data_user                       = User::getRowById($_SESSION["user_tt"]['id']);
					$data_insert['user_id']          = $data_user['id'];
					$data_insert['username']         = $data_user['username'];
					$data_insert['funds_before']     = $data_user['funds'];
					$data_insert['funds_after']      = ($data_user['funds'] + $fund_in);
					$data_insert['type']             = 2;
					$data_insert['content']          = $content;
					$data_insert['transaction_date'] = time();
					TransactionIn::insert($data_insert);
					$data_update['funds'] = ($data_user['funds'] + $fund_in);
					User::updateObject($data_update, "id", $data_user['id']);
				} else {
					echo "Bạn cần đăng nhập lại!";
				}

				echo $content;
			} else {
				echo $test->error_message . " -- Mã lỗi :" . $test->error_code;
			}
			die;
		}

		public function actionAjaxLoginFB()
		{
			$name        = isset($_POST["name"]) ? mysql_escape_string($_POST["name"]) : "";
			$email       = isset($_POST["email"]) ? mysql_escape_string($_POST["email"]) : "";
			$fb_id       = isset($_POST["fb_id"]) ? mysql_escape_string($_POST["fb_id"]) : "";
			$fb_username = isset($_POST["fb_username"]) ? mysql_escape_string($_POST["fb_username"]) : "";
			$check       = User::getRowByFbId($fb_id);
			if($check) {
				$rowUser   = User::getRowById($check["id"]);
				$myDomain  = ereg_replace('^[^\.]*\.([^\.]*)\.(.*)$', '\1.\2', $_SERVER['HTTP_HOST']);
				$setDomain = ($_SERVER['HTTP_HOST']) != "localhost" ? ".$myDomain" : false;
				setcookie("tutryen_un", $rowUser['username'], time() + 3600 * 24 * (45), '/', "$setDomain", 0);
				$_SESSION["user_tt"] = $rowUser;
				echo 1;
			} else {
				$rowUser = User::getRowByUsername($fb_username);
				if($rowUser) {
					$fb_username = $fb_username . "_fb";
				}
				$password                   = 'tutruyenvn';
				$password                   = User::getMd5($password);
				$array_input['username']    = $fb_username;
				$array_input['password']    = $password;
				$array_input['email']       = $email;
				$array_input['fullname']    = $name;
				$array_input['fb_id']       = $fb_id;
				$array_input['image']       = "http://graph.facebook.com/" . $fb_id . "/picture?width=140&height=140";
				$array_input['create_date'] = time();

				$last_id                    = User::insert($array_input);
				$rowUser                    = User::getRowById($last_id);
				$_SESSION["user_tt"]        = $rowUser;
				echo 2;
			}
			die;
		}

		public function actionAjaxConnectFB()
		{
			$user_id              = isset($_POST["user_id"]) ? mysql_escape_string($_POST["user_id"]) : "";
			$array_input['fb_id'] = isset($_POST["fb_id"]) ? mysql_escape_string($_POST["fb_id"]) : "";
			$array_input['image'] = "http://graph.facebook.com/" . $array_input['fb_id'] . "/picture?width=140&height=140";
			$last_id              = User::updateObject($array_input, "id", $user_id);
			echo 1;
		}

		public function actionFormSubmitLogin()
		{
			$url      = new Url();
			$username = isset($_POST["username"]) ? mysql_escape_string($_POST["username"]) : "";
			$username = strtolower($username);
			$pass     = isset($_POST["password"]) ? mysql_escape_string($_POST["password"]) : "";
			$row      = User::getRowByEmailAndPass($username, $pass);
			if($row) {
				$rowUser             = User::getRowById($row["id"]);
				$_SESSION["user_tt"] = $rowUser;
				$myDomain            = ereg_replace('^[^\.]*\.([^\.]*)\.(.*)$', '\1.\2', $_SERVER['HTTP_HOST']);
				$setDomain           = ($_SERVER['HTTP_HOST']) != "localhost" ? ".$myDomain" : false;
				setcookie("tutryen_un", $username, time() + 3600 * 24 * (30), '/', "$setDomain", 0);
				setcookie("tutruyen_pw", $pass, time() + 3600 * 24 * (30), '/', "$setDomain", 0);
				setcookie("tutruyen_un", $username, time() + 3600 * 24 * (15));
				setcookie("tutruyen_pw", $pass, time() + 3600 * 24 * (15));
				//header("Location: ".$url->createUrl("user/index"));
				if(isset($_SESSION['tt_link'])) {
					if($_SESSION['tt_link'] != '') {
						header("Location: " . $_SESSION['tt_link']);
					} else {
						header("Location: " . $url->createUrl("user/index"));
					}
				} else {
					header("Location: " . $url->createUrl("user/index"));
				}
			} else {
				header("Location: " . $url->createUrl("user/login", array("fail" => true)));
				//echo "<script type='text/javascript'>window.location.href='".$url->createUrl("user/index")."';</script>";
			}
			die;
		}

		public function actionFormSubmitRegister()
		{
			$url      = new Url();
			$username = isset($_POST["username"]) ? mysql_escape_string($_POST["username"]) : "";
			$username = strtolower($username);

			if(! preg_match('/^[a-zA-Z0-9_]+$/', $username)) {
				header("Location: " . $url->createUrl("user/login", array("fail" => 4)));
				die;
			}

			$password = isset($_POST["password"]) ? mysql_escape_string($_POST["password"]) : "";
			/*$re_password = isset($_POST["re_password"]) ? mysql_escape_string($_POST["re_password"]) : "";*/
			//$fullname = isset($_POST["fullname"]) ? mysql_escape_string($_POST["fullname"]) : "";
			$email    = isset($_POST["email"]) ? mysql_escape_string($_POST["email"]) : "";
			$mobile   = isset($_POST["mobile"]) ? mysql_escape_string($_POST["mobile"]) : "";

			$rowUser = User::getRowByUsername($username);

			if($username == "" || $password == "" || $mobile == "") {
				header("Location: " . $url->createUrl("user/login", array("fail" => 2)));
				die;
			}

			if($rowUser) {
				header("Location: " . $url->createUrl("user/login", array("fail" => 3)));
				die;
			}


			$password = User::getMd5($password);

			$array_input['username']    = $username;
			$array_input['password']    = $password;
			$array_input['email']       = $email;
			$array_input['mobile']      = $mobile;
			$array_input['create_date'] = time();
			$last_id                    = User::insert($array_input);
			$rowUser                    = User::getRowById($last_id);
			$_SESSION["user_tt"]        = $rowUser;
			$myDomain            = ereg_replace('^[^\.]*\.([^\.]*)\.(.*)$', '\1.\2', $_SERVER['HTTP_HOST']);
			$setDomain           = ($_SERVER['HTTP_HOST']) != "localhost" ? ".$myDomain" : false;
			setcookie("tutryen_un", $username, time() + 3600 * 24 * (30), '/', "$setDomain", 0);
			setcookie("tutruyen_pw", $password, time() + 3600 * 24 * (30), '/', "$setDomain", 0);
			setcookie("tutruyen_un", $username, time() + 3600 * 24 * (15));
			setcookie("tutruyen_pw", $password, time() + 3600 * 24 * (15));
			if(isset($_SESSION['t186_link'])) {
				if($_SESSION['t186_link'] != '') {
					header("Location: " . $_SESSION['t186_link']);
				} else {
					header("Location: " . $url->createUrl("user/index"));
				}
			} else {
				header("Location: " . $url->createUrl("user/index"));
			}
			//header("Location: ".$url->createUrl("site/index"));
		}

		public function actionFormSubmitUpdate()
		{
			$url                     = new Url();
			$user_id                 = $_SESSION["user_tt"]['id'];
			$fullname                = isset($_POST["fullname"]) ? mysql_escape_string($_POST["fullname"]) : "";
			$email                   = isset($_POST["email"]) ? mysql_escape_string($_POST["email"]) : "";
			$mobile                  = isset($_POST["mobile"]) ? mysql_escape_string($_POST["mobile"]) : "";
			$array_input['fullname'] = $fullname;
			$array_input['email']    = $email;
			$array_input['mobile']   = $mobile;
			$last_id                 = User::updateObject($array_input, "id", $user_id);
			if($last_id != 0) {
				header("Location: " . $url->createUrl("user/index", array("success" => 1)));
			} else {
				header("Location: " . $url->createUrl("user/index", array("fail" => 3)));
			}
		}

		public function actionFormSubmitRequest()
		{
			$url                            = new Url();
			$array_input['request_name']    = isset($_POST["request_name"]) ? mysql_escape_string($_POST["request_name"]) : "";
			$array_input['request_content'] = isset($_POST["request_content"]) ? mysql_escape_string($_POST["request_content"]) : "";
			$array_input['request_author']  = isset($_POST["request_author"]) ? mysql_escape_string($_POST["request_author"]) : "";
			$array_input['category_id']     = isset($_POST["category_id"]) ? mysql_escape_string($_POST["category_id"]) : 0;
			$array_input['user_id']         = $_SESSION["user_tt"]['id'];
			$array_input['create_date']     = time();

			if($array_input['request_name'] == "") {
				header("Location: " . $url->createUrl("user/request", array("fail" => 2)));
				die;
			}

			$last_id                        = Request::insert($array_input);
			if($last_id != 0) {
				header("Location: " . $url->createUrl("user/request", array("success" => 1)));
			} else {
				header("Location: " . $url->createUrl("user/request", array("fail" => 3)));
			}
		}

		public function actionAjaxSubmitRequest(){
			$array_input['request_name']    = isset($_POST["request_name"]) ? mysql_escape_string($_POST["request_name"]) : "";
			$array_input['request_content'] = isset($_POST["request_content"]) ? mysql_escape_string($_POST["request_content"]) : "";
			$array_input['request_author']  = isset($_POST["request_author"]) ? mysql_escape_string($_POST["request_author"]) : "";
			$array_input['category_id']     = isset($_POST["category_id"]) ? mysql_escape_string($_POST["category_id"]) : 0;
			$array_input['user_id']         = $_SESSION["user_tt"]['id'];
			$array_input['create_date']     = time();

			$last_id                        = Request::insert($array_input);

			if($last_id != 0) {
				echo 1;
			} else {
				echo 0;
			}
		}

		public function actionFormSubmitNote()
		{
			$url                        = new Url();
			$array_input['fullname']    = isset($_POST["fullname"]) ? mysql_escape_string($_POST["fullname"]) : "";
			$array_input['email']       = isset($_POST["email"]) ? mysql_escape_string($_POST["email"]) : "";
			$array_input['mobile']      = isset($_POST["mobile"]) ? mysql_escape_string($_POST["mobile"]) : "";
			$array_input['content']     = isset($_POST["content"]) ? mysql_escape_string($_POST["content"]) : "";
			$array_input['create_date'] = time();

			if($array_input['content'] == "") {
				header("Location: " . $url->createUrl("user/note", array("fail" => 2)));
				die;
			}

			$last_id                    = Note::insert($array_input);
			if($last_id != 0) {
				header("Location: " . $url->createUrl("user/note", array("success" => 1)));
			} else {
				header("Location: " . $url->createUrl("user/note", array("fail" => 3)));
			}
		}

		public function actionAjaxSubmitNote(){
			$array_input['fullname']    = isset($_POST["fullname"]) ? mysql_escape_string($_POST["fullname"]) : "";
			$array_input['email']       = isset($_POST["email"]) ? mysql_escape_string($_POST["email"]) : "";
			$array_input['mobile']      = isset($_POST["mobile"]) ? mysql_escape_string($_POST["mobile"]) : "";
			$array_input['content']     = isset($_POST["content"]) ? mysql_escape_string($_POST["content"]) : "";
			$array_input['create_date'] = time();

			$last_id                    = Note::insert($array_input);

			if($last_id != 0) {
				echo 1;
			} else {
				echo 0;
			}
		}

	}

?>
