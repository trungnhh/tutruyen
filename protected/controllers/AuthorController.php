<?php

    class AuthorController extends Controller {
        public $metaDescription = null;
        public $metaKeywords = null;
        public $linkCanoncical = null;
        public $title = null;
        public $layout = 'column_main';        

        public function actionError(){                
            if($error=Yii::app()->errorHandler->error)
            {
                //echo $error['message'];die;
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->renderPartial('error', $error);
            }
        }        

        public function actionIndex(){
            $url = new Url();
            $page = isset($_GET['page']) ? intval($_GET ['page']):1;
            $rows_per_page = 10;
            $begin = ($page - 1)*$rows_per_page;                        
            $end = $rows_per_page;            
            $where = " AND author_status = 1";                        
            $count = Authors::countDataSearch($where);
            if($count % $rows_per_page == 0)        
            {
                $totalpage = floor($count/$rows_per_page);
            }
            else
            {
                $totalpage = floor($count/$rows_per_page) + 1;                             
            }                    
            $util = new Paging();

            $data_author = Authors::getSearch($where,$begin,$end);
                        
            $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("author/index").'',"");
            $this->render("index",array('data_author'=>$data_author,'paging'=>$paging));
        }                 
     
        public function actionDetail(){
            $url = new Url();    
            $author_alias = isset($_GET['author_alias']) ? $_GET ['author_alias']:"";
	        $page = isset($_GET['page']) ? intval($_GET ['page']):1;
	        $rows_per_page = 10;
	        $begin = ($page - 1)*$rows_per_page;
	        $end = $rows_per_page;
            $data_author = Authors::getRowByAlias($author_alias);
	        $data_category = Category::getAllRows();
            $where = " AND post_is_chap = 0 AND t186_posts.author_id = " . $data_author['author_id'];

	        $count = Posts::countDataSearch($where);
	        if($count % $rows_per_page == 0)
	        {
		        $totalpage = floor($count/$rows_per_page);
	        }
	        else
	        {
		        $totalpage = floor($count/$rows_per_page) + 1;
	        }
	        $util = new Paging();

            $data_posts = Posts::getSearch($where,$begin,$end);
	        $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("author/detail",array("author_alias"=>$author_alias)).'/',"");
            $this->render("detail",array('data_author'=>$data_author,'data_posts'=>$data_posts,'data_category'=>$data_category,'paging'=>$paging));
        }
        
    }

?>
