<?php

    class SiteController extends Controller {
        public $metaDescription = null;
        public $metaKeywords = null;
        public $linkCanoncical = null;
        public $title = null;
        public $layout = 'column_main';

	    public function init(){
			/*Yii::app()->theme = "smartphone";
		    parent::init();*/
	    }

        public function actionError(){                
            if($error=Yii::app()->errorHandler->error)
            {                
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    //$this->renderPartial('error', $error);
                    $this->render('error', $error);
            }
        }

        public function actionIndex(){                    
            $url = new Url();     
            $this->layout = "column_main";

            $begin = 0;
            $end = 10;

	        $data_posts_new = Posts::getDataByCategoryIdPaging(0,$begin,$end,0,"date");
	        $data_posts_hot = Posts::getDataByCategoryIdPaging(0,$begin,$end,0,"view");
	        $data_posts_vote = Posts::getDataByCategoryIdPaging(0,$begin,$end,0,"hot");

	        $data_category_hot = Category::getAllRowsIsHot();
	        for($i=0;$i<count($data_category_hot);$i++){
		        $data_posts[$i] = Posts::getDataByCategoryIdPaging($data_category_hot[$i]['id'],$begin,$end,0,"date");
	        }

	        $data_category = Category::getAllRows();
	        
            $this->render("index",array('data_posts_new'=>$data_posts_new,'data_posts_hot'=>$data_posts_hot,'data_posts_vote'=>$data_posts_vote,'data_category'=>$data_category,'data_posts'=>$data_posts,'data_category_hot'=>$data_category_hot));
        }

	    public function actionAjaxLoadMoreCategory(){
		    $begin = 0;
		    $end = 10;
		    $data_category_hot = Category::getAllRowsIsHot();
		    for($i=0;$i<count($data_category_hot);$i++){
			    $data_posts[$i] = Posts::getDataByCategoryIdPaging($data_category_hot[$i]['id'],$begin,$end,0,"date");
		    }
		    $this->renderPartial("load_more_category",array('data_posts'=>$data_posts,'data_category_hot'=>$data_category_hot));
	    }

        public function actionSearch(){
            $url = new Url();
            $data_category = Category::getAllRows();
            $data_post = array();
            if(isset($_GET['keyword']) || isset($_GET['category_id']) || isset($_GET['post_is_full'])){
                $category_id = isset($_GET["category_id"])?mysql_escape_string($_GET["category_id"]):"0";
                $keyword = isset($_GET["keyword"])?mysql_escape_string($_GET["keyword"]):"";
                $is_full = isset($_GET["post_is_full"])?mysql_escape_string($_GET["post_is_full"]):"0";
                $page = isset($_GET['page']) ? intval($_GET ['page']):1;
                $rows_per_page = 10;
                $begin = ($page - 1)*$rows_per_page;
                $end = $rows_per_page;
                $where = "AND 1 = 1 ";
                if($category_id != 0){
                    $where = $where . " AND category_id = " . $category_id;
                }
                if($is_full == 1){
                    $where = $where . " AND post_is_full = 1";
                }
                $where = $where . " AND post_is_chap = 0 ";
                if($keyword != ""){
                    $keyword = trim($keyword);
                    $alias = StringUtils::removeTitle($keyword,'');
                    $alias = strtolower($alias);
                    $where = $where . " AND (post_title LIKE '%".$keyword."%' OR post_alias LIKE '%".$alias."%')";
                }
                $count = Posts::countDataSearch($where);
                if($count % $rows_per_page == 0)
                {
                    $totalpage = floor($count/$rows_per_page);
                }
                else
                {
                    $totalpage = floor($count/$rows_per_page) + 1;
                }
                $util = new Paging();
                $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("site/search",array("category_id"=>$category_id,"keyword"=>$keyword,"post_is_full"=>$is_full)).'/',"");
                $data_posts = Posts::getSearch($where,$begin,$end);
            }
            $this->render("search",array('data_category'=>$data_category,'data_posts'=>$data_posts,'paging'=>$paging,'count'=>$count));
        }                

        public function actionContact(){
            $data_posts = array();
            $this->render("contact",array('data_posts'=>$data_posts));
        }

	    public function actionPolicy(){
		    $data_posts = array();
		    $this->render("policy",array('data_posts'=>$data_posts));
	    }

	    public function actionHot(){
		    $url = new Url();
		    $this->layout = "column_main";
		    $category_alias = isset($_GET["alias"])?$_GET["alias"]:"";
		    if($category_alias == ''){
			    $page = isset($_GET['page']) ? intval($_GET ['page']):1;
			    $rows_per_page = 10;
			    $begin = ($page - 1)*$rows_per_page;
			    $end = $rows_per_page;
				$title = "Truyện HOT";

			    $where_sql = 'AND post_is_chap = 0 AND category_id <> 1 AND category_id <> 4 AND category_id <> 22';
			    $count = Posts::countDataSearch($where_sql);
			    if($count % $rows_per_page == 0)
			    {
				    $totalpage = floor($count/$rows_per_page);
			    }
			    else
			    {
				    $totalpage = floor($count/$rows_per_page) + 1;
			    }
			    $util = new Paging();

			    $data_posts = Posts::getDataByCategoryIdPaging(0,$begin,$end,0,"view");
			    $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("site/hot").'/',"");

			    $data_category = Category::getAllRows();

			    $this->render("hot",array('data_posts'=>$data_posts,'data_category'=>$data_category,'paging'=>$paging,'title'=>$title));
		    }else{
			    $page = isset($_GET['page']) ? intval($_GET ['page']):1;
			    $rows_per_page = 10;
			    $begin = ($page - 1)*$rows_per_page;
			    $end = $rows_per_page;
			    $data_category = Category::getRowByAlias($category_alias);

			    $count = Posts::countDataByCategoryId($data_category['id'],0);
			    if($count % $rows_per_page == 0)
			    {
				    $totalpage = floor($count/$rows_per_page);
			    }
			    else
			    {
				    $totalpage = floor($count/$rows_per_page) + 1;
			    }
			    $util = new Paging();

			    $data_posts = Posts::getDataByCategoryIdPaging($data_category['id'],$begin,$end,0,"view");
			    $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("site/hot",array("alias"=>$category_alias)).'/',"");

			    $this->render("hot",array('data_posts'=>$data_posts,'data_category'=>$data_category,'paging'=>$paging,'is_hot'=>1));
		    }

	    }

	    public function actionNew(){
		    $url = new Url();
		    $this->layout = "column_main";
		    $category_alias = isset($_GET["alias"])?$_GET["alias"]:"";
		    if($category_alias == ''){
			    $page = isset($_GET['page']) ? intval($_GET ['page']):1;
			    $rows_per_page = 10;
			    $begin = ($page - 1)*$rows_per_page;
			    $end = $rows_per_page;
			    $title = "Truyện MỚI";

			    $where_sql = 'AND post_is_chap = 0 AND category_id <> 1 AND category_id <> 4 AND category_id <> 22';
			    $count = Posts::countDataSearch($where_sql);
			    if($count % $rows_per_page == 0)
			    {
				    $totalpage = floor($count/$rows_per_page);
			    }
			    else
			    {
				    $totalpage = floor($count/$rows_per_page) + 1;
			    }
			    $util = new Paging();

			    $data_posts = Posts::getDataByCategoryIdPaging(0,$begin,$end,0,"date");
			    $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("site/new").'/',"");

			    $data_category = Category::getAllRows();

			    $this->render("hot",array('data_posts'=>$data_posts,'data_category'=>$data_category,'paging'=>$paging,'title'=>$title));
		    }else{
			    $page = isset($_GET['page']) ? intval($_GET ['page']):1;
			    $rows_per_page = 10;
			    $begin = ($page - 1)*$rows_per_page;
			    $end = $rows_per_page;
			    $data_category = Category::getRowByAlias($category_alias);

			    $count = Posts::countDataByCategoryId($data_category['id'],0);
			    if($count % $rows_per_page == 0)
			    {
				    $totalpage = floor($count/$rows_per_page);
			    }
			    else
			    {
				    $totalpage = floor($count/$rows_per_page) + 1;
			    }
			    $util = new Paging();

			    $data_posts = Posts::getDataByCategoryIdPaging($data_category['id'],$begin,$end,0,"view");
			    $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("site/hot",array("alias"=>$category_alias)).'/',"");

			    $this->render("hot",array('data_posts'=>$data_posts,'data_category'=>$data_category,'paging'=>$paging,'is_hot'=>1));
		    }

	    }

	    public function actionVote(){
		    $url = new Url();
		    $this->layout = "column_main";
		    $category_alias = isset($_GET["alias"])?$_GET["alias"]:"";
		    if($category_alias == ''){
			    $page = isset($_GET['page']) ? intval($_GET ['page']):1;
			    $rows_per_page = 10;
			    $begin = ($page - 1)*$rows_per_page;
			    $end = $rows_per_page;
			    $title = "Truyện HAY";

			    $where_sql = 'AND post_is_hot = 1 AND post_is_chap = 0 AND category_id <> 1 AND category_id <> 4 AND category_id <> 22';
			    $count = Posts::countDataSearch($where_sql);
			    if($count % $rows_per_page == 0)
			    {
				    $totalpage = floor($count/$rows_per_page);
			    }
			    else
			    {
				    $totalpage = floor($count/$rows_per_page) + 1;
			    }
			    $util = new Paging();

			    $data_posts = Posts::getDataByCategoryIdPaging(0,$begin,$end,0,"hot");
			    $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("site/vote").'/',"");

			    $data_category = Category::getAllRows();

			    $this->render("hot",array('data_posts'=>$data_posts,'data_category'=>$data_category,'paging'=>$paging,'title'=>$title));
		    }else{
			    $page = isset($_GET['page']) ? intval($_GET ['page']):1;
			    $rows_per_page = 10;
			    $begin = ($page - 1)*$rows_per_page;
			    $end = $rows_per_page;
			    $data_category = Category::getRowByAlias($category_alias);

			    $count = Posts::countDataByCategoryId($data_category['id'],0);
			    if($count % $rows_per_page == 0)
			    {
				    $totalpage = floor($count/$rows_per_page);
			    }
			    else
			    {
				    $totalpage = floor($count/$rows_per_page) + 1;
			    }
			    $util = new Paging();

			    $data_posts = Posts::getDataByCategoryIdPaging($data_category['id'],$begin,$end,0,"view");
			    $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("site/hot",array("alias"=>$category_alias)).'/',"");

			    $this->render("hot",array('data_posts'=>$data_posts,'data_category'=>$data_category,'paging'=>$paging,'is_hot'=>1));
		    }

	    }

	    public function actionChangeMobile(){
		    $url = new Url();
		    Yii::app()->session['themes'] = "stupidphone";
		    $this->redirect($url->createUrl("site/index"));
		    //echo Yii::app()->session['themes'];die;
	    }

	    public function actionChangeSmart(){
		    $url = new Url();
		    Yii::app()->session['themes'] = "smartphone";
		    $this->redirect($url->createUrl("site/index"));
		    //echo Yii::app()->session['themes'];die;
	    }

	    public function actionChangePc(){
		    $url = new Url();
		    //Yii::app()->session['themes'] = "stupidphone";
		    unset(Yii::app()->session['themes']);
		    $this->redirect($url->createUrl("site/index"));
	    }

	    public function actionFixAuthorAlias(){
		    $data_author = Authors::getAllRows();
		    foreach($data_author as $row){
			    if($row['author_alias'] == ''){
				    $update['author_alias'] = StringUtils::removeTitle($row['author_name'],'');
				    Authors::update($row['author_id'],$update);
			    }
		    }
		    echo "oke";
	    }

    }

?>
