<?php

    class CategoryController extends Controller {
        public $metaDescription = null;
        public $metaKeywords = null;
        public $linkCanoncical = null;
        public $title = null;
        public $layout = 'column_main';        

        public function actionError(){                
            if($error=Yii::app()->errorHandler->error)
            {
                //echo $error['message'];die;
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->renderPartial('error', $error);
            }
        }        

        public function actionIndex(){
            $url = new Url();            
            $page = isset($_GET['page']) ? intval($_GET ['page']):1;
            $rows_per_page = 24;
            $begin = ($page - 1)*$rows_per_page;                        
            $end = $rows_per_page;
            $category_id = isset($_GET["category_id"])?$_GET["category_id"]:"";
            $sort_view = isset($_GET["sort_view"])?$_GET["sort_view"]:0;
            $sort_complete = isset($_GET["sort_complete"])?$_GET["sort_complete"]:0;
            $category_alias = isset($_GET["alias"])?$_GET["alias"]:"";
            $data_category = Category::getRowByAlias($category_alias);
            $count = Posts::countDataByCategoryId($data_category['id'],$sort_complete);
            if($count % $rows_per_page == 0)        
            {
                $totalpage = floor($count/$rows_per_page);
            }
            else
            {
                $totalpage = floor($count/$rows_per_page) + 1;                             
            }                    
            $util = new Paging();

	        if($sort_view == 0){
		        $data_posts = Posts::getDataByCategoryIdPaging($data_category['id'],$begin,$end,$sort_complete,"date");
	        }else{
		        $data_posts = Posts::getDataByCategoryIdPaging($data_category['id'],$begin,$end,$sort_complete,"view");
	        }

	        $url_sort = '/view/' . $sort_view . '/complete/' . $sort_complete;
            $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("category/index",array("alias"=>$category_alias)).'/',$url_sort);

            //$data_adv = Adv::getAllRows();

            $this->title = $data_category['name'] . " - SauTruyen.Com";
            $this->metaKeywords = $data_category['name'] .", doc truyen online, truyen ngan, truyen cuoi, truyen tinh yeu, truyen dai ky, tieu thuyet, thich truyen, doc truyen";
            $this->metaDescription = "Bạn đang ở chuyên mục " . $data_category['name'] . " - " . StringUtils::RemoveSign($data_category['name']) . " từ trang sautruyen.com. " ;
            
            $this->render("index",array('data_category'=>$data_category,'data_posts'=>$data_posts,'paging'=>$paging,'category_alias'=>$category_alias,'page'=>$page,'sort_view'=>$sort_view,'sort_complete'=>$sort_complete));
        }                 

	    public function actionAjaxChangeFilter(){
		    $url = new Url();
		    $page = isset($_POST['page']) ? intval($_POST ['page']):1;
		    $sort_view = isset($_POST["sort_view"])?$_POST["sort_view"]:0;
		    $sort_complete = isset($_POST["sort_complete"])?$_POST["sort_complete"]:0;
		    $category_alias = isset($_POST["category_alias"])?$_POST["category_alias"]:"";
		    echo $url->createUrl("category/index", array("alias" => $category_alias, "page" => $page, "sort_view" => $sort_view, "sort_complete" => $sort_complete));
		    die;
	    }

    }

?>
