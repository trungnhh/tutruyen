<?php

    class TransactionController extends Controller {
        public $metaDescription = null;
        public $metaKeywords = null;
        public $linkCanoncical = null;
        public $title = null;         
        public $layout = 'column_main';        

        public function actionIndex(){
            $url = new Url();
            if(isset($_SESSION["user_tt"])){
                $data_user = User::getRowById($_SESSION["user_tt"]['id']);
                $this->render("index",array("data_user"=>$data_user));
            }else{
                $this->redirect($url->createUrl("user/login")); 
            }
        }        

        public function actionAjaxFundsIn(){              
            $url = new Url();            
            $type = isset($_GET["type"])?mysql_escape_string($_GET["type"]):"";                    
            $content = isset($_GET["content"])?mysql_escape_string($_GET["content"]):"";                    
            $username = isset($_GET["username"])?mysql_escape_string($_GET["username"]):"";                    
            $fund_in = isset($_GET["fund_in"])?mysql_escape_string($_GET["fund_in"]):"";                                
            if($content == 'sms'){
                $content = "Bạn đã nạp 150 xu qua tin nhắn.";
            }
            if(isset($username)){
                $data_user = User::getRowByUsername($username);
                $data_insert['user_id'] = $data_user['id'];
                $data_insert['username'] = $data_user['username'];
                $data_insert['funds_before'] = $data_user['funds'];
                $data_insert['funds_after'] = ($data_user['funds'] + $fund_in);
                $data_insert['type'] = $type;
                $data_insert['content'] = $content;
                $data_insert['transaction_date'] = time();
                TransactionIn::insert($data_insert);
                $data_update['funds'] = ($data_user['funds'] + $fund_in);                    
                User::updateObject($data_update,"id",$data_user['id']);                    
                echo 1;die;
            }              
            echo 0;die;
        }

        public function actionAjaxFundsOut(){            
            $url = new Url();
            $type = isset($_POST["type"])?mysql_escape_string($_POST["type"]):"";                    
            $content = isset($_POST["content"])?mysql_escape_string($_POST["content"]):"";                    
            $fund_out = isset($_POST["fund_out"])?mysql_escape_string($_POST["fund_out"]):"";
            if(isset($_SESSION["user_tt"]['id'])){
                $data_user = User::getRowById($_SESSION["user_tt"]['id']);
                $data_insert['user_id'] = $data_user['id'];
                $data_insert['username'] = $data_user['username'];
                $data_insert['funds_before'] = $data_user['funds'];
                $data_insert['funds_after'] = ($data_user['funds'] - $fund_out);
                $data_insert['type'] = $type;
                $data_insert['content'] = $content;
                $data_insert['transaction_date'] = time();
                TransactionOut::insert($data_insert);
                echo 1;die;
            }              
            echo 0;die;            
        }        

    }

?>