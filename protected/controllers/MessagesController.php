<?php

    class MessagesController extends Controller {
        public $metaDescription = null;
        public $metaKeywords = null;
        public $linkCanoncical = null;
        public $title = null;
        public $layout = 'column_main';        

        public function actionError(){                
            if($error=Yii::app()->errorHandler->error)
            {
                //echo $error['message'];die;
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->renderPartial('error', $error);
            }
        }        

        public function actionIndex(){
            $url = new Url();

	        if(isset($_SESSION["user_tt"])) {
		        $page = isset($_GET['page']) ? intval($_GET ['page']):1;
		        $rows_per_page = 4;
		        $begin = ($page - 1)*$rows_per_page;
		        $end = $rows_per_page;
		        $username = $_SESSION["user_tt"]['username'];
		        $where = " AND receiver = '".$username."' OR type = 1";
		        $count = Messages::countDataSearch($where);
		        if($count % $rows_per_page == 0)
		        {
			        $totalpage = floor($count/$rows_per_page);
		        }
		        else
		        {
			        $totalpage = floor($count/$rows_per_page) + 1;
		        }
		        $util = new Paging();

		        $data_messages = Messages::getSearch($where,$begin,$end);
		        $paging = $util->showPageNavigationMore($page,$totalpage,$url->createUrl("messages/index")."/","");
		        $this->render("index",array('data_messages'=>$data_messages,'paging'=>$paging));
	        } else {
		        $this->redirect($url->createUrl("user/login"));
	        }

        }                 
     
        public function actionDetail(){
            $message_id = isset($_GET['id']) ? intval($_GET ['id']):1;
            $data_message = Messages::getRowById($message_id);
	        if($data_message['status'] == ''){
		        $data_insert['message_id'] = $message_id;
		        $data_insert['username'] = $_SESSION["user_tt"]['username'];
		        MessagesCheck::insert($data_insert);
	        }
            $this->render("detail",array('data_message'=>$data_message));
        }
        
    }

?>
