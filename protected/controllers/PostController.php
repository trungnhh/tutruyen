<?php

    class PostController extends Controller {
        public $metaDescription = null;
        public $metaKeywords = null;
        public $linkCanoncical = null;
        public $title = null;
        public $layout = 'column_main';
        public $image = null;

        public function actionError(){                
            if($error=Yii::app()->errorHandler->error)
            {
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->renderPartial('error', $error);
            }
        }        

        public function actionDetail(){
            $url = new Url();                                    
            $post_alias = isset($_GET["post_alias"])?$_GET["post_alias"]:"";                         
            $category_alias = isset($_GET["category_alias"])?$_GET["category_alias"]:"";            
            $page = isset($_GET['page']) ? intval($_GET ['page']):1;
            $rows_per_page = 10;
            $begin = ($page - 1)*$rows_per_page;
            $end = $rows_per_page;            
            $data_post = Posts::getRowByAlias($post_alias);
	        $count = 0;

            /*Begin Update lượng view cho post*/                        
            Posts::updatePostViews($data_post['id']);
            /*End Update lượng view cho post*/

            /*Begin Lấy dữ liệu chương*/
            if($data_post['id'] != ""){
                if($data_post['post_is_chap'] == 0){
                    $count = PostsLink::countRowByParentPostId($data_post['id']);
                }                           

                $data_chap = array();
                if($count != 0){
                    if($count % $rows_per_page == 0)        
                    {
                        $totalpage = floor($count/$rows_per_page);
                    }
                    else
                    {
                        $totalpage = floor($count/$rows_per_page) + 1;                             
                    }                    
                    $util = new Paging();

                    $data_chap = PostsLink::getRowByParentPostIdPaging($data_post['id'],$begin,$end);                    

                    $paging = $util->showPageNavigation($page,$totalpage,$url->createUrl("post/detail",array("category_alias"=>$category_alias,"post_alias"=>$post_alias)).'?','#document_activity');
                }                                                 

                $data_all_chap = null;
                $post_parent = PostsLink::getRowByPostId($data_post['id']);
                if($post_parent != null){
                    $post_parent = $post_parent['post_parent'];     
                    $data_all_chap = PostsLink::getRowByParentPostId($post_parent);        
                    $data_post_parent = Posts::getRowById($post_parent);
	                $post_parent_author_id = $data_post_parent['author_id'];
	                if($post_parent_author_id != 0){
		                $post_parent_author = Authors::getRowById($data_post_parent['author_id']);
		                $data_post['author_name'] = $post_parent_author['author_name'];
                        $data_post['post_image'] = $post_parent_author['post_image'];
	                }
                }                                

            }                                                   
            /*End Lấy dữ liệu chương*/

            /*Begin Kiểm tra bookmark*/
            $check_bookmark = 0;
            if(isset($_SESSION["user_tt"])){
                $bookmark = Bookmark::getRowByCheckBookmark($data_post['id'],$_SESSION["user_tt"]['id']);
                if($bookmark == null){
                    $check_bookmark = 1;
                }else{
                    $check_bookmark = 2;    
                }
            }                        
            /*Up Kiểm tra bookmark*/

            /*Begin Kiểm tra vote*/
            $check_vote = 0;
            if(isset($_SESSION["user_tt"])){
                $vote = Vote::getRowByCheckVote($data_post['id'],$_SESSION["user_tt"]['id']);
                if($vote == null){
                    $check_vote = 1;
                }else{
                    $check_vote = 2;    
                }
            }            
            /*End Kiểm tra vote*/

            /*Begin Lấy dữ liệu truyện random cùng chuyên mục*/
            $data_post_random = Posts::getPostRandomByCategory($data_post['category_id']);
            shuffle($data_post_random);
            /*End Lấy dữ liệu truyện random cùng chuyên mục*/

	        /*Begin Lấy dữ liệu comment*/
	        $data_comment = Comment::getDataCommentPaging($data_post['id'],0,1,0,10);
	        //$data_comment_child = Comment::getDataComment($data_post['id'],1,1);
	        /*End Lấy dữ liệu comment*/

            $this->image = $data_post['post_image'];

            /*Begin SEO*/
            $this->title = $data_post['post_title'] . " - SauTruyen.Com";
            $this->metaKeywords = $data_post['post_title'] .", doc truyen online, truyen ngan, truyen cuoi, truyen tinh yeu, truyen dai ky, tieu thuyet, thich truyen, doc truyen";
            $this->metaDescription = "Bạn đang đọc truyện " . $data_post['post_title'] . " (". StringUtils::RemoveSign($data_post['post_title']) .")" . " ở chuyên mục " . $data_post['category_name'] . " từ trang sautruyen.com. " ;
            /*$this->linkCanoncical = $url->createUrl("ketqua/index");*/            
            /*End SEO*/
            $this->render("detail",array('post_alias'=>$post_alias,'category_alias'=>$category_alias,'data_post'=>$data_post,'data_chap'=>$data_chap,'paging'=>$paging,'data_all_chap'=>$data_all_chap,'check_bookmark'=>$check_bookmark,'check_vote'=>$check_vote,'data_post_random'=>$data_post_random,'data_post_parent'=>$data_post_parent,'count'=>$count,'data_comment'=>$data_comment));
        }

        public function actionAjaxAddBookmark(){                    
            $array_input['user_id'] = $_SESSION["user_tt"]['id'];
            $array_input['post_id'] = isset($_POST["post_id"])?mysql_escape_string($_POST["post_id"]):"";
            $array_input['post_alias'] = isset($_POST["post_alias"])?mysql_escape_string($_POST["post_alias"]):"";
            $status = isset($_POST["status"])?mysql_escape_string($_POST["status"]):"";
            $array_input['create_date'] = time();
	        if($array_input['user_id'] == 0 || $array_input['user_id'] == null){
		        echo 0;die;
	        }
	        if($status == 1){
		        $last_id = Bookmark::insert($array_input);
	        }else{
		        $last_id = Bookmark::delete("post_id=".$array_input['post_id']." and user_id=".$array_input['user_id']);
	        }
            echo 1;die;

        }

        public function actionAjaxRemoveBookmark(){                                
            $bookmark_id = isset($_POST["bookmark_id"])?mysql_escape_string($_POST["bookmark_id"]):"";                        
            Bookmark::delete("id=".$bookmark_id);
            echo 1;die;

        }

        public function actionAjaxVotePost(){                    
            $array_input['user_id'] = $_SESSION["user_tt"]['id'];
            $array_input['post_id'] = isset($_POST["post_id"])?mysql_escape_string($_POST["post_id"]):"";
            $array_input['create_date'] = time();
	        $status = isset($_POST["status"])?mysql_escape_string($_POST["status"]):"";
	        $data_post = Posts::getRowById($array_input['post_id']);
	        if($array_input['user_id'] == 0 || $array_input['user_id'] == null){
		        echo 0;die;
	        }
	        if($status == 1){
		        $last_id = Vote::insert($array_input);
		        $array_update['post_vote'] = $data_post['post_vote'] + 1;
		        Posts::update($array_input['post_id'],$array_update);
	        }else{
		        $last_id = Vote::delete("post_id=".$array_input['post_id']." and user_id=".$array_input['user_id']);
		        $array_update['post_vote'] = $data_post['post_vote'] - 1;
		        Posts::update($array_input['post_id'],$array_update);
	        }
            echo 1;die;

        }

        public function actionAjaxAddComment(){
            $array_input['user_id'] = $_SESSION["user_tt"]['id'];
            $array_input['post_id'] = isset($_POST["post_id"])?mysql_escape_string($_POST["post_id"]):0;
            $array_input['comment_parent'] = isset($_POST["comment_parent"])?mysql_escape_string($_POST["comment_parent"]):0;
            $array_input['comment'] = isset($_POST["comment"])?mysql_escape_string($_POST["comment"]):"";
            $array_input['create_date'] = time();			
            if($array_input['post_id'] != 0){
                $last_id = Comment::insert($array_input);           
                $data_post = Posts::getRowById($array_input['post_id']);
                $data_update['post_comments'] = intval($data_post['post_comments']) + 1;
                Posts::update($array_input['post_id'],$data_update);
            }
	        $this->renderPartial('comment_content', array('data_comment'=>$array_input));
        }

	    public function actionAjaxLoadMoreComment(){
		    $post_id = isset($_POST["post_id"])?mysql_escape_string($_POST["post_id"]):"";
		    $comment_page = isset($_POST["comment_page"])?mysql_escape_string($_POST["comment_page"]):1;
		    $rows_per_page = 10;
		    $begin = ($comment_page - 1)*$rows_per_page;
		    $end = $rows_per_page;
		    $data_comment = Comment::getDataCommentPaging($post_id,0,1,$begin,$end);
		    $data_comment_child = Comment::getDataComment($post_id,1,1);
		    $this->renderPartial('comment_paging', array('data_comment'=>$data_comment,'data_comment_child'=>$data_comment_child));
	    }

        public function actionAjaxPurchasePost(){                    
            $array_input['user_id'] = $_SESSION["user_tt"]['id'];
            $array_input['post_id'] = isset($_POST["post_id"])?mysql_escape_string($_POST["post_id"]):"";                                    
            $array_input['price_sold'] = isset($_POST["post_cost"])?mysql_escape_string($_POST["post_cost"]):"";                                    
            $array_input['create_date'] = time();
            $data_user = User::getRowById($_SESSION["user_tt"]['id']);
            $data_post = Posts::getRowById($array_input['post_id']);
            if($data_user['funds'] < $array_input['price_sold']){
                echo 3;die;
            }
            if(isset($array_input['user_id']) && isset($array_input['post_id'])){
                $check = PostsSold::insert($array_input);            
                $array_transaction['user_id'] = $data_user['id'];
                $array_transaction['username'] = $data_user['username'];
                $array_transaction['funds_before'] = $data_user['funds'];
                $array_transaction['funds_after'] = $data_user['funds'] - $array_input['price_sold'];
                $array_transaction['post_id'] = $array_input['post_id'];
                $array_transaction['type'] = 1;
                $array_transaction['content'] = "Thanh toán mua truyện " . $data_post['post_title'];
                $array_transaction['transaction_date'] = time();
                TransactionOut::insert($array_transaction);
                $data_update['funds'] = $array_transaction['funds_after'];
                User::updateObject($data_update,"id",$data_user['id']);            
                echo 1;die;    
            }else{
                echo 0;die;
            }

        }

    }

?>
