<?php
class Helpers
{
    protected $_configuration = null;

    public function __construct()
    {
        try{
            $this->setConfiguration();
        }catch (Exception $e){
            var_dump($e->getMessage());die;
        }
    }

    public function getBaseUrl()
    {
        return Yii::App()->params['base_url'];
    }

    public function getBrandDomain()
    {
        return Yii::App()->params['brand_domain'];
    }

    public function getBrandName()
    {
        return Yii::App()->params['brand_name'];
    }

    public function getImageUrl()
    {
        return Yii::App()->params['image_url'];
    }

    public function getImageBaseUrl()
    {
        return Yii::App()->params['image_base_url'];
    }

    public function getConfigurationValue($key)
    {
        $options = new Options();
        $configuration = $options->getConfiguration();
        if(isset($configuration[$key])){
            return $configuration[$key];
        }
        return '';
    }

    public function getPostDefaultImage()
    {
        return $this->getBaseUrl().'images/tutruyen_cover.jpg';
    }

    public function getCurrentUrl()
    {
        return 'http'.(empty($_SERVER['HTTPS'])?'':'s').'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
    }

    public function getExceptionMessage()
    {
        return 'Error';
    }

    public function getBaseDir($type = '')
    {
        if(ROOT_DIR){
            return ROOT_DIR . DS . $type;
        }
        return $_SERVER['DOCUMENT_ROOT'] . DS . $type;
    }

    public static function log($message, $level = null, $file = 'system.log', $forceLog = false)
    {
        $level  = is_null($level) ? CLogger::LEVEL_ERROR : $level;
        $file = empty($file) ? 'system.log' : $file;

        try {
            $logDir  = self::getBaseDir('var') . DS . 'log';
            $logFile = $logDir . DS . $file;

            if (!is_dir($logDir)) {
                mkdir($logDir);
                chmod($logDir, 0777);
            }

            if (!file_exists($logFile)) {
                try{
                    file_put_contents($logFile, '');
                    chmod($logFile, 0640);
                }catch (Exception $e){
                    var_dump($e->getMessage());die;
                }
            }
            $format = '%timestamp% %priorityName%: %message%' . PHP_EOL;

            if (is_array($message) || is_object($message)) {
                $message = print_r($message, true);
            }
            $formats = array(
                'timestamp'     =>  date('Y-m-d H:i:s'),
                'priorityName'  =>  $level,
                'message'       =>  $message
            );
            foreach($formats as $k => $v){
                $format = str_replace("%{$k}%", $v, $format);
            }

            file_put_contents($logFile, $format);
        }
        catch (Exception $e) {
        }
    }
}