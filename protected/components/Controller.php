<?php
/**
* Controller is the customized base controller class.
* All controller classes for this application should extend from this base class.
*/
class Controller extends CController
{
    /**
    * @var string the default layout for the controller view. Defaults to '//layouts/column1',
    * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
    */  
    public $layout='//layouts/column1';
    /**
    * @var array context menu items. This property will be assigned to {@link CMenu::items}.
    */
    public $menu=array();
    /**
    * @var array the breadcrumbs of the current page. The value of this property will
    * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
    * for more details on how to specify this property.
    */
    public $breadcrumbs = null;
    protected function beforeAction($action)
    {
        $route = $this->getId() . '/' . $action->id;
        $publicPages = array(
            'site/error',
        );

	    StringUtils::detechDevice();

	    if(Yii::app()->session['themes'] == 'stupidphone'){
		    Yii::app()->theme = "stupidphone";
	    }

	    if(Yii::app()->session['themes'] == 'smartphone'){
		    Yii::app()->theme = "smartphone";
	    }

        if(Yii::app()->user->isGuest && !in_array($route, $publicPages)){
            //echo 'chua dang nhap<br>';
            //echo $route;
            //Yii::app()->user->loginRequired();
        }
        return true;
    }
}