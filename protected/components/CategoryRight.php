<?php
Yii::import('zii.widgets.CPortlet');

class CategoryRight extends CPortlet
{
    protected function renderContent(){       
        $data_category = Category::getAllRows();                     
        $this->render("category",array('data_category'=>$data_category));         
    }
}
