<?php
Yii::import('zii.widgets.CPortlet');

class AdvRight extends CPortlet
{
    protected function renderContent(){       
        $data_adv = Adv::getAllRows();                        
        $this->render("adv",array('data_adv'=>$data_adv));         
    }
}