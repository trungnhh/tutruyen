<?php
Yii::import('zii.widgets.CPortlet');

class Author extends CPortlet
{
    protected function renderContent(){       
        $data_category = Category::getAllRows();                     
        $this->render("author",array('data_category'=>$data_category));         
    }
}