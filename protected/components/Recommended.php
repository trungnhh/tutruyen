<?php
Yii::import('zii.widgets.CPortlet');

class Recommended extends CPortlet
{
    protected function renderContent(){       
        $category_alias = isset($_GET['alias']) ? $_GET ['alias']:"";                
        if($category_alias == ""){
            $category_id = 0;
        }else{
            $data_category = Category::getRowByAlias($category_alias);
            $category_id = $data_category['id'];
        }
        $data_featured = Posts::getRanPostFeaturedByCategory($category_id);        
        if($data_featured == null){
            $data_featured = Posts::getRanPostFeaturedByCategory(0);
        }                
        shuffle($data_featured);
        $this->render("recommended",array('data_featured'=>$data_featured));
    }
}