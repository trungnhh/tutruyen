<?php
	Yii::import('zii.widgets.CPortlet');
	class StupidFooter extends CPortlet{
		protected function renderContent(){
			$data_category = Category::getAllRows();
			$this->render("stupid_footer",array('data_category' => $data_category));
		}
	}