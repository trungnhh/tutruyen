<?php
Yii::import('zii.widgets.CPortlet');

class UserMenu extends CPortlet
{
    protected function renderContent(){       
        $data_adv = array();
        $this->render("user_menu",array('data_adv'=>$data_adv));
    }
}