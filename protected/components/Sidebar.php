<?php
Yii::import('zii.widgets.CPortlet');

class Sidebar extends CPortlet
{
    protected function renderContent(){                
        $this->render("sidebar",array());
    }
}