<?php
class CDbConnectionRewrite extends CDbConnection
{

    public function __construct($dsn='',$username='',$password='')
    {
        parent::__construct($dsn, $username, $password);
    }

    /**
     * Open or close the DB connection.
     * @param boolean $value whether to open or close DB connection
     * @throws CException if connection fails
     */
    public function setActive($value)
    {
        if($value != $this->getActive())
        {
            if($value){
                try{
                    $this->open();
                }catch (Exception $e)
                {
                    $timestamp = (string)time();
                    Helpers::log($e->getMessage(), 'ERR', "{$timestamp}.log", true);
                    include ROOT_DIR."/maintenance.html";die;
                }
            }
            else
                $this->close();
        }
    }
}