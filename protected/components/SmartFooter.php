<?php
Yii::import('zii.widgets.CPortlet');

class SmartFooter extends CPortlet
{
    protected function renderContent(){
        $this->render("smart_footer",array());
    }
}