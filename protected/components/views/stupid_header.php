<?php
	$url = new Url();
	$controller = Yii::app()->controller->id;
	$action = Yii::app()->controller->action->id;
	$ac = $controller . '-' . $action;
?>
<!-- BEGIN HEADER -->
<div class="header">
	<div class="top-header">
		<div class="form-search">
			<form id="tuturyen_search_form" method="get" autocomplete="off" action="<?php echo $url->createUrl("site/search"); ?>">
				<table>
					<tbody>
					<tr>
						<td><input id="keyword" name="keyword" type="text" autocomplete="off" placeholder="Tìm kiếm..." class="input-text p-search">
						</td>
						<td>
							<button type="submit" class="button-search"><span></span></button>
						</td>
					</tr>
					</tbody>
				</table>
			</form>
		</div>
		<div class="quick-access">
			<?php if(isset($_SESSION["user_tt"])){ ?>
				<a href="<?php echo $url->createUrl("user/index"); ?>" class="login"><span><?php echo $_SESSION["user_tt"]['username']; ?></span></a>
			<?php }else{ ?>
				<a href="<?php echo $url->createUrl("user/login"); ?>" class="login"><span>Đăng nhập</span></a>
			<?php } ?>
            <div class="post-require">
                <a href="<?php echo $url->createUrl("user/request"); ?>"><span>Yêu cầu truyện</span></a>
            </div>
        </div>
	</div>

	<div class="bottom-header">
		<h1 class="logo" title="Tủ truyện">
			<a href="/"><img src="<?php echo Yii::app()->params['urlRs']; ?>/images/stupidphone/logo.png" alt="Tủ Truyện Logo"></a>
		</h1>
	</div>
</div>
<!-- END HEADER -->