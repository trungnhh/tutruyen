<?php 
    $url = new Url();
	$controller = Yii::app()->controller->id;
	$action = Yii::app()->controller->action->id;
	$ac = $controller.'-'.$action;
?>
<div id="global_header" class="global_header fullscreen make_sticky">
    <div class="inner">
        <div class="right_section">
            <div class="user_menu">
	            <?php if(isset($_SESSION["user_tt"])){ ?>
		            <div id="user_menu" class="user_util">
			            <div class="hover_menu">
				            <div style="background-image: url(<?php echo Yii::app()->params['urlRs'] . "/images/mystery-man.jpg"; ?>)" class="user_pic" >
					            <img src="<?php echo StringUtils::getImageUrl($_SESSION["user_tt"]['image'],"user"); ?>" width="30px" height="30px">
				            </div>
				            <div class="sprite_container">
					            <div class="sprite header_arrow_down"></div>
				            </div>
				            <div class="sub_menu">
					            <div class="sprite white_tri_up"></div>
					            <div class="top">
						            <div>
							            <a href="javascript:" class="user_name"><strong><?php echo $_SESSION["user_tt"]['username']; ?></strong></a>
						            </div>
						            <div><a href="<?php echo $url->createUrl("user/index"); ?>">Trang cá nhân</a></div>
					            </div>
					            <div class="list">
						            <a href="<?php echo $url->createUrl("user/recently"); ?>" class="row <?php if($ac == 'user-recently'){ echo 'selected';} ?>">Truyện vừa đọc</a>
						            <a href="<?php echo $url->createUrl("user/vote"); ?>" class="row <?php if($ac == 'user-vote'){ echo 'selected';} ?>">Truyện yêu thích</a>
						            <a href="<?php echo $url->createUrl("user/bookmark"); ?>" class="row <?php if($ac == 'user-bookmark'){ echo 'selected';} ?>">Tủ sách của tôi</a>
					            </div>
					            <div class="divider"></div>
					            <div class="list last">
						            <a href="<?php echo $url->createUrl("user/index"); ?>" class="row <?php if($ac == 'user-index'){ echo 'selected';} ?>">Tài khoản</a>
						            <a href="javascript:" class="row">Tài trợ</a>
						            <a href="javascript:" class="row">Trợ giúp</a>
						            <a href="<?php echo $url->createUrl("user/logout"); ?>" class="row">Thoát</a>
					            </div>
				            </div>
			            </div>
		            </div>
	            <?php }else{ ?>
		            <a href="<?php echo $url->createUrl("user/login"); ?>" class="flat_btn light_blue upload_button">
			            <span class="sprite upload"></span>Đăng ký
		            </a>
		            <a href="<?php echo $url->createUrl("user/login"); ?>" class="flat_btn light_blue upload_button" style="width:100px;">
			            <span class="sprite upload"></span>Đăng nhập
		            </a>
	            <?php } ?>
            </div>
        </div>
        <a title="SauTruyen.Com" href="<?php echo $url->createUrl("site/index"); ?>" class="logo" style="text-indent: 0;"><img src="<?=Yii::app()->params["urlRs"] ?>/images/logo-tutruyen.png" width="137"></a>

        <div id="header_search" class="header_search">
            <form id="tuturyen_search_form" method="get" autocomplete="off" action="<?php echo $url->createUrl("site/search"); ?>">
                <div class="search_outer">
                    <div class="sprite black_search search_button search_icon" onclick="submit_form_search()"></div>
                    <input type="text" id="keyword" name="keyword" placeholder="Tìm kiếm" class="search_input inactive" autocomplete="off" value="<?php echo $_GET['keyword']; ?>">
                </div>
            </form>
        </div>
        <div class="browse_menu">
            <div class="hover_menu">
                <span class="sprite book"></span>
                <a href="javascript:" class="browse_link">Danh mục</a>
                <span class="sprite header_arrow_down"></span>
                <div class="sub_menu">
                    <div class="sprite white_tri_up"></div>
                    <div class="column left_col">
                        <a href="javascript:" class="browse_header">Truyện</a>
                        <div class="list">
                            <a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-cuoi")); ?>" class="row">Truyện cười</a>
	                        <a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-ngon-tinh")); ?>" class="row">Truyện ngôn tình</a>
	                        <a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-teen")); ?>" class="row">Truyện Teen</a>
                            <a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-dai-ky")); ?>" class="row">Truyện dài kỳ</a>
                            <a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-ngan")); ?>" class="row">Truyện ngắn</a>
                            <a href="<?php echo $url->createUrl("category/index",array("alias"=>"tac-pham-viet-nam")); ?>" class="row">Tác phẩm Việt</a>
                            <a href="<?php echo $url->createUrl("category/index",array("alias"=>"van-hoc-nuoc-ngoai")); ?>" class="row">Văn học nước ngoài</a>
                            <a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-kiem-hiep")); ?>" class="row">Truyện kiếm hiệp</a>
	                        <a href="<?php echo $url->createUrl("category/index",array("alias"=>"tieu-thuyet")); ?>" class="row">Tiểu thuyết</a>
                            <a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-ma-kinh-di")); ?>" class="row">Truyện ma</a>
                            <!--<a href="<?php /*echo $url->createUrl("category/index",array("alias"=>"trinh-tham-hinh-su")); */?>" class="row">Truyện trinh thám</a>-->
                            <a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-nguoi-lon-18")); ?>" class="row">Truyện 18+</a>
                        </div>
                    </div>
                    <div class="column right_col">
                        <a href="javascript:" class="browse_header">Nội dung khác</a>
                        <div class="list">
                            <a href="<?php echo $url->createUrl("category/index",array("alias"=>"tinh-yeu-gioi-tinh")); ?>" class="row">Tình yêu giới tính</a>
                            <a href="<?php echo $url->createUrl("category/index",array("alias"=>"qua-tang-cuoc-song")); ?>" class="row">Quà tặng cuộc sống</a>
                            <a href="<?php echo $url->createUrl("category/index",array("alias"=>"tam-su")); ?>" class="row">Tâm sự</a>
                            <a href="<?php echo $url->createUrl("category/index",array("alias"=>"nhung-dieu-ky-bi")); ?>" class="row">Những điều kỳ bí</a>
                            <a href="<?php echo $url->createUrl("category/index",array("alias"=>"12-chom-sao")); ?>" class="row">12 chòm sao</a>
	                        <a href="<?php echo $url->createUrl("category/index",array("alias"=>"kien-thuc-moi-ngay")); ?>" class="row">Kiến thức mỗi ngày</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    new Scribd.Header("#global_header");

	function submit_form_search(){
		var keyword = $('#keyword').val();
		if(keyword != ''){
			$('form#tuturyen_search_form').submit();
		}
	}

</script>