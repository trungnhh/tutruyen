<?php
	$url = new Url();
	$controller = Yii::app()->controller->id;
	$action = Yii::app()->controller->action->id;
	$ac = $controller . '-' . $action;
?>
<div class="block acc-menu">
	<h4>Tài khoản</h4>
	<ul>
		<li><a href="<?=$url->createUrl("user/index") ?>#user-index"><span>Thông tin cá nhân</span></a></li>
		<li><a href="<?=$url->createUrl("user/bookmark") ?>"><span>Truyện đánh dấu</span></a></li>
		<li><a href="<?=$url->createUrl("user/request") ?>"><span>Yêu cầu truyện</span></a></li>
		<li><a href="<?=$url->createUrl("messages/index") ?>#user-messages"><span>Tin nhắn</span></a></li>
		<li><a href="<?=$url->createUrl("user/note") ?>#user-note"><span>Góp ý</span></a></li>
		<li><a href="<?=$url->createUrl("user/logout") ?>"><span>Thoát</span></a></li>
	</ul>
</div>