<?php
	$url = new Url();
?>

<div id="recommended_docs_outer_container">
	<div class="autogen_class_views_recommended_documents_show">
		<div id="recommended_stories" class="sidebar_documents first_page">
			<h2>Truyện nổi bật</h2>

			<div style="display:block;" class="sidebar_doc_page">
				<?php for($i = 0; $i < 4; $i ++) { ?>
					<div class="sidebar_doc">
						<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $data_featured[$i]['alias'], "post_alias" => $data_featured[$i]['post_alias'])); ?>">
							<div class="thumb">
								<img src="<?php echo StringUtils::getImageUrl($data_featured[$i]['post_image'], "post"); ?>" class="thumb">
							</div>
						</a>

						<div class="doc_content">
							<div class="doc_title">
								<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $data_featured[$i]['alias'], "post_alias" => $data_featured[$i]['post_alias'])); ?>"><?php echo $data_featured[$i]['post_title']; ?></a>
							</div>
							<div class="doc_author">
								<?php if($data_featured[$i]['author_id'] == 0) {
										echo "Sưu tầm";
									} else { ?>
										<a href="<?php echo $url->createUrl("author/detail", array("author_alias" => $data_featured[$i]['author_alias'])); ?>"><?php echo $data_featured[$i]['author_name']; ?></a>
									<?php } ?>
							</div>
							<p>
								<?php
									if($data_featured[$i]['post_description'] != '') {
										$content = $data_featured[$i]['post_description'];
									} else {
										$content = $data_featured[$i]['post_content'];
									}
									$content = StringUtils::cutstring($content, 100, true);
									$content = nl2br($content);
									echo $content;
								?>
							</p>
						</div>
					</div>
				<?php } ?>
			</div>

			<?php for($a = 1; $a < 5; $a ++) { ?>

				<div style="display:none;" class="sidebar_doc_page">
					<?php $b = $a * 4;
						for($i = $b; $i < ($b + 4); $i ++) { ?>
							<div class="sidebar_doc">
								<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $data_featured[$i]['alias'], "post_alias" => $data_featured[$i]['post_alias'])); ?>">
									<div class="thumb">
										<img src="<?php echo StringUtils::getImageUrl($data_featured[$i]['post_image'], "post"); ?>" class="thumb">
									</div>
								</a>

								<div class="doc_content">
									<div class="doc_title">
										<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $data_featured[$i]['alias'], "post_alias" => $data_featured[$i]['post_alias'])); ?>"><?php echo $data_featured[$i]['post_title']; ?></a>
									</div>
									<div class="doc_author">
										<?php if($data_featured[$i]['author_id'] == 0) {
											echo "Sưu tầm";
										} else { ?>
											<a href="<?php echo $url->createUrl("author/detail", array("author_alias" => $data_featured[$i]['author_alias'])); ?>"><?php echo $data_featured[$i]['author_name']; ?></a>
										<?php } ?>
									</div>
									<p>
										<?php
											if($data_featured[$i]['post_description'] != '') {
												$content = $data_featured[$i]['post_description'];
											} else {
												$content = $data_featured[$i]['post_content'];
											}
											$content = StringUtils::cutstring($content, 100, true);
											$content = nl2br($content);
											echo $content;
										?>
									</p>
								</div>
							</div>
						<?php } ?>
				</div>

			<?php } ?>


			<div class="document_list_pager">
					<span class="pager_links">
						<a href="javascript:" class="prev_page_btn">Trước</a>
						<span class="vert_divider">|</span>
						<a href="javascript:" class="next_page_btn">Sau</a>
					</span>
				<span class="page_label">Trang <span class="current_page">1</span> / 5</span>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	//&lt;![CDATA[
	new Scribd.SidebarDocumentList("#recommended_stories");

	//]]&gt;
</script>