﻿<?php
	$url = new Url();
	$controller = Yii::app()->controller->id;
	$action = Yii::app()->controller->action->id;
	$ac = $controller . '-' . $action;
?>

<div class="page-sidebar">
	<div class="page-sidebar-scroll">

		<a href="tel_3A+84945694499" class="sidebar-button"><em class="sidebar-button-call">GỌI TỦ TRUYỆN</em></a>
		<a href="sms_3A+84945694499" class="sidebar-button"><em class="sidebar-button-text">SMS TỦ TRUYỆN</em></a>
		<a href="mailto:info@sautruyen.com" class="sidebar-button hide2-sidebar"><em class="sidebar-button-close">ĐÓNG</em></a>

		<div class="clear"></div>

		<img class="sidebar-logo replace-2x" src="<?php echo Yii::app()->params["urlRs"]; ?>/images/smartphone/logo-tutruyen.png" alt="img">

		<div class="menu">

			<div class="menu-item">
				<strong class="home-icon"></strong>
				<a class="<?php if($ac == 'site-index'){ echo 'menu-enabled';}else{echo 'menu-disabled';} ?>" href="<?php echo $url->createUrl("site/index"); ?>">Trang chủ</a>
			</div>

			<div class="menu-item">
				<strong class="icon-booklarge"></strong>
				<a class="menu-disabled deploy-submenu" href="#">Danh mục Truyện</a>
				<div class="clear"></div>
				<div class="submenu">
					<a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-cuoi")); ?>">Truyện cười</a> <em class="submenu-decoration"></em>
					<a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-dai-ky")); ?>">Truyện dài kỳ</a> <em class="submenu-decoration"></em>
					<a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-ngan")); ?>">Truyện ngắn</a> <em class="submenu-decoration"></em>
					<a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-ngon-tinh")); ?>">Truyện ngôn tình</a> <em class="submenu-decoration"></em>
					<a href="<?php echo $url->createUrl("category/index",array("alias"=>"tac-pham-viet-nam")); ?>">Tác phẩm Việt</a> <em class="submenu-decoration"></em>
					<a href="<?php echo $url->createUrl("category/index",array("alias"=>"van-hoc-nuoc-ngoai")); ?>">Văn học nước ngoài</a> <em class="submenu-decoration"></em>
					<a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-kiem-hiep")); ?>">Truyện kiếm hiệp</a> <em class="submenu-decoration"></em>
					<a href="<?php echo $url->createUrl("category/index",array("alias"=>"tieu-thuyet")); ?>">Tiểu thuyết</a> <em class="submenu-decoration"></em>
					<a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-ma-kinh-di")); ?>">Truyện ma</a> <em class="submenu-decoration"></em>
					<a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-nguoi-lon-18")); ?>">Truyện 18+</a>
				</div>
			</div>

			<div class="menu-item">
				<strong class="icon-books2"></strong>
				<a class="menu-disabled deploy-submenu" href="#">Nội dung khác</a>
				<div class="clear"></div>
				<div class="submenu">
					<a href="<?php echo $url->createUrl("category/index",array("alias"=>"tinh-yeu-gioi-tinh")); ?>">Tình yêu giới tính</a> <em class="submenu-decoration"></em>
					<a href="<?php echo $url->createUrl("category/index",array("alias"=>"qua-tang-cuoc-song")); ?>">Quà tặng cuộc sống</a> <em class="submenu-decoration"></em>
					<a href="<?php echo $url->createUrl("category/index",array("alias"=>"tam-su")); ?>">Tâm sự</a> <em class="submenu-decoration"></em>
					<a href="<?php echo $url->createUrl("category/index",array("alias"=>"nhung-dieu-ky-bi")); ?>">Những điều kỳ bí</a> <em class="submenu-decoration"></em>
					<a href="<?php echo $url->createUrl("category/index",array("alias"=>"12-chom-sao")); ?>">12 chòm sao</a> <em class="submenu-decoration"></em>
					<a href="<?php echo $url->createUrl("category/index",array("alias"=>"kien-thuc-moi-ngay")); ?>">Kiến thức mỗi ngày</a>
				</div>
			</div>

			<div class="menu-item">
				<strong class="icon-magnifier2"></strong>
				<a class="<?php if($ac == 'site-search'){ echo 'menu-enabled';}else{echo 'menu-disabled';} ?>" href="<?php echo $url->createUrl("site/search"); ?>">Tìm kiếm</a>
			</div>
			<?php if(isset($_SESSION["user_tt"])){ ?>
				<div class="menu-item">
					<strong class="icon-user1"></strong>
					<a class="menu-disabled" href="#">Tài khoản</a>
				</div>
			<?php }else{ ?>
				<div class="menu-item">
					<strong class="icon-shadow-locked1"></strong>
					<a class="menu-disabled" href="#">Đăng ký</a>
				</div>
				<div class="menu-item">
					<strong class="icon-shadow-key2"></strong>
					<a class="menu-disabled" href="#">Đăng nhập</a>
				</div>
			<?php } ?>
		</div>

		<p class="sidebar-heading">CẬP NHẬT</p>

		<div class="sidebar-decoration"></div>

		<div class="sidebar-updates">
			<a href="#" class="update-box update-news">PHIÊN BẢN ĐIỆN THOẠI ĐƯỢC CẬP NHẬT!<br/>21/12/2013</a>

			<div class="sidebar-decoration"></div>
			<a href="#" class="update-box update-blog">TRUYỆN CÔ NÀNG 9X ĐÃ CÓ CHAP MỚI<br>21/12/2013</a>

			<div class="sidebar-decoration"></div>
			<a href="#" class="update-box update-folio">TRUYỆN BÁO CÁO T/Y ĐÃ HOÀN THÀNH<br>21/12/2013</a>

			<!--<div class="sidebar-decoration"></div>
			<a href="#" class="update-box update-blog">POST ADDED<br>18' JAN 2012</a>

			<div class="sidebar-decoration"></div>
			<a href="#" class="update-box update-folio">NEW ITEM ADDED TOO PORTFOLIO<br>17' JAN 2012</a>-->

			<div class="sidebar-decoration"></div>
		</div>

		<p class="sidebar-copyright center-text">Copyright 2016 SauTruyen.Com</p>

		<a href="#" class="sidebar-button"><em class="sidebar-button-facebook">FACEBOOK</em></a>
		<a href="#" class="sidebar-button"><em class="sidebar-button-twitter">TWITTER</em></a>
		<a href="#" class="sidebar-button"><em class="sidebar-button-rss">LIÊN HỆ</em></a>

		<div class="clear"></div>

	</div>
</div>