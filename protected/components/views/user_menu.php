<?php 
    $url = new Url();
	$controller = Yii::app()->controller->id;
	$action = Yii::app()->controller->action->id;
	$ac = $controller.'-'.$action;
?>
<div class="nav">
	<ul class="tabs">
		<li class="<?php if($ac == 'user-index'){ echo 'selected';} ?>"><a href="<?php echo $url->createUrl("user/index"); ?>">Cá nhân</a></li>
		<li class="<?php if($ac == 'user-history'){ echo 'selected';} ?>"><a href="<?php echo $url->createUrl("user/history"); ?>">Lịch sử giao dịch</a></li>
		<li class="<?php if($ac == 'user-note'){ echo 'selected';} ?>"><a href="<?php echo $url->createUrl("user/note"); ?>">Góp ý</a></li>
		<li class="<?php if($ac == 'user-request'){ echo 'selected';} ?>"><a href="<?php echo $url->createUrl("user/request"); ?>">Yêu cầu truyện</a></li>
		<li class="<?php if($ac == 'messages-index'){ echo 'selected';} ?>"><a href="<?php echo $url->createUrl("messages/index"); ?>">Tin nhắn</a></li>
	</ul>
</div>