<?php
	$url = new Url();
?>
<div class="home_branding">
	<div id="home_branding" class="autogen_class_views_home_promo">
		<div class="promotions">
			<a href="javascript:">
				<div style="" class="promo_container active">
					<div class="promotion f2_larger home_image"></div>
				</div>
			</a>
			<div style="right:-1px;" class="right_column active">
				<a href="http://sautruyen.com/news/detail/news_id/1">
					<div class="transparent_background"></div>
					<p class="title">Góp ý hay - Nhận quà ngay</p>
					<p class="text">Gửi góp ý cho Tủ Truyện để có cơ hội nhận ngay những đầu sách, truyện mới và hay nhất hiện nay.</p>
				</a>
				<div class="footer">
					<a href="http://sautruyen.com/news/detail/news_id/1">
						<div class="home_btn flat_orange wide">
							<div class="btn_inner ">Chi tiết</div>
						</div>
					</a>
				</div>
			</div>

			<a href="javascript:">
				<div style="display:none;" class="promo_container ">
					<div class="promotion f1_larger home_image"></div>
				</div>
			</a>
			<div style="" class="right_column ">
				<a href="javascript:">
					<div class="transparent_background"></div>
					<p class="title">Hợp đồng hôn nhân 100 ngày</p>
					<p class="text">Một tác phẩm ngôn tình nổi bật được tìm đọc nhiều nhất hiện nay...</p>
				</a>

				<div class="footer">
					<a href="http://sautruyen.com/truyen-dai-ky/hop-dong-hon-nhan-100-ngay">
						<div class="home_btn flat_orange wide">
							<div class="btn_inner ">Đọc ngay</div>
						</div>
					</a>
				</div>
			</div>

			<a href="javascript:">
				<div style="display:none;" class="promo_container ">
					<div class="promotion f3_larger home_image"></div>
				</div>
			</a>
			<div style="" class="right_column ">
				<a href="javascript:">
					<div class="transparent_background"></div>
					<p class="title">Sáng tác truyện với Tủ Truyện</p>
					<p class="text">Hãy để các tác phẩm của bạn được đến với cộng động yêu truyện của Tủ Truyện.</p>
				</a>

				<div class="footer">
					<a href="<?php echo $url->createUrl("user/login"); ?>">
						<div class="flat_btn light_blue wide big_45">Đăng ký ngay</div>
					</a>
				</div>
			</div>

			<div class="new_dots">
				<div class="circle selected"></div>
				<div class="circle"></div>
				<div class="circle"></div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	//window.promos = new Scribd.UI.HomePagePromo("#home_branding", { stop_on_hover: false, disable_animation: true});
	window.promos = new Scribd.UI.HomePagePromo("#home_branding", { stop_on_hover: false});
</script>