<?php 
    $url = new Url();
	$controller = Yii::app()->controller->id;
	$action = Yii::app()->controller->action->id;
	$ac = $controller.'-'.$action;
?>
<div class="header">
	<a href="#" class="show-sidebar"></a>
	<a href="#" class="hide-sidebar"></a>
	<div class="form-search">
		<form id="tuturyen_search_form" method="get" autocomplete="off" action="<?php echo $url->createUrl("site/search"); ?>">
			<input id="keyword" name="keyword" type="text" autocomplete="off" placeholder="Tìm kiếm..." class="input-text p-search">
			<button type="submit" class="button-search"><span></span></button>
		</form>
	</div>
</div>
<div class="header-decoration"></div>

<div class="container">
	<a href="<?php echo $url->createUrl("site/index"); ?>"><img class="logo replace-2x" src="<?php echo Yii::app()->params["urlRs"]; ?>/images/smartphone/logo-tutruyenb.png" alt="logo"></a>
</div>