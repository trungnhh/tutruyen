<?php 
    $url = new Url();    
?>
<div class="shared_pmp_promo_banner">
	<div class="photo"></div>
	<div class="messages">
		<div class="column first">
			<h4 class="primary_header" style="font-size: 18px;">
				<span class="text">Đọc hàng ngàn truyện với hàng chục thể loại hấp dẫn khác nhau </span> hoàn toàn miễn phí</span>
			</h4>
			<a href="<?php echo $url->createUrl("user/login"); ?>" class="start_trial_button">Đăng ký ngay</a>
		</div>
		<div class="column second">
			<ul class="incentives">
				<li>Không bị làm phiền bởi quảng cáo</li>
				<li>Đọc không giới hạn</li>
				<li>Đọc trên các thiết bị Android, iOS và Java</li>
			</ul>
		</div>
	</div>
</div>