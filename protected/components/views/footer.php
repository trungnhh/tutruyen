﻿<?php
$url = new Url();
?>

    <div id="global_footer" class="global_footer">
        <div class="inner">
            <div><a href="/"><img src="<?=Yii::app()->params["urlRs"] ?>/images/logo-tutruyenb.png" width="137"></a></div>
            <div class="social">
                <a target="_blank" title="Follow <?=Helpers::getBrandDomain(); ?> on Twitter" href="<?=Helpers::getConfigurationValue('twitter'); ?>" class="sprite twitter_badge"></a>
                <a target="_blank" title="Fan <?=Helpers::getBrandDomain(); ?> on Facebook" href="<?=Helpers::getConfigurationValue('facebook'); ?>" class="sprite facebook_badge"></a>
                <br>
	            <div class="fb-like" data-href="<?=Helpers::getConfigurationValue('facebook'); ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
	            <br>
            </div>
            <div class="columns">
                <div class="column "><strong>Giới thiệu</strong>
                    <ul>
                        <li><a href="<?php echo $url->createUrl("site/policy"); ?>">Điều khoản & chính sách</a></li>
                        <li><a href="<?php echo $url->createUrl("site/contact"); ?>">Liên hệ với chúng tôi</a></li>
                    </ul>
                </div>
                <div class="column "><h2><strong>Truyện Hay</strong></h2>
                    <ul>
                        <li><h2><a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-ngan")); ?>">Truyện ngắn</a></h2></li>
                        <li><h2><a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-dai-ky")); ?>">Truyện dài kỳ</a></h2></li>
                    </ul>
                </div>
                <div class="column "><h2><strong>Truyên Hot</strong></h2>
                    <ul>
                        <li><h2><a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-ma-kinh-di")); ?>">Truyện ma</a></h2></li>
                        <li><h2><a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-cuoi")); ?>">Truyện cười</a></h2></li>
                    </ul>
                </div>
                <div class="column "><h2><strong>Đọc truyện</strong></h2>
                    <ul>
                        <li><h2><a href="<?php echo $url->createUrl("category/index",array("alias"=>"tieu-thuyet")); ?>">Tiểu thuyết</a></h2></li>
                        <li><h2><a href="<?php echo $url->createUrl("category/index",array("alias"=>"tac-pham-viet-nam")); ?>">Truyện Việt</a></h2></li>
                    </ul>
                </div>
                <div class="column "><h2><strong>Truyện Online</strong></h2>
                    <ul>
                        <li><h2><a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-kiem-hiep")); ?>">Truyện kiếm hiệp</a></h2></li>
                        <li><h2><a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-ngon-tinh")); ?>">Truyện ngôn tình</a></h2></li>
                    </ul>
                </div>
                <div class="column "><h2><strong>Truyện Teen</strong></h2>
                    <ul>
                        <li><h2><a href="<?php echo $url->createUrl("category/index",array("alias"=>"tinh-yeu-gioi-tinh")); ?>">Tình yêu giới tính</a></h2></li>
                        <li><h2><a href="<?php echo $url->createUrl("category/index",array("alias"=>"truyen-teen")); ?>">Truyện Teen</a></h2></li>
                    </ul>
                </div>
            </div>
            <div class="bottom">
                <div class="copyright">&copy; Copyright 2016 <a href="<?php echo $url->createUrl("site/index"); ?>"><?php echo Helpers::getBrandDomain();?></a> - <a href="<?php echo $url->createUrl("site/index"); ?>">Đọc truyện Online</a></div>
            </div>
        </div>
    </div>

	<div id="fb-root"></div>
	<script>
		window.fbAsyncInit = function() {
			// init the FB JS SDK
			FB.init({
				appId      : 1445009389053355,                        // App ID from the app dashboard
				status     : true,                                 // Check Facebook Login status
				xfbml      : true                                  // Look for social plugins on the page
			});

			// Additional initialization code such as adding Event Listeners goes here
		};

		// Load the SDK asynchronously
		(function(d, s, id){
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/vi_VN/all.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

<?php
/*
if (isset($_GET['test'])) {
    $connect = yii::app()->db;
    echo $connect->showSql;
}

if ($_SESSION["user_tt"]['username'] == 'admin') {
    $connect = yii::app()->db;
    echo $connect->showSql;
}
*/
?>