<?php $url = new Url();?>
<div class="footer">
	<h2 class="danh-muc">Danh mục</h2>
	<div class="menu">
		<ul>
			<?php foreach($data_category as $row){ ?>
				<li><a href="<?php echo $url->createUrl("category/index",array("alias"=>$row['alias'])); ?>"><?php echo $row['name']; ?></a></li>
			<?php } ?>
		</ul>
	</div>
	<div class="form-search">
		<form id="tuturyen_search_form" method="get" autocomplete="off" action="<?php echo $url->createUrl("site/search"); ?>">
                <table>
                    <tbody>
                    <tr>
                        <td><input id="keyword" name="keyword" type="text" autocomplete="off" placeholder="Tìm kiếm..." class="input-text p-search"></td>
                        <td><button type="submit" class="button-search"><span>Go</span></button></td>
                    </tr>
                    </tbody>
                </table>
			</form>
            </div>
	<p class="copyright">&copy;Copyright 2016 <a href="<?php echo $url->createUrl("site/index"); ?>"><?php echo Helpers::getBrandDomain();?></a> - <a href="mailto:info@sautruyen.com">info@sautruyen.com</a> - <a href="<?php echo $url->createUrl("site/index"); ?>">Doc truyen online</a></p>
	<?php

		if (isset($_GET['test'])) {
			$connect = yii::app()->db;
			echo $connect->showSql;
		}

		if ($_SESSION["user_tt"]['username'] == 'admin') {
			$connect = yii::app()->db;
			echo $connect->showSql;
		}

	?>
</div>