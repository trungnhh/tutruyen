<?php 
    $url = new Url();    
?>
<div class="banner_spacer">
	<div id="adv_post_footer" class="doc_banner stuck collapsed">
		<div class="subscribe_pmp_promo banner_wrapper">
			<div class="toggle_banner_btn"></div>
			<div class="message_area">
				<h1>Đăng ký là thành viên của Tủ Truyện để đọc truyện thoải mái không phải nghĩ.</h1>
				<div class="hide_if_collapsed">
					<div>Với hàng ngàn truyện mới và được chú ý nhất hiện nay, chỉ với 15.000VND/tháng.</div>
					<img width="225px" height="25px" src="<?php echo Yii::app()->params['urlRs']; ?>/images/devices_lockup.png" class="devices_lockup">
					<div class="learn_more">
						<a href="javascript:"><img src="<?php echo Yii::app()->params['urlRs']; ?>/images/arrow.png">CHI TIẾT</a>
					</div>
				</div>
			</div>
			<div class="action_area">
				<div class="action large">
					<a href="<?php echo $url->createUrl("user/login"); ?>" class="color_btn primary_action_btn premium_signup_btn">Đăng ký</a>

					<div class="hide_if_collapsed message">
						<span style="float:left; margin: 22px 0;">Hoặc mua ngay truyện này chỉ với 5.000VND</span>

						<div style="float:right; margin-top: 15px;" class="flat_btn gray buy_doc_btn">Mua ngay</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

	Scribd.read_banner = new Scribd.ReadBanner("#adv_post_footer");

</script>