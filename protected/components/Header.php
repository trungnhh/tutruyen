<?php
Yii::import('zii.widgets.CPortlet');

class Header extends CPortlet
{
    protected function renderContent(){       
        $data_category = Category::getAllRows();      
        if(isset($_COOKIE['tutruyen_un'])){
            $rowUser = User::getRowByUsername($_COOKIE['tutruyen_un']);
            $_SESSION["user_tt"]=$rowUser;
        }                   
        $this->render("header",array('data_category'=>$data_category));         
    }
}
?>