<?php
Yii::import('zii.widgets.CPortlet');

class Branding extends CPortlet
{
    protected function renderContent(){       
        $data_category = Category::getAllRows();
        $this->render("branding",array('data_category'=>$data_category));
    }
}
?>