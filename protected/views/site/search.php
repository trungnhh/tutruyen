<?php
	$url = new Url();
?>

	<link href="<?php echo Yii::app()->params['urlRs']; ?>/css/search.css" rel="stylesheet" type="text/css" />

	<div id="autogen_id_11683755" class="autogen_class_views_solr_books autogen_class_views_solr_documents autogen_class_views_solr_single_type autogen_class_views_solr_base">
		<div id="base_background">
			<div id="base_container" class="clearfix">
			<div id="autogen_id_11680807" class="autogen_class_views_solr_header">
				<div class="sort_picker">

					<div style="display: inline-block">
						<a href="javascript:" data-track="All" class="picker_item"><span class="icon sprite search_all"></span>Tất cả</a>
					</div>
					<div style="display: inline-block">
						<a href="javascript:" data-track="Documents" class="picker_item"><span class="icon sprite search_documents"></span>Nội dung khác</a>
					</div>
					<div style="display: inline-block" class="selected">
						<a href="javascript:" data-track="Books" class="picker_item"><span class="icon sprite search_books"></span>Truyện</a>
					</div>
					<div style="display: inline-block">
						<a href="javascript:" data-track="People" class="picker_item"><span class="icon sprite search_people"></span>Thành viên</a>
					</div>
				</div>
			</div>
				<div id="autogen_id_11681840" class="autogen_class_views_solr_facets">
					<div class="filter_picker">
						<span data-type="category" class="hover_menu picker_item">
							<span>Danh mục</span>
							<span class="sprite light_tri_down"></span>
							<span class="sprite white_tri_down"></span>
							<div class="sub_menu  scrolling_menu">
								<span class="sprite white_tri_up"></span>
								<a href="javascript:" class="row">Bất kỳ</a>
								<?php foreach($data_category as $row){ ?>
									<a href="javascript:" class="row"><?php echo $row['name']; ?></a>
								<?php } ?>
							</div>
						</span>
						<span data-type="uploaded_on" class="hover_menu picker_item">
							<span>Thời gian truyện được đăng</span>
							<span class="sprite light_tri_down"></span>
							<span class="sprite white_tri_down"></span>
							<div class="sub_menu  scrolling_menu">
								<span class="sprite white_tri_up"></span>
								<a href="javsacript:" class="row">Bất kỳ lúc nào</a>
								<a href="javsacript:" class="row">Hôm nay</a>
								<a href="javsacript:" class="row">Một tuần trước</a>
								<a href="javsacript:" class="row">Một tháng trước</a>
								<a href="javsacript:" class="row">Một năm trước</a>
							</div>
						</span>
						<span data-type="payment" class="hover_menu picker_item">
							<span>Nâng cao</span><span class="sprite light_tri_down"></span>
							<span class="sprite white_tri_down"></span>
							<div class="sub_menu  scrolling_menu">
								<span class="sprite white_tri_up"></span>
								<a href="javsacript:" class="row">Truyện đã hoàn thành</a>
								<a href="javsacript:" class="row">Truyện VIP</a>
							</div></span>
						<a href="javascript:" class="name_link reset">Làm lại</a>
					</div>
				</div>
				<div class="main_content_section">
					<div id="autogen_id_11686368" class="autogen_class_views_solr_result_list_documents autogen_class_views_solr_result_list_base">
						<div class="result_container">
						<p class="about_results">Có <?php echo $count; ?> truyện cho từ khoá '<?php echo $_GET['keyword']; ?>'</p>
							<?php foreach($data_posts as $row){ ?>
							<div class="autogen_class_views_solr_result_document autogen_class_views_solr_result_base">
								<div class="search_result">
									<div class="cover_design">
										<a href="<?php echo $url->createUrl("post/detail",array("category_alias"=>$row['alias'],"post_alias"=>$row['post_alias'])); ?>">
											<div class="thumbnail">
												<img width="185" height="252" src="<?php echo StringUtils::getImageUrl($row['post_image'],"post"); ?>">
												<?php if($row['post_end'] == 1) { ?>
													<div class="flag purchase_only" style="display: block;">HOÀN THÀNH</div>
												<?php } ?>
												<?php if($row['post_is_full'] == 1) { ?>
													<div class="flag sample" style="display: block;">ĐANG CẬP NHẬT</div>
												<?php } ?>
											</div>
										</a>
									</div>
									<div class="result_body">
										<div class="doc_title_wrap">
											<h4 class="result_title notranslate">
												<a href="<?php echo $url->createUrl("post/detail",array("category_alias"=>$row['alias'],"post_alias"=>$row['post_alias'])); ?>" class="name_link"><?php echo $row['post_title']; ?></a>
											</h4>
											<p class="result_attribution">
												<?php if($row['author_name'] != ''){ ?>
													<a class="username notranslate" href="javascript:"><?php echo $row['author_name']; ?></a>
												<?php }else{ ?>
													<a class="username notranslate" href="javascript:">Sưu tầm</a>
												<?php } ?>
											</p>
										</div>
										<p class="document_description notranslate"><?php echo $row['post_description']; ?></p>

										<div class="bottom_controls document_mixed">
											<div position="1" class="bookmark">
												<span class="profilesprite bookmark_light sprite"></span><span>Lưu vào tủ sách</span>
											</div>
											<div data-docid="170426598" class="collections">
												<div class="sprite small_clip profilesprite"></div>
												<span>Lưu vào bộ sưu tập</span>
											</div>
											<a style="display: none;" class="duplicates_note"></a>

											<div class="clearfix"></div>
										</div>
									</div>
									<div class="result_metadata">
										<div class="stat_row">
											<span class="label">Danh mục: </span><a class="username notranslate" href="javascript:"><?php echo $row['category_name']; ?></a>
										</div>
										<div class="stat_row">
											<span class="label">Ngày đăng : </span><span class="content"><?php echo date('d/m/Y',$row['create_date']);  ?></span>
										</div>
										<div class="stat_row">
											<span class="label">Ngày cập nhật : </span><span class="content"><?php if($row['update_date'] != '' && $row['update_date'] != 0){echo date('d/m/Y',$row['update_date']);}else{echo date('d/m/Y',$row['create_date']);} ?></span>
										</div>
										<div class="stat_row">
											<span class="label">Lượt xem : </span><span class="content"><?php echo $row['post_views']; ?></span>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<?php if($count > 0){ ?>
				<div class="footer_row">
					<div class="paginator">
						<ul class="paging">
							<?php
								echo $paging;
							?>
						</ul>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>

<?php //include "index.js.php"; ?>