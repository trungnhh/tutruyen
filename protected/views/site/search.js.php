<script type="">        

    $(document).ready(function() {          
        var topOffset = 55;
        var topOverlap = 10;
        var top = $('#recommended_users').offset().top - topOffset + topOverlap;
        $(window).scroll(function (event) {
            if ($(this).scrollTop() > top) {
                $('#recommended_users').addClass('panel_fixed');
            }
            else {
                $('#recommended_users').removeClass('panel_fixed');
            }
        });
    });

</script>