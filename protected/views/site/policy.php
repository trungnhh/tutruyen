<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRs"] ?>/css/static.css" media="all" />
<div class="autogen_class_views_static_contact autogen_class_views_static_about_container">

	<div class="container">
		<h1>Điều khoản & Chính sách</h1>

		<div class="intro">
			<p>
			WEBSITE NÀY THUỘC QUYỀN SỞ HỮU VÀ QUẢN LÝ CỦA <?php echo strtoupper(Helpers::getBrandDomain()); ?>. KHI TRUY CẬP, SỬ DỤNG WEBSITE NÀY, QUÝ KHÁCH ĐÃ MẶC NHIÊN ĐỒNG Ý VỚI CÁC ĐIỀU KHOẢN VÀ ĐIỀU KIỆN ĐỀ RA Ở ĐÂY. DO VẬY ĐỀ NGHỊ QUÝ KHÁCH ĐỌC VÀ NGHIÊN CỨU KỸ TRƯỚC KHI SỬ DỤNG TIẾP.
				<br><br>1. Điều khoản chung
				<br><br>a. Chấp thuận các Điều kiện Sử dụng
				<br><br>Khi sử dụng Website Thương mại Điện tử của <?php echo Helpers::getBrandDomain(); ?> (sau đây gọi tắt là “Website”), Quý khách đã mặc nhiên chấp thuận các điều khoản và điều kiện sử dụng (sau đây gọi tắt là “Điều kiện Sử dụng”) được quy định dưới đây. Để biết được các sửa đổi mới nhất, Quý khách nên thường xuyên kiểm tra lại “Điều kiện Sử dụng”. <?php echo Helpers::getBrandDomain(); ?> có quyền thay đổi, điều chỉnh, thêm hay bớt các nội dung của “Điều kiện Sử dụng” tại bất kỳ thời điểm nào. Nếu Quý khách vẫn tiếp tục sử dụng Website sau khi có các thay đổi như vậy thì có nghĩa là Quý khách đã chấp thuận các thay đổi đó.
				<br><br>b. Tính chất của thông tin hiển thị
				<br><br>Các nội dung hiển thị trên Website nhằm mục đích cung cấp thông tin về <?php echo Helpers::getBrandDomain(); ?>, về chương trình khách hàng thường xuyên Bông Sen Vàng, về dịch vụ vận chuyển hành khách, hành lý và hàng hóa của hãng, dịch vụ khách sạn, cũng như các dịch vụ bổ trợ khác liên quan đến du lịch, lữ hành của nhiều nhà cung cấp khác nhau (sau đây được gọi chung là “Nhà Cung Cấp”).
				<br><br>c. Chính sách bảo mật thông tin
				<br><br>Mọi thông tin của khách hàng sẽ được tuyệt đối giữ bí mật, SauTruyen sẽ cam kết không đưa thông tin cho bên thứ 3. SauTruyen chỉ sử dụng để nhằm nâng cao chất lượng dịch vụ của website.
				<br><br>d. Liên kết đến Website khác
				<br><br>Website cung cấp một số liên kết tới trang Web hoặc nguồn dữ liệu khác. Quý khách tự chịu trách nhiệm khi sử dụng các liên kết này. <?php echo Helpers::getBrandDomain(); ?> không tiến hành thẩm định hay xác thực nội dung, tính chính xác, quan điểm thể hiện tại các trang Web và nguồn dữ liệu liên kết này. <?php echo Helpers::getBrandDomain(); ?> từ chối bất cứ trách nhiệm pháp lý nào liên quan tới tính chính xác, nội dung thể hiện, mức độ an toàn và việc cho hiển thị hay che đi các thông tin trên các trang Web và nguồn dữ liệu nói trên.
				<br><br>e. Liên kết từ Website khác
				<br><br><?php echo Helpers::getBrandDomain(); ?> không cho phép bất kỳ nhà cung cấp dịch vụ internet nào được phép “đặt toàn bộ” hay “nhúng” bất kỳ thành tố nào của Website này sang một trang khác hoặc sử dụng các kỹ thuật làm thay đổi giao diện / hiển thị mặc định của Website.
				<br><br>f. MIỄN TRỪ TRÁCH NHIỆM
				<br><br>THÔNG TIN HIỂN THỊ TẠI WEBSITE NÀY KHÔNG ĐI KÈM BẤT KỲ ĐẢM BẢO HAY CAM KẾT TRÁCH NHIỆM DƯỚI BẤT KỲ HÌNH THỨC NÀO TỪ PHÍA TuTruyen HAY NHÀ CUNG CẤP KHÁC, CHÍNH THỨC HAY HÀM Ý, BAO GỒM NHƯNG KHÔNG GIỚI HẠN VỀ SỰ PHÙ HỢP CỦA SẢN PHẨM, DỊCH VỤ MÀ NGƯỜI MUA ĐÃ LỰA CHỌN.
				<br><br>TUTRUYEN VÀ CÁC NHÀ CUNG CẤP KHÁC CŨNG TỪ CHỐI TRÁCH NHIỆM HAY ĐƯA RA ĐẢM BẢO RẰNG WEBSITE SẼ KHÔNG CÓ LỖI VẬN HÀNH, AN TOÀN, KHÔNG BỊ GIÁN ĐOẠN HAY BẤT CỨ ĐẢM BẢO NÀO VỀ TÍNH CHÍNH XÁC, ĐẦY ĐỦ VÀ ĐÚNG HẠN CỦA CÁC THÔNG TIN HIỂN THỊ.
				<br><br>KHI TRUY CẬP VÀO WEBSITE NÀY, QUÝ KHÁCH MẶC NHIÊN ĐỒNG Ý RẰNG TUTRUYEN, CÁC NHÀ CUNG CẤP KHÁC CÙNG VỚI ĐỐI TÁC LIÊN KẾT, VIÊN CHỨC, CÁN BỘ QUẢN LÝ VÀ NGƯỜI ĐẠI DIỆN CỦA HỌ KHÔNG CHỊU BẤT CỨ TRÁCH NHIỆM NÀO LIÊN QUAN ĐẾN THƯƠNG TẬT, MẤT MÁT, KHIẾU KIỆN, THIỆT HẠI TRỰC TIẾP HOẶC THIỆT HẠI GIÁN TIẾP DO KHÔNG LƯỜNG TRƯỚC HOẶC DO HẬU QUẢ ĐỂ LẠI DƯỚI BẤT KỲ HÌNH THỨC NÀO PHÁT SINH TỪ HAY CÓ LIÊN QUAN ĐẾN VIỆC: (1) SỬ DỤNG CÁC THÔNG TIN TRÊN WEBSITE NÀY; (2) CÁC TRUY CẬP KẾT NỐI TỪ WEBSITE NÀY;(3) ĐĂNG KÝ THÀNH VIÊN, ĐĂNG KÝ NHẬN THƯ ĐIỆN TỬ HAY THAM GIA VÀO CHƯƠNG TRÌNH KHÁCH HÀNG THƯỜNG XUYÊN CỦA TuTruyen; (4) TuTruyenHAY MỘT NHÀ CUNG CẤP NÀO ĐÓ CÓ THỰC HIỆN VIỆC CUNG CẤP DỊCH VỤ HAY KHÔNG THẬM CHÍ TRONG TRƯỜNG HỢP TuTruyen HAY NHÀ CUNG CẤP ĐÓ ĐÃ ĐƯỢC CẢNH BÁO VỀ KHẢ NĂNG XẨY RA THIỆT HẠI;
				<br><br>- Chúng tôi không đảm bảo cho tính chính xác, đầy đủ hay phù hợp cho các mục đích của người sử dụng, tuy nhiên chúng tôi cố gắng hết sức để đáp ứng yêu cầu đó. Khi bạn sử dụng những tài nguyên từ website, bạn phải tự chấp nhận những rủi ro từ việc sử dụng đó. Các tài nguyên chỉ mang tính tham khảo.
				<br><br>- Máy chủ lưu trữ website này có thể bị nhiễm virus hay những thành phần khác có thể gây hại cho máy tính hay tài sản của bạn khi truy cập và/hoặc sử dụng website và/hoặc tài nguyên của website này. Chúng tôi sẽ không chịu trách nhiệm cho bất kỳ sự mất mát hay hư hỏng nảy sinh do sự sử dụng, sự truy cập hay không thể sử dụng, truy cập website này.
				<br><br>- Website có thể tạm hoãn và/hoặc dừng những dịch vụ được cung cấp bất cứ khi nào và không cần thông báo trước. TuTruyen sẽ không chịu trách nhiệm cho bất kỳ hư hỏng nảy sinh do bất kỳ sự sửa đổi hay thay đổi nội dung hoặc không thể sử dụng website này, kể cả trong trường hợp đã báo trước cho chúng tôi.
				<br><br>- Sản phẩm và dịch vụ được trình bày trong website này không mặc nhiên có hiệu lực vào mọi thời điểm và tại mọi địa điểm. Sự giới thiệu sản phẩm hay dịch vụ trong website này không ám chỉ rằng sản phẩm hay dịch vụ này sẽ có giá trị bất cứ lúc nào ở địa điểm xác định của người sử dụng website.
				<br><br>- Website của TuTruyen có thể kết nối tới các website của các hãng thứ ba, và các liên kết website này chỉ dành cho mục đích cung cấp đường dẫn đến các nguồn thông tin có thể hữu ích đối với người sử dụng website của TuTruyen. TuTruyen không chịu trách nhiệm đối với nội dung, sản phẩm, dịch vụ của các website kết nối bao gồm sự chính xác, hoàn chỉnh, độ tin cậy của các thông tin trên website của các bên thứ ba. Khi đó bạn không buộc phải tuân theo hay chịu sự điều chỉnh của Điều khoản này, bởi vậy bạn phải xem xét kỹ Điều khoản sử dụng của các website đó.
				<br><br>g. Quyền sở hữu trí tuệ
				<br><br>Website này và mọi nội dung xếp đặt, hiển thị đều thuộc sở hữu và là tài sản độc quyền khai thác của TuTruyen và các nhà cung cấp có liên quan khác. Mọi sử dụng, trích dẫn phải không gây thiệt hại cho TuTruyen và đều phải tuân thủ các điều kiện sau: (1) Chỉ sử dụng cho mục đích cá nhân, phi thương mại; (2) các sao chép hoặc trích dẫn đều phải giữ nguyên dấu hiệu bản quyền hoặc các yết thị về quyền sở hữu trí tuệ như đã thể hiện trong phiên bản gốc; và (3) mọi sản phẩm, công nghệ hay quy trình được sử dụng hay hiển thị tại Website này đều có thể liên đới đến bản quyền hay sở hữu trí tuệ khác của TuTruyen và các nhà cung cấp có liên quan mà không thể chuyển nhượng được. Tất cả các nội dung được cung cấp tại Website này không được phép nhân bản, hiển thị, công bố, phổ biến, đưa tin tức hay lưu hành cho bất cứ ai, dưới bất kỳ hình thức nào, kể cả trên các Website độc lập khác mà không được sự chấp thuận của TuTruyen . Mọi hình ảnh, thương hiệu, nhãn hiệu hay biểu tượng trình bày tại Website này được bảo vệ bởi Luật sở hữu trí tuệ và các điều luật có liên quan khác và việc Quý khách sử dụng Website này không cho phép Quý khách được quyền sử dụng, nhân bản hay sở hữu.
				<br><br>h. Điều chỉnh và sửa đổi
				<br><br>TuTruyen bảo lưu quyền thay đổi, chỉnh sửa hoặc chấm dứt hoạt động của Website này vào bất cứ thời điểm nào.
				<br><br>i. Luật điều chỉnh và cơ quan giải quyết tranh chấp
				<br><br>Điều kiện Sử dụng này được điều chỉnh bởi luật của nước Cộng hoà Xã hội Chủ Nghĩa Việt nam. Tòa án nước Cộng hòa Xã hội Chủ nghĩa Việt Nam là cơ quan duy nhất có thẩm quyền giải quyết tất cả các tranh chấp có liên quan.
				<br><br>j. Nội dung đăng tải
				<br><br>Hiện TuTruyen đang sử dụng phần mềm máy tính để thu thập, lọc và cập nhật thông tin một cách tự đồng từ nhiều nguồn truyện khác nhau trên Internet. Do đó, TuTruyen sẽ không chịu bất cứ trách nhiệm nào về vi phạm bản quyền tác giả, thông tin... Nếu quý khách có phát hiện bất cứ thông tin nào sai lệch, vi phạm bản quyền ... TuTruyen rất vui vẻ lắng nghe ý kiến của quý khách qua trang Liên hệ .
				<br><br>k. Hạn chế sử dụng
				<br><br>TuTruyen không chấp nhận bất kỳ việc sử dụng website và/hoặc tài nguyên nào cùa website vào một trong những việc sau:
				<br><br>- Chống phá nhà nước CHXHCN Việt Nam.
				<br><br>- Xâm phạm quyền tự do cá nhân của người khác; và/hoặc làm nhục, phỉ báng, bôi nhọ người khác; và/hoặc gây phương hại hay gây bất lợi cho người khác.
				<br><br>- Gây rối trật tự công cộng; và/hoặc phạm pháp hình sự.
				<br><br>- Truyền bá và phân phối thông tin cá nhân của bên thứ ba mà không được sự chấp thuận của họ.
				<br><br>- Sử dụng vào mục đích kinh doanh và/hoặc thương mại mà không có sự chấp thuận trước bằng văn bản của chúng tôi, như là các cuộc thi, cá cược, đổi chác, quảng cáo hoặc kinh doanh đa cấp.
				<br><br>- Truyền đi những tập tin máy tính bị nhiễm virus gây hư hại hoạt động của các máy tính khác.
				<br><br>- Sử dụng bất kỳ thiết bị, phần mềm và/hoặc tiến trình nào nhằm xâm phạm hoặc cố ý xâm phạm đến hoạt động của website.
				<br><br>- Bất kỳ hành động nào không hợp pháp và/hoặc bị cấm bởi các bộ luật tương ứng.
				<br><br>- Hành động xâm phạm đến quyền và lợi ích hợp pháp của TuTruyen .
				<br><br>11. Bất kỳ hành động nào mà chúng tôi cho rằng không thích hợp.
				<br><br>TuTruyen bảo lưu quyền huỷ bỏ yêu cầu sử dụng tính năng này của Quý khách mà không cần bất kỳ thông báo nào nếu thấy rằng Quý khách đang vi phạm, hoặc có thể sẽ vi phạm giới hạn sử dụng này.
			</p>

		</div>

	</div>
</div>