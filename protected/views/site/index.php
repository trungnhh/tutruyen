<?php
	$url = new Url();
?>

<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRs"] ?>/css/home.css" media="all" />
<script src="<?=Yii::app()->params["urlRs"] ?>/js/home.js" type="text/javascript"></script>

<div class="autogen_class_views_home_index">

	<div class="positioned">

		<?php $this->widget("Branding"); ?>

		<div class="document_lists">

			<div id="posts_new" data-lazy_images="true" data-lazy_horizontal_images="true" class="autogen_class_views_shared_carousels_document_carousel autogen_class_views_shared_carousels_carousel has_paddles carousel_widget">
				<div class="module_edge_right"></div>
				<div class="paddle page_left carousel_prev always_visible">
					<div class="sprite paddle_left"></div>
				</div>
				<div class="paddle page_right carousel_next always_visible">
					<div class="sprite paddle_right"></div>
				</div>
				<div class="carousel_title">
					<h2><a href="<?php echo $url->createUrl("site/new"); ?>" class="title_link">Tuyển tập truyện mới</a></h2>
					<span class="sub_title">HOT</span>
				</div>
				<div class="carousel" data-jcarousel="true">
					<ul style="left: 0px; top: 0px;">
						<?php foreach($data_posts_new as $row) {
							$cat_ids = explode(',', $row['category_id']);
							foreach($data_category as $item){
								if(in_array($item['id'], $cat_ids)){
									$category_alias = $item['alias'];
								}
							}
							?>
							<li class="item">
								<div class="document_cell object_cell ">
									<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>">
										<div
											style="background-image: url(&quot;<?php echo StringUtils::getImageUrl($row['post_image'], "post"); ?>&quot;);"
											class="thumb">
											<?php if($row['post_end'] == 1) { ?>
												<div class="flag purchase_only">HOÀN THÀNH</div>
											<?php } ?>
											<?php if($row['post_is_full'] == 1) { ?>
												<div class="flag sample">ĐANG CẬP NHẬT</div>
											<?php } ?>
											<!--<div class="geo_restricted flag">NOT AVAILABLE</div>-->
											<div class="overlay">
												<div class="sprite white_big_open_book">
												</div>
											</div>
										</div>
									</a>

									<div class="cell_data">
										<div class="document_title">
											<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>">
												<?php echo $row['post_title']; ?>
											</a>
										</div>
										<div class="document_author">
											<div class="author_row">
												<?php if($row['author_name'] == '') {
														echo "Sưu tầm";
													} else { ?>
														<a href="<?php echo $url->createUrl("author/detail", array("author_alias" => $row['author_alias'])); ?>"><?php echo $row['author_name']; ?></a>
													<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</li>
						<?php } ?>
						<li class="carousel_separator item"></li>
					</ul>
				</div>
			</div>

			<div id="posts_hot" data-lazy_images="true" data-lazy_horizontal_images="true" class="autogen_class_views_shared_carousels_document_carousel autogen_class_views_shared_carousels_carousel has_paddles carousel_widget">
				<div class="module_edge_right"></div>
				<div class="paddle page_left carousel_prev always_visible">
					<div class="sprite paddle_left"></div>
				</div>
				<div class="paddle page_right carousel_next always_visible">
					<div class="sprite paddle_right"></div>
				</div>
				<div class="carousel_title">
					<h2><a href="<?php echo $url->createUrl("site/hot"); ?>" class="title_link">Tuyển tập truyện Hot</a></h2>
					<span class="sub_title">HOT</span>
				</div>
				<div class="carousel" data-jcarousel="true">
					<ul style="left: 0px; top: 0px;">
						<?php foreach($data_posts_hot as $row) {
							$cat_hot_ids = explode(',', $row['category_id']);
							foreach($data_category as $item){
								if(in_array($item['id'], $cat_hot_ids)){
									$category_alias = $item['alias'];
								}
							}
							?>
							<li class="item">
								<div class="document_cell object_cell ">
									<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>">
										<div
											style="background-image: url(&quot;<?php echo StringUtils::getImageUrl($row['post_image'], "post"); ?>&quot;);"
											class="thumb">
											<?php if($row['post_end'] == 1) { ?>
												<div class="flag purchase_only">HOÀN THÀNH</div>
											<?php } ?>
											<?php if($row['post_is_full'] == 1) { ?>
												<div class="flag sample">ĐANG CẬP NHẬT</div>
											<?php } ?>
											<!--<div class="geo_restricted flag">NOT AVAILABLE</div>-->
											<div class="overlay">
												<div class="sprite white_big_open_book">
												</div>
											</div>
										</div>
									</a>

									<div class="cell_data">
										<div class="document_title">
											<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>">
												<?php echo $row['post_title']; ?>
											</a>
										</div>
										<div class="document_author">
											<div class="author_row">
												<?php if($row['author_name'] == '') {
													echo "Sưu tầm";
												} else { ?>
													<a href="<?php echo $url->createUrl("author/detail", array("author_alias" => $row['author_alias'])); ?>"><?php echo $row['author_name']; ?></a>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</li>
						<?php } ?>
						<li class="carousel_separator item"></li>
					</ul>
				</div>
			</div>

			<div id="posts_vote" data-lazy_images="true" data-lazy_horizontal_images="true" class="autogen_class_views_shared_carousels_document_carousel autogen_class_views_shared_carousels_carousel has_paddles carousel_widget">
				<div class="module_edge_right"></div>
				<div class="paddle page_left carousel_prev always_visible">
					<div class="sprite paddle_left"></div>
				</div>
				<div class="paddle page_right carousel_next always_visible">
					<div class="sprite paddle_right"></div>
				</div>
				<div class="carousel_title">
					<h2><a href="<?php echo $url->createUrl("site/vote"); ?>" class="title_link">Tuyển tập truyện hay</a></h2>
					<span class="sub_title">HOT</span>
				</div>
				<div class="carousel" data-jcarousel="true">
					<ul style="left: 0px; top: 0px;">
						<?php foreach($data_posts_vote as $row) {
							$cat_vote_ids = explode(',', $row['category_id']);
							foreach($data_category as $item){
								if(in_array($item['id'], $cat_vote_ids)){
									$category_alias = $item['alias'];
								}
							}
							?>
							<li class="item">
								<div class="document_cell object_cell ">
									<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>">
										<div
											style="background-image: url(&quot;<?php echo StringUtils::getImageUrl($row['post_image'], "post"); ?>&quot;);"
											class="thumb">
											<?php if($row['post_end'] == 1) { ?>
												<div class="flag purchase_only">HOÀN THÀNH</div>
											<?php } ?>
											<?php if($row['post_is_full'] == 1) { ?>
												<div class="flag sample">ĐANG CẬP NHẬT</div>
											<?php } ?>
											<!--<div class="geo_restricted flag">NOT AVAILABLE</div>-->
											<div class="overlay">
												<div class="sprite white_big_open_book">
												</div>
											</div>
										</div>
									</a>

									<div class="cell_data">
										<div class="document_title">
											<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>">
												<?php echo $row['post_title']; ?>
											</a>
										</div>
										<div class="document_author">
											<div class="author_row">
												<?php if($row['author_name'] == '') {
													echo "Sưu tầm";
												} else { ?>
													<a href="<?php echo $url->createUrl("author/detail", array("author_alias" => $row['author_alias'])); ?>"><?php echo $row['author_name']; ?></a>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</li>
						<?php } ?>
						<li class="carousel_separator item"></li>
					</ul>
				</div>
			</div>

			<?php for($i = 0; $i < count($data_category_hot); $i ++) { ?>
				<div id="category_<?php echo $data_category_hot[$i]['id']; ?>" data-lazy_images="true" data-lazy_horizontal_images="true" class="autogen_class_views_shared_carousels_document_carousel autogen_class_views_shared_carousels_carousel has_paddles carousel_widget">
					<div class="module_edge_right"></div>
					<div class="paddle page_left carousel_prev always_visible">
						<div class="sprite paddle_left"></div>
					</div>
					<div class="paddle page_right carousel_next always_visible">
						<div class="sprite paddle_right"></div>
					</div>
					<div class="carousel_title">
						<h2><a href="<?php echo $url->createUrl("category/index", array("alias" => $data_category_hot[$i]['alias'])); ?>" class="title_link"><?php echo $data_category_hot[$i]['name']; ?></a></h2>
					</div>
					<div class="carousel" data-jcarousel="true">
						<ul style="left: 0px; top: 0px;">
							<?php foreach($data_posts[$i] as $row) { ?>
								<li class="item">
									<div class="document_cell object_cell ">
										<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $data_category_hot[$i]['alias'], "post_alias" => $row['post_alias'])); ?>">
											<div style="background-image: url(&quot;<?php echo StringUtils::getImageUrl($row['post_image'], "post"); ?>&quot;);" class="thumb">
												<?php if($row['post_end'] == 1) { ?>
													<div class="flag purchase_only">HOÀN THÀNH</div>
												<?php } ?>
												<?php if($row['post_is_full'] == 1) { ?>
													<div class="flag sample">ĐANG CẬP NHẬT</div>
												<?php } ?>
												<!--<div class="geo_restricted flag">NOT AVAILABLE</div>-->

												<div class="overlay">
													<div class="sprite white_big_open_book">
													</div>
												</div>
											</div>
										</a>

										<div class="cell_data">
											<div class="document_title">
												<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $data_category_hot[$i]['alias'], "post_alias" => $row['post_alias'])); ?>">
													<?php echo $row['post_title']; ?>
												</a>
											</div>
											<div class="document_author">
												<div class="author_row">
													<?php if($row['author_name'] == '') {
														echo "Sưu tầm";
													} else { ?>
														<a href="<?php echo $url->createUrl("author/detail", array("author_alias" => $row['author_alias'])); ?>"><?php echo $row['author_name']; ?></a>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
								</li>
							<?php } ?>
							<li class="carousel_separator item"></li>
						</ul>
					</div>
				</div>

				<script type="text/javascript">
					//&lt;![CDATA[

					new Scribd.UI.Carousel("#category_<?php echo $data_category_hot[$i]['id']; ?>")

					//]]&gt;
				</script>
			<?php } ?>

			<!--<div id="load_more_btn" class="load_more load_more_btn">
				<span class="normal_text" onclick="load_more()">TẢI THÊM</span>
				<span class="load_text" style="display: none;">ĐANG TẢI</span>
				<span class="empty_text">HẾT</span>
			</div>-->

		</div>

	</div>

</div>

<script type="text/javascript">
	//&lt;![CDATA[
	new Scribd.UI.Carousel("#posts_new");
	new Scribd.UI.Carousel("#posts_hot");
	new Scribd.UI.Carousel("#posts_vote");

	function load_more(){
		loadingAjax();
		$('.normal_text').hide();
		$('.load_text').show();

		var strUrl = '<?=$url->createUrl("site/ajaxLoadMoreCategory") ?>';
		$.ajax({
			type: "POST",
			url: strUrl,
			data: {},
			success: function(msg){
				$('#posts_vote').after(msg);
				$('.normal_text').hide();
				$('.load_text').hide();
				$('.empty_text').show();
				closeLoadingAjax();
			}
		});
	}

	//]]&gt;
</script>