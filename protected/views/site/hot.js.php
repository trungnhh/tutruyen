<script>

	new Scribd.Browse.GridPage("#category_page");

	var category_alias = $('#category_alias').val();
	var page = $('#page').val();
	var sort_view = $('#sort_view').val();
	var sort_complete = $('#sort_complete').val();

	function change_filter_category(type) {

		if(type == "new"){
			sort_view = 0;
		}
		if(type == "view"){
			sort_view = 1;
		}
		if(type == "all"){
			sort_complete = 0;
		}
		if(type == "complete"){
			sort_complete = 1;
		}

		var strUrl = '<?php echo $url->createUrl("category/ajaxChangeFilter") ?>';
		$.ajax({
			type: 'POST',
			url: strUrl,
			data: {
				sort_view: sort_view,
				category_alias: category_alias,
				sort_complete: sort_complete,
				page: page
			},
			success: function (data) {
				window.location.href=data;
			},
			error: function (data) {
				window.location.href=data;
			}
		});
	}

</script>