<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRs"] ?>/css/static.css" media="all" />
<div class="autogen_class_views_static_contact autogen_class_views_static_about_container">

	<div class="container">
		<h1>Liên hệ với chúng tôi</h1>

		<h2>Giúp đỡ và hỗ trợ</h2>

		<p class="email_contacts">
			Hỗ trợ người đọc:
			<a href="mailto:info@sautruyen.com">info@sautruyen.com</a><br>
			Hỗ trợ sử dụng hệ thống:
			<a href="mailto:anhtl@sautruyen.com">anhtl@sautruyen.com</a><br>
			Các vấn đề về bản quyền:
			<a href="mailto:content@sautruyen.com">content@sautruyen.com</a><br>
		</p><br>

		<h2>Hợp tác kinh doanh</h2>

		<p class="email_contacts">
			Tác giả / Nhóm dịch:
			<a href="javascript:">content@sautruyen.com</a><br>
			Hợp tác phát triển kinh doanh:
			<a href="mailto:muoidv@sautruyen.com">muoidv@sautruyen.com</a><br>
			Quảng cáo:
			<a href="mailto:namnx@sautruyen.com">namnx@sautruyen.com</a><br></p>
		<hr>
		<div class="contact_address_wrapper">
			<div class="left_column">
				<div class="info_block mailing">
					<h3>Văn phòng Tủ Truyện</h3>Hà Nội<br>Việt Nam<br>
				</div>
				<div class="info_block social"><h3>Các kênh truyền thông:</h3>
					<a href="javascript:" class="social_link facebook"><span class="contact_icon facebook"></span>Facebook</a><br>
					<a href="javscript:" class="social_link linkedin"><span class="contact_icon linkedin"></span>LinkedIn</a><br>
					<a href="javscript:" class="social_link twitter"><span class="contact_icon twitter"></span>Twitter</a>
				</div>
			</div>
			<div class="right_column"><br>
				<div class="embedded_map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14894.588888618053!2d105.81114830000001!3d21.04679700000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab9bd9861ca1%3A0xe7887f7b72ca17a9!2zaMOgIG7hu5lp!5e0!3m2!1sen!2s!4v1387000283500" width="335" height="335" frameborder="0" style="border:0"></iframe>
				</div>
			</div>
		</div>
	</div>
</div>