﻿<?php
	$url = new Url();
	$content = nl2br($data_post['post_content']);
	$post_title_no_sign = StringUtils::RemoveSign($data_post['post_title']);
?>
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRs"] ?>/css/post.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRs"] ?>/css/home.css" media="all" />
	<script src="<?=Yii::app()->params["urlRs"] ?>/js/read.js" type="text/javascript"></script>
	<input type="hidden" id="check_login" value="<?php echo isset($_SESSION["user_tt"])?1:0; ?>">

	<div class="epub_page doc_page standard_view">

	<div class="toolbar_spacer" style="height: 49px;">
		<div id="doc_toolbar" class="autogen_class_views_read2_epub_toolbar doc_toolbar osx" style="top: 56px;">
			<div class="toolbar_notification welcome_notification">
				<!--<span class="icon icon-close close_btn" onclick="hide_error()"></span>-->
				<span id="msg-content">
					Chào mừng bạn đến với <?php echo Helpers::getBrandDomain(); ?>
				<a href="javascript:">Đăng ký ➡</a>
					</span>
			</div>
			<div class="doc_toolbar_inner">
				<div class="toolbar_left_actions">
					<div class="read_mode_toggle">
						<div data-name="standard" class="toggle_btn left_btn normal_mode_btn active">
							<div class="book_icon single_icon"></div>
							Chế độ xem chuẩn
						</div><div data-name="full" class="toggle_btn right_btn read_mode_btn">
							<div class="book_icon double_icon"></div>
							Chế độ xem full
						</div>
					</div>
				</div>
				<span id="read_percent" class="read_percent">0%</span>

				<div class="toolbar_buttons">

					<div data-tooltip="Cấu hình" class="toolbar_button font_settings_btn">
						<span class="icon icon-font"></span>
					</div>

					<?php if($check_bookmark == 0){ ?>
						<div id="toolbar_button_library" class="toolbar_button save_bookmark_btn" data-tooltip="Lưu vào tủ sách" onclick="show_error('not_login')">
							<span class="icon icon-books"></span>
							<span class="icon_label">Lưu vào tủ sách</span>
						</div>
					<?php } ?>
					<?php if($check_bookmark == 1){ ?>
						<div id="toolbar_button_library" class="toolbar_button save_bookmark_btn" data-tooltip="Lưu vào tủ sách" onclick="add_bookmark(<?php echo $data_post['id']; ?>,'<?php echo $data_post['post_alias']; ?>',1)">
							<span class="icon icon-books"></span>
							<span class="icon_label">Lưu vào tủ sách</span>
						</div>
					<?php } ?>
					<?php if($check_bookmark == 2){ ?>
						<div id="toolbar_button_library" class="toolbar_button save_bookmark_btn saved" data-tooltip="Lưu vào tủ sách" onclick="add_bookmark(<?php echo $data_post['id']; ?>,'<?php echo $data_post['post_alias']; ?>',0)">
							<span class="icon icon-books"></span>
							<span class="icon_label">Lưu vào tủ sách</span>
						</div>
					<?php } ?>

					<?php if($check_vote == 0){ ?>
						<div id="toolbar_button_like" class="toolbar_button readcast_btn" data-tooltip="Bình chọn" onclick="show_error('not_login')">
							<span class="icon icon-like"></span>
							<span class="icon_label">Bình chọn</span>
						</div>
					<?php } ?>
					<?php if($check_vote == 1){ ?>
						<div id="toolbar_button_like" class="toolbar_button readcast_btn" data-tooltip="Bình chọn" onclick="vote_post(<?php echo $data_post['id']; ?>,1)">
							<span class="icon icon-like"></span>
							<span class="icon_label">Bình chọn</span>
						</div>
					<?php } ?>
					<?php if($check_vote == 2){ ?>
						<div id="toolbar_button_like" class="toolbar_button readcast_btn liked" data-tooltip="Bình chọn" onclick="vote_post(<?php echo $data_post['id']; ?>,0)">
							<span class="icon icon-like"></span>
							<span class="icon_label">Bình chọn</span>
						</div>
					<?php } ?>

					<div class="toolbar_button sync_mobile_btn" data-tooltip="Đồng bộ">
						<span class="icon icon-mobile"></span>
						<span class="icon_label">Đồng bộ với điện thoại</span>
					</div>

					<div class="toolbar_button share_btn" data-tooltip="Chia sẻ">
						<span class="icon icon-share"></span>
						<span class="icon_label">Chia sẻ</span>
					</div>
				</div>
				<div class="toolbar_popups">
					<div id="sync_mobile_popup" class="sync_mobile_popup toolbar_popup">
						<div class="sprite popup_arrow"></div>
						<div class="close_btn">×</div>
						<div class="popup_content">
							<div class="mobile_signup">
								<p class="error_sms">There was a problem sending you an sms. Check your phone number or try again later.</p>

								<p class="sent_sms">We've sent
									<span class="phone_number"></span> a link to the Scribd app. If you didn't receive it, try again.
								</p>

								<p class="send_sms">Chúng tôi sẽ sớm có mặt trên thiết bị di động của bạn.</p>

								<div class="mobile_signup footer">
									<a href="javascript:">iTunes App Store</a> |
									<a href="javascript:">Google Play Store</a>
								</div>
							</div>
						</div>
					</div>
					<div id="font_popup" class="font_popup toolbar_popup">
						<div class="sprite popup_arrow"></div>
						<div class="close_btn">×</div>
						<div class="popup_content">
							<div data-picker_name="font_size" class="option_picker epub_font_size_picker three_way">
								<div class="label">Size</div>
								<div class="option selected" onclick="change_size(0)">Mặc định</div>
								<div class="option minus" onclick="change_size(1)">-</div>
								<div class="option plus last" onclick="change_size(2)">+</div>
							</div>
							<div class="line"></div>
							<div data-picker_name="font_style" class="option_picker font_style_picker three_way">
								<div class="label">Font</div>
								<div class="option serif_text selected" onclick="change_font(0)">Mặc định</div>
								<div class="option sans_text " onclick="change_font(1)">Verdana</div>
								<div class="option serif_text last" onclick="change_font(2)">Time New</div>
							</div>
							<div class="line"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="epub_doc_view">

		<div id="doc_page_column" class="doc_page_column">

			<div id="doc_container" class="doc_container" style="width: 884px;">

				<div id="doc_info" class="doc_info">
					<h1 class="doc_title"><b><?php echo $data_post['post_title']; ?></b></h1>

					<div class="doc_authors">
						<span class="verified_badge"></span>
						<?php if($data_post['author_name'] != '') { ?>
							<a target="_blank" href="<?php echo $url->createUrl("author/detail", array("author_alias" => $data_post['author_alias'])); ?>" id="author"><?php echo $data_post['author_name']; ?></a>
						<?php } else { ?>
							<a href="javascript:" id="author">Sưu tầm</a>
						<?php } ?>
					</div>
					<div class="doc_stats">
						<span data-tooltip_template="Unescaped" data-tooltip="&lt;span style='white-space: nowrap'&gt;Views: 22,952,&lt;/span&gt; &lt;span style='white-space: nowrap'&gt;Embed Views: 233&lt;/span&gt;" class="inline_stat">
							<span class="label">Lượt đọc: </span>
							<span class="value"><?php echo $data_post['post_views']; ?></span>
						</span>
						<span class="stat_divider">|</span>
						<span class="inline_stat">
							<span class="label">Lượt bình chọn: </span>
							<span class="value like_count_value"><?php echo $data_post['post_vote']; ?></span>
						</span>
					</div>
					<div class="fb_like_header">
						<span style="float: left">Like để ủng hộ Sâu Truyện bạn nhé</span>
						<div>
							<div class="fb-like" data-width="80" data-layout="button_count" data-show-faces="false" data-send="true"></div>
							<div class="g-plusone"></div>
						</div>
					</div>
					<div class="doc_description doc_description_short"></div>

					<div class="more_holder toggle_open_btn"></div>
				</div>

				<div id="storyText" class="autogen_class_views_read2_epub_standard_view">
					<?php
						echo $content;
					?>
					<h2 class="tag">Tags : <a href="<?php echo $url->createUrl("category/index",array("alias"=>$category_alias)); ?>"><?php echo str_replace('-',' ',$category_alias); ?></a>
						<?php if($data_post_parent != null){ ?> , <a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $data_post_parent['post_alias'])); ?>">
							<?php echo StringUtils::RemoveSign($data_post_parent['post_title']); ?></a>, <a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $data_post_parent['post_alias'])); ?>"><?php echo $data_post_parent['post_title']; ?></a> <?php } ?>
					</h2>
				</div>

				<?php
					if($data_all_chap != null){
						for($i=0;$i<count($data_all_chap);$i++){
							$a = 0;
							$a = $i - 1;
							$b = $i + 1;
							if($data_all_chap[$i]['post_id'] ==  $data_post['id']){
								?>
				<div id="document_activity" class="document_activity has_more">
						<h2></h2>
								<div class="action_picker" style="border-bottom: none;">
									<div class="action_buttons">
										<?php if($data_all_chap[$a] != null){ ?>
										<?php } ?>
										<?php if($data_all_chap[$b] != null){ ?>
											<a href="<?php echo $url->createUrl("post/detail",array("category_alias"=>$data_all_chap[$b]['alias'],"post_alias"=>$data_all_chap[$b]['post_alias'])); ?>" class="action_btn readcast_action selected">
												<span class="icon icon-embed"></span>Đọc tiếp : <?php echo $data_all_chap[$b]['post_title']; ?> ››
											</a>
										<?php } ?>
									</div>
								</div>
			</div>
							<?php
							}
						}
					}
				?>

				<?php if($data_chap != null){  ?>
				<div id="chap_area" class="footer_documents">
				<div id="document_activity" class="document_activity has_more">
					<h2 id="box-title">Danh sách chương <span class="activity_count">(<span class="activity_count_value"><?php echo $count; ?></span>)</span></h2>

					<!--<div class="action_picker">
						<div class="action_buttons">
							<a href="" class="action_btn add_to_collection_btn">
								<span class="icon icon-collections"></span>Add to collection
							</a>
							<a href="" class="action_btn review_action">
								<span class="icon icon-edit"></span>Review
							</a>
							<a href="" class="action_btn addnote_action">
								<span class="icon icon-addnote"></span>Add note
							</a>
							<a href="" class="action_btn readcast_action selected">
								<span class="icon icon-like"></span>Like
							</a>
						</div>
					</div>-->
					<div class="event_lists">
						<div class="event_list">
							<?php foreach($data_chap as $row){  ?>
							<div class="document_readcast chap_list document_event">
								<div class="event_content ml0">
									<div class="event_data">
										<a href="<?php echo $url->createUrl("post/detail",array("category_alias"=>$row['alias'],"post_alias"=>$row['post_alias'])); ?>"><?php echo $row['post_title'] ?></a>
										<span class="data_divider">|</span><span class="event_time">Cập nhật ngày <?php echo date('d/m/Y',$row['update_date']) ?></span>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>

					<div class="events_footer">
						<div class="footer_row">
						<div class="paginator">
							<ul class="paging">
								<?php
									echo $paging;
								?>
							</ul>
						</div>
						</div>
					</div>
				</div>
					</div>
				<?php } ?>


				<div class="fb-comments" data-href="<?php echo Helpers::getCurrentUrl(); ?>" data-width="100%" data-numposts="10" data-order-by="social" data-colorscheme="light"></div>

				<div id="footer_documents" class="footer_documents">

					<div id="user_docs_container">

						<div id="posts_same_category" data-lazy_horizontal_images="true" class="autogen_class_views_read2_lists_latest_user_docs autogen_class_views_shared_carousels_document_carousel autogen_class_views_shared_carousels_carousel has_paddles carousel_widget">
							<div class="module_edge_right"></div>
							<div data-track="carousel:previous" class="paddle page_left carousel_prev" style="bottom: 145px;">
								<div class="sprite paddle_left"></div>
							</div>
							<div data-track="carousel:next" class="paddle page_right carousel_next" style="bottom: 145px;">
								<div class="sprite paddle_right"></div>
							</div>
							<div data-track="more_from_this_author" class="carousel_title">
								<a href="javascript:" class="title_link">Tác phẩm cùng chuyên mục</a>
							</div>
							<div class="carousel" data-jcarousel="true">

								<ul style="left: 0px; top: 0px;">

									<?php foreach($data_post_random as $row) { ?>
										<li class="item">
											<div class="document_cell object_cell ">
												<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>">
													<div style="background-image: url('<?php echo StringUtils::getImageUrl($row['post_image'], "post"); ?>')" data-thumb_url="" class="thumb">
														<?php if($row['post_end'] == 1) { ?>
															<div class="flag purchase_only">HOÀN THÀNH</div>
														<?php } ?>
														<?php if($row['post_is_full'] == 1) { ?>
															<div class="flag sample">ĐANG CẬP NHẬT</div>
														<?php } ?>
														<div class="overlay">
															<div class="sprite white_big_open_book">
															</div>
														</div>
													</div>
												</a>

												<div class="cell_data">
													<div class="document_title">
														<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $category_alias, "post_alias" => $row['post_alias'])); ?>">
															<?php echo $row['post_title']; ?>
														</a>
													</div>
													<div class="document_author">
														<div class="author_row">
															<a href="javascript:"><?php if($row['author_id'] == 0) {
																	echo "Sưu tầm";
																} else {
																	echo $row['author_name'];
																} ?></a>
														</div>

													</div>
												</div>
											</div>
										</li>
									<?php } ?>

									<li class="carousel_separator item"></li>
								</ul>
							</div>

						</div>

						<script type="text/javascript">
							//&lt;![CDATA[

							new Scribd.UI.Carousel("#posts_same_category", {page: 'read2'})


							//]]&gt;
						</script>

					</div>

				</div>

			</div>

			<div id="doc_sidebar" class="doc_sidebar" style="">
				<?php $this->widget("Recommended"); ?>
			</div>

		</div>

	</div>

	</div>

<?php
	if(!isset($_SESSION["user_t186"])){
		//$this->widget("AdvPostFooter");
	}
?>


<?php include "detail.js.php"; ?>