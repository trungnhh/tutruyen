<script type="text/javascript">

	var size_default = 16;
	var size_current = 16;
	var font_default = 'Helvetica,sans-serif';
	var font_current = 'Helvetica,sans-serif';

	Scribd.ReadPage.toolbar = new Scribd.EpubToolbar("#doc_toolbar");
	new Scribd.SyncMobilePopup("#sync_mobile_popup");
	Scribd.ReadPage.font_popup = new Scribd.FontPopup("#font_popup");

	$(window).scroll(function(){
		var $this = $(this),scrollPercentage = 100 * $this.scrollTop() / ($('#doc_container').height()-$this.height());
		var scroll_percent = scrollPercentage.toFixed(0);
		if(scroll_percent > 100){
			scroll_percent = 100;
		}
		$('#read_percent').html(scroll_percent+'%');
	});

	function vote_post(post_id,status){
		if(post_id == 0 || post_id == ''){
			return false;
		}
		$("#toolbar_button_like").removeAttr("onclick");
		var strUrl = '<?=$url->createUrl("post/ajaxVotePost") ?>';
		$.ajax({
			type: "POST",
			url: strUrl,
			data: {post_id:post_id,status:status},
			success: function(msg){
				if(status == 1){
					$("#toolbar_button_like").addClass("liked");
				}else{
					$("#toolbar_button_like").removeClass("liked");
				}
			}
		});
	}

	function add_bookmark(post_id,post_alias,status){
		if(post_id == 0 || post_id == ''){
			return false;
		}
		$("#toolbar_button_library").removeAttr("onclick");
		var strUrl = '<?=$url->createUrl("post/ajaxAddBookmark") ?>';
		$.ajax({
			type: "POST",
			url: strUrl,
			data: {post_id:post_id,post_alias:post_alias,status:status},
			success: function(msg){
				if(status == 1){
					$("#toolbar_button_library").addClass("saved");
				}else{
					$("#toolbar_button_library").removeClass("saved");
				}
			}
		});

		//alert(post_id);return;
	}

	function change_size(size){
		switch(size){
			case 0:
				$('#storyText').css('font-size',size_default + "px");
				break;
			case 1:
				size_current--;
				$('#storyText').css('font-size',size_current + "px");
				break;
			case 2:
				size_current++;
				$('#storyText').css('font-size',size_current + "px");
				break;
			default :
				$('#storyText').css('font-size',size_default + "px");
				break;
		}
	}

	function change_font(font){
		switch(font){
			case 0:
				$('#storyText').css('font-family',font_default);
				break;
			case 1:
				font_current = 'Verdana,sans-serif';
				$('#storyText').css('font-family',font_current);
				break;
			case 2:
				font_current = 'Times New Roman,sans-serif';
				$('#storyText').css('font-family',font_current);
				break;
			default :
				$('#storyText').css('font-family',font_default);
				break;
		}
	}

	function submitComment(post_id,comment_parent) {
		var check_login = $('#check_login').val();
		if(check_login == 0){
			show_comment_error('Bạn hãy đăng nhập để sử dụng chức năng này!');
			return false;
		}
		var comment = $.trim($('#comment').val());
		if(comment_parent != 0){
			var textbox = comment_parent + "_replybox";
			comment = $.trim($('#'+textbox).val());
		}

		if (comment=="") {
			show_comment_error('Bạn chưa nhập nội dung bình luận!');
			return;
		}
		if (comment.length>2000) {
			show_comment_error('Nội dung bình luận của bạn quá dài!');
			return;
		}

		$('#comment').attr('disabled', 'true');
		$('#comment_button').attr('disabled', 'true');

		var strUrl = '<?=$url->createUrl("post/ajaxAddComment") ?>';
		$.ajax({
			type: 'POST',
			url: strUrl,
			data: {
				post_id: post_id,
				comment_parent: comment_parent,
				comment: comment
			},
			success: function(data) {
				hide_comment_error();
				if (data!='') {

					if(comment_parent == 0){
						var comment = $(data).hide();
							comment.prependTo('#comments_list').slideDown('slow', function() {
							$('#comment').val('').removeAttr('disabled');
						});
					}else{
						var comment = $(data).hide();
						comment.appendTo(replyarea).slideDown('slow', function() {
							// Update comment textarea
							$('#comment').val('').removeAttr('disabled').trigger('change');
							$("#"+textbox).val('').removeAttr('disabled').trigger('change');
							$(replytools + ' .commentbutton').removeAttr("disabled");
						});
					}

				}
				else {
					show_comment_error('Có lỗi trong quá trình hiển thị comment. Hãy tải lại trang web.');
				}
			},
			error: function(data) {
				$('#comment').removeAttr('disabled');
				$('#comment_button').removeAttr('disabled');
			}
		});
	}

	function show_error(msg){
		if(msg == 'not_login'){
			msg = 'Bạn cần là thành viên của Tủ Truyện để sử dụng chức năng này!   <a href="<?php echo $url->createUrl("user/login"); ?>">Đăng ký ➡</a>';
		}
		$('#msg-content').html(msg);
		$('.toolbar_notification').slideDown('slow');
		$('.toolbar_notification').delay(4000).slideUp('slow');
	}

	function hide_error(){
		$('.toolbar_notification').slideUp('slow');
	}

	function show_comment_error(msg){
		$('.comment_form_error .error_text').html(msg);
		$('.comment_form_error').css('opacity',0).show().animate({opacity:1});
	}

	function hide_comment_error(){
		$('.comment_form_error').css('opacity',1).show().animate({opacity:0});
	}

</script>

<script type="text/javascript">
	window.___gcfg = {lang: 'vi'};

	(function() {
		var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		po.src = 'https://apis.google.com/js/platform.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	})();
</script>