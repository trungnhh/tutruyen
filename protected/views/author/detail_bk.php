<?php 
    $url = new Url();     
?>         
<div class="column_left">

    <div class="container left">               
        <div style="padding:10px;">
            <div class="panel_avatar ">
                <h2 style=" white-space: nowrap;"><?php echo $data_author['author_name']; ?></h2>
                <img width="128" height="128" class="picframe" src="<?php echo StringUtils::getImageUrl($data_author['author_image'],"author"); ?>">
            </div>

            <div class="panel_text ">
                <?php
                    $content = $data_author['author_description'];                                                                        
                    $content = nl2br($content);     
                    echo $content;
                ?>
            </div>
        </div>

    </div>

    <div class="container left">
        <h2 class="container_head">Tác phẩm</h2>
        <?php foreach($data_post as $row){ ?>
            <table width="100%" cellspacing="0" cellpadding="0" border="0" class="text_item_table">
                <tbody>
                    <tr class="storyitem evenrow" id="21140777">
                        <td class="left">
                            <a href="<?php echo $url->createUrl("post/detail",array("category_alias"=>$row['alias'],"post_alias"=>$row['post_alias'])); ?>">
                                <img width="64" height="100" class="picframe" alt="<?php echo $row['post_title']; ?>" src="<?php echo StringUtils::getImageUrl($row['post_image'],"post"); ?>">
                            </a>
                        </td>
                        <td valign="top" style="position:relative;">
                            <h1 style="margin-bottom:2px">
                                <a class="title" href="<?php echo $url->createUrl("post/detail",array("category_alias"=>$row['alias'],"post_alias"=>$row['post_alias'])); ?>"><?php echo $row['post_title']; ?></a>
                            </h1>
                            <div class="meta_element">cập nhật <?php echo date('d/m/Y',$row['update_date']); ?></div>                                                        
                                <?php
                                    if($row['post_description'] != ''){
                                        echo $row['post_description'];    
                                    }else{
                                        $content = $row['post_content'];                                    
                                        $content = StringUtils::cutstring($content,200,true);                        
                                        $content = nl2br($content);     
                                        echo $content;
                                    }
                                ?>
                            <div class="meta_stats"> 
                                    <span class="high"><?php echo $row['post_views']; ?> lượt đọc</span> 
                                    <img width="14" height="14" style="vertical-align:middle" title="votes" alt="votes" src="<?php echo Yii::app()->params['urlRs'];?>/images/votes.png"> 
                                    <span class="high"><?php echo $row['post_vote']; ?></span> 
                                    <img width="14" height="14" style="vertical-align:middle" title="comments" alt="comments" src="<?php echo Yii::app()->params['urlRs'];?>/images/comments.png"> 
                                    <span class="high"><?php echo $row['post_comments']; ?></span> 
                                </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php } ?>
    </div>    

</div> 

<div class="column_right_bubble">    
    <?php $this->widget("Featured");?>
    <?php $this->widget("AdvRight");?>
    <div id="recommended_users" class="">        
        <?php $this->widget("CategoryRight");?>

        <?php //$this->widget("Author");?>    

        <?php $this->widget("FooterRight");?>    
    </div>
</div>