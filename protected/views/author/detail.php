<?php
	$url = new Url();
?>

<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRs"] ?>/css/category.css" media="all" />

<div id="category_page" class="autogen_class_views_browse_index autogen_class_views_shared_filtered_document_page">

	<input type="hidden" id="category_alias" value="<?php echo $category_alias; ?>">
	<input type="hidden" id="page" value="<?php echo $page; ?>">
	<input type="hidden" id="sort_view" value="<?php echo $sort_view; ?>">
	<input type="hidden" id="sort_complete" value="<?php echo $sort_complete; ?>">

	<div class="page_header">
		<h1>
			<a href="javascript:" class="sub_crumb">Các tác phẩm của</a>
			<span class="split"></span>
			<span class="crumb"><?php echo $data_author['author_name']; ?></span>
		</h1>
	</div>

	<?php
		if(!isset($_SESSION["user_tt"])){
			$this->widget("AdvCategoryTop");
		}

	?>

	<div class="document_drop document_grid object_grid">
		<?php foreach($data_posts as $row) {
			?>
			<div data-object_type="document" class="object_cell document_cell">
				<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $row['alias'], "post_alias" => $row['post_alias'])); ?>">
					<div style="background-image: url('<?php echo StringUtils::getImageUrl($row['post_image'], "post"); ?>')" class="thumb">
						<?php if($row['post_end'] == 1) { ?>
							<div class="flag purchase_only">HOÀN THÀNH</div>
						<?php } ?>
						<?php if($row['post_is_full'] == 1) { ?>
							<div class="flag sample">ĐANG CẬP NHẬT</div>
						<?php } ?>
						<div class="overlay">
							<div class="sprite white_big_open_book"></div>
						</div>
					</div>
				</a>

				<div class="content">
					<div class="title_row">
						<a href="<?php echo $url->createUrl("post/detail", array("category_alias" => $row['alias'], "post_alias" => $row['post_alias'])); ?>" class="title">
							<?php echo $row['post_title']; ?>
						</a>
					</div>
					<div class="author_row">
						<a href="javascript:"><?php if($row['author_name'] == '') {
								echo "Sưu tầm";
							} else {
								echo $row['author_name'];
							} ?></a>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>

	<div class="footer_row">
		<div class="paginator">
			<ul class="paging">
				<?php
					echo $paging;
				?>
			</ul>
		</div>
	</div>
</div>

<?php include "detail.js.php"; ?>
