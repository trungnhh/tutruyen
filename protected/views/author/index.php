<?php 
    $url = new Url();     
?>         
<div class="column_left">
    <div class="container left">
        <h2 style="cursor:default;" class="container_head">
            <span style="margin-left:20px;">Tác giả</span>
        </h2>
        <div style="border:none;" class="panel">
            <table width="100%" cellspacing="0" class="lists">
                <tbody>
                    <?php for($i=0;$i<count($data_author);$i++){ ?>
                        <tr class="<?php if(($i%2)==0){echo 'oddrow';}else{echo 'evenrow';} ?>">
                            <td width="10%" valign="top" style="padding:10px">
                                <a title="icoldstyle" href="<?php echo $url->createUrl("author/detail",array('author_id'=>$data_author[$i]['author_id'])); ?>">
                                    <img width="64" border="0" align="top" height="64" class="picframe" alt="<?php echo $data_author[$i]['author_image']; ?>" src="<?php echo StringUtils::getImageUrl($data_author[$i]['author_image'],"author"); ?>"></a>
                                </td>
                            <td width="90%" valign="top" style="padding:10px 0;">
                                <a style="font-size: 16px;" href="<?php echo $url->createUrl("author/detail",array('author_id'=>$data_author[$i]['author_id'])); ?>"><?php echo $data_author[$i]['author_name']; ?></a>
                                <div style="display:inline; padding: 0 0 0 3px;"><img style="vertical-align:middle" title="Bình chọn" alt="votes" src="http://a.wattpad.net/image/22007/v2/votes.png"> </div><div style="display:inline; padding: 0 0 0 3px;"><img style="vertical-align:middle" title="Những người theo" alt="reads" src="http://a.wattpad.net/image/22007/v2/reads.png"> 0</div>
                                <div style="width:500px">
                                <?php
                                    $content = $data_author[$i]['author_description'];                                    
                                    $content = StringUtils::cutstring($content,200,true);                        
                                    $content = nl2br($content);     
                                    echo $content;
                                ?>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>                    
                </tbody>
            </table>
            <br>
            <div class="fillter clearfix">
                <ul class="paging_186">
                    <?php
                        echo $paging;
                    ?>
                </ul>
            </div>
        </div>                  
    </div>
</div>

<div class="column_right_bubble">    
    <?php $this->widget("Featured");?>
    <?php $this->widget("AdvRight");?>
    <div id="recommended_users" class="">        
        <?php $this->widget("CategoryRight");?>

        <?php //$this->widget("Author");?>    

        <?php $this->widget("FooterRight");?>    
    </div>
</div>

<?php include "index.js.php"; ?>        