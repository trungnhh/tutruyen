<?php
$url = new Url();
$controller = Yii::app()->controller->id;
$action = Yii::app()->controller->action->id;
$ac = $controller . '-' . $action;
$fb_app_id = Helpers::getConfigurationValue('facebook_app_id');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php require_once("_meta.php"); ?>
</head>
<body class="has_sticky_header has_header_shadow has_fullscreen_header">
<?php if($fb_app_id && $fb_app_id != ''): ?>
    <!-- Facebook JavaScript SDK -->
    <div id="fb-root"></div>
    <script type="text/javascript">
        window.fbAsyncInit = function() {
            FB.init({appId: '<?php echo $fb_app_id; ?>', status: true, cookie: true,
                xfbml: true});
        };
        (function() {
            var e = document.createElement('script'); e.async = true;
            e.src = document.location.protocol +
                '//connect.facebook.net/en_US/all.js';
            document.getElementById('fb-root').appendChild(e);
        }());
    </script>
    <!-- End Facebook JavaScript SDK -->
<?php else: ?>
    <div id="fb-root"></div>
    <script type="text/javascript">
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<?php endif; ?>
<?php $this->widget("Header"); ?>

<div class="global_wrapper">
<?php echo $content; ?>
</div>

<?php $this->widget("Footer"); ?>

</body>
</html>
