<?php 
    $url = new Url();
    $controller = Yii::app()->controller->id;
    $action = Yii::app()->controller->action->id;
    $ac = $controller.'-'.$action;
	if($ac != "user-login"){
		$_SESSION['tt_link'] = StringUtils::getCurrentPageURL();
	}
    $fb_app_id = Helpers::getConfigurationValue('facebook_app_id');
?>
<title><?=isset($this->title) ? $this->title : Helpers::getConfigurationValue('page_title'); ?></title>
<meta http-equiv="content-type" content="text/html;charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta http-equiv="Expires" content="Fri, Jan 01 2015 00:00:00 GMT">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Lang" content="en">
<meta name="author" content="<?php Helpers::getBrandDomain();?>">
<meta name="copyright" content="<?php Helpers::getBrandDomain();?>" />
<meta http-equiv="Reply-to" content="<?php Helpers::getconfigurationValue('info_email');?>">
<meta name="generator" content="<?php Helpers::getBrandDomain();?>">
<meta name="description" content="<?=isset($this->metaDescription) ? $this->metaDescription : Helpers::getconfigurationValue('meta_desc') ?>"/>
<meta name="keywords" content="<?=isset($this->metaKeywords) ? $this->metaKeywords : Helpers::getconfigurationValue('meta_keyword') ?>"/>
<meta name="creation-date" content="01/12/2012">
<meta name="revisit-after" content="1 days">

<meta property="og:title" content="<?=isset($this->title) ? $this->title :  Helpers::getConfigurationValue('page_title') ?>">
<meta property="og:description" content="<?=isset($this->metaDescription) ? $this->metaDescription : Helpers::getconfigurationValue('meta_desc') ?>">
<meta property="og:type" content="website" />
<meta property="og:image" content="<?php echo Helpers::getBaseUrl(); ?>images/tutruyen_cover.jpg">
<meta property="og:url" content="<?php echo Helpers::getCurrentUrl(); ?>">
<meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="200">
<meta property="og:image:height" content="200">
<?php if($fb_app_id && $fb_app_id != ''): ?>
    <meta property="fb:app_id" content="<?php echo $fb_app_id; ?>"/>
<?php endif; ?>

<link rel="canonical" href="<?php echo Helpers::getCurrentUrl(); ?>" />
<link href="<?php echo Yii::app()->params["urlRs"]; ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />

<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRs"] ?>/css/browse.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRs"] ?>/css/style.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRs"] ?>/css/library.css" media="all" />
<script src="<?=Yii::app()->params["urlRs"] ?>/js/chrome.js" type="text/javascript"></script>
<script src="<?=Yii::app()->params["urlRs"] ?>/js/browse.js" type="text/javascript"></script>
<script src="<?=Yii::app()->params["urlRs"] ?>/js/library.js" type="text/javascript"></script>
