<?php
	$url = new Url();
?>
<link href="<?php echo Yii::app()->params['urlRs']; ?>/css/notifications.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo Yii::app()->params['urlRs']; ?>/css/accounts.css" rel="stylesheet" type="text/css"/>

<div class="autogen_class_views_notifications_index autogen_class_views_account_themes_show">
	<div class="home_container">
		<div class="page_header">
			<h2 class="title">Yêu cầu truyện</h2>
			<?php $this->widget("UserMenu"); ?>
		</div>
		<div class="page_body">
			<div class="error_message" style="display: none"></div>
			<div class="saved_message" style="display: none"></div>
		</div>
		<div class="form">
			<div class="row">
				<div class="label">Tiêu đề truyện yêu cầu</div>
				<div class="field">
					<input name="request_name" id="request_name" type="text" value="" class="text_input">
				</div>
			</div>
			<div class="row">
				<div class="label">Thể loại</div>
				<div class="field">
					<select class="input-text" id="category_id" name="category_id">
						<option value="0">Không rõ</option>
						<?php foreach($data_category as $row){ ?>
							<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="label">Mô tả</div>
				<div class="field">
					<textarea name="request_content" id="request_content" class="text_input" style="height: 100px;"></textarea>
				</div>
			</div>
			<div class="row">
				<div class="label">Tác giả</div>
				<div class="field">
					<input name="request_author" id="request_author" type="text" value="" class="text_input">
				</div>
			</div>
			<div class="bottom_buttons">
				<div class="row">
					<div class="label"></div>
					<div class="field">
						<div class="flat_btn darkblue" onclick="send_request()">Gửi</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<script>

	function send_request() {
		var request_name = $('#request_name').val();
		var category_id = $('#category_id').val();
		var request_content = $('#request_content').val();
		var request_author = $('#request_author').val();
		if(request_name == ''){
			show_error('Bạn chưa nhập tiêu đề truyện yêu cầu!');
			return false;
		}
		$('.error_message').hide('slow');
		var strUrl = '<?=$url->createUrl("user/ajaxSubmitRequest") ?>';
		$.ajax({
			type: "POST",
			url: strUrl,
			data: {request_name:request_name,category_id:category_id,request_content:request_content,request_author:request_author},
			success: function(msg){
					$('.saved_message').slideDown('slow');
					$('.saved_message').html('Yêu cầu của bạn đã được gửi cho chúng tôi ;) Hãy quay lại Tủ Truyện thường xuyên trong thời gian tới để đọc truyện bạn vừa yêu cầu nhé.');
					$('html, body').animate({ scrollTop: 0 }, 'slow');
			}
		});
	}

	function show_error(text) {
		$('.error_message').slideDown('slow');
		$('.error_message').html(text);
		$('html, body').animate({ scrollTop: 0 }, 'slow');
	}

</script>