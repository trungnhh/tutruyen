<?php
	$url = new Url();
?>
<link href="<?php echo Yii::app()->params['urlRs']; ?>/css/notifications.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo Yii::app()->params['urlRs']; ?>/css/accounts.css" rel="stylesheet" type="text/css"/>

<div class="autogen_class_views_notifications_index autogen_class_views_account_themes_show">
	<div class="home_container">
		<div class="page_header">
			<h2 class="title">Góp ý</h2>
			<?php $this->widget("UserMenu"); ?>
		</div>
		<div class="page_body">
			<div class="error_message" style="display: none"></div>
			<div class="saved_message" style="display: none"></div>
		</div>
		<div class="form">
			<div class="row">
				<div class="label">Họ tên</div>
				<div class="field">
					<input name="fullname" id="fullname" type="text" value="<?php echo $_SESSION["user_tt"]['fullname']; ?>" class="text_input">
				</div>
			</div>
			<div class="row">
				<div class="label">Email</div>
				<div class="field">
					<input name="email" id="email" type="text" value="<?php echo $_SESSION["user_tt"]['email']; ?>" class="text_input">
				</div>
			</div>
			<div class="row">
				<div class="label">Điện thoại</div>
				<div class="field">
					<input name="mobile" id="mobile" type="text" value="<?php echo $_SESSION["user_tt"]['mobile']; ?>" class="text_input">
				</div>
			</div>
			<div class="row">
				<div class="label">Nội dung góp ý</div>
				<div class="field">
					<textarea name="content" id="content" class="text_input" style="height: 100px;"></textarea>
				</div>
			</div>
			<div class="bottom_buttons">
				<div class="row">
					<div class="label"></div>
					<div class="field">
						<div class="flat_btn darkblue" onclick="send_note()">Gửi</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<script>

	function send_note() {
		var fullname = $('#fullname').val();
		var email = $('#email').val();
		var mobile = $('#mobile').val();
		var content = $('#content').val();
		if(fullname == ''){
			show_error('Bạn chưa nhập họ tên!');
			return false;
		}
		if(mobile == ''){
			show_error('Bạn chưa nhập số điện thoại!');
			return false;
		}
		if(content == ''){
			show_error('Bạn chưa nhập nội dung góp ý!');
			return false;
		}
		$('.error_message').hide('slow');
		var strUrl = '<?=$url->createUrl("user/ajaxSubmitNote") ?>';
		$.ajax({
			type: "POST",
			url: strUrl,
			data: {content:content,mobile:mobile,fullname:fullname,email:email},
			success: function(msg){
					$('.saved_message').slideDown('slow');
					$('.saved_message').html('Góp ý của bạn đã được gửi cho chúng tôi :D Chúng tôi sẽ luôn cố gắng để bạn có được những trải nghiệm tốt nhất ở Tủ Truyện.');
					$('html, body').animate({ scrollTop: 0 }, 'slow');
			}
		});
	}

	function show_error(text) {
		$('.error_message').slideDown('slow');
		$('.error_message').html(text);
		$('html, body').animate({ scrollTop: 0 }, 'slow');
	}

</script>