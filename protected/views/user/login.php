<?php
	$url = new Url();
?>

	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRs"] ?>/css/login.css" media="all" />
	<script src="<?=Yii::app()->params["urlRs"] ?>/js/landing.js" type="text/javascript"></script>

	<div id="autogen_id_856067200" class="autogen_class_views_login_login login_forms" style="position: relative; z-index: 0; background: none repeat scroll 0% 0% transparent;">
		<div class="login_wrapper">
			<h1>Tham gia cộng đồng Tủ Truyện</h1>

			<div class="facebook_container">
				<div id="autogen_id_856064190" class="facebook_login_button">
					<span class="scribd_fb_button" onclick="fb_login()"><span class="fb_label"><div class="fb_logo"></div><span class="inner">Đăng nhập bằng Facebook</span></span></span>
					<p style="display: none;" class="fb_fail_message">Chúng tôi không thể kết nối tới tài khoản facebook của bạn. Xin hãy thử lại.</p>
				</div>
			</div>
			<div class="home_container"><h2>Bạn đã có tài khoản?</h2>

				<form method="post" data-type="json" data-remote="true" class="login_form newform" action="#">
					<label>
						<div class="label_text">Tên đăng nhập</div>
						<input type="text" size="20" id="username_field" name="login_or_email" class="input"></label><label>
						<div class="label_text">Mật khẩu</div>
						<input type="password" size="20" id="password_field" name="login_password" class="input"></label>
						<div id="login_msg_err" class="input_error"></div>
					<div class="button_container">
						<input type="button" value="Đăng nhập" data-act_as="submit" class="flat_btn" onclick="user_login()">
						<span class="button_links"><a href="javascript:" class="forgot_password_btn">Bạn không thể đăng nhập?</a></span>
					</div>
				</form>
			</div>
			<div class="home_container"><h2>Bạn chưa có tài khoản? Hãy đăng ký ngay một tài khoản miễn phí.</h2>

				<div class="signup_container">
					<form method="post" data-type="json" data-remote="true" class="signup_form newform">
						<label class="inline_block">
							<div class="label_text">Tên đăng nhập (bắt buộc)</div>
							<input type="text" id="username" name="word_user[login]" maxlength="20" class="input username_input"></label><label class="inline_block">
							<div class="label_text">Mật khẩu (Bắt buộc)</div>
							<input type="password" id="password" name="word_user[password]" class="input"></label><label class="inline_block">
							<div class="label_text">Email (bắt buộc)</div>
							<input type="email" id="email" name="email_address[email]" class="input email_input">
						</label>
						<label class="inline_block">
							<div class="label_text">Họ tên</div>
							<input type="text" id="fullname" name="word_user[login]" maxlength="20" class="input username_input"></label><label class="inline_block">
							<div class="label_text">Số điện thoại</div>
							<input type="text" id="mobile" name="mobile" class="input email_input">
						</label>
						<label class="signup_optin"><input type="checkbox" value="y" name="optin" class="optin_check" checked="checked">Đồng ý với các điều khoản của <?php echo Helpers::getBrandDomain(); ?></label>

						<div id="register_msg_err" class="input_error"></div>
						<div class="button_container">
							<input type="button" value="Đăng ký" class="flat_btn orange" onclick="user_register()">
							<!--<span class="button_links"><a onclick="window.open(this.href);return false;" class="privacy_policy" href="http://www.scribd.com/privacy">Privacy policy</a></span>-->
						</div>
						<!--<div class="signup_disclosure">
							Bạn sẽ nhận được email thông báo liên quan đến hoạt động tài khoản của bạn. Bạn có thể quản lý các thông báo trong cài đặt tài khoản của bạn. Chúng tôi hứa sẽ tôn trọng sự riêng tư của bạn.
						</div>-->
					</form>
				</div>
			</div>
			<div id="autogen_id_856065477" class="autogen_class_views_new_chrome_login_login_footer">
				<div class="login_footer login_landing">
					<div class="column left">
						<div class="line">
							<span class="one">THAM GIA VỚI</span><span class="two">80.000</span><br><span class="three">NGƯỜI ĐỌC MỖI THÁNG</span>
						</div>
					</div>
					<div class="column center">
						<div class="line">
							<span class="one">TỪ</span><span class="two">100+</span><br><span class="three">QUỐC GIA</span>
						</div>
					</div>
					<div class="column right">
						<div class="line">
							<span class="one">ĐỌC</span><span class="two">40.000</span><br><span class="three">TRUYỆN VÀ SÁCH</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="backstretch" style="left: 50%; top: 0px; overflow: hidden; margin: 0px; padding: 0px; height: 860px; width: 1280px; z-index: -999998; position: absolute;margin-left: -640px;">
			<img style="position: absolute; margin: 0px; padding: 0px; border: medium none; width: 2334.29px; height: 860px; max-height: none; max-width: none; z-index: -999999; left: -527.143px; top: 0px;" src="<?=Yii::app()->params["urlRs"] ?>/images/login.jpg">
		</div>
	</div>

	<script>

		function fb_login(){
			loadingAjax();
			FB.login(function(response) {
				if (response.authResponse) {
					console.log('Welcome!  Fetching your information.... ');
					FB.api('/me', function(response) {
						console.log(response);
						console.log('Good to see you, ' + response.name + '.');
						var name = response.name;
						var email = response.email;
						var fb_id = response.id;
						var fb_username = response.name;

						var strUrl = '<?=$url->createUrl("user/ajaxLoginFB") ?>';
						$.ajax({
							type: "POST",
							url: strUrl,
							data: {name:name,email:email,fb_id:fb_id,fb_username:fb_username},
							success: function(msg){
								window.location.href='<?=$url->createUrl("site/index") ?>';
							}
						});

					});
					closeLoadingAjax();
				} else {
					closeLoadingAjax();
					$('.fb_fail_message').show();
					console.log('User cancelled login or did not fully authorize.');
				}
			},{scope: 'email'});
		}

	</script>


<?php include "login.js.php"; ?>