<?php 
    $url = new Url();    
?>

<link href="<?php echo Yii::app()->params['urlRs']; ?>/css/content_list.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->params['urlRs']; ?>/css/recently_read.css" rel="stylesheet" type="text/css" />

<div id="autogen_id_498329412" class="autogen_class_views_recently_read_index autogen_class_views_shared_content_list_index autogen_class_views_shared_contained_page">
	<div class="home_container">
		<div id="autogen_id_498330439" class="autogen_class_views_recently_read_header content_list_header recently_read_header">
			<div class="relative_container"><h2 class="page_title">Truyện vừa đọc
					<span class="count"><span>(</span><span class="value">0</span><span>)</span></span></h2></div>
		</div>
		<div id="autogen_id_498331521" class="items_container_placeholder">
			<div class="items_container">
				<div class="items"></div>
				<div class="no_items active">
					<p>Bạn chưa đọc truyện nào!</p>
					<p>Khi bạn băt đầu đọc truyên, trang này sẽ lưu lại những truyện bạn đọc gần đây nhất.</p>
				</div>
			</div>
		</div>
		<div id="autogen_id_498332693" class="infinite_paginator paginator">
			<a class="navigation_link load_more_2" href="javascript:">Tải thêm</a>
		</div>
	</div>
</div>
