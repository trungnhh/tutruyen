<script type="">

	$(document).ready(function () {
		$('#password_field').keypress(function(event){

			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				user_login();
			}

		});
	});

    function user_login(){
        var username = $('#username_field').val();
        var password = $('#password_field').val();

        if ($('#username_field').val()==null || $('#username_field').val()=='') {
            errorMessage('login_msg_err', 'Tài khoản không được để trống.', '#username_field');
            return false;
        }                                                  
        else if ($('#password_field').val()==null || $('#password_field').val()==''){
            errorMessage('login_msg_err', 'Mật khẩu không được để trống.', '#password_field');
            return false;
        }
        var strUrl = '<?=$url->createUrl("user/ajaxSubmitLogin") ?>';
        loadingAjax();
        $.ajax({
            type: "POST",
            url: strUrl,
            data: {username:username,password:password},
            success: function(msg){                
                if(msg==0){
	                errorMessage('login_msg_err', 'Tài khoản hoặc mật khẩu sai!', '#username_field');
                }else{
                    window.location.href=msg;
                }                
            }
        });
        closeLoadingAjax();
    }

    function user_register(){
	    var username = $('#username').val();
	    var password = $('#password').val();
	    var email = $('#email').val();
	    var fullname = $('#fullname').val();
	    var mobile = $('#mobile').val();

	    if (username==null || username=='') {
		    errorMessage('register_msg_err', 'Tài khoản không được để trống.', '##username');
		    return false;
	    }
	    if (password==null || password==''){
		    errorMessage('register_msg_err', 'Mật khẩu không được để trống.', '#password');
		    return false;
	    }
	    if (email==null || email==''){
		    errorMessage('register_msg_err', 'Email không được để trống.', '#email');
		    return false;
	    }
	    var strUrl = '<?=$url->createUrl("user/ajaxSubmitRegister") ?>';
	    loadingAjax();
	    $.ajax({
		    type: "POST",
		    url: strUrl,
		    data: {username:username,password:password,email:email,fullname:fullname,mobile:mobile},
		    success: function(msg){
			    if(msg==0){
				    errorMessage('register_msg_err', 'Tên đăng nhập đã tồn tại! Bạn hãy chọn một tên đăng nhập khác.', '#username');
			    }else{
				    window.location.href='<?php echo $url->createUrl("site/index"); ?>';
			    }
		    }
	    });
	    closeLoadingAjax();
    }

    function errorMessage(type_err ,msg, focus) {
        $('#error_message').html(msg);
        if(focus) {
            var field = $(focus);
            field.focus();
            $("#"+type_err).html(msg);
        }
    }    
    
</script>