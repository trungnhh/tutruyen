<?php 
    $url = new Url();     
?>         

<div class="container big">

    <?php foreach($data_news as $row){ ?>
        <div style="padding:20px">
            <!--<h1 style="padding: 0;" class="styled_text"><span style="color:#ED602E;">Hướng dẫn</span> nạp xu và giải đáp thắc mắc</h1>-->
            <a href="<?php echo $url->createUrl("news/detail",array("news_id"=>$row['id'])); ?>"><h1 style="padding: 0;font-size: 25px;" class="styled_text"><?php echo $row['news_title']; ?></h1></a>
            <br>
            <div id="main-content-new" style="font-size: 16px;">
                <?php             
                    $content = $row['news_content'];                                         
                    $content = strip_tags($content);
                    $content = StringUtils::cut_string($content,500);                    
                    echo $content;
                ?><a href="<?php echo $url->createUrl("news/detail",array("news_id"=>$row['id'])); ?>">Xem thêm</a>
            </div>        
        </div>            
        <?php } ?>    

</div>

<?php include "index.js.php"; ?>        