<link rel="stylesheet" type="text/css" href="<?=Yii::app()->params["urlRs"] ?>/css/static.css" media="all" />
<div class="autogen_class_views_static_contact autogen_class_views_static_about_container">

	<div class="container">
		<h1><?php echo $data_new['news_title']; ?></h1>

		<div class="intro" style="color: #333333;font-size: 18px;line-height: 28px;margin-bottom: 18px;">
			<?php echo $data_new['news_content']; ?>
		</div>

	</div>
</div>