<?php
	$url = new Url();
?>
	<link href="<?php echo Yii::app()->params['urlRs']; ?>/css/account_settings.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->params['urlRs']; ?>/css/notifications.css" rel="stylesheet" type="text/css"/>

	<div class="autogen_class_views_notifications_index account_settings_page">
		<div class="home_container">
			<div class="page_header">
				<h2 class="title">Tin nhắn</h2>
				<?php $this->widget("UserMenu"); ?>
			</div>
			<div class="autogen_class_views_account_settings_tabs_personal autogen_class_views_account_settings_tabs_base">

				<div class="error_message" style="display: none"></div>
				<div class="saved_message" style="display: none"></div>
				<div id="" class="items_container_placeholder">
					<div class="items_container">
						<?php if($data_messages != null) { ?>
							<div class="items">
								<?php foreach($data_messages as $row) { ?>
									<div class="item collection">

										<div class="preview">
											<a href="javascript:">
												<img src="<?php echo StringUtils::getImageUrl('', "post"); ?>">
											</a>
										</div>
										<div class="details">
											<div class="item_header">
												<h3 class="title">
													<a href="javascript:"><?php echo $row['title']; ?></a>
												</h3>
												<div class="author"><?php echo $row['sender']; ?></div>
											</div>
											<div class="main_column"><?php echo $row['content']; ?></div>
											<div class="metadata">
												<ul class="metrics">
													<li><?php if($row['type'] == 1){ echo "Thông báo"; }else{ echo "Cá nhân"; } ?></li>
												</ul>
											</div>
											<div class="action_date"><?php echo date('h:i d/m/Y',$row['create_date']);  ?></div>
										</div>
									</div>
								<?php } ?>
								<div class="footer_row">
									<div class="paginator">
										<ul class="paging">
											<?php
												echo $paging;
											?>
										</ul>
									</div>
								</div>
							</div>
						<?php } else { ?>
							<div class="no_items">
								<div class="no_notifications">Hiện tại bạn vẫn chưa nhận được thông báo nào.</div>
							</div>
						<?php } ?>
					</div>
				</div>

			</div>
		</div>
	</div>

<?php include "messege.js.php"; ?>