<?php

    class StringUtils {        

        public static function isValidEmail($email) {
            return eregi ( "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email );
        }        

        public static function removeQuote($string) {
            $sting = trim ( $string );
            $sting = str_replace ( "\'", "'", $sting );
            $sting = str_replace ( "'", "''", $sting );
            return $string;
        }

        public static function randomkey($str, $keyword = 0) {
            $return = '';
            $strreturn = explode ( " ", trim ( $str ) );
            $i = 0;
            $listid = '';
            while ( $i < count ( $strreturn ) ) {
                $id = rand ( 0, count ( $strreturn ) );
                if (strpos ( $listid, '[' . $id . ']' ) === false) {
                    if (isset ( $strreturn [$id] )) {
                        $return .= $strreturn [$id] . ' ';
                        $i ++;
                        if ($keyword == 1 && ($i % 2) == 0 && $i < count ( $strreturn ))
                            $return .= ',';
                        $listid .= '[' . $id . ']';
                    }
                }
            }
            return $return;
        }

        public static function random() {
            $rand_value = "";
            $rand_value .= rand ( 1000, 9999 );
            $rand_value .= chr ( rand ( 65, 90 ) );
            $rand_value .= rand ( 1000, 9999 );
            $rand_value .= chr ( rand ( 97, 122 ) );
            $rand_value .= rand ( 1000, 9999 );
            $rand_value .= chr ( rand ( 97, 122 ) );
            $rand_value .= rand ( 1000, 9999 );
            return $rand_value;
        }

        public static function str_encode($encodeStr = "") {
            $returnStr = "";
            if ($encodeStr == '')
                return $encodeStr;
            if (! empty ( $encodeStr )) {
                $enc = base64_encode ( $encodeStr );
                $enc = str_replace ( '=', '', $enc );
                $enc = str_rot13 ( $enc );
                $returnStr = $enc;
            }
            return $returnStr;
        }

        public static function str_decode($encodedStr = "", $type = 0) {
            $returnStr = "";
            $encodedStr = str_replace ( " ", "+", $encodedStr );
            if (! empty ( $encodedStr )) {
                $dec = str_rot13 ( $encodedStr );
                $dec = base64_decode ( $dec );
                $returnStr = $dec;
            }
            return $returnStr;
        }

        //hàm get URL
        public static function getURL($serverName = 0, $scriptName = 0, $fileName = 1, $queryString = 1, $varDenied = '') {
            $url = '';
            $slash = '/';
            if ($scriptName != 0)
                $slash = "";
            if ($serverName != 0) {
                if (isset ( $_SERVER ['SERVER_NAME'] )) {
                    $url .= 'http://' . $_SERVER ['SERVER_NAME'];
                    if (isset ( $_SERVER ['SERVER_PORT'] ))
                        $url .= ":" . $_SERVER ['SERVER_PORT'];
                    $url .= $slash;
                }
            }
            if ($scriptName != 0) {
                if (isset ( $_SERVER ['SCRIPT_NAME'] ))
                    $url .= substr ( $_SERVER ['SCRIPT_NAME'], 0, strrpos ( $_SERVER ['SCRIPT_NAME'], '/' ) + 1 );
            }
            if ($fileName != 0) {
                if (isset ( $_SERVER ['SCRIPT_NAME'] ))
                    $url .= substr ( $_SERVER ['SCRIPT_NAME'], strrpos ( $_SERVER ['SCRIPT_NAME'], '/' ) + 1 );
            }
            if ($queryString != 0) {
                $dau = 0;
                $url .= '?';
                reset ( $_GET );
                if ($varDenied != '') {
                    $arrVarDenied = explode ( '|', $varDenied );
                    while ( list ( $k, $v ) = each ( $_GET ) ) {
                        if (array_search ( $k, $arrVarDenied ) === false) {
                            $url .= (($dau == 0) ? '' : '&') . $k . '=' . urlencode ( $v );
                            $dau = 1;
                        }
                    }
                } else {
                    while ( list ( $k, $v ) = each ( $_GET ) )
                        $url .= '&' . $k . '=' . urlencode ( $v );
                }
            }
            $url = str_replace ( '"', '&quot;', strval ( $url ) );
            return $url;
        }

        public static function getCurrentPageURL() {
            $pageURL = 'http';
            if (!empty($_SERVER['HTTPS'])) {if($_SERVER['HTTPS'] == 'on'){$pageURL .= "s";}} 
            $pageURL .= "://";
            if ($_SERVER["SERVER_PORT"] != "80") {
                $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
            } else {
                $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
            }
            return $pageURL;
        }        

        //loại bỏ hoạt động của các thẻ html, vô hiệu hóa
        public static function htmlspecialbo($str) {
            $arrDenied = array ('<', '>', '"' );
            $arrReplace = array ('&lt;', '&gt;', '&quot;' );
            $str = str_replace ( $arrDenied, $arrReplace, $str );
            return $str;
        }

        // loại bỏ các thẻ html, javascript
        public static function removeHTML($string) {
            $string = mb_convert_encoding ( $string, "UTF-8", "UTF-8" );
            $string = preg_replace ( '/<script.*?\>.*?<\/script>/si', ' ', $string );
            $string = preg_replace ( '/<style.*?\>.*?<\/style>/si', ' ', $string );
            $string = preg_replace ( '/<.*?\>/si', ' ', $string );
            $string = str_replace ( '&nbsp;', ' ', $string );
            //$string = html_entity_decode ($string);
            return $string;
        }

        // hàm redirect : 1 url
        public static function redirect($url, $http = 0) {
            $url = str_replace ( "'", "\'", $url );
            echo '<script type="text/javascript">';
            echo 'window.location.href="' . $url . '";';
            echo '</script>';
            echo '<noscript>';
            echo '<meta http-equiv="refresh" content="0;url=' . $url . '" />';
            echo '</noscript>';
            exit ();
            exit ();
        }

        //hàm cắt chuổi
        public static function cut_string($str, $length) {
            if (mb_strlen ( $str, "UTF-8" ) > $length)
                return mb_substr ( $str, 0, $length, "UTF-8" ) . '...';
            else
                return $str;
        }

        public static function cutstring($str, $len, $more) {
            if ($str == "" || $str == NULL)
                return $str;
            if (strlen ( $str ) <= $len)
                return $str;
            $str = preg_replace("/<img[^>]+\>/i", " ", $str);
            $str = strip_tags ( $str,"<br>" );
            $str = preg_replace("/<br>/i", " ", $str);            
            $str = trim ( $str );
            $str = substr ( $str, 0, $len );
            if ($str != "") {
                if (! substr_count ( $str, " " )) {
                    if ($more)
                        $str .= " ...";
                    return $str;
                }
                while ( strlen ( $str ) && ($str [strlen ( $str ) - 1] != " ") ) {
                    $str = substr ( $str, 0, - 1 );
                }
                $str = substr ( $str, 0, - 1 );
                if ($more)
                    $str .= " ...";    
            }
            return $str;
        }

        public static function length($str) {
            return mb_strlen ( $str, "UTF-8" );
        }
        //
        public static function replaceMQ($text) {
            $text = str_replace ( "\'", "'", $text );
            $text = str_replace ( "'", "''", $text );
            return $text;
        }

        public static function RemoveSign($str) {
            $coDau = array ("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ", "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ", "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ", "ỳ", "ý", "ỵ", "ỷ", "ỹ", "đ", "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ", "Ì", "Í", "Ị", "Ỉ", "Ĩ", "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ", "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ", "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ", "Đ", "ê", "ù", "à", "ỏ", "á", "é", "ã", "ó", "ò"  );
            $khongDau = array ("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "i", "i", "i", "i", "i", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "y", "y", "y", "y", "y", "d", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "I", "I", "I", "I", "I", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "Y", "Y", "Y", "Y", "Y", "D", "e", "u", "a", "o", "a", "e", "a", "o","o" );
            return str_replace ( $coDau, $khongDau, $str );
        }

        public static function removeKey($string) {
            $string = trim ( preg_replace ( "/[^A-Za-z0-9àáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸ]/i", " ", $string ) );
            $string = str_replace ( " ", "-", $string );
            $string = str_replace ( "--", "-", $string );
            $string = str_replace ( "--", "-", $string );
            $string = mb_convert_encoding ( $string, "UTF-8", "UTF-8" );
            return $string;
        }

        public static function removeTitle($string, $keyReplace) {                        
            $string = StringUtils::RemoveSign ( $string );                                    
            //neu muon de co dau

            $string = trim ( preg_replace ( "/[^A-Za-z0-9]/i", " ", $string ) ); // khong dau                        
            $string = str_replace ( " ", "-", $string );
            $string = str_replace ( "--", "-", $string );
            $string = str_replace ( "--", "-", $string );
            $string = str_replace ( "--", "-", $string );
            $string = str_replace ( "--", "-", $string );
            $string = str_replace ( "--", "-", $string );
            $string = str_replace ( "--", "-", $string );
            $string = str_replace ( "--", "-", $string );
            $string = str_replace ( $keyReplace, "-", $string );
            return $string;
        }

        public static function getKeyRef($query, $keyname = "q") {
            $strreturn = '';
            preg_match ( "#" . $keyname . "=(.*)#si", $query, $match );
            if (isset ( $match [1] ))
                $strreturn = preg_replace ( '#\&(.*)#si', "", $match [1] );
            return urldecode ( $strreturn );
        }
        //ham ma hoa
        public static function fSencode($encodeStr = "") {
            $returnStr = "";
            if (! empty ( $encodeStr )) {
                $enc = base64_encode ( $encodeStr );
                $enc = str_replace ( '=', '', $enc );
                $enc = str_rot13 ( $enc );
                $returnStr = $enc;
            }

            return $returnStr;
        }

        //ham giai mai
        public static function fSdecode($encodedStr = "", $type = 0) {
            $returnStr = "";
            $encodedStr = str_replace ( " ", "+", $encodedStr );
            if (! empty ( $encodedStr )) {
                $dec = str_rot13 ( $encodedStr );
                $dec = base64_decode ( $dec );
                $returnStr = $dec;
            }
            switch ($type) {
                case 0 :
                    $returnStr = str_replace ( "\'", "'", $returnStr );
                    $returnStr = str_replace ( "'", "''", $returnStr );
                    return $returnStr;
                    break;
                case 1 :
                    return intval ( $returnStr );
                    break;
                case 3 :
                    return doubleval ( $returnStr );
                    break;
            }
        }

        public static function word_limiter($text, $limit = 30, $chars = '') {

            $text = StringUtils::removeHTML($text);
            $text = stripslashes($text);

            if (strlen ( $text ) > $limit) {
                $text = mb_substr($text,0,$limit,'utf8');
                $text = $text.'&hellip;';
                /*$words = str_word_count ( $text, 2, $chars );
                echo $words[1];die;
                $words = array_reverse ( $words, TRUE );
                foreach ( $words as $length => $word ) {
                if ($length + strlen ( $word ) >= $limit) {
                array_shift ( $words );
                } else {
                break;
                }
                }
                $words = array_reverse ( $words );
                $text = implode ( " ", $words ) . '&hellip;';*/
            }
            return $text;
        }

        public static function htmlNormalize($text){
            //$text = '<strong><pre><script>aaaaaaaaaaaaaaaaaa</script></pre></strong>';
            //$text = StringUtils::removeHTML($text);
            //$text = stripslashes($text);

            //preg_match_all("/<script>(.*)<\/script>/u",$text,$output);
            //$text = str_replace("\n","<br />",$text);
            $text = str_replace('<script','<pre>&lt;script',$text);
            $text = str_replace('</script>','&lt;/script&gt;</pre>',$text);

            return $text;
        }

        public static function getDayVi($day){
            $day = intval($day);
            $day_vi = "";
            switch($day){
                case 0:
                    $day_vi = 8;
                    break;
                case 1:
                    $day_vi = 2;
                    break;
                case 2:
                    $day_vi = 3;
                    break;
                case 3:
                    $day_vi = 4;
                    break;
                case 4:
                    $day_vi = 5;
                    break;
                case 5:
                    $day_vi = 6;
                    break;
                case 6:
                    $day_vi = 7;
                    break;           
            }
            return $day_vi;
        }

        public static function getDayViPrint($day){
            $day = intval($day);
            $day_vi = "";
            switch($day){
                case 0:
                    $day_vi = "CN";
                    break;
                case 1:
                    $day_vi = "T.2";
                    break;
                case 2:
                    $day_vi = "T.3";
                    break;
                case 3:
                    $day_vi = "T.4";
                    break;
                case 4:
                    $day_vi = "T.5";
                    break;
                case 5:
                    $day_vi = "T.6";
                    break;
                case 6:
                    $day_vi = "T.7";
                    break;           
            }
            return $day_vi;
        }

        public static function getDayViText($day){ 
            $day = intval($day);
            $day_vi = "";
            switch($day){
                case 0:
                    $day_vi = "Chủ nhật";
                    break;
                case 1:
                    $day_vi = "Thứ hai";
                    break;
                case 2:
                    $day_vi = "Thứ ba";
                    break;
                case 3:
                    $day_vi = "Thứ tư";
                    break;
                case 4:
                    $day_vi = "Thứ năm";
                    break;
                case 5:
                    $day_vi = "Thứ sáu";
                    break;
                case 6:
                    $day_vi = "Thứ bảy";
                    break;           
            }
            return $day_vi;
        }

        public static function getDayByDate($date){ 
            $arrDate = explode("/",$date);

            $day = "";
            if(count($arrDate) >1){  
                $time = mktime(0,0,0,$arrDate[1],$arrDate[0],$arrDate[2]);         
                $date = getdate($time);
                $day = StringUtils::getDayViText($date["wday"]);
            }
            return $day;
        }                

        public static function getIntroText($str,$len){
            $position = 0;
            $new_str = "";
            $total_len = strlen($str);
            $end = $total_len - $position;
            $i=0;  
            $test = array();
            if($total_len > $len){ 
                while($end > 0){
                    $arr = StringUtils::getSubString($str,$position,$len);          
                    $position = $arr['position'];           
                    $new_str = $new_str.$arr['str'];
                    $end = $total_len - $position;
                    $i++;
                } 

            }else{
                $new_str = $str; 
            }
            return $new_str; 
        }

        public static function getSubString($str,$position,$len){ 
            $arr = array();

            $sub_str = substr($str,$position,$len);
            /*Kiểm tra chuỗi con có khoảng trắng không */
            $check = strpos($sub_str," ",1);        
            if($check > 0){            
                $sub_str = substr($str,$position,$check);
                $arr['str'] = $sub_str."<br/>";
                $arr['position'] = $position+$check+1;           
            }else{
                $arr['str'] = $sub_str."<br/>";
                $arr['position'] = $position + $len;    
            }
            return $arr;

        }

        public static function formatNumber($value){
            if($value < 1000)
            {
                $str = $value;
            }
            else
            {
                $str = number_format($value,0,"",".");
            }
            return $str;
        }

        public static function checkStupidPhone(){
            $useragent=$_SERVER['HTTP_USER_AGENT'];            
            $stupid_phone = 1;
            if (stripos(strtolower($useragent), 'mozilla') !== false) {
                $stupid_phone = 0;
            }

            if (stripos(strtolower($useragent), 'gecko') !== false) {
                $stupid_phone = 0;
            }            
            return $stupid_phone;
        }

        public static function checkSuperAdmin(){
            $admin_id = Yii::app()->user->id;
            $data_admin = Admin::getRowsById($admin_id);
            $super_admin = 0;
            if($data_admin['is_admin'] == 1){
                $super_admin = 1;
            }
            return $super_admin;
        }

        public static function getImageUrl($image_url,$type){
            $default_post_image = "tutruyen_cover.jpg";
            $default_author_image = "mystery-man.jpg";
            $default_user_image = "mystery-man.jpg";
            $url = "";
            switch($type){
                case "post":
                    if($image_url != ''){
                        if (stripos($image_url, "http:") === false) {
                            $url = Yii::app()->params['urlImage'] . "/post/" . $image_url;
                        }else{
                            $url = $image_url; 
                        }
                    }else{
                        $url = Yii::app()->params['urlRs'] . "/images/" . $default_post_image;   
                    }
                    break;
                case "user":
                    if($image_url != ''){
                        if (stripos($image_url, "http:") === false) {
                            $url = Yii::app()->params['urlImage'] . "/user/" . $image_url;
                        }else{
                            $url = $image_url; 
                        }
                    }else{
                        $url = Yii::app()->params['urlRs'] . "/images/" . $default_user_image;   
                    }
                    break;
                case "author":
                    if($image_url != ''){
                        if (stripos($image_url, "http:") === false) {
                            $url = Yii::app()->params['urlImage'] . "/author/" . $image_url;
                        }else{
                            $url = Yii::app()->params['urlRs'] . "/images/" . $default_author_image;   
                        }
                    }else{
                        $url = Yii::app()->params['urlRs'] . "/images/" . $default_author_image;   
                    }
                    break;
            }
            return $url;
        }

        public static function detechDevice(){
            if(preg_match('/(alcatel|amoi|android|avantgo|blackberry|benq|cell|cricket|docomo|elaine|htc|iemobile|iphone|ipad|ipaq|ipod|j2me|java|midp|mini|mmp|mobi|motorola|nec-|nokia|palm|panasonic|philips|phone|playbook|sagem|sharp|sie-|silk|smartphone|sony|symbian|t-mobile|telus|up\.browser|up\.link|vodafone|wap|webos|wireless|xda|xoom|zte)/i', $_SERVER['HTTP_USER_AGENT'])){
	            Yii::app()->session['themes'] = "smartphone";
	            //if(preg_match('/(iphone|ipad)/i', $_SERVER['HTTP_USER_AGENT'])){
                if(preg_match('/(ipad)/i', $_SERVER['HTTP_USER_AGENT'])){
		            Yii::app()->session['themes'] = "smartphone";
	            }
                /*$path = $_SERVER['REQUEST_URI'];
                header('Location: http://m.thichtruyen.vn'.$path);*/
            }
            else{
                //header('Location: http://demo.thichtruyen.vn');
            }
        }

    }

?>    