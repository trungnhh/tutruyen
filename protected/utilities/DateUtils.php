<?php
class DateUtils {

    public static function convertDateToString($datetime) {
        if($datetime == "" || $datetime == null){
            return "khong co thoi gian";
        }
        $arrDatetime = explode ( " ", $datetime );
        $arrDate = explode ( "/", $arrDatetime [0] );
        $arrTime = explode ( ":", $arrDatetime [1] );
        //echo $datetime.'<br>';
        if ($arrDate[2] >= 2010) {
            //if ($arrDate[2]>2010) $arrDate[2]=2010;   
            $time = mktime ( $arrTime [0], $arrTime [1], 0, $arrDate [1], $arrDate [0], $arrDate [2] );
            if ($time <= 1280595600) {
                $arrDate[1] = 8;
                $arrDate[0] = 22;
                $arrDate[2] = 2010;
                $time = mktime ( $arrTime [0], $arrTime [1], 0, $arrDate[1],$arrDate[0], $arrDate[2] );
            }

            $number_time = "vài giây trước";
            if (time () - $time > 0) {
                $date = floor ( (time() - $time) / 86400 );
                $hour = floor ( (time() - $time) / 3600 );
                $minutes = floor ( (time() - $time) / 60 );
                if ($minutes < 60 && $minutes >= 1) {
                    $number_time = $minutes . ' phút trước';
                } elseif ($hour < 24 && $hour >= 1) {
                    $number_time = $hour . ' giờ trước';
                } elseif ($date < 30 && $date >= 1) {
                    $number_time = $date . ' ngày trước';
                } elseif ($date >= 30){
                    $number_time = $arrDate [0] . '.' . $arrDate [1] . '.' . $arrDate [2];
                }
            }
        } else {
            $number_time = '22.8.2010';
        }

        return $number_time;
    }

    public static function convertIntToDate($date){
        $date_format ="";
        if($date != 0){
            $default = mktime(0,0,0,8,15,2010);
            if($date> $default){
                $date_format = date("d/m/Y",$date);
            }else{
                $date_format = "22/8/2010";
            }
        }else{
            $date_format = "20/8/2010";
        }
        return $date_format;
    }

    public static function convertToNumberTime($i){
        $time = $i*8;
        $number_time = '1 phút trước';
        if($time<60){
            $number_time = $time .' phút trước';
        }elseif ($time >= 60 && $time < 1440){
            $number_time = floor($time/60).' giờ trước';
        }else{
            $number_time = 'vài ngày trước';
        }
        return $number_time;
    }

    public static function convertToThu($time=""){
        $time = $time == ""?time():$time;
        $date = gmdate("l",$time + 7*3600);
        $str_in = array ( "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");            
        $str_out = array ( "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy", "Chủ nhật");
        $thu  = str_replace( $str_in, $str_out, $date);
        return $thu;
    }

    public static function convertThuToNumber($time=""){
        $time = $time == ""?time():$time;
        $date = gmdate("l",$time + 7*3600);     
        $str_in = array ( "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");            
        $str_out = array ( "2", "3", "4", "5", "6", "7", "8");
        $thu  = str_replace( $str_in, $str_out, $date);
        return $thu;
    }

    public static function subtractionWeek($w){
        $date = date('Y-m-j');
        $new_date = strtotime ( '-'.$w.' week' , strtotime ($date) ) ;
        $new_date = date ( 'Y-m-j' ,$new_date );        
        return $new_date;
    }

    public static function convertDatedb($date){
        if($date == '' || $date == null)
        {
            return null;
        }
        $dtmp = explode('/',$date);
        if(count($dtmp)> 1){
            $dadate = mktime(0,0,0,$dtmp[1],$dtmp[0],$dtmp[2]);
            return  date('Y-m-d',$dadate);    
        }else{
            return $date;
        }

    }

    public static function convertViDate($date){
        if($date == '' || $date == null)
        {
            return null;
        }
        $dtmp = explode('/',$date);
        $vidate = mktime(0,0,0,$dtmp[1],$dtmp[0],$dtmp[2]);
        return  date('d-m-Y',$vidate);                    
    }

    public static function convertViDatetoDatedb($vidate){
        if($vidate == '' || $vidate == null)
        {
            return null;
        }
        $dtmp = explode('-',$vidate); 
        $vidatedb = mktime(0,0,0,$dtmp[1],$dtmp[0],$dtmp[2]);
        return  date('Y-m-d',$vidatedb);                    
    }

    public static function calculatorDate($date,$day,$stt=""){
        if($stt!=""){
            $newDate =  date('j/n/Y',(strtotime($date)+($day*86400)));  
        }else{
            $newDate =  date('j/n/Y',(strtotime($date)-($day*86400)));

        } 

        return $newDate;            
    }

    public static function convertDateweb($date,$type=""){   
        if($date == '' || $date == null)
        {
            return null;
        }        
        if($type == '-'){
            $dtmp = explode('-',$date);
            return  date("j-n-Y", mktime(0, 0, 0, $dtmp[1],$dtmp[2], $dtmp[0]));    
        }else{
            $dtmp = explode('-',$date);
            return  date("j/n/Y", mktime(0, 0, 0, $dtmp[1],$dtmp[2], $dtmp[0]));    
        }              
    }

    public static function convertDateDbToWeb($date){
        if($date == '' || $date == null)
        {
            return null;
        }        
        $dtmp = explode('-',$date);
        return  date("j/n/Y", mktime(0, 0, 0, $dtmp[1],$dtmp[2], $dtmp[0]));    

    }

    public static function calculateDaysFromDate($date){
        $days = round((strtotime(date("Y-m-d")) - strtotime($date)) / (60 * 60 * 24));
        return $days;        
    }

    public static function subDates($date1,$date2){
        $days = round((strtotime($date1) - strtotime($date2)) / (60 * 60 * 24));
        return $days;        
    }

    //$date đầu vào phải theo định dạng : Y-m-d
    public static function DateSubDate($date1,$date2){
        $test = strtotime($date1);
        $test2 = strtotime($date2);
        $test3 = $test - $test2;
        $_days=24*60*60;
        $days = $test3-($test3%$_days);
        $days=$days/$_days;
        return $days;
    }

    public static function checkTime($arrProvince){
        $rowProvince = Array();
        $timeMB = 19*60+10;
        $timeMT = 17*60+10;
        $timeMN = 16*60+10;

        $time_h = date('H',time()); 
        $time_i = date('i',time());  
        $time_now = $time_h*60+$time_i; 
        if(count($arrProvince)>0){
            for($i=0;$i<count($arrProvince);$i++){

                if($arrProvince[$i]['region']==1){
                    if($time_now < $timeMB){
                        $rowProvince[$i]['id'] = $arrProvince[$i]['id'];
                        $rowProvince[$i]['name'] = $arrProvince[$i]['name'];       
                    }                      
                } else if($arrProvince[$i]['region']==2){
                        if($time_now < $timeMT){
                            $rowProvince[$i]['id'] = $arrProvince[$i]['id'];
                            $rowProvince[$i]['name'] = $arrProvince[$i]['name'];       
                        }  
                } else if($arrProvince[$i]['region']==3){
                        if($time_now < $timeMN){
                            $rowProvince[$i]['id'] = $arrProvince[$i]['id'];
                            $rowProvince[$i]['name'] = $arrProvince[$i]['name'];       
                        }  
                }
            }
            return $rowProvince;
        }
    }

    public static function checkTimeClose($region){
        $timeMB = 19*60+46;
        $timeMT = 17*60+46;
        $timeMN = 16*60+46;

        $time_h = date('H',time()); 
        $time_i = date('i',time());  
        $time_now = $time_h*60+$time_i;
        switch($region){
            case 1: 
                if($time_now < $timeMB){
                    return false;
                }else{
                    return true;
                }
                break;
            case 2: 
                if($time_now < $timeMT){
                    return false;
                }else{
                    return true;
                }
                break;
            case 3: 
                if($time_now < $timeMN){
                    return false;
                }else{
                    return true;
                }
                break;            
        }       
    }

    public static function convertDateToInt($date,$char = '/'){
        //echo $date;die;
        $arr_date = explode($char,$date);
        if(count($arr_date) >2){

            $time = mktime(0,0,0,$arr_date[1],$arr_date[0],$arr_date[2]);
        }else{                                      
            $time = mktime(0,0,0,date("m",time()),date("d",time()),date("Y",time())); 
        }
        return $time;
    }

    public static function getQuarterByMonth($month){
        $quarter = 0;                        
        if($month == '01' || $month == '02' || $month == '03'){
            $quarter = 1;    
        }
        if($month == '04' || $month == '05' || $month == '06'){
            $quarter = 2;    
        }
        if($month == '07' || $month == '08' || $month == '09'){
            $quarter = 3;   
        }
        if($month == '10' || $month == '11' || $month == '12'){
            $quarter = 4;    
        }        
        return $quarter;        
    } 

    public static function week_number_month($date = 'today')
    {
        $ct = 1;
        $currentDay = date(j, strtotime($date));
        $dow = array("Mon" => 1,"Tue" => 2,"Wed" => 3,"Thu" => 4,"Fri" => 5,"Sat" => 6,"Sun" => 7);
        $FirstDayOfMonth = $dow[date(D, mktime(0,0,0,date(n),1,date(Y)))];
        $DaysInFirstWeek = (7-$FirstDayOfMonth)+1;
        if($currentDay <= $DaysInFirstWeek){return $ct;}
        $DaysRemaining = $currentDay-$DaysInFirstWeek;
        while($DaysRemaining > 0){$DaysRemaining -= 7;if($DaysRemaining > 0){$ct++;}}
        return $ct+=1;
    }

}