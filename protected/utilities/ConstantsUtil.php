<?php

class ConstantsUtil
{

    const ROW_PER_PAGE= 20;

    const TIME_CACHE_300  = 300;
    const TIME_CACHE_900  = 900;
    const TIME_CACHE_1800 = 1800;
    const TIME_CACHE_3600  = 3600;
    const TIME_CACHE_10800  = 10800;
    const TIME_CACHE_86400 = 86400;
    
    public static $arr_module = array(
        1 => array("label"=>"Quản trị nhập kết quả trong ngày","name"=>"trongngay-all"),
        2 => array("label"=>"Quản trị kết quả","name"=>"ketqua-all"),
        3 => array("label"=>"Quản trị thơ đề","name"=>"thode-all"),
        4 => array("label"=>"Quản trị lodo game","name"=>"lode-all"),
        5 => array("label"=>"Quản trị khách hàng","name"=>"user-all"),
        6 => array("label"=>"Quản trị admin","name"=>"admin-all"),
        7 => array("label"=>"Quản trị dự đoán","name"=>"dudoan-all"),
        8 => array("label"=>"Quản trị messenger","name"=>"messenger-all"),
        9 => array("label"=>"Quản trị đổi thưởng","name"=>"doithuong-all"),
        10 => array("label"=>"Quản trị services","name"=>"services-all"),
    );                   
               
}
?>
