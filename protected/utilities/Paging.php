<?php

class Paging {
   
    public static function showPageNavigation($currentPage, $maxPage, $path = '', $more) {

    	$url = new Url();
        $nav = array(
            // bao nhiêu trang bên trái currentPage
            'left' => 2,
            // bao nhiêu trang bên phải currentPage
            'right' => 2,
        );

        // nếu maxPage < currentPage thì cho currentPage = maxPage
        if ($maxPage < $currentPage) {
            $currentPage = $maxPage;
        }

        // số trang hiển thị
        $max = $nav['left'] + $nav['right'];

        // phân tích cách hiển thị
        if ($max >= $maxPage) {
            $start = 1;
            $end = $maxPage;
        } elseif ($currentPage - $nav['left'] <= 0) {
            $start = 1;
            $end = $max + 1;
        } elseif (($right = $maxPage - ($currentPage + $nav['right'])) <= 0) {
            $start = $maxPage - $max;
            $end = $maxPage;
        } else {
            $start = $currentPage - $nav['left'];
            if ($start == 2) {
                $start = 1;
            }

            $end = $start + $max;
            if ($end == $maxPage - 1) {
                ++$end;
            }
        }

        $navig = '';
        if ($currentPage >= 2) {
            // thêm nút "Trước"
            $navig .= '<li><a href="' . $path . 'page=' . ceil($currentPage - 1) . $more . '">Trước</a></li>';
            if ($currentPage >= $nav['left']) {
                if ($currentPage - $nav['left'] > 2 && $max < $maxPage) {
                    // thêm nút "1"
                    $navig .= '<li><a href="' . $path . 'page=1' . $more . '">1</a></li>';
                    $navig .= '<li><a href="javascript:">...</a></li>';
                }
            }
        }

        for ($i = $start; $i <= $end; $i++) {
            // trang hiện tại
            if ($i == $currentPage) {
                $navig .= '<li><a href="' . $path . 'page=' . $i . $more . '" class="active">' . $i . '</a></li>';
            }
            // trang khác
            else {
                //     $pg_link = $path.'page='.$i;
                $navig .= '<li><a href="' . $path . 'page=' . $i . $more . '">' . $i . '</a></li>';
            }
        }

        if ($currentPage <= $maxPage - 1) {
            if ($currentPage + $nav['right'] < $maxPage - 1 && $max + 1 < $maxPage) {
                // trang cuoi
                $navig .= '<li><a href="javascript:">...</a></li>';
                $navig .= '<li><a href="' . $path . 'page=' . $maxPage . $more . '">' . $maxPage . '</a></li>';
            }
            $navig .= '<li><a href="' . $path . 'page=' . ($currentPage + 1) . $more . '">Sau</a></li>';
        }

        // hiển thị kết quả
        return $navig;
    }

    public static function showPageNavigationAjax($currentPage, $maxPage, $function_js, $path, $divId) {
        $url = new Url();
        $nav = array(
            // bao nhiêu trang bên trái currentPage
            'left' => 2,
            // bao nhiêu trang bên phải currentPage
            'right' => 2,
        );

        // nếu maxPage < currentPage thì cho currentPage = maxPage
        if ($maxPage < $currentPage) {
            $currentPage = $maxPage;
        }

        // số trang hiển thị
        $max = $nav['left'] + $nav['right'];

        // phân tích cách hiển thị
        if ($max >= $maxPage) {
            $start = 1;
            $end = $maxPage;
        } elseif ($currentPage - $nav['left'] <= 0) {
            $start = 1;
            $end = $max + 1;
        } elseif (($right = $maxPage - ($currentPage + $nav['right'])) <= 0) {
            $start = $maxPage - $max;
            $end = $maxPage;
        } else {
            $start = $currentPage - $nav['left'];
            if ($start == 2) {
                $start = 1;
            }

            $end = $start + $max;
            if ($end == $maxPage - 1) {
                ++$end;
            }
        }

        $navig = '';
        if ($currentPage >= 2) {
            // thêm nút "Trước"
            
            $navig .= '<li><a href="javascript:void(0);" style="cursor: pointer;" onclick="return '.$function_js.'(\''.$path.'\','.ceil($currentPage - 1).',\''.$divId.'\');">Trước</a></li>';
            if ($currentPage >= $nav['left']) {
                if ($currentPage - $nav['left'] > 2 && $max < $maxPage) {
                    // thêm nút "1"
                    
                   
                	$navig .= '<li><a href="javascript:void(0);" style="cursor: pointer;" onclick="return '.$function_js.'(\''.$path.'\',1,\''.$divId.'\');">1</a></li>';
                    $navig .= '<li>...</li>';
                }
            }
        }

        for ($i = $start; $i <= $end; $i++) {
            // trang hiện tại
            if ($i == $currentPage) {
                
                
            	$navig .= '<li><a class="active" style="cursor: pointer;" onclick="return '.$function_js.'(\''.$path.'\','.$i.',\''.$divId.'\');">'.$i.'</a></li>';
            }
            // trang khác
            else {
                //     $pg_link = $path.'page='.$i;
                
            	$navig .= '<li><a href="javascript:void(0);" style="cursor: pointer;" onclick="return '.$function_js.'(\''.$path.'\','.$i.',\''.$divId.'\');">'.$i.'</a></li>';
            }
        }

        if ($currentPage <= $maxPage - 1) {
            if ($currentPage + $nav['right'] < $maxPage - 1 && $max + 1 < $maxPage) {
                // trang cuoi
                $navig .= '<li>...</li>';
                
                $navig .= '<li><a href="javascript:void(0);" style="cursor: pointer;" onclick="return '.$function_js.'(\''.$path.'\','.$maxPage.',\''.$divId.'\');">'.$maxPage.'</a></li>';
            }
            
            $navig .= '<li><a href="javascript:void(0);" style="cursor:pointer;" onclick="return '.$function_js.'(\''.$path.'\','.($currentPage + 1).',\''.$divId.'\');">Tiếp theo</a></li>';
        }

        // hiển thị kết quả
        return $navig;
    }
    
	public static function showPageNavigationAjaxByCat($currentPage, $maxPage,$path,$divId,$iCat) {
        $url = new Url();
        $nav = array(
            // bao nhiêu trang bên trái currentPage
            'left' => 2,
            // bao nhiêu trang bên phải currentPage
            'right' => 2,
        );

        // nếu maxPage < currentPage thì cho currentPage = maxPage
        if ($maxPage < $currentPage) {
            $currentPage = $maxPage;
        }

        // số trang hiển thị
        $max = $nav['left'] + $nav['right'];

        // phân tích cách hiển thị
        if ($max >= $maxPage) {
            $start = 1;
            $end = $maxPage;
        } elseif ($currentPage - $nav['left'] <= 0) {
            $start = 1;
            $end = $max + 1;
        } elseif (($right = $maxPage - ($currentPage + $nav['right'])) <= 0) {
            $start = $maxPage - $max;
            $end = $maxPage;
        } else {
            $start = $currentPage - $nav['left'];
            if ($start == 2) {
                $start = 1;
            }

            $end = $start + $max;
            if ($end == $maxPage - 1) {
                ++$end;
            }
        }

        $navig = '';
        if ($currentPage >= 2) {
            // thêm nút "Trước"
            
            $navig .= '<li><a href="javascript:void(0);" style="cursor: pointer;" onclick="return pagingByCatAjax(\''.$url->createUrl($path).'\','.ceil($currentPage - 1).',\''.$divId.'\','.$iCat.');">Trước</a></li>';
            if ($currentPage >= $nav['left']) {
                if ($currentPage - $nav['left'] > 2 && $max < $maxPage) {
                    // thêm nút "1"
                    
                   
                	$navig .= '<li><a href="javascript:void(0);" style="cursor: pointer;" onclick="return pagingByCatAjax(\''.$url->createUrl($path).'\',1,\''.$divId.'\','.$iCat.');">1</a></li>';
                    $navig .= '<li>...</li>';
                }
            }
        }

        for ($i = $start; $i <= $end; $i++) {
            // trang hiện tại
            if ($i == $currentPage) {
                
                
            	$navig .= '<li><a class="paging-active" style="cursor: pointer;" onclick="return pagingByCatAjax(\''.$url->createUrl($path).'\','.$i.',\''.$divId.'\','.$iCat.');">'.$i.'</a></li>';
            }
            // trang khác
            else {
                //     $pg_link = $path.'page='.$i;
                
            	$navig .= '<li><a href="javascript:void(0);" style="cursor: pointer;" onclick="return pagingByCatAjax(\''.$url->createUrl($path).'\','.$i.',\''.$divId.'\','.$iCat.');">'.$i.'</a></li>';
            }
        }

        if ($currentPage <= $maxPage - 1) {
            if ($currentPage + $nav['right'] < $maxPage - 1 && $max + 1 < $maxPage) {
                // trang cuoi
                $navig .= '<li>...</li>';
                
                $navig .= '<li><a href="javascript:void(0);" style="cursor: pointer;" onclick="return pagingByCatAjax(\''.$url->createUrl($path).'\','.$maxPage.',\''.$divId.'\','.$iCat.');">'.$maxPage.'</a></li>';
            }
            
            $navig .= '<li><a href="javascript:void(0);" style="cursor:pointer;" onclick="return pagingByCatAjax(\''.$url->createUrl($path).'\','.($currentPage + 1).',\''.$divId.'\','.$iCat.');">Tiếp theo</a></li>';
        }

        // hiển thị kết quả
        return $navig;
    }

    public static function showPageNavigationMore($currentPage, $maxPage, $path = '',$object = '',$first = '',$last = '') {
        $url = new Url();
        $nav = array(
            // bao nhiêu trang bên trái currentPage
            'left' => 2,
            // bao nhiêu trang bên phải currentPage
            'right' => 2,
        );

        // nếu maxPage < currentPage thì cho currentPage = maxPage
        if ($maxPage < $currentPage) {
            $currentPage = $maxPage;
        }

        // số trang hiển thị
        $max = $nav['left'] + $nav['right'];

        // phân tích cách hiển thị
        if ($max >= $maxPage) {
            $start = 1;
            $end = $maxPage;
        } elseif ($currentPage - $nav['left'] <= 0) {
            $start = 1;
            $end = $max + 1;
        } elseif (($right = $maxPage - ($currentPage + $nav['right'])) <= 0) {
            $start = $maxPage - $max;
            $end = $maxPage;
        } else {
            $start = $currentPage - $nav['left'];
            if ($start == 2) {
                $start = 1;
            }

            $end = $start + $max;
            if ($end == $maxPage - 1) {
                ++$end;
            }
        }

        $navig = '';
        if ($currentPage >= 2) {
            // thêm nút "Trước"
            $navig .= '<li><a href="' . $path . 'page=' . ceil($currentPage - 1) . $object . '">Trước</a></li>';
            if ($currentPage >= $nav['left']) {
                if ($currentPage - $nav['left'] > 2 && $max < $maxPage) {
                    // thêm nút "1"
                    $navig .= '<li><a href="' . $path . 'page/1' . $object .'">'. $first .'1'. $last .'</a></li>';
                    $navig .= '<li><a href="javascript:">...</a></li>';
                }
            }
        }

        for ($i = $start; $i <= $end; $i++) {
            // trang hiện tại
            if ($i == $currentPage) {
                $navig .= '<li><a href="' . $path . 'page/' . $i . $object . '" class="active">'. $first . $i . $last .'</a></li>';
            }
            // trang khác
            else {
                //     $pg_link = $path.'page='.$i;
                $navig .= '<li><a href="' . $path . 'page/' . $i . $object . '">'. $first . $i . $last .'</a></li>';
            }
        }

        if ($currentPage <= $maxPage - 1) {
            if ($currentPage + $nav['right'] < $maxPage - 1 && $max + 1 < $maxPage) {
                // trang cuoi
	            $navig .= '<li><a href="javascript:">...</a></li>';
                $navig .= '<li><a href="' . $path . 'page/' . $maxPage . $object . '">'. $first . $maxPage . $last .'</a></li>';
            }
            $navig .= '<li><a href="' . $path . 'page/' . ($currentPage + 1) . $object . '">Sau</a></li>';
        }

        // hiển thị kết quả
        return $navig;
    }
}