<?php
class LoadConfig{
    /*
    danh sách loại game
    */
    static public $arrayTypeGame    = array(
               1=>"Lo game"
              ,2=>"Do game"
              ,3=>"Xien game"
    );
    
    /*Danh sách miền*/
    static public $arrayRegion      = array(
                1=>"mien_bac"
                ,2=>"mien_trung"
                ,3=>"mien_nam"
    ); 
    
    /*Danh sách tỉnh*/
    static public $arrayProvince    = array(
            1=>'Truyền Thống',
            2=>'An Giang',
            3=>'Bạc Liêu',
            4=>'Bến Tre',
            5=>'Bình Dương',
            6=>'Bình Phước',
            7=>'Bình Thuận',
            8=>'Cà Mau',
            9=>'Cần Thơ',
            10=>'Đà Lạt',
            11=>'Đồng Nai',
            12=>'Đồng Tháp',
            13=>'Hậu Giang',
            14=>'Hồ Chí Minh',
            15=>'Kiên Giang',
            16=>'Long An',
            17=>'Sóc Trăng',
            18=>'Tây Ninh',
            19=>'Tiền Giang',
            20=>'Trà Vinh',
            21=>'Vĩnh Long',
            22=>'Vũng Tàu',
            23=>'Bình Định',
            24=>'Đà Nẵng',
            25=>'Đắc Lắc',
            26=>'Đắc Nông',
            27=>'Gia Lai',
            28=>'Khánh Hòa',
            29=>'Kon Tum',
            30=>'Ninh Thuận',
            31=>'Phú Yên',
            32=>'Quảng Bình',
            33=>'Quảng Ngãi',
            34=>'Quảng Nam',
            35=>'Quảng Trị',
            36=>'Thừa Thiên Huế'
    );
    
    static public $arrayTienCuocLo    = array(
            0=>"1",
            1=>"2",
            2=>"5",
            3=>"10",
            4=>"20",
            5=>"40",
            6=>"50",
            7=>"60",
            8=>"80",
            9=>"100",
            10=>"200"
    );
    
    static public $arrayTienCuocDo      = array(
            0=>"2000",
            1=>"5000",
            2=>"10000",
            3=>"20000",
            4=>"30000",
            5=>"50000",
            6=>"100000",
            7=>"200000",
            8=>"300000",
            9=>"500000"
    );   
    
    static public $arrayContactPhone      = array(
                "Hotline"=>"0989706005"
                /*,"Kinh doanh"=>"0945604499"*/
                /*,"Kinh doanh 2"=>""*/
                /*,"CSKH"=>"01298226812"*/
    );
    
    static public $arrayContactMail      = array(
                "Email"=>"ketquaveso@gmail.com"                
    );
    
    static public $arrayContactYahoo      = array(
                1=>"ketquaveso"
    );
    
    static public $arrayContactSkype      = array(
                1=>"cold_boy_89"
    );
    
    static public $arrayTienSoiCau      = array(
            1=>"2000",            
            2=>"4000",            
            3=>"6000",            
            4=>"8000",            
            5=>"10000",            
            6=>"12000",
            7=>"14000"            
    );
    
    static public $arrayTienKetQua      = array(
            7=>"3500",
            20=>"10000",
            30=>"15000"            
    );
    
    static public $arrayTypeTransactionOut      = array(
            1=>"Lo Game",
            2=>"Do Game",
            3=>"Xien Game",
            4=>"Soi Cau",
            5=>"Ket Qua",
            6=>"Doi Thuong"            
    );
    
    static public $arrayTypeTransactionIn      = array(
            1=>"Lo Game",
            2=>"Do Game",
            3=>"Xien Game",
            4=>"Sms",
            5=>"Card"            
    );
    
    static public $arrayTypeSanPham     = array(
            1=>"Thẻ cào",
            2=>"Điện thoại"
    );
    
    static public $arrayTypeBill        = array(
            1=>"Dự đoán soi cầu",
            2=>"KQXS"
    );
    
}

