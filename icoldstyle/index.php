<?php 
echo "3234";die;
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>iColdStyle - Nguyen Tuan Anh porfolio - Web developer</title>
	<meta name="description" content="iColdStyle - Nguyen Tuan Anh porfolio">
	<meta name="author" content="iColdStyle">

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
  ================================================== -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600,700,800' rel='stylesheet'
	      type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Marck+Script' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="stylesheets/base.css">
	<link rel="stylesheet" href="stylesheets/skeleton.css">

	<link rel="stylesheet" href="stylesheets/layout-orange.css">
	<link rel="alternate stylesheet" type="text/css" media="screen" title="blue-theme"
	      href="stylesheets/layout-blue.css"/>
	<link rel="alternate stylesheet" type="text/css" media="screen" title="yellow-theme"
	      href="stylesheets/layout-yellow.css"/>

	<link href="fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="stylesheets/elastislide.css">

	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

</head>
<body>


<!--<a href="#" class="show_hide"><div class="css_switcher"><img src="images/temp_css.png" alt=""/></div></a>

<div class="slidingDiv">
    <p>Màu:</p>
    <a href="javascript:chooseStyle('none', 60)" class="orange">Da cam</a>
    <a href="javascript:chooseStyle('blue-theme', 60)" class="blue">Xanh</a>
    <a href="javascript:chooseStyle('yellow-theme', 60)" class="yellow">Vàng</a>

    <a href="#" class="show_hide">Đóng</a>
</div>-->


<header>
	<div class="topbar" id="top"></div>

	<nav>
		<ul>
			<li><a href="#services">Services</a></li>
			<li><a href="#work">Dự án</a></li>
			<li><a href="#intro"><span class="logo"><span class="logotext">ics</span></span></a></li>
			<li><a href="#about">Giới thiệu</a></li>
			<li><a href="#contact">Liên hệ</a></li>
		</ul>
	</nav>

</header>


<div id="intro">
	<div class="title">
		<span class="title_line"></span><span class="cursive">Hello</span><span class="title_line"></span>

		<h1>
			<span class="small">WELCOME, MY NAME IS</span><br/>
			TUẤN ANH<br/>
			<span class="small">I'M A WEB DEVELOPER</span>
		</h1>
	</div>
	<!-- end title -->
</div>
<!-- end intro -->


<div class="jagged_bottom"></div>
<div class="cover_bar"></div>
<div class="nav_bg"></div>


<div id="services">
	<div class="container">

		<div class="sixteen columns">
			<h2>SERVICES</h2>

			<p>Với 20+ dự án live và 30+ Freelance work thuộc nhiều thể loại và ngôn ngữ lập trình. Tôi tự tin về khả năng kỹ thuật của mình, có thể đáp ứng nhu cầu của bạn !</p>
		</div>
		<!-- end sixteen columns -->

		<div class="one-third column">
			<div class="serv_bg"></div>
			<div class="serv_icon">
				<div class="serv_icon1"></div>
				<h3>LẬP TRÌNH</h3>

				<p>Bạn đang cần một giải pháp IT để phục vụ cho công việc của bạn? Bạn gặp khó khăn, mất thời gian trong việc xử lý công việc của bạn hiện tại?
				   Với kinh nghiệm của mình, tôi có thể giúp được bạn.
				</p>
			</div>
			<!-- end serv_icon -->
		</div>
		<!-- end one-third column -->

		<div class="one-third column">
			<div class="serv_bg"></div>
			<div class="serv_icon">
				<div class="serv_icon3"></div>
				<h3>THIẾT KẾ WEBSITE</h3>

				<p>Bị ảnh hưởng bởi phong cách thiết kế hiện đại của các dự án với đối tác nước ngoài.
					Các thiết kế của tôi sẽ mang những trải nghiệm tốt nhất đến với khách hàng của bạn.
				</p>
			</div>
			<!-- end serv_icon -->
		</div>
		<!-- end one-third column -->

		<div class="one-third column">
			<div class="serv_bg"></div>
			<div class="serv_icon">
				<div class="serv_icon2"></div>
				<h3>TỐI ƯU HỆ THÔNG</h3>

				<p>Hệ thống chậm chạp và thiếu các chức năng mới đang làm giảm hiệu quả kinh doanh của bạn?
					Không cần thiết phải đập đi làm lại một hệ thống mới, tôi sẽ giúp bạn.
				</p>
			</div>
			<!-- end serv_icon -->
		</div>
		<!-- end one-third column -->

		<div class="sixteen columns">
			<div class="div_line"></div>
		</div>

		<div class="one-third column">
			<div class="more_services">
				<h4>VÀ TÔI CÒN CÓ THỂ GIÚP BẠN</h4>
				<ul>
					<li><p>SEO</p></li>
					<li><p>Responsive Design</p></li>
					<li><p>Thiết kế đồ họa</p></li>
					<li><p>Tư vấn kinh doanh online</p></li>
				</ul>
			</div>
			<!-- end more services -->
		</div>
		<!-- end one-third column -->

		<div class="one-third column">
			<div class="more_services2">
				<ul>
					<li><p>SEM</p></li>
					<li><p>App mobile</p></li>
					<li><p>Làm thuê đồ án, project</p></li>
					<li><p>Tư vấn marketing online</p></li>
				</ul>
			</div>
			<!-- end more services -->
		</div>
		<!-- end one-third column -->

		<div class="one-third column">
			<div class="text_box">
				<p>Nếu bạn đang ấp ủ một ý tưởng, sản phẩm nào đấy, sẽ thật tuyệt vời nếu tôi được bạn chia sẻ :D Biết đâu chúng ta sẽ có thể hợp tác và làm việc cùng với nhau ;)</p>
			</div>
			<!-- end text_box -->
		</div>
		<!-- end one-third column -->


	</div>
	<!-- end container -->
</div>
<!-- end services -->

<div class="jagged"></div>
<div class="separator"></div>
<div class="jagged_bottom"></div>

<div class="clear"></div>

<div id="work">
	<div class="container">
		<div class="sixteen columns">
			<h2>DỰ ÁN</h2>

			<p>Các dự án tiêu biểu mà tôi đã tham gia trong thời gian qua...</p>
		</div>
		<!-- end sixteen columns -->

		<div class="clear"></div>

		<div class="gallery">

			<div class="one-third column">
				<a href="images/project_images/thichtruyen.png" class="single_image">
					<img src="images/project_images/thichtruyen.png" alt="" class="scale-with-grid"/>
				</a>

				<p class="proj_name">Thích Truyện</p>

				<p class="proj_type">Team Lead / Develop / Design / HTML</p>
			</div>
			<!-- end one-third column -->

			<div class="one-third column">
				<a href="images/project_images/andam.png" class="single_image">
					<img src="images/project_images/andam.png" alt="" class="scale-with-grid"/>
				</a>

				<p class="proj_name">Ăn dặm App</p>

				<p class="proj_type">Team Lead / Develop / Design</p>
			</div>
			<!-- end one-third column -->

			<div class="one-third column">
				<a href="images/project_images/wish.png" class="single_image">
					<img src="images/project_images/wish.png" alt="" class="scale-with-grid"/>
				</a>

				<p class="proj_name">Wish</p>

				<p class="proj_type">Develop / HTML</p>
			</div>
			<!-- end one-third column -->

		</div>
		<!-- end gallery -->

		<div class="clear"></div>

		<div class="gallery">

			<div class="one-third column">
				<a href="images/project_images/veso.png" class="single_image">
					<img src="images/project_images/veso.png" alt="" class="scale-with-grid"/>
				</a>

				<p class="proj_name">Kết quả vé số</p>

				<p class="proj_type">Develop / HTML</p>
			</div>
			<!-- end one-third column -->

			<div class="one-third column">
				<a href="images/project_images/daikim.png" class="single_image">
					<img src="images/project_images/daikim.png" alt="" class="scale-with-grid"/>
				</a>

				<p class="proj_name">THCS Đại Kim</p>

				<p class="proj_type">Develop / HTML</p>
			</div>
			<!-- end one-third column -->

			<div class="one-third column">
				<a href="images/project_images/az24.png" class="single_image">
					<img src="images/project_images/az24.png" alt="" class="scale-with-grid"/>
				</a>

				<p class="proj_name">AZ24</p>

				<p class="proj_type">Develop / HTML</p>
			</div>
			<!-- end one-third column -->

		</div>
		<!-- end gallery -->

		<div class="clear"></div>

		<div class="sixteen columns">

			<h4>CÁC DỰ ÁN KHÁC CỦA TÔI</h4>

			<!-- Elastislide Carousel -->
			<div id="carousel" class="es-carousel-wrapper">
				<div class="es-carousel">
					<ul>
						<li><a href="javascript:"><img src="images/project_images/tuyensinh.png" alt=""/></a></li>
						<li><a href="javascript:"><img src="images/project_images/kedao.png" alt=""/></a></li>
						<li><a href="javascript:"><img src="images/project_images/hocnhom.png" alt=""/></a></li>
						<li><a href="javascript:"><img src="images/project_images/phale.png" alt=""/></a></li>
						<li><a href="javascript:"><img src="images/project_images/186.png" alt=""/></a></li>
						<li><a href="javascript:"><img src="images/project_images/smartphone.png" alt=""/></a></li>
					</ul>
				</div>
			</div>

		</div>
		<!-- end sixteen columns -->

	</div>
	<!-- end container -->
</div>
<!-- end work -->


<div class="jagged"></div>
<div class="separator"></div>
<div class="jagged_bottom"></div>


<div id="about">
	<div class="container">
		<div class="sixteen columns">
			<h2>GIỚI THIỆU</h2>

			<p>25 tuổi, chuẩn men, vẫn còn single nhưng lại không còn available @@
			   Thuộc cung con cua (24/06) nên cái tôi có vẻ không được thon gọn cho lắm /:)
			   Ít nói (vì ăn nói kém) nhưng lại thích tham gia các hoạt động tập thể.
			   Thích chơi thể thao, xem phim (kinh dị), đi đây đi đó và vẫn còn khá hiếu động, k thể ngồi yên 1 chỗ mà không động đậy tay chân :D
			</p>
		</div>
		<!-- end sixteen columns -->

		<div class="twelve columns">
			<h5>Who Am I?</h5>

			<p>Từng có ý định bỏ học để đi làm, học tập Bill Gates với Mark Zuckerberg nhưng rất tiếc vẫn chưa thể thoát được cái nghiệp học hành.
			   Đã vật vã học xong và có bằng Credit HDSE (Diploma in Software Engineering) của Hà Nội Aptech. Và hiện đang học khoa Quản trị kinh doanh trường Kinh Tế Quốc Dân.
			</p>

			<p>Bắt đầu vác laptop đi làm từ cuối 2009, nhưng phải đến đàu 2011 mới bắt đầu nghiêm túc theo nghiệp Web Developer.
			   Và đến hiện nay là đã có hơn 3 năm kinh nghiệm với PHP. Tuỳ theo yêu cầu công việc ở các công ty của tôi, tôi đã được tiếp xúc với
			   Yii Framework, Zend Framework, Magento Platform, Joomla, ... Cùng với kinh nghiệm làm các dự án ngoài, theo yêu cầu của khách,
			   tôi có khả năng làm việc với các hệ thống lớn, sử dụng cache data, web services, responsive design, html5 và các API của Facebook, Google, OpenId, ...
			</p>

			<p>Điểm mạnh của tôi là khả năng tiếp thu, học hỏi. Tôi thích sự thử thách, làm việc với các hệ thống, dự án với những chức năng
			   mang tính đặc thù riêng. Phong cách thiết kế các sản phẩm của tôi là đơn giản và hiện đại.
			</p>
		</div>
		<!-- end twelve columns -->

		<div class="four columns">
			<div class="social">
				<ul>
					<li><a href="http://twitter.com/cold_boy_89" target="_blank">
						<div class="twitter"></div>
						Twitter</a></li>
					<li><a href="https://www.facebook.com/coldboy89" target="_blank">
						<div class="facebook"></div>
						Facebook</a></li>
					<li><a href="http://vn.linkedin.com/in/icoldstyle/" target="_blank">
						<div class="linkedin"></div>
						Linkedin</a></li>
					<li><a href="#">
						<div class="pinterest"></div>
						Pinterest</a></li>
					<li><a href="#">
						<div class="dribbble"></div>
						Dribbble</a></li>
					<li><a href="https://plus.google.com/u/0/102996401959779793662" target="_blank">
						<div class="gplus"></div>
						Google+</a></li>
				</ul>
			</div>
		</div>
		<!-- end four columns -->

		<div class="sixteen columns">
			<div class="skills">
				<h5>Kỹ năng</h5>

				<div class="skill_bg">
					<div class="skill2 skill_hover" style="width: 80%;"></div>
				</div>
				<p>PHP</p>

				<div class="skill_bg">
					<div class="skill1 skill_hover" style="width: 85%"></div>
				</div>
				<p>Javascript</p>

				<div class="skill_bg">
					<div class="skill4 skill_hover" style="width: 70%"></div>
				</div>
				<p>HTML/CSS</p>

				<div class="skill_bg">
					<div class="skill3 skill_hover" style="width: 80%"></div>
				</div>
				<p>Database</p>

				<div class="skill_bg">
					<div class="skill3 skill_hover" style="width: 100%"></div>
				</div>
				<p>Tán gái</p>

			</div>
			<!-- end skills -->
		</div>
		<!-- end sixteen columns -->

	</div>
	<!-- end container -->
</div>
<!-- end about -->


<div class="jagged"></div>
<div class="separator"></div>
<div class="jagged_bottom_color"></div>


<div id="contact">
	<div class="container">
		<div class="sixteen columns">
			<h2>Liên hệ</h2>

			<p>Đến được phần này thì chắc hẳn bạn đã bắt đầu quan tâm tới khả năng hay các dịch vụ của tôi. Còn chờ gì nữa mà không gọi hoặc viết cho tôi một cái mail? ;)
				<br>
			   Sẽ thật toẹt vời nếu tôi và bạn có thể được làm việc, hợp tác cùng với nhau.</p>
		</div>
		<!-- end sixteen columns -->

		<div class="eight columns">
			<div class="contact_form">

				<div class="done">
					<b>Cảm ơn bạn!</b> Tôi đã nhận được tin nhắn của bạn.
				</div>

				<form method="post" action="process.php">
					<p>tên</p>
					<input type="text" name="name" class="text"/>

					<p>email</p>
					<input type="text" name="email" class="text" id="email"/>

					<p>nội dung</p>
					<textarea name="comment" class="text"></textarea>

					<input type="submit" id="submit" value="gửi" class="submit-button"/>
				</form>

			</div>
			<!-- end contact_form -->
		</div>
		<!-- end eight columns -->

		<div class="eight columns">

			<iframe class="map" width="460" height="200" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
			        src="https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=21.020459,105.833958&amp;spn=0.006501,0.019698&amp;z=15&amp;output=embed"></iframe>

			<div class="contact_info">
				<div class="four columns alpha">
					<p><img src="images/icn_phone.png" alt=""/> +849 456 9 4499</p>

					<p><img src="images/icn_mail.png" alt=""/> icoldstyle@gmail.com</p>
				</div>

				<div class="four columns omega">
					<p><img src="images/icn_address.png" alt=""/> 95 Trung Tả, Khâm Thiên<br/>
					                                              Đống Đa, Hà Nội
					</p>
				</div>
			</div>

		</div>
		<!-- end eight columns -->

	</div>
	<!-- end container -->
</div>
<!-- end contact -->


<div class="jagged_top_color"></div>

<div class="copyright">
	<p>&copy; 2014 All rights reserved<br/>
	          Một sản phẩm của <a href="http://icoldstyle.com/" target="_blank">iColdStyle</a></p>
</div>


<!-- JS
================================================== -->
<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script src="javascripts/tabs.js"></script>
<script src="javascripts/styleswitch.js"></script>
<script src="javascripts/smooth-scroll.js" type="text/javascript"></script>
<script src="javascripts/smooth-scroll-ios.js" type="text/javascript"></script>
<script src="javascripts/jquery.sticky.js" type="text/javascript"></script>
<script src="javascripts/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="javascripts/jquery.elastislide.js" type="text/javascript"></script>
<script src="fancybox/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script>
<script src="contact-form.js"></script>


<!-- Sticky background navigation -->
<script>
	$(document).ready(function () {
		$(".nav_bg").sticky({topSpacing: 80});
	});
</script>


<!-- temp -->
<script type="text/javascript">

	$(document).ready(function () {

		$(".slidingDiv").hide();
		$(".show_hide").show();

		$('.show_hide').click(function () {
			$(".slidingDiv").slideToggle();
		});

	});

</script>


<!-- caroussel slider -->
<script>
	$('#carousel').elastislide({
		imageW: 180
	});
</script>


<!-- fancybox -->
<script type="text/javascript">
	$(document).ready(function () {

		/* This is basic - uses default settings */

		$("a.single_image").fancybox();

		/* Using custom settings */

		$("a#inline").fancybox({
			'hideOnContentClick': true
		});

		/* Apply fancybox to multiple items */

		$("a.group").fancybox({
			'transitionIn': 'elastic',
			'transitionOut': 'elastic',
			'speedIn': 600,
			'speedOut': 200,
			'overlayShow': false
		});

	});
</script>


<!-- End Document
================================================== -->
</body>
</html>