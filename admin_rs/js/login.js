$(document).ready(function () {	  
    $('#login').show().animate({   opacity: 1 }, 2000);
    $('.logo').show().animate({   opacity: 1,top: '40%'}, 800,function(){			
        $('.logo').show().delay(1200).animate({   opacity: 1,top: '12%' }, 300,function(){
            $('.formLogin').animate({   opacity: 1,left: '0' }, 300);
            $('.userbox').animate({ opacity: 0 }, 200).hide();
        });		

    })	
    $(".on_off_checkbox").iphoneStyle();
    $('.tip a ').tipsy({gravity: 'sw'});
    $('.tip input').tipsy({ trigger: 'focus', gravity: 'w' });
});	

$('.userload').click(function(e){
    $('.formLogin').animate({   opacity: 1,left: '0' }, 300);			    
    $('.userbox').animate({ opacity: 0 }, 200,function(){
        $('.userbox').hide();				
    });
});

$('#alertMessage').click(function(){
    hideTop();
});

function showError(str){
    $('#alertMessage').addClass('error').html(str).stop(true,true).show().animate({ opacity: 1,right: '10'}, 500);	

}

function showSuccess(str){
    $('#alertMessage').removeClass('error').html(str).stop(true,true).show().animate({ opacity: 1,right: '10'}, 500);	
}

function hideTop(){
    $('#alertMessage').animate({ opacity: 0,right: '-20'}, 500,function(){ $(this).hide(); });	
}	

function loading(name,overlay) {  
    $('body').append('<div id="overlay"></div><div id="preloader">'+name+'..</div>');
    if(overlay==1){
        $('#overlay').css('opacity',0.1).fadeIn(function(){  $('#preloader').fadeIn();	});
        return  false;
    }
    $('#preloader').fadeIn();	  
}

function unloading() {  
    $('#preloader').fadeOut('fast',function(){ $('#overlay').fadeOut(); });
}
