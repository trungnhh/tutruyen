/* :files, 'spec_javascripts', ... @ (none) */


/* :files, 'spec_javascripts', ... @ (none) */


/* :asset_packager_compatibility, 'config/asset_packages.yml' @ 1384554040 */
/* public/javascripts/4gen.js @ 1384554040 */
init_4gen = function () {
    var e;
    typeof jQuery != "undefined" && (e = jQuery);
    var t = "scroll", n = 3, r = 8, i = "/images/4gen/trans_1x1.gif", s = 30, o = 315, u, a = 1, f = 2, l = 3, c = 12;
    window.console || (window.console = {log: function () {
    }});
    var h = function () {
        function h(e) {
        }

        function p(e, t) {
            e.prototype.addEvent = h.prototype.addEvent, e.prototype.removeEvent = h.prototype.removeEvent, e.prototype.fireEvent = h.prototype.fireEvent, e.prototype.initEventHelper = h.prototype.initEventHelper, e.prototype.possibleEvents = t
        }

        function S(e, t, n, r, i, s) {
            this.id = e, this.shortstyle = t, this.family = n, this.fallback = r, this.weight = i, this.style = s
        }

        function x(e) {
            this.fonts = [], this.docManager = e, this._cssRuleQueue = [], this._fontLoadQueue = []
        }

        function T(e, t) {
            this.pages = [], this.loaded = !1, this.fonts = {}, this.numFonts = 0, this.fontLoader = t, this.groupNum = e || 0
        }

        function O(e) {
            for (var t in this._defaultParams)this._defaultParams.hasOwnProperty(t) && (this[t] = e[t] || this._defaultParams[t]);
            for (var n = 0; n < this._requiredParams.length; n++) {
                var r = this._requiredParams[n];
                if (!this[r])throw"Missing required Page param: " + r
            }
            if (!this.contentUrl && !this.innerPageElem)throw"Must initialize a page with either a contentUrl or innerPageElem element";
            if (this.containerElem.boundToPageObj === !0)throw"Container Elem is already bound to a page.  We shouldn't get here";
            this.containerElem.boundToPageObj = !0, this._targetWidth = null, this._innerPageVisible = !!this.innerPageElem, this._imagesTurnedOn = !1, this.boundingRect = null, this.isVisible = !1, this.displayDirty = !0, this.displayOn = null, this.loadHasStarted = !!this.innerPageElem
        }

        function _() {
            this.initEventHelper(), this.viewRect = null, this.enabled = !1;
            var e = this;
            this._scrollCallback = function () {
                e._eventHandler("scroll")
            }, this._resizeCallback = function (t) {
                e._eventHandler("resize")
            }
        }

        function D() {
        }

        function P(t, n, r) {
            typeof e == "undefined" && typeof scribd != "undefined" && F.setJQuery(scribd.jQuery), e("html, body").animate({scrollTop: e(t).offset().top}, {queue: !1, duration: n, easing: "linear", complete: r})
        }

        function H() {
            this._name = "book", this.currentPageId = null;
            var e = this;
            this._fullscreenResizedCallback = function (t) {
                e._fullscreenResized(t)
            }
        }

        function B() {
            this._name = "slideshow", this.currentPageId = null;
            var e = this;
            this._fullscreenResizedCallback = function (t) {
                e._fullscreenResized(t)
            }
        }

        function j() {
            this._name = "scroll";
            var e = this;
            this._verticalPositionChangeCallback = function () {
                e.checkAndUpdateVisiblePages()
            }, this._fullscreenResizedCallback = function () {
                e._fullscreenResized()
            }, this._afterGotoPage = function () {
                e.documentManager.setIsScrolling(!1), e.isScrolling || (e.documentManager.visiblePagesChanged(), e._scrollEffect && delete e._scrollEffect)
            }
        }

        function F(t, n, r) {
            this.options = r || {}, this.options.extrasWidth = this.options.extrasWidth || o, this.defaultViewMode = t || "scroll", this.mobile = n || !1, e && (this.view_manager_deferred = e.Deferred()), this.initEventHelper(), this.pages = {}, this._pageWidths = null, this._fontLoader = new x(this), this.viewManagers = {scroll: new j, slideshow: new B, book: new H}, this.viewportManager = new _, this._currentFontAggregatorHostIdx = 0, this.visiblePages = [], this.firstVisiblePage = null, this.lastVisiblePage = null, this.currentFontGroup = new T(0, this._fontLoader), this.isScrolling = !1, this._scrollingCount = 0
        }

        function I(e, t, n) {
            return Math.min(n, Math.max(t, e))
        }

        function q(e) {
            return Math.floor(e * 100) / 100
        }

        var t = {_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", decode: function (e) {
            var n = "", r, i, s, o, u, a, f, l = 0;
            e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            while (l < e.length)o = this._keyStr.indexOf(e.charAt(l++)), u = this._keyStr.indexOf(e.charAt(l++)), a = this._keyStr.indexOf(e.charAt(l++)), f = this._keyStr.indexOf(e.charAt(l++)), r = o << 2 | u >> 4, i = (u & 15) << 4 | a >> 2, s = (a & 3) << 6 | f, n += String.fromCharCode(r), a != 64 && (n += String.fromCharCode(i)), f != 64 && (n += String.fromCharCode(s));
            return n = t._utf8_decode(n), n
        }, _utf8_decode: function (e) {
            var t = "", n = 0, r = 0, i = 0, s = 0;
            while (n < e.length)r = e.charCodeAt(n), r < 128 ? (t += String.fromCharCode(r), n++) : r > 191 && r < 224 ? (s = e.charCodeAt(n + 1), t += String.fromCharCode((r & 31) << 6 | s & 63), n += 2) : (s = e.charCodeAt(n + 1), c3 = e.charCodeAt(n + 2), t += String.fromCharCode((r & 15) << 12 | (s & 63) << 6 | c3 & 63), n += 3);
            return t
        }};
        h.prototype.initEventHelper = function () {
            this.eventListenerEnabled = !0, this.eventListeners = {};
            for (var e = 0; e < this.possibleEvents.length; e++)this.eventListeners[this.possibleEvents[e]] = {}
        }, h.prototype.addEvent = function (e, t) {
            if (!this.eventListeners[e])throw e + " is not a valid type of event";
            this.eventListeners[e].next_uid || (this.eventListeners[e].next_uid = 1), t._event_listener_uid || (t._event_listener_uid = this.eventListeners[e].next_uid, this.eventListeners[e].next_uid++), this.eventListeners[e][t._event_listener_uid] = t
        }, h.prototype.removeEvent = function (e, t) {
            delete this.eventListeners[e][t._event_listener_uid]
        }, h.prototype.fireEvent = function (e, t) {
            function o(e) {
                e.apply(s, r)
            }

            if (!this.eventListenerEnabled)return;
            var n = this.eventListeners[e], r = [];
            if (arguments.length > 1) {
                r[arguments.length - 2] = null;
                for (var i = 1; i < arguments.length; i++)r[i - 1] = arguments[i]
            }
            var s = this;
            for (var u in n)u != "next_uid" && n.hasOwnProperty(u) && o(n[u])
        };
        var d = "font_preload_bed", v = "", m = function () {
            return document.styleSheets[0] && !document.styleSheets[0].insertRule
        }, g = function (e) {
            return e % 1 == 0
        }, y = function (e, t) {
            if (m()) {
                var n = e.innerHTML;
                e.href = t, e.innerHTML != n && (e.innerHTML = n)
            } else e.href = t
        }, b = function () {
            var e = navigator.userAgent.toLowerCase();
            return e.search("android 2.2") > -1
        }(), w = function () {
            var e = navigator.userAgent.toLowerCase();
            return e.search("mobile") > -1 && e.search("safari") > -1 && !b
        }(), E = navigator.userAgent.indexOf("AppleWebKit/") > -1;
        u = function () {
            return m() ? a : w ? f : f
        }(), m() && (r = 5, n = 2), b && (r = 1, n = 1), S.prototype.eotCssRule = function (e) {
            var t = "src: url(" + e + this.family + ".eot); " + "font-family: " + this.family + "; font-weight: " + this.weight + "; font-style: " + this.style;
            return"@font-face {" + t + "}"
        }, S.prototype.ttfCssRule = function (e) {
            var t = "src: url(" + e + this.family + ".ttf) format('truetype'); " + "font-family: " + this.family + "; font-weight: " + this.weight + "; font-style: " + this.style;
            return"@font-face {" + t + "}"
        }, S.prototype.svgCssRule = function (e) {
            var t = "src: url(" + e + "#" + this.family + ") format('svg'); " + "font-family: " + this.family + "; font-weight: " + this.weight + "; font-style: " + this.style;
            return"@font-face {" + t + "}"
        }, S.prototype.createPreloadElem = function () {
            return"<span style='font-family: " + this.family + "'>scribd.</span> "
        }, x.prototype._makeNewStyleBlock = function () {
            var e = document.createElement("style");
            window.createPopup || e.appendChild(document.createTextNode(""));
            var t = document.getElementsByTagName("head")[0];
            return t.appendChild(e), e
        }, x.prototype._insertCssRule = function (e) {
            this._cssRuleQueue.push(e)
        }, x.prototype._flushCssRuleQueue = function (e) {
            if (this._cssRuleQueue.length > 0) {
                var t = e && document.getElementById(e) || this._makeNewStyleBlock(), n = this._cssRuleQueue.join("\n");
                m() ? t.styleSheet.cssText = n : window.createPopup ? t.innerHTML = n : t.appendChild(document.createTextNode(n)), this._cssRuleQueue = []
            }
        }, x.prototype.getFontAggregatorHostForFonts = function (e) {
            var t = [];
            for (var n = 0; n < e.length; n++)t.push(e[n].shortstyle + e[n].id);
            t.sort();
            var r = this.docManager.nextFontAggregatorHost() + "/" + this.docManager.assetPrefix + "/" + t.join(",") + "/" + c + "/";
            switch (u) {
                case a:
                    break;
                case f:
                    b || (r += "ttfs.css");
                    break;
                case l:
                    r += "fonts.svg"
            }
            return r
        }, x.prototype._addTTFRules = function (e, t) {
            for (var n = 0; n < e.length; n++) {
                var r = e[n];
                this._insertCssRule(r.ttfCssRule(t))
            }
        }, x.prototype._addSVGRules = function (e, t) {
            for (var n = 0; n < e.length; n++) {
                var r = e[n];
                this._insertCssRule(r.svgCssRule(t))
            }
        }, x.prototype._addEOTRules = function (e, t) {
            for (var n = 0; n < e.length; n++) {
                var r = e[n];
                this._insertCssRule(r.eotCssRule(t))
            }
        }, x.prototype._nextPreloadId = function () {
            this._preloadId || (this._preloadId = 0);
            var e = "preload_bed" + this._preloadId;
            return this._preloadId += 1, e
        }, x.prototype._addCSSWebkit = function (e, t) {
            function i() {
                var e = document.createElement("div"), i = n._nextPreloadId();
                e.innerHTML = o;
                var s = document.getElementById(d), u = window.setInterval(function () {
                    r.contentDocument.styleSheets.length > 0 && (window.clearInterval(u), r.contentDocument.body.getBoundingClientRect(), s.appendChild(e), window.setTimeout(function () {
                        r.contentDocument.body.getBoundingClientRect(), n._addCSSLink(t)
                    }, 500))
                }, 0)
            }

            var n = this, r = document.createElement("iframe"), s = this._nextPreloadId();
            r.id = s;
            var o = "";
            for (var u = 0; u < e.length; u++) {
                var a = e[u];
                o += a.createPreloadElem()
            }
            var f = this._makeNewStyleBlock();
            r.addEventListener("load", function () {
                i()
            }, !1), r.style.display = "none", document.body.appendChild(r), r.contentDocument.body.innerHTML = o;
            var l = r.contentDocument.createElement("link");
            l.href = t, l.rel = "stylesheet", l.type = "text/css", l.media = "screen";
            var c = r.contentDocument.getElementsByTagName("head")[0];
            c.appendChild(l)
        }, x.prototype._addCSSLink = function (e) {
            var t = document.createElement("link");
            t.href = e, t.rel = "stylesheet", t.type = "text/css";
            var n = document.getElementsByTagName("head")[0];
            n.appendChild(t)
        }, x.prototype.flushFontQueue = function () {
            if (this._fontLoadQueue.length === 0)return;
            if (this.docManager.displayType == "rasterize")return;
            var e = this._fontLoadQueue;
            this._fontLoadQueue = [];
            var t = this.getFontAggregatorHostForFonts(e);
            switch (u) {
                case a:
                    this._addEOTRules(e, t);
                    break;
                case f:
                    b ? this._addTTFRules(e, t) : E ? this._addCSSWebkit(e, t) : this._addCSSLink(t);
                    break;
                case l:
                    this._addSVGRules(e, t)
            }
            this._flushCssRuleQueue()
        }, x.prototype.addFontToQueue = function (e) {
            var t = this.fonts[e];
            t._loadQueued || (t._loadQueued = !0, this._fontLoadQueue.push(t))
        }, x.prototype.addFont = function (e, t, n, r, i, s) {
            var o = new S(e, t, n, r, i, s);
            this.fonts[e] = o
        }, x.prototype.setNumFonts = function (e) {
            for (var t = 0; t < e; t++)this.fonts[t] = new S(t, "", "ff" + t, "sans-serif", "normal", "normal")
        }, x.prototype._initHidersCSS = function () {
            var e = [];
            for (var t = 0; t < this.fonts.length; t++)e.push("." + this.fonts[t].family);
            this._insertCssRule(e.join(", ") + " {display: none;}\n")
        }, x.prototype._initFamilyCSS = function () {
            for (var e = 0; e < this.fonts.length; e++) {
                var t = this.fonts[e].family, n = this.fonts[e].fallback, r = this.fonts[e].weight, i = this.fonts[e].style, s = "div." + t + " span";
                typeof scribd != "undefined" && typeof scribd.embed_div_id != "undefined" && (s = "#" + scribd.embed_div_id + " " + s), m() ? this._insertCssRule(s + " {font-family: " + t + " !important;\n}") : this._insertCssRule(s + " {font-family: " + t + ", " + n + "; font-weight: " + r + "; font-style: " + i + ";\n}")
            }
        }, x.prototype.initStyles = function (e) {
            if (this.docManager.displayType == "rasterize")return;
            this._initFamilyCSS();
            switch (u) {
                case a:
                    break;
                case f:
                    b || this._initHidersCSS();
                    break;
                case l:
            }
            this._flushCssRuleQueue("preload_styler")
        }, x.prototype.setupTestElements = function () {
            if (u != f || E || this.docManager.displayType == "rasterize")return;
            var e = "";
            for (var t = 0; t < this.fonts.length; t++) {
                var n = this.fonts[t];
                e += n.createPreloadElem()
            }
            var r = document.getElementById(d);
            r.innerHTML = e, document.body.appendChild(r), this._insertCssRule("#" + d + " span {display: block; visibility: hidden}"), this._flushCssRuleQueue()
        }, T.prototype.addPage = function (e) {
            for (var t = 0; t < e.fonts.length; t++) {
                var n = e.fonts[t];
                this.fonts[n] === undefined && (this.fonts[n] = !0, this.numFonts += 1)
            }
            this.pages.push(e)
        }, T.prototype.isFull = function () {
            var e = this.pages.length, t = this.groupNum, n = this.numFonts;
            return u == a ? this.hasLoaded || n > 50 || n >= 20 && (t === 0 && e >= r + 3 || e >= 15) : u == l || b ? this.hasLoaded || n >= 5 && e >= r : this.hasLoaded || n > 100 || n >= 20 && (t === 0 && e >= r + 3 || e >= 100)
        }, T.prototype.load = function (e) {
            function n() {
                for (var e in t.fonts)t.fonts.hasOwnProperty(e) && t.fontLoader.addFontToQueue(e);
                t.fontLoader.flushFontQueue()
            }

            if (this.hasLoaded)return;
            this.hasLoaded = !0;
            var t = this;
            e ? window.setTimeout(function () {
                n()
            }, e) : n()
        }, T.prototype.newNextGroup = function () {
            return new T(this.groupNum + 1, this.fontLoader)
        };
        var N = 1, C = 2, k = 3, L = 4, A = function () {
            return document.documentElement.style.WebkitTransform !== undefined ? N : document.documentElement.style.MozTransform !== undefined ? C : document.documentElement.style.OTransform !== undefined ? L : k
        }();
        O.prototype._defaultParams = {containerElem: null, innerPageElem: null, contentUrl: null, origWidth: null, origHeight: null, fonts: null, docManager: null, pageNum: null}, O.prototype._requiredParams = ["origWidth", "origHeight", "fonts", "docManager", "containerElem", "pageNum"], O.prototype._updateBoundingRect = function () {
            var e, t, n, r;
            if (this.containerElem.getBoundingClientRect && this.docManager.viewportManager.viewRect) {
                var i = this.containerElem.getBoundingClientRect(), s = this.docManager.viewportManager.viewRect;
                t = i.left + s.left, e = i.top + s.top, n = i.right - i.left, r = i.bottom - i.top, this.boundingRect = {left: t, top: e, bottom: e + r, right: t + n, width: n, height: r}
            } else e = this.containerElem.offsetTop, t = this.containerElem.offsetLeft, n = this.containerElem.offsetWidth, r = this.containerElem.offsetHeight, this.boundingRect = {left: t, top: e, bottom: e + r, right: t + n, width: n, height: r}
        }, O.prototype._setContainerContents = function (e) {
            var t = /<noscript *><img[^<>]*\/><\/noscript *>/g, n = document.createElement("div");
            n.innerHTML = e.replace(t, ""), this.containerElem.appendChild(n);
            var r = this;
            this.innerPageElem = this.containerElem.lastChild, this.turnOnLinks(), this.turnOnImages(), this.fixSVGFonts(), this.displayDirty = !0, this.displayOn ? this.display() : this.hide()
        }, O.prototype.fixSVGFonts = function () {
            if (this._svgFontsFixed)throw"Already fixed the svg fonts";
            if (!this.innerPageElem)return;
            if (w) {
                var e = function (t) {
                    if (t.nodeType == document.TEXT_NODE) {
                        var n = t.textContent.search(/[  \n][^ \n ]/);
                        n >= 0 && e(t.splitText(n + 1))
                    } else {
                        var r = t.childNodes;
                        for (var i = 0; i < r.length; i++)e(r[i])
                    }
                }, t = function (e) {
                    var n = e.childNodes;
                    for (var r = 0; r < n.length; r++) {
                        var i = n[r];
                        if (i.nodeType == document.ELEMENT_NODE)t(i); else {
                            var s = n[r + 1];
                            s && s.nodeName == "#text" && e.insertBefore(document.createElement("span"), s)
                        }
                    }
                };
                e(this.innerPageElem), t(this.innerPageElem)
            }
            this._svgFontsFixed = !0
        }, O.prototype.imagePageContent = function (e) {
            return str = "<img src='" + e + "'></img>", str
        }, O.prototype.load = function () {
            this.currentlyLoading = !0, this.loadHasStarted = !0;
            if (this.innerPageElem)throw"We already have loaded this page, but it looks like you called loadPage again";
            this.loadFonts();
            var t = "page" + this.pageNum + "_callback";
            if (window[t])try {
                delete window[t]
            } catch (n) {
                window[t] = undefined
            }
            if (this.docManager.displayType == "rasterize") {
                delete this.currentlyLoading, this._setContainerContents(this.imagePageContent(this.contentUrl));
                return
            }
            var r = document.createElement("script"), i = this;
            window[t] = function (n) {
                document.body.removeChild(r);
                var s = n[0];
                delete i.currentlyLoading, i._setContainerContents(s), typeof e != "undefined" && e("#" + i.containerElem.id).hasClass("blurred_page") && i.docManager.doDynamicBlurring(i.containerElem.id), i.docManager.fireEvent("pageLoaded", i.containerElem);
                try {
                    delete window[t]
                } catch (o) {
                    window[t] = undefined
                }
            }, r.src = this.contentUrl, r.type = "text/javascript", r.charset = "UTF-8", document.body.appendChild(r)
        }, O.prototype.remove = function () {
            if (this.innerPageElem) {
                var e = this.innerPageElem.parentNode;
                e.removeChild(this.innerPageElem), delete this.innerPageElem, delete this.currentLoading, delete this.loadHasStarted, this._linksTurnedOn = !1, this._imagesTurnedOn = !1, this._svgFontsFixed = !1
            }
        }, O.prototype.display = function (e, t) {
            if (this.displayOn && !this.displayDirty)return;
            this.displayOn = !0;
            if (this.currentlyLoading)return;
            if (!this.innerPageElem) {
                if (this.loadHasStarted)return;
                if (e) {
                    this.load();
                    return
                }
                return
            }
            this.displayDirty = !1, t || (this._linksTurnedOn || this.turnOnLinks(), this._imagesTurnedOn || this.turnOnImages(), this._svgFontsFixed || this.fixSVGFonts()), this.loadFonts();
            if (this._innerPageVisible)return;
            this.containerElem.className = this.containerElem.className.replace(/placeholder|not_visible/g, ""), this._innerPageVisible = !0, t || (this._fitContentsToWidth(), this.innerPageElem.style.display = "block")
        }, O.prototype.hide = function () {
            if (!this.displayOn && !this.displayDirty)return;
            this.displayOn = !1;
            if (!this.innerPageElem)return;
            this.displayDirty = !1, this.containerElem.className = this.containerElem.className + " not_visible", this._innerPageVisible = !1, this.innerPageElem.style.display = "none"
        }, O.prototype.setLoadFontGroup = function (e) {
            e.addPage(this), this.loadFontGroup = e
        }, O.prototype.loadFonts = function () {
            this.loadFontGroup.load()
        };
        var M = !!(document.all && /msie 6./i.test(navigator.appVersion) && window.ActiveXObject);
        return O.prototype._setZoomScale = function (e) {
            var t = this.innerPageElem;
            switch (A) {
                case N:
                    t.style.WebkitTransform = "scale(" + e + ")", t.style.WebkitTransformOrigin = "top left";
                    break;
                case C:
                    t.style.MozTransform = "scale(" + e + ")", t.style.MozTransformOrigin = "top left";
                    break;
                case L:
                    t.style.OTransform = "scale(" + e + ")", t.style.OTransformOrigin = "top left";
                    break;
                case k:
                    t.originalZoom || (t.originalZoom = t.currentStyle.zoom == "normal" ? 1 : parseFloat(t.currentStyle.zoom) / 100, M && !this.docManager._isEmbed && (t.originalZoom *= 1.35)), t.style.zoom = t.originalZoom * e * 100 + "%";
                    if (M) {
                        var n = this.innerPageElem;
                        setTimeout(function () {
                            n.style.marginLeft = n.style.marginLeft === "" ? 0 : ""
                        }, 500)
                    }
                    break;
                default:
                    throw"Unknown scale method " + A
            }
        }, O.prototype._fitContentsToWidth = function () {
            if (this._targetWidth && this.innerPageElem && this._innerPageVisible) {
                var e = this._targetWidth / this.origWidth;
                this._setZoomScale(e), this._targetWidth = null
            }
        }, O.prototype.setWidth = function (e) {
            var t = Math.ceil(e / this.origWidth * this.origHeight);
            this.containerElem.style.width = e + "px", this.containerElem.style.height = t + "px", this._targetWidth = e, this._fitContentsToWidth()
        }, O.prototype.setBounds = function (e, t) {
            this.origWidth / this.origHeight > e / t ? t = Math.ceil(e / this.origWidth * this.origHeight) : e = Math.ceil(t / this.origHeight * this.origWidth), this.containerElem.style.width = e + "px", this.containerElem.style.height = t + "px", this._targetWidth = e, this._fitContentsToWidth()
        }, O.prototype.turnOnImages = function () {
            if (!this.innerPageElem)throw"Can't turn on images for a page that's not loaded";
            if (this._imagesTurnedOn)throw"Images have already been turned on for this document";
            this._imagesTurnedOn = !0;
            var e = this.innerPageElem.getElementsByTagName("img");
            for (var t = 0; t < e.length; t++) {
                var n = e[t];
                if (n.className.toLowerCase().search("absimg") > -1 && !n.src) {
                    var r = this.docManager.subImageSrc(n.getAttribute("orig"));
                    this.docManager.enablePNGHack ? (n.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + r + "', sizingMethod='scale')", n.src = i) : n.src = r, n.removeAttribute("orig"), n.style.display = "block"
                }
            }
        }, O.prototype.turnOnLinks = function () {
            if (!this.innerPageElem)throw"Can't turn on links for a page that's not loaded";
            if (this._linksTurnedOn)throw"Links have already been turned on for this document";
            this._linksTurnedOn = !0;
            var e = this.docManager, n = function (t) {
                return function () {
                    e.gotoPage(t.substring(4))
                }
            }, r = this.innerPageElem.getElementsByTagName("a");
            for (var i = 0; i < r.length; i++) {
                var s = r[i];
                if (s.className.toLowerCase().search("ll") > -1 && !s.href) {
                    var o = s.getAttribute("orig");
                    if (o) {
                        var u = t.decode(o).replace(/^j[\W]*a[\W]*v[\W]*a[\W]*s[\W]*c[\W]*r[\W]*i[\W]*p[\W]*t[\W]*:|^f[\W]*i[\W]*l[\W]*e[\W]*:/ig, "");
                        u.search(/^page/) > -1 ? s.onclick = n(u) : (u.search(/^mailto:/) >= 0 || u.search(/^(http|ftp)/) < 0 && (u = "http://" + u), s.target = "_blank", y(s, u), s.rel = "nofollow")
                    }
                }
            }
        }, p(_, ["vertical", "horizontal", "either", "resize"]), _.prototype._makeViewRect = function () {
            var e = document.documentElement, t = window.scrollY || window.pageYOffset || e.scrollTop, n = window.scrollX || window.pageXOffset || e.scrollLeft, r = window.innerWidth || e.clientWidth, i = window.innerHeight || e.clientHeight, s = n + r, o = t + i;
            return{top: t, left: n, right: s, bottom: o, width: r, height: i}
        }, _.prototype._updateViewRect = function () {
            var e = this.viewRect;
            this.viewRect = this._makeViewRect();
            var t = !e || e.left != this.viewRect.left || e.width != this.viewRect.width, n = !e || e.top != this.viewRect.top || e.height != this.viewRect.height;
            return{xChanged: t, yChanged: n}
        }, _.prototype._eventHandler = function (e) {
            var t = this._updateViewRect();
            (e == "resize" || e == "both") && (t.xChanged || t.yChanged) && this.fireEvent("resize", this.viewRect), (t.xChanged || t.yChanged) && this.fireEvent("either", this.viewRect), t.xChanged && this.fireEvent("horizontal", this.viewRect), t.yChanged && this.fireEvent("vertical", this.viewRect)
        }, _.prototype.enable = function () {
            if (this.enabled)throw"ViewportManager has already been enabled";
            this.enabled = !0, this.container = window, this._eventHandler("both"), window.addEventListener ? (window.addEventListener("resize", this._resizeCallback, !1), window.document.addEventListener("scroll", this._scrollCallback, !1)) : window.attachEvent && (window.attachEvent("onresize", this._resizeCallback), window.attachEvent("onscroll", this._scrollCallback))
        }, _.prototype.disable = function () {
            if (!this.enabled)throw"ViewportManager has already been disabled";
            this.enabled = !1, this.container.removeEventListener ? (window.removeEventListener("resize", this._resizeCallback, !1), this.container.removeEventListener("scroll", this._scrollCallback, !1)) : window.detatchEvent && (window.detatchEvent("onresize", this._resizeCallback), this.container.detatchEvent("onscroll", this._scrollCallback))
        }, D.prototype.name = function () {
            return this._name
        }, D.prototype.register = function (e, t) {
            if (this.registered)throw"This ViewManager is already registered";
            this.registered = !0, this.documentManager = e, this.viewportManager = t, this._currentPageWidth = defaultViewWidth, this._currentZoomMultiplier = 1, this._updatePageWidths();
            var n = this.documentManager.firstVisiblePage;
            this._register(e, t), n && this.documentManager.gotoPage(n.pageNum, {pretty: !1}), e.view_manager_deferred && e.view_manager_deferred.resolve(e)
        }, D.prototype._zoomedPageWidth = function () {
            return this._currentPageWidth * this._currentZoomMultiplier
        }, D.prototype._updatePageWidths = function () {
            this.documentManager.setPageWidths(this._zoomedPageWidth())
        }, D.prototype.isTopPageInView = function () {
            return!0
        }, D.prototype._register = function (e, t) {
        }, D.prototype.unregister = function () {
            if (!this.registered)throw"This ViewManager is already unregistered";
            this.isFullscreen && this.exitFullscreen(), this._checkBodyWidth(), this._unregister(), this.registered = !1, delete this.documentManager, delete this.viewportManager
        }, D.prototype._unregister = function () {
        }, D.prototype._pagingStep = function () {
            return 1
        }, D.prototype.gotoPage = function (e, t) {
            if (!this.registered)throw"ViewManager must be registerd to call gotoPage";
            this._gotoPage(e, t)
        }, D.prototype._gotoPage = function (e, t) {
        }, D.prototype._fireHideExtras = function () {
            this.documentManager._fireHideExtras(), this._extrasHidden = !0
        }, D.prototype._fireShowExtras = function () {
            this.documentManager._fireShowExtras(), this._extrasHidden = !1
        }, D.prototype.enterFullscreen = function () {
            if (this.isFullscreen)throw"Fullscreen is already set";
            this._checkBodyWidth(), this.viewportManager.addEvent("resize", this._fullscreenResizedCallback), this._fireHideExtras(), this.resetZoom(), this._enterFullscreen(), this._fullscreenResized(this.viewportManager.viewRect), this._currentPageWidth = this.viewportManager.viewRect.width, this.isFullscreen = !0;
            var e = this.documentManager.firstVisiblePage;
            e && this.documentManager.gotoPage(e.pageNum, {pretty: !1}), this.documentManager._fireEnteredFullscreen()
        }, D.prototype.exitFullscreen = function () {
            if (!this.isFullscreen)throw"Fullscreen is not set";
            this.viewportManager.removeEvent("resize", this._fullscreenResizedCallback), this._fireShowExtras(), this.resetZoom(), this._exitFullscreen(), this.isFullscreen = !1;
            var e = this.documentManager.firstVisiblePage;
            e && this.documentManager.gotoPage(e.pageNum, {pretty: !1}), this.documentManager._fireExitedFullscreen()
        }, D.prototype._viewBarWidth = function () {
            return this._extrasHidden ? 0 : this.documentManager.options.extrasWidth
        }, D.prototype._enterFullscreen = function () {
        }, D.prototype._exitFullscreen = function () {
        }, D.prototype._fullscreenResized = function (e) {
        }, D.prototype._scrollWithZoom = function (e, t) {
            window.scrollTo(0, e.top * t)
        }, D.prototype.zoom = function (e) {
            var t = this.viewportManager.viewRect;
            if (!t)return;
            this._currentZoomMultiplier *= e, this._checkBodyWidth(), this._updatePageWidths(), this.documentManager.setIsScrolling(!0), this._scrollWithZoom(t, e), this._zoomed(), this.documentManager.setIsScrolling(!1), this.documentManager._fireZoomed(e)
        }, D.prototype._zoomed = function () {
        }, D.prototype.resetZoom = function () {
            this._currentZoomMultiplier = 1, this._checkBodyWidth(), this._updatePageWidths(), this._zoomed()
        }, D.prototype._checkBodyWidth = function (e) {
            var t = document.documentElement.clientWidth, n = this._zoomedPageWidth() + this._viewBarWidth() + 10;
            globalHeader = document.getElementById("global_header"), n > t ? (document.body.style.width = n + "px", globalHeader && (globalHeader.style.width = t + "px")) : (document.body.style.width = "100%", globalHeader && (globalHeader.style.width = "100%"))
        }, H.prototype = new D, H.prototype._register = function (e, t) {
            this._prepareDisplay()
        }, H.prototype._unregister = function () {
            for (var t in this.documentManager.pages)if (this.documentManager.pages.hasOwnProperty(t)) {
                var n = this.documentManager.pages[t];
                e(n.containerElem).removeClass("book_view"), n.containerElem.style.display = ""
            }
        }, H.prototype._prepareDisplay = function () {
            for (var t in this.documentManager.pages)if (this.documentManager.pages.hasOwnProperty(t)) {
                var n = this.documentManager.pages[t];
                e(n.containerElem).addClass("book_view"), n.containerElem.style.display = "none", n.hide()
            }
            this.documentManager.setPageMissingModulesVisible(!1), this.documentManager.gotoPage(this.documentManager.currentPageNum() || 1)
        }, H.prototype._zoomed = function () {
            var e = this.documentManager.pages[this.currentPageId]
        }, H.prototype._updatePageWidths = function () {
            this.documentManager.setPageWidths(this._zoomedPageWidth() / 2)
        }, H.prototype._pagingStep = function () {
            return 2
        }, H.prototype._showPage = function (e) {
            var t = this.documentManager.pages[e];
            t && (t.isVisible = !0, t.display(!0), t.containerElem.style.display = "")
        }, H.prototype._hidePage = function (e) {
            var t = this.documentManager.pages[e];
            t && (t.isVisible = !1, t.containerElem.style.display = "none", t.hide())
        }, H.prototype._gotoPage = function (e, t) {
            e = parseInt(e, 10), e % 2 == 1 && (e -= 1);
            var n = null, r = null;
            typeof this.currentPageId == "number" && (this._hidePage(this.currentPageId), this._hidePage(this.currentPageId + 1)), n = this.documentManager.pages[e], r = this.documentManager.pages[e + 1];
            if (!n && !r)return;
            this.currentPageId = e, this.isFullscreen && this._setPageBounds(this.viewportManager.viewRect), this._showPage(e), this._showPage(e + 1), this.documentManager.visiblePagesChanged()
        }, H.prototype._setWidth = function (e) {
            this._currentPageWidth = e, this.documentManager.setPageWidths(e)
        }, H.prototype._fullscreenResized = function (e) {
            this._setWidth(e.width - s)
        }, H.prototype._enterFullscreen = function () {
            this._previousPageWidth = this._currentPageWidth || defaultViewWidth, P(this.documentManager.pages[this.currentPageId].containerElem, 300)
        }, H.prototype._exitFullscreen = function () {
            this._setWidth(defaultViewWidth), this._checkBodyWidth()
        }, B.prototype = new D, B.prototype._register = function (e, t) {
            this._prepareDisplay();
            var n = document.getElementById("scroll_preventer");
            n && (n.style.overflow = "hidden", n.style.height = "100%")
        }, B.prototype._unregister = function () {
            var e = document.getElementById("scroll_preventer");
            e && (e.style.overflow = "visible", e.style.height = "auto")
        }, B.prototype._prepareDisplay = function () {
            for (var e in this.documentManager.pages)if (this.documentManager.pages.hasOwnProperty(e)) {
                var t = this.documentManager.pages[e];
                t.containerElem.style.display = "none", t.hide()
            }
            this.documentManager.setPageMissingModulesVisible(!1), this.documentManager.gotoPage(this.documentManager.currentPageNum() || 1)
        }, B.prototype._setPageBounds = function (e) {
            var t = this.documentManager.pages[this.currentPageId];
            t && t.setBounds(e.width, e.height)
        }, B.prototype._gotoPage = function (e, t) {
            if (!this.documentManager.pages.hasOwnProperty(e))return;
            var n;
            this.currentPageId && (n = this.documentManager.pages[this.currentPageId], n && (n.isVisible = !1, n.containerElem.style.display = "none", n.hide())), this.currentPageId = e, n = this.documentManager.pages[e], this.isFullscreen && this._setPageBounds(this.viewportManager.viewRect), n.isVisible = !0, n.display(!0), n.containerElem.style.display = "", n._updateBoundingRect(), this.documentManager.visiblePagesChanged()
        }, B.prototype._setWidth = function (e) {
            this._currentPageWidth = e, this.documentManager.setPageWidths(e)
        }, B.prototype._fullscreenResized = function (e) {
            this._setPageBounds(e)
        }, B.prototype._enterFullscreen = function () {
            this._previousPageWidth = this._currentPageWidth || defaultViewWidth, P(this.documentManager.pages[this.currentPageId].containerElem, 300)
        }, B.prototype._exitFullscreen = function () {
            this._setWidth(defaultViewWidth), this._checkBodyWidth()
        }, j.prototype = new D, j.prototype._updateInViewport = function (e) {
            var t = this.viewportManager.viewRect, n = e.boundingRect, r = e.isVisible;
            return!t || !n ? e.isVisible = !1 : e.isVisible = n.left < t.right && n.right > t.left && n.top < t.bottom && n.bottom > t.top, this.adjacentVisiblePages = 2, r != e.isVisible
        }, j.prototype.checkAndUpdateVisiblePages = function () {
            var e = !1;
            for (var t in this.pages)if (this.pages.hasOwnProperty(t)) {
                var n = this.pages[t], r = this._updateInViewport(n);
                e = e || r
            }
            e ? this.documentManager.visiblePagesChanged() : this.documentManager.scheduleLogPageView(), this._updateDisplayOnPages()
        }, j.prototype._updateDisplayOnPages = function () {
            if (this.documentManager.firstVisiblePage && this.documentManager.lastVisiblePage) {
                var e = this.documentManager.firstVisiblePage.pageNum - this.adjacentVisiblePages, t = this.documentManager.lastVisiblePage.pageNum + this.adjacentVisiblePages;
                for (var n in this.pages)if (this.pages.hasOwnProperty(n)) {
                    var r = this.pages[n];
                    r && (r.pageNum >= e && r.pageNum <= t ? r.display() : (r.hide(), this.documentManager.fireEvent("pageHide", r)))
                }
            }
        }, j.prototype._zoomed = function () {
            this.checkAndUpdateVisiblePages()
        }, j.prototype._register = function (e, t) {
            this.pages = e.pages, this.viewportManager.addEvent("vertical", this._verticalPositionChangeCallback);
            for (var n in this.pages)if (this.pages.hasOwnProperty(n)) {
                var r = this.documentManager.pages[n];
                r.containerElem.style.display = ""
            }
            this.documentManager.setPageMissingModulesVisible(!0), this.documentManager._updatePageBoundingRects(), this.checkAndUpdateVisiblePages()
        }, j.prototype.isTopPageInView = function () {
            var e = this.documentManager.firstVisiblePage;
            return e ? this.documentManager._expectedFirstPageNum > e.pageNum ? !0 : e.boundingRect.top + 5 >= this.viewportManager.viewRect.top : !0
        }, j.prototype._unregister = function () {
            this.viewportManager.removeEvent("vertical", this._verticalPositionChangeCallback), delete this.pages
        }, j.prototype._gotoPage = function (t, n) {
            typeof e == "undefined" && typeof scribd != "undefined" && F.setJQuery(scribd.jQuery);
            if (!this.pages.hasOwnProperty(t))return;
            var r = this.documentManager.pages[t];
            this.documentManager.setIsScrolling(!0);
            var i = typeof e != "undefined" && typeof e.scrollTo != "undefined";
            if (n.pretty && i) {
                var s = r.boundingRect;
                if (n.frac) {
                    var o = r.boundingRect.top + Math.floor(r.boundingRect.height * n.frac);
                    s = e.extend({}, r.boundingRect, {top: o})
                }
                n.offset && (s.top -= n.offset), e.scrollTo(s, {duration: 600, onAfter: this._afterGotoPage})
            } else r._updateBoundingRect(), scrollTo(r.boundingRect.left, r.boundingRect.top - (n.offset || 0)), this._afterGotoPage();
            return r.pageNum
        }, j.prototype._gotoNextPage = function () {
            if (!this.documentManager.firstVisiblePage)return;
            this.documentManager.gotoPage(this.documentManager.firstVisiblePage.pageNum + 1, {direction: 1})
        }, j.prototype._gotoPreviousPage = function () {
            if (!this.documentManager.firstVisiblePage)return;
            this.documentManager.gotoPage(this.documentManager.firstVisiblePage.pageNum - 1, {direction: -1})
        }, j.prototype._setWidth = function (e) {
            var t = this.pagePosition();
            this._currentPageWidth = e, this._updatePageWidths(), this._checkBodyWidth(), this.restorePosition(t)
        }, j.prototype._fullscreenResized = function () {
            this._setWidth((window.innerWidth || document.documentElement.clientWidth) - s)
        }, j.prototype._enterFullscreen = function () {
            this._previousPageWidth = this._currentWidth || defaultViewWidth
        }, j.prototype._exitFullscreen = function () {
            this._setWidth(this._previousPageWidth)
        }, j.prototype.pagePosition = function () {
            var t = this.documentManager.currentPageNum(), n = e("#outer_page_" + t), r = (e(window).scrollTop() - n.offset().top) / n.height();
            return[t, r]
        }, j.prototype.restorePosition = function (t) {
            var n = e(this.pages[t[0]].containerElem);
            this._gotoPage(t[0], {offset: -Math.floor(t[1] * n.height())})
        }, j.prototype.zoom = function (e) {
            var t = this.viewportManager.viewRect;
            if (!t)return;
            var n = this.pagePosition();
            this._currentZoomMultiplier *= e, this._checkBodyWidth(), this._updatePageWidths(), this.restorePosition(n), this._zoomed(), this.documentManager._fireZoomed(e)
        }, p(F, ["expectedFirstPageChanged", "viewmodeChanged", "enteredFullscreen", "exitedFullscreen", "hideExtras", "showExtras", "zoomed", "pageHide", "pageLoaded", "allPagesAdded", "pageView", "viewmodeInitialized"]), F.prototype._fireZoomed = function (e) {
            this.fireEvent("zoomed", e)
        }, F.prototype.nextFontAggregatorHost = function () {
            return this._currentFontAggregatorHostIdx = (this._currentFontAggregatorHostIdx + 1) % this.fontAggregatorHosts.length, this.fontAggregatorHosts[this._currentFontAggregatorHostIdx]
        }, F.prototype._fireHideExtras = function () {
            this.fireEvent("hideExtras")
        }, F.prototype._fireShowExtras = function () {
            this.fireEvent("showExtras")
        }, F.prototype.currentPageNum = function () {
            return this._expectedFirstPageNum
        }, F.prototype.setupTestElements = function () {
            this._fontLoader.setupTestElements()
        }, F.prototype.pageCount = function () {
            if (this.hasOwnProperty("_pageCount"))return this._pageCount;
            var e = 0;
            for (var t in this.pages)this.pages.hasOwnProperty(t) && e++;
            return this._pageCount = e, this._pageCount
        }, F.prototype.setupPaidDocument = function (e, t) {
            this.allowedPages = e, this.originalPageCount = t, this._allowedPagesHash = [], this._maximumAllowedPage = Math.
                max.apply(null, this.allowedPages), this._minimumAllowedPage = Math.min.apply(null, this.allowedPages), this._isPaidDocument = !0, this._pageMissingElements = [];
            for (var n = 0; n < e.length; n++)this._allowedPagesHash[e[n]] = !0, (n > 0 && e[n] > e[n - 1] + 1 || n === 0 && e[n] != 1) && this._pageMissingElements.push("page_missing_explanation_" + e[n].toString());
            this._maximumAllowedPage != this.originalPageCount && this._pageMissingElements.push("page_missing_explanation_" + (this.originalPageCount + 1).toString())
        }, F.prototype.minimumPageNumber = function () {
            return this.viewMode() == "book" ? 0 : 1
        }, F.prototype.maximumPageNumber = function () {
            return this.allowedPages ? this.originalPageCount : this.pageCount()
        }, F.prototype.getClosestPageNumber = function (e, t) {
            if (!this.allowedPages)return e;
            if (this._allowedPagesHash[e])return e;
            if (e >= this._maximumAllowedPage)return this._maximumAllowedPage;
            if (e <= this._minimumAllowedPage)return this._minimumAllowedPage;
            for (var n = 1; n <= this.originalPageCount; n++) {
                if (t <= 0 && this._allowedPagesHash[e - n] === !0)return e - n;
                if (t >= 0 && e + n < this.originalPageCount && this._allowedPagesHash[e + n] === !0)return e + n
            }
        }, F.prototype.setPageMissingModulesVisible = function (t) {
            typeof e == "undefined" && typeof scribd != "undefined" && F.setJQuery(scribd.jQuery);
            if (!this._isPaidDocument)return;
            for (var n = 0; n < this._pageMissingElements.length; n++) {
                var r = e(this._pageMissingElements[n]);
                r && (t ? r.show() : r.hide())
            }
        }, F.prototype.getNextAvailablePage = function (e) {
            return getClosestPageNumber(e, 1)
        }, F.prototype.getPreviousAvailablePage = function (e) {
            return getClosestPageNumber(e, -1)
        }, F.prototype.flushFontQueue = function () {
            this._fontLoader.flushFontQueue()
        }, F.prototype.visiblePagesChanged = function () {
            var e = [];
            for (var t in this.pages)if (this.pages.hasOwnProperty(t)) {
                var n = this.pages[t];
                n.isVisible && e.push(n)
            }
            e.sort(function (e, t) {
                return e.pageNum < t.pageNum ? -1 : 1
            }), this.visiblePages = e;
            var r = this.firstVisiblePage;
            this.firstVisiblePage = e.length > 0 ? e[0] : null, this.lastVisiblePage = e.length > 0 ? e[e.length - 1] : null, this.isScrolling || (this._loadAdjacentFonts(), this._loadAdjacentPages()), (this.firstVisiblePage !== r && (!this.firstVisiblePage || !r) || this.firstVisiblePage.pageNum != r.pageNum) && !this.isScrolling && this.firstVisiblePage && this._updateExpectedFirstPage(this.firstVisiblePage.pageNum), this.scheduleLogPageView()
        }, F.prototype.boundingRatioForPage = function (e) {
            var t = this.viewportManager.viewRect, n = e.boundingRect, r = n.bottom - n.top, i = n.right - n.left;
            return{left: (t.left - n.left) / i, right: (t.right - n.right) / i + 1, top: I((t.top - n.top) / r + e.pageNum, e.pageNum, e.pageNum + 1), bottom: I((t.bottom - n.bottom) / r + e.pageNum + 1, e.pageNum, e.pageNum + 1)}
        }, F.prototype.scheduleLogPageView = function () {
            this.logPageViewTimout && window.clearTimeout(this.logPageViewTimout);
            var e = this;
            this.logPageViewTimout = window.setTimeout(function () {
                e.logPageView(), e.logPageViewTimout = null
            }, 1e3)
        }, F.prototype.getVisibleBBox = function () {
            var e = {};
            if (this.firstVisiblePage) {
                var t = this.boundingRatioForPage(this.firstVisiblePage);
                e.left = t.left, e.right = t.right, e.top = t.top
            }
            if (this.lastVisiblePage) {
                var n = this.boundingRatioForPage(this.lastVisiblePage);
                e.bottom = n.bottom
            }
            return e
        }, F.prototype.logPageView = function () {
            var e = this.getVisibleBBox();
            for (var t in e)e.hasOwnProperty(t) && (e[t] = q(e[t]));
            var n = window.$rat;
            if (n) {
                var r;
                window.RAT_API_VERSION == "2" ? r = [
                    [e.left, e.top],
                    [e.right, e.bottom]
                ] : r = "(" + e.left + " " + e.top + ") (" + e.right + " " + e.bottom + ")", n("fourgen.viewchange", r)
            }
            return this.fireEvent("pageView"), e
        }, F.prototype._updateExpectedFirstPage = function (e) {
            this._expectedFirstPageNum = e, this.fireEvent("expectedFirstPageChanged", this._expectedFirstPageNum)
        }, F.prototype._loadAdjacentFonts = function () {
            if (this.firstVisiblePage && this.lastVisiblePage) {
                var e = [], t = this.firstVisiblePage.pageNum - r, n = this.lastVisiblePage.pageNum + r;
                for (var i = t; i <= n; i++) {
                    var s = this.pages[i];
                    s && s.loadFonts()
                }
            }
        }, F.prototype._loadAdjacentPages = function () {
            if (this.firstVisiblePage && this.lastVisiblePage) {
                var e = [], t = this.firstVisiblePage.pageNum - n, r = this.lastVisiblePage.pageNum + n;
                for (var i = t; i <= r; i++) {
                    var s = this.pages[i];
                    s && !s.loadHasStarted && (s.load(), this.mobile && s.setWidth(this._pageWidths))
                }
                this.mobile && this._removeUnusedPages()
            }
        }, F.prototype._removeUnusedPages = function () {
            var e = this.firstVisiblePage.pageNum - n, t = this.lastVisiblePage.pageNum + n, r = this.pages, i = n + 1;
            while (r[i])(i < e || i > t) && r[i].remove(), i++
        }, F.prototype._updatePageBoundingRects = function () {
            for (var e in this.pages)this.pages.hasOwnProperty(e) && this.pages[e]._updateBoundingRect()
        }, F.prototype.removeInsteadOfBlurring = function () {
            return/iPhone|iPod|iPad|Android|BlackBerry/.test(navigator.userAgent) ? !0 : typeof Scribd == "undefined" || !Scribd.test_choice_value || typeof e == "undefined" ? !0 : Scribd.test_choice_value("blur_test") !== "test"
        }, F.prototype.addPage = function (e) {
            if (!e.blur || !this.removeInsteadOfBlurring()) {
                if (e.pageNum === undefined)throw"must have pageNum param";
                e.docManager = this;
                var t = new O(e);
                return this.pages[e.pageNum] = t, this._pageWidths && t.setWidth(this._pageWidths), this.currentFontGroup.isFull() && (this.currentFontGroup = this.currentFontGroup.newNextGroup()), t.setLoadFontGroup(this.currentFontGroup), t
            }
            e.containerElem.parentNode.removeChild(e.containerElem)
        }, F.prototype.setIsScrolling = function (e) {
            e ? this._scrollingCount += 1 : this._scrollingCount -= 1, this._scrollingCount < 0 && (this._scrollingCount = 0), this._scrollingCount === 0 ? this.isScrolling = !1 : this.isScrolling = !0
        }, F.prototype.setViewManager = function (e, t) {
            if (this._currentViewManager)this._setViewManager(e), typeof t == "function" && t(); else {
                var n = this;
                this.addEvent("viewmodeInitialized", function () {
                    n.setViewManager(e, t)
                })
            }
        }, F.prototype._setViewManager = function (e, t) {
            t || this._currentViewManager.unregister();
            var n = this._currentViewManager;
            this._currentViewManager = this.viewManagers[e], this._currentViewManager.register(this, this.viewportManager), this.fireEvent("viewmodeChanged", this.viewMode(), n ? n.name() : null)
        }, F.prototype.setInitialViewManager = function (e) {
            if (this._currentViewManager)throw"This should be called before any view manager exists";
            this._setViewManager(e, !0), this.fireEvent("viewmodeInitialized", this.viewMode(), null)
        }, F.prototype.setDefaultWidth = function (e) {
            this._currentViewManager._currentPageWidth = e, this._currentViewManager._currentZoomMultiplier = 1, this._currentViewManager._updatePageWidths()
        }, F.prototype.subImageSrc = function (e) {
            var t, n = 0;
            for (t = 0; t < e.length; t++)n += e.charCodeAt(t);
            var r = this._imageDomainSubstitutionList[n % this._imageDomainSubstitutionList.length];
            return e.replace(this._imageDomainSubstitutionFrom, r)
        }, F.prototype.allPagesAdded = function () {
            if (this._allPagesAddedCalled)throw"can only call allPagesAdded once";
            this.viewportManager.enable(), this._updatePageBoundingRects(), this.setInitialViewManager(this.defaultViewMode), this.fireEvent("allPagesAdded")
        }, F.prototype.setEmbeddedDoc = function (e) {
            this._isEmbed = e === "True"
        }, F.prototype.setPageWidths = function (e) {
            this._pageWidths = e;
            for (var t in this.pages)this.pages.hasOwnProperty(t) && this.pages[t].setWidth(this._pageWidths);
            this._updatePageBoundingRects()
        }, F.prototype.addFont = function (e, t, n, r, i, s) {
            this._fontLoader.addFont(e, t, n, r, i, s)
        }, F.prototype.setNumFonts = function (e) {
            this._fontLoader.setNumFonts(e)
        }, F.prototype.initStyles = function () {
            this._fontLoader.initStyles()
        }, F.prototype.gotoPage = function (e, t) {
            var n = !g(e), r = null;
            n && (r = +(e % 1).toFixed(2), e = Math.floor(e));
            if (e < this.minimumPageNumber() || e > this.maximumPageNumber())return;
            t || (t = {}), t.pretty === undefined && (t.pretty = !0), n && t.frac == undefined && (t.frac = r);
            var i = t.direction || 0, s = this.pages[e];
            s === undefined && this._isPaidDocument && (e = this.getClosestPageNumber(e, i), s = this.pages[e]), this.mobile && this.pages[e] && this.pages[e].setWidth(this._pageWidths), this._updateExpectedFirstPage(e), this._currentViewManager.gotoPage(this._expectedFirstPageNum, t)
        }, F.prototype.gotoPreviousPage = function () {
            var e = this._currentViewManager._pagingStep();
            this._currentViewManager.isTopPageInView() ? this.gotoPage(this._expectedFirstPageNum - e, {direction: -1}) : this.gotoPage(this._expectedFirstPageNum, {direction: -1})
        }, F.prototype.gotoNextPage = function () {
            var e = this._currentViewManager._pagingStep();
            this.gotoPage(this._expectedFirstPageNum + e, {direction: 1})
        }, F.prototype.enterFullscreen = function () {
            this._currentViewManager.enterFullscreen()
        }, F.prototype.exitFullscreen = function () {
            this._currentViewManager.exitFullscreen()
        }, F.prototype.isFullscreen = function () {
            return this._currentViewManager.isFullscreen
        }, F.prototype._fireEnteredFullscreen = function () {
            this.fireEvent("enteredFullscreen")
        }, F.prototype._fireExitedFullscreen = function () {
            this.fireEvent("exitedFullscreen")
        }, F.prototype.viewMode = function () {
            return this._currentViewManager ? this._currentViewManager.name() : null
        }, F.prototype.zoom = function (e) {
            this._currentViewManager.zoom(e)
        }, F.prototype.resetZoom = function () {
            this._currentViewManager.resetZoom()
        }, F.prototype.setImageDomainSubstitution = function (e, t) {
            this._imageDomainSubstitutionFrom = e, this._imageDomainSubstitutionList = t
        }, F.prototype.disableViewManagerResizeWidth = function () {
            D.prototype._checkBodyWidth = function () {
            }
        }, F.prototype.disable = function () {
            this.disabled || (this.disabled = !0, this.viewportManager.disable())
        }, F.prototype.doDynamicBlurring = function (t) {
            var n, r, i;
            this.removeInsteadOfBlurring() || (n = e("#" + t), n.on("dragstart, selectstart, contextmenu", function (e) {
                e.preventDefault()
            }), n.find("*").andSelf().attr("unselectable", "on"), i = Modernizr && Modernizr.textshadow && e.browser && (e.browser.webkit && !e.browser.safari || e.browser.mozilla || e.browser.opera) && !(e.browser.mozilla === !0 && parseFloat(e.browser.version) < 4 && parseFloat(e.browser.version) >= 3), e.browser || console.log("Hi there, it seems as though you've upgraded jQuery and browser detection is no longer working in 4gen.js"), e(document).ready(function () {
                r = e("#grab_blur_promo_here").contents().clone().appendTo("#" + t).css("zIndex", 8).show(), r.find(".gap").html("Page " + t.split("_").pop() + " is not shown in this preview."), i || r.find(".page-blur-promo-overlay").height("100%").width("100%").css("zIndex", 7).show()
            }), i && (n.find(".text_layer").css({color: "transparent", "text-shadow": "0px 0px 70px black"}), n.find(".image_layer img").each(function () {
                var t = this;
                e(t).imagesLoaded(function () {
                    t.height > 250 && t.width > 250 && e(t).css({opacity: .04})
                })
            })))
        }, F.setJQuery = function (t) {
            e = t
        }, F.ScrollViewManager = j, F.SlideViewManager = B, F.BookViewManager = H, F
    }();
    window.DocumentManager = h
}, window.DocumentManager === undefined && init_4gen();


/* public/javascripts/reflow_logic.js @ 1384554040 */
function TextPosition(e, t, n) {
    this.chapter_idx = e || 0, this.block_idx = t || 0, this.word_idx = n || 0, this.done = !1, this.chapter = book.chapters[this.chapter_idx], this.block = this.chapter.blocks[this.block_idx], this.is_first_word = function () {
        return this.word_idx == 0
    }, this.is_book_start = function () {
        return this.chapter_idx === 0 && this.block_idx === 0 && this.word_idx === 0
    }, this.next_chapter = function () {
        this.chapter_idx++, this.chapter_idx == book.chapters.length && (this.done = !0), this.chapter = book.chapters[this.chapter_idx]
    }, this.next_block = function () {
        this.block_idx++, this.block_idx == this.chapter.blocks.length && (this.block_idx = 0, this.next_chapter()), this.chapter !== undefined ? this.block = this.chapter.blocks[this.block_idx] : this.block = undefined
    }, this.next_word = function () {
        if (this.block.words !== undefined) {
            this.word_idx++;
            var e = this.block.words.length;
            this.word_idx >= e && (this.word_idx = 0, this.next_block())
        } else this.word_idx = 0, this.next_block()
    }, this.go_backward = function () {
        var e = this.clone();
        return e.next_chapter = function () {
            this.chapter_idx--, this.chapter_idx < 0 && (this.chapter_idx = 0, this.done = !0), this.chapter = book.chapters[this.chapter_idx]
        }, e.next_block = function () {
            if (this.block_idx == 0) {
                if (this.chapter_idx == 0) {
                    this.done = !0;
                    return
                }
                this.next_chapter(), this.chapter !== undefined ? (this.block_idx = this.chapter.blocks.length - 1, this.block = this.chapter.blocks[this.block_idx]) : this.block = undefined
            } else this.block_idx--, this.block = this.chapter.blocks[this.block_idx];
            this.block !== undefined && this.block.words !== undefined && (this.word_idx = this.block.words.length - 1)
        }, e.next_word = function () {
            this.word_idx == 0 ? this.next_block() : this.word_idx--
        }, e.is_first_word = function () {
            return this.block !== undefined && this.block.words !== undefined && this.word_idx == this.block.words.length - 1
        }, e.backwards = !0, e
    }, this.cancel_backward = function () {
        var e = this.clone();
        return e
    }, this.preceeds_position = function (e) {
        if (e.chapter_idx > this.chapter_idx)return!0;
        if (e.chapter_idx == this.chapter_idx)if (e.block_idx > this.block_idx || e.block_idx == this.block_idx && e.word_idx > this.word_idx)return!0;
        return!1
    }, this.words_until_end_of_block = function () {
        return this.block.words ? this.block.words.length - this.word_idx : TextPosition.count_words_in_block(this.block)
    }, this.words_until_end_of_chapter = function () {
        var e = this.words_until_end_of_block(), t;
        for (var n = this.block_idx + 1, r = this.chapter.blocks.length; n < r; n++)t = this.chapter.blocks[n], e += TextPosition.count_words_in_block(t);
        return e
    }, this.words_until_position = function (e) {
        if (e.preceeds_position(this))return-1 * e.words_until_position(this);
        var t = this.chapter_idx, n = t == e.chapter_idx, r = n && this.block_idx == e.block_idx;
        if (r)return e.word_idx - this.word_idx;
        var i = 0, s;
        for (var o = t + 1; o <= e.chapter_idx; o++)if (window.reflow_toc)i += reflow_toc[o].words_count; else {
            s = book.chapters[o];
            for (var u = 0, a = s.blocks.length; u < a; u++)i += TextPosition.count_words_in_block(s.blocks[u])
        }
        return i += this.words_until_end_of_chapter() - e.words_until_end_of_chapter(), i
    }
}
function SectionizedReflow(e) {
    function n(e, t, n, r) {
        this.j = e, this.lineHeight = t, this.font = n, this.width = r, this.composite = typeof words[e] == "object"
    }

    function s(e) {
        r && window.console !== undefined && window.console !== null && window.console.log(e)
    }

    function o(e, t, n, r, i, s, o) {
        var u = o.column_pad_left, a = o.column_pad_right, f = o.column_pad_top, l = o.column_pad_bottom;
        i == 2 && (r == 0 ? a = o.column_distance / 2 : u = o.column_distance / 2), o.pad_columns || (u = 0, a = 0, f = 0, l = 0), isIe() || (u /= s, a /= s, f /= s, l /= s), this.y = 0, this.offset_x = u, this.offset_y = f, this.currentLineHeight = 0, isIe() && (t *= s, n *= s), this.width = t - (u + a), this.height = n - (f + l), this.html = e
    }

    function u(e) {
        this.size = e, this.leading = e * 1.5, this.smaller = undefined, this.larger = undefined, this.size_of_space_glyph = 512 * e / 1024, this.spaceWidth = e / 3
    }

    function a() {
    }

    function f() {
        for (var e = 0; e < book.chapters.length; e++) {
            var t = book.chapters[e];
            for (var n = 0; n < t.blocks.length; n++) {
                var r = t.blocks[n];
                if (r.type == "preview_message")return!0
            }
        }
        return!1
    }

    function l(e, t, n, r) {
        window.$rat && (window.$rat(e, [t, n]), window.$rat(e + "2", {positions: [t, n], word_count: r}))
    }

    function c(e) {
        Scribd.ReadingProgress != undefined && (new Scribd.ReadingProgress({scroll_watch: !1})).ping_reading_progress(e)
    }

    var t = 800;
    this.pad_columns = !0, this.column_pad_left = 90, this.column_pad_right = 90, this.column_pad_top = 102, this.column_pad_bottom = 130, this.column_distance = 90, this.font_size_constants = {step: .1, min: .1, max: 2, "default": .5}, this.rat_key = "epub_read_fullscreen", this.should_track_read = !0;
    var r = !0;
    o.prototype = {advanceY: function (e) {
        this.y += e, this.y >= this.height && (this.done = !0)
    }}, u.prototype = {get_widths: function () {
        return fontsize_to_wordwidths[this.size]
    }}, a.prototype = {push: Array.prototype.push, isComplex: function () {
        var e, t = this[0].lineHeight;
        for (var n = 0; n < this.length; n++) {
            var r = this[n];
            if (r.composite)return!0;
            if (r.lineHeight != t)return!0;
            if (window.word_metadata !== undefined && word_metadata[r.j] != null)if (word_metadata[r.j].href !== undefined || word_metadata[r.j].underline)return!0
        }
        e = styles[this[0].j];
        for (var n = 0; n < this.length; n++) {
            var r = this[n];
            if (styles[r.j] != e)return!0
        }
        return!1
    }}, this.construct = function () {
        this.parse_fontsizes(), this.page = new TextPosition(0), this.next_page = null, this.font_size = this.fontsizes[2], this.font = this.fontsize_to_font[this.font_size], this.render = !0, this.columns = [], this.num_columns = 0, this.scale = this.font_size_constants["default"], this.redraw_callbacks = [], this.font_style_load_callbacks = [], window.$rat && (this.track_reads_after_x_ms = 1e3)
    }, this.last_book_location = function () {
        var e = book.chapters, t = e[e.length - 1], n = t.blocks, r = n[n.length - 1], i = 0;
        r.words && (words = r.words.length - 1);
        var s = new TextPosition(e.length - 1, n.length - 1, i);
        return s.done = !0, s
    }, this.resize_tables = function () {
        var e = [];
        for (var t = 0; t < book.chapters.length; t++) {
            var n = book.chapters[t];
            for (var r = 0; r < n.blocks.length; r++) {
                var i = n.blocks[r];
                i.type == "raw" && !i.resizeable && e.push(i)
            }
        }
        if (e.length > 0) {
            var s = function (e, t) {
                var n = 0;
                return function () {
                    n++, n == e && (t.ready_pages(), t.gotoPage(t.page))
                }
            }(e.length, this);
            for (var t = 0, o = e.length; t < o; t++)this.ask_browser_for_table_size(e[t], s)
        }
        return!1
    }, this.ask_browser_for_table_size = function (e, n) {
        (function (e) {
            body = document.getElementsByTagName("body")[0];
            if (e.size_timeout_id === undefined) {
                var r = document.createElement("div");
                r.style.position = "absolute", r.style.width = t + "px", r.style.top = "0px", r.style.left = "0px", r.style.fontFamily = "serif", r.style.fontSize = "24px", r.style.zIndex = "-9999", r.innerHTML = e.data, body.appendChild(r), e.size_div = r, e.size_timeout_id = window.setTimeout(function () {
                    e.width = r.offsetWidth, e.height = r.offsetHeight, e.resizeable = !0, body.removeChild(r), e.size_timeout_id = undefined, n()
                }, 1e3)
            }
        })(e)
    }, this.set_page_size = function (e, t, n, r) {
        r === undefined && (this.scale === undefined ? r = this.font_size_constants["default"] : r = this.scale), this.scale = r, this.column_width = Math.ceil(e / r), this.column_height = Math.ceil(t / r), this.num_columns = n || 1, this.resize_tables()
    }, this.set_columns = function (e) {
        this.columns = e, this.set_page_size(e[0].offsetWidth, e[0].offsetHeight, e.length)
    }, this.get_position = function () {
        return this.page
    }, this.total_blocks_in_book = function () {
        var e = 0;
        for (var t = 0, n = book.chapters.length; t < n; t++)e += book.chapters[t].blocks.length;
        return e
    }, this.start_position_to_track = function () {
        return this.page.fractional_position()
    }, this.end_position_to_track = function () {
        return this.next_page ? this.next_page.fractional_position() : this.total_blocks_in_book()
    }, this.num_words_visible = function () {
        var e = this.next_page || this.last_book_location(), t = this.page.words_until_position(e);
        return e.done && (t += 1), t
    }, this.track_read = function () {
        var e = this.start_position_to_track(), t = this.end_position_to_track();
        e !== undefined && t !== undefined && (l(this.rat_key, e, t, this.num_words_visible()), c(e))
    }, this.fill_toc = function (e) {
        e.innerHTML = "<h1>Sections</h1>";
        for (var t = 0; t < book.chapters.length; t++) {
            var n = book.chapters[t].title, r = document.createElement("div");
            r.onmouseup = function (e, t) {
                return function () {
                    e.gotoChapter(t)
                }
            }(this, t), r.className = "toc_item", r.appendChild(document.createTextNode(n)), e.appendChild(r)
        }
        if (window.reflow_toc === undefined)return;
        for (; t < reflow_toc.length; t++) {
            var n = reflow_toc[t].title, r = document.createElement("div");
            r.className = "missing", r.title = "This chapter is not part of the preview version", r.appendChild(document.createTextNode(n)), e.appendChild(r)
        }
    }, this.get_toc = function () {
        var e = [];
        for (var t = 0; t < book.chapters.length; t++)e.push([t, book.chapters[t].title]);
        return e
    }, this.fill_fontsizeselector = function (e) {
        e.innerHTML = "";
        var t = this.get_fontsizes();
        for (var n = 0; n < t.length; n++) {
            var r = document.createElement("a");
            r.href = "javascript:reflowUI.reflow.set_fontsize(" + t[n] + ");", r.appendChild(document.createTextNode(t[n])), e.appendChild(r), e.appendChild(document.createElement("br"))
        }
    }, this.parse_fontsizes = function () {
        var e = [], t = [], n = this.font_size_constants["default"];
        for (var r in fontsize_to_wordwidths)e[e.length] = parseInt(r), t.push(n), n += .1;
        e.sort(), this.fontsizes = e, this.draw_scales = t, this.fontsize_to_font = {};
        for (var i = 0; i < e.length; i++) {
            var r = e[i], s = new u(r);
            this.fontsize_to_font[r] = s
        }
        var o = undefined;
        for (var i = 0; i < e.length; i++) {
            var r = e[i], s = this.fontsize_to_font[r];
            o ? (o.larger = s, s.smaller = o) : s.smaller = s, o = s
        }
        o.larger = o
    }, this.get_fontsizes = function () {
        return this.fontsizes
    }, this.get_fontstyles = function () {
        if (typeof flavors == "undefined")return["default"];
        var e = [];
        for (i in flavors)e[i] = flavors[i].name;
        return e
    }, this.set_font = function (e) {
        e >= this.fontsizes.length && (e = this.fontsizes.length - 1), this.set_fontsize(this.fontsizes[e])
    }, this.get_fontsize = function () {
        for (var e = 0, t = this.draw_scales.length; e < t; e++)if (this.draw_scales[e] == this.scale)return this.fontsizes[e];
        return this.font_size
    }, this.set_fontsize = function (e) {
        var t = null;
        for (var n = 0, r = this.fontsizes.length; n < r; n++)if (this.fontsizes[n] == e) {
            t = n;
            break
        }
        if (t === null)return;
        this.set_scale(this.draw_scales[t])
    }, this.get_fontstyle = function () {
        return active_flavor
    }, this.set_fontstyle = function (e) {
        var t = this;
        if (typeof flavors == "undefined")return;
        for (i in flavors) {
            var n = flavors[i];
            if (n.name != e)continue;
            var r = n.url, s = document.getElementById("epub_scripts"), o = document.createElement("script");
            o.async = !0, o.type = "text/javascript", o.src = r, s.appendChild(o), o.onload = o.onreadystatechange = function () {
                if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")o.onload = o.onreadystatechange = null, o && o.parentNode && o.parentNode.removeChild(o), t.font_style_loaded()
            };
            break
        }
    }, this.font_style_loaded = function () {
        for (var e = 0, t = this.font_style_load_callbacks.length; e < t; e++)this.font_style_load_callbacks[e](this)
    }, this.set_scale = function (e) {
        this.scale = e, this.columns && this.columns.length && this.set_columns(this.columns), this.redraw()
    }, this.reset_scale = function () {
        return this.set_scale(this.font_size_constants["default"]), !0
    }, this.at_default_scale = function () {
        return this.scale == this.font_size_constants["default"]
    }, this.update_scale = function (e) {
        var t = this.scale + e;
        return t >= this.font_size_constants.min && t <= this.font_size_constants.max ? (this.set_scale(t), !0) : !1
    }, this.decrease_scale = function () {
        return this.update_scale(-this.font_size_constants.step)
    }, this.increase_scale = function () {
        return this.update_scale(this.font_size_constants.step)
    }, this.can_increase_scale = function () {
        var e = this.font_size_constants;
        return this.scale + e.step <= e.max
    }, this.can_decrease_scale = function () {
        var e = this.font_size_constants;
        return this.scale - e.step >= e.min
    }, this.draw_one_column = function (e, t, n, r) {
        var i = n.clone();
        this.render && (e = this.clear_column(e));
        var s = new o(e, this.column_width, this.column_height, t, this.num_columns, this.scale, this);
        this.debug && this.render && this.draw_boundaries(s);
        while (!s.done && !n.done && (!r || n.preceeds_position(r)))n.block.type == "text" ? this.renderText(s, n, r) : n.block.type == "image" ? this.renderImage(s, n) : n.block.type == "raw" ? this.renderRaw(s, n) : n.block.type == "spacer" ? this.renderSpacer(s, n) : n.block.type == "page_break" ? this.renderPageBreak(s, n) : n.block.type == "hr" ? this.renderHorizontalRuler(s, n) : n.block.type == "preview_message" ? this.renderPreviewMessage(s, n) : n.next_block();
        return r && this.render && this.treat_position_like_chapter_start(i) && r.chapter_idx == i.chapter_idx && (!r.chapter || r.block_idx != 0 && r.chapter.blocks[r.block_idx - 1].type != "page_break") && s.y < s.height && (s.html.style.marginTop = this.scale * (s.height - s.y) + "px"), s
    }, this.redraw = function () {
        if (this.render && this.should_track_read && this.track_reads_after_x_ms) {
            this.read_timeout !== undefined && window.clearTimeout(this.read_timeout);
            var e = this;
            this.read_timeout = window.setTimeout(function () {
                e.track_read()
            }, this.track_reads_after_x_ms)
        }
        s("redraw at " + this.page.toString()), this.next_page = this.draw_all_columns(this.page.clone());
        for (var t = 0; t < this.redraw_callbacks.length; t++)this.redraw_callbacks[t](this)
    }, this.register_font_style_load_callback = function (e) {
        this.font_style_load_callbacks.push(e)
    }, this.register_redraw_callback = function (e) {
        this.redraw_callbacks.push(e)
    }, this.draw_boundaries = function (e) {
        var t = document.createElement("div");
        t.style.position = "absolute", t.style.left = e.offset_x + "px", t.style.top = e.offset_y + "px", t.style.width = e.width + "px", t.style.height = e.height + "px", t.style.border = "1px solid black", e.html.appendChild(t)
    }, this.clear_column = function (e) {
        e.innerHTML = "";
        var t = this.scale, n = document.createElement("div");
        n.style.position = "absolute", n.style.left = "0", n.style.top = "0", n.style.width = this.column_width + "px", n.style.height = this.column_height + "px";
        var r = document.documentElement.style;
        if (r.WebkitTransform !== undefined)n.style.WebkitTransform = "scale(" + t + ")", n.style.WebkitTransformOrigin = "top left"; else if (r.MozTransform !== undefined)n.style.MozTransform = "scale(" + t + ")", n.style.MozTransformOrigin = "top left"; else if (r.OTransform !== undefined)n.style.OTransform = "scale(" + t + ")", n.style.OTransformOrigin = "top left"; else if (r.msTransform !== undefined)n.style.msTransform = "scale(" + t + ")", n.style.msTransformOrigin = "top left"; else {
            var i = document.createElement("div");
            i.style.position = "relative", i.style.left = "0", i.style.top = "0", i.style.zoom = t, i.appendChild(n), n = i
        }
        return e.appendChild(n), n
    }, this.draw_all_columns = function (e, t) {
        t === undefined && (t = this.columns);
        if (t.length == 0)return;
        for (var n = 0; n < this.num_columns; n++) {
            var r = null;
            this.render && (r = t[n]), this.draw_one_column(r, n, e)
        }
        return e
    }, this.clear_columns = function (e) {
        var t;
        for (t = 0; t < e.length; t++)e[t].innerHTML = ""
    }, this.draw_prev_page = function (e) {
        if (this.page.is_start()) {
            this.clear_columns(e);
            return
        }
        var t = this.scan_backward();
        this.draw_all_columns(t, e)
    }, this.draw_next_page = function (e) {
        this.draw_all_columns(this.next_page.clone(), e)
    }, this.fillPages = function () {
        s("re-running fillpages. Warning, this is a slow operation"), this.render = !1;
        var e = new TextPosition, t = new Array;
        while (!e.done) {
            t[t.length] = e.clone();
            for (var n = 0; n < this.num_columns; n++)this.draw_one_column(null, n, e)
        }
        return this.render = !0, this.numPages = t.length, t
    }, this.gotoChapter = function (e) {
        this.gotoPage(new TextPosition(e))
    }, this.block_to_position = function (e) {
        var t = 0, n = 0, r = e;
        for (var i = 0; i < book.chapters.length; i++) {
            if (!(book.chapters[i].blocks.length <= r)) {
                t = r, n = i;
                break
            }
            r -= book.chapters[i].blocks.length
        }
        return new TextPosition(n, t)
    }, this.gotoBlock = function (e) {
        this.gotoPage(this.block_to_position(e))
    }, this.scan_backward = function (e, t) {
        var n = this.render;
        n && (this.render = !1), e = (e || this.page).go_backward(), t = t || this.num_columns, e.next_word();
        for (var r = 0; r < t; r++)this.draw_one_column(null, r, e);
        var i;
        return e.done ? i = new TextPosition : (i = e.cancel_backward(), i.next_word()), this.render = n, i
    }, this.has_prev_page = function () {
        return this.page.is_start() ? !1 : !0
    }, this.has_next_page = function () {
        return this.next_page.done ? !1 : !0
    }, this.prevPage = function () {
        return this.has_prev_page() ? (this.gotoPage(this.scan_backward()), !0) : !1
    }, this.nextPage = function () {
        return this.has_next_page() ? (s("go forward"), this.gotoPage(this.next_page), !0) : !1
    }, this.gotoPage = function (e) {
        this.page = e, this.redraw()
    }, this.wordAsHtml = function (e, t, n) {
        var r, i = words[e];
        if (typeof i == "object") {
            if (Object.prototype.toString.call(i) != "[object Array]") {
                var s = document.createElement("img"), o = fontsize_to_wordwidths[n.size][e] / i.width;
                return s.src = i.src, s.style.width = i.width * o + "px", s.style.height = i.height * o + "px", s
            }
            r = words[e]
        } else r = [e];
        var u = document.createElement("div"), a = n.get_widths();
        for (var f = 0; f < r.length; f++) {
            var l = r[f], c = window.word_metadata !== undefined ? word_metadata[l] : {};
            c == null && (c = {});
            var h = document.createElement("span");
            h.style.cssText = styles[l], h.style.fontSize = n.size + "px";
            if (c.href !== undefined) {
                var p = c.external_link === !1, d = !p || typeof anchors != "undefined" && anchors[c.href] !== undefined;
                if (d) {
                    h.style.cursor = "pointer";
                    var v = function () {
                    };
                    p ? v = function (e, t) {
                        return function () {
                            t.gotoBlock(e)
                        }
                    }(anchors[c.href], this) : v = function (e) {
                        return function () {
                            window.open(e)
                        }
                    }(c.href), h.onclick = v
                }
            }
            c.underline && (h = this.addUnderline(h, a[l]), !u.style.color && h.style.color && (u.style.color = h.style.color));
            var m = words[l];
            h.appendChild(document.createTextNode(m)), u.appendChild(h)
        }
        return u.style.width = a[e] * 2 + "px", u
    }, this.shouldJoinUnderlines = function (e, t) {
        var n = window.word_metadata !== undefined ? word_metadata[e] : {};
        n == null && (n = {});
        var r = window.word_metadata !== undefined ? word_metadata[t] : {};
        return r == null && (r = {}), n.underline && r.underline
    }, this.joinUnderlines = function (e, t) {
        var n = document.createElement("div");
        return n.style.position = "absolute", n.style.height = "2px", n.style.bottom = "-2px", n.style.left = "-" + t + "px", n.style.borderBottom = "1px solid", n.style.width = t + "px", n.innerHTML = "&nbsp;", e.appendChild(n), e
    }, this.addUnderline = function (e, t) {
        var n = document.createElement("div");
        return n.style.position = "absolute", n.style.height = "2px", n.style.bottom = "-2px", n.style.borderBottom = "1px solid", isIe() && (t *= this.scale), n.style.width = t + "px", n.innerHTML = "&nbsp;", e.appendChild(n), e
    }, this.advanceY = function (e, t) {
        isIe() && (t *= this.scale), e.advanceY(t)
    }, this.renderText = function (e, t, r) {
        r || (r = new TextPosition(t.chapter_idx, t.block_idx + 1));
        var i = this.font;
        t.block.size == "headline" && (i = i.larger);
        var s = t.block, o = i.leading;
        if (e.y + o > e.height) {
            if (o >= e.height) {
                t.next_block();
                return
            }
            if (e.y > 0) {
                e.done = !0;
                return
            }
        }
        var u = 0, f = s.align, l = new a, c = 0, h = !1;
        s["block-decoration"] == "underline" && (h = !0), f === undefined && (f = "justify");
        var p = i.get_widths(), d = t.is_first_word(), v = undefined;
        d && (v = s.bullet_id);
        var m = 0, g = 0;
        s["list-level"] ? (m = (s["list-level"] - 1) * i.size, S += m, m += i.size, g += i.size * 2) : m = 0, s.bullet_id !== undefined && (g += i.get_widths()[s.bullet_id]), g += s["block-indent"] ? s["block-indent"] * i.size : 0;
        for (; ;) {
            var y = t.block != s, b = !t.preceeds_position(r), w = s.words[t.word_idx], E = w !== undefined && p[w], S = g;
            d && s["text-indent"] && (S = Math.max(0, S + s["text-indent"] * i.size));
            var x = e.width;
            isIe() && (x /= this.scale);
            if (y || b || c + E > x - S) {
                this.renderLine(l, S, e, f, y, i, h, v, m), l = new a, c = S, this.advanceY(e, o);
                if (y || b)return;
                if (e.y + o > e.height) {
                    e.done = !0;
                    return
                }
                d = !1, v = undefined
            }
            this.render && (isIe() ? l.push(new n(w, o * this.scale, i, E)) : l.push(new n(w, o, i, E))), c += E, t.next_word(), t.preceeds_position(r) && (c += i.spaceWidth)
        }
    }, this.renderLine = function (e, t, n, r, i, s, o, u, a) {
        if (e.length === 0 || e.length === undefined)return;
        if (!this.render)return;
        var f = 0;
        for (var l = 0; l < e.length; l++) {
            var c = e[l];
            f += c.width
        }
        var h = s.spaceWidth;
        isIe() && (h *= this.scale);
        var p = f + h * (e.length - 1), d = n.offset_x + t;
        isIe() && (d = n.offset_x + t * this.scale);
        if (r == "justify" && e.length > 1 && !i) {
            var v = n.width - t - f;
            isIe() && (v = n.width / this.scale - t - f), h = v / (e.length - 1), h > s.spaceWidth * 1.75 ? h = s.spaceWidth * 1.3 : (p = n.width - t, isIe() && (p = n.width - t * this.scale)), isIe() && (h *= this.scale)
        } else r == "center" ? isIe() ? d += (n.width - p * this.scale - t * this.scale) / 2 : d += (n.width - p - t) / 2 : r == "right" && (isIe() ? d += n.width - p * this.scale - t * this.scale : d += n.width - p - t);
        var m = e[0].lineHeight;
        if (!e.isComplex()) {
            var g = document.createElement("div"), y = "";
            for (var l = 0; l < e.length; l++) {
                var b = e[l];
                y += words[b.j] + " "
            }
            g.style.cssText = styles[e[0].j], isIe() ? g.style.wordSpacing = h - s.size_of_space_glyph * this.scale + "px" : g.style.wordSpacing = h - s.size_of_space_glyph + "px", g.style.fontSize = s.size + "px", g.style.position = "absolute", g.style.left = d + "px", g.style.top = n.offset_y + n.y + "px", isIe() ? (g.style.height = m * this.scale + "px", g.style.lineHeight = (m /= this.scale) + "px") : (g.style.height = m + "px", g.style.lineHeight = m + "px"), g.style.width = n.width * 2 + "px", g.appendChild(document.createTextNode(y)), n.html.appendChild(g)
        } else {
            var w = 0, E = null;
            for (var l = 0; l < e.length; l++) {
                var b = e[l];
                if (Object.prototype.toString.call(words[b.j]) == "[object Array]")var S = words[b.j][0]; else var S = b.j;
                var x = this.wordAsHtml(b.j, b.lineHeight, b.font);
                E != null && this.shouldJoinUnderlines(E, S) && this.joinUnderlines(x, h), x.style.position = "absolute", x.style.left = d + w + "px", x.style.top = n.offset_y + n.y + "px", x.style.height = m + "px", isIe() ? x.style.lineHeight = m / this.scale + "px" : x.style.lineHeight = m + "px", n.html.appendChild(x), isIe() ? w += b.width * this.scale : w += b.width, w += h;
                if (Object.prototype.toString.call(words[b.j]) == "[object Array]")var E = words[b.j].slice(-1)[0]; else var E = b.j
            }
        }
        if (o) {
            var g = document.createElement("div");
            g.style.position = "absolute", g.style.height = m + "px", g.style.top = n.offset_y + n.y + "px", g.style.left = d + "px", g.style.fontSize = s.size + "px", g.style.lineHeight = m + "px", g.style.borderBottom = "1px solid #000000", g.style.width = p + "px", g.innerHTML = "&nbsp;", n.html.appendChild(g)
        }
        if (u) {
            var g = this.wordAsHtml(u, m, s);
            g.style.position = "absolute", g.style.height = m + "px", g.style.lineHeight = m + "px", g.style.top = n.offset_y + n.y + "px", g.style.left = n.offset_x + a + "px", n.html.appendChild(g)
        }
    }, this.renderImage = function (e, n) {
        var r = n.block, i = r.width / this.scale, s = r.height / this.scale;
        isIe() && (i *= this.scale, s *= this.scale), i > e.width && (s = s * e.width / i, i = e.width), s > e.height && this.render && (i = i * e.height / s, s = e.height);
        if (e.y > 0 && e.y + s >= e.height) {
            e.done = !0;
            return
        }
        if (this.render) {
            var o;
            if (r.type == "raw") {
                var u = i / r.width;
                isIe() && (u /= this.scale), o = document.createElement("div"), div = document.createElement("div"), div.style.fontFamily = "serif", div.style.fontSize = "24px", div.style.width = t + "px", div.innerHTML = r.data, div.style.webkitTransform = "scale(" + u + ")", div.style.webkitTransformOrigin = "top left", div.style.OTransform = "scale(" + u + ")", div.style.OTransformOrigin = "top left", div.style.msTransform = "scale(" + u + ")", div.style.msTransformOrigin = "top left", div.style.MozTransform = "scale(" + u + ")", div.style.MozTransformOrigin = "top left";
                if (isIe()) {
                    var a = document.createElement("div");
                    a.appendChild(div), i /= this.scale, s /= this.scale, a.style.zoom = "" + u, div = a
                }
                o.appendChild(div)
            } else o = document.createElement("img"), o.src = r.src, isIe() && (i /= this.scale, s /= this.scale), o.width = i, o.height = s;
            o.style.position = "absolute", o.style.top = e.offset_y + e.y + "px";
            if (r.center) {
                isIe() && (i *= this.scale);
                var f = (e.width - i) / 2;
                o.style.left = e.offset_x + f + "px"
            } else o.style.left = e.offset_x + "px";
            e.html.appendChild(o)
        }
        this.advanceY(e, s), n.next_block()
    }, this.renderRaw = function (e, t) {
        var n = t.block;
        if (n.resizeable)return this.renderImage(e, t);
        if (e.y > 0 && e.y + n.height > e.height) {
            e.done = !0;
            return
        }
        if (this.render) {
            var r = document.createElement("div");
            r.style.position = "absolute", r.style.top = e.offset_y + e.y + "px", r.style.left = e.offset_x + "px", r.style.fontFamily = "serif", r.innerHTML = n.data, e.html.appendChild(r)
        }
        this.advanceY(e, n.height), t.next_block()
    }, this.renderPreviewMessageElement = function (e) {
        return e.style.fontFamily = "serif", e.style.background = "#999999", e.style.color = "#ffffff", e.style.border = "1px solid #000000", e.style.textAlign = "center", e.style.marginTop = "20px", e.style.padding = "5px", e.className = "epub_preview_message", e.innerHTML = "You've been reading a free preview.", e
    }, this.renderPreviewMessage = function (e, t) {
        var n = t.block, r = e.width, i = e.width / 2;
        if (e.y > 0 && e.y + i > e.height) {
            e.done = !0;
            return
        }
        if (this.render) {
            var s = document.createElement("div");
            s.style.position = "absolute", s.style.top = e.offset_y + e.y + "px", s.style.left = e.offset_x + "px", s.style.width = r + "px", s = this.renderPreviewMessageElement(s), e.html.appendChild(s)
        }
        this.advanceY(e, i), t.next_block()
    }, this.renderHorizontalRuler = function (e, t) {
        var n = t.block, r = 10;
        if (e.y > 0 && e.y + r > e.height) {
            e.done = !0;
            return
        }
        if (this.render) {
            var i = document.createElement("div");
            i.style.position = "absolute", i.style.top = e.offset_y + e.y + r / 2 + "px", i.style.left = e.offset_x + "px", i.style.width = e.width + "px", i.style.height = "1px", i.style.background = "#000000", e.html.appendChild(i)
        }
        this.advanceY(e, r), t.next_block()
    }, this.renderPageBreak = function (e, t) {
        if (e.y == 0) {
            t.next_block();
            return
        }
        e.done = !0, t.next_block()
    }, this.renderSpacer = function (e, t) {
        if (e.y == 0) {
            t.next_block();
            return
        }
        var n = t.block, r = n.size * this.font.size;
        this.advanceY(e, r), t.next_block()
    }, this.block_is_content = function (e) {
        return e && e.type != "page_break" && e.type != "spacer"
    }, this.treat_position_like_chapter_start = function (e) {
        if (e.word_idx > 0)return!1;
        if (e.block_idx === 0)return!0;
        var t = e.chapter;
        if (!t)return!1;
        var n = e.block_idx, r;
        for (var i = 0, s = t.blocks.length; i < s; i++) {
            r = t.blocks[i];
            if (i == n)return!0;
            if (this.block_is_content(r))return!1
        }
        return!1
    }, e || this.construct()
}
function VerticalReflow(e) {
    this.rat_key = "epub_read", this.construct = function () {
        VerticalReflow.prototype.construct.call(this), this.page_heights = [], this.needs_redraw_callbacks = [], this.page_change_callbacks = []
    }, this.register_needs_redraw_callback = function (e) {
        this.needs_redraw_callbacks.push(e)
    }, this.register_page_change_callback = function (e) {
        this.page_change_callbacks.push(e)
    }, this.fillPages = function () {
        var e = this.render;
        e && (this.render = !1);
        var t = new TextPosition, n = new Array, r = 0;
        while (!t.done)n[n.length] = t.clone(), state = this.draw_one_column(null, 0, t), this.page_heights[r] = state.y, r += 1;
        return this.render = e, this.numPages = n.length, n
    }, this.redraw = function () {
        return
    }, this.ready_pages = function () {
        for (var e = 0; e < this.needs_redraw_callbacks.length; e++)this.needs_redraw_callbacks[e]()
    }, this.gotoPage = function (e) {
        for (var t = 0; t < this.page_change_callbacks.length; t++)this.page_change_callbacks[t](e)
    }, e || this.construct()
}
function PagedReflow(e) {
    this.construct = function () {
        PagedReflow.prototype.construct.call(this), this.page_number = 0, this.pages = [], this.focus()
    }, this.focus = function () {
        this.focus_position = this.page
    }, this.redraw = function () {
        PagedReflow.prototype.redraw.call(this), this.has_next_page() ? this.next_page = this.pages[this.page_number + this.num_columns].clone() : this.next_page = this.last_book_location()
    }, this.set_page_size = function (e, t, n, r) {
        PagedReflow.prototype.set_page_size.call(this, e, t, n, r), this.ready_pages()
    }, this.ready_pages = function () {
        this.num_columns && (this.pages = this.fillPages())
    }, this.draw_next_page = function (e) {
        if (!this.has_next_page()) {
            this.clear_columns(e);
            return
        }
        this.draw_all_columns(this.pages[this.page_number + 1].clone(), e)
    }, this.position_to_page = function (e) {
        var t;
        for (var n = 0, r = this.pages.length; n < r; n++) {
            t = this.pages[n];
            if (e.preceeds_position(t))return this.treat_position_like_chapter_start(t) && t.chapter_idx == e.chapter_idx ? n : n - 1
        }
        return this.pages.length - 1
    }, this.gotoPage = function (e) {
        var t = this.position_to_page(e);
        if (t < 0)return;
        this.page_number = t, this.page = this.pages[t], this.has_next_page() && (this.next_page = this.pages[t + 1]), this.redraw()
    }, this.scan_backward = function (e, t) {
        t = t || this.num_columns;
        var n = e ? this.position_to_page(e) : this.page_number;
        return n - t <= 0 ? new TextPosition : this.pages[n - t].clone()
    }, this.has_prev_page = function () {
        return this.page_number > 0
    }, this.has_next_page = function () {
        return this.page_number + this.num_columns < this.pages.length
    }, this.fillPages = function () {
        var e = this.render;
        e && (this.render = !1);
        var t = new TextPosition, n;
        this.page ? n = this.page.clone() : n = new TextPosition;
        var r = new Array, i = !1;
        while (!t.done)if (t.chapter_idx == n.chapter_idx && !i) {
            i = !0, t = n.clone();
            var s = [], o;
            r.length ? o = r[r.length - 1] : o = new TextPosition;
            var u = this.num_columns;
            for (; ;) {
                t = PagedReflow.prototype.scan_backward.call(this, t, 1);
                if (!o.preceeds_position(t))break;
                s.push(t)
            }
            o.preceeds_position(n) && o.is_book_start() && s.push(new TextPosition);
            for (var a = s.length - 1, f = -1; a > f; a--)r.push(s[a]);
            i = !0, t = n.clone()
        } else r[r.length] = t.clone(), this.draw_one_column(null, 0, t);
        return this.render = e, this.numPages = r.length, r
    }, this.draw_all_columns = function (e, t) {
        return this.render && this.numPages ? this.draw_paged_columns(e, t) : PagedReflow.prototype.draw_all_columns.call(this, e, t)
    }, this.draw_paged_columns = function (e, t) {
        var n = this.position_to_page(e), r;
        t = t || this.columns;
        if (!t.length)return;
        for (var i = 0, s = this.num_columns; i < s; i++)n + i < this.pages.length - 1 ? r = this.pages[n + i + 1] : r = null, n + i < this.pages.length ? this.draw_one_column(t[i], i, this.pages[n + i].clone(), r ? r.clone() : null) : this.clear_column(t[i]);
        return n + this.num_columns < this.pages.length ? this.pages[n + this.num_columns].clone() : this.pages[this.pages.length - 1].clone()
    }, this.font_style_loaded = function () {
        this.ready_pages(), this.gotoPage(this.page), PagedReflow.prototype.font_style_loaded.call(this)
    }, e || this.construct()
}
function supported_epub_versions() {
    return["mpub"]
}
function isIe() {
    return this._isie === undefined && (this._isie = document.styleSheets[0] && !document.styleSheets[0].insertRule), this._isie
}
TextPosition.count_words_in_block = function (e) {
    if (e.type == "text")return e.words.length;
    if (e.type == "image")return e.width > 128 && e.height > 128 ? 15 : 0;
    if (e.type == "raw") {
        var t = document.createElement("div");
        return t.innerHTML = e.data, t.innerText.split(" ").length
    }
    return 0
}, TextPosition.prototype = {clone: function () {
    var e = new TextPosition;
    return e.chapter_idx = this.chapter_idx, e.block_idx = this.block_idx, e.word_idx = this.word_idx, e.done = this.done, e.chapter = this.chapter, e.block = this.block, e
}, is_start: function () {
    return this.word_idx == 0 && this.block_idx == 0 && this.chapter_idx == 0
}, toString: function () {
    var e = this.chapter ? this.chapter.blocks.length : 0;
    return"(" + this.chapter_idx + "/" + book.chapters.length + ", " + this.block_idx + "/" + e + ", " + this.word_idx + ", " + this.done + ")"
}, fractional_position: function () {
    if (this.block === undefined)return undefined;
    var e = 0, t = 0;
    for (var n = 0; n < this.chapter_idx; n++)t += book.chapters[n].blocks.length;
    return t += this.block_idx, this.block.words && this.block.words.length && (t += this.word_idx / this.block.words.length), t
}}, SectionizedReflow.font_size_constants = {step: .1, min: .1, max: 2, "default": .5}, VerticalReflow.prototype = function () {
    var e = function () {
    };
    return e.prototype = new SectionizedReflow(!0), new e
}(), PagedReflow.prototype = function () {
    var e = function () {
    };
    return e.prototype = new SectionizedReflow(!0), new e
}();


/* public/javascripts/verify_fonts.js @ 1384554040 */
function activate_font_verification() {
    function s() {
        if (t)return;
        var s = document.getElementById("hairspace_ff");
        e += 1;
        var o = null, u = 0, a = 0, f = 0;
        for (nr = 0; nr < i.length; nr++) {
            var l = i[nr], c = document.getElementById("scb_fonttest" + nr);
            if (c === null)continue;
            l.ok || c.offsetWidth > 10 ? (l.ok = !0, u++) : (o === null ? o = "" : o += ", ", o += nr, a++), f++
        }
        if (u == f) {
            t = !0, n !== null && (r.removeChild(n), n = null);
            return
        }
        if (e > 10 && a > f / 2) {
            n = document.createElement("div"), n.style.color = "white", n.style.backgroundColor = "#ffc0c0", n.style.zIndex = 1073741824, n.style.position = "fixed", n.style.top = "20px", n.style.left = "20px", n.style.right = "20px", n.style.height = "40px", n.style.padding = "10px", n.style.border = "1px solid #ff0000", n.style.borderRadius = "7px", n.style.opacity = "50%";
            var h = "We've detected an error with the way your browser renders custom fonts. Some content may display incorrectly.";
            h += "<br />", h += "(Error with fonts " + o + ")", n.innerHTML = h, r.appendChild(n)
        }
        e += 1
    }

    var e = 0, t = !1, n = null, r = null, i = null;
    window.check_fontface_fonts = function () {
        i = window.ff_fonts;
        if (i === undefined)return;
        r = document.getElementsByTagName("body")[0];
        for (nr = 0; nr < i.length; nr++) {
            var e = i[nr], t = document.createElement("div");
            t.innerHTML = "&#8202;", t.id = "scb_fonttest" + nr, t.style.cssText = e.style, t.style.position = "fixed", t.style.left = "0px", t.style.top = "0px", t.style.color = "#ffffff", t.style.backgroundColor = "#ffffff", t.style.zIndex = -1073741824, t.style.display = "inline-block", t.style.fontSize = "32px", r.appendChild(t)
        }
        setInterval(s, 1e3)
    }
}
try {
    Scribd.rails_env == "qa" && activate_font_verification()
} catch (e) {
}
;


/* public/javascripts/jquery.mousewheel.js @ 1384554040 */
/*! Copyright (c) 2013 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.1.3
 *
 * Requires: 1.2.2+
 */
(function (e) {
    typeof define == "function" && define.amd ? define(["jquery"], e) : typeof exports == "object" ? module.exports = e : e(jQuery)
})(function (e) {
    function o(t) {
        var n = t || window.event, s = [].slice.call(arguments, 1), o = 0, u = 0, a = 0, f = 0, l = 0, c;
        t = e.event.fix(n), t.type = "mousewheel", n.wheelDelta && (o = n.wheelDelta), n.detail && (o = n.detail * -1), n.deltaY && (a = n.deltaY * -1, o = a), n.deltaX && (u = n.deltaX, o = u * -1), n.wheelDeltaY !== undefined && (a = n.wheelDeltaY), n.wheelDeltaX !== undefined && (u = n.wheelDeltaX * -1), f = Math.abs(o);
        if (!r || f < r)r = f;
        l = Math.max(Math.abs(a), Math.abs(u));
        if (!i || l < i)i = l;
        return c = o > 0 ? "floor" : "ceil", o = Math[c](o / r), u = Math[c](u / i), a = Math[c](a / i), s.unshift(t, o, u, a), (e.event.dispatch || e.event.handle).apply(this, s)
    }

    var t = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"], n = "onwheel"in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"], r, i;
    if (e.event.fixHooks)for (var s = t.length; s;)e.event.fixHooks[t[--s]] = e.event.mouseHooks;
    e.event.special.mousewheel = {setup: function () {
        if (this.addEventListener)for (var e = n.length; e;)this.addEventListener(n[--e], o, !1); else this.onmousewheel = o
    }, teardown: function () {
        if (this.removeEventListener)for (var e = n.length; e;)this.removeEventListener(n[--e], o, !1); else this.onmousewheel = null
    }}, e.fn.extend({mousewheel: function (e) {
        return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
    }, unmousewheel: function (e) {
        return this.unbind("mousewheel", e)
    }})
});


/* public/javascripts/jquery.nanoscroller.js @ 1384554040 */
/*! nanoScrollerJS - v0.7.2
 * http://jamesflorentino.github.com/nanoScrollerJS/
 * Copyright (c) 2013 James Florentino; Licensed MIT */
(function (e, t, n) {
    "use strict";
    var r, i, s, o, u, a, f, l, c, h, p, d, v, m, g, y, b, w, E, S, x;
    S = {paneClass: "pane", sliderClass: "slider", contentClass: "content", iOSNativeScrolling: !1, preventPageScrolling: !1, disableResize: !1, alwaysVisible: !1, flashDelay: 1500, sliderMinHeight: 20, sliderMaxHeight: null}, y = "scrollbar", g = "scroll", l = "mousedown", c = "mousemove", p = "mousewheel", h = "mouseup", m = "resize", u = "drag", w = "up", v = "panedown", s = "DOMMouseScroll", o = "down", E = "wheel", a = "keydown", f = "keyup", b = "touchmove", r = t.navigator.appName === "Microsoft Internet Explorer" && /msie 7./i.test(t.navigator.appVersion) && t.ActiveXObject, i = null, x = function () {
        var e, t, r;
        return e = n.createElement("div"), t = e.style, t.position = "absolute", t.width = "100px", t.height = "100px", t.overflow = g, t.top = "-9999px", n.body.appendChild(e), r = e.offsetWidth - e.clientWidth, n.body.removeChild(e), r
    }, d = function () {
        function a(r, s) {
            this.el = r, this.options = s, i || (i = x()), this.$el = e(this.el), this.doc = e(n), this.win = e(t), this.$content = this.$el.children("." + s.contentClass), this.$content.attr("tabindex", 0), this.content = this.$content[0], this.options.iOSNativeScrolling && this.el.style.WebkitOverflowScrolling != null ? this.nativeScrolling() : this.generate(), this.createEvents(), this.addEvents(), this.reset()
        }

        return a.prototype.preventScrolling = function (e, t) {
            if (!this.isActive)return;
            if (e.type === s)(t === o && e.originalEvent.detail > 0 || t === w && e.originalEvent.detail < 0) && e.preventDefault(); else if (e.type === p) {
                if (!e.originalEvent || !e.originalEvent.wheelDelta)return;
                (t === o && e.originalEvent.wheelDelta < 0 || t === w && e.originalEvent.wheelDelta > 0) && e.preventDefault()
            }
        }, a.prototype.nativeScrolling = function () {
            this.$content.css({WebkitOverflowScrolling: "touch"}), this.iOSNativeScrolling = !0, this.isActive = !0
        }, a.prototype.updateScrollValues = function () {
            var e;
            e = this.content, this.maxScrollTop = e.scrollHeight - e.clientHeight, this.contentScrollTop = e.scrollTop, this.iOSNativeScrolling || (this.maxSliderTop = this.paneHeight - this.sliderHeight, this.sliderTop = this.contentScrollTop * this.maxSliderTop / this.maxScrollTop)
        }, a.prototype.createEvents = function () {
            var e = this;
            this.events = {down: function (t) {
                return e.isBeingDragged = !0, e.offsetY = t.pageY - e.slider.offset().top, e.pane.addClass("active"), e.doc.bind(c, e.events[u]).bind(h, e.events[w]), !1
            }, drag: function (t) {
                return e.sliderY = t.pageY - e.$el.offset().top - e.offsetY, e.scroll(), e.updateScrollValues(), e.contentScrollTop >= e.maxScrollTop ? e.$el.trigger("scrollend") : e.contentScrollTop === 0 && e.$el.trigger("scrolltop"), !1
            }, up: function (t) {
                return e.isBeingDragged = !1, e.pane.removeClass("active"), e.doc.unbind(c, e.events[u]).unbind(h, e.events[w]), !1
            }, resize: function (t) {
                e.reset()
            }, panedown: function (t) {
                return e.sliderY = (t.offsetY || t.originalEvent.layerY) - e.sliderHeight * .5, e.scroll(), e.events.down(t), !1
            }, scroll: function (t) {
                if (e.isBeingDragged)return;
                e.updateScrollValues(), e.iOSNativeScrolling || (e.sliderY = e.sliderTop, e.slider.css({top: e.sliderTop}));
                if (t == null)return;
                e.contentScrollTop >= e.maxScrollTop ? (e.options.preventPageScrolling && e.preventScrolling(t, o), e.$el.trigger("scrollend")) : e.contentScrollTop === 0 && (e.options.preventPageScrolling && e.preventScrolling(t, w), e.$el.trigger("scrolltop"))
            }, wheel: function (t) {
                if (t == null)return;
                return e.sliderY += -t.wheelDeltaY || -t.delta, e.scroll(), !1
            }}
        }, a.prototype.addEvents = function () {
            var e;
            this.removeEvents(), e = this.events, this.options.disableResize || this.win.bind(m, e[m]), this.iOSNativeScrolling || (this.slider.bind(l, e[o]), this.pane.bind(l, e[v]).bind("" + p + " " + s, e[E])), this.$content.bind("" + g + " " + p + " " + s + " " + b, e[g])
        }, a.prototype.removeEvents = function () {
            var e;
            e = this.events, this.win.unbind(m, e[m]), this.iOSNativeScrolling || (this.slider.unbind(), this.pane.unbind()), this.$content.unbind("" + g + " " + p + " " + s + " " + b, e[g])
        }, a.prototype.generate = function () {
            var e, t, n, r, s;
            return n = this.options, r = n.paneClass, s = n.sliderClass, e = n.contentClass, !this.$el.find("" + r).length && !this.$el.find("" + s).length && this.$el.append('<div class="' + r + '"><div class="' + s + '" /></div>'), this.pane = this.$el.children("." + r), this.slider = this.pane.find("." + s), i && (t = this.$el.css("direction") === "rtl" ? {left: -i} : {right: -i}, this.$el.addClass("has-scrollbar")), t != null && this.$content.css(t), this
        }, a.prototype.restore = function () {
            this.stopped = !1, this.pane.show(), this.addEvents()
        }, a.prototype.reset = function () {
            var e, t, n, s, o, u, a, f, l;
            if (this.iOSNativeScrolling) {
                this.contentHeight = this.content.scrollHeight;
                return
            }
            return this.$el.find("." + this.options.paneClass).length || this.generate().stop(), this.stopped && this.restore(), e = this.content, n = e.style, s = n.overflowY, r && this.$content.css({height: this.$content.height()}), t = e.scrollHeight + i, u = this.pane.outerHeight(), f = parseInt(this.pane.css("top"), 10), o = parseInt(this.pane.css("bottom"), 10), a = u + f + o, l = Math.round(a / t * a), l < this.options.sliderMinHeight ? l = this.options.sliderMinHeight : this.options.sliderMaxHeight != null && l > this.options.sliderMaxHeight && (l = this.options.sliderMaxHeight), s === g && n.overflowX !== g && (l += i), this.maxSliderTop = a - l, this.contentHeight = t, this.paneHeight = u, this.paneOuterHeight = a, this.sliderHeight = l, this.slider.height(l), this.events.scroll(), this.pane.show(), this.isActive = !0, e.scrollHeight === e.clientHeight || this.pane.outerHeight(!0) >= e.scrollHeight && s !== g ? (this.pane.hide(), this.isActive = !1) : this.el.clientHeight === e.scrollHeight && s === g ? this.slider.hide() : this.slider.show(), this.pane.css({opacity: this.options.alwaysVisible ? 1 : "", visibility: this.options.alwaysVisible ? "visible" : ""}), this
        }, a.prototype.scroll = function () {
            if (!this.isActive)return;
            return this.sliderY = Math.max(0, this.sliderY), this.sliderY = Math.min(this.maxSliderTop, this.sliderY), this.$content.scrollTop((this.paneHeight - this.contentHeight + i) * this.sliderY / this.maxSliderTop * -1), this.iOSNativeScrolling || this.slider.css({top: this.sliderY}), this
        }, a.prototype.scrollBottom = function (e) {
            if (!this.isActive)return;
            return this.reset(), this.$content.scrollTop(this.contentHeight - this.$content.height() - e).trigger(p), this
        }, a.prototype.scrollTop = function (e) {
            if (!this.isActive)return;
            return this.reset(), this.$content.scrollTop(+e).trigger(p), this
        }, a.prototype.scrollTo = function (t) {
            if (!this.isActive)return;
            return this.reset(), this.scrollTop(e(t).get(0).offsetTop), this
        }, a.prototype.stop = function () {
            return this.stopped = !0, this.removeEvents(), this.pane.hide(), this
        }, a.prototype.flash = function () {
            var e = this;
            if (!this.isActive)return;
            return this.reset(), this.pane.addClass("flashed"), setTimeout(function () {
                e.pane.removeClass("flashed")
            }, this.options.flashDelay), this
        }, a
    }(), e.fn.nanoScroller = function (t) {
        return this.each(function () {
            var n, r;
            (r = this.nanoscroller) || (n = e.extend({}, S, t), this.nanoscroller = r = new d(this, n));
            if (t && typeof t == "object") {
                e.extend(r.options, t);
                if (t.scrollBottom)return r.scrollBottom(t.scrollBottom);
                if (t.scrollTop)return r.scrollTop(t.scrollTop);
                if (t.scrollTo)return r.scrollTo(t.scrollTo);
                if (t.scroll === "bottom")return r.scrollBottom(0);
                if (t.scroll === "top")return r.scrollTop(0);
                if (t.scroll && t.scroll instanceof e)return r.scrollTo(t.scroll);
                if (t.stop)return r.stop();
                if (t.flash)return r.flash()
            }
            return r.reset()
        })
    }
})(jQuery, window, document);


/* public/javascripts/jquery.dotdotdot.js @ 1384554040 */
/*	
 *	jQuery dotdotdot 1.5.6
 *	
 *	Copyright (c) 2013 Fred Heusschen
 *	www.frebsite.nl
 *
 *	Plugin website:
 *	dotdotdot.frebsite.nl
 *
 *	Dual licensed under the MIT and GPL licenses.
 *	http://en.wikipedia.org/wiki/MIT_License
 *	http://en.wikipedia.org/wiki/GNU_General_Public_License
 */
(function (e) {
    function n(e, t, n) {
        var r = e.children(), i = !1;
        e.empty();
        for (var o = 0, u = r.length; o < u; o++) {
            var a = r.eq(o);
            e.append(a), n && e.append(n);
            if (s(e, t)) {
                a.remove(), i = !0;
                break
            }
            n && n.remove()
        }
        return i
    }

    function r(t, n, o, u, a) {
        var f = t.contents(), l = !1;
        t.empty();
        var c = "table, thead, tbody, tfoot, tr, col, colgroup, object, embed, param, ol, ul, dl, select, optgroup, option, textarea, script, style";
        for (var h = 0, p = f.length; h < p; h++) {
            if (l)break;
            var d = f[h], v = e(d);
            if (typeof d == "undefined")continue;
            t.append(v), a && t[t.is(c) ? "after" : "append"](a), d.nodeType == 3 ? s(o, u) && (l = i(v, n, o, u, a)) : l = r(v, n, o, u, a), l || a && a.remove()
        }
        return l
    }

    function i(e, t, n, r, u) {
        var l = !1, c = e[0];
        if (typeof c == "undefined")return!1;
        var h = r.wrap == "letter" ? "" : " ", p = f(c).split(h), d = -1, v = -1, m = 0, g = p.length - 1;
        while (m <= g) {
            var y = Math.floor((m + g) / 2);
            if (y == v)break;
            v = y, a(c, p.slice(0, v + 1).join(h) + r.ellipsis), s(n, r) ? g = v : (d = v, m = v)
        }
        if (d == -1 || p.length == 1 && p[0].length == 0) {
            var w = e.parent();
            e.remove();
            var E = u ? u.length : 0;
            if (w.contents().size() > E) {
                var S = w.contents().eq(-1 - E);
                l = i(S, t, n, r, u)
            } else {
                var c = w.prev().contents().eq(-1)[0];
                if (typeof c != "undefined") {
                    var b = o(f(c), r);
                    a(c, b), w.remove(), l = !0
                }
            }
        } else {
            var b = o(p.slice(0, d + 1).join(h), r);
            l = !0, a(c, b)
        }
        return l
    }

    function s(e, t) {
        return e.innerHeight() > t.maxHeight
    }

    function o(t, n) {
        while (e.inArray(t.slice(-1), n.lastCharacter.remove) > -1)t = t.slice(0, -1);
        return e.inArray(t.slice(-1), n.lastCharacter.noEllipsis) < 0 && (t += n.ellipsis), t
    }

    function u(e) {
        return{width: e.innerWidth(), height: e.innerHeight()}
    }

    function a(e, t) {
        e.innerText ? e.innerText = t : e.nodeValue ? e.nodeValue = t : e.textContent && (e.textContent = t)
    }

    function f(e) {
        return e.innerText ? e.innerText : e.nodeValue ? e.nodeValue : e.textContent ? e.textContent : ""
    }

    function l(t, n) {
        return typeof t == "undefined" ? !1 : t ? typeof t == "string" ? (t = e(t, n), t.length ? t : !1) : typeof t == "object" ? typeof t.jquery == "undefined" ? !1 : t : !1 : !1
    }

    function c(e) {
        var t = e.innerHeight(), n = ["paddingTop", "paddingBottom"];
        for (var r = 0, i = n.length; r < i; r++) {
            var s = parseInt(e.css(n[r]), 10);
            isNaN(s) && (s = 0), t -= s
        }
        return t
    }

    function h(e, t) {
        return e ? (typeof t == "string" ? t = "dotdotdot: " + t : t = ["dotdotdot:", t], typeof window.console != "undefined" && typeof window.console.log != "undefined" && window.console.log(t), !1) : !1
    }

    if (e.fn.dotdotdot)return;
    e.fn.dotdotdot = function (i) {
        if (this.length == 0)return h(!0, 'No element found for "' + this.selector + '".'), this;
        if (this.length > 1)return this.each(function () {
            e(this).dotdotdot(i)
        });
        var o = this;
        o.data("dotdotdot") && o.trigger("destroy.dot"), o.bind_events = function () {
            return o.bind("update.dot",function (t, i) {
                t.preventDefault(), t.stopPropagation(), f.maxHeight = typeof f.height == "number" ? f.height : c(o), f.maxHeight += f.tolerance;
                if (typeof i != "undefined") {
                    if (typeof i == "string" || i instanceof HTMLElement)i = e("<div />").append(i).contents();
                    i instanceof e && (a = i)
                }
                m = o.wrapInner('<div class="dotdotdot" />').children(), m.empty().append(a.clone(!0)).css({height: "auto", width: "auto", border: "none", padding: 0, margin: 0});
                var u = !1, l = !1;
                return p.afterElement && (u = p.afterElement.clone(!0), p.afterElement.remove()), s(m, f) && (f.wrap == "children" ? l = n(m, f, u) : l = r(m, o, m, f, u)), m.replaceWith(m.contents()), m = null, e.isFunction(f.callback) && f.callback.call(o[0], l, a), p.isTruncated = l, l
            }).bind("isTruncated.dot",function (e, t) {
                return e.preventDefault(), e.stopPropagation(), typeof t == "function" && t.call(o[0], p.isTruncated), p.isTruncated
            }).bind("originalContent.dot",function (e, t) {
                return e.preventDefault(), e.stopPropagation(), typeof t == "function" && t.call(o[0], a), a
            }).bind("destroy.dot", function (e) {
                e.preventDefault(), e.stopPropagation(), o.unwatch().unbind_events().empty().append(a).data("dotdotdot", !1)
            }), o
        }, o.unbind_events = function () {
            return o.unbind(".dot"), o
        }, o.watch = function () {
            o.unwatch();
            if (f.watch == "window") {
                var t = e(window), n = t.width(), r = t.height();
                t.bind("resize.dot" + p.dotId, function () {
                    if (n != t.width() || r != t.height() || !f.windowResizeFix)n = t.width(), r = t.height(), v && clearInterval(v), v = setTimeout(function () {
                        o.trigger("update.dot")
                    }, 10)
                })
            } else d = u(o), v = setInterval(function () {
                var e = u(o);
                if (d.width != e.width || d.height != e.height)o.trigger("update.dot"), d = u(o)
            }, 100);
            return o
        }, o.unwatch = function () {
            return e(window).unbind("resize.dot" + p.dotId), v && clearInterval(v), o
        };
        var a = o.contents(), f = e.extend(!0, {}, e.fn.dotdotdot.defaults, i), p = {}, d = {}, v = null, m = null;
        return p.afterElement = l(f.after, o), p.isTruncated = !1, p.dotId = t++, o.data("dotdotdot", !0).bind_events().trigger("update.dot"), f.watch && o.watch(), o
    }, e.fn.dotdotdot.defaults = {ellipsis: "... ", wrap: "word", lastCharacter: {remove: [" ", ",", ";", ".", "!", "?"], noEllipsis: []}, tolerance: 0, callback: null, after: null, height: null, watch: !1, windowResizeFix: !0, debug: !1};
    var t = 1, p = e.fn.html;
    e.fn.html = function (e) {
        if (typeof e != "undefined")return this.data("dotdotdot") && typeof e != "function" ? this.trigger("update", [e]) : p.call(this, e);
        return p.call(this)
    };
    var d = e.fn.text;
    e.fn.text = function (t) {
        if (typeof t != "undefined") {
            if (this.data("dotdotdot")) {
                var n = e("<div />");
                return n.text(t), t = n.html(), n.remove(), this.trigger("update", [t])
            }
            return d.call(this, t)
        }
        return d.call(this)
    }
})(jQuery);


/* public/javascripts/storage.js @ 1384554040 */
// Storage polyfill by Remy Sharp
// https://gist.github.com/350433
// Needed for IE7-
// Dependencies:
//  JSON (use json2.js if necessary)
// Tweaks by Joshua Bell (inexorabletash@gmail.com)
//  * URI-encode item keys
//  * Use String() for stringifying
//  * added length
(!window.localStorage || !window.sessionStorage) && function () {
    var e = function (e) {
        function t(e, t, n) {
            var r, i;
            n ? (r = new Date, r.setTime(r.getTime() + n * 24 * 60 * 60 * 1e3), i = "; expires=" + r.toGMTString()) : i = "", document.cookie = e + "=" + t + i + "; path=/"
        }

        function n(e) {
            var t = e + "=", n = document.cookie.split(";"), r, i;
            for (r = 0; r < n.length; r++) {
                i = n[r];
                while (i.charAt(0) == " ")i = i.substring(1, i.length);
                if (i.indexOf(t) == 0)return i.substring(t.length, i.length)
            }
            return null
        }

        function r(n) {
            n = JSON.stringify(n), e == "session" ? window.name = n : t("localStorage", n, 365)
        }

        function i() {
            e == "session" ? window.name = "" : t("localStorage", "", 365)
        }

        function s() {
            var t = e == "session" ? window.name : n("localStorage");
            return t ? JSON.parse(t) : {}
        }

        function u() {
            var e = 0;
            for (var t in o)o.hasOwnProperty(t) && (e += 1);
            return e
        }

        var o = s();
        return{clear: function () {
            o = {}, i(), this.length = u()
        }, getItem: function (e) {
            return e = encodeURIComponent(e), o[e] === undefined ? null : o[e]
        }, key: function (e) {
            var t = 0;
            for (var n in o) {
                if (t == e)return decodeURIComponent(n);
                t++
            }
            return null
        }, removeItem: function (e) {
            e = encodeURIComponent(e), delete o[e], r(o), this.length = u()
        }, setItem: function (e, t) {
            e = encodeURIComponent(e), o[e] = String(t), r(o), this.length = u()
        }, length: 0}
    };
    window.localStorage || (window.localStorage = new e("local")), window.sessionStorage || (window.sessionStorage = new e("session"))
}();


/* public/javascripts/modernizr.custom.65765.js @ 1384554040 */
/* Modernizr 2.6.2 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-textshadow
 */
window.Modernizr = function (e, t, n) {
    function b(e) {
        a.cssText = e
    }

    function w(e, t) {
        return b(prefixes.join(e + ";") + (t || ""))
    }

    function E(e, t) {
        return typeof e === t
    }

    function S(e, t) {
        return!!~("" + e).indexOf(t)
    }

    function x(e, t, r) {
        for (var i in e) {
            var s = t[e[i]];
            if (s !== n)return r === !1 ? e[i] : E(s, "function") ? s.bind(r || t) : s
        }
        return!1
    }

    var r = "2.6.2", i = {}, s = t.documentElement, o = "modernizr", u = t.createElement(o), a = u.style, f, l = {}.toString, c = {}, h = {}, p = {}, d = [], v = d.slice, m, g = {}.hasOwnProperty, y;
    !E(g, "undefined") && !E(g.call, "undefined") ? y = function (e, t) {
        return g.call(e, t)
    } : y = function (e, t) {
        return t in e && E(e.constructor.prototype[t], "undefined")
    }, Function.prototype.bind || (Function.prototype.bind = function (t) {
        var n = this;
        if (typeof n != "function")throw new TypeError;
        var r = v.call(arguments, 1), i = function () {
            if (this instanceof i) {
                var e = function () {
                };
                e.prototype = n.prototype;
                var s = new e, o = n.apply(s, r.concat(v.call(arguments)));
                return Object(o) === o ? o : s
            }
            return n.apply(t, r.concat(v.call(arguments)))
        };
        return i
    }), c.textshadow = function () {
        return t.createElement("div").style.textShadow === ""
    };
    for (var T in c)y(c, T) && (m = T.toLowerCase(), i[m] = c[T](), d.push((i[m] ? "" : "no-") + m));
    return i.addTest = function (e, t) {
        if (typeof e == "object")for (var r in e)y(e, r) && i.addTest(r, e[r]); else {
            e = e.toLowerCase();
            if (i[e] !== n)return i;
            t = typeof t == "function" ? t() : t, typeof enableClasses != "undefined" && enableClasses && (s.className += " " + (t ? "" : "no-") + e), i[e] = t
        }
        return i
    }, b(""), u = f = null, i._version = r, i
}(this, this.document);


/* public/javascripts/imagesloaded.pkgd.js @ 1384554040 */
/*!
 * imagesLoaded PACKAGED v3.0.2
 * JavaScript is all like "You images are done yet or what?"
 */
/*!
 * EventEmitter v4.1.0 - git.io/ee
 * Oliver Caldwell
 * MIT license
 * @preserve
 */
(function (e) {
    "use strict";
    function t() {
    }

    function i(e, t) {
        if (r)return t.indexOf(e);
        var n = t.length;
        while (n--)if (t[n] === e)return n;
        return-1
    }

    var n = t.prototype, r = Array.prototype.indexOf ? !0 : !1;
    n._getEvents = function () {
        return this._events || (this._events = {})
    }, n.getListeners = function (e) {
        var t = this._getEvents(), n, r;
        if (typeof e == "object") {
            n = {};
            for (r in t)t.hasOwnProperty(r) && e.test(r) && (n[r] = t[r])
        } else n = t[e] || (t[e] = []);
        return n
    }, n.getListenersAsObject = function (e) {
        var t = this.getListeners(e), n;
        return t instanceof Array && (n = {}, n[e] = t), n || t
    }, n.addListener = function (e, t) {
        var n = this.getListenersAsObject(e), r;
        for (r in n)n.hasOwnProperty(r) && i(t, n[r]) === -1 && n[r].push(t);
        return this
    }, n.on = n.addListener, n.defineEvent = function (e) {
        return this.getListeners(e), this
    }, n.defineEvents = function (e) {
        for (var t = 0; t < e.length; t += 1)this.defineEvent(e[t]);
        return this
    }, n.removeListener = function (e, t) {
        var n = this.getListenersAsObject(e), r, s;
        for (s in n)n.hasOwnProperty(s) && (r = i(t, n[s]), r !== -1 && n[s].splice(r, 1));
        return this
    }, n.off = n.removeListener, n.addListeners = function (e, t) {
        return this.manipulateListeners(!1, e, t)
    }, n.removeListeners = function (e, t) {
        return this.manipulateListeners(!0, e, t)
    }, n.manipulateListeners = function (e, t, n) {
        var r, i, s = e ? this.removeListener : this.addListener, o = e ? this.removeListeners : this.addListeners;
        if (typeof t != "object" || t instanceof RegExp) {
            r = n.length;
            while (r--)s.call(this, t, n[r])
        } else for (r in t)t.hasOwnProperty(r) && (i = t[r]) && (typeof i == "function" ? s.call(this, r, i) : o.call(this, r, i));
        return this
    }, n.removeEvent = function (e) {
        var t = typeof e, n = this._getEvents(), r;
        if (t === "string")delete n[e]; else if (t === "object")for (r in n)n.hasOwnProperty(r) && e.test(r) && delete n[r]; else delete this._events;
        return this
    }, n.emitEvent = function (e, t) {
        var n = this.getListenersAsObject(e), r, i, s;
        for (i in n)if (n.hasOwnProperty(i)) {
            r = n[i].length;
            while (r--)s = t ? n[i][r].apply(null, t) : n[i][r](), s === !0 && this.removeListener(e, n[i][r])
        }
        return this
    }, n.trigger = n.emitEvent, n.emit = function (e) {
        var t = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(e, t)
    }, typeof define == "function" && define.amd ? define(function () {
        return t
    }) : e.EventEmitter = t
})(this), function (e) {
    "use strict";
    var t = document.documentElement, n = function () {
    };
    t.addEventListener ? n = function (e, t, n) {
        e.addEventListener(t, n, !1)
    } : t.attachEvent && (n = function (t, n, r) {
        t[n + r] = r.handleEvent ? function () {
            var t = e.event;
            t.target = t.target || t.srcElement, r.handleEvent.call(r, t)
        } : function () {
            var n = e.event;
            n.target = n.target || n.srcElement, r.call(t, n)
        }, t.attachEvent("on" + n, t[n + r])
    });
    var r = function () {
    };
    t.removeEventListener ? r = function (e, t, n) {
        e.removeEventListener(t, n, !1)
    } : t.detachEvent && (r = function (e, t, n) {
        e.detachEvent("on" + t, e[t + n]);
        try {
            delete e[t + n]
        } catch (r) {
            e[t + n] = undefined
        }
    });
    var i = {bind: n, unbind: r};
    typeof define == "function" && define.amd ? define(i) : e.eventie = i
}(this), function (e) {
    "use strict";
    function i(e, t) {
        for (var n in t)e[n] = t[n];
        return e
    }

    function o(e) {
        return s.call(e) === "[object Array]"
    }

    function u(e) {
        var t = [];
        if (o(e))t = e; else if (typeof e.length == "number")for (var n = 0, r = e.length; n < r; n++)t.push(e[n]); else t.push(e);
        return t
    }

    function a(e, s) {
        function o(e, n, r) {
            if (!(this instanceof o))return new o(e, n);
            typeof e == "string" && (e = document.querySelectorAll(e)), this.elements = u(e), this.options = i({}, this.options), typeof n == "function" ? r = n : i(this.options, n), r && this.on("always", r), this.getImages(), t && (this.jqDeferred = new t.Deferred);
            var s = this;
            setTimeout(function () {
                s.check()
            })
        }

        function f(e) {
            this.img = e
        }

        o.prototype = new e, o.prototype.options = {}, o.prototype.getImages = function () {
            this.images = [];
            for (var e = 0, t = this.elements.length; e < t; e++) {
                var n = this.elements[e];
                n.nodeName === "IMG" && this.addImage(n);
                var r = n.querySelectorAll("img");
                for (var i = 0, s = r.length; i < s; i++) {
                    var o = r[i];
                    this.addImage(o)
                }
            }
        }, o.prototype.addImage = function (e) {
            var t = new f(e);
            this.images.push(t)
        }, o.prototype.check = function () {
            function s(s, o) {
                return e.options.debug && r && n.log("confirm", s, o), e.progress(s), t++, t === i && e.complete(), !0
            }

            var e = this, t = 0, i = this.images.length;
            this.hasAnyBroken = !1;
            if (!i) {
                this.complete();
                return
            }
            for (var o = 0; o < i; o++) {
                var u = this.images[o];
                u.on("confirm", s), u.check()
            }
        }, o.prototype.progress = function (e) {
            this.hasAnyBroken = this.hasAnyBroken || !e.isLoaded, this.emit("progress", this, e), this.jqDeferred && this.jqDeferred.notify(this, e)
        }, o.prototype.complete = function () {
            var e = this.hasAnyBroken ? "fail" : "done";
            this.isComplete = !0, this.emit(e, this), this.emit("always", this);
            if (this.jqDeferred) {
                var t = this.hasAnyBroken ? "reject" : "resolve";
                this.jqDeferred[t](this)
            }
        }, t && (t.fn.imagesLoaded = function (e, n) {
            var r = new o(this, e, n);
            return r.jqDeferred.promise(t(this))
        });
        var a = {};
        return f.prototype = new e, f.prototype.check = function () {
            var e = a[this.img.src];
            if (e) {
                this.useCached(e);
                return
            }
            a[this.img.src] = this;
            if (this.img.complete && this.img.naturalWidth !== undefined) {
                this.confirm(this.img.naturalWidth !== 0, "naturalWidth");
                return
            }
            var t = this.proxyImage = new Image;
            s.bind(t, "load", this), s.bind(t, "error", this), t.src = this.img.src
        }, f.prototype.useCached = function (e) {
            if (e.isConfirmed)this.confirm(e.isLoaded, "cached was confirmed"); else {
                var t = this;
                e.on("confirm", function (e) {
                    return t.confirm(e.isLoaded, "cache emitted confirmed"), !0
                })
            }
        }, f.prototype.confirm = function (e, t) {
            this.isConfirmed = !0, this.isLoaded = e, this.emit("confirm", this, t)
        }, f.prototype.handleEvent = function (e) {
            var t = "on" + e.type;
            this[t] && this[t](e)
        }, f.prototype.onload = function () {
            this.confirm(!0, "onload"), this.unbindProxyEvents()
        }, f.prototype.onerror = function () {
            this.confirm(!1, "onerror"), this.unbindProxyEvents()
        }, f.prototype.unbindProxyEvents = function () {
            s.unbind(this.proxyImage, "load", this), s.unbind(this.proxyImage, "error", this)
        }, o
    }

    var t = e.jQuery, n = e.console, r = typeof n != "undefined", s = Object.prototype.toString;
    typeof define == "function" && define.amd ? define(["eventEmitter", "eventie"], a) : e.imagesLoaded = a(e.EventEmitter, e.eventie)
}(window);


/* public/javascripts/backbone/backbone-relational.js @ 1384554040 */
/**
 * Backbone-relational.js 0.6.0
 * (c) 2011 Paul Uithol
 *
 * Backbone-relational may be freely distributed under the MIT license; see the accompanying LICENSE.txt.
 * For details and documentation: https://github.com/PaulUithol/Backbone-relational.
 * Depends on Backbone (and thus on Underscore as well): https://github.com/documentcloud/backbone.
 */
(function (e) {
    "use strict";
    var t, n, r;
    typeof window == "undefined" ? (t = require("underscore"), n = require("backbone"), r = module.exports = n) : (t = window._, n = window.Backbone, r = window), n.Relational = {showWarnings: !0}, n.Semaphore = {_permitsAvailable: null, _permitsUsed: 0, acquire: function () {
        if (this._permitsAvailable && this._permitsUsed >= this._permitsAvailable)throw new Error("Max permits acquired");
        this._permitsUsed++
    }, release: function () {
        if (this._permitsUsed === 0)throw new Error("All permits released");
        this._permitsUsed--
    }, isLocked: function () {
        return this._permitsUsed > 0
    }, setAvailablePermits: function (e) {
        if (this._permitsUsed > e)throw new Error("Available permits cannot be less than used permits");
        this._permitsAvailable = e
    }}, n.BlockingQueue = function () {
        this._queue = []
    }, t.extend(n.BlockingQueue.prototype, n.Semaphore, {_queue: null, add: function (e) {
        this.isBlocked() ? this._queue.push(e) : e()
    }, process: function () {
        while (this._queue && this._queue.length)this._queue.shift()()
    }, block: function () {
        this.acquire()
    }, unblock: function () {
        this.release(), this.isBlocked() || this.process()
    }, isBlocked: function () {
        return this.isLocked()
    }}), n.Relational.eventQueue = new n.BlockingQueue, n.Store = function () {
        this._collections = [], this._reverseRelations = [], this._subModels = [], this._modelScopes = [r]
    }, t.extend(n.Store.prototype, n.Events, {addModelScope: function (e) {
        this._modelScopes.push(e)
    }, addSubModels: function (e, t) {
        this._subModels.push({superModelType: t, subModels: e})
    }, setupSuperModel: function (e) {
        t.find(this._subModels, function (n) {
            return t.find(n.subModels, function (t, r) {
                var i = this.getObjectByName(t);
                if (e === i)return n.superModelType._subModels[r] = e, e._superModel = n.superModelType, e._subModelTypeValue = r, e._subModelTypeAttribute = n.superModelType.prototype.subModelTypeAttribute, !0
            }, this)
        }, this)
    }, addReverseRelation: function (e) {
        var n = t.any(this._reverseRelations, function (n) {
            return t.all(e, function (e, t) {
                return e === n[t]
            })
        });
        if (!n && e.model && e.type) {
            this._reverseRelations.push(e);
            var r = function (e, n) {
                e.prototype.relations || (e.prototype.relations = []), e.prototype.relations.push(n), t.each(e._subModels, function (e) {
                    r(e, n)
                }, this)
            };
            r(e.model, e), this.retroFitRelation(e)
        }
    }, retroFitRelation: function (e) {
        var t = this.getCollection(e.model);
        t.each(function (t) {
            if (!(t instanceof e.model))return;
            new e.type(t, e)
        }, this)
    }, getCollection: function (e) {
        e instanceof n.RelationalModel && (e = e.constructor);
        var r = e;
        while (r._superModel)r = r._superModel;
        var i = t.detect(this._collections, function (e) {
            return e.model === r
        });
        return i || (i = this._createCollection(r)), i
    }, getObjectByName: function (e) {
        var n = e.split("."), r = null;
        return t.find(this._modelScopes, function (e) {
            r = t.reduce(n, function (e, t) {
                return e[t]
            }, e);
            if (r && r !== e)return!0
        }, this), r
    }, _createCollection: function (e) {
        var t;
        return e instanceof n.RelationalModel && (e = e.constructor), e.prototype instanceof n.RelationalModel && (t = new n.Collection, t.model = e, this._collections.push(t)), t
    }, resolveIdForItem: function (e, r) {
        var i = t.isString(r) || t.isNumber(r) ? r : null;
        return i === null && (r instanceof n.RelationalModel ? i = r.id : t.isObject(r) && (i = r[e.prototype.idAttribute])), !i && i !== 0 && (i = null), i
    }, find: function (e, t) {
        var n = this.resolveIdForItem(e, t), r = this.getCollection(e);
        if (r) {
            var i = r.get(n);
            if (i instanceof e)return i
        }
        return null
    }, register: function (e) {
        var t = this.getCollection(e);
        if (t) {
            if (t.get(e))throw new Error("Cannot instantiate more than one Backbone.RelationalModel with the same id per type!");
            var n = e.collection;
            t.add(e), e.bind("destroy", this.unregister, this), e.collection = n
        }
    }, update: function (e) {
        var t = this.getCollection(e);
        t._onModelEvent("change:" + e.idAttribute, e, t)
    }, unregister: function (e) {
        e.unbind("destroy", this.unregister);
        var t = this.getCollection(e);
        t && t.remove(e)
    }}), n.Relational.store = new n.Store, n.Relation = function (e, r) {
        this.instance = e, r = t.isObject(r) ? r : {}, this.reverseRelation = t.defaults(r.reverseRelation || {}, this.options.reverseRelation), this.reverseRelation.type = t.isString(this.reverseRelation.type) ? n[this.reverseRelation.type] || n.Relational.store.getObjectByName(this.reverseRelation.type) : this.reverseRelation.type, this.model = r.model || this.instance.constructor, this.options = t.defaults(r, this.options, n.Relation.prototype.options), this.key = this.options.key, this.keySource = this.options.keySource || this.key, this.keyDestination = this.options.keyDestination || this.keySource || this.key, this.relatedModel = this.options.relatedModel, t.isString(this.relatedModel) && (this.relatedModel = n.Relational.store.getObjectByName(this.relatedModel));
        if (!this.checkPreconditions())return!1;
        e && (this.keyContents = this.instance.get(this.keySource), this.key !== this.keySource && this.instance.unset(this.keySource, {silent: !0}), this.instance._relations.push(this)), !this.options.isAutoRelation && this.reverseRelation.type && this.reverseRelation.key && n.Relational.store.addReverseRelation(t.defaults({isAutoRelation: !0, model: this.relatedModel, relatedModel: this.model, reverseRelation: this.options}, this.reverseRelation)), t.bindAll(this, "_modelRemovedFromCollection", "_relatedModelAdded", "_relatedModelRemoved"), e && (this.initialize(), n.Relational.store.getCollection(this.instance).bind("relational:remove", this._modelRemovedFromCollection), n.Relational.store.getCollection(this.relatedModel).bind("relational:add", this._relatedModelAdded).bind("relational:remove", this._relatedModelRemoved))
    }, n.Relation.extend = n.Model.extend, t.extend(n.Relation.prototype, n.Events, n.Semaphore, {options: {createModels: !0, includeInJSON: !0, isAutoRelation: !1}, instance: null, key: null, keyContents: null, relatedModel: null, reverseRelation: null, related: null, _relatedModelAdded: function (e, t, n) {
        var r = this;
        e.queue(function () {
            r.tryAddRelated(e, n)
        })
    }, _relatedModelRemoved: function (e, t, n) {
        this.removeRelated(e, n)
    }, _modelRemovedFromCollection: function (e) {
        e === this.instance && this.destroy()
    }, checkPreconditions: function () {
        var e = this.instance, r = this.key, i = this.model, s = this.relatedModel, o = n.Relational.showWarnings && typeof console != "undefined";
        if (!i || !r || !s)return o && console.warn("Relation=%o; no model, key or relatedModel (%o, %o, %o)", this, i, r, s), !1;
        if (i.prototype instanceof n.RelationalModel) {
            if (s.prototype instanceof n.RelationalModel) {
                if (this instanceof n.HasMany && this.reverseRelation.type === n.HasMany)return o && console.warn("Relation=%o; relation is a HasMany, and the reverseRelation is HasMany as well.", this), !1;
                if (e && e._relations.length) {
                    var u = t.any(e._relations, function (e) {
                        var t = this.reverseRelation.key && e.reverseRelation.key;
                        return e.relatedModel === s && e.key === r && (!t || this.reverseRelation.key === e.reverseRelation.key)
                    }, this);
                    if (u)return o && console.warn("Relation=%o between instance=%o.%s and relatedModel=%o.%s already exists", this, e, r, s, this.reverseRelation.key), !1
                }
                return!0
            }
            return o && console.warn("Relation=%o; relatedModel does not inherit from Backbone.RelationalModel (%o)", this, s), !1
        }
        return o && console.warn("Relation=%o; model does not inherit from Backbone.RelationalModel (%o)", this, e), !1
    }, setRelated: function (e, n) {
        this.related = e, this.instance.acquire(), this.instance.set(this.key, e, t.defaults(n || {}, {silent: !0})), this.instance.release()
    }, _isReverseRelation: function (e) {
        return e.instance instanceof this.relatedModel && this.reverseRelation.key === e.key && this.key === e.reverseRelation.key ? !0 : !1
    }, getReverseRelations: function (e) {
        var n = [], r = t.isUndefined(e) ? this.related && (this.related.models || [this.related]) : [e];
        return t.each(r, function (e) {
            t.each(e.getRelations(), function (e) {
                this._isReverseRelation(e) && n.push(e)
            }, this)
        }, this), n
    }, sanitizeOptions: function (e) {
        return e = e ? t.clone(e) : {}, e.silent && (e.silentChange = !0, delete e.silent), e
    }, unsanitizeOptions: function (e) {
        return e = e ? t.clone(e) : {}, e.silentChange && (e.silent = !0, delete e.silentChange), e
    }, destroy: function () {
        n.Relational.store.getCollection(this.instance).unbind("relational:remove", this._modelRemovedFromCollection), n.Relational.store.getCollection(this.relatedModel).unbind("relational:add", this._relatedModelAdded).unbind("relational:remove", this._relatedModelRemoved), t.each(this.getReverseRelations(), function (e) {
            e.removeRelated(this.instance)
        }, this)
    }}), n.HasOne = n.Relation.extend({options: {reverseRelation: {type: "HasMany"}}, initialize: function () {
        t.bindAll(this, "onChange"), this.instance.bind("relational:change:" + this.key, this.onChange);
        var e = this.findRelated({silent: !0});
        this.setRelated(e), t.each(this.getReverseRelations(), function (e) {
            e.addRelated(this.instance)
        }, this)
    }, findRelated: function (e) {
        var t = this.keyContents, n = null;
        if (t instanceof this.relatedModel)n = t; else if (t || t === 0)n = this.relatedModel.findOrCreate(t, {create: this.options.createModels});
        return n
    }, onChange: function (e, r, i) {
        if (this.isLocked())return;
        this.acquire(), i = this.sanitizeOptions(i);
        var s = t.isUndefined(i._related), o = s ? this.related : i._related;
        if (s) {
            this.keyContents = r;
            if (r instanceof this.relatedModel)this.related = r; else if (r) {
                var u = this.findRelated(i);
                this.setRelated(u)
            } else this.setRelated(null)
        }
        o && this.related !== o && t.each(this.getReverseRelations(o), function (e) {
            e.removeRelated(this.instance, i)
        }, this), t.each(this.getReverseRelations(), function (e) {
            e.addRelated(this.instance, i)
        }, this);
        if (!i.silentChange && this.related !== o) {
            var a = this;
            n.Relational.eventQueue.add(function () {
                a.instance.trigger("update:" + a.key, a.instance, a.related, i)
            })
        }
        this.release()
    }, tryAddRelated: function (e, r) {
        if (this.related)return;
        r = this.sanitizeOptions(r);
        var i = this.keyContents;
        if (i || i === 0) {
            var s = n.Relational.store.resolveIdForItem(this.relatedModel, i);
            !t.isNull(s) && e.id === s && this.addRelated(e, r)
        }
    }, addRelated: function (e, t) {
        if (e !== this.related) {
            var n = this.related || null;
            this.setRelated(e), this.onChange(this.instance, e, {_related: n})
        }
    }, removeRelated: function (e, t) {
        if (!this.related)return;
        if (e === this.related) {
            var n = this.related || null;
            this.setRelated(null), this.onChange(this.instance, e, {_related: n})
        }
    }}), n.HasMany = n.Relation.extend({collectionType: null, options: {reverseRelation: {type: "HasOne"}, collectionType: n.Collection, collectionKey: !0, collectionOptions: {}}, initialize: function () {
        t.bindAll(this, "onChange", "handleAddition", "handleRemoval", "handleReset"), this.instance.bind("relational:change:" + this.key, this.onChange), this.collectionType = this.options.collectionType, t.isString(this.collectionType) && (this.collectionType = n.Relational.store.getObjectByName(this.collectionType));
        if (!this.collectionType.prototype instanceof n.Collection)throw new Error("collectionType must inherit from Backbone.Collection");
        this.keyContents instanceof n.Collection ? this.setRelated(this._prepareCollection(this.keyContents)) : this.setRelated(this._prepareCollection()), this.findRelated({silent: !0})
    }, _getCollectionOptions: function () {
        return t.isFunction(this.options.collectionOptions) ? this.options.collectionOptions(this.instance) : this.options.collectionOptions
    }, _prepareCollection: function (e) {
        this.related && this.related.unbind("relational:add", this.handleAddition).unbind("relational:remove", this.handleRemoval).unbind("relational:reset", this.handleReset);
        if (!e || !(e instanceof n.Collection))e = new this.collectionType([], this._getCollectionOptions());
        e.model = this.relatedModel;
        if (this.options.collectionKey) {
            var t = this.options.collectionKey === !0 ? this.options.reverseRelation.key : this.options.collectionKey;
            e[t] && e[t] !== this.instance ? n.Relational.showWarnings && typeof console != "undefined" && console.warn("Relation=%o; collectionKey=%s already exists on collection=%o", this, t, this.options.collectionKey) : t && (e[t] = this.instance)
        }
        return e.bind("relational:add", this.handleAddition).bind("relational:remove", this.handleRemoval).bind("relational:reset", this.handleReset), e
    }, findRelated: function (e) {
        if (this.keyContents) {
            var r = [];
            this.keyContents instanceof n.Collection ? r = this.keyContents.models : (this.keyContents = t.isArray(this.keyContents) ? this.keyContents : [this.keyContents], t.each(this.keyContents, function (e) {
                var t = null;
                if (e instanceof this.relatedModel)t = e; else if (e || e === 0)t = this.relatedModel.findOrCreate(e, {create: this.options.createModels});
                t && !this.related.getByCid(t) && !this.related.get(t) && r.push(t)
            }, this)), r.length && (e = this.unsanitizeOptions(e), this.related.add(r, e))
        }
    }, onChange: function (e, r, i) {
        i = this.sanitizeOptions(i), this.keyContents = r, t.each(this.getReverseRelations(), function (e) {
            e.removeRelated(this.instance, i)
        }, this);
        if (r instanceof n.Collection)this._prepareCollection(r), this.related = r; else {
            var s;
            this.related instanceof n.Collection ? (s = this.related, s.remove(s.models)) : s = this._prepareCollection(), this.setRelated(s), this.findRelated(i)
        }
        t.each(this.getReverseRelations(), function (e) {
            e.addRelated(this.instance, i)
        }, this);
        var o = this;
        n.Relational.eventQueue.add(function () {
            !i.silentChange && o.instance.trigger("update:" + o.key, o.instance, o.related, i)
        })
    }, tryAddRelated: function (e, r) {
        r = this.sanitizeOptions(r);
        if (!this.related.getByCid(e) && !this.related.get(e)) {
            var i = t.any(this.keyContents, function (r) {
                var i = n.Relational.store.resolveIdForItem(this.relatedModel, r);
                return!t.isNull(i) && i === e.id
            }, this);
            i && this.related.add(e, r)
        }
    }, handleAddition: function (e, r, i) {
        if (!(e instanceof n.Model))return;
        i = this.sanitizeOptions(i), t.each(this.getReverseRelations(e), function (e) {
            e.addRelated(this.instance, i)
        }, this);
        var s = this;
        n.Relational.eventQueue.add(function () {
            !i.silentChange && s.instance.trigger("add:" + s.key, e, s.related, i)
        })
    }, handleRemoval: function (e, r, i) {
        if (!(e instanceof n.Model))return;
        i = this.sanitizeOptions(i), t.each(this.getReverseRelations(e), function (e) {
            e.removeRelated(this.instance, i)
        }, this);
        var s = this;
        n.Relational.eventQueue.add(function () {
            !i.silentChange && s.instance.trigger("remove:" + s.key, e, s.related, i)
        })
    }, handleReset: function (e, t) {
        t = this.sanitizeOptions(t);
        var r = this;
        n.Relational.eventQueue.add(function () {
            !t.silentChange && r.instance.trigger("reset:" + r.key, r.related, t)
        })
    }, addRelated: function (e, t) {
        var n = this;
        t = this.unsanitizeOptions(t), e.queue(function () {
            n.related && !n.related.getByCid(e) && !n.related.get(e) && n.related.add(e, t)
        })
    }, removeRelated: function (e, t) {
        t = this.unsanitizeOptions(t), (this.related.getByCid(e) || this.related.get(e)) && this.related.remove(e, t)
    }}), n.RelationalModel = n.Model.extend({relations: null, _relations: null, _isInitialized: !1, _deferProcessing: !1, _queue: null, subModelTypeAttribute: "type", subModelTypes: null, constructor: function (e, r) {
        var i = this;
        if (r && r.collection) {
            this._deferProcessing = !0;
            var s = function (e) {
                e === i && (i._deferProcessing = !1, i.processQueue(), r.collection.unbind("relational:add", s))
            };
            r.collection.bind("relational:add", s), t.defer(function () {
                s(i)
            })
        }
        this._queue = new n.BlockingQueue, this._queue.block(), n.Relational.eventQueue.block(), n.Model.apply(this, arguments), n.Relational.eventQueue.unblock()
    }, trigger: function (e) {
        if (e.length > 5 && "change" === e.substr(0, 6)) {
            var t = this, r = arguments;
            n.Relational.eventQueue.add(function () {
                n.Model.prototype.trigger.apply(t, r)
            })
        } else n.Model.prototype.trigger.apply(this, arguments);
        return this
    }, initializeRelations: function () {
        this.acquire(), this._relations = [], t.each(this.relations, function (e) {
            var r = t.isString(e.type) ? n[e.type] || n.Relational.store.getObjectByName(e.type) : e.type;
            r && r.prototype instanceof n.Relation ? new r(this, e) : n.Relational.showWarnings && typeof console != "undefined" && console.warn("Relation=%o; missing or invalid type!", e)
        }, this), this._isInitialized = !0, this.release(), this.processQueue()
    }, updateRelations: function (e) {
        this._isInitialized && !this.isLocked() && t.each(this._relations, function (t) {
            var n = this.attributes[t.keySource] || this.attributes[t.key];
            t.related !== n && this.trigger("relational:change:" + t.key, this, n, e || {})
        }, this)
    }, queue: function (e) {
        this._queue.add(e)
    }, processQueue: function () {
        this._isInitialized && !this._deferProcessing && this._queue.isBlocked() && this._queue.unblock()
    }, getRelation: function (e) {
        return t.detect(this._relations, function (t) {
            if (t.key === e)return!0
        }, this)
    }, getRelations: function () {
        return this._relations
    }, fetchRelated: function (e, r, i) {
        r || (r = {});
        var s, o = [], u = this.getRelation(e), a = u && u.keyContents, f = a && t.select(t.isArray(a) ? a : [a], function (e) {
            var r = n.Relational.store.resolveIdForItem(u.relatedModel, e);
            return!t.isNull(r) && (i || !n.Relational.store.find(u.relatedModel, r))
        }, this);
        if (f && f.length) {
            var l = t.map(f, function (e) {
                var n;
                if (t.isObject(e))n = u.relatedModel.build(e); else {
                    var r = {};
                    r[u.relatedModel.prototype.idAttribute] = e, n = u.relatedModel.build(r)
                }
                return n
            }, this);
            u.related instanceof n.Collection && t.isFunction(u.related.url) && (s = u.related.url(l));
            if (s && s !== u.related.url()) {
                var c = t.defaults({error: function () {
                    var e = arguments;
                    t.each(l, function (t) {
                        t.trigger("destroy", t, t.collection, r), r.error && r.error.apply(t, e)
                    })
                }, url: s}, r, {add: !0});
                o = [u.related.fetch(c)]
            } else o = t.map(l, function (e) {
                var n = t.defaults({error: function () {
                    e.trigger("destroy", e, e.collection, r), r.error && r.error.apply(e, arguments)
                }}, r);
                return e.fetch(n)
            }, this)
        }
        return o
    }, set: function (e, r, i) {
        n.Relational.eventQueue.block();
        var s;
        t.isObject(e) || e == null ? (s = e, i = r) : (s = {}, s[e] = r);
        var o = n.Model.prototype.set.apply(this, arguments);
        return!this._isInitialized && !this.isLocked() ? (this.constructor.initializeModelHierarchy(), n.Relational.store.register(this), this.initializeRelations()) : s && this.idAttribute in s && n.Relational.store.update(this), s && this.updateRelations(i), n.Relational.eventQueue.unblock(), o
    }, unset: function (e, t) {
        n.Relational.eventQueue.block();
        var r = n.Model.prototype.unset.apply(this, arguments);
        return this.updateRelations(t), n.Relational.eventQueue.unblock(), r
    }, clear: function (e) {
        n.Relational.eventQueue.block();
        var t = n.Model.prototype.clear.apply(this, arguments);
        return this.updateRelations(e), n.Relational.eventQueue.unblock(), t
    }, change: function (e) {
        var t = this, r = arguments;
        n.Relational.eventQueue.add(function () {
            n.Model.prototype.change.apply(t, r)
        })
    }, clone: function () {
        var e = t.clone(this.attributes);
        return t.isUndefined(e[this.idAttribute]) || (e[this.idAttribute] = null), t.each(this.getRelations(), function (t) {
            delete e[t.key]
        }), new this.constructor(e)
    }, toJSON: function () {
        if (this.isLocked())return this.id;
        this.acquire();
        var e = n.Model.prototype.toJSON.call(this);
        return this.constructor._superModel && !(this.constructor._subModelTypeAttribute in e) && (e[this.constructor._subModelTypeAttribute] = this.constructor._subModelTypeValue), t.each(this._relations, function (r) {
            var i = e[r.key];
            if (r.options.includeInJSON === !0)i && t.isFunction(i.toJSON) ? e[r.keyDestination] = i.toJSON() : e[r.keyDestination] = null; else if (t.isString(r.options.includeInJSON))i instanceof n.Collection ? e[r.keyDestination] = i.pluck(r.options.includeInJSON) : i instanceof n.Model ? e[r.keyDestination] = i.get(r.options.includeInJSON) : e[r.keyDestination] = null; else if (t.isArray(r.options.includeInJSON))if (i instanceof n.Collection) {
                var s = [];
                i.each(function (e) {
                    var n = {};
                    t.each(r.options.includeInJSON, function (t) {
                        n[t] = e.get(t)
                    }), s.push(n)
                }), e[r.keyDestination] = s
            } else if (i instanceof n.Model) {
                var s = {};
                t.each(r.options.includeInJSON, function (e) {
                    s[e] = i.get(e)
                }), e[r.keyDestination] = s
            } else e[r.keyDestination] = null; else delete e[r.key];
            r.keyDestination !== r.key && delete e[r.key]
        }), this.release(), e
    }}, {setup: function (e) {
        return this.prototype.relations = (this.prototype.relations || []).slice(0), this._subModels = {}, this._superModel = null, this.prototype.hasOwnProperty("subModelTypes") ? n.Relational.store.addSubModels(this.prototype.subModelTypes, this) : this.prototype.subModelTypes = null, t.each(this.prototype.relations, function (e) {
            e.model || (e.model = this);
            if (e.reverseRelation && e.model === this) {
                var r = !0;
                if (t.isString(e.relatedModel)) {
                    var i = n.Relational.store.getObjectByName(e.relatedModel);
                    r = i && i.prototype instanceof n.RelationalModel
                }
                var s = t.isString(e.type) ? n[e.type] || n.Relational.store.getObjectByName(e.type) : e.type;
                r && s && s.prototype instanceof n.Relation && new s(null, e)
            }
        }, this), this
    }, build: function (e, t) {
        var n = this;
        this.initializeModelHierarchy();
        if (this._subModels && this.prototype.subModelTypeAttribute in e) {
            var r = e[this.prototype.subModelTypeAttribute], i = this._subModels[r];
            i && (n = i)
        }
        return new n(e, t)
    }, initializeModelHierarchy: function () {
        if (t.isUndefined(this._superModel) || t.isNull(this._superModel)) {
            n.Relational.store.setupSuperModel(this);
            if (this._superModel) {
                if (this._superModel.prototype.relations) {
                    var e = t.any(this.prototype.relations, function (e) {
                        return e.model && e.model !== this
                    }, this);
                    e || (this.prototype.relations = this._superModel.prototype.relations.concat(this.prototype.relations))
                }
            } else this._superModel = !1
        }
        this.prototype.subModelTypes && t.keys(this.prototype.subModelTypes).length !== t.keys(this._subModels).length && t.each(this.prototype.subModelTypes, function (e) {
            var t = n.Relational.store.getObjectByName(e);
            t && t.initializeModelHierarchy()
        })
    }, findOrCreate: function (e, r) {
        var i = n.Relational.store.find(this, e);
        if (t.isObject(e))if (i)i.set(i.parse ? i.parse(e) : e, r); else if (!r || r && r.create !== !1)i = this.build(e, r);
        return i
    }}), t.extend(n.RelationalModel.prototype, n.Semaphore), n.Collection.prototype.__prepareModel = n.Collection.prototype._prepareModel, n.Collection.prototype._prepareModel = function (e, t) {
        t || (t = {});
        if (e instanceof n.Model)e.collection || (e.collection = this); else {
            var r = e;
            t.collection = this, typeof this.model.findOrCreate != "undefined" ? e = this.model.findOrCreate(r, t) : e = new this.model(r, t), e._validate(e.attributes, t) || (e = !1)
        }
        return e
    };
    var i = n.Collection.prototype.__add = n.Collection.prototype.add;
    n.Collection.prototype.add = function (e, r) {
        r || (r = {}), t.isArray(e) || (e = [e]);
        var s = [];
        return t.each(e, function (e) {
            e instanceof n.Model || (e = n.Collection.prototype._prepareModel.call(this, e, r)), e instanceof n.Model && !this.get(e) && !this.getByCid(e) && s.push(e)
        }, this), s.length && (i.call(this, s, r), t.each(s, function (e) {
            this.trigger("relational:add", e, this, r)
        }, this)), this
    };
    var s = n.Collection.prototype.__remove = n.Collection.prototype.remove;
    n.Collection.prototype.remove = function (e, r) {
        return r || (r = {}), t.isArray(e) ? e = e.slice(0) : e = [e], t.each(e, function (e) {
            e = this.getByCid(e) || this.get(e), e instanceof n.Model && (s.call(this, e, r), this.trigger("relational:remove", e, this, r))
        }, this), this
    };
    var o = n.Collection.prototype.__reset = n.Collection.prototype.reset;
    n.Collection.prototype.reset = function (e, t) {
        return o.call(this, e, t), this.trigger("relational:reset", this, t), this
    };
    var u = n.Collection.prototype.__sort = n.Collection.prototype.sort;
    n.Collection.prototype.sort = function (e) {
        return u.call(this, e), this.trigger("relational:reset", this, e), this
    };
    var a = n.Collection.prototype.__trigger = n.Collection.prototype.trigger;
    n.Collection.prototype.trigger = function (e) {
        if (e === "add" || e === "remove" || e === "reset") {
            var r = this, i = arguments;
            e === "add" && (i = t.toArray(i), t.isObject(i[3]) && (i[3] = t.clone(i[3]))), n.Relational.eventQueue.add(function () {
                a.apply(r, i)
            })
        } else a.apply(this, arguments);
        return this
    }, n.RelationalModel.extend = function (e, t) {
        var r = n.Model.extend.apply(this, arguments);
        return r.setup(this), r
    };
    var f = n.Collection.prototype.fetch;
    n.Collection.prototype.fetch = function (e) {
        e || (e = {}), t.defaults(e, {add: !0}), e.reset && this.reset();
        var n = this, r = f.call(this, e);
        return r.done(function () {
            e.silent || n.trigger("fetch", n, e)
        }), r
    }
})();


/* public/javascripts/backbone/backbone-subset.js @ 1384554040 */
/**
 * @class  Backbone.Subset
 * @name   Backbone Subset collections
 * @desc   Implements a collection that is a subset other Backbone Collections
 */
(function () {
    function t(e, t) {
        return _.difference(_.union(e, t), _.intersection(e, t))
    }

    var e = {};
    Backbone.Subset = function (t, n) {
        var r;
        n = n || {}, n.parent && (this.parent = n.parent);
        if (!(r = _.result(this, "parent")))throw new Error("Can't create a subset without a parent collection");
        this.model = r.model, this.comparator = this.comparator || n.comparator || r.comparator, this.liveupdate_keys = this.liveupdate_keys || n.liveupdate_keys || "none", _.bindAll(this, "_onModelEvent", "_unbindModelEvents", "_proxyAdd", "_proxyReset", "_proxyRemove", "_proxyChange"), r.bind("add", this._proxyAdd), r.bind("remove", this._proxyRemove), r.bind("reset", this._proxyReset), r.bind("all", this._proxyChange), this.beforeInitialize && this.beforeInitialize.apply(this, arguments), n.no_reset ? this._resetSubset({silent: !0}) : (this._reset(), this.reset(t || r.models, {silent: !0})), this.initialize.apply(this, arguments)
    }, e.exclusiveSubset = function () {
        return!1
    }, e.reset = function (e, n) {
        var r = _.result(this, "parent"), i = _.clone(r.models), s, o = this.pluck("id");
        return e = e || [], e = _.isArray(e) ? e : [e], n = n || {}, i = _.reject(i, function (e) {
            return _.include(o, e.id)
        }), _.each(e, function (e) {
            i.push(e)
        }), s = t(o, _.pluck(e, "id")), r.reset(i, _.extend({silent: !0}, n)), this.exclusiveSubset() ? r.trigger("reset", this, _.extend({model_ids: s, exclusive_collection: this}, n)) : r.trigger("reset", this, _.extend({model_ids: s}, n)), this
    }, e.recalculate = function (e) {
        e = e || {};
        var t, n = this;
        return t = _.result(this, "parent").reduce(function (e, t) {
            return n._updateModelMembership(t, {silent: !0}) || e
        }, !1), t && !e.silent && this.trigger("reset", this, e), this
    }, e._resetSubset = function (e) {
        return e = e || {}, this.each(this._unbindModelEvents), this._reset(), _.result(this, "parent").each(function (e) {
            this._addToSubset(e, {silent: !0})
        }, this), e.silent || this.trigger("reset", this, e), this
    }, e.add = function (e, t) {
        return this.exclusiveSubset() && (t = _.extend(t, {exclusive_collection: this})), _.result(this, "parent").add(e, t)
    }, e._addToSubset = function (e, t) {
        if (this.sieve(e))return Backbone.Collection.prototype.add.call(this, e, t)
    }, e.remove = function (e, t) {
        return this.exclusiveSubset() && (t = _.extend(t, {exclusive_collection: this})), _.result(this, "parent").remove(e, t)
    }, e._removeFromSubset = function (e, t) {
        return Backbone.Collection.prototype.remove.call(this, e, t)
    }, e._prepareModel = function (e, t) {
        var n = _.result(this, "parent");
        if (e instanceof Backbone.Model)e.collection || (e.collection = n); else {
            var r = e;
            e = new this.model(r, {collection: n}), e.validate && !e._performValidation(e.attributes, t) && (e = !1)
        }
        return e = this.sieve(e) ? e : !1, e
    }, e._proxyAdd = function (e, t, n) {
        n = n || {};
        if (n.exclusive_collection && n.exclusive_collection !== this)return;
        t !== this && this.sieve(e) && !n.noproxy && this._addToSubset(e, n)
    }, e._proxyRemove = function (e, t, n) {
        n = n || {};
        if (n.exclusive_collection && n.exclusive_collection !== this)return;
        t !== this && this.sieve(e) && !n.noproxy && this._removeFromSubset(e, n)
    }, e._proxyChange = function (e, t, n) {
        n !== this && e === "change" && this.liveupdate_keys === "all" ? this._updateModelMembership(t) : e.slice(0, 7) === "change:" && _.isArray(this.liveupdate_keys) && _.include(this.liveupdate_keys, e.slice(7)) && this._updateModelMembership(t)
    }, e._proxyReset = function (e, t) {
        function i() {
            return _.filter(t.model_ids, function (e) {
                var t = r.parent().get(e) || r.get(e);
                return t && r.sieve(t)
            })
        }

        t = t || {};
        var n, r = this;
        if (t.exclusive_collection && t.exclusive_collection !== this)return;
        (!t || !t.noproxy) && (!t.model_ids || this === e || i().length) && this._resetSubset(_.extend(_.clone(t), {proxied: !0}))
    }, e._updateModelMembership = function (e, t) {
        var n = !e.id, r = this._byCid[e.cid] || n && this._byId[e.id];
        if (this.sieve(e)) {
            if (!r)return this._addToSubset(e, t), !0
        } else if (r)return this._removeFromSubset(e, t), !0;
        return!1
    }, e._unbindModelEvents = function (e) {
        e.unbind("all", this._onModelEvent)
    }, _.extend(Backbone.Subset.prototype, Backbone.Collection.prototype, e), Backbone.Subset.extend = Backbone.Collection.extend
})();


/* public/javascripts/jquery.transify.js @ 1384554040 */
// Transify v1.0
// COPYRIGHT JOREN RAPINI 2010
// jorenrapini@gmail.com
(function (e) {
    e.fn.untransify = function (t) {
        return this.each(function () {
            var t = e(this);
            t.data("transify_prev") && (oldCSS = {zIndex: t.data("transify_prev").zIndex, background: t.data("transify_prev").background, border: t.data("transify_prev").border}, t.css(oldCSS), t.children(".transify").remove())
        }), this
    }
})(jQuery), function (e) {
    e.fn.transify = function (t) {
        var n = {opacityOrig: .5, fadeSpeed: 600}, r = e.extend({}, n, t);
        return this.each(function () {
            var t = e(this);
            t.append('<div class="transify"></div>');
            var n = t.find(".transify");
            n.css({backgroundColor: t.css("backgroundColor"), backgroundImage: t.css("backgroundImage"), backgroundRepeat: t.css("backgroundRepeat"), borderTopColor: t.css("borderTopColor"), borderTopWidth: t.css("borderTopWidth"), borderTopStyle: t.css("borderTopStyle"), borderRightColor: t.css("borderRightColor"), borderRightWidth: t.css("borderRightWidth"), borderRightStyle: t.css("borderRightStyle"), borderBottomColor: t.css("borderBottomColor"), borderBottomWidth: t.css("borderBottomWidth"), borderBottomStyle: t.css("borderBottomStyle"), borderLeftColor: t.css("borderLeftColor"), borderLeftWidth: t.css("borderLeftWidth"), borderLeftStyle: t.css("borderLeftStyle"), position: "absolute", top: 0, left: 0, zIndex: -1, width: t.width() + parseInt(t.css("padding-left"), 10) + parseInt(t.css("padding-right"), 10), height: t.height() + parseInt(t.css("padding-top"), 10) + parseInt(t.css("padding-bottom"), 10), opacity: r.opacityOrig}), r.percentWidth && n.css("width", r.percentWidth), t.data("transify_prev", {zIndex: t.css("zIndex"), background: t.css("background"), border: t.css("border")}), t.css({zIndex: 4, background: "none", border: "none"}), r.opacityNew && t.hover(function () {
                n.stop().animate({opacity: r.opacityNew}, r.fadeSpeed)
            }, function () {
                n.stop().animate({opacity: r.opacityOrig}, r.fadeSpeed)
            })
        }), this
    }
}(jQuery);


/* public/javascripts/jquery.nearest.js @ 1384554040 */
/*!
 * jQuery Nearest plugin v1.2.0
 *
 * Finds elements closest to a single point based on screen location and pixel dimensions
 * http://gilmoreorless.github.com/jquery-nearest/
 * Open source under the MIT licence: http://gilmoreorless.mit-license.org/2011/
 *
 * Requires jQuery 1.4 or above
 * Also supports Ben Alman's "each2" plugin for faster looping (if available)
 */
/**
 * Method signatures:
 *
 * $.nearest({x, y}, selector) - find $(selector) closest to point
 * $(elem).nearest(selector) - find $(selector) closest to elem
 * $(elemSet).nearest({x, y}) - filter $(elemSet) and return closest to point
 *
 * Also:
 * $.furthest()
 * $(elem).furthest()
 *
 * $.touching()
 * $(elem).touching()
 */
(function (e, t) {
    function r(t, r, i) {
        t || (t = "div");
        var s = e(r.container), o = s.offset() || {left: 0, top: 0}, u = [o.left + s.width(), o.top + s.height()], a = {x: 0, y: 1, w: 0, h: 1}, f, l;
        for (f in a)a.hasOwnProperty(f) && (l = n.exec(r[f]), l && (r[f] = u[a[f]] * l[1] / 100));
        var c = e(t), h = [], p = !!r.furthest, d = !!r.checkHoriz, v = !!r.checkVert, m = p ? 0 : Infinity, g = parseFloat(r.x) || 0, y = parseFloat(r.y) || 0, b = parseFloat(g + r.w) || g, w = parseFloat(y + r.h) || y, E = r.tolerance || 0, S = !!e.fn.each2, x = Math.min, T = Math.max;
        !r.includeSelf && i && (c = c.not(i)), E < 0 && (E = 0), c[S ? "each2" : "each"](function (t, n) {
            var r = S ? n : e(this), i = r.offset(), s = i.left, o = i.top, u = r.outerWidth(), a = r.outerHeight(), f = s + u, l = o + a, c = T(s, g), N = x(f, b), C = T(o, y), k = x(l, w), L = N >= c, A = k >= C, O, M, _, D;
            if (d && v || !d && !v && L && A || d && A || v && L)O = L ? 0 : c - N, M = A ? 0 : C - k, _ = L || A ? T(O, M) : Math.sqrt(O * O + M * M), D = p ? _ >= m - E : _ <= m + E, D && (m = p ? T(m, _) : x(m, _), h.push({node: this, dist: _}))
        });
        var N = h.length, C = [], k, L, A, O;
        if (N) {
            p ? (k = m - E, L = m) : (k = m, L = m + E);
            for (A = 0; A < N; A++)O = h[A], O.dist >= k && O.dist <= L && C.push(O.node)
        }
        return C
    }

    var n = /^([\d.]+)%$/;
    e.each(["nearest", "furthest", "touching"], function (n, i) {
        var s = {x: 0, y: 0, w: 0, h: 0, tolerance: 1, container: document, furthest: i == "furthest", includeSelf: !1, checkHoriz: i != "touching", checkVert: i != "touching"};
        e[i] = function (n, i, o) {
            if (!n || n.x === t || n.y === t)return e([]);
            var u = e.extend({}, s, n, o || {});
            return e(r(i, u))
        }, e.fn[i] = function (t, n) {
            var i;
            if (t && e.isPlainObject(t))return i = e.extend({}, s, t, n || {}), this.pushStack(r(this, i));
            var o = this.offset(), u = {x: o.left, y: o.top, w: this.outerWidth(), h: this.outerHeight()};
            return i = e.extend({}, s, u, n || {}), this.pushStack(r(t, i, this))
        }
    })
})(jQuery);


/* public/javascripts/moment.js @ 1384554040 */
// moment.js
// version : 1.7.2
// author : Tim Wood
// license : MIT
// momentjs.com
(function (e) {
    function O(e, t, n, r) {
        var i = n.lang();
        return i[e].call ? i[e](n, r) : i[e][t]
    }

    function M(e, t) {
        return function (n) {
            return B(e.call(this, n), t)
        }
    }

    function _(e) {
        return function (t) {
            var n = e.call(this, t);
            return n + this.lang().ordinal(n)
        }
    }

    function D(e, t, n) {
        this._d = e, this._isUTC = !!t, this._a = e._a || null, this._lang = n || !1
    }

    function P(e) {
        var t = this._data = {}, n = e.years || e.y || 0, r = e.months || e.M || 0, i = e.weeks || e.w || 0, s = e.days || e.d || 0, o = e.hours || e.h || 0, u = e.minutes || e.m || 0, a = e.seconds || e.s || 0, f = e.milliseconds || e.ms || 0;
        this._milliseconds = f + a * 1e3 + u * 6e4 + o * 36e5, this._days = s + i * 7, this._months = r + n * 12, t.milliseconds = f % 1e3, a += H(f / 1e3), t.seconds = a % 60, u += H(a / 60), t.minutes = u % 60, o += H(u / 60), t.hours = o % 24, s += H(o / 24), s += i * 7, t.days = s % 30, r += H(s / 30), t.months = r % 12, n += H(r / 12), t.years = n, this._lang = !1
    }

    function H(e) {
        return e < 0 ? Math.ceil(e) : Math.floor(e)
    }

    function B(e, t) {
        var n = e + "";
        while (n.length < t)n = "0" + n;
        return n
    }

    function j(e, t, n) {
        var r = t._milliseconds, i = t._days, s = t._months, o;
        r && e._d.setTime(+e + r * n), i && e.date(e.date() + i * n), s && (o = e.date(), e.date(1).month(e.month() + s * n).date(Math.min(o, e.daysInMonth())))
    }

    function F(e) {
        return Object.prototype.toString.call(e) === "[object Array]"
    }

    function I(e, t) {
        var n = Math.min(e.length, t.length), r = Math.abs(e.length - t.length), i = 0, s;
        for (s = 0; s < n; s++)~~e[s] !== ~~t[s] && i++;
        return i + r
    }

    function q(e, t, n, r) {
        var i, s, o = [];
        for (i = 0; i < 7; i++)o[i] = e[i] = e[i] == null ? i === 2 ? 1 : 0 : e[i];
        return e[7] = o[7] = t, e[8] != null && (o[8] = e[8]), e[3] += n || 0, e[4] += r || 0, s = new Date(0), t ? (s.setUTCFullYear(e[0], e[1], e[2]), s.setUTCHours(e[3], e[4], e[5], e[6])) : (s.setFullYear(e[0], e[1], e[2]), s.setHours(e[3], e[4], e[5], e[6])), s._a = o, s
    }

    function R(e, n) {
        var r, i, o = [];
        !n && u && (n = require("./lang/" + e));
        for (r = 0; r < a.length; r++)n[a[r]] = n[a[r]] || s.en[a[r]];
        for (r = 0; r < 12; r++)i = t([2e3, r]), o[r] = new RegExp("^" + (n.months[r] || n.months(i, "")) + "|^" + (n.monthsShort[r] || n.monthsShort(i, "")).replace(".", ""), "i");
        return n.monthsParse = n.monthsParse || o, s[e] = n, n
    }

    function U(e) {
        var n = typeof e == "string" && e || e && e._lang || null;
        return n ? s[n] || R(n) : t
    }

    function z(e) {
        return e.match(/\[.*\]/) ? e.replace(/^\[|\]$/g, "") : e.replace(/\\/g, "")
    }

    function W(e) {
        var t = e.match(l), n, r;
        for (n = 0, r = t.length; n < r; n++)A[t[n]] ? t[n] = A[t[n]] : t[n] = z(t[n]);
        return function (i) {
            var s = "";
            for (n = 0; n < r; n++)s += typeof t[n].call == "function" ? t[n].call(i, e) : t[n];
            return s
        }
    }

    function X(e, t) {
        function r(t) {
            return e.lang().longDateFormat[t] || t
        }

        var n = 5;
        while (n-- && c.test(t))t = t.replace(c, r);
        return C[t] || (C[t] = W(t)), C[t](e)
    }

    function V(e) {
        switch (e) {
            case"DDDD":
                return v;
            case"YYYY":
                return m;
            case"S":
            case"SS":
            case"SSS":
            case"DDD":
                return d;
            case"MMM":
            case"MMMM":
            case"dd":
            case"ddd":
            case"dddd":
            case"a":
            case"A":
                return g;
            case"Z":
            case"ZZ":
                return y;
            case"T":
                return b;
            case"MM":
            case"DD":
            case"YY":
            case"HH":
            case"hh":
            case"mm":
            case"ss":
            case"M":
            case"D":
            case"d":
            case"H":
            case"h":
            case"m":
            case"s":
                return p;
            default:
                return new RegExp(e.replace("\\", ""))
        }
    }

    function $(e, t, n, r) {
        var i, s;
        switch (e) {
            case"M":
            case"MM":
                n[1] = t == null ? 0 : ~~t - 1;
                break;
            case"MMM":
            case"MMMM":
                for (i = 0; i < 12; i++)if (U().monthsParse[i].test(t)) {
                    n[1] = i, s = !0;
                    break
                }
                s || (n[8] = !1);
                break;
            case"D":
            case"DD":
            case"DDD":
            case"DDDD":
                t != null && (n[2] = ~~t);
                break;
            case"YY":
                n[0] = ~~t + (~~t > 70 ? 1900 : 2e3);
                break;
            case"YYYY":
                n[0] = ~~Math.abs(t);
                break;
            case"a":
            case"A":
                r.isPm = (t + "").toLowerCase() === "pm";
                break;
            case"H":
            case"HH":
            case"h":
            case"hh":
                n[3] = ~~t;
                break;
            case"m":
            case"mm":
                n[4] = ~~t;
                break;
            case"s":
            case"ss":
                n[5] = ~~t;
                break;
            case"S":
            case"SS":
            case"SSS":
                n[6] = ~~(("0." + t) * 1e3);
                break;
            case"Z":
            case"ZZ":
                r.isUTC = !0, i = (t + "").match(x), i && i[1] && (r.tzh = ~~i[1]), i && i[2] && (r.tzm = ~~i[2]), i && i[0] === "+" && (r.tzh = -r.tzh, r.tzm = -r.tzm)
        }
        t == null && (n[8] = !1)
    }

    function J(e, t) {
        var n = [0, 0, 1, 0, 0, 0, 0], r = {tzh: 0, tzm: 0}, i = t.match(l), s, o;
        for (s = 0; s < i.length; s++)o = (V(i[s]).exec(e) || [])[0], o && (e = e.slice(e.indexOf(o) + o.length)), A[i[s]] && $(i[s], o, n, r);
        return r.isPm && n[3] < 12 && (n[3] += 12), r.isPm === !1 && n[3] === 12 && (n[3] = 0), q(n, r.isUTC, r.tzh, r.tzm)
    }

    function K(e, t) {
        var n, r = e.match(h) || [], i, s = 99, o, u, a;
        for (o = 0; o < t.length; o++)u = J(e, t[o]), i = X(new D(u), t[o]).match(h) || [], a = I(r, i), a < s && (s = a, n = u);
        return n
    }

    function Q(e) {
        var t = "YYYY-MM-DDT", n;
        if (w.exec(e)) {
            for (n = 0; n < 4; n++)if (S[n][1].exec(e)) {
                t += S[n][0];
                break
            }
            return y.exec(e) ? J(e, t + " Z") : J(e, t)
        }
        return new Date(e)
    }

    function G(e, t, n, r, i) {
        var s = i.relativeTime[e];
        return typeof s == "function" ? s(t || 1, !!n, e, r) : s.replace(/%d/i, t || 1)
    }

    function Y(e, t, n) {
        var i = r(Math.abs(e) / 1e3), s = r(i / 60), o = r(s / 60), u = r(o / 24), a = r(u / 365), f = i < 45 && ["s", i] || s === 1 && ["m"] || s < 45 && ["mm", s] || o === 1 && ["h"] || o < 22 && ["hh", o] || u === 1 && ["d"] || u <= 25 && ["dd", u] || u <= 45 && ["M"] || u < 345 && ["MM", r(u / 30)] || a === 1 && ["y"] || ["yy", a];
        return f[2] = t, f[3] = e > 0, f[4] = n, G.apply({}, f)
    }

    function Z(e, n) {
        t.fn[e] = function (e) {
            var t = this._isUTC ? "UTC" : "";
            return e != null ? (this._d["set" + t + n](e), this) : this._d["get" + t + n]()
        }
    }

    function et(e) {
        t.duration.fn[e] = function () {
            return this._data[e]
        }
    }

    function tt(e, n) {
        t.duration.fn["as" + e] = function () {
            return+this / n
        }
    }

    var t, n = "1.7.2", r = Math.round, i, s = {}, o = "en", u = typeof module != "undefined" && module.exports, a = "months|monthsShort|weekdays|weekdaysShort|weekdaysMin|longDateFormat|calendar|relativeTime|ordinal|meridiem".split("|"), f = /^\/?Date\((\-?\d+)/i, l = /(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|YYYY|YY|a|A|hh?|HH?|mm?|ss?|SS?S?|zz?|ZZ?|.)/g, c = /(\[[^\[]*\])|(\\)?(LT|LL?L?L?)/g, h = /([0-9a-zA-Z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)/gi, p = /\d\d?/, d = /\d{1,3}/, v = /\d{3}/, m = /\d{1,4}/, g = /[0-9a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+/i, y = /Z|[\+\-]\d\d:?\d\d/i, b = /T/i, w = /^\s*\d{4}-\d\d-\d\d(T(\d\d(:\d\d(:\d\d(\.\d\d?\d?)?)?)?)?([\+\-]\d\d:?\d\d)?)?/, E = "YYYY-MM-DDTHH:mm:ssZ", S = [
        ["HH:mm:ss.S", /T\d\d:\d\d:\d\d\.\d{1,3}/],
        ["HH:mm:ss", /T\d\d:\d\d:\d\d/],
        ["HH:mm", /T\d\d:\d\d/],
        ["HH", /T\d\d/]
    ], x = /([\+\-]|\d\d)/gi, T = "Month|Date|Hours|Minutes|Seconds|Milliseconds".split("|"), N = {Milliseconds: 1, Seconds: 1e3, Minutes: 6e4, Hours: 36e5, Days: 864e5, Months: 2592e6, Years: 31536e6}, C = {}, k = "DDD w M D d".split(" "), L = "M D H h m s w".split(" "), A = {M: function () {
        return this.month() + 1
    }, MMM: function (e) {
        return O("monthsShort", this.month(), this, e)
    }, MMMM: function (e) {
        return O("months", this.month(), this, e)
    }, D: function () {
        return this.date()
    }, DDD: function () {
        var e = new Date(this.year(), this.month(), this.date()), t = new Date(this.year(), 0, 1);
        return~~((e - t) / 864e5 + 1.5)
    }, d: function () {
        return this.day()
    }, dd: function (e) {
        return O("weekdaysMin", this.day(), this, e)
    }, ddd: function (e) {
        return O("weekdaysShort", this.day(), this, e)
    }, dddd: function (e) {
        return O("weekdays", this.day(), this, e)
    }, w: function () {
        var e = new Date(this.year(), this.month(), this.date() - this.day() + 5), t = new Date(e.getFullYear(), 0, 4);
        return~~((e - t) / 864e5 / 7 + 1.5)
    }, YY: function () {
        return B(this.year() % 100, 2)
    }, YYYY: function () {
        return B(this.year(), 4)
    }, a: function () {
        return this.lang().meridiem(this.hours(), this.minutes(), !0)
    }, A: function () {
        return this.lang().meridiem(this.hours(), this.minutes(), !1)
    }, H: function () {
        return this.hours()
    }, h: function () {
        return this.hours() % 12 || 12
    }, m: function () {
        return this.minutes()
    }, s: function () {
        return this.seconds()
    }, S: function () {
        return~~(this.milliseconds() / 100)
    }, SS: function () {
        return B(~~(this.milliseconds() / 10), 2)
    }, SSS: function () {
        return B(this.milliseconds(), 3)
    }, Z: function () {
        var e = -this.zone(), t = "+";
        return e < 0 && (e = -e, t = "-"), t + B(~~(e / 60), 2) + ":" + B(~~e % 60, 2)
    }, ZZ: function () {
        var e = -this.zone(), t = "+";
        return e < 0 && (e = -e, t = "-"), t + B(~~(10 * e / 6), 4)
    }};
    while (k.length)i = k.pop(), A[i + "o"] = _(A[i]);
    while (L.length)i = L.pop(), A[i + i] = M(A[i], 2);
    A.DDDD = M(A.DDD, 3), t = function (n, r) {
        if (n === null || n === "")return null;
        var i, s;
        return t.isMoment(n) ? new D(new Date(+n._d), n._isUTC, n._lang) : (r ? F(r) ? i = K(n, r) : i = J(n, r) : (s = f.exec(n), i = n === e ? new Date : s ? new Date(+s[1]) : n instanceof Date ? n : F(n) ? q(n) : typeof n == "string" ? Q(n) : new Date(n)), new D(i))
    }, t.utc = function (e, n) {
        return F(e) ? new D(q(e, !0), !0) : (typeof e == "string" && !y.exec(e) && (e += " +0000", n && (n += " Z")), t(e, n).utc())
    }, t.unix = function (e) {
        return t(e * 1e3)
    }, t.duration = function (e, n) {
        var r = t.isDuration(e), i = typeof e == "number", s = r ? e._data : i ? {} : e, o;
        return i && (n ? s[n] = e : s.milliseconds = e), o = new P(s), r && (o._lang = e._lang), o
    }, t.humanizeDuration = function (e, n, r) {
        return t.duration(e, n === !0 ? null : n).humanize(n === !0 ? !0 : r)
    }, t.version = n, t.defaultFormat = E, t.lang = function (e, n) {
        var r;
        if (!e)return o;
        (n || !s[e]) && R(e, n);
        if (s[e]) {
            for (r = 0; r < a.length; r++)t[a[r]] = s[e][a[r]];
            t.monthsParse = s[e].monthsParse, o = e
        }
    }, t.langData = U, t.isMoment = function (e) {
        return e instanceof D
    }, t.isDuration = function (e) {
        return e instanceof P
    }, t.lang("en", {months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"), monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"), weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"), weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"), weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"), longDateFormat: {LT: "h:mm A", L: "MM/DD/YYYY", LL: "MMMM D YYYY", LLL: "MMMM D YYYY LT", LLLL: "dddd, MMMM D YYYY LT"}, meridiem: function (e, t, n) {
        return e > 11 ? n ? "pm" : "PM" : n ? "am" : "AM"
    }, calendar: {sameDay: "[Today at] LT", nextDay: "[Tomorrow at] LT", nextWeek: "dddd [at] LT", lastDay: "[Yesterday at] LT", lastWeek: "[last] dddd [at] LT", sameElse: "L"}, relativeTime: {future: "in %s", past: "%s ago", s: "a few seconds", m: "a minute", mm: "%d minutes", h: "an hour", hh: "%d hours", d: "a day", dd: "%d days", M: "a month", MM: "%d months", y: "a year", yy: "%d years"}, ordinal: function (e) {
        var t = e % 10;
        return~~(e % 100 / 10) === 1 ? "th" : t === 1 ? "st" : t === 2 ? "nd" : t === 3 ? "rd" : "th"
    }}), t.fn = D.prototype = {clone: function () {
        return t(this)
    }, valueOf: function () {
        return+this._d
    }, unix: function () {
        return Math.floor(+this._d / 1e3)
    }, toString: function () {
        return this._d.toString()
    }, toDate: function () {
        return this._d
    }, toArray: function () {
        var e = this;
        return[e.year(), e.month(), e.date(), e.hours(), e.minutes(), e.seconds(), e.milliseconds(), !!this._isUTC]
    }, isValid: function () {
        return this._a ? this._a[8] != null ? !!this._a[8] : !I(this._a, (this._a[7] ? t.utc(this._a) : t(this._a)).toArray()) : !isNaN(this._d.getTime())
    }, utc: function () {
        return this._isUTC = !0, this
    }, local: function () {
        return this._isUTC = !1, this
    }, format: function (e) {
        return X(this, e ? e : t.defaultFormat)
    }, add: function (e, n) {
        var r = n ? t.duration(+n, e) : t.duration(e);
        return j(this, r, 1), this
    }, subtract: function (e, n) {
        var r = n ? t.duration(+n, e) : t.duration(e);
        return j(this, r, -1), this
    }, diff: function (e, n, i) {
        var s = this._isUTC ? t(e).utc() : t(e).local(), o = (this.zone() - s.zone()) * 6e4, u = this._d - s._d - o, a = this.year() - s.year(), f = this.month() - s.month(), l = this.date() - s.date(), c;
        return n === "months" ? c = a * 12 + f + l / 30 : n === "years" ? c = a + (f + l / 30) / 12 : c = n === "seconds" ? u / 1e3 : n === "minutes" ? u / 6e4 : n === "hours" ? u / 36e5 : n === "days" ? u / 864e5 : n === "weeks" ? u / 6048e5 : u, i ? c : r(c)
    }, from: function (e, n) {
        return t.duration(this.diff(e)).lang(this._lang).humanize(!n)
    }, fromNow: function (e) {
        return this.from(t(), e)
    }, calendar: function () {
        var e = this.diff(t().sod(), "days", !0), n = this.lang().calendar, r = n.sameElse, i = e < -6 ? r : e < -1 ? n.lastWeek : e < 0 ? n.lastDay : e < 1 ? n.sameDay : e < 2 ? n.nextDay : e < 7 ? n.nextWeek : r;
        return this.format(typeof i == "function" ? i.apply(this) : i)
    }, isLeapYear: function () {
        var e = this.year();
        return e % 4 === 0 && e % 100 !== 0 || e % 400 === 0
    }, isDST: function () {
        return this.zone() < t([this.year()]).zone() || this.zone() < t([this.year(), 5]).zone()
    }, day: function (e) {
        var t = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
        return e == null ? t : this.add({d: e - t})
    }, startOf: function (e) {
        switch (e.replace(/s$/, "")) {
            case"year":
                this.month(0);
            case"month":
                this.date(1);
            case"day":
                this.hours(0);
            case"hour":
                this.minutes(0);
            case"minute":
                this.seconds(0);
            case"second":
                this.milliseconds(0)
        }
        return this
    }, endOf: function (e) {
        return this.startOf(e).add(e.replace(/s?$/, "s"), 1).subtract("ms", 1)
    }, sod: function () {
        return this.clone().startOf("day")
    }, eod: function () {
        return this.clone().endOf("day")
    }, zone: function () {
        return this._isUTC ? 0 : this._d.getTimezoneOffset()
    }, daysInMonth: function () {
        return t.utc([this.year(), this.month() + 1, 0]).date()
    }, lang: function (t) {
        return t === e ? U(this) : (this._lang = t, this)
    }};
    for (i = 0; i < T.length; i++)Z(T[i].toLowerCase(), T[i]);
    Z("year", "FullYear"), t.duration.fn = P.prototype = {weeks: function () {
        return H(this.days() / 7)
    }, valueOf: function () {
        return this._milliseconds + this._days * 864e5 + this._months * 2592e6
    }, humanize: function (e) {
        var t = +this, n = this.lang().relativeTime, r = Y(t, !e, this.lang()), i = t <= 0 ? n.past : n.future;
        return e && (typeof i == "function" ? r = i(r) : r = i.replace(/%s/i, r)), r
    }, lang: t.fn.lang};
    for (i in N)N.hasOwnProperty(i) && (tt(i, N[i]), et(i.toLowerCase()));
    tt("Weeks", 6048e5), u && (module.exports = t), typeof ender == "undefined" && (this.moment = t), typeof define == "function" && define.amd && define("moment", [], function () {
        return t
    })
}).call(this);


/* public/javascripts/jquery.jcarousel.js @ 1384554040 */
/*! jCarousel - v0.3.0-beta.5 - 2013-07-10
 * http://sorgalla.com/jcarousel
 * Copyright (c) 2013 Jan Sorgalla; Licensed MIT */
(function (e) {
    "use strict";
    var t = e.jCarousel = {};
    t.version = "0.3.0-beta.5";
    var n = /^([+\-]=)?(.+)$/;
    t.parseTarget = function (e) {
        var t = !1, r = typeof e != "object" ? n.exec(e) : null;
        return r ? (e = parseInt(r[2], 10) || 0, r[1] && (t = !0, r[1] === "-=" && (e *= -1))) : typeof e != "object" && (e = parseInt(e, 10) || 0), {target: e, relative: t}
    }, t.detectCarousel = function (e) {
        var t;
        while (e.size() > 0) {
            t = e.filter("[data-jcarousel]");
            if (t.size() > 0)return t;
            t = e.find("[data-jcarousel]");
            if (t.size() > 0)return t;
            e = e.parent()
        }
        return null
    }, t.base = function (n) {
        return{version: t.version, _options: {}, _element: null, _carousel: null, _init: e.noop, _create: e.noop, _destroy: e.noop, _reload: e.noop, create: function () {
            return this._element.attr("data-" + n.toLowerCase(), !0).data(n, this), !1 === this._trigger("create") ? this : (this._create(), this._trigger("createend"), this)
        }, destroy: function () {
            return!1 === this._trigger("destroy") ? this : (this._destroy(), this._trigger("destroyend"), this._element.removeData(n).removeAttr("data-" + n.toLowerCase()), this)
        }, reload: function (e) {
            return!1 === this._trigger("reload") ? this : (e && this.options(e), this._reload(), this._trigger("reloadend"), this)
        }, element: function () {
            return this._element
        }, options: function (t, n) {
            if (arguments.length === 0)return e.extend({}, this._options);
            if (typeof t == "string") {
                if (typeof n == "undefined")return typeof this._options[t] == "undefined" ? null : this._options[t];
                this._options[t] = n
            } else this._options = e.extend({}, this._options, t);
            return this
        }, carousel: function () {
            return this._carousel || (this._carousel = t.detectCarousel(this.options("carousel") || this._element), this._carousel || e.error('Could not detect carousel for plugin "' + n + '"')), this._carousel
        }, _trigger: function (t, r, i) {
            var s, o = !1;
            return i = [this].concat(i || []), (r || this._element).each(function () {
                s = e.Event((t + "." + n).toLowerCase()), e(this).trigger(s, i), s.isDefaultPrevented() && (o = !0)
            }), !o
        }}
    }, t.plugin = function (n, r) {
        var i = e[n] = function (t, n) {
            this._element = e(t), this.options(n), this._init(), this.create()
        };
        return i.fn = i.prototype = e.extend({}, t.base(n), r), e.fn[n] = function (t) {
            var r = Array.prototype.slice.call(arguments, 1), s = this;
            return typeof t == "string" ? this.each(function () {
                var i = e(this).data(n);
                if (!i)return e.error("Cannot call methods on " + n + " prior to initialization; " + 'attempted to call method "' + t + '"');
                if (!e.isFunction(i[t]) || t.charAt(0) === "_")return e.error('No such method "' + t + '" for ' + n + " instance");
                var o = i[t].apply(i, r);
                if (o !== i && typeof o != "undefined")return s = o, !1
            }) : this.each(function () {
                var r = e(this).data(n);
                r instanceof i ? r.reload(t) : new i(this, t)
            }), s
        }, i
    }
})(jQuery), function (e, t) {
    "use strict";
    var n = function (e) {
        return parseFloat(e) || 0
    };
    e.jCarousel.plugin("jcarousel", {animating: !1, tail: 0, inTail: !1, resizeTimer: null, lt: null, vertical: !1, rtl: !1, circular: !1, underflow: !1, _options: {list: function () {
        return this.element().children().eq(0)
    }, items: function () {
        return this.list().children()
    }, animation: 400, transitions: !1, wrap: null, vertical: null, rtl: null, center: !1}, _list: null, _items: null, _target: null, _first: null, _last: null, _visible: null, _fullyvisible: null, _init: function () {
        var e = this;
        return this.onWindowResize = function () {
            e.resizeTimer && clearTimeout(e.resizeTimer), e.resizeTimer = setTimeout(function () {
                e.reload()
            }, 100)
        }, this
    }, _create: function () {
        this._reload(), e(t).on("resize.jcarousel", this.onWindowResize)
    }, _destroy: function () {
        e(t).off("resize.jcarousel", this.onWindowResize)
    }, _reload: function () {
        this.vertical = this.options("vertical"), this.vertical == null && (this.vertical = this.list().height() > this.list().width()), this.rtl = this.options("rtl"), this.rtl == null && (this.rtl = function (t) {
            if (("" + t.attr("dir")).toLowerCase() === "rtl")return!0;
            var n = !1;
            return t.parents("[dir]").each(function () {
                if (/rtl/i.test(e(this).attr("dir")))return n = !0, !1
            }), n
        }(this._element)), this.lt = this.vertical ? "top" : "left", this._items = null;
        var t = this._target && this.index(this._target) >= 0 ? this._target : this.closest();
        this.circular = this.options("wrap") === "circular", this.underflow = !1;
        var n = {left: 0, top: 0};
        return t.size() > 0 && (this._prepare(t), this.list().find("[data-jcarousel-clone]").remove(), this._items = null, this.underflow = this._fullyvisible.size() >= this.items().size(), this.circular = this.circular && !this.underflow, n[this.lt] = this._position(t) + "px"), this.move(n), this
    }, list: function () {
        if (this._list === null) {
            var t = this.options("list");
            this._list = e.isFunction(t) ? t.call(this) : this._element.find(t)
        }
        return this._list
    }, items: function () {
        if (this._items === null) {
            var t = this.options("items");
            this._items = (e.isFunction(t) ? t.call(this) : this.list().find(t)).not("[data-jcarousel-clone]")
        }
        return this._items
    }, index: function (e) {
        return this.items().index(e)
    }, closest: function () {
        var t = this, r = this.list().position()[this.lt], i = e(), s = !1, o = this.vertical ? "bottom" : this.rtl ? "left" : "right", u;
        return this.rtl && !this.vertical && (r = (r + this.list().width() - this.clipping()) * -1), this.items().each(function () {
            i = e(this);
            if (s)return!1;
            var a = t.dimension(i);
            r += a;
            if (r >= 0) {
                u = a - n(i.css("margin-" + o));
                if (!(Math.abs(r) - a + u / 2 <= 0))return!1;
                s = !0
            }
        }), i
    }, target: function () {
        return this._target
    }, first: function () {
        return this._first
    }, last: function () {
        return this._last
    }, visible: function () {
        return this._visible
    }, fullyvisible: function () {
        return this._fullyvisible
    }, hasNext: function () {
        if (!1 === this._trigger("hasnext"))return!0;
        var e = this.options("wrap"), t = this.items().size() - 1;
        return t >= 0 && (e && e !== "first" || this.index(this._last) < t || this.tail && !this.inTail) ? !0 : !1
    }, hasPrev: function () {
        if (!1 === this._trigger("hasprev"))return!0;
        var e = this.options("wrap");
        return this.items().size() > 0 && (e && e !== "last" || this.index(this._first) > 0 || this.tail && this.inTail) ? !0 : !1
    }, clipping: function () {
        return this._element["inner" + (this.vertical ? "Height" : "Width")]()
    }, dimension: function (e) {
        return e["outer" + (this.vertical ? "Height" : "Width")](!0)
    }, scroll: function (t, r, i) {
        if (this.animating)return this;
        if (!1 === this._trigger("scroll", null, [t, r]))return this;
        e.isFunction(r) && (i = r, r = !0);
        var s = e.jCarousel.parseTarget(t);
        if (s.relative) {
            var o = this.items().size() - 1, u = Math.abs(s.target), a = this.options("wrap"), f, l, c, h, p, d, v, m;
            if (s.target > 0) {
                var g = this.index(this._last);
                if (g >= o && this.tail)this.inTail ? a === "both" || a === "last" ? this._scroll(0, r, i) : this._scroll(Math.min(this.index(this._target) + u, o), r, i) : this._scrollTail(r, i); else {
                    f = this.index(this._target);
                    if (this.underflow && f === o && (a === "circular" || a === "both" || a === "last") || !this.underflow && g === o && (a === "both" || a === "last"))this._scroll(0, r, i); else {
                        c = f + u;
                        if (this.circular && c > o) {
                            m = o, p = this.items().get(-1);
                            while (m++ < c)p = this.items().eq(0), d = this._visible.index(p) >= 0, d && p.after(p.clone(!0).attr("data-jcarousel-clone", !0)), this.list().append(p), d || (v = {}, v[this.lt] = this.dimension(p) * (this.rtl ? -1 : 1), this.moveBy(v)), this._items = null;
                            this._scroll(p, r, i)
                        } else this._scroll(Math.min(c, o), r, i)
                    }
                }
            } else if (this.inTail)this._scroll(Math.max(this.index(this._first) - u + 1, 0), r, i); else {
                l = this.index(this._first), f = this.index(this._target), h = this.underflow ? f : l, c = h - u;
                if (h <= 0 && (this.underflow && a === "circular" || a === "both" || a === "first"))this._scroll(o, r, i); else if (this.circular && c < 0) {
                    m = c, p = this.items().get(0);
                    while (m++ < 0) {
                        p = this.items().eq(-1), d = this._visible.index(p) >= 0, d && p.after(p.clone(!0).attr("data-jcarousel-clone", !0)), this.list().prepend(p), this._items = null;
                        var y = n(this.list().position()[this.lt]), b = this.dimension(p);
                        this.rtl && !this.vertical ? y += b : y -= b, v = {}, v[this.lt] = y + "px", this.move(v)
                    }
                    this._scroll(p, r, i)
                } else this._scroll(Math.max(c, 0), r, i)
            }
        } else this._scroll(s.target, r, i);
        return this._trigger("scrollend"), this
    }, moveBy: function (e, t) {
        var r = this.list().position();
        return e.left && (e.left = r.left + n(e.left) + "px"), e.top && (e.top = r.top + n(e.top) + "px"), this.move(e, t)
    }, move: function (t, n) {
        n = n || {};
        var r = this.options("transitions"), i = !!r, s = !!r.transforms, o = !!r.transforms3d, u = n.duration || 0, a = this.list();
        if (!i && u > 0) {
            a.animate(t, n);
            return
        }
        var f = n.complete || e.noop, l = {};
        if (i) {
            var c = a.css(["transitionDuration", "transitionTimingFunction", "transitionProperty"]), h = f;
            f = function () {
                e(this).css(c), h.call(this)
            }, l = {transitionDuration: (u > 0 ? u / 1e3 : 0) + "s", transitionTimingFunction: r.easing || n.easing, transitionProperty: u > 0 ? function () {
                return s || o ? "all" : t.left ? "left" : "top"
            }() : "none", transform: "none"}
        }
        o ? l.transform = "translate3d(" + (t.left || 0) + "," + (t.top || 0) + ",0)" : s ? l.transform = "translate(" + (t.left || 0) + "," + (t.top || 0) + ")" : e.extend(l, t), i && u > 0 && a.one("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd", f), a.css(l), u <= 0 && a.each(function () {
            f.call(this)
        })
    }, _scroll: function (t, r, i) {
        if (this.animating)return e.isFunction(i) && i.call(this, !1), this;
        typeof t != "object" ? t = this.items().eq(t) : typeof t.jquery == "undefined" && (t = e(t));
        if (t.size() === 0)return e.isFunction(i) && i.call(this, !1), this;
        this.inTail = !1, this._prepare(t);
        var s = this._position(t), o = n(this.list().position()[this.lt]);
        if (s === o)return e.isFunction(i) && i.call(this, !1), this;
        var u = {};
        return u[this.lt] = s + "px", this._animate(u, r, i), this
    }, _scrollTail: function (t, n) {
        if (this.animating || !this.tail)return e.isFunction(n) && n.call(this, !1), this;
        var r = this.list().position()[this.lt];
        this.rtl ? r += this.tail : r -= this.tail, this.inTail = !0;
        var i = {};
        return i[this.lt] = r + "px", this._update({target: this._target.next(), fullyvisible: this._fullyvisible.slice(1).add(this._visible.last())}), this._animate(i, t, n), this
    }, _animate: function (t, n, r) {
        r = r || e.noop;
        if (!1 === this._trigger("animate"))return r.call(this, !1), this;
        this.animating = !0;
        var i = this.options("animation"), s = e.proxy(function () {
            this.animating = !1;
            var e = this.list().find("[data-jcarousel-clone]");
            e.size() > 0 && (e.remove(), this._reload()), this._trigger("animateend"), r.call(this, !0)
        }, this), o = typeof i == "object" ? e.extend({}, i) : {duration: i}, u = o.complete || e.noop;
        return n === !1 ? o.duration = 0 : typeof e.fx.speeds[o.duration] != "undefined" && (o.duration = e.fx.speeds[o.duration]), o.complete = function () {
            s(), u.call(this)
        }, this.move(t, o), this
    }, _prepare: function (t) {
        var r = this.index(t), i = r, s = this.dimension(t), o = this.clipping(), u = this.vertical ? "bottom" : this.rtl ? "left" : "right", a = this.options("center"), f = {target: t, first: t, last: t, visible: t, fullyvisible: s <= o ? t : e()}, l, c, h;
        a && (s /= 2, o /= 2);
        if (s < o)for (; ;) {
            l = this.items().eq(++i);
            if (l.size() === 0) {
                if (!this.circular)break;
                l = this.items().eq(0);
                if (t.get(0) === l.get(0))break;
                c = this._visible.index(l) >= 0, c && l.after(l.clone(!0).attr("data-jcarousel-clone", !0)), this.list().append(l);
                if (!c) {
                    var p = {};
                    p[this.lt] = this.dimension(l) * (this.rtl ? -1 : 1), this.moveBy(p)
                }
                this._items = null
            }
            s += this.dimension(l), f.last = l, f.visible = f.visible.add(l), h = n(l.css("margin-" + u)), s - h <= o && (f.fullyvisible = f.fullyvisible.add(l));
            if (s >= o)break
        }
        if (!this.circular && !a && s < o) {
            i = r;
            for (; ;) {
                if (--i < 0)break;
                l = this.items().eq(i);
                if (l.size() === 0)break;
                s += this.dimension(l), f.first = l, f.visible = f.visible.add(l), h = n(l.css("margin-" + u)), s - h <= o && (f.fullyvisible = f.fullyvisible.add(l));
                if (s >= o)break
            }
        }
        return this._update(f), this.tail = 0, !a && this.options("wrap") !== "circular" && this.options("wrap") !== "custom" && this.index(f.last) === this.items().size() - 1 && (s -= n(f.last.css("margin-" + u)), s > o && (this.tail = s - o)), this
    }, _position: function (e) {
        var t = this._first, n = t.position()[this.lt], r = this.options("center"), i = r ? this.clipping() / 2 - this.dimension(t) / 2 : 0;
        return this.rtl && !this.vertical ? (n -= this.clipping() - this.dimension(t), n += i) : n -= i, !r && (this.index(e) > this.index(t) || this.inTail) && this.tail ? (n = this.rtl ? n - this.tail : n + this.tail, this.inTail = !0) : this.inTail = !1, -n
    }, _update: function (t) {
        var n = this, r = {target: this._target || e(), first: this._first || e(), last: this._last || e(), visible: this._visible || e(), fullyvisible: this._fullyvisible || e()}, i = this.index(t.first || r.first) < this.index(r.first), s, o = function (s) {
            var o = [], u = [];
            t[s].each(function () {
                r[s].index(this) < 0 && o.push(this)
            }), r[s].each(function () {
                t[s].index(this) < 0 && u.push(this)
            }), i ? o = o.reverse() : u = u.reverse(), n._trigger("item" + s + "in", e(o)), n._trigger("item" + s + "out", e(u)), n["_" + s] = t[s]
        };
        for (s in t)o(s);
        return this
    }})
}(jQuery, window), function (e) {
    "use strict";
    e.jcarousel.fn.scrollIntoView = function (t, n, r) {
        var i = e.jCarousel.parseTarget(t), s = this.index(this._fullyvisible.first()), o = this.index(this._fullyvisible.last()), u;
        i.relative ? u = i.target < 0 ? Math.max(0, s + i.target) : o + i.target : u = typeof i.target != "object" ? i.target : this.index(i.target);
        if (u < s)return this.scroll(u, n, r);
        if (u >= s && u <= o)return e.isFunction(r) && r.call(this, !1), this;
        var a = this.items(), f = this.clipping(), l = this.vertical ? "bottom" : this.rtl ? "left" : "right", c = 0, h;
        for (; ;) {
            h = a.eq(u);
            if (h.size() === 0)break;
            c += this.dimension(h);
            if (c >= f) {
                var p = parseFloat(h.css("margin-" + l)) || 0;
                c - p !== f && u++;
                break
            }
            if (u <= 0)break;
            u--
        }
        return this.scroll(u, n, r)
    }
}(jQuery), function (e) {
    "use strict";
    e.jCarousel.plugin("jcarouselControl", {_options: {target: "+=1", event: "click", method: "scroll"}, _active: null, _init: function () {
        this.onDestroy = e.proxy(function () {
            this._destroy(), this.carousel().one("createend.jcarousel", e.proxy(this._create, this))
        }, this), this.onReload = e.proxy(this._reload, this), this.onEvent = e.proxy(function (t) {
            t.preventDefault();
            var n = this.options("method");
            e.isFunction(n) ? n.call(this) : this.carousel().jcarousel(this.options("method"), this.options("target"))
        }, this)
    }, _create: function () {
        this.carousel().one("destroy.jcarousel", this.onDestroy).on("reloadend.jcarousel scrollend.jcarousel", this.onReload), this._element.on(this.options("event") + ".jcarouselcontrol", this.onEvent), this._reload()
    }, _destroy: function () {
        this._element.off(".jcarouselcontrol", this.onEvent), this.carousel().off("destroy.jcarousel", this.onDestroy).off("reloadend.jcarousel scrollend.jcarousel", this.onReload)
    }, _reload: function () {
        var t = e.jCarousel.parseTarget(this.options("target")), n = this.carousel(), r;
        if (t.relative)r = n.jcarousel(t.target > 0 ? "hasNext" : "hasPrev"); else {
            var i = typeof t.target != "object" ? n.jcarousel("items").eq(t.target) : t.target;
            r = n.jcarousel("target").index(i) >= 0
        }
        return this._active !== r && (this._trigger(r ? "active" : "inactive"), this._active = r), this
    }})
}(jQuery), function (e) {
    "use strict";
    e.jCarousel.plugin("jcarouselPagination", {_options: {perPage: null, item: function (e) {
        return'<a href="#' + e + '">' + e + "</a>"
    }, event: "click", method: "scroll"}, _pages: {}, _items: {}, _currentPage: null, _init: function () {
        this.onDestroy = e.proxy(function () {
            this._destroy(), this.carousel().one("createend.jcarousel", e.proxy(this._create, this))
        }, this), this.onReload = e.proxy(this._reload, this), this.onScroll = e.proxy(this._update, this)
    }, _create: function () {
        this.carousel().one("destroy.jcarousel", this.onDestroy).on("reloadend.jcarousel", this.onReload).on("scrollend.jcarousel", this.onScroll), this._reload()
    }, _destroy: function () {
        this._clear(), this.carousel().off("destroy.jcarousel", this.onDestroy).off("reloadend.jcarousel", this.onReload).off("scrollend.jcarousel", this.onScroll)
    }, _reload: function () {
        var t = this.options("perPage");
        this._pages = {}, this._items = {}, e.isFunction(t) && (t = t.call(this));
        if (t == null)this._pages = this._calculatePages(); else {
            var n = parseInt(t, 10) || 0, r = this.carousel().jcarousel("items"), i = 1, s = 0, o;
            for (; ;) {
                o = r.eq(s++);
                if (o.size() === 0)break;
                this._pages[i] ? this._pages[i] = this._pages[i].add(o) : this._pages[i] = o, s % n === 0 && i++
            }
        }
        this._clear();
        var u = this, a = this.carousel().data("jcarousel"), f = this._element, l = this.options("item");
        e.each(this._pages, function (t, n) {
            var r = u._items[t] = e(l.call(u, t, n));
            r.on(u.options("event") + ".jcarouselpagination", e.proxy(function () {
                var e = n.eq(0);
                if (a.circular) {
                    var r = a.index(a.target()), i = a.index(e);
                    parseFloat(t) > parseFloat(u._currentPage) ? i < r && (e = "+=" + (a.items().size() - r + i)) : i > r && (e = "-=" + (r + (a.items().size() - i)))
                }
                a[this.options("method")](e)
            }, u)), f.append(r)
        }), this._update()
    }, _update: function () {
        var t = this.carousel().jcarousel("target"), n;
        e.each(this._pages, function (e, r) {
            r.each(function () {
                if (t.is(this))return n = e, !1
            });
            if (n)return!1
        }), this._currentPage !== n && (this._trigger("inactive", this._items[this._currentPage]), this._trigger("active", this._items[n])), this._currentPage = n
    }, items: function () {
        return this._items
    }, _clear: function () {
        this._element.empty(), this._currentPage = null
    }, _calculatePages: function () {
        var e = this.carousel().data("jcarousel"), t = e.items(), n = e.clipping(), r = 0, i = 0, s = 1, o = {}, u;
        for (; ;) {
            u = t.eq(i++);
            if (u.size() === 0)break;
            o[s] ? o[s] = o[s].add(u) : o[s] = u, r += e.dimension(u), r >= n && (s++, r = 0)
        }
        return o
    }})
}(jQuery), function (e) {
    "use strict";
    e.jCarousel.plugin("jcarouselAutoscroll", {_options: {target: "+=1", interval: 3e3, autostart: !0}, _timer: null, _init: function () {
        this.onDestroy = e.proxy(function () {
            this._destroy(), this.carousel().one("createend.jcarousel", e.proxy(this._create, this))
        }, this), this.onAnimateEnd = e.proxy(this.start, this)
    }, _create: function () {
        this.carousel().one("destroy.jcarousel", this.onDestroy), this.options("autostart") && this.start()
    }, _destroy: function () {
        this.stop(), this.carousel().off("destroy.jcarousel", this.onDestroy)
    }, start: function () {
        return this.stop(), this.carousel().one("animateend.jcarousel", this.onAnimateEnd), this._timer = setTimeout(e.proxy(function () {
            this.carousel().jcarousel("scroll", this.options("target"))
        }, this), this.options("interval")), this
    }, stop: function () {
        return this._timer && (this._timer = clearTimeout(this._timer)), this.carousel().off("animateend.jcarousel", this.onAnimateEnd), this
    }})
}(jQuery);


/* public/javascripts/jquery.touchwipe.1.1.1.js @ 1384554040 */
/**
 * jQuery Plugin to obtain touch gestures from iPhone, iPod Touch and iPad, should also work with Android mobile phones (not tested yet!)
 * Common usage: wipe images (left and right to show the previous or next image)
 *
 * @author Andreas Waltl, netCU Internetagentur (http://www.netcu.de)
 * @version 1.1.1 (9th December 2010) - fix bug (older IE's had problems)
 * @version 1.1 (1st September 2010) - support wipe up and wipe down
 * @version 1.0 (15th July 2010)
 */
(function (e) {
    e.fn.touchwipe = function (t) {
        var n = {min_move_x: 20, min_move_y: 20, wipeLeft: function () {
        }, wipeRight: function () {
        }, wipeUp: function () {
        }, wipeDown: function () {
        }, preventDefaultEvents: !0};
        return t && e.extend(n, t), this.each(function () {
            function i() {
                this.removeEventListener("touchmove", s), e = null, r = !1
            }

            function s(s) {
                n.preventDefaultEvents && s.preventDefault();
                if (r) {
                    var o = s.touches[0].pageX, u = s.touches[0].pageY, a = e - o, f = t - u;
                    Math.abs(a) >= n.min_move_x ? (i(), a > 0 ? n.wipeLeft() : n.wipeRight()) : Math.abs(f) >= n.min_move_y && (i(), f > 0 ? n.wipeDown() : n.wipeUp())
                }
            }

            function o(n) {
                n.touches.length == 1 && (e = n.touches[0].pageX, t = n.touches[0].pageY, r = !0, this.addEventListener("touchmove", s, !1))
            }

            var e, t, r = !1;
            try {
                this.addEventListener("touchstart", o, !1)
            } catch (u) {
                console.log("Touch start events are not supported in this Browser/OS")
            }
        }), this
    }
})(jQuery);


/* :asset_packager_compatibility, 'config/asset_packages.yml' @ 1384554040 */
/* public/javascripts/jquery_global/sticky.coffee @ 1384554040 */
(function () {
    $.fn.sticky_container = function (e) {
        var t, n, r, i, s, o, u, a, f, l, c, h, p, d, v, m, g = this;
        return e == null && (e = {}), f = this.offset(), m = $(window), a = e.padding != null ? e.padding : 0, h = e.sticky_class || "stuck", s = e.bottom_class || "bottomed", c = e.sticky_callback, l = e.static_callback, n = e.before_stick, i = e.before_unstick, t = e.before_bottom, r = e.before_unbottom, e.unstick_after && (v = $(e.unstick_after)), e.dont_override_elem && (u = $(e.dont_override_elem)), p = !1, o = !1, d = function () {
            var e, d, y, b, E, S;
            return S = m.scrollTop(), E = S > f.top - a, v && (E ? (e = v.offset().top + v.height(), S + m.height() > e ? (E = !1, o || (typeof t == "function" && t(g), o = !0, s && g.addClass(s))) : o && (typeof r == "function" && r(g), o = !1, s && g.removeClass(s))) : o && (typeof r == "function" && r(g), o = !1, s && g.removeClass(s))), u && E && (b = u.offset().top, d = S + g.height() - b, d > 0 ? g.css("top", -d + 38) : g.css("top", 38)), y = !1, E ? (p || (typeof n == "function" && n(g), g.addClass(h), p = !0, y = !0), typeof c == "function" && c(g, y)) : (p && (typeof i == "function" && i(g), g.removeClass(h), p = !1, y = !0), typeof l == "function" && l(g, y)), void 0
        }, this.on("Scribd:update_root", function () {
            return f = g.offset()
        }), this.on("Scribd:restick", d), m.on("scroll resize", d), this
    }
}).call(this);


/* public/javascripts/newhome/actions.coffee @ 1384554040 */
(function () {
    var e = function (e, t) {
        return function () {
            return e.apply(t, arguments)
        }
    };
    Scribd.DocumentActions = function () {
        function n(t, n) {
            n == null && (n = this.track_category), this.default_params = e(this.default_params, this), this.container = $(t), n ? this.container.dispatch("click", n, this, this) : this.container.dispatch("click", this, this)
        }

        var t = this;
        return n.run = function (e, t, n, r) {
            var i;
            return r == null && (r = Scribd.current_doc), i = new Scribd.DocumentActions($(), n), i.find_object = function () {
                return r
            }, i[e](t)
        }, n.prototype.track_category = !1, n.prototype.download_page = "timeline", n.prototype.download_action = "timeline_download", n.prototype.default_params = function (e) {
            var t;
            return t = {id: e.id}, e.secret_password && (t.secret_password = e.secret_password), t
        }, n.prototype.find_object = function (e) {
            return e.closest(".event").data()
        }, n.prototype.download_btn = function (e) {
            var t;
            return t = this.find_object(e).id, Scribd.download_actions.download(t, this.download_page, this.download_action)
        }, n.prototype.readcast_btn = function (e, t) {
            var n = this;
            if (e.is(".readcasted"))return;
            return Scribd.with_login("like_document", function () {
                var r;
                return e.addClass("disabled"), e.addClass("readcasted").trigger("scribd:update_tooltip"), r = n.find_object(e), $.ajax({url: "/doc/like", type: "post", data: {document_id: r.id}, success: function (n) {
                    return e.removeClass("disabled"), typeof t == "function" ? t(data) : void 0
                }})
            })
        }, n.prototype.save_bookmark_btn = function (e, t) {
            var n, r = this;
            return n = this.find_object(e), e.addClass("disabled"), e.hasClass("saved") ? ($.ajax({url: "/document_bookmarks/delete", type: "post", data: Scribd.CSRF.with_token({_method: "delete", document_id: n.id}), success: function (r) {
                return e.removeClass("disabled"), $(window.document).trigger("scribd:library:document_removed", n), typeof t == "function" ? t(r) : void 0
            }}), null) : $.ajax({url: "/document_bookmarks/create", type: "post", data: Scribd.CSRF.with_token({document_id: n.id}), dataType: "json", success: function (r) {
                return e.removeClass("disabled"), $(window.document).trigger("scribd:library:document_added", n), typeof t == "function" ? t(r) : void 0
            }}), e.toggleClass("saved").trigger("scribd:update_tooltip")
        }, n.prototype.add_to_collection_btn = function (e, t) {
            var n, r = this;
            return n = this.default_params(this.find_object(e)), Scribd.with_login("add_to_collection", "/", function () {
                return Scribd.Lightbox.remote_open("lightbox_document_collections", "/read/collections_dialog", n)
            })
        }, n.prototype.embed_btn = function (e, t) {
            var n;
            return n = this.default_params(this.find_object(e)), Scribd.Lightbox.remote_open("embed_dialog", "/read/embed_dialog", n)
        }, n
    }.call(this)
}).call(this);


/* public/javascripts/newhome/sliding.coffee @ 1384554040 */
(function () {
    Scribd.UI || (Scribd.UI = {}), Scribd.UI.PageDots = function () {
        function e(e, t) {
            this.total_pages = e, this.per_page = t != null ? t : 1, this.container = $(this.template).css({width: this.total_pages * this.dot_size * this.per_page + "px"}), this.fill = this.container.find(".fill"), this.current_page = 1, this.update_fill()
        }

        return e.prototype.dot_size = 12, e.prototype.template = '<div class="page_picker">\n  <div class="fill"></div>\n  <div class="dots"></div>\n</div>', e.prototype.update_fill = function () {
            return this.fill.stop().animate({left: (this.current_page - 1) * this.dot_size * this.per_page + "px", width: this.per_page * this.dot_size + "px"}, "fast")
        }, e.prototype.goto_page = function (e) {
            return this.current_page = e, this.update_fill()
        }, e.prototype.is_first = function () {
            return this.current_page === 1
        }, e.prototype.is_last = function () {
            return this.current_page === this.total_pages
        }, e
    }(), Scribd.UI.SlidingList = function () {
        function e(e) {
            this.container = $(e), this.container.data("list", this), this.sliding = this.container.find(".sliding_content"), this.list_content = this.container.find(".list_content"), this.list_content.height(this.sliding.children(":first").height()), this.page = 1, this.calculate_pages(), this.num_pages > 1 && this.PageDots && (this.dots = new this.PageDots(this.num_pages), this.container.find(".list_dots").append(this.dots.container)), this.goto_page(1)
        }

        return e.prototype.speed = "fast", e.prototype.PageDots = Scribd.UI.PageDots, e.prototype.calculate_pages = function () {
            var e, t, n, r, i;
            t = 0, i = this.sliding.children();
            for (n = 0, r = i.length; n < r; n++)e = i[n], t += $(e).outerWidth(!0);
            return this.sliding.width(t), this.page_width = this.sliding.parent().width(), this.num_pages = Math.ceil(t / this.page_width)
        }, e.prototype.page_left = function () {
            return this.goto_page((this.page - 2 + this.num_pages) % this.num_pages + 1)
        }, e.prototype.page_right = function () {
            return this.goto_page(this.page % this.num_pages + 1)
        }, e.prototype.page_offset = function (e) {
            return this.page_width * (e - 1)
        }, e.prototype.goto_page = function (e) {
            var t, n;
            return t = Math.max(1, Math.min(this.num_pages, e)), this.sliding.animate({marginLeft: -this.page_offset(t)}, this.speed), (n = this.dots) != null && n.goto_page(t), this.page = t, this.container.toggleClass("on_first_page", t === 1).toggleClass("on_last_page", t === this.num_pages)
        }, e
    }()
}).call(this);


/* public/javascripts/shared/embed_code.coffee @ 1384554040 */
(function () {
    var e, t = {}.hasOwnProperty, n = function (e, n) {
        function i() {
            this.constructor = e
        }

        for (var r in n)t.call(n, r) && (e[r] = n[r]);
        return i.prototype = n.prototype, e.prototype = new i, e.__super__ = n.prototype, e
    };
    Scribd.Embed || (Scribd.Embed = {}), e = function () {
        function e(e) {
            this.options = $.extend({object_id: "doc_" + Math.floor(Math.random() * 99999)}, this.default_options, e), this.options.auto_width || this.options.width === "100%" ? this.options.width = "100%" : parseInt(e.width) < this.options.minWidth && (this.options.width = this.options.minWidth), this.options.auto_height ? this.options.height = this.default_options.height : parseInt(e.height) < this.options.min_height && (this.options.height = this.options.min_height)
        }

        return e.prototype.default_options = {minWidth: 400, minHeight: 400, maxWidth: 1600, maxHeight: 1600, width: 400, height: 600, page: 1, mode: "scroll", auto_width: !0, auto_height: !1, show_title: !0, show_recommendations: !0}, e.prototype.make_title = function (e) {
            var t, n, r, i, s, o, u;
            return n = [], s = _.escape(Scribd.truncate($.trim(e.title), 110)), e.user_title != null && (u = _.escape(Scribd.truncate($.trim(e.user_title), 110)), o = _.escape(e.user_title)), e.show_title && (i = _.escape(e.title), r = ' style="\nmargin: 12px auto 6px auto;\nfont-family: Helvetica,Arial,Sans-serif;\nfont-style: normal;\nfont-variant: normal;\nfont-weight: normal;\nfont-size: 14px;\nline-height: normal;\nfont-size-adjust: none;\nfont-stretch: normal;\n-x-system-font: none;\ndisplay: block;"', t = ' style="text-decoration: underline;" ', n.push("<p " + r + '>\n  <a title="View ' + i + ' on Scribd" href="' + e.doc_url + '" ' + t + ">" + s + "</a>"), e.user_title != null && n.push(' by\n<a title="View ' + o + '\'s profile on Scribd" href="' + e.user_url + '" ' + t + ">" + u + "</a>"), n.push("</p>")), n
        }, e.prototype.make_protocol_relative = function (e) {
            return e.replace(/^http:\/\//, "//")
        }, e
    }(), Scribd.Embed.HTML5CodeGenerator = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return n(t, e), t.prototype.embed_url = function () {
            var e, t;
            return t = this.make_protocol_relative("http://www.scribd.com/embeds/" + this.options.doc_id + "/content"), e = {start_page: this.options.page, view_mode: this.options.mode}, this.options.access_key && ("" + this.options.access_key).length && (e.access_key = this.options.access_key), e.show_recommendations = this.options.show_recommendations, t + "?" + $.param(e)
        }, t.prototype.render = function (e) {
            var t, n, r, i;
            return e && (this.options = $.extend(this.options, e)), t = !1, this.options.auto_height && this.options.aspect_ratio && (this.options.auto_width ? t = !0 : this.options.height = Math.round(1 / this.options.aspect_ratio * this.options.width) + 60), i = this.make_title(this.options), r = this.embed_url(), n = !!this.options.auto_height, i.push('<iframe class="scribd_iframe_embed" src="' + r + '" data-auto-height="' + n + '" data-aspect-ratio="' + this.options.aspect_ratio + '" scrolling="no" id="' + this.options.object_id + '" width="' + this.options.width + '" height="' + this.options.height + '" frameborder="0"></iframe>'), t && i.push('<script type="text/javascript">(function() { var scribd = document.createElement("script"); scribd.type = "text/javascript"; scribd.async = true; scribd.src = "' + this.make_protocol_relative("http://www.scribd.com/") + 'javascripts/embed_code/inject.js"; var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(scribd, s); })();</script>'), i.join("").replace(/\n/g, " ")
        }, t
    }(e), Scribd.Embed.FlashCodeGenerator = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return n(t, e), t.prototype.render = function (e) {
            var t;
            return e && (this.options = $.extend(this.options, e)), t = this.make_title(this.options), t.push('<object id="' + this.options.object_id + '" name="' + this.options.object_id + '" height="' + this.options.height + '" width="' + this.options.width + '" type="application/x-shockwave-flash" data="//d1.scribdassets.com/ScribdViewer.swf" style="outline:none;" >\n<param name="movie" value="//d1.scribdassets.com/ScribdViewer.swf"> \n<param name="wmode" value="opaque"> \n<param name="bgcolor" value="#ffffff"> \n<param name="allowFullScreen" value="true"> \n<param name="allowScriptAccess" value="always"> \n<param name="FlashVars" value="document_id=' + this.options.doc_id + "&access_key=" + this.options.access_key + "&page=" + this.options.page + "&viewMode=" + this.options.mode + '"> \n<embed id="' + this.options.object_id + '" name="' + this.options.object_id + '" src="//d1.scribdassets.com/ScribdViewer.swf?document_id=' + this.options.doc_id + "&access_key=" + this.options.access_key + "&page=" + this.options.page + "&viewMode=" + this.options.mode + '" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" height="' + this.options.height + '" width="' + this.options.width + '" wmode="opaque" bgcolor="#ffffff"></embed> \n</object>'), t.join("").replace(/\n/g, " ")
        }, t
    }(e), Scribd.Embed.WordpressCodeGenerator = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return n(t, e), t.prototype.render = function (e) {
            return e && (this.options = $.extend(this.options, e)), "[scribd id=" + this.options.doc_id + " key=" + this.options.access_key + " mode=" + this.options.mode + "]"
        }, t
    }(e)
}).call(this);


/* public/javascripts/reading_progress.coffee @ 1384554040 */
(function () {
    var e = {}.hasOwnProperty, t = function (t, n) {
        function i() {
            this.constructor = t
        }

        for (var r in n)e.call(n, r) && (t[r] = n[r]);
        return i.prototype = n.prototype, t.prototype = new i, t.__super__ = n.prototype, t
    };
    Scribd.ReadingProgress = function () {
        function t(t) {
            t == null && (t = {}), t.scroll_watch === void 0 && (t.scroll_watch = !0), t.huge_jump = t.huge_jump || 1, t.num_valid_scrolls_after_jump = t.num_valid_scrolls_after_jump || 2, this.watch_scroll = t.scroll_watch, this.debounce_time = this.constructor.debounce_time, this.state_of_jump_huge_amount = !1, this.curr_times_scroll_after_big_jump = 0, this.definition_of_huge_jump = t.huge_jump, this.num_times_to_scroll_after_big_jump = t.num_valid_scrolls_after_jump, this.watch_scroll && (this.scroll_jump_watcher = new e(this), this.setup_listeners())
        }

        var e;
        return e = function () {
            function e(e) {
                this.reading_progress = e, this.starting_jump_point = null, this.ending_jump_point = null, this.watch_interval_id = null
            }

            return e.prototype.watch = function () {
                var e = this;
                return this.starting_jump_point = this.reading_progress.current_top_position(), this.watch_interval_id = setInterval(function () {
                    return e.ending_jump_point = e.reading_progress.current_top_position()
                }, 100)
            }, e.prototype.running_watch = function () {
                return this.watch_interval_id !== null
            }, e.prototype.stop_watching = function () {
                return clearInterval(this.watch_interval_id), this.watch_interval_id = null
            }, e.prototype.distance_jumped = function () {
                return Math.abs(this.ending_jump_point - this.starting_jump_point)
            }, e
        }(), t.url = "/reading-history/public/update_progress", t.debounce_time = Scribd.ServerOptions && Scribd.ServerOptions.reading_history_scroll_timeout && parseInt(Scribd.ServerOptions.reading_history_scroll_timeout), t.prototype.is_disabled = function () {
            var e;
            return((e = Scribd.current_doc) != null ? !e.reading_history : !void 0) || Scribd.named_current_user_sync().masked_admin || this.watch_scroll && +this.debounce_time <= 0
        }, t.prototype.current_top_position = function () {
            var e;
            e = docManager.getVisibleBBox();
            if (!e.top)return;
            return e.top.toFixed(2)
        }, t.prototype.ping_reading_progress = function (e) {
            if (this.is_disabled())return;
            if (!e)return;
            if (e > 1.1) {
                if (e === this.last_pinged_progress)return;
                return this.last_pinged_progress = e, $.post(this.constructor.url, $.extend({}, this.reading_history_ping_opts(), {reading_progress: e}))
            }
            return
        }, t.prototype.reading_history_ping_opts = function () {
            return Scribd.current_doc.reading_history
        }, t.prototype.update_progress = function () {
            return this.ping_reading_progress(this.current_top_position())
        }, t.prototype.setup_listeners = function () {
            var e = this;
            return $(document).on("scroll", function () {
                return e.scroll_jump_watcher.running_watch() || e.scroll_jump_watcher.watch(), e.scroll_watch()
            })
        }, t.prototype.scroll_watch = _.debounce(function () {
            var e;
            this.scroll_jump_watcher.stop_watching(), e = this.scroll_jump_watcher.distance_jumped() > this.definition_of_huge_jump, this.state_of_jump_huge_amount && (this.curr_times_scroll_after_big_jump = e ? 0 : this.curr_times_scroll_after_big_jump + 1);
            if (!this.state_of_jump_huge_amount || this.curr_times_scroll_after_big_jump >= this.num_times_to_scroll_after_big_jump) {
                this.state_of_jump_huge_amount = e;
                if (!this.state_of_jump_huge_amount)return this.curr_times_scroll_after_big_jump = 0, this.update_progress()
            }
        }, t.debounce_time), t
    }(), Scribd.MobileReadingProgress = function (e) {
        function n(e) {
            this.doc_info = e
        }

        return t(n, e), n.prototype.is_disabled = function () {
            return Scribd.named_current_user_sync().masked_admin
        }, n.prototype.reading_history_ping_opts = function () {
            return this.doc_info
        }, n
    }(Scribd.ReadingProgress), Scribd.SectionizedReflowReadingProgress = function (e) {
        function n() {
            n.__super__.constructor.apply(this, arguments), this.maxElement = null, this.prefix = "buPe"
        }

        return t(n, e), n.prototype.get_top = function (e) {
            var t;
            t = 0;
            while (e && !isNaN(e.offsetLeft) && !isNaN(e.offsetTop))t += e.offsetTop, e = e.offsetParent;
            return t
        }, n.prototype.get_bottom = function (e) {
            return this.add_bottom(e, this.get_top(e))
        }, n.prototype.add_bottom = function (e, t) {
            try {
                t += e.offsetHeight
            } catch (n) {
            }
            return t
        }, n.prototype.get_max_element_index = function () {
            var e, t;
            if (this.maxElement === null) {
                t = 1024, e = 0;
                while (t !== 0) {
                    while (document.getElementById(this.prefix + (t + e)) !== null)e += t;
                    t >>= 1
                }
                this.maxElement = e
            }
            return this.maxElement
        }, n.prototype.find_first_element_below = function (e) {
            var t, n, r, i, s, o;
            o = 0, s = this.get_max_element_index() + 1, r = s >> 1, i = s;
            while (r !== i)t = document.getElementById(this.prefix + r), n = this.get_top(t), n > e ? s = r : o = r, i = r, r = o + s >> 1;
            return[t, r]
        }, n.prototype.find_first_element_above = function (e) {
            var t, n, r, i, s, o;
            o = 0, s = this.get_max_element_index() + 1, r = s >> 1, i = s;
            while (r !== i)t = document.getElementById(this.prefix + r), n = this.get_top(t), n <= e ? o = r : s = r, i = r, r = o + s >> 1;
            return[t, r]
        }, n.prototype.derive_half_visible_ratio = function (e, t) {
            var n, r, i;
            return r = this.get_top(e), i = this.get_bottom(e, r), n = (t - r) / (i - r), n
        }, n.prototype.current_top_position = function () {
            var e, t, n, r;
            return n = $(window).scrollTop(), r = this.find_first_element_below(n), e = r[0], t = r[1], t += this.derive_half_visible_ratio(e, n), t
        }, n.prototype.store_rat_read = function (e, t) {
            if (!window.$rat)return;
            return window.$rat("epub_read", [e, t])
        }, n.prototype.update_progress = function () {
            var e, t, n, r, i, s, o, u, a, f;
            o = $(window).scrollTop(), u = o + window.innerHeight, a = this.find_first_element_below(o), e = a[0], i = a[1], f = this.find_first_element_above(u), t = f[0], s = f[1], n = this.derive_half_visible_ratio(e, o), r = this.derive_half_visible_ratio(t, u), i += n, s += r;
            if (isNaN(i) || isNaN(s) || n < 0 || n > 1 || r < 0 || r > 1)return;
            this.store_rat_read(i, s), this.ping_reading_progress(i);
            if (this.debug)return e.style.backgroundColor = "#00ff00", this.last_top !== null && this.last_top !== e && (last_top.style.backgroundColor = "#ffffff"), this.last_top = e, t.style.backgroundColor = "#ff8080", this.last_bottom !== null && this.last_bottom !== t && (this.last_bottom.style.backgroundColor = "#ffffff"), this.last_bottom = t
        }, n
    }(Scribd.ReadingProgress)
}).call(this);


/* public/javascripts/jquery_global/star_ratings.coffee @ 1384554040 */
(function () {
    $.fn.star_ratings = function (e) {
        var t, n, r, i, s, o, u, a, f, l = this;
        e == null && (e = {}), n = ["strong_lit", "light_lit"], this.setValue = function (e, t) {
            return t == null && (t = "light_lit"), l.container.data("value", e), l.container.find("span").removeClass(t), l.container.find("span").slice(0, e).addClass(t)
        }, t = 5, $(this).addClass("star_ratings"), this.container = $(this), this.container.addClass("rating");
        if (e.setValue) {
            if (this.container.data("star_ratings")) {
                this.setValue(e.setValue);
                return
            }
            e.defaultValue = e.setValue
        }
        r = $(this.find(".icon-feature")).length;
        for (s = f = r; r <= t ? f < t : f > t; s = r <= t ? ++f : --f)u = $("<span>").addClass("icon-feature").data("value", s), this.container.append(u);
        return i = e.defaultValue || 0, o = e.setValue || 0, this.setValue(i, "strong_lit"), this.setValue(o), a = this.container.find(".icon-feature"), e.disable_hover || (this.container.on("mouseenter", ".icon-feature", function (e) {
            var n;
            return n = $(e.target).data("value") + 1, a.slice(0, n).addClass("light_lit"), a.slice(n, t).removeClass("light_lit")
        }), this.container.on("mouseleave", function (e) {
            return l.setValue(l.container.data("value"))
        })), this.animation_lock = !1, this.container.on("click", ".icon-feature", function (n) {
            var r;
            return r = function () {
                var e, r, i;
                return r = $(n.target), i = r.data("value") + 1, l.container.data("value", i), r.rotate && !l.animation_lock && ($.browser.chrome || $.browser.safari) && (l.animation_lock = !0, e = r.data("angle") || 0, r.rotate(e + 144), setTimeout(function () {
                    return l.animation_lock = !1
                }, 1e3)), l.container.trigger("Scribd:rating", i), a.removeClass("strong_lit"), a.slice(0, i).addClass("light_lit"), a.slice(i, t).removeClass("light_lit")
            }, e.force_login && !Scribd.logged_in ? Scribd.with_login("rate_document", r) : r()
        }), this.container.data("star_ratings", this), this
    }
}).call(this);


/* :files, 'public/javascripts/shared', ... @ (none) */


/* :files, 'app/views', ... @ (none) */


/* :files, 'app/views', ... @ 1384554002 */
/* app/views/shared/download_document.coffee @ 1384554002 */
(function () {
    Scribd.toolbar_params = function (e) {
        var t;
        return Scribd.current_doc && (e = Scribd.current_doc), t = {id: e.id}, e.secret_password && (t.secret_password = e.secret_password), t
    }, Scribd.download_actions = {register_download_attempt: function (e, t, n, r) {
        var i;
        return $.ajax("/document_downloads/register_download_attempt", {type: "post", data: {doc_id: r.id, next_screen: n, source: e}}), Scribd.content_discovery_version && (i = $(window).scrollTop() > 700 ? "BelowFoldDownloadSidebar" : "AboveFoldDownloadSidebar", Scribd.track_event(Scribd.content_discovery_version, "Click", i, 1)), Scribd.track_event("download", "click", e, Scribd.logged_in ? 1 : 0), $rat("analytics.sidebar.document.download", {title: r.title})
    }, download_attempt: function (e, t, n, r) {
        var i, s, o, u, a = this;
        return i = !Scribd.logged_in, s = encodeURIComponent(JSON.stringify({context: r.signup_context, page: t, action: n, platform: "web", logged_in: !i})), r.show_archive_paywall ? (o = "doc=" + r.id + "&metadata=" + s, i ? (this.register_download_attempt(t, n, "login_lightbox", r), Scribd.LoginLightbox.remote_open("archive_lb", "/archive/login?" + o, Scribd.toolbar_params(r))) : (u = function () {
            return a.register_download_attempt(t, n, "paywall", r), window.location = "/archive/plans?" + o
        }, Scribd.FacebookSession.attempt_facebook_upgrade(u))) : i ? (this.register_download_attempt(t, n, "login_lightbox", r), Scribd.SignIn.open("download", r.url + "#download")) : (this.register_download_attempt(t, n, "download_lightbox", r), e(r))
    }, download_dialog_lightbox: function (e) {
        return Scribd.Lightbox.remote_open("download_dialog", "/read/download_dialog", Scribd.toolbar_params(e))
    }, download: function (e, t, n) {
        var r;
        return r = this.download_dialog_lightbox, Scribd.current_doc ? Scribd.download_actions.download_attempt(r, t, n, Scribd.current_doc) : $.ajax("/document_downloads/request_document_for_download", {type: "post", data: {id: e}, success: function (e) {
            return Scribd.download_actions.download_attempt(r, t, n, e)
        }})
    }}
}).call(this);


/* app/views/shared/sms_mobile_app_form.coffee @ 1384554002 */
(function () {
    Scribd.SMSMobileAppForm = function () {
        function e(e) {
            var t, n = this;
            this.container = $(e), t = this.container.find(".text_form .text_me_number"), this.container.on("submit", ".text_form", function (e) {
                return $.trim(t.val()) === "" ? !1 : $(e.currentTarget).addClass("loading")
            }), this.container.on("ajax:complete", ".text_form", function (e) {
                return $(e.currentTarget).removeClass("loading")
            }), this.container.on("ajax:success", ".text_form", function (e, r) {
                return n.container.removeClass("has_error sms_complete"), r.valid ? (n.container.find(".phone_number").text(t.val()), n.container.addClass("sms_complete")) : n.container.addClass("has_error"), t.val("")
            })
        }

        return e
    }()
}).call(this);


/* app/views/read2/comments/_comments.coffee @ 1384554002 */
(function () {
    var e, t, n = function (e, t) {
        return function () {
            return e.apply(t, arguments)
        }
    }, r = {}.hasOwnProperty, i = function (e, t) {
        function i() {
            this.constructor = e
        }

        for (var n in t)r.call(t, n) && (e[n] = t[n]);
        return i.prototype = t.prototype, e.prototype = new i, e.__super__ = t.prototype, e
    };
    e = function (t) {
        return e.tracker("Click", "comments_" + t)
    }, e.tracker = Scribd.make_tracker("newdoc"), Scribd.Backbone = (t = Scribd.Backbone) != null ? t : {}, Scribd.Backbone.Comment = function (e) {
        function t() {
            return this.mark_ham = n(this.mark_ham, this), this.mark_spam = n(this.mark_spam, this), this.mark = n(this.mark, this), this.viewable_children = n(this.viewable_children, this), this.is_viewable = n(this.is_viewable, this), this.user_owned = n(this.user_owned, this), this.has_annotation = n(this.has_annotation, this), this.is_root = n(this.is_root, this), this.urlRoot = n(this.urlRoot, this), t.__super__.constructor.apply(this, arguments)
        }

        return i(t, e), t.prototype.idAttribute = "_id", t.prototype.defaults = {deleted: !1, is_spam: !1}, t.prototype.initialize = function (e) {
            return this.set("depth", this.depth), this.set("time_str", moment(this.get("created_at")).fromNow()), this.set("user_owned", this.user_owned)
        }, t.prototype.relations = [
            {type: Backbone.HasMany, key: "children", relatedModel: "Scribd.Backbone.Comment", collectionType: "Scribd.Backbone.CommentCollection"},
            {type: Backbone.HasOne, key: "parent", relatedModel: "Scribd.Backbone.Comment", collectionType: "Scribd.Backbone.CommentCollection"},
            {type: Backbone.HasOne, key: "word_user", relatedModel: "Scribd.Backbone.WordUser", includeInJSON: ["login", "comment_profile_picture", "small_comment_profile_picture", "_id", "name"]}
        ], t.prototype.urlRoot = function () {
            return"/documents/" + this.get("document_id") + "/comments/"
        }, t.prototype.is_root = function () {
            return!this.get("parent") && !this.get("parent_id")
        }, t.prototype.has_annotation = function () {
            return!!this.get("annotation") || !!this.get("annotation_id")
        }, t.prototype.user_owned = function () {
            return Scribd.Backbone.current_user ? Scribd.Backbone.current_user.get("id") === this.get("word_user_id") || Scribd.current_doc.author_id === Scribd.Backbone.current_user.get("id") : !1
        }, t.prototype.is_viewable = function () {
            return this.get("viewable") === "show" || this.get("word_user") === Scribd.Backbone.current_user && !this.get("deleted")
        }, t.prototype.viewable_children = function () {
            var e = this;
            return this.get("children").some(function (e) {
                return e.is_viewable()
            })
        }, t.prototype.mark = function (e) {
            return $.ajax("/admin/documents/" + Scribd.current_doc.id + "/newcomments/" + this.get("_id") + "/mark_" + e, {type: "put"})
        }, t.prototype.mark_spam = function () {
            return this.set("is_spam", !0), this.mark("spam"), this.set("viewable", "hidden")
        }, t.prototype.mark_ham = function () {
            return this.mark("ham")
        }, t
    }(Backbone.RelationalModel), Scribd.Backbone.Comment.setup(), Scribd.Backbone.Document = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return i(t, e), t.prototype.urlRoot = "/documents", t.prototype.defaults = {next_page: 1}, t.prototype.initialize = function () {
            return this.set("_id", Scribd.current_doc.id)
        }, t.prototype.relations = [
            {type: Backbone.HasMany, key: "comments", relatedModel: "Scribd.Backbone.Comment", collectionType: "Scribd.Backbone.CommentCollection", reverseRelation: {key: "document", includeInJSON: "_id"}},
            {type: Backbone.HasMany, key: "annotations", relatedModel: "Scribd.Backbone.Annotation", collectionType: "Scribd.Backbone.AnnotationCollection", reverseRelation: {key: "document", includeInJSON: "_id"}}
        ], t
    }(Backbone.RelationalModel), Scribd.Backbone.Document.setup(), Scribd.Backbone.WordUser = function (e) {
        function t() {
            return this.is_admin = n(this.is_admin, this), t.__super__.constructor.apply(this, arguments)
        }

        return i(t, e), t.prototype.initialize = function () {
            if (!this.get("name"))return this.set("name", this.get("login"))
        }, t.prototype.is_admin = function () {
        }, t
    }(Backbone.RelationalModel), Scribd.Backbone.WordUser.setup(), Scribd.Backbone.CommentCollection = function (e) {
        function t() {
            return this.url = n(this.url, this), this.next_page = n(this.next_page, this), this.fetch = n(this.fetch, this), this.page_info = n(this.page_info, this), this.parse = n(this.parse, this), this.comparator = n(this.comparator, this), t.__super__.constructor.apply(this, arguments)
        }

        return i(t, e), t.prototype.model = Scribd.Backbone.Comment, t.prototype.initialize = function () {
            return this.page = 1, this.per_page = 10, this.document_id = Scribd.current_doc.id
        }, t.prototype.comparator = function (e) {
            return-1 * moment(e.get("_id"))
        }, t.prototype.parse = function (e) {
            return this.page = e.page, this.per_page = e.per_page, this.total = e.total, e.comments
        }, t.prototype.page_info = function () {
            var e, t;
            return e = {total: this.total, page: this.page, per_page: this.per_page, pages: Math.ceil(this.total / this.per_page), prev: !1, next: !1}, t = Math.min(this.total, this.page * this.per_page), e.range = [this.page - 1 * this.per_page + 1, t], this.page > 1 && (e.prev = this.page - 1), this.page < e.pages && (e.next = this.page + 1), e
        }, t.prototype.fetch = function (e) {
            var n, r, i = this;
            return e == null && (e = {}), r = function () {
                i.trigger("loaded");
                if (!i.page_info().next)return i.trigger("all_loaded")
            }, e.success ? (n = e.success, e.success = function () {
                return n(), r
            }) : e.success = r, t.__super__.fetch.call(this, e)
        }, t.prototype.next_page = function () {
            return this.page_info().next ? (this.page += 1, this.fetch({add: !0, silent: !1})) : !1
        }, t.prototype.url = function () {
            var e;
            return e = $.param({page: this.page, per_page: this.per_page}), "/documents/" + this.document_id + "/comments?" + e
        }, t
    }(Backbone.Collection), Scribd.Backbone.RootComments = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return i(t, e), t.prototype.sieve = function (e) {
            return e.is_root()
        }, t
    }(Backbone.Subset), Scribd.Backbone.DocumentCommentsView = function (t) {
        function r() {
            return this.render = n(this.render, this), this.load_more_comments = n(this.load_more_comments, this), this.loaded = n(this.loaded, this), r.__super__.constructor.apply(this, arguments)
        }

        return i(r, t), r.prototype.initialize = function () {
            var e, t;
            return this.render(), t = $(this.el).find(".document_comment_form:first"), Scribd.Backbone.current_user.get("blocked") && t.hide(), e = $(this.el).find(".document_comments_collection:first"), this.comment_form = new Scribd.Backbone.CommentFormView({model: this.model, el: t, parent_view: this}), this.collection_view = new Scribd.Backbone.CommentCollectionView({collection: new Scribd.Backbone.RootComments(null, {parent: this.model.get("comments")}), el: e, parent_view: this}), this.model.get("comments").on("loaded", this.loaded)
        }, r.prototype.events = {"click .load_more": "load_more_comments"}, r.prototype.loaded = function () {
            return this.$el.find(".load_more").removeClass("loading")
        }, r.prototype.load_more_comments = function () {
            return e("more_comments_button"), this.$el.find(".load_more").addClass("loading"), this.model.get("comments").next_page()
        }, r.prototype.render = function () {
            return this.$el.find(".load_more").hide(), this
        }, r
    }(Backbone.View), Scribd.Backbone.CommentFormView = function (t) {
        function r() {
            return this.submit_new_comment = n(this.submit_new_comment, this), this.require_login = n(this.require_login, this), this.template = n(this.template, this), this.enable_form = n(this.enable_form, this), this.filter_enter = n(this.filter_enter, this), this.input_changed = n(this.input_changed, this), this.show_error_message = n(this.show_error_message, this), this.disable_form = n(this.disable_form, this), this.render = n(this.render, this), r.__super__.constructor.apply(this, arguments)
        }

        return i(r, t), r.prototype.initialize = function () {
            return this.render()
        }, r.prototype.render = function () {
            return $(this.el).html(_.template(this.template())), this.$el.length && this.$(".comment_text:first").val() && this.$(".comment_text:first").val().length || this.$(".submit_button:first").addClass("disabled"), this
        }, r.prototype.disable_form = function () {
            return this.$(".comment_text:first").attr("disabled", !0), this.$(".submit_button:first").addClass("disabled"), this.$(".loading:first").show(), this.disabled = !0
        }, r.prototype.show_error_message = function () {
            return $("#comment_errors").text("There was an error submitting your comment. Please try again.")
        }, r.prototype.input_changed = function (e) {
            if (!this.$el)return;
            return this.$(".comment_text:first").val() && this.$(".comment_text:first").val().length ? this.$(".submit_button:first").removeClass("disabled") : $(".submit_button:first").addClass("disabled")
        }, r.prototype.filter_enter = function (e) {
            return e.keyCode === 13 ? (e.preventDefault(), this.submit_new_comment(e), !1) : !0
        }, r.prototype.enable_form = function () {
            return this.disabled = !1, this.$(".comment_text:first").attr("value", ""), this.$(".comment_text:first").attr("disabled", !1), this.$(".submit_button:first").removeClass("disabled"), this.$(".loading:first").hide()
        }, r.prototype.template = function () {
            return this._template || (this._template = $(".comment_form_template").html())
        }, r.prototype.events = {"click .submit_button": "submit_new_comment", "change .comment_text": "input_changed", "keyup .comment_text": "input_changed", "keydown .comment_text": "filter_enter", "click .comment_text": "require_login", "focus .comment_text": "require_login"}, r.prototype.require_login = function (e) {
            var t, n = this;
            if (!Scribd.logged_in)return typeof (t = $(e.target)).blur == "function" && t.blur(), Scribd.with_login("add_note", function () {
            })
        }, r.prototype.submit_new_comment = function (t) {
            var n = this;
            if (_(this.$(".comment_text:first").val() || "").chain().trim().isBlank().value())return;
            return e("submit_comment"), Scribd.with_login("add_comment", function () {
                var e, t;
                if (!n.disabled) {
                    t = {text: n.$(".comment_text:first").val()};
                    if (n.model instanceof Scribd.Backbone.Document)e = "document_id"; else {
                        if (!(n.model instanceof Scribd.Backbone.Annotation))throw"model must be an annotation or a document to be commented on.";
                        e = "annotation_id", t.document_id = n.model.get("document").get("_id")
                    }
                    return t[e] = n.model.get("_id"), n.model.get("comments").create(t, {wait: !0, error: n.show_error_message, complete: function () {
                        return n.enable_form()
                    }}), n.disable_form()
                }
            })
        }, r
    }(Backbone.View), Scribd.Backbone.AnnotationCommentFormView = function (t) {
        function r() {
            return this.submit_new_comment = n(this.submit_new_comment, this), this.template = n(this.template, this), r.__super__.constructor.apply(this, arguments)
        }

        return i(r, t), r.prototype.template = function () {
            return this._template || (this._template = $(".annotation_comment_form_template").html())
        }, r.prototype.submit_new_comment = function (t) {
            var n = this;
            trackEvent("newdoc", "Click", "annotation_submit_reply", Scribd.logged_in ? 1 : 0);
            if (_(this.$(".comment_text:first").val() || "").chain().trim().isBlank().value())return;
            return e("submit_comment"), Scribd.with_login("add_comment", function () {
                var e, t;
                if (!n.disabled) {
                    t = {text: n.$(".comment_text:first").val()};
                    if (n.model instanceof Scribd.Backbone.Document)e = "document_id"; else {
                        if (!(n.model instanceof Scribd.Backbone.Annotation))throw"model must be an annotation or a document to be commented on.";
                        e = "annotation_id", t.document_id = n.model.get("document").get("_id")
                    }
                    return t[e] = n.model.get("_id"), t.parent_id = n.model.get("comments").first().get("_id"), n.model.get("comments").first().get("children").create(t, {wait: !0, error: n.show_error_message, complete: function () {
                        return n.enable_form()
                    }}), n.disable_form()
                }
            })
        }, r
    }(Scribd.Backbone.CommentFormView), Scribd.Backbone.CommentReplyFormView = function (t) {
        function r() {
            return this.submit_new_reply = n(this.submit_new_reply, this), r.__super__.constructor.apply(this, arguments)
        }

        return i(r, t), r.prototype.events = {"click .submit_button": "submit_new_reply", "change .comment_text": "input_changed", "keyup .comment_text": "input_changed", "click .comment_text": "require_login", "focus .comment_text": "require_login"}, r.prototype.submit_new_reply = function (t) {
            var n = this;
            if (!this.disabledthis)return e("submit_reply"), Scribd.Backbone.document.get("comments").trigger("change"), this.model.get("children").create({text: this.$(".comment_text:first").val(), document_id: this.model.get("document_id"), parent_id: this.model.get("_id")}, {wait: !0, success: function () {
                return n.$el.hide()
            }, error: this.show_error_message, complete: this.enable_form}), this.disable_form()
        }, r
    }(Scribd.Backbone.CommentFormView), Scribd.Backbone.CommentView = function (t) {
        function r() {
            return this.toggle_reply_form = n(this.toggle_reply_form, this), this.toggle_children = n(this.toggle_children, this), this.destroy_comment = n(this.destroy_comment, this), this.mark_spam = n(this.mark_spam, this), this.mark_ham = n(this.mark_ham, this), this.render = n(this.render, this), this.scroll_to_annotation = n(this.scroll_to_annotation, this), this.template = n(this.template, this), r.__super__.constructor.apply(this, arguments)
        }

        return i(r, t), r.prototype.initialize = function (e) {
            return this.render(), this.parent_view = e.parent_view, this.collection_view = new Scribd.Backbone.CommentCollectionView({collection: this.model.get("children"), el: $(this.el).find(".child_collection:first")}), this.comment_form = new Scribd.Backbone.CommentReplyFormView({model: this.model, el: $(this.el).find(".reply_form.nested:first").hide()}), this.model.on("change", this.render)
        }, r.prototype.events = {"click .children_toggle": "toggle_children", "click .delete": "destroy_comment", "click .reply": "toggle_reply_form", "click .goto_annotation": "scroll_to_annotation", "click .mark_spam": "mark_spam", "click .mark_ham": "mark_ham"}, r.prototype.template = function () {
            return this._template || (this._template = $(".comment_template").html())
        }, r.prototype.scroll_to_annotation = function () {
            return $(".annotation").removeClass("on"), $.scrollTo($("#annotation_view_" + this.model.get("annotation").get("_id")).addClass("on")), $.scrollTo("-=60px")
        }, r.prototype.render = function () {
            var e;
            if (this.model.isNew())return;
            return e = !!$(this.el).children().length, e || $(this.el).html(_.template(this.template(), this.model.toJSON())), this.model.viewable_children() || $(this.el).find(".children_toggle:first").remove(), this.model.is_viewable() ? $(this.el).find(".comment_text:first").html(this.model.get("text")) : $(this.el).remove(), this
        }, r.prototype.mark_ham = function (e) {
            if (this.$el.find(".mark_ham:first") === $(e.currentTarget))return this.model.mark_ham(), this.$el.children().flash()
        }, r.prototype.mark_spam = function (e) {
            if (this.$el.find(".mark_spam:first")[0] === e.currentTarget)return this.model.mark_spam()
        }, r.prototype.destroy_comment = function (t) {
            var n = this;
            return e("delete_button"), Scribd.with_login("add_comment", function () {
                if (n.$(t.target).is(n.$(".delete:first")))return n.model.destroy({silent: !1, success: function () {
                }})
            })
        }, r.prototype.toggle_children = function (t) {
            e("toggle_children");
            if (this.$(t.target).is(this.$(".children_toggle:first")))return this.$(".children_toggle:first").toggleClass("closed"), this.$(".triangle").toggle(), this.collection_view.$el.slideToggle("fast")
        }, r.prototype.toggle_reply_form = function (t) {
            var n = this;
            if (this.model.get("deleted"))return;
            return e("reply_button"), Scribd.with_login("add_comment", function () {
                if (n.$(t.target).is(n.$(".reply:first")))return n.comment_form.$el.toggle()
            })
        }, r
    }(Backbone.View), Scribd.Backbone.CommentCollectionView = function (e) {
        function t() {
            return this.render = n(this.render, this), this.template = n(this.template, this), this.remove_comment_view = n(this.remove_comment_view, this), this.add_comment_view = n(this.add_comment_view, this), t.__super__.constructor.apply(this, arguments)
        }

        return i(t, e), t.prototype.initialize = function () {
            var e, t, n, r;
            this.comment_views = {}, this.render(), r = this.collection.models;
            for (t = 0, n = r.length; t < n; t++)e = r[t], this.add_comment_view(e);
            return this.collection.on("add", this.add_comment_view), this.collection.on("remove", this.remove_comment_view)
        }, t.prototype.in_annotation_view = function () {
            return this._in_annotation_view || (this._in_annotation_view = this.$el.parents(".annotation").length > 0)
        }, t.prototype.add_comment_view = function (e) {
            var t, n, r, i, s, o;
            if (this.comment_views[parseInt(e.get("_id"))])return;
            return this.in_annotation_view() ? (t = parseInt((o = this.$el.find(".comment:first").attr("id")) != null ? o.replace(/\D/g, "") : void 0), e.get("_id") < t ? r = this.$el.find(".comments_list").prepend("<div></div>").find("div:first") : r = this.$el.find(".comments_list:first").append("<div></div>").find("div:last")) : (i = parseInt((s = this.$el.find(".comment:first").attr("id")) != null ? s.replace(/\D/g, "") : void 0), !e.get("_id") || e.get("_id") > i ? r = this.$el.find(".comments_list").prepend("<div></div>").find("div:first") : r = this.$el.find(".comments_list:first").append("<div></div>").find("div:last")), n = new Scribd.Backbone.CommentView({model: e, el: r, parent_view: this}), this.comment_views[parseInt(e.get("_id"))] = n
        }, t.prototype.remove_comment_view = function (e) {
            var t, n;
            t = parseInt(e.get("_id")), n = this.comment_views[t];
            if (n.remove())return delete this.comment_views[t]
        }, t.prototype.template = function () {
            return this._template || (this._template = $(".comment_collection_template").html())
        }, t.prototype.render = function () {
            return this._rendered || (this._rendered = !0, $(this.el).html(this.template())), this
        }, t
    }(Backbone.View), Scribd.Backbone.DocumentCommentCollectionView = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return i(t, e), t
    }(Scribd.Backbone.CommentCollectionView)
}).call(this);


/* app/views/read2/store/buy_doc_manager.coffee @ 1384554002 */
(function () {
    Scribd.UI.BuyDocManager = function () {
        function e(e) {
            this.buy_url = e
        }

        return e.prototype.track_event = Scribd.make_tracker("payments"), e.prototype.setup_buy_buttons = function (e) {
            var t, n = this;
            return t = $(document), t.ready(function () {
                return Scribd.ServerOptions && Scribd.ServerOptions.payments_store_disabled === !0 ? $(".missing_page_buy_button").hide() : (Scribd.current_doc.show_archive_paywall ? $(".missing_page_buy_link .btn_inner").text(e) : $(".missing_page_buy_button").hide(), t.dispatch("click", "paid", {missing_page_buy_button: Scribd.current_doc.is_pmp ? n.get_premium_callback("missing_page_signup") : Scribd.current_doc.can_purchase ? n.get_buy_callback("button:click") : Scribd.current_doc.view_restricted ? n.get_archive_callback("missing_page_signup") : void 0}))
            })
        }, e.prototype.setup_missing_page_title = function (e) {
            var t, n = this;
            return t = $(document), t.ready(function () {
                if (Scribd.current_doc.is_pmp || Scribd.current_doc.can_purchase || Scribd.current_doc.show_archive_paywall)return $(".missing_page_number_title").text(e)
            })
        }, e.prototype.get_buy_callback = function (e) {
            var t = this;
            return function () {
                var n;
                return n = function () {
                    return t.track_event("newdoc/store", "buy_now" + e), window.location.href = t.buy_url
                }, Scribd.logged_in ? Scribd.FacebookSession.attempt_facebook_upgrade(n) : n(), !1
            }
        }, e.prototype.get_subscription_callback = function (e, t) {
            var n = this;
            return function () {
                var r, i, s;
                return r = {context: Scribd.current_doc.signup_context, page: "read", action: e, platform: "web", logged_in: !!Scribd.logged_in}, r = encodeURIComponent(JSON.stringify(r)), i = "doc=" + Scribd.current_doc.id + "&metadata=" + r, s = function () {
                    return n.track_event("newdoc", "" + Scribd.current_doc.signup_context + ":missing_page_button:click"), window.location.href = t + "?" + i
                }, Scribd.logged_in ? Scribd.FacebookSession.attempt_facebook_upgrade(s) : Scribd.LoginLightbox.remote_open("archive_lb", "/archive/login?" + i, Scribd.toolbar_params()), !1
            }
        }, e.prototype.get_premium_callback = function (e) {
            return this.get_subscription_callback(e, "/archive/pmp_checkout")
        }, e.prototype.get_archive_callback = function (e) {
            return this.get_subscription_callback(e, "/archive/plans")
        }, e
    }()
}).call(this);


/* :class_inlines, 'app/views', ... @ 1384554002 */
/* app/views/shared/dialogs/download_dialog.coffee @ 1384554002 */
(function () {
    Scribd.UI || (Scribd.UI = {}), Scribd.UI.dispatch = function (e, t, n, r, i) {
        var s, o, u;
        for (u in t) {
            if (!t.hasOwnProperty(u))continue;
            s = $(e.target).parents().andSelf().filter("." + u);
            if (s.size() > 0)return i || (e.stopPropagation(), e.preventDefault()), s.hasClass("disabled") ? !0 : (o = r ? r + "_" + u : u, trackEvent("newdoc", "Click", o, Scribd.logged_in ? 1 : 0), t[u].call(n, s, e), !0)
        }
        return!1
    }, Scribd.UI.DownloadDialog = function () {
        function e(e, t) {
            _.bindAll(this), this.options = _.extend({secret_password: "", extension_sizes: {}, show_premium: !1}, t || {}), this.container = $(e), this.suggested_type = this.container.find(".selected_documents").data("suggested_type"), this.radios = this.container.find(".download_types"), this.sizes_node = $("#document_sizes"), this.extensions_node = $("#document_extensions"), this.download_icon = this.container.find(".big_download_icon"), this.download_button = this.container.find(".download_button"), this.after_download_test = this.container.find(".object_grid").length > 0, this.current_extension = this.getCurrentExtension(), this.document = this.options.document, this.download_query = {secret_password: this.options.secret_password, source: this.options.source}, this.autodown_query = {secret_password: this.options.secret_password, source: this.options.source}, this.updateExtension(this.current_extension), this.setupEvents()
        }

        return e.prototype.actions = {thumb: function (e) {
            return window.location = $(e).parent().attr("href")
        }, title_row: function (e) {
            return window.location = $(e).find(".title").attr("href")
        }, download_button: function () {
        }, try_again: function () {
            return Scribd.download_actions.download_dialog_lightbox(this.document)
        }, bookmark: function (e) {
            var t;
            return e = $(e), t = e.closest(".document_cell").data("object_id"), $.post("/document_bookmarks/create", {document_id: t}, "json"), this.switchButton(e)
        }, unbookmark: function (e) {
            var t;
            return e = $(e), t = e.closest(".document_cell").data("object_id"), $.post("/document_bookmarks/delete", {document_id: t}, "json"), this.switchButton(e)
        }}, e.prototype.getCurrentExtension = function () {
            return this.radios.find("input:checked").val()
        }, e.prototype.setupEvents = function () {
            var e, t = this;
            return this.actions_classes = function () {
                var t, n, r, i;
                r = _.keys(this.actions), i = [];
                for (t = 0, n = r.length; t < n; t++)e = r[t], i.push("." + e);
                return i
            }.call(this).join(", "), this.radios.find("input").on("click", function (e) {
                return t.updateExtension($(e.target).val())
            }), this.actions.download_button = this.options.show_premium ? this.premiumDownloadHandler : this.downloadHandler, this.container.click(this.actions_classes, function (e) {
                Scribd.UI.dispatch(e, t.actions, t, "download_dialog")
            })
        }, e.prototype.switchButton = function (e) {
            return e = $(e), e.siblings(".save_button").andSelf().toggleClass("hidden")
        }, e.prototype.downloadHandler = function (e) {
            var t = this;
            return this.container.addClass("download_started"), this.after_download_test && (this.suggested_type && trackEvent("newdoc", "Click", "download_dialog_show_" + this.suggested_type, 1), this.container.addClass("after_download_test"), this.container.find(".title:first").text("Your Download Has Started, You Might Like"), this.recommened_documents_grid = new Scribd.UI.ObjectGrid(this.container.find(".selected_documents .document_grid")), this.popups = new Scribd.UI.DocumentPopups(this.container.find(".document_popups"), this.container.find(".selected_documents"), "download_dialog:document_popups"), this.recommened_documents_grid.container.find(".thumb").each(function () {
                var e;
                return e = $(this).css("background-image"), $(this).css("background-image", "none"), $(this).css("background-image", e)
            })), _.delay(function () {
                return window.location.href = t.downloadUrl()
            }, 2e3)
        }, e.prototype.premiumDownloadHandler = function () {
            var e;
            return e = $.param.querystring("http://www.scribd.com/archive/plans", {doc: this.options.document_id}), window.location = e
        }, e.prototype.updateExtension = function (e) {
            var t;
            return this.current_extension = e, t = function (t, n) {
                return n = $(n), n.data("ext") === e ? n.show() : n.hide()
            }, this.extensions_node.find("span").each(t), this.sizes_node.find("span").each(t), this.download_icon.find(".icon_text").html(e), this.download_query.extension = e, this.autodown_query.autodown = e, this.download_button.find(".button_text").html("Download " + e)
        }, e.prototype.downloadUrl = function () {
            return $.param.querystring(this.options.base_url, this.download_query)
        }, e.prototype.autodownUrl = function () {
            return $.param.querystring(this.options.autodown_base_url, this.autodown_query)
        }, e
    }()
}).call(this);


/* app/views/shared/dialogs/collections_dialog.coffee @ 1384554002 */
(function () {
    var e, t = {}.hasOwnProperty, n = function (e, n) {
        function i() {
            this.constructor = e
        }

        for (var r in n)t.call(n, r) && (e[r] = n[r]);
        return i.prototype = n.prototype, e.prototype = new i, e.__super__ = n.prototype, e
    }, r = [].slice;
    Scribd.Backbone = (e = Scribd.Backbone) != null ? e : {}, Scribd.Backbone.DocumentCollectionsDialog = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return n(t, e), t.make_list = function (e) {
            return new Scribd.Backbone.DocumentCollectionList([], e)
        }, t.create_or_replace = function (e, t) {
            var n, i, s, o, u = this;
            t == null && (t = {}), this.instance && (this.instance.remove(), this.instance.undelegateEvents()), i = this.make_list(t), o = {el: $(e), collection: i};
            for (n in t)s = t[n], o[n] = s;
            return this.instance = new this(o), i.fetch({silent: !1, success: function () {
                return i.trigger.apply(i, ["fetch"].concat(r.call(arguments)))
            }}), this.instance
        }, t.prototype.initialize = function () {
            return _.bindAll(this, "render"), this.list_view = new Scribd.Backbone.DocumentCollectionListView({el: this.$el.find(".list_view:first"), collection: this.collection}), this.collection.on("change", this.render), this.collection.on("add", this.render), this.render()
        }, t.prototype.render = function () {
            return this.list_view.render()
        }, t
    }(Backbone.View), Scribd.Backbone.PickCollectionDialog = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return n(t, e), t.prototype.events = {"click .list_item": function (e) {
            return $(e.target).addClass("selected"), this.$el.trigger("scribd:collection_picked", $(e.currentTarget).data("model").toJSON()), Scribd.Lightbox.close()
        }}, t
    }(Scribd.Backbone.DocumentCollectionsDialog), Scribd.Backbone.AddToCollectionDialog = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return n(t, e), t.prototype.events = {"click .save": function (e) {
            if (!this.form_visible)return this.collection.save(), Scribd.Lightbox.close();
            if (this.form_view.save())return this.show_listing()
        }, "click .cancel": function (e) {
            return this.form_visible && this.collection.length > 0 ? (this.show_listing(), this.form_view.$el.find("textarea, input").val("")) : Scribd.Lightbox.close()
        }, "click .new_collection": function (e) {
            return this.show_form()
        }}, t.prototype.render = function () {
            return this.list_view.render(), this.form_view.render(), this.show_listing()
        }, t.prototype.initialize = function () {
            return _.bindAll(this, "show_form", "show_listing"), this.form_view = new Scribd.Backbone.DocumentCollectionFormView({el: this.$el.find(".form_view:first"), collection: this.collection}), t.__super__.initialize.apply(this, arguments)
        }, t.prototype.show_form = function () {
            return this.$el.find(".title").text("Create a New Collection"), this.list_view.$el.hide(), this.form_view.$el.show(), this.form_visible = !0
        }, t.prototype.show_listing = function () {
            return this.$el.find(".title").text("Add Documents to Your Collections"), this.list_view.$el.show(), this.form_view.$el.hide(), this.form_visible = !1
        }, t
    }(Scribd.Backbone.DocumentCollectionsDialog), Scribd.Backbone.DocumentCollectionFormView = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return n(t, e), t.prototype.events = {"click .show_privacy": "show_privacy_options", "focus #collection_name_input, #collection_description_input ": "clear_default", "blur #collection_name_input, #collection_description_input ": "show_default"}, t.prototype.initialize = function () {
            return _.bindAll(this, "show_privacy_options", "save"), this.show_default({target: $("#collection_description_input")}), this.show_default({target: $("#collection_name_input")})
        }, t.prototype.render = function () {
            return this.$el.find(".privacy_option").not(".default").hide(), this.$el.find(".privacy_option.default").show()
        }, t.prototype.show_privacy_options = function () {
            return this.$el.find(".privacy_option").not(".default").show(), this.$el.find(".privacy_option.default").hide()
        }, t.prototype.clear_default = function (e) {
            var t;
            t = $(e.target);
            if (t.val() === t.data("default"))return t.removeClass("default"), t.val("")
        }, t.prototype.show_default = function (e) {
            var t;
            t = $(e.target);
            if (t.val() === "")return t.addClass("default"), t.val(t.data("default"))
        }, t.prototype.is_valid = function () {
            var e;
            return e = this.$el.find("#collection_name_input").val(), !/^\s*$/.test(e)
        }, t.prototype.save = function () {
            var e;
            return!this.is_valid() || this.$el.find("#collection_name_input").hasClass("default") ? (this.$el.find(".new_collection_name .label").addClass("error"), this.$el.find(".action.error.hidden").removeClass("hidden"), !1) : ($(".new_collection_name .label").removeClass("error"), this.$el.find(".action.error").addClass("hidden"), Scribd.Backbone.new_collection = e = this.collection.create({combined_privacy_type: this.$el.find(".privacy_option input:radio:checked").val(), html_description: this.$el.find("#collection_description_input").val(), name: this.$el.find("#collection_name_input").val()}, {wait: !0}), e.set("selected", "1"), e.set("word_documents", "1"), e.set("word_documents", "1"), this.collection.add(e), e.isNew = function () {
                return!1
            }, this.$el.find("input, textarea").empty())
        }, t
    }(Backbone.View), Scribd.Backbone.DocumentCollectionView = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return n(t, e), t.prototype.events = {"click input[type=checkbox]": function (e) {
            return this.model.set("selected", $(e.target).is(":checked"))
        }}, t.prototype.initialize = function () {
            var e = this;
            return _.bindAll(this, "render", "template"), this.render(), this.model.on("hide", function () {
                return e.$el.hide()
            }), this.model.on("show", function () {
                return e.$el.show()
            })
        }, t.prototype.template = function () {
            return this._template || (this._template = _.template($(".collection_template").html()))
        }, t.prototype.render = function () {
            return this.$el.html(this.template()(this.model.toJSON())), this.$el.find(".select_collection:first").prop("checked", parseInt(this.model.get("selected"), 10) === 1), this.model.original_selected && this.$el.find(".inner_container:first").addClass("selected"), this.model.get("privacy_type") !== "private" && this.$el.find(".private:first").hide(), this.$el.find(".list_item, input").val(this.model.id).data("model", this.model), this
        }, t
    }(Backbone.View), Scribd.Backbone.DocumentCollectionListView = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return n(t, e), t.prototype.events = {"click .next_page": function () {
            return this.collection.next_page()
        }, "click .prev_page": function () {
            return this.collection.prev_page()
        }}, t.prototype.initialize = function () {
            return _.bindAll(this, "render", "add_document_collection_view", "remove_document_collection_view", "clear_document_collection_views", "paginator"), this.doc_collection_views = {}, this.collection.on("add", this.add_document_collection_view), this.collection.on("remove", this.remove_document_collection_view), this.collection.on("reset", this.clear_document_collection_views), this.collection.on("page_changed", this.paginator), this.render()
        }, t.prototype.template = function () {
            return this._template || (this._template = $(".paginator_template").html())
        }, t.prototype.render = function () {
            return this.collection.each(function (e) {
                return e.render
            }), this.paginator()
        }, t.prototype.paginator = function () {
            var e;
            if (this.collection.length > 0)return $(this.el).find(".paginator").html(_.template(this.template(), this.collection.toJSON())), e = this.collection.page_info(), this.$el.toggleClass("hide_paginator", !e.next && !e.prev), e.next ? $(this.el).find(".paginator .next_page").removeClass("disabled") : $(this.el).find(".paginator .next_page").addClass("disabled"), e.prev ? $(this.el).find(".paginator .prev_page").removeClass("disabled") : $(this.el).find(".paginator .prev_page").addClass("disabled");
            return
        }, t.prototype.add_document_collection_view = function (e, t) {
            var n;
            return this.doc_collection_views[e] = n = new Scribd.Backbone.DocumentCollectionView({model: e}), this.$el.find(".list_container").append(n.el), this.paginator()
        }, t.prototype.remove_document_collection_view = function (e, t) {
            var n;
            return n = this.doc_collection_views[e], n.$el.remove(), this.paginator()
        }, t.prototype.clear_document_collection_views = function () {
            return this.$el.find(".list_container").empty(), this.paginator()
        }, t
    }(Backbone.View), Scribd.Backbone.DocumentCollection = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return n(t, e), t.prototype.initialize = function () {
            return this.original_selected = !!parseInt(this.get("selected"), 10), this.set("visible", !0), this.set("name", _.str.truncate(this.get("name"), 30))
        }, t.prototype.save = function (e, t) {
            return this.isNew() ? this.save_new(t.collection.document_id) : this.get("selected") && this.get("selected") !== "0" ? this.add_document_to_collection(e) : this.remove_document_from_collection(e)
        }, t.prototype.add_document_to_collection = function (e) {
            if (this.original_selected)return;
            return $.ajax("" + this.add_remove_url() + "/add_document", {type: "post", data: {document_id: e}})
        }, t.prototype.remove_document_from_collection = function (e) {
            if (!this.original_selected)return;
            return $.ajax("" + this.add_remove_url() + "/remove_document", {type: "post", data: {_method: "delete", document_id: e}})
        }, t.prototype.save_new = function (e) {
            if (!this.isNew())return;
            return $.ajax(this.save_url(), {type: "post", data: {"document_collection[combined_privacy_type]": this.get("combined_privacy_type"), "document_collection[html_description]": this.get("html_description"), "document_collection[name]": this.get("name"), format: "json", document_id: e}, success: this.parse})
        }, t.prototype.parse = function (e) {
            return e.collections ? e : e
        }, t.prototype.save_url = function () {
            var e;
            return e = Scribd.named_current_user_sync().login, "/users/" + e + "/document_collections"
        }, t.prototype.add_remove_url = function () {
            return"/document_collections/" + this.get("id")
        }, t
    }(Backbone.Model), Scribd.Backbone.DocumentCollectionList = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return n(t, e), t.prototype.model = Scribd.Backbone.DocumentCollection, t.prototype.initialize = function (e, n) {
            return _.bindAll(this, "page_changed"), this.loaded_pages = [1], this.view = n.view_mode || "add_document_to_collections", this.page = n.page || 1, this.per_page = n.per_page || 10, this.document_id = n.document_id, t.__super__.initialize.apply(this, arguments)
        }, t.prototype.fetch = function (e) {
            var t, n = this;
            return e = _.extend({add: !0}, e), e.reset && this.reset(), t = Backbone.Collection.prototype.fetch.call(this, e), t.done(function () {
                if (!e.silent)return n.trigger("fetch", n, e)
            }), t
        }, t.prototype.page_info = function () {
            var e;
            return this.info != null ? this.info : (this.info = {total: this.total, page: this.page, per_page: this.per_page, pages: Math.ceil(this.total / this.per_page), prev: !1, next: !1}, e = Math.min(this.total, this.page * this.per_page), this.info.range = [(this.page - 1) * this.per_page + 1, e], this.start = this.info.range[0], this.end = this.info.range[1], this.page > 1 && (this.info.prev = this.page - 1), this.page < this.info.pages && (this.info.next = this.page + 1), this.last_page = this.page, this.info)
        }, t.prototype.next_page = function () {
            return this.page_info().next ? (this.page += 1, this.info = null, _.contains(this.loaded_pages, this.page) ? this.page_changed() : this.fetch({add: !0, remove: !1, silent: !1, success: this.page_changed})) : !1
        }, t.prototype.prev_page = function () {
            return this.page_info().prev ? (this.page -= 1, this.info = null, _.contains(this.loaded_pages, this.page) ? this.page_changed() : this.fetch({add: !0, remove: !1, silent: !1, success: this.page_changed})) : !1
        }, t.prototype.page_changed = function () {
            var e, t, n = this;
            return _.contains(this.loaded_pages, this.page) || this.loaded_pages.push(this.page), t = this.filter(function (e) {
                return parseInt(e.get("page")) === parseInt(n.page)
            }), e = this.filter(function (e) {
                return parseInt(e.get("page")) !== parseInt(n.page)
            }), _.each(t, function (e) {
                return e.trigger("show")
            }), _.each(e, function (e) {
                return e.trigger("hide")
            }), this.trigger("page_changed")
        }, t.prototype.url_params = function () {
            var e;
            return e = {view: this.view, page: this.page, per_page: this.per_page}, this.document_id && (e.document_id = this.document_id), e
        }, t.prototype.toJSON = function () {
            return this.page_info(), {start: this.start, end: this.end, page: this.page, per_page: this.per_page, total: this.total}
        }, t.prototype.save = function () {
            var e = this;
            return this.each(function (t) {
                return t.save(e.document_id)
            })
        }, t.prototype.url = function () {
            return"/my_document_collections.json?" + $.param(this.url_params())
        }, t.prototype.parse = function (e) {
            var t = this;
            return this.page = e.page, this.per_page = e.per_page, this.total = e.total, _.each(e.collections, function (e) {
                return e.page = t.page
            }), e.collections
        }, t
    }(Backbone.Collection)
}).call(this);


/* app/views/shared/carousels/carousel.coffee @ 1384554002 */
(function () {
    Scribd.UI.Carousel = function () {
        function e(e, t) {
            var n, r, i, s = this;
            this.container = e, t == null && (t = {}), Scribd.UI.Carousel.carousels.push(this), this.container = $(this.container), this.carousel = this.container.find(".carousel"), this.container.delegate_tracking("click", t.page || "carousel"), this.carousel.jcarousel({wrap: "circular"}), this.toggle_carousel_tools(), $(window).on("resize.jcarousel", function () {
                return s.toggle_carousel_tools()
            }), this.container.one("click", ".carousel_prev,.carousel_next", function () {
                return Scribd.track_event(t.page || "carousel", "click", "carousel:first_carousel_click")
            }), this.container.find(".carousel_prev").on("click", function () {
                return s.carousel.jcarousel("scroll", "-=3", function () {
                    return s.enable_all_images()
                })
            }), this.container.find(".carousel_next").on("click", function () {
                return s.carousel.jcarousel("scroll", "+=3", function () {
                    return s.enable_all_images()
                })
            }), this.container.on("mouseenter", function () {
                var e, t, n, r;
                r = Scribd.UI.Carousel.carousels;
                for (t = 0, n = r.length; t < n; t++)e = r[t], e.show_paddles(!1);
                return s.show_paddles(!0)
            }), this.carousel.touchwipe({wipeLeft: function () {
                return s.carousel.jcarousel("scroll", "+=3"), s.enable_all_images(), Scribd.track_event("carousel", "wipe", "left")
            }, wipeRight: function () {
                return s.carousel.jcarousel("scroll", "-=3"), s.enable_all_images(), Scribd.track_event("carousel", "wipe", "right")
            }, min_move_x: 20, min_move_y: 20, preventDefaultEvents: !0}), this.animation_lock = !1, this.container.mousewheel(function (e, t, n, r) {
                if (r !== 0)return;
                s.enable_all_images(), e.preventDefault();
                if (s.animation_lock)return;
                return s.animation_lock = !0, setTimeout(function () {
                    return s.animation_lock = !1
                }, 200), n > 0 ? (s.carousel.jcarousel("scroll", "+=" + Math.min(n, 3)), Scribd.track_event("carousel", "scroll", "right")) : n < 0 && (s.carousel.jcarousel("scroll", "+=" + Math.max(n, -3)), Scribd.track_event("carousel", "scroll", "left")), !1
            }), this.container.data("lazy_images") && (r = this.container.offset().top, i = $(window), n = function () {
                if (i.scrollTop() + i.height() > r)return s.enable_visible_images(), i.off("scroll resize", n)
            }, i.on("scroll resize", n), _.defer(n))
        }

        return e.prototype.toggle_carousel_tools = function () {
            return this.carousel.jcarousel("fullyvisible").length === this.carousel.find("li.item").length ? this.container.addClass("fits_on_screen") : this.container.removeClass("fits_on_screen")
        }, e.prototype.enable_visible_images = function () {
            var e, t, n, r, i;
            if (!this.container.data("lazy_images"))return;
            this.container.data("lazy_images", !1), r = this.carousel.jcarousel("visible"), i = [];
            for (t = 0, n = r.length; t < n; t++)e = r[t], i.push(this.show_image($(e)));
            return i
        }, e.prototype.enable_all_images = function () {
            var e, t, n, r, i;
            if (!this.container.data("lazy_horizontal_images"))return;
            this.container.data("lazy_horizontal_images", !1), r = this.carousel.jcarousel("items"), i = [];
            for (t = 0, n = r.length; t < n; t++)e = r[t], i.push(this.show_image($(e)));
            return i
        }, e.prototype.show_image = function (e) {
            var t, n;
            t = e.find(".thumb");
            if (n = t.data("thumb_url"))return t.removeData("thumb_url"), window.location.hash.match(/\bdebug_images\b/) ? (t.css({backgroundColor: "red"}), setTimeout(function () {
                return t.css({backgroundImage: "url('" + n + "')"})
            }, 500)) : t.css({backgroundImage: "url('" + n + "')"})
        }, e.prototype.show_paddles = function (e) {
            return e == null && (e = !0), e ? this.container.find(".paddle").addClass("always_visible") : this.container.find(".paddle").removeClass("always_visible")
        }, e
    }(), Scribd.UI.Carousel.carousels = []
}).call(this);


/* app/views/login/_archive_login_lightbox.coffee @ 1384554002 */
(function () {
    Scribd.ArchiveLoginLightbox = function () {
        function e(e) {
            var t = this;
            this.set_page(), $(e).on("click", "a.skip_login", function () {
                return Scribd.track_event("login_lightbox", "click", "" + t.page + ":login_lightbox:skip_login")
            })
        }

        return e.prototype.set_page = function () {
            var e, t;
            this.page = window.location.pathname.split("/")[1], this.page || (this.page = "home"), ((t = Scribd.current_doc) != null ? t.signup_context : void 0) != null && (this.page = "" + this.page + "_" + Scribd.current_doc.signup_context);
            if ($("#archive_login_test_choice"))return e = $("#archive_login_test_choice").data("choice"), this.page = "" + this.page + "_" + e
        }, e
    }()
}).call(this);


/* app/views/shared/document_popups.coffee @ 1384554002 */
(function () {
    var e, t = {}.hasOwnProperty, n = function (e, n) {
        function i() {
            this.constructor = e
        }

        for (var r in n)t.call(n, r) && (e[r] = n[r]);
        return i.prototype = n.prototype, e.prototype = new i, e.__super__ = n.prototype, e
    };
    e = function (e) {
        function t(e, n) {
            t.__super__.constructor.apply(this, arguments), n && this.container.delegate_tracking("click", n)
        }

        return n(t, e), t.prototype.find_object = function (e) {
            var t, n;
            return n = e.closest(".object_popup"), t = n.data(), {type: n.is(".collection_popup") && "document_collection" || "document", id: t.object_id}
        }, t.prototype.download_btn = function (e) {
            var t;
            return t = this.find_object(e).id, Scribd.download_actions.download(t, "document_list", "document_list_download")
        }, t.prototype.hide_document_btn = function (e, t) {
            var n, r = this;
            return n = e.closest(".object_popup").data("object_id"), $.ajax({type: "POST", url: "/documents_hidden_from_profiles", data: Scribd.CSRF.with_token({id: n}), success: function () {
                return window.location.reload()
            }})
        }, t.prototype.unhide_document_btn = function (e, t) {
            var n, r;
            return r = e.closest(".object_popup"), n = r.data("object_id"), $.ajax({type: "POST", url: "/documents_hidden_from_profiles/" + n, data: Scribd.CSRF.with_token({_method: "delete"}), success: function (e) {
                return typeof t == "function" ? typeof t == "function" ? t(e) : void 0 : r.removeClass("is_hidden show_hide")
            }})
        }, t.rate_document = function (e, t, n) {
            var r = this;
            return $.ajax({type: "POST", url: "/ratings", data: {word_document_id: e, rated: t}, success: function (e) {
                if (e.total_document_count != null)return n(e.total_document_count)
            }})
        }, t
    }(Scribd.DocumentActions), Scribd.UI || (Scribd.UI = {}), Scribd.UI.DocumentPopups = function () {
        function t(e, t, n) {
            var r = this;
            n && (this.context = n), this._popup_drop = $(e), t && this.bind(t), $(document.body).on("click", function (e) {
                if ($(e.target).closest(".object_popup").length)return;
                return r.hide_popup()
            }), $(document.body).on("Scribd:hide_popups", function () {
                return r.hide_popup()
            }), new this.constructor.Actions(this._popup_drop, this.context)
        }

        return t.prototype.context = "document_popups", t.prototype.popup_timeout = 300, t.prototype.fetch_data_timeout = 150, t.prototype.remove_popup_timeout = 500, t.prototype.show_hide = !1, t.Actions = e, t.prototype.hide_popup = function () {
            return this.current_popup && this.current_popup.data("cell").removeClass("popup_open"), this.current_popup = null, this._popup_drop.children().detach()
        }, t.prototype.make_document_popup = function (e) {
            var t, n = this;
            return this._doc_popup_template || (this._doc_popup_template = _.template($("#tpl_document_popup").html())), !/excerpt/i.test(e.title) && e.document_type === "book_excerpt" && (e.title = "[Excerpt] " + e.title), t = $(this._doc_popup_template(e)), t.on("click", ".save_bookmark_btn", function (e) {
                return $(e.currentTarget).find(".btn_inner").text("Saved!")
            }), e.user_can_save_bookmarks && t.addClass("user_can_save_bookmarks"), e.publisher.is_verified && (e.authors.length ? t.find(".verified_badge.author_badge").removeClass("hidden") : t.find(".verified_badge.publisher_badge").removeClass("hidden"), t.find(".stat_ratings").addClass("margin-left")), e.is_hidden && t.addClass("is_hidden show_hide"), e.page_count === 1 && t.find(".page_count").addClass("singular"), e.bookmark_id && t.find(".save_bookmark_btn").addClass("saved"), e.document_type === "book_excerpt" && t.addClass("book_excerpt"), e.view_restriction_message !== "" && (t.find(".view_restriction_msg").html(e.view_restriction_message), t.find(".view_restriction_msg").css("display", "inline-block")), e.read_full ? t.find(".read_now_btn").show() : t.find(".read_excerpt_btn").show(), t
        }, t.prototype.make_collection_popup = function (e) {
            var t, n, r, i;
            return this._col_popup_template || (this._col_popup_template = _.template($("#tpl_collection_popup").html())), e.remaining_docs = e.document_count - e.documents.length, i = $(this._col_popup_template(e)), e.remaining_docs < 1 && i.addClass("no_remaining"), e.document_count === 1 && i.find(".document_count").addClass("singular"), this._doc_row_template || (this._doc_row_template = _.template($("#tpl_popup_document_row").html())), n = function () {
                var n, i, s, o;
                s = e.documents, o = [];
                for (r = n = 0, i = s.length; n < i; r = ++n)t = s[r], t.index = r + 1, o.push($(this._doc_row_template(t))[0]);
                return o
            }.call(this), $(n).appendTo(i.find(".document_list")), i
        }, t.prototype.show_popup = function (e, t) {
            var n, r, i, s, o, u, a, f, l, c, h, p, d, v, m, g = this;
            if ((m = this.current_popup) != null ? m.data("cell").is(e) : void 0)return;
            return this.current_popup && this.hide_popup(), (this.tracker || (this.tracker = Scribd.make_tracker(this.context)))("show", "show", {interactive: !1}), t.context = this.context, t.list_title = e.data("object_list_title"), t.position = e.data("object_position"), (a = e.data("popup_elm")) || (o = !0, a = e.is(".document_cell") ? this.make_document_popup(t) : e.is(".collection_cell") ? this.make_collection_popup(t) : void 0, a.data("cell", e), a.data("object", t), (f = e.data("popup_classes")) && a.addClass(f), e.data("popup_elm", a)), _.isEqual(e.offsetParent()[0], this._popup_drop.offsetParent()[0]) ? p = e.position() : p = e.offset(), h = 539, c = 24, l = 195, d = $(window).width() - c, u = p.left + l + h > d, u && (r = p.left - h + c, r < 0 && (p.left += -r)), e.addClass("popup_open"), this.current_popup = a.toggleClass("on_left", u).css({left: p.left + "px", top: p.top + "px"}).appendTo(this._popup_drop.children().detach().end()), o && (i = a.find(".document_description"), s = i.find(".description"), s.text(_.trim(_.unescape(s.text().replace("&nbsp;", " ")))), i.length && a.find(".document_description").dotdotdot({after: "a.more", callback: function (e) {
                if (!e)return i.find(".more").remove()
            }}), n = a.find(".creator_row_publisher"), n.dotdotdot({wrap: "letter"}), v = a.find(".stat_ratings"), v.find(".stars").star_ratings({defaultValue: t.stats.rating_average, force_login: !0}), v.on("Scribd:rating", function (e, t) {
                var n;
                return n = a.data("object_id"), v.find(".label").text("Your Rating:"), Scribd.UI.DocumentPopups.Actions.rate_document(n, t, function (e) {
                    return a.find(".rating_number").text(_.numberFormat(e))
                })
            })), this.current_popup.hide().fadeIn()
        }, t.prototype.popup_data = function (e, t) {
            var n, r;
            r = e.data("request"), n = e.data("object_id"), r || (r = function () {
                if (e.is(".document_cell"))return $.get("/documents/popup_data?id=" + n);
                if (e.is(".collection_cell"))return $.get("/user_document_collections/popup_data?id=" + n);
                throw"Don't know how to handle popup"
            }(), e.data("request", r));
            if (t)return r.done(t)
        }, t.prototype.bind = function (e) {
            var t, n, r, i, s, o = this;
            return e = $(e), t = null, r = _.debounce(function () {
                var e;
                e = t;
                if (e)return o.popup_data(e, function (n) {
                    if (e !== t)return;
                    return o.show_popup(e, n)
                })
            }, this.popup_timeout), n = _.debounce(function () {
                if (t)return o.popup_data(t)
            }, this.fetch_data_timeout), $(document.body).on("mousemove", r).on("mousemove", n), i = function (e) {
                var t;
                if (t = e.data("remove_popup_timeout"))return e.removeData("remove_popup_timeout"), window.clearTimeout(t)
            }, s = function (e) {
                var t, n;
                if (o.current_popup && o.current_popup.data("cell").is(e)) {
                    if (e.data("remove_popup_timeout"))return;
                    return t = o.current_popup, n = window.setTimeout(function () {
                        e.removeData("remove_popup_timeout");
                        if (o.current_popup === t)return o.hide_popup()
                    }, o.remove_popup_timeout), e.data("remove_popup_timeout", n)
                }
            }, e.on("mouseenter mouseleave", ".object_cell .thumb", function (e) {
                var n, r;
                return n = $(e.currentTarget).closest(".object_cell"), r = e.type === "mouseenter", r ? (t = n, i(n)) : (t = null, s(n))
            }), e.on("mouseenter mouseleave", ".object_popup", function (e) {
                var t, n, r;
                return r = $(e.currentTarget), t = r.data("cell"), n = e.type === "mouseenter", n ? i(t) : s(t)
            })
        }, t
    }()
}).call(this);


/* app/views/read2/show.coffee @ 1384554002 */
(function () {
    var e = {}.hasOwnProperty, t = function (t, n) {
        function i() {
            this.constructor = t
        }

        for (var r in n)e.call(n, r) && (t[r] = n[r]);
        return i.prototype = n.prototype, t.prototype = new i, t.__super__ = n.prototype, t
    }, n = function (e, t) {
        return function () {
            return e.apply(t, arguments)
        }
    };
    Scribd.BaseReadPage = function () {
        function e() {
        }

        return e.prototype.setup_buy = function () {
            var e, t, n, r;
            if (!Scribd.current_doc.buy_url)return;
            return r = Scribd.current_doc, e = r.buy_url, t = r.missing_page_label, n = r.missing_page_title, this.buy_manager = new Scribd.UI.BuyDocManager(e), this.buy_manager.setup_buy_buttons(t), this.buy_manager.setup_missing_page_title(n)
        }, e.prototype.setup_events = function () {
            var e = this;
            return $(document.body).on("scribd:show_notes", function () {
                return Scribd.ReadPage.overlay_panel.toggle(), Scribd.ReadPage.activity_list.container.find(".picker_item[data-value='notes']").click()
            })
        }, e.prototype.toggle_reading_mode = function (e) {
            this._to_hide || (this._to_hide = $("#doc_toolbar, #global_header, #epub_scrubber")), this._to_show || (this._to_show = this.container.find("#reading_mode_bar"));
            if (!this.container.is(":visible"))return;
            if (this._to_hide.is(":animated"))return;
            if (e === !0 && this.in_reading_mode)return;
            if (e === !1 && !this.in_reading_mode)return;
            if (!this.in_reading_mode && !this.reading_mode_allowed())return;
            return this.in_reading_mode = !this.in_reading_mode, this.in_reading_mode ? Scribd.ReadPage.toolbar.lock() : Scribd.ReadPage.toolbar.unlock(), this._to_hide.fadeToggle(!this.in_reading_mode), this._to_show.fadeToggle(this.in_reading_mode), $(".doc_banner").toggleClass("collapsed", this.in_reading_mode), $(document.body).toggleClass("reading_mode", this.in_reading_mode)
        }, e.prototype.setup_overlay = function () {
            var e = this;
            return this.container.on("scribd:overlay_panel_show", function () {
                return e.toggle_reading_mode(!1), e.container.addClass("overlay_open")
            }), this.container.on("scribd:overlay_panel_hide", function () {
                return e.container.removeClass("overlay_open")
            })
        }, e.prototype.tracking_name = function () {
            return"" + (this.page_category || "read2") + ":" + (this.page_name || "unknown")
        }, e.prototype.send_tracking = function () {
            this.opts.pmp_data && typeof $rat == "function" && $rat("pmp_read", this.opts.pmp_data), Scribd.track_event(this.page_category || "read2", "hit", this.page_name || "hit", Scribd.named_current_user_sync() && 1 || 0);
            if (this.opts.pingback_url)return this._send_pingback(this.opts.pingback_url.join(""))
        }, e.prototype._send_pingback = function (e) {
            var t, n;
            if (typeof navigator != "undefined" && navigator !== null ? (n = navigator.userAgent) != null ? n.match(/Google (Web|Instant) Preview/) : void 0 : void 0)return;
            return t = {}, document.referrer && (t.referer = document.referrer), $.post(e, t)
        }, e.prototype.fetch_data = function () {
            var e, t = this;
            return e = $(document.body), $.get(this.opts.data_url).done(function (t) {
                e.toggleClass("buy_disabled", !!t.disabled_store_sale && Scribd.current_doc.is_paid), e.toggleClass("pmp_disabled", !!t.disabled_pmp_sale && Scribd.current_doc.is_pmp), (Scribd.current_doc.is_pmp && !t.disabled_pmp_sale || Scribd.current_doc.is_paid && !t.disabled_store_sale) && e.toggleClass("action_button_shown", !0), Scribd.current_doc.is_pmp && !t.disabled_pmp_sale && Scribd.current_doc.is_paid && !t.disabled_store_sale && e.toggleClass("buy_hidden", !0), t.disabled_store_sale && e.trigger("scribd:buy_disabled", [t]);
                if (t.disabled_pmp_sale)return e.trigger("scribd:pmp_disabled", [t])
            })
        }, e.prototype.setup_print_handler = function () {
            var e, t, n;
            if (!Scribd.current_doc.downloadable_for_premium_users)return;
            return e = function (e) {
                return Scribd.track_event("newdoc", "print", "handle:" + e), Scribd.Lightbox.remote_open("print_dialog", "/read/print_dialog", Scribd.toolbar_params())
            }, typeof window.matchMedia == "function" && window.matchMedia("print").addListener(_.debounce(function () {
                return e("match_media")
            }, 3e3, !0)), n = window.onbeforeprint, window.onbeforeprint = function () {
                return e("on_before_print"), typeof n == "function" && n(), !1
            }, t = navigator.platform.match(/Mac/), $(document).keydown(function (n) {
                if (n.keyCode === "P".charCodeAt(0) && (t && n.metaKey || !t && n.ctrlKey))return e("keydown"), !1
            })
        }, e
    }(), Scribd.ReadPage = function (e) {
        function r(e, t) {
            var n, r = this;
            this.opts = t, this.container = $(e), this.doc_container = this.container.find("#doc_container"), this.fetch_data(), (n = Scribd.Ads) != null && n.load(), this.setup_footer(), this.setup_reading_mode(), this.setup_responsive(), this.setup_width(), this.setup_sticky_download(), this.setup_annotations(), this.setup_reading_progress(), this.setup_events(), this.setup_buy(), this.setup_print_handler(), this.disable_check_body_width(), typeof docManager != "undefined" && docManager !== null && docManager.addEvent("expectedFirstPageChanged", function (e) {
                return r.check_between_page_ad(e)
            }), this.check_between_page_ad(1), this.send_tracking(), this.opts.enter_fullscreen && _.defer(function () {
                var e;
                return(e = Scribd.ReadPage.toolbar) != null ? e.actions.enter_read_mode_btn() : void 0
            }), window.location.hash === "#download" && _.defer(function () {
                var e;
                return(e = Scribd.ReadPage.toolbar) != null ? e.actions.download_btn() : void 0
            })
        }

        return t(r, e), r.early_initialize = function () {
            var e;
            e = $(window).width() - 340, $("<style>#doc_container { width: " + e + "px; }</style>").appendTo($("head"));
            if (DocumentManager)return DocumentManager.ScrollViewManager.prototype._checkBodyWidth = function () {
            }
        }, r.prototype.in_reading_mode = !1, r.prototype.exit_reading_mode_threshold = 100, r.prototype.page_name = "pdf", r.prototype.check_between_page_ad = function (e) {
            var t;
            this._checked_ad || (this._checked_ad = {});
            if (this._checked_ad[e])return;
            this._checked_ad[e] = !0, t = $("#between_page_ads_" + e);
            if (t.length && t.height() > 0)return t.addClass("is_filled")
        }, r.prototype.disable_check_body_width = function () {
        }, r.prototype.setup_reading_progress = function () {
            var e, t, n, r = this;
            new Scribd.ReadingProgress, e = ((t = window.location.hash.match(/\bpage=(\d+)/)) != null ? t[1] : void 0) || ((n = Scribd.current_doc.reading_progress) != null ? n[0] : void 0);
            if (e && +e > 1)return docManager.view_manager_deferred.then(function () {
                var t;
                return docManager.gotoPage(+e), (t = Scribd.ReadPage.toolbar) != null ? t.move_pushdown(0) : void 0
            })
        }, r.prototype.setup_annotations = function () {
            var e, r = this;
            return e = $('<div class="annotation_selector"></div>').prependTo(this.container.find(".outer_page_container")), Scribd.UI.Annotations.selector = new Scribd.UI.Annotations.Selector(e), Scribd.Backbone.annotations_collection_view = new Scribd.Backbone.AnnotationCollectionView({el: e, collection: Scribd.Backbone.document.get("annotations"), view_cls: function (e) {
                function r() {
                    return this.goto_comments = n(this.goto_comments, this), r.__super__.constructor.apply(this, arguments)
                }

                return t(r, e), r.prototype.goto_comments = function () {
                    return $(document.body).trigger("scribd:show_notes")
                }, r
            }(Scribd.Backbone.AnnotationView)}), Scribd.Backbone.document.get("annotations").fetch({success: function () {
                return Scribd.Backbone.document.get("comments").fetch()
            }})
        }, r.prototype.setup_sticky_download = function () {
            var e, t, n, r, i, s = this;
            if (!this.container.find(".toolbar_download_panel").length)return;
            return r = $("#doc_sidebar"), i = $(window), t = r.outerHeight() + r.offset().top, e = !1, n = function () {
                if (i.scrollTop() > t) {
                    if (e)return;
                    return e = !0, Scribd.ReadPage.toolbar.toggle_panel("download", !0)
                }
                if (!e)return;
                return e = !1, Scribd.ReadPage.toolbar.toggle_panel("download", !1)
            }, i.on("scroll", n), n()
        }, r.prototype.setup_reading_mode = function () {
            var e, t = this;
            return $(document.body).on("click", function (e) {
                var n;
                if (e.which !== 1)return;
                if (e.clientY <= t.exit_reading_mode_threshold)return;
                n = $(e.target);
                if (n.closest(".doc_toolbar, #global_header, a, .doc_sidebar, .doc_banner, input, textarea").length)return;
                return t.toggle_reading_mode()
            }), e = $(window), e.on("scroll", function (e) {
                if (t.in_reading_mode && !t.reading_mode_allowed())return t.toggle_reading_mode()
            }), e.on("mousemove", function (e) {
                if (t.in_reading_mode && e.clientY < t.exit_reading_mode_threshold)return t.toggle_reading_mode()
            }), this.reading_mode_bar = $("#reading_mode_bar"), this.container.on("scribd:toggle_panel", function (e, n, r) {
                if (n === "find" && r && t.in_reading_mode)return t.toggle_reading_mode(!1)
            }), typeof docManager != "undefined" && docManager !== null ? docManager.addEvent("expectedFirstPageChanged", function (e) {
                return t.reading_mode_bar.find(".page_num").text(e)
            }) : void 0
        }, r.prototype.reading_mode_allowed = function () {
            var e, t, n, r;
            return r = $(window), t = $("body"), e = $("#document_activity"), n = this.container.find(".outer_page_container, #chunk"), this.reading_mode_allowed = function () {
                var e, i, s, o;
                return t.is(".overlay_open") ? !1 : ((s = Scribd.ReadPage.toolbar) != null ? (o = s.current_panel) != null ? o.is(".toolbar_find_panel") : void 0 : void 0) ? !1 : (i = r.scrollTop(), e = n.offset().top, i > e && i + r.height() < e + n.height())
            }, this.reading_mode_allowed()
        }, r.prototype.toggle_sidebar = function (e) {
            return this.container.toggleClass("sidebar_hidden"), this.update_page_width()
        }, r.prototype.update_page_width = function () {
            var e, t, n, r, i, s, o, u, a;
            return a = $(window), s = 300, i = 81, u = 1, t = 41, o = 0, n = 40, r = 728, e = $(document.body), this.update_page_width = function () {
                var f, l, c, h, p, d;
                return h = a.width(), p = h - (s + n), p = Math.max(r, p), this.container.is(".w1111") ? (l = u, f = o) : (l = i, f = t), c = this.container.is(".sidebar_hidden") ? (f = o, h) : p, typeof docManager != "undefined" && docManager !== null && (d = docManager._currentViewManager) != null && d._setWidth(c - f - l), this.doc_container.width(c - f).trigger("scribd:reshape", [p, c]), e.trigger("scribd:render_annotations")
            }, this.update_page_width()
        }, r.prototype.setup_width = function () {
            var e, t = this;
            return e = _.debounce(function () {
                return t.update_page_width()
            }, 20), $(window).on("resize", e), this.update_page_width()
        }, r.prototype.setup_footer = function () {
            return this.footer_documents = this.container.find(".footer_documents")
        }, r.prototype.setup_responsive = function () {
            var e, t, n, r, i, s = this;
            return r = [1111], e = [function () {
                var e, t, i;
                i = [];
                for (e = 0, t = r.length; e < t; e++)n = r[e], i.push("w" + n);
                return i
            }()].join(" "), i = $(window), t = function () {
                var t, n, o, u;
                s.container.removeClass(e), u = [];
                for (n = 0, o = r.length; n < o; n++) {
                    t = r[n];
                    if (i.width() < t) {
                        s.container.addClass("w" + t);
                        break
                    }
                    u.push(void 0)
                }
                return u
            }, $(window).on("resize", t), t()
        }, r
    }(Scribd.BaseReadPage)
}).call(this);


/* app/views/read2/epub/_standard_view.coffee @ 1384554002 */
(function () {
    var e = function (e, t) {
        return function () {
            return e.apply(t, arguments)
        }
    }, t = {}.hasOwnProperty, n = function (e, n) {
        function i() {
            this.constructor = e
        }

        for (var r in n)t.call(n, r) && (e[r] = n[r]);
        return i.prototype = n.prototype, e.prototype = new i, e.__super__ = n.prototype, e
    }, r = [].indexOf || function (e) {
        for (var t = 0, n = this.length; t < n; t++)if (t in this && this[t] === e)return t;
        return-1
    };
    Scribd.EpubReadPage = function (t) {
        function i(t, n) {
            var r, i = this;
            this.opts = n, this.undraw_pages = e(this.undraw_pages, this), this.draw_pages = e(this.draw_pages, this), window.check_fontface_fonts && check_fontface_fonts(), this.container = $(t), this.doc_container = this.container.find("#doc_container"), this.fetch_data(), (r = Scribd.Ads) != null && r.load(), this.setup_footer(), this.setup_reading_mode(), this.setup_reflow(), this.setup_toc(), this.setup_width(), this.setup_buy(), this.setup_events(), this.setup_responsive(), this.setup_reading_progress(), this.setup_overlay(), this.setup_print_handler(), $(window).on("resize", _.debounce(function (e) {
                if (!i.container.is(":visible"))return;
                return i.redraw()
            }, 100)), $(window).on("scroll", _.debounce(function (e) {
                if (!i.container.is(":visible"))return;
                return i.scroll_page()
            }, 1)), this.global_header = $("#global_header"), _.defer(function () {
                var e;
                return(e = i.global_header.data("instance")) != null ? e.make_sticky(!1) : void 0
            }), this.opts.epub_view === "standard" && this.send_tracking()
        }

        return n(i, t), i.prototype.defaults = {font_size: .6}, i.prototype.page_name = "epub", i.prototype.settings_key = "scribd:epub_reader_settings", i.prototype.header_height = function () {
            var e, t;
            return t = $(".doc_toolbar:visible"), e = this.container.find(".reading_mode_bar:visible"), t.length ? parseFloat(t.css("top")) + t.height() : e.length ? parseFloat(e.css("top")) + e.height() : 0
        }, i.prototype.scroll_page = function () {
            var e, t, n, r, i, s, o;
            r = $(window).scrollTop() - $("#chunk").offset().top + this.header_height(), o = this.page_positions;
            for (e = i = 0, s = o.length; i < s; e = ++i) {
                n = o[e];
                if (n > r) {
                    t = e;
                    break
                }
            }
            return t == null && (t = this.pages.length - 1), this.at_page(t), !1
        }, i.prototype.setup_reflow = function () {
            var e = this;
            return this.reflow = new VerticalReflow, this.reflow.column_pad_top = 0, this.reflow.column_pad_bottom = 0, this.reflow.column_pad_left = 110, this.reflow.column_pad_right = 110, this.reflow.should_track_read = this.opts.epub_view === "standard", this.reflow.register_needs_redraw_callback(function () {
                return e.redraw()
            }), this.reflow.register_page_change_callback(function (t) {
                return e.goto_pos(t)
            }), this.setup_pages(), this.setup_initial_pages(), this.supported_styles = ["font_size", "font_style"], this.load_default_style(), $(document.body).on("scribd:epub_appearance", function (t, n, r) {
                return e.update_appearance(n, r)
            })
        }, i.prototype.setup_pages = function () {
            var e, t, n;
            return this.page_cluster_size = 10, n = this.doc_container.width(), this.container.find(".reader_columns").css({width: n}), t = 500, e = 1, this.reflow.set_page_size(n, t, e), this.pages = this.reflow.fillPages(), this.reflow.pages = this.pages
        }, i.prototype.setup_initial_pages = function () {
            var e, t, n, r, i, s, o;
            e = this.container.find(".reader_columns").empty(), i = 0, this.page_positions = [];
            for (r = s = 0, o = this.pages.length; 0 <= o ? s < o : s > o; r = 0 <= o ? ++s : --s)n = this.reflow.page_heights[r] * this.reflow.scale, i += n, this.page_positions[r] = i, t = $('<div class="reader_column"></div>'), $(t).css({height: n}), e.append(t);
            return e.css({height: i}), this._columns = e.find(".reader_column"), this.reflow.set_columns(this._columns.show()), this.scroll_page()
        }, i.prototype.track_read = function () {
            var e, t = this;
            if (!this.reflow.should_track_read)return;
            return this.reflow.read_timeout != null && window.clearTimeout(this.reflow.read_timeout), e = function () {
                return t.reflow.track_read()
            }, this.reflow.read_timeout = window.setTimeout(e, this.reflow.track_reads_after_x_ms)
        }, i.prototype.draw_pages = function () {
            var e, t, n, r, i;
            t = this.container.find(".reader_columns .reader_column").slice(this.start_page, this.end_page), n = this.start_page;
            for (r = 0, i = t.length; r < i; r++)e = t[r], $(e).is(".drawn") || ($(e).addClass("drawn"), this.reflow.column_height = this.reflow.page_heights[n], this.reflow.draw_one_column(e, n, this.pages[n].clone())), n += 1;
            return this.will_draw = !1
        }, i.prototype.undraw_pages = function () {
            var e, t, n, r, i, s, o;
            r = this.container.find(".reader_columns .reader_column"), t = $(r.slice(0, this.start_page)).filter(".drawn"), e = $(r.slice(this.end_page, this.pages.length)).filter(".drawn"), i = $.merge(t, e);
            for (s = 0, o = i.length; s < o; s++)n = i[s], $(n).removeClass("drawn"), $(n).empty();
            return this.will_undraw = !1
        }, i.prototype.at_page = function (e) {
            var t;
            return this.current_page = Math.min(e, this.pages.length - 1), t = this.current_page / (this.pages.length - 1), $(document.body).trigger("scribd:epub_progress", [t]), this.reflow.page = this.reflow.pages[this.current_page].clone(), this.current_page + 1 < this.pages.length ? this.reflow.next_page = this.reflow.pages[this.current_page + 1].clone() : this.reflow.next_page = null, this.start_page = Math.max(this.current_page - this.page_cluster_size / 2, 0), this.end_page = Math.min(this.current_page + this.page_cluster_size / 2, this.pages.length), this.will_draw || (this.will_draw = !0, this.draw_pages()), this.will_undraw || (this.will_undraw = !0, setTimeout(this.undraw_pages, 1e3)), this.track_read()
        }, i.prototype.goto_page = function (e, t) {
            var n;
            return t == null && (t = 0), n = this.container.find(".reader_columns .reader_column")[e], Scribd.ReadPage.toolbar.lock(), e > 0 ? window.scrollTo(0, $(n).offset().top - this.header_height() + t) : $(window).scrollTop(0), Scribd.ReadPage.toolbar.unlock()
        }, i.prototype.goto_pos = function (e) {
            var t, n, r;
            return t = this.globalize_block(e.chapter_idx, e.block_idx), n = this.block_to_page(t), r = this.location_from_position_in_page(e, n), this.goto_page(n, r)
        }, i.prototype.location_from_position_in_page = function (e, t) {
            var n;
            return this.reflow.render = !1, this.reflow.column_height = this.reflow.page_heights[t], n = this.reflow.draw_one_column(null, 0, this.pages[t].clone(), e), this.reflow.render = !0, n.y * this.reflow.scale
        }, i.prototype.position_from_location_in_page = function (e, t) {
            var n, r, i;
            return r = this.pages[t].clone(), e === 0 ? r : (this.reflow.render = !1, t + 1 < this.pages.length ? n = this.pages[t + 1].clone() : n = null, this.reflow.column_height = e / this.reflow.scale, i = this.reflow.draw_one_column(null, 0, r, n), this.reflow.render = !0, r)
        }, i.prototype.current_position = function () {
            var e, t, n, r;
            return this.current_page > 0 ? r = this.page_positions[this.current_page - 1] : r = 0, n = $(window).scrollTop() - $("#chunk").offset().top + this.header_height(), e = Math.max(n - r, 0), t = this.position_from_location_in_page(e, this.current_page), t
        }, i.prototype.redraw = function () {
            var e;
            return e = this.current_position(), this.setup_pages(), this.setup_initial_pages(), this.goto_pos(e)
        }, i.prototype.setup_reading_progress = function () {
            return Scribd.Epub.prototype.setup_reading_progress.call(this)
        }, i.prototype.setup_toc = function () {
            var e;
            return e = $("#doc_toolbar").find(".toolbar_find_panel .section_list"), Scribd.Epub.prototype.setup_toc.call(this, e)
        }, i.prototype.setup_toc_events = function () {
            return Scribd.Epub.prototype.setup_toc_events.call(this)
        }, i.prototype.update_appearance = function (e, t, n) {
            n == null && (n = !0);
            if (r.call(this.supported_styles, e) < 0)return;
            Scribd.Epub.prototype.update_appearance.call(this, e, t, n);
            if (!this.container.is(":visible"))return;
            return this.redraw()
        }, i.prototype.update_default_settings = function (e, t) {
            if (r.call(this.supported_styles, e) < 0)return;
            return Scribd.Epub.prototype.update_default_settings.call(this, e, t)
        }, i.prototype.load_default_style = function () {
            return Scribd.Epub.prototype.load_default_style.call(this, this.supported_styles)
        }, i.prototype.total_blocks = function () {
            return Scribd.Epub.prototype.total_blocks.call(this)
        }, i.prototype.globalize_block = function (e, t) {
            return Scribd.Epub.prototype.globalize_block.call(this, e, t)
        }, i.prototype.block_to_page = function (e) {
            return Scribd.Epub.prototype.block_to_page.call(this, e)
        }, i
    }(Scribd.ReadPage)
}).call(this);


/* app/views/read2/epub/_book_view.coffee @ 1384554002 */
(function () {
    var e, t = function (e, t) {
        return function () {
            return e.apply(t, arguments)
        }
    }, n = {}.hasOwnProperty, r = function (e, t) {
        function i() {
            this.constructor = e
        }

        for (var r in t)n.call(t, r) && (e[r] = t[r]);
        return i.prototype = t.prototype, e.prototype = new i, e.__super__ = t.prototype, e
    }, i = [].indexOf || function (e) {
        for (var t = 0, n = this.length; t < n; t++)if (t in this && this[t] === e)return t;
        return-1
    };
    e = function () {
        function e(e) {
            var t = this;
            this.container = $(e), this.container.addClass("transition"), this.container.on("mousedown", function (e) {
                return t.start_drag(e), $(document.body).one("mouseup", function (e) {
                    return t.end_drag(e)
                })
            })
        }

        return e.prototype.start_drag = function (e) {
            var t, n, r, i = this;
            return t = this.container.offset().left, n = t + this.container.width(), r = function (e) {
                var r;
                return r = (e.pageX - t) / (n - t), r < 0 && (r = 0), r > 1 && (r = 1), i.container.trigger("scribd:scrub", [r])
            }, this.is_dragging = !0, $(document.body).addClass("unselectable").on("mousemove.scrubber", r), r(e), this.container.removeClass("transition")
        }, e.prototype.end_drag = function () {
            return this.is_dragging = !1, $(document.body).removeClass("unselectable").off("mousemove.scrubber"), this.container.trigger("scribd:scrub", [this.t]), this.container.addClass("transition")
        }, e.prototype.set_value = function (e) {
            return this.t = e, this._filled || (this._filled = this.container.find(".scrubber_filled")), this._filled.css({width: "" + this.t * 100 + "%"})
        }, e
    }(), Scribd.EpubReader = function (n) {
        function s(n, r) {
            var i, s = this;
            this.opts = r, this.setup_reading_mode = t(this.setup_reading_mode, this), window.check_fontface_fonts && check_fontface_fonts(), this.container = $(n), this.opts.is_embed && (this.page_name = "epub", this.page_category = "embed"), this.fetch_data(), this.container.dispatch("click", this.tracking_name(), {next_btn: function () {
                return s.toggle_reading_mode(!0), s.move_page(s.reflow.num_columns)
            }, prev_btn: function () {
                return s.toggle_reading_mode(!0), s.move_page(-s.reflow.num_columns)
            }}), this.scrubber = new e(this.container.find(".scrubber")), this.container.on("scribd:scrub", function (e, t) {
                var n;
                return n = Math.round((s.reflow.pages.length - 1) * t), n === s.current_page ? !1 : (s.goto_page(n), !1)
            }), i = this.opts.is_embed, $(".banner_wrapper").length > 0 ? (this.container.find(".reader_column").css({bottom: "+=145px"}), $(".page_left, .page_right").css({bottom: "+=145px"})) : $(".doc_banner").hide(), this.setup_reflow(), i || this.setup_toc(), this.setup_hotkeys(), this.setup_scrolling(), i || this.setup_reading_progress(), this.setup_buy(), i || this.setup_reading_mode(), i || this.setup_overlay(), this.setup_events(), this.setup_print_handler(), $(document.body).addClass("fullscreen_view").on("scribd:epub_appearance", function (e, t, n) {
                return s.update_appearance(t, n)
            }), $(window).on("resize", _.debounce(function (e) {
                if (!s.container.is(":visible"))return;
                return s.adjust_size(), s.setup_pages()
            }, 300)), this.global_header = $("#global_header"), _.defer(function () {
                var e;
                return(e = s.global_header.data("instance")) != null ? e.make_sticky(!1) : void 0
            }), this.opts.epub_view === "full" && this.send_tracking()
        }

        return r(s, n), s.prototype.scroll_speed = -10, s.prototype.in_reading_mode = !1, s.prototype.defaults = {font_size: .6}, s.prototype.exit_reading_mode_threshold = 60, s.prototype.page_name = "epub_fullscreen", s.prototype.settings_key = "scribd:epub_reader_settings", s.prototype.reading_mode_allowed = function () {
            return!$(document.body).is(".overlay_open")
        }, s.prototype.setup_reading_mode = function () {
            var e = this;
            return $(document.body).on("click", function (t) {
                var n;
                if (!e.container.is(":visible"))return;
                if (t.which !== 1)return;
                if (t.clientY <= e.exit_reading_mode_threshold)return;
                n = $(t.target);
                if (n.closest(".doc_toolbar, #global_header, a, .doc_sidebar, input, textarea").length)return;
                return e.toggle_reading_mode()
            }), $(window).on("mousemove", function (t) {
                if (!e.container.is(":visible"))return;
                if (e.in_reading_mode && t.clientY < e.exit_reading_mode_threshold)return e.toggle_reading_mode()
            })
        }, s.prototype.setup_reflow = function () {
            var e, t = this;
            return this.reflow = new PagedReflow, this.reflow.column_pad_top = 40, this.reflow.column_pad_bottom = 40, this.reflow.column_pad_left = 110, this.reflow.column_pad_right = 110, this.reflow.should_track_read = this.opts.epub_view === "full", this.supported_styles = ["font_size", "font_style", "color"], this.load_default_style(), e = this.container.find(".percentage .p_value"), this.reflow.register_redraw_callback(function () {
                var n;
                return t.current_page = t.reflow.page_number, n = t.reflow.next_page.done && !t.scrubber.is_dragging ? 1 : t.reflow.pages ? t.current_page / (t.reflow.pages.length - 1) : 0, t.scrubber.set_value(n), t.container.toggleClass("first_page", n === 0), t.container.toggleClass("last_page", n === 1), e.text(Math.round(n * 100)), $(document.body).trigger("scribd:epub_progress", [n])
            }), this.adjust_size(), this.setup_pages(), this.goto_page(0)
        }, s.prototype.setup_pages = function () {
            return this.reflow.ready_pages(), this.reflow.gotoPage(this.reflow.page), this.current_page = this.reflow.page_number
        }, s.prototype.setup_hotkeys = function () {
            var e = this;
            return $(window.document).keydown(function (t) {
                if (!e.container.is(":visible"))return;
                if ($(t.target).closest("input, textarea").length)return;
                if (t.ctrlKey || t.metaKey)return;
                switch (t.keyCode) {
                    case 37:
                    case 75:
                    case 8:
                        return e.move_page(-e.reflow.num_columns), e.toggle_reading_mode(!0), !1;
                    case 39:
                    case 74:
                    case 32:
                        return e.move_page(e.reflow.num_columns), e.toggle_reading_mode(!0), !1
                }
            })
        }, s.prototype.setup_scrolling = function () {
            var e, t, n, r, i, s = this;
            return r = 5, e = !1, t = _.debounce(function () {
                return e = !1
            }, 100), n = null, i = _.throttle(function () {
                if (n !== null && s.current_page !== n)return s.move_page(n - s.current_page)
            }, 50), this.container.on("mousewheel", _.throttle(function (o, u, a, f) {
                var l;
                if ($(o.target).closest(".toolbar_popup, .scrolling_content").length)return;
                if (f === 0)return;
                return f = -f, l = Math.abs(f), l > r && (l = r + Math.pow(l - r, .4)), f *= l / 10 / Math.abs(f), f < 0 ? f = Math.floor(f) : f = Math.ceil(f), e === !1 ? n = s.current_page + f : n += f, n = Math.max(0, Math.min(n, s.reflow.pages.length - 1)), e = !0, t(), i(), !1
            }, 60))
        }, s.prototype.current_position = function () {
            return this.reflow.pages[this.current_page].clone()
        }, s.prototype.adjust_size = function () {
            var e;
            return this._columns || (this._columns = this.container.find(".reader_columns .reader_column")), e = $(window).width() < 1280 ? 1 : 2, this.container.toggleClass("double_column", e === 2), this._columns.slice(e).hide(), this.reflow.set_columns(this._columns.slice(0, e).show()), this.reflow.redraw()
        }, s.prototype.goto_page = function (e) {
            return this.current_page = e, this.reflow.gotoPage(this.reflow.pages[this.current_page].clone())
        }, s.prototype.move_page = function (e) {
            var t;
            if (this.reflow.pages)return t = Math.max(0, Math.min(this.reflow.pages.length - 1, this.reflow.page_number + e)), this.goto_page(t), this.reflow.focus()
        }, s.prototype.toggle_reading_mode = function () {
            if (this.opts.is_embed)return;
            return s.__super__.toggle_reading_mode.apply(this, arguments)
        }, s.prototype.setup_reading_progress = function () {
            return Scribd.Epub.prototype.setup_reading_progress.call(this)
        }, s.prototype.setup_toc = function () {
            var e;
            return e = $("#doc_toolbar").find(".toc_popup .section_list"), Scribd.Epub.prototype.setup_toc.call(this, e)
        }, s.prototype.setup_toc_events = function () {
            return Scribd.Epub.prototype.setup_toc_events.call(this)
        }, s.prototype.update_appearance = function (e, t, n) {
            n == null && (n = !0);
            if (i.call(this.supported_styles, e) < 0)return;
            Scribd.Epub.prototype.update_appearance.call(this, e, t, n);
            if (!this.container.is(":visible"))return;
            return this.setup_pages()
        }, s.prototype.update_default_settings = function (e, t) {
            if (i.call(this.supported_styles, e) < 0)return;
            return Scribd.Epub.prototype.update_default_settings.call(this, e, t)
        }, s.prototype.load_default_style = function () {
            if (this.opts.is_embed)return;
            return Scribd.Epub.prototype.load_default_style.call(this, this.supported_styles)
        }, s.prototype.total_blocks = function () {
            return Scribd.Epub.prototype.total_blocks.call(this)
        }, s.prototype.globalize_block = function (e, t) {
            return Scribd.Epub.prototype.globalize_block.call(this, e, t)
        }, s.prototype.block_to_page = function (e) {
            return Scribd.Epub.prototype.block_to_page.call(this, e)
        }, s
    }(Scribd.BaseReadPage)
}).call(this);


/* app/views/read2/_toolbar.coffee @ 1384554002 */
(function () {
    var e, t = function (e, t) {
        return function () {
            return e.apply(t, arguments)
        }
    }, n = {}.hasOwnProperty, r = function (e, t) {
        function i() {
            this.constructor = e
        }

        for (var r in t)n.call(t, r) && (e[r] = t[r]);
        return i.prototype = t.prototype, e.prototype = new i, e.__super__ = t.prototype, e
    };
    e = Scribd.make_tracker("read2:toolbar"), Scribd.Toolbar = function () {
        function n(e) {
            this.setup_mode_toggle = t(this.setup_mode_toggle, this), this.container = $(e), this.height = this.container.height(), this.extra_height = this.container.is(".has_notification") ? this.container.find(".toolbar_notification").outerHeight() : 0, this.container.parent().height(this.container.height() + this.extra_height), this.max_push += this.extra_height, this.pushdown = this.max_push, this.setup_events(), this.setup_popups(), this.setup_sidebar_buttons(), this.setup_tooltips(), this.setup_hotkeys(), this.setup_mode_toggle(), this.setup_panels(), this.setup_docmanager(), this.setup_misc()
        }

        return n.prototype.max_push = 56, n.prototype.pushdown = 56, n.prototype.actions_cls = function () {
            return Scribd.ToolbarActions
        }, n.prototype.notification_version = 1, n.prototype.setup_mode_toggle = function () {
            var t, n = this;
            return t = this.container.find(".read_mode_toggle .toggle_btn"), this.container.on("click", ".read_mode_toggle .toggle_btn", function (r) {
                var i;
                return i = $(r.currentTarget), i.is(".active") ? !1 : (e("click", "toggle_read_mode"), n.goto_read_mode(i.data("name")) && (t.removeClass("active"), i.addClass("active")), !1)
            })
        }, n.prototype.goto_read_mode = function (e) {
            switch (e) {
                case"full":
                    if (Scribd.read_page.container.is(".sidebar_hidden"))return;
                    Scribd.read_page.toggle_sidebar(), docManager.resetZoom(), this.update_zoomers({max_zoom: !0, mode_picker: !0});
                    break;
                case"standard":
                    if (!Scribd.read_page.container.is(".sidebar_hidden"))return;
                    this.update_zoomers({max_zoom: !1, mode_picker: !0}), Scribd.read_page.toggle_sidebar()
            }
            return!0
        }, n.prototype.set_mode_value = function (e) {
            return this.container.find(".read_mode_toggle .toggle_btn").removeClass("active").filter("[data-name='" + e + "']").addClass("active")
        }, n.prototype.setup_misc = function () {
            var e = this;
            return $(document.body).on("scribd:buy_disabled", function (t, n) {
                var r;
                return r = e.container.find(".primary_action_btn").filter(".premium_signup_btn, .buy_doc_btn").remove()
            }), $(document.body).trigger("scribd:fix_icons", [this.container]), $(document.body).on("scribd:increment_activity_count", function (t, n) {
                var r;
                n == null && (n = 1), e.activity_flag || (e.activity_flag = e.container.find(".activity_flag"));
                if ((r = e.activity_flag.html()) != null ? r.match(/^\d+$/) : void 0)return e.activity_flag.removeClass("has_none").html(parseInt(e.activity_flag.html(), 10) + n)
            })
        }, n.prototype.setup_docmanager = function () {
            var e;
            return e = docManager.gotoPage, docManager.gotoPage = function (t, n) {
                return e.call(docManager, t, $.extend({offset: 110}, n))
            }
        }, n.prototype.setup_hotkeys = function (e) {
            var t, n = this;
            return e == null && (e = !0), t = navigator.platform.match(/Mac/), t && this.container.addClass("osx"), $(window.document).keydown(function (r) {
                var i;
                if (e && r.keyCode === 70 && (t && r.metaKey || !t && r.ctrlKey))return i = n.toggle_panel("find"), i && i.find(".find_input").focus(), !1;
                if ($(r.target).closest("input, textarea, select").length)return;
                if (r.keyCode === 73 && !r.ctrlKey && !r.metaKey)return n.actions.info_btn(n.container.find(".info_btn")), !1
            })
        }, n.prototype.setup_panels = function () {
            var e = this;
            return this.container.on("scribd:update_panel", function (t) {
                var n, r;
                if (!e.current_panel)return;
                return r = e.current_panel.height(), n = e.current_panel.addClass("disable_transition").css({height: ""}).height(), e.current_panel.css({height: "" + r + "px"}), _.defer(function () {
                    return e.current_panel.removeClass("disable_transition"), $("#doc_sidebar").css({marginTop: "" + n + "px"}), e.current_panel.css({height: "" + n + "px"})
                })
            }), this.setup_find_panel()
        }, n.prototype.setup_find_panel = function () {
            return new Scribd.ToolbarFindPanel(this.container.find(".toolbar_find_panel"), this)
        }, n.prototype.setup_tooltips = function () {
            var e, t, n, r, i, s, o = this;
            if (this.container.is(".show_labels"))return;
            s = this.container.find("span.icon");
            for (r = 0, i = s.length; r < i; r++) {
                e = s[r], e = $(e), n = e.next("span").text();
                if (!n)continue;
                e.parent().attr("data-tooltip", n)
            }
            return t = this.container.find(".doc_toolbar_inner"), Scribd.hook_tooltips(this.container, {position: function (e, n) {
                var r, i, s, o;
                return i = e.position(), s = n.outerWidth(), o = e.outerWidth(), r = e.outerHeight(), n.css({left: i.left + Math.floor((o - s) / 2), top: i.top + r + 4 + t.position().top})
            }})
        }, n.prototype.setup_popups = function () {
            var e = this;
            return this._popups || (this._popups = this.container.find(".toolbar_popups")), this._popups.on("click", ".close_btn", function (t) {
                return e.close_popup()
            }), $(document.body).on("click", function (t) {
                if (!$(t.target).closest(".toolbar_popup").length)return e.close_popup()
            })
        }, n.prototype.setup_sidebar_buttons = function () {
            var e, t, n = this;
            e = $("#doc_container");
            if (!e.length)return;
            return t = function (t) {
                var r;
                return n.sidebar_offset = t || e.outerWidth() - 1, (r = n.current_panel) != null ? r.css({left: "" + n.sidebar_offset + "px"}) : void 0
            }, t(), e.on("scribd:reshape", function (n, r) {
                var i;
                return i = e.innerWidth() - e.width(), t(r + i)
            })
        }, n.prototype.close_popup = function (e) {
            var t;
            if (e && !e.is(this.current_popup))return;
            return(t = this.current_popup) != null && t.trigger("scribd:popup_close").data("btn").removeClass("popup_active"), this._popups.find(".toolbar_popup").removeClass("is_open"), this.current_popup = null
        }, n.prototype.show_popup = function (e, t) {
            var n, r, i, s;
            e = this.container.find(e);
            if (!e.length)throw"failed to find popup " + e.selector;
            if (n = this.current_popup) {
                this.close_popup();
                if (n.data("btn").is(t))return
            }
            return s = t.position(), i = e.width(), r = Math.floor(s.left + t.outerWidth() / 2), r + i > $(window).width() ? (e.addClass("align_right"), r -= i) : e.removeClass("align_right"), e.data("btn", t).css({left: "" + r + "px", top: "" + (s.top + t.height() - 8) + "px"}).addClass("is_open").appendTo(this._popup_drop), t.addClass("popup_active"), this.current_popup = e.trigger("scribd:popup_open", [t])
        }, n.prototype.toggle_panel = function (e, t) {
            var n, r, i, s, o, u = this;
            r = $("#doc_sidebar"), this._panels || (this._panels = this.container.find(".toolbar_panels").find(".toolbar_panel")), this._panel_buttons || (this._panel_buttons = this.container.find("[data-panel_name]")), s = this._panels.filter(".toolbar_" + e + "_panel"), n = this._panel_buttons.filter("[data-panel_name='" + e + "']"), i = s.is(".open");
            if (t === !0 && i)return;
            if (t === !1 && !i)return;
            return this.container.trigger("scribd:toggle_panel", [e, !i]), i ? (n.add(s).addClass("closing").removeClass("open"), this.current_panel = null, r.css({marginTop: ""}), s.css({height: "0"}), !1) : (this._panel_buttons.add(this._panels).removeClass("open"), n.add(s).addClass("open"), o = s.css({height: ""}).height(), s.css({left: "" + this.sidebar_offset + "px", top: "" + this.height + "px", right: "0", height: "0"}), _.defer(function () {
                return r.css({marginTop: "" + o + "px"}), s.css({height: "" + o + "px"})
            }), this.current_panel = s, s)
        }, n.prototype.setup_events = function () {
            var e, t, n, r, i, s = this;
            return t = this.actions_cls(), this.actions = new t(this.container, this), this.setup_pager(), this.global_header = $("#global_header"), _.defer(function () {
                return s.global_header.data("instance").make_sticky(!1)
            }), i = $(window), n = null, r = 0, this.lock = function () {
                return r++, n = null
            }, this.unlock = function () {
                return r--, n = null
            }, e = this.container.is(".active"), i.on("scroll", function (t) {
                var o, u;
                if (r)return;
                u = i.scrollTop(), u < 0 && (u = 0);
                if (n != null) {
                    o = u - n;
                    if (o < 0 && s.pushdown < s.max_push || o > 0 && s.pushdown > 0)s.pushdown -= o, s.pushdown > s.max_push && (s.pushdown = s.max_push), s.pushdown < 0 && (s.pushdown = 0), s.apply_pushdown()
                }
                return e && s.pushdown !== 0 && (s.container.removeClass("active"), e = !1), !e && s.pushdown === 0 && (s.container.addClass("active"), e = !0), n = u
            })
        }, n.prototype.setup_pager = function () {
            var e, t, n = this;
            this.container.find(".pager .total_value").text(this.get_highest_page()), e = this.container.find(".pager .current_value"), t = function (t) {
                var n;
                return t != null && e.val(t), n = e.val().length, e.width(18 + Math.max(0, n - 1) * 8)
            }, docManager.addEvent("expectedFirstPageChanged", function (e) {
                return n.current_page_num = e, t(e)
            }), e.on("keydown keyup", function (r) {
                switch (r.type === "keydown" && r.keyCode) {
                    case 38:
                        return n.move_page(-1), !1;
                    case 40:
                        return n.move_page(1), !1;
                    case 13:
                        navigator.userAgent.match(/msie/i) && n.goto_page(+e.val())
                }
                return t()
            }), e.on("change", function (t) {
                return n.goto_page(+e.val())
            }), e.on("click", function (e) {
                return e.target.select()
            });
            if (docManager.firstVisiblePage)return _.defer(function () {
                return n.current_page_num = docManager.firstVisiblePage.pageNum, t(n.current_page_num)
            })
        }, n.prototype.goto_page = function (e) {
            var t, n, r = this;
            return t = e > this.current_page_num ? 1 : -1, n = docManager.getClosestPageNumber(e, t), this.with_lock(function () {
                return docManager.gotoPage(n, {direction: t})
            })
        }, n.prototype.move_page = function (e) {
            return this.goto_page(e + this.current_page_num)
        }, n.prototype.get_highest_page = function () {
            var e, t, n;
            e = (n = Scribd.current_doc) != null ? n.page_count : void 0;
            if (!e) {
                e = 0;
                for (t in docManager.pages)e = Math.max(e, t)
            }
            return e
        }, n.prototype.update_notification = function () {
            return $(document.body).toggleClass("has_read_notification", this.container.is(".has_notification"))
        }, n.prototype.hide_notification = function (e) {
            var t, n, r = this;
            e == null && (e = !0);
            if (!this.container.is(".has_notification"))return;
            return this.container.removeClass("has_notification"), t = this.container.find(".toolbar_notification"), n = this.container.parent(), e ? this.with_lock(function () {
                return n.animate({height: n.height() - r.extra_height}, delay), $(r).animate({pushdown: r.pushdown - r.extra_height, max_push: r.max_push - r.extra_height}, {duration: delay, step: function () {
                    return r.apply_pushdown()
                }, complete: function () {
                    return t.remove()
                }})
            }) : (n.height(n.height() - this.extra_height), this.pushdown -= this.extra_height, this.max_push -= this.extra_height, this.extra_height = 0, this.apply_pushdown(), t.remove()), this.update_notification()
        }, n.prototype.apply_pushdown = function () {
            return this.container.css({top: "" + this.pushdown + "px"}), this.global_header.css({top: "" + (this.pushdown - this.max_push) + "px"})
        }, n.prototype.move_pushdown = function (e) {
            var t = this;
            return e == null && (e = 0), e = Math.max(0, Math.min(e, this.max_push)), $(this).animate({pushdown: e}, {duration: "fast", step: function () {
                return t.apply_pushdown()
            }, complete: function () {
                return t.pushdown = e, t.apply_pushdown()
            }})
        }, n.prototype.update_zoomers = function (e) {
            var t;
            this._zoom_in || (this._zoom_in = this.container.find(".zoom_in_btn")), this._zoom_out || (this._zoom_out = this.container.find(".zoom_out_btn")), e.min_zoom != null && (this._zoom_out.toggleClass("disabled", e.min_zoom), this.container.toggleClass("min_zoom", e.min_zoom)), e.max_zoom != null && (this._zoom_in.toggleClass("disabled", e.max_zoom), this.container.toggleClass("max_zoom", e.max_zoom));
            if (!e.mode_picker)return t = e.max_zoom ? "full" : "standard", this.set_mode_value(t)
        }, n.prototype.lock = function () {
        }, n.prototype.unlock = function () {
        }, n.prototype.with_lock = function (e) {
            return this.lock(), e(), this.unlock()
        }, n
    }(), Scribd.ToolbarActions = function (e) {
        function t(e, n) {
            var r = this;
            this._toolbar = n, t.__super__.constructor.call(this, e), $(document.body).on("scribd:overlay_panel_show", function () {
                return r.container.find(".info_btn").addClass("open")
            }), $(document.body).on("scribd:overlay_panel_hide", function () {
                return r.container.find(".info_btn").removeClass("open")
            })
        }

        return r(t, e), t.prototype.track_category = "read2:toolbar", t.prototype.download_page = "read", t.prototype.download_action = "toolbar_download", t.prototype.buy_doc_btn = function (e) {
            return Scribd.read_page.buy_manager.get_buy_callback("toolbar")()
        }, t.prototype.premium_signup_btn = function (e) {
            return Scribd.read_page.buy_manager.get_premium_callback("toolbar")()
        }, t.prototype.info_btn = function (e) {
            return Scribd.ReadPage.overlay_panel.toggle()
        }, t.prototype.find_object = function (e) {
            return{type: "document", id: Scribd.current_doc.id}
        }, t.prototype.zoom_in_btn = function (e) {
            var t;
            t = docManager._currentViewManager._currentZoomMultiplier, this._toolbar.update_zoomers({min_zoom: !1});
            if (t >= 1) {
                if (Scribd.read_page.container.is(".sidebar_hidden"))return;
                return Scribd.read_page.toggle_sidebar(), this._toolbar.update_zoomers({max_zoom: !0})
            }
            return this._toolbar.with_lock(function () {
                return docManager.zoom(1.25)
            })
        }, t.prototype.zoom_out_btn = function (e) {
            return this._toolbar.update_zoomers({max_zoom: !1}), Scribd.read_page.container.is(".sidebar_hidden") ? Scribd.read_page.toggle_sidebar() : this._toolbar.with_lock(function () {
                var e;
                docManager.zoom(.8), e = docManager._currentViewManager._currentZoomMultiplier;
                if (e < .2)return this._toolbar.update_zoomers({min_zoom: !0})
            })
        }, t.prototype.add_note_btn = function (e) {
            var t = this;
            return Scribd.with_login("add_note", function () {
                return t._toolbar.show_popup(".add_note_popup", e)
            })
        }, t.prototype.find_btn = function (e) {
            var t;
            return(t = Scribd.ReadPage.overlay_panel) != null && t.toggle(!1), this._toolbar.toggle_panel("find")
        }, t.prototype.embed_btn = function (e) {
            return Scribd.Lightbox.remote_open("embed_dialog", "/read/embed_dialog", Scribd.toolbar_params())
        }, t.prototype.share_btn = function (e) {
            return this._toolbar.show_popup(".share_popup", e)
        }, t.prototype.font_settings_btn = function (e) {
            return this._toolbar.show_popup(".font_popup", e)
        }, t.prototype.save_bookmark_btn = function (e) {
            var n = this;
            return Scribd.with_login("save_for_later", function () {
                var r, i;
                return i = n._toolbar.container.find(".saved_popup"), r = e.data("labels"), e.hasClass("saved") ? (n._toolbar.close_popup(i), e.data("tooltip", r.not_saved)) : (n._toolbar.show_popup(i, e), e.data("tooltip", r.saved)), i.removeClass("success"), t.__super__.save_bookmark_btn.call(n, e, function (e) {
                    return i.addClass("success")
                })
            })
        }, t.prototype.readcast_btn = function (e) {
            var t = this;
            return Scribd.with_login("like_document", function () {
                return t._toolbar.show_popup(t._toolbar.container.find(".readcaster_popup"), e)
            })
        }, t.prototype.sync_mobile_btn = function (e) {
            return this._toolbar.show_popup(".sync_mobile_popup", e)
        }, t
    }(Scribd.DocumentActions)
}).call(this);


/* app/views/read2/epub/_toolbar.coffee @ 1384554002 */
(function () {
    var e = {}.hasOwnProperty, t = function (t, n) {
        function i() {
            this.constructor = t
        }

        for (var r in n)e.call(n, r) && (t[r] = n[r]);
        return i.prototype = n.prototype, t.prototype = new i, t.__super__ = n.prototype, t
    };
    Scribd.EpubToolbar = function (e) {
        function n(e) {
            var t = this;
            n.__super__.constructor.call(this, e), this.actions.find_btn = function (e) {
                var n;
                return t.epub_mode === "full" ? t.show_popup(".toc_popup", e) : ((n = Scribd.ReadPage.overlay_panel) != null && n.toggle(!1), t.toggle_panel("find"))
            }
        }

        return t(n, e), n.prototype.setup_pager = function () {
        }, n.prototype.setup_find_panel = function () {
        }, n.prototype.setup_docmanager = function () {
        }, n.prototype.setup_misc = function () {
            return n.__super__.setup_misc.apply(this, arguments)
        }, n.prototype.goto_read_mode = function (e, t) {
            var n;
            t == null && (t = !0), this.epub_mode = e;
            switch (e) {
                case"standard":
                    $(".epub_page").removeClass("sidebar_hidden"), $(".doc_container").css('width','884px'), $(".global_footer").show();
                    break;
                case"full":
                    $(".epub_page").addClass("sidebar_hidden"), $(".doc_container").css('width','100%'), $(".global_footer").hide(), this.hide_notification(0), $(window).scrollTop(0)
            }
            return this.container.find(".read_mode_toggle .toggle_btn").removeClass("active").filter("[data-name='" + e + "']").addClass("active"), !0
        }, n
    }(Scribd.Toolbar)
}).call(this);


/* app/views/read2/popups/_commenting_popup.coffee @ 1384554002 */
(function () {
    Scribd.CommentingPopup = function () {
        function e(e) {
            this.container = $(e), this.setup_comment_form()
        }

        return e.prototype.track_category = "comment_popup", e.prototype.increment_activity = !0, e.prototype.setup_comment_form = function () {
            var e = this;
            return this.container.on("submit", ".comment_form", function (e) {
                if ($.trim($(e.target).find(".comment_input").val()) === "")return!1
            }), this.container.on("ajax:success", ".comment_form", function (t, n) {
                var r, i;
                r = $(t.target), e.container.addClass("comment_posted"), r.find(".comment_input").val(""), e.increment_activity && $(document.body).trigger("scribd:increment_activity_count", [1, "comment"]);
                if (n.comment)return(i = Scribd.ReadPage.activity_list) != null ? i.append_new_comment(n.comment) : void 0
            }), this.container.dispatch("click", "read2:" + this.track_category, {add_another_btn: function () {
                return e.container.removeClass("comment_posted")
            }})
        }, e
    }()
}).call(this);


/* app/views/read2/_overlay_panel.coffee @ 1384554002 */
(function () {
    $.fn.detach_with_spacer = function () {
        var e, t, n, r;
        t = function (e) {
            var t;
            t = e.data("spacer");
            if (t != null ? !t.length : !void 0)t = $('<div class="detach_spacer"></div>'), e.data("spacer", t);
            return t.css({margin: e.css("margin"), width: e.outerWidth() + "px", height: e.outerHeight() + "px", display: e.css("display"), "float": e.css("float")}).insertAfter(e), e.detach()
        };
        for (n = 0, r = this.length; n < r; n++)e = this[n], t($(e));
        return this
    }, $.fn.reattach_from_spacer = function () {
        var e, t, n, r;
        t = function (e) {
            var t;
            t = e.data("spacer");
            if (t && t.length)return t.after(e).detach()
        };
        for (n = 0, r = this.length; n < r; n++)e = this[n], t($(e));
        return this
    }, Scribd.OverlayPanel = function () {
        function e(e) {
            var t = this;
            this.container = $(e), this.content = this.container.find(".scrolling_content"), this.shroud = this.container.prev(".overlay_panel_shroud"), $(document.body).on("click", ".overlay_panel_shroud, .toggle_overlay_btn", function (e) {
                return t.toggle(), !1
            }), this.setup_scrolling(), this.container.on("scribd:update_height", function (e) {
                return t.recalc_scrolling()
            }), $(window.document).keydown(function (e) {
                if (e.keyCode === 27 && t.container.is(".visible"))return t.toggle(!1), !1
            })
        }

        return e.prototype.max_width = 800, e.prototype.anim_time = 400, e.prototype.easing = "ease_in_out_cubic", e.prototype.setup_scrolling = function () {
            return this.scroller = this.container.find(".nano").nanoScroller({preventPageScrolling: !0, contentClass: "scrolling_content"})
        }, e.prototype.recalc_scrolling = function () {
            var e = this;
            return _.defer(function () {
                return e.scroller.nanoScroller({preventPageScrolling: !0})
            })
        }, e.prototype.attach_content = function () {
            return this.attach_content = function () {
                return Scribd.ReadPage.activity_list.container.detach_with_spacer().appendTo(this.content)
            }, Scribd.ReadPage.doc_info.container.clone().appendTo(this.content), this.attach_content()
        }, e.prototype.detach_content = function () {
            return Scribd.ReadPage.activity_list.container.reattach_from_spacer()
        }, e.prototype.min_position = function () {
            var e, t;
            return e = $("#global_header").height(), t = Scribd.ReadPage.toolbar.container, this.min_position = function () {
                var n;
                return n = t.is(".has_notification") ? t.find(".toolbar_notification").height() : 0, e + n
            }, this.min_position()
        }, e.prototype.max_position = function () {
            var e, t;
            return e = (t = Scribd.ReadPage.activity_list) != null ? t.container : void 0, e.offset().top
        }, e.prototype.toggle = function (e) {
            var t, n, r, i, s, o, u, a, f = this;
            if (this.container.is(":animated"))return!1;
            n = this.container.is(".visible");
            if (e === !0 && n)return;
            if (e === !1 && !n)return;
            s = Scribd.ReadPage.toolbar, u = $(window), t = $(document.body);
            if (n) {
                u.off("scroll.overlay_panel"), this.shroud.removeClass("visible"), s.move_pushdown(this._old_pushdown), this.container.animate({right: -this.container.outerWidth()}, this.anim_time, this.easing, function () {
                    return f.container.removeClass("visible").hide(), t.removeClass("overlay_open"), f.shroud.hide(), s.unlock()
                }), this.detach_content(), this.container.trigger("scribd:overlay_panel_hide");
                return
            }
            i = this.min_position(), r = this.max_position();
            if (r - i <= u.height() && !$(".doc_page").is(".book_view"))return;
            return(a = Scribd.ReadPage.toolbar) != null && a.toggle_panel("find", !1), o = Math.min($(window).width() - 100, this.max_width), this.shroud.show(), _.defer(function () {
                return f.shroud.addClass("visible")
            }), this.container.width(o).css("right", -o).show().addClass("visible").animate({right: 0}, this.anim_time, this.easing, function () {
                return u.on("scroll.overlay_panel", function () {
                    var e;
                    e = u.scrollTop();
                    if (e < i || r && e + u.height() > r)return f.toggle(!1), u.off("scroll.overlay_panel")
                }), f.recalc_scrolling()
            }), t.addClass("overlay_open"), s.lock(), this._old_pushdown = Scribd.ReadPage.toolbar.pushdown, s.move_pushdown(0), u.scrollTop() < i && $("html, body").animate({scrollTop: i}, "fast"), this.attach_content(), this.container.trigger("scribd:overlay_panel_show")
        }, e
    }()
}).call(this);


/* app/views/read2/_activity.coffee @ 1384554002 */
(function () {
    var e, t = function (e, t) {
        return function () {
            return e.apply(t, arguments)
        }
    };
    e = Scribd.make_tracker("read2:activity"), Scribd.ActivityList = function () {
        function n(n, r) {
            var i, s, o = this;
            this.opts = r, this.mark_comment = t(this.mark_comment, this), this.update_pagination = t(this.update_pagination, this), this.append_new_comment = t(this.append_new_comment, this), this.container = $(n), this.filter_params = {id: Scribd.current_doc.id}, s = this.container.find(".event_filters"), i = this.container.find(".action_picker"), this.comment_form = this.container.find(".comment_form:first"), this.form_rating = new Scribd.RatingPicker(this.comment_form.find(".rating_picker")), this.update_pagination(), $(document.body).on("scribd:increment_activity_count", function (e, t) {
                var n;
                return t == null && (t = 1), n = o.container.find(".activity_count_value"), n.html(parseInt(n.html().replace(/[^\d]/, ""), 10) + t)
            }), (this.is_anonymous = this.container.data("anonymous")) && this.container.on("click", ".comment_form", function () {
                return e("click", "comment_force_login"), Scribd.with_login("add_comment"), !1
            }), this.container.dispatch("click", "read2:activity", {picker_item: function (e) {
                var t, n;
                n = e.data("value");
                if (!n)return;
                return t = e.closest("[data-param]").data("param"), o.filter_params[t] = n, e.addClass("selected").siblings(".picker_item").removeClass("selected"), o.reload_events()
            }, toggle_filters_btn: function (e) {
                return e.toggleClass("open"), s.slideToggle("fast", function () {
                    return e.trigger("scribd:update_height")
                })
            }, spam_comment_btn: function (e) {
                var t;
                return t = e.closest(".document_comment"), o.mark_comment(t.data("id"), "spam"), e.remove()
            }, ham_comment_btn: function (e) {
                var t;
                return t = e.closest(".document_comment"), o.mark_comment(t.data("id"), "ham"), e.remove()
            }, delete_comment_btn: function (e) {
                var t, n;
                return t = e.closest(".document_comment").addClass("disabled"), n = Scribd.CSRF.with_token({_method: "DELETE"}), $.post("/documents/" + Scribd.current_doc.id + "/comments/" + t.data("id"), n, function (n) {
                    return t.slideUp("fast", function () {
                        return t.remove(), e.trigger("scribd:update_height"), $(document.body).trigger("scribd:increment_activity_count", [-1])
                    })
                })
            }, reply_comment_btn: function (e) {
                return o.toggle_reply(e.closest(".document_comment"))
            }, load_more_events_btn: function (e) {
                return o.filter_params.next_page = o._next_page, o.reload_events(!0), delete o.filter_params.next_page
            }, goto_annotation: function (e) {
                var t, n, r, i;
                n = e.data("annotation_id"), $(".annotation").removeClass("on"), t = $("#annotation_view_" + n);
                if (t.length)return t.addClass("on").offset(), (i = Scribd.ReadPage.overlay_panel) != null && i.toggle(!1), Scribd.UI.Annotations.update_body(), r = t.offset().top - 120, $("html, body").scrollTop(r)
            }}), this.container.on("click", ".action_btn", function (t) {
                var n, r;
                return n = $(t.currentTarget), r = n[0].className.match(/\b\w+_action\b/), r && e("click", r), n.is(".embed_action") ? Scribd.DocumentActions.run("embed_btn", n, "read2:activity") : n.is(".add_to_collection_btn") ? Scribd.DocumentActions.run("add_to_collection_btn", n, "read2:activity") : o.toggle_form(n, !0), !1
            }), this.container.on("submit", function (e) {
                var t, n;
                if (o.is_anonymous)return!1;
                n = $(e.target).closest(".comment_form");
                if (!n.length)return;
                n.removeClass("has_error");
                if (t = o.validate_form(n))return n.find(".error_text").text(t).end().addClass("has_error"), !1;
                n.addClass("loading");
                if (n.is(".readcast_form"))return $.post(o.container.data("like_url"))
            }), this.container.on("ajax:success", function (e, t) {
                var n;
                n = $(e.target).removeClass("loading"), $(document.body).trigger("scribd:increment_activity_count"), t.errors && n.find(".error_text").text(t.errors.join(", ")).end().addClass("has_error"), t.comment && o.append_new_comment(t.comment, n), o.reset_form(n);
                if (n.is(".reply_form"))return o.toggle_reply(n.prev(".document_comment"))
            })
        }

        return n.prototype.append_new_comment = function (e, t) {
            var n, r = this;
            n = this.container.find(".event_list:first"), this.container.find(".event_lists .empty_message").remove();
            if (t != null ? t.is(".reply_form") : void 0)n = t.next(".child_comments"), n.length || (n = $('<div class="child_comments"></div>').insertAfter(t));
            return $(e).children(":first").prependTo(n).hide().slideDown("fast", function () {
                return r.container.trigger("scribd:update_height")
            })
        }, n.prototype.update_pagination = function () {
            var e;
            return e = this.container.find(".event_list:last"), this._next_page = e.data("next_page"), this.container.toggleClass("has_more", !!this._next_page)
        }, n.prototype.toggle_form = function (e, t) {
            var n;
            this._action_buttons || (this._action_buttons = this.container.find(".action_btn"));
            if (_.isString(e)) {
                e = this._action_buttons.filter("[data-type='" + e + "']");
                if (!e.length)throw"Unknown activity action"
            }
            if (t === !0 && e.is(".selected")) {
                this.is_anonymous || this.comment_form.find("textarea").focus();
                return
            }
            if (t === !1 && !e.is(".selected"))return;
            return e.is(".selected") ? (e.removeClass("selected"), this.comment_form.removeClass("visible")) : (this.comment_form.removeClass("note_form review_form readcast_form").addClass("visible"), (n = e.data("type")) && this.comment_form.addClass("" + n + "_form"), this.form_rating.disable(n !== "review"), this.comment_form.attr("action", e.data("url")), this.set_form_placeholder(this.comment_form), this.is_anonymous || this.comment_form.find("textarea").focus(), this._action_buttons.removeClass("selected"), e.addClass("selected")), e.trigger("scribd:update_height")
        }, n.prototype.validate_form = function (e) {
            var t, n, r;
            n = e.find("textarea"), r = n.val();
            if ($.trim(r) === "")return"Please enter a comment";
            if (r.length > 400)return"You comment must be at most 400 characters";
            if (e.is(".review_form")) {
                t = parseInt(e.find("input[name='rated']").val(), 10);
                if (!(1 <= t && t <= 5))return"Please choose a rating"
            }
        }, n.prototype.reset_form = function (e) {
            return e.removeClass("has_error").find("textarea").val("")
        }, n.prototype.set_form_placeholder = function (e) {
            var t, n, r, i, s;
            r = e.find("textarea"), t = r.data("placeholders"), r.attr("placeholder", ""), s = [];
            for (i in t) {
                n = t[i];
                if (e.is("." + i + "_form")) {
                    r.attr("placeholder", n);
                    break
                }
                s.push(void 0)
            }
            return s
        }, n.prototype.reload_events = function (e) {
            var t = this;
            return this.container.addClass("loading").trigger("scribd:update_height"), this._event_lists || (this._event_lists = this.container.find(".event_lists")), e || this._event_lists.empty(), $.get(this.opts.url + "?" + $.param(this.filter_params)).done(function (e) {
                return t._event_lists.append(e.events), t.container.removeClass("loading").trigger("scribd:update_height"), t.update_pagination()
            })
        }, n.prototype.toggle_reply = function (e) {
            var t, n = this;
            return this._reply_form || (this._reply_form = this.comment_form.clone().attr("action", this.container.data("reply_url")).find(".rating_picker").remove().end().addClass("reply_form").removeClass("has_error note_form review_form readcast_form").prepend("<input type='hidden' name='parent_id' value='' />"), this.set_form_placeholder(this._reply_form)), this._reply_form.prev(".document_comment").removeClass("open"), e.next().is(this._reply_form) ? this._reply_form.stop().slideUp("fast", function () {
                return n._reply_form.detach(), n.container.trigger("scribd:update_height")
            }) : (this._reply_form.parent().length && (t = $("<div class='reply_spacer'></div>").height(this._reply_form.outerHeight(!0)).insertAfter(this._reply_form).slideUp("fast", function () {
                return t.remove()
            })), this._reply_form.find("input[name='parent_id']").val(e.data("id")), this._reply_form.stop().insertAfter(e.addClass("open")).hide().slideDown("fast", function () {
                return n.container.trigger("scribd:update_height")
            }))
        }, n.prototype.mark_comment = function (e, t) {
            return t == null && (t = "spam"), $.ajax({url: "/admin/documents/" + Scribd.current_doc.id + "/newcomments/" + e + "/mark_" + t, type: "put"})
        }, n
    }(), Scribd.RatingPicker = function () {
        function t(t) {
            var n, r, i = this;
            this.container = $(t), n = this.container.data("field_name") || "rating", this.input = $("<input type='hidden' name='" + n + "' value='0'/>").appendTo(this.container), r = this.container.find(".star_icon"), this.container.on("mousemove", ".star_icon", function (e) {
                var t;
                return t = $(e.target).closest(".star_icon"), r.removeClass("highlight"), t.prevAll().addBack().addClass("highlight")
            }), this.container.on("click", ".star_icon", function (t) {
                var n;
                return e("click", "choose_star_rating"), n = $(t.target).closest(".star_icon"), r.removeClass("selected"), n.prevAll().addBack().addClass("selected"), i.set_value(n.data("value")), !1
            })
        }

        return t.prototype.set_value = function (e) {
            return this.input.val(e), this.container.addClass("has_value").find(".current_rating").text(e)
        }, t.prototype.disable = function (e) {
            return this.input.prop("disabled", e)
        }, t
    }()
}).call(this);


/* app/views/read2/_banner.coffee @ 1384554002 */
(function () {
    Scribd.ReadBanner = function () {
        function e(e) {
            var t = this;
            this.container =
                $(e),
                this.make_sticky(),
                this.container.dispatch("click", "read2:preview_banner", {toggle_banner_btn: function (e) {
                return t.collapse_banner()
            }})
        }

        return e.prototype.make_sticky = function () {
            return this.sticky_area = this.container.sticky_container({unstick_after: this.container.parent(), padding: this.container.position().top + this.container.height()}), this.sticky_area.trigger("Scribd:restick")
        }, e.prototype.collapse_banner = function () {
            return this.container.toggleClass("collapsed"), $(".reader_footer").toggleClass("collapsed")
        }, e
    }()
}).call(this);


/* app/views/read2/comments/annotations.coffee @ 1384554002 */
(function () {
    var e, t, n = {}.hasOwnProperty, r = function (e, t) {
        function i() {
            this.constructor = e
        }

        for (var r in t)n.call(t, r) && (e[r] = t[r]);
        return i.prototype = t.prototype, e.prototype = new i, e.__super__ = t.prototype, e
    }, i = function (e, t) {
        return function () {
            return e.apply(t, arguments)
        }
    };
    (t = Scribd.UI).Annotations || (t.Annotations = {}), e = function () {
        return $(document.body).toggleClass("annotation_open", $(".annotation").filter(".on, .hover_on").length > 0 || $(".annotation_selector .selection_el").length > 0)
    }, Scribd.UI.Annotations.update_body = e, Scribd.UI.Annotations.Selector = function () {
        function t(e, t) {
            this.container = $(e), this.form = $(t), this.prepare_document(), this.outer_page_container = $(".outer_page_container"), this.selection_el = null, this.selecting = !1, this.start_position = null, this.end_position = null
        }

        return t.prototype.prepare_document = function () {
            return $(".outer_page").each(function (e, t) {
                var n;
                return n = $(t), n.data("page_num", n.prop("id").replace(/[A-Za-z-_]/g, ""))
            })
        }, t.prototype.first_outer_page = function () {
            return this._first_outer_page || (this._first_outer_page = this.outer_page_container.children(".outer_page:first"))
        }, t.prototype.fit_container = function () {
            return this.container.css({width: this.first_outer_page().width(), height: this.outer_page_container.height(), marginLeft: parseInt(this.first_outer_page().css("marginLeft")) + 2, marginRight: parseInt(this.first_outer_page().css("marginRight")) + 2})
        }, t.prototype.selection = function () {
            var e, t;
            if (!this.start_position || !this.end_position)return;
            if (this.start_position.page && this.end_position.page) {
                if (Math.abs(this.end_position.page - this.start_position.page) > 1)return;
                return t = $("#outer_page_" + this.start_position.page), e = $("#outer_page_" + this.end_position.page), {start_position: {page: this.start_position.page, page_width: t.width(), page_height: t.height(), x: this.start_position.x - t.position().left, y: this.start_position.y - t.position().top}, end_position: {page: this.end_position.page, page_width: e.width(), page_height: e.height(), x: this.end_position.x - e.position().left, y: this.end_position.y - e.position().top}}
            }
        }, t.prototype.disable = function () {
            var t;
            return $(".outer_page_container").removeClass("unselectable"), this.enabled = !1, this.container.removeClass("active"), this.container.off(), $(document).off(".annotations_selector"), (t = this.selection_el) != null && t.remove(), this.selecting = !1, this.start_position = null, this.end_position = null, e()
        }, t.prototype.enable = function (e) {
            var t = this;
            return $(".annotation.on").removeClass("on"), this.container.addClass("active"), $(".outer_page_container").addClass("unselectable"), this.default_comment_text = e, this.enabled = !0, this.fit_container(), $(document).on("mousedown.annotations_selector", function (e) {
                return $(e.target).closest(t.selection_el).length || t.disable(), !0
            }), $(document).on("keydown.annotations_selector", function (e) {
                if (e.which === 27 && t.enabled)return t.disable()
            }), $(document).on("mouseup.annotations_selector", function (e) {
                return t.selecting && (t.selecting = !1, t.end_position = t.mouse_position(e), t.selection_el.untransify(), t.selection_el.width() > 10 && t.selection_el.height() > 10 && t.end_position.page - t.start_position.page <= 1 ? (t.show_comment_form(), t.selection_el.transify()) : t.selection_el.remove()), !0
            }), this.container.mousedown(function (e) {
                var n;
                return $(e.target).closest(".comments_container").length ? !0 : (t.end_position = null, t.start_position = t.mouse_position(e), (n = t.selection_el) != null && n.remove(), t.selecting || (t.selection_el = $('<div class="selection_el selecting"></div>'), t.selecting = !0, t.selection_el.css({position: "absolute", left: t.start_position.x, top: t.start_position.y}), t.container.append(t.selection_el)), !1)
            }), this.container.mousemove(function (e) {
                return t.selecting ? (t.current_position = t.mouse_position(e), t.selection_el.untransify(), t.selection_el.css({width: Math.abs(t.current_position.x - t.start_position.x), height: Math.abs(t.current_position.y - t.start_position.y), left: Math.min(t.start_position.x, t.current_position.x), top: Math.min(t.start_position.y, t.current_position.y)}), t.selection_el.transify()) : !0
            })
        }, t.prototype.toggle = function () {
            return this.enabled ? this.disable() : this.enable()
        }, t.prototype.show_comment_form = function () {
            return this.selection_el.width() < 10 || this.selection_el.height() < 10 || (this.selection_el.html("<div class='comments_container'></div>"), this.comment_form = new Scribd.Backbone.AnnotationFormView({model: Scribd.Backbone.document, selection: this.selection(), selector: this, el: this.selection_el.children().first()}), this.selection_el.find(".comments_container:first").prepend($("<div class='read_sprite indicator'></div>")), this.selection_el.find(".comments_container").css("left", this.outer_page_container.width() - (parseInt(this.first_outer_page().css("marginRight")) * 2 + this.selection_el.position().left)), this.selection_el.find(".comments_container").css("top", -4), this.selection_el.find(".comment_text:first").val(this.default_comment_text)), e()
        }, t.prototype.hide_comment_form = function () {
            if (this.comment_form)return this.comment_form.remove()
        }, t.prototype.mouse_position = function (e) {
            var t, n, r;
            return n = {x: e.pageX, y: e.pageY}, r = {x: n.x - this.container.offset().left, y: n.y - this.container.offset().top}, t = $.nearest(n, ".outer_page").first(), t.length && (r.page = parseInt(t.data("page_num")), r.page_width = t.outerWidth(!0), r.page_height = t.outerHeight(!0)), r
        }, t
    }(), Scribd.Backbone.Annotation = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return r(t, e), t.prototype.idAttribute = "_id", t.prototype.urlRoot = function () {
            var e;
            return"/documents/" + ((e = this.get("document")) != null ? e.get("_id") : void 0) + "/annotations/"
        }, t.prototype.relations = [
            {type: Backbone.HasMany, key: "comments", relatedModel: "Scribd.Backbone.Comment", collectionType: "Scribd.Backbone.CommentCollection", reverseRelation: {key: "annotation"}},
            {type: Backbone.HasOne, key: "word_user", relatedModel: "Scribd.Backbone.WordUser"}
        ], t
    }(Backbone.RelationalModel), Scribd.Backbone.Annotation.setup(), Scribd.Backbone.AnnotationCollection = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return r(t, e), t.prototype.model = Scribd.Backbone.Annotation, t.prototype.url = function () {
            var e;
            return"/documents/" + ((e = this.document) != null ? e.get("_id") : void 0) + "/annotations/"
        }, t.prototype.comparator = function (e) {
            return e.get("start_position").y / e.get("start_position").page_height
        }, t
    }(Backbone.Collection), Scribd.Backbone.AnnotationFormView = function (e) {
        function t() {
            return this.submit_new_comment = i(this.submit_new_comment, this), this.toggle_privacy = i(this.toggle_privacy, this), t.__super__.constructor.apply(this, arguments)
        }

        return r(t, e), t.prototype.events = {"click .submit_button": "submit_new_comment", "change .comment_text": "input_changed", "keyup .comment_text": "input_changed", "click .comment_text": "require_login", "click .toggle_privacy": "toggle_privacy", "focus .comment_text": "require_login"}, t.prototype.initialize = function (e) {
            return e == null && (e = {}), this.selection = e.selection, this.selector = e.selector, this.privacy_setting = "public", t.__super__.initialize.apply(this, arguments)
        }, t.prototype.toggle_privacy = function (e) {
            this.$(".setting").toggle();
            switch (this.privacy_setting) {
                case"private":
                    return this.privacy_setting = "public";
                case"public":
                    return this.privacy_setting = "private"
            }
        }, t.prototype.submit_new_comment = function (e) {
            var t = this;
            trackEvent("newdoc", "Click", "annotation_post_note", Scribd.logged_in ? 1 : 0);
            if (_(this.$(".comment_text:first").val() || "").chain().trim().isBlank().value())return;
            return Scribd.with_login("add_comment", function () {
                var e, n, r;
                if (!t.disabled) {
                    n = {text: t.$el.find(".comment_text:first").val(), word_user: Scribd.Backbone.current_user, document: Scribd.Backbone.document, document_id: Scribd.Backbone.document.get("_id")};
                    if (t.selection)return r = _.extend(t.selection, {word_user: Scribd.Backbone.current_user, privacy_setting: t.privacy_setting, document: Scribd.Backbone.document, comments: [n]}), e = Scribd.Backbone.Annotation.build(r), e.save(null, {wait: !0, silent: !1, add: !0, success: function (e) {
                        return Scribd.Backbone.document.get("annotations").add(e, {silent: !1}), Scribd.Backbone.document.get("comments").add(e.get("comments").first()), $(".annotation").removeClass("on"), $("#annotation_view_" + e.id).addClass("on"), t.selector.disable()
                    }})
                }
            })
        }, t
    }(Scribd.Backbone.CommentFormView), Scribd.Backbone.AnnotationView = function (t) {
        function n() {
            return this.render = i(this.render, this), this.get_offset = i(this.get_offset, this), this.added = i(this.added, this), n.__super__.constructor.apply(this, arguments)
        }

        return r(n, t), n.prototype.events = {"click .show_all": "show_all_comments", "click .marker .indicator": "toggle_on", "keydown .comment_text": "set_on", "click .comments_container .show_form": "toggle_form", "click .view_all_notes:first": "goto_comments"}, n.COMMENT_CUTOFF = 3, n.prototype.toggle_on = function () {
            return window.trackEvent("newdoc", "Click", "annotation_toggle_on", Scribd.logged_in ? 1 : 0), $(".annotation").not(this.$el).removeClass("on"), this.$el.toggleClass("on"), e(), !1
        }, n.prototype.set_on = function () {
            return $(".annotation").not(this.$el).removeClass("on"), this.$el.addClass("on"), e(), !0
        }, n.prototype.toggle_form = function () {
            return this.$el.find(".comments_container:first").toggleClass("form_visible"), !1
        }, n.prototype.goto_comments = function () {
            return $.scrollTo($("#comments_container")), !1
        }, n.prototype.show_all_comments = function () {
            return this.$el.find(".show_all").hide(), this.collection_view.show_all_comments(), !1
        }, n.prototype.initialize = function () {
            return docManager.addEvent("zoomed", this.render), $(document).on("scribd:dom_height_changed scribd:render_annotations", this.render), this.model.on("remove:comments", this.render), this.model.on("add:comments", this.added), this.model.on("change:comments", this.render), this.render(), this.delegateEvents(this.events)
        }, n.prototype.added = function () {
            return this.model.get("comments").length > Scribd.Backbone.AnnotationView.COMMENT_CUTOFF ? this.$el.find(".show_all").show() : this.$el.find("show_all").hide(), this.render(), this.$el.find(".comments_container:first").removeClass("form_visible")
        }, n.prototype.get_offset = function () {
            var e;
            return e = $(".outer_page:first, #chunk"), this.get_offset = function () {
                return e.outerWidth()
            }, this.get_offset()
        }, n.prototype.render = function () {
            var t, n, r, i, s = this;
            if (this.model.isNew())return this;
            t = this.model.get("comments").every(function (e) {
                return!e.is_viewable()
            });
            if (t)return this.$el.empty(), this._rendered = !1, this;
            this._rendered || (this._rendered = !0, this.$el.html("<div class='selection'>\n</div>\n<div class='marker'>\n  <div class='read_sprite indicator' />\n  <div class='comments_container'>\n    <div class='comments_collection'></div>\n    <div class='root_form'></div>\n    <div class='bottom_tools'>\n      <a class='view_all_notes'>View all notes</span>\n      <a class='show_all' style='display: none'>Show all</span>\n    </div>\n  </div>\n</div>"), this.collection_view = new Scribd.Backbone.AnnotationCommentCollectionView({collection: new Scribd.Backbone.RootComments(null, {parent: this.model.get("comments")}), el: this.$el.find(".comments_collection:first"), parent_view: this}), this.comment_form = new Scribd.Backbone.AnnotationCommentFormView({model: this.model, el: this.$el.find(".root_form:last"), parent_view: this}), Scribd.Backbone.current_user || this.comment_form.hide(), i = function () {
                return s.$el.addClass("hover_on"), e(), !0
            }, r = function () {
                return _.delay(function () {
                    return s.$el.removeClass("hover_on"), e()
                }, 200), !0
            }, this.$el.find(".marker:first").hover(i, r), this.model.get("comments").length > Scribd.Backbone.AnnotationView.COMMENT_CUTOFF && this.$el.find(".show_all").show(), this.$el.attr("id", "annotation_view_" + this.model.id), this.model.get("word_user") === Scribd.Backbone.current_user && this.$el.addClass("mine"), this.model.get("privacy_setting") === "private" && this.$el.addClass("private")), this.$el.css({top: this.model.get("start_position").y * 1 / this.model.get("start_position").page_height * this.$el.parent().height()}), n = $("#outer_page_" + this.model.get("end_position").page), this.$el.toggle(n.length > 0);
            if (n.length === 0)return;
            return this.$el.find(".selection:first").css({left: this.model.get("start_position").x * 1 / this.model.get("start_position").page_width * this.$el.parent().width(), width: Math.abs(this.model.get("end_position").x - this.model.get("start_position").x) / (this.model.get("start_position").page_width / this.$el.parent().width()), height: Math.abs(this.model.get("end_position").y - this.model.get("start_position").y) / (this.model.get("start_position").page_height / this.$el.parent().height())}), this.$el.find(".marker").css({left: this.get_offset()}), this
        }, n
    }(Backbone.View), Scribd.Backbone.AnnotationCommentCollectionView = function (e) {
        function t() {
            return this.add_comment_view = i(this.add_comment_view, this), t.__super__.constructor.apply(this, arguments)
        }

        return r(t, e), t.prototype.initialize = function () {
            return t.__super__.initialize.apply(this, arguments), this.all_shown = !1
        }, t.prototype.show_all_comments = function () {
            var e, t, n;
            n = this.comment_views;
            for (e in n)t = n[e], t.$el.show();
            return this.all_shown = !!this.comment_views.length
        }, t.prototype.add_comment_view = function (e, n) {
            var r;
            t.__super__.add_comment_view.apply(this, arguments);
            if (_.keys(this.comment_views).length > Scribd.Backbone.AnnotationView.COMMENT_CUTOFF && !this.all_shown)return(r = this.comment_views[parseInt(e.get("_id"))]) != null ? r.$el.hide() : void 0
        }, t
    }(Scribd.Backbone.CommentCollectionView), Scribd.Backbone.AnnotationCollectionView = function (e) {
        function t() {
            return this.render = i(this.render, this), this.add_view = i(this.add_view, this), t.__super__.constructor.apply(this, arguments)
        }

        return r(t, e), t.prototype.initialize = function () {
            var e, t, n, r;
            this.annotation_views = {}, this.render(), r = this.collection.models;
            for (t = 0, n = r.length; t < n; t++)e = r[t], this.add_view(e);
            return this.collection.on("add", this.add_view)
        }, t.prototype.outer_page_container = function () {
            var e;
            return this.outer_page_container = function () {
                return e
            }, e = $(".outer_page_container")
        }, t.prototype.add_view = function (e, t) {
            var n, r, i;
            if (parseInt(e.get("_id")) && this.annotation_views[parseInt(e.get("_id"))])return;
            i = this.outer_page_container().children("#outer_page_" + e.get("start_position").page), i.append($('<div class="annotation"></div>')), r = i.find("div.annotation:last"), n = new (this.options.view_cls || Scribd.Backbone.AnnotationView)({model: e, parent_view: this, el: r}), Scribd.UI.annotation_views ? Scribd.UI.annotation_views << n : Scribd.UI.annotation_views = [n];
            if (parseInt(e.get("_id")))return this.annotation_views[parseInt(e.get("_id"))] = n
        }, t.prototype.render = function () {
            return this
        }, t
    }(Backbone.View)
}).call(this);


/* app/views/read2/dialogs/flag_doc_dialog.coffee @ 1384554002 */
(function () {
    Scribd.FlagDocDialog = function () {
        function e(e) {
            var t = this;
            this.container = $(e).on("submit",function (e) {
                return t.container.addClass("loading")
            }).on("ajax:complete", function (e, n) {
                return t.container.removeClass("loading"), Scribd.Lightbox.close()
            })
        }

        return e
    }()
}).call(this);


/* app/views/read2/dialogs/print_dialog.coffee @ 1384554002 */
(function () {
    var e;
    e = Scribd.make_tracker("read2:print_dialog"), Scribd.PrintDialog = function () {
        function t(t) {
            var n = this;
            this.container = $(t), e("show", "show"), this.container.dispatch("click", "read2:print_dialog", {download_btn: function () {
                return Scribd.download_actions.download(Scribd.current_doc.id, "read", "print")
            }})
        }

        return t
    }()
}).call(this);


/* app/views/read2/dialogs/readcast_history_dialog.coffee @ 1384554002 */
(function () {
    var e, t, n, r, i, s, o = {}.hasOwnProperty, u = function (e, t) {
        function r() {
            this.constructor = e
        }

        for (var n in t)o.call(t, n) && (e[n] = t[n]);
        return r.prototype = t.prototype, e.prototype = new r, e.__super__ = t.prototype, e
    };
    (i = window.Scribd) == null && (window.Scribd = {}), (s = Scribd.UI) == null && (Scribd.UI = {}), r = {delete_readcast: function (e) {
        var t;
        return t = Scribd.CSRF.with_token({_method: "delete"}), $.post("/readcast/" + e, t)
    }, fetch_readcasts: function (e) {
        return $.get("/readcast/all", {page: e})
    }}, n = function (n) {
        function i() {
            return i.__super__.constructor.apply(this, arguments)
        }

        return u(i, n), i.prototype.initialize = function () {
            var n = this;
            return _.bindAll(this), this.paginator = new t({page_size: 10, item_count: this.options.readcast_count}), this.footer = new e({el: this.$(".footer"), paginator: this.paginator}), this.paginator.on("change:page", function (e) {
                return n.update_page()
            }), this.paginator.on("change:item_count", function (e, t) {
                return n.update_page(t)
            }), this.update_page(!1)
        }, i.prototype.update_page = function (e) {
            return e == null && (e = !0), this.paginator.item_count === 0 ? this.render_no_items() : e ? this.load_page().then(this.footer.update) : this.footer.update()
        }, i.prototype.events = {"click .next_page": "next_page", "click .prev_page": "prev_page", "click .delete_button": "confirm_delete", "click .cancel_button, .done_button": function (e) {
            return Scribd.Lightbox.close()
        }}, i.prototype.render_no_items = function () {
            var e;
            return e = Scribd.template("readcast_history_no_items")(), this.$(".items").replaceWith(e), this.footer.remove()
        }, i.prototype.next_page = function (e) {
            return e.preventDefault(), this.paginator.next()
        }, i.prototype.prev_page = function (e) {
            return e.preventDefault(), this.paginator.prev()
        }, i.prototype.load_page = function () {
            var e = this;
            return r.fetch_readcasts(this.paginator.page).then(function (t) {
                return e.$(".items").html(t)
            })
        }, i.prototype.confirm_delete = function (e) {
            var t, n, r;
            e.preventDefault(), t = $(e.currentTarget).closest(".item"), r = t.find(".item_title").text(), n = 'Are you sure you want to delete the shares for "' + r + '"?';
            if (confirm(n))return this.delete_item(t)
        }, i.prototype.delete_item = function (e) {
            var t, n;
            return r.delete_readcast(e.data("id")), $(document).trigger("scribd:delete_readcast", [e.data("readcast_id")]), e.addClass("deleted"), n = e.find(".item_title"), n.replaceWith("<span class='item_title'>" + n.text() + "</span>"), t = e.find(".item_date"), t.replaceWith("<span class='status'>Deleted</span>"), this.paginator.set_item_count(this.paginator.item_count - 1), e.delay(2e3).slideUp(150)
        }, i
    }(Backbone.View), e = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return u(t, e), t.prototype.initialize = function () {
            return _.bindAll(this), this.paginator = this.options.paginator, this.update()
        }, t.prototype.update = function () {
            return this.update_status(), this.update_nav_buttons()
        }, t.prototype.update_nav_buttons = function () {
            var e, t, n, r;
            return r = !this.paginator.at_first(), n = !this.paginator.at_last(), t = [
                [".prev_page", r],
                [".divider", r && n],
                [".next_page", n]
            ], e = this.$(".paginator"), _.each(t, function (t) {
                var n, r, i;
                return r = t[0], i = t[1], n = e.find(r), i ? n.show() : n.hide()
            })
        }, t.prototype.update_status = function () {
            var e, t, n, r, i, s;
            return s = this.paginator, n = s.page, r = s.page_size, t = s.item_count, i = Scribd.template("readcast_history_footer_status"), e = i({start: (n - 1) * r + 1, end: Math.min(n * r, t), count: t}), this.$(".footer_status").html(e)
        }, t
    }(Backbone.View), t = function () {
        function e(e) {
            var t;
            _.bindAll(this), this.item_count = e.item_count, this.page_size = e.page_size, this.page = e.page, (t = this.page) == null && (this.page = 1)
        }

        return _.extend(e.prototype, Scribd.Events), e.prototype.next = function () {
            if (this.at_last())throw new Error("Already on the last page");
            return this.trigger("change:page", ++this.page)
        }, e.prototype.prev = function () {
            if (this.at_first())throw new Error("Already on the first page");
            return this.trigger("change:page", --this.page)
        }, e.prototype.at_first = function () {
            return this.page === 1
        }, e.prototype.at_last = function () {
            return this.page === this.max_page()
        }, e.prototype.max_page = function () {
            return Math.ceil(this.item_count / this.page_size)
        }, e.prototype.set_item_count = function (e) {
            var t, n;
            if (e >= 0)return this.item_count = e, n = this.page, this.page = Math.min(this.page, this.max_page()), t = this.page !== n, this.trigger("change:item_count", e, t);
            throw new Error("Item count must be at least 0")
        }, e
    }(), Scribd.UI.ReadcastHistoryDialog = n
}).call(this);


/* app/views/read2/epub.coffee @ 1384554002 */
(function () {
    Scribd.Epub = function () {
        function e() {
        }

        return e.prototype.setup_reading_progress = function () {
            var e, t;
            if (typeof Scribd != "undefined" && Scribd !== null ? (t = Scribd.current_doc) != null ? t.reading_progress : void 0 : void 0)return e = Math.floor(Scribd.current_doc.reading_progress[0]), this.goto_page(this.block_to_page(e))
        }, e.prototype.setup_toc = function (e) {
            var t, n, r, i, s, o;
            return o = e, s = _.template($("#tpl_section_row").html()), r = _.template($("#tpl_section_row_disabled").html()), n = function () {
                var e, n, o;
                o = [];
                for (i = e = 0, n = reflow_toc.length; e < n; i = ++e)t = reflow_toc[i], t.included !== !1 ? o.push(s({chapter: i, title: t.title, anchor: t.anchor})) : o.push(r({title: t.title}));
                return o
            }(), Scribd.ReadPage.toolbar.container.toggleClass("no_find_panel", n.length === 0), o.empty().append(n), this.setup_toc_events()
        }, e.prototype.setup_toc_events = function () {
            var e = this;
            return $("#doc_toolbar").dispatch("click", this.tracking_name(), {section_btn: function (t) {
                var n, r, i;
                if (!e.container.is(":visible"))return;
                return r = t.data("chapter"), n = e.globalize_block(r, 0), i = e.block_to_page(n), e.goto_page(i)
            }})
        }, e.prototype.update_appearance = function (e, t, n) {
            var r, i, s = this;
            n == null && (n = !0), i = !1, e === "font_size" && (t === "plus" ? this.reflow.increase_scale() : t === "minus" ? this.reflow.decrease_scale() : (isNaN(t) || !this.reflow.update_scale(t - this.reflow.scale)) && this.reflow.reset_scale(), t = this.reflow.scale, this.reflow.at_default_scale() && (i = !0)), n && this.update_default_settings(e, t);
            if (!n || i)r = t, e === "font_size" && this.reflow.at_default_scale() && (r = "default"), _.defer(function () {
                var t;
                return(t = Scribd.ReadPage.font_popup) != null ? t.set_value(e, r, !0, !1) : void 0
            });
            switch (e) {
                case"font_style":
                    return this.reflow.set_fontstyle(t);
                case"color":
                    return this.container.toggleClass("is_dark", t === "black").toggleClass("is_sepia", t === "sepia")
            }
        }, e.prototype.update_default_settings = function (e, t) {
            var n = this;
            return Scribd.with_local_storage(function (r) {
                var i;
                return i = JSON.parse(r.getItem(n.settings_key)) || {}, i[e] = t, r.setItem(n.settings_key, JSON.stringify(i))
            })
        }, e.prototype.load_default_style = function (e) {
            var t, n, r, i, s, o, u = this;
            e == null && (e = []), n = {}, Scribd.with_local_storage(function (e) {
                return n = JSON.parse(e.getItem(u.settings_key)) || n
            }), o = [];
            for (i = 0, s = e.length; i < s; i++)t = e[i], (r = n[t] || this.defaults[t]) ? o.push(this.update_appearance(t, r, !1)) : o.push(void 0);
            return o
        }, e.prototype.total_blocks = function () {
            var e, t, n, r, i;
            t = 0, i = book.chapters;
            for (n = 0, r = i.length; n < r; n++)e = i[n], t += e.blocks.length;
            return t = Math.max(0, t - 10), this.total_blocks = function () {
                return t
            }, t
        }, e.prototype.globalize_block = function (e, t) {
            var n, r, i;
            n = 0;
            for (r = i = 0; 0 <= e ? i < e : i > e; r = 0 <= e ? ++i : --i)n += book.chapters[r].blocks.length;
            return n + t
        }, e.prototype.block_to_page = function (e) {
            var t, n, r, i, s, o, u, a, f, l, c, h;
            t = 1, i = e + 1, n = 0, u = this.reflow.pages.length - 1, h = this.reflow.pages;
            for (s = f = 0, c = h.length; f < c; s = ++f) {
                a = h[s];
                if (n !== a.chapter_idx || s === u) {
                    r = a.chapter_idx - n - 1;
                    for (o = l = 0; 0 <= r ? l <= r : l >= r; o = 0 <= r ? ++l : --l)t += book.chapters[n + o].blocks.length;
                    n = a.chapter_idx
                }
                if (i < t + a.block_idx)return s - 1
            }
            return this.reflow.pages.length - 1
        }, e
    }()
}).call(this);


/* app/views/read2/_footer_documents.coffee @ 1384554002 */
(function () {
    Scribd.ReadFooter = function () {
        function e(e) {
            this.container = $(e), new Scribd.UI.DocumentPopups(this.container.find(".document_popups"), this.container, "read2:document_popups")
        }

        return e
    }()
}).call(this);


/* app/views/read2/_info.coffee @ 1384554002 */
(function () {
    var e, t = {}.hasOwnProperty, n = function (e, n) {
        function i() {
            this.constructor = e
        }

        for (var r in n)t.call(n, r) && (e[r] = n[r]);
        return i.prototype = n.prototype, e.prototype = new i, e.__super__ = n.prototype, e
    };
    e = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return n(t, e), t.prototype.download_page = "read", t.prototype.download_action = "info_download", t.prototype.find_object = function (e) {
            return{type: "document", id: Scribd.current_doc.id, secret_password: Scribd.current_doc.secret_password}
        }, t.prototype.flag_document_btn = function (e) {
            var t = this;
            return Scribd.with_login("flag_document", Scribd.current_doc.url, function () {
                return Scribd.Lightbox.remote_open("lightbox_flag_document", "/read/flag_dialog", Scribd.toolbar_params())
            })
        }, t
    }(Scribd.DocumentActions), Scribd.DocInfo = function () {
        function t(t, n) {
            var r = this;
            n == null && (n = {}), this.container = $(t).has_expandable_text(function (e) {
                return e.trigger("scribd:update_height")
            }), this.container.find(".doc_description_short").dotdotdot(), new e(this.container, "document_info"), this.container.dispatch("click", "read2:info", {toggle_open_btn: function () {
                return r.container.toggleClass("open")
            }}), Scribd.hook_tooltips(this.container), $(document.body).on("scribd:increment_activity_count", function (e, t, n) {
                var i;
                t == null && (t = 1);
                if (n === "like")return i = r.container.find(".like_count_value"), i.html(parseInt(i.html(), 10) + t)
            }), this.container.find(".rating_star_holder").star_ratings({defaultValue: n.initial_rating || 0, force_login: !0, setValue: n.user_set_rating || null}), this.container.on("Scribd:rating", function (e, t) {
                var n;
                return n = 3, $.post("/ratings?word_document_id=" + Scribd.current_doc.id + ".json", {rated: t, source: n}, function (e) {
                    if (e.total_document_count != null)return $(".doc_info").find(".rating_stat .value").text("(" + e.total_document_count + ")")
                })
            })
        }

        return t
    }()
}).call(this);


/* app/views/read2/popups/_add_note_popup.coffee @ 1384554002 */
(function () {
    var e = {}.hasOwnProperty, t = function (t, n) {
        function i() {
            this.constructor = t
        }

        for (var r in n)e.call(n, r) && (t[r] = n[r]);
        return i.prototype = n.prototype, t.prototype = new i, t.__super__ = n.prototype, t
    };
    Scribd.AddNotePopup = function (e) {
        function n(e) {
            var t = this;
            this.container = $(e), this.setup_comment_form(), this.container.dispatch("click", "read2:" + this.track_category, {view_all_notes_btn: function (e) {
                return Scribd.ReadPage.toolbar.close_popup(), $(document.body).trigger("scribd:show_notes")
            }, select_region_btn: function () {
                var e;
                return Scribd.UI.Annotations.selector.enable(t.container.find(".comment_text:first").val()), e = $(".outer_page_container").offset().top, $(window).scrollTop() < e && $("body, html").animate({scrollTop: e}), Scribd.ReadPage.toolbar.close_popup()
            }})
        }

        return t(n, e), n.prototype.track_category = "note_popup", n
    }(Scribd.CommentingPopup)
}).call(this);


/* app/views/read2/popups/_font_popup.coffee @ 1384554002 */
(function () {
    var e = function (e, t) {
        return function () {
            return e.apply(t, arguments)
        }
    };
    Scribd.FontPopup = function () {
        function t(t) {
            this.set_value = e(this.set_value, this);
            var n = this;
            this.container = $(t), this.container.delegate_tracking("click", "read2"), this.container.dispatch("click", "read2:font_popup", {option: function (e) {
                var t;
                return t = e.closest(".option_picker"), t.find(".option").removeClass("selected"), e.addClass("selected"), n.set_value(t.data("picker_name"), e.data("value"))
            }})
        }

        return t.prototype.set_value = function (e, t, n, r) {
            var i;
            n == null && (n = !1), r == null && (r = !0), n && (i = this.container.find("[data-picker_name='" + e + "']"), i.find(".option").removeClass("selected").filter("[data-value='" + t + "']").addClass("selected"));
            if (r)return this.container.trigger("scribd:epub_appearance", [e, t])
        }, t
    }()
}).call(this);


/* app/views/read2/popups/_readcast_popup.coffee @ 1384554002 */
(function () {
    var e = {}.hasOwnProperty, t = function (t, n) {
        function i() {
            this.constructor = t
        }

        for (var r in n)e.call(n, r) && (t[r] = n[r]);
        return i.prototype = n.prototype, t.prototype = new i, t.__super__ = n.prototype, t
    }, n = [].slice;
    Scribd.ReadcastPopup = function (e) {
        function r(e) {
            var t = this;
            this.container = $(e), this._has_readcasted = !1, $(document).on("scribd:delete_readcast", function (e, n) {
                var r;
                if (n === ((r = t.submitted_readcast) != null ? r.like_id : void 0))return t._after_undo()
            }), this.container.on("scribd:popup_open", function (e, n) {
                if (t._has_readcasted)return;
                return t.toolbar_btn = n, t._has_readcasted = !0, t._submit_readcast().done(function (e) {
                    return t.submitted_readcast = e.data, t.set_readcast_message(e.data.total_readcasts), t.container.addClass("success"), t.toolbar_btn.addClass("liked"), $(document.body).trigger("scribd:increment_activity_count", [1, "like"])
                })
            }), this._toggle_facebook_share = _.debounce(function () {
                var e;
                return e = 1 <= arguments.length ? n.call(arguments, 0) : [], this.constructor.prototype._toggle_facebook_share.apply(this, e)
            }, 200), this.container.dispatch("click", "read2:" + this.track_category, {undo_readcast_btn: function () {
                return t._undo_readcast()
            }, view_history_btn: function (e) {
                return Scribd.Lightbox.remote_open("readcast_history_dialog", "/read/readcast_history_dialog")
            }, toggle_share_btn: function (e) {
                if (!Scribd.named_current_user_sync().is_facebook_user) {
                    Scribd.facebook.login(function (t) {
                        if (Scribd.named_current_user_sync().is_facebook_user)return e.trigger("click")
                    });
                    return
                }
                return t._toggle_facebook_share(), e.toggleClass("enabled")
            }}), this.setup_comment_form(), this._check_facebook()
        }

        return t(r, e), r.prototype.track_category = "readcast_popup", r.prototype.increment_activity = !1, r.prototype._check_facebook = function () {
            var e;
            if (!(this._is_facebook_user() && ((e = Scribd.current_doc.auto_share) != null ? e.facebook : void 0)))return;
            return this.needs_facebook = Scribd.facebook_login_status.then(function () {
                return Scribd.facebook.check_permission("publish_stream")
            })
        }, r.prototype._is_facebook_user = function () {
            return Scribd.named_current_user_sync().is_facebook_user
        }, r.prototype._toggle_facebook_share = function () {
            var e, t, n;
            return e = Scribd.current_doc.auto_share, n = {_method: "put"}, t = function (e) {
                return"options[event_settings][reading][" + e + "]"
            }, e.scribd = !e.scribd, n[t("scribd")] = e.scribd ? "on" : "off", e.facebook = e.scribd, n[t("facebook")] = n[t("scribd")], $.post("/sharing_preference", n)
        }, r.prototype._submit_readcast = function () {
            var e, t, n = this;
            return this.needs_facebook ? (e = $.Deferred(), t = function () {
                return $.post(n.container.data("like_url")).done(function () {
                    return e.resolve.apply(e, arguments)
                })
            }, this.needs_facebook.then(t, function () {
                return Scribd.facebook.login(function (e) {
                    if (e)return t(), n.needs_facebook = null
                })
            }), e) : $.post(this.container.data("like_url"))
        }, r.prototype._undo_readcast = function () {
            return $.post(this.container.data("unlike_url"), {like_id: this.submitted_readcast.like_id}), this._after_undo()
        }, r.prototype._after_undo = function () {
            var e;
            return this.container.removeClass("success").addClass("canceled"), (e = this.toolbar_btn) != null && e.removeClass("liked"), $(document.body).trigger("scribd:increment_activity_count", [-1, "like"])
        }, r.prototype.set_readcast_message = function (e) {
            var t;
            return t = function () {
                switch (e) {
                    case 0:
                    case 1:
                        return I18n.t("js.newdoc.you_liked_this");
                    case 2:
                        return I18n.t("js.newdoc.you_and_one_other_liked_this");
                    default:
                        return isNaN(e) ? I18n.t("js.newdoc.you_liked_this") : I18n.t("js.newdoc.you_and_x_others_liked_this", {count: e - 1})
                }
            }(), t + ".", this.container.find(".readcast_total > .text").html(t)
        }, r
    }(Scribd.CommentingPopup)
}).call(this);


/* app/views/read2/popups/_share_popup.coffee @ 1384554002 */
(function () {
    var e = function (e, t) {
        return function () {
            return e.apply(t, arguments)
        }
    };
    Scribd.SharePopup = function () {
        function t(t) {
            this.add_scripts = e(this.add_scripts, this);
            var n = this;
            this.container = $(t), this.container.on("scribd:popup_open", function (e) {
                return n.add_scripts(), n.add_scripts = function () {
                }
            })
        }

        return t.prototype.add_scripts = function () {
            return $(document.body).append(this.container.data("external_scripts")), FB.XFBML.parse(this.container[0])
        }, t
    }()
}).call(this);


/* app/views/read2/popups/_sync_mobile_popup.coffee @ 1384554002 */
(function () {
    var e, t = {}.hasOwnProperty, n = function (e, n) {
        function i() {
            this.constructor = e
        }

        for (var r in n)t.call(n, r) && (e[r] = n[r]);
        return i.prototype = n.prototype, e.prototype = new i, e.__super__ = n.prototype, e
    };
    e = Scribd.make_tracker("read2:sync_mobile_popup"), Scribd.SyncMobilePopup = function (t) {
        function r() {
            var t = this;
            r.__super__.constructor.apply(this, arguments), this.container.on("submit", ".text_form", function (t) {
                return e("click", "submit_phone")
            }), this.container.dispatch("click", "read2:sync_mobile_popup", {email_signup_link: function () {
                return t.container.addClass("email_complete")
            }})
        }

        return n(r, t), r
    }(Scribd.SMSMobileAppForm)
}).call(this);


/* app/views/read2/show_bot.coffee @ 1384554002 */
(function () {
    var e = {}.hasOwnProperty, t = function (t, n) {
        function i() {
            this.constructor = t
        }

        for (var r in n)e.call(n, r) && (t[r] = n[r]);
        return i.prototype = n.prototype, t.prototype = new i, t.__super__ = n.prototype, t
    };
    Scribd.BotReadPage = function (e) {
        function n(e, t) {
            t == null && (t = {}), this.container = $(e), this.doc_container = $(), this.setup_responsive(), this.setup_width()
        }

        return t(n, e), n
    }(Scribd.ReadPage)
}).call(this);


/* app/views/read2/_sidebar.coffee @ 1384554002 */
(function () {
    Scribd.ReadSidebar = function () {
        function e(e) {
            this.container = $(e), this.setup_responsive(), this.make_sticky(), this.is_downloader = !!this.container.find(".sidebar_downloader .has_download").length
        }

        return e.prototype.height_threshold = 1100, e.prototype.make_sticky = function () {
            var e = this;
            if (this.container.find(".sidebar_downloader").is(".no_stick"))return;
            return this.sticky_area = this.container.find(".sticky_area").sticky_container({unstick_after: this.container.parent(), padding: 55, before_stick: function (t) {
                if (t.is(".bottomed"))return;
                if (t.is(".unbottoming")) {
                    t.removeClass("unbottoming");
                    return
                }
                return e.reveal_sticky_content()
            }, before_unstick: function (t) {
                if (t.is(".bottomed"))return;
                return e.hide_sticky_content()
            }, before_bottom: function (t) {
                var n;
                return n = t.offset().top - e.container.offset().top, t.css({top: n})
            }, before_unbottom: function (e) {
                return e.css({top: ""}).addClass("unbottoming")
            }}), $(document.body).on("scribd:ads_disabled", function () {
                return e.sticky_area.trigger("Scribd:update_root").trigger("Scribd:restick")
            })
        }, e.prototype.reveal_sticky_content = function () {
            var e;
            if (!this.should_stick_promo())return;
            if ((e = this._stuck_element) != null ? e.is(":animated") : void 0) {
                this._stuck_element.stop(!0).hide().fadeIn();
                return
            }
            return this._promo_clone || (this._promo_clone = this.container.find(".sidebar_downloader").clone(!0), this.update_sticky_promo()), this._stuck_element = this._promo_clone, this.sticky_area.append(this._stuck_element), this._stuck_element.hide().fadeIn()
        }, e.prototype.hide_sticky_content = function () {
            var e = this;
            if (this._stuck_element)return this._stuck_element.stop(!0).fadeOut(function () {
                return e._promo_clone.detach(), delete e._stuck_element
            })
        }, e.prototype.should_stick_promo = function () {
            return this.is_downloader ? !0 : $(document.body).is(".ads_disabled") ? !0 : !1
        }, e.prototype.update_sticky_promo = function () {
            var e, t;
            return t = $(window), e = $(document.body), this.update_sticky_promo = function () {
                var n;
                return(n = this._promo_clone) != null ? n.toggleClass("is_short", !e.is(".ads_disabled") && t.height() < this.height_threshold) : void 0
            }, this.update_sticky_promo()
        }, e.prototype.setup_responsive = function () {
            var e = this;
            return $(window).on("resize", function () {
                return e.update_sticky_promo()
            }), this.update_sticky_promo()
        }, e
    }()
}).call(this);


/* app/views/read2/sidebar/_admin.coffee @ 1384554002 */
(function () {
    Scribd.ReadPageAdmin = function () {
        function e(e) {
            var t, n, r = this;
            this.container = $(e), n = !0, t = !0, this.container.dispatch("click", {toggle_admin_btn: function () {
                return r.container.toggleClass("open")
            }}), this.container.on("ajax:success", ".feature_btn", function (e) {
                var t, n;
                return t = $(e.currentTarget), n = t.data("params"), n.featured = !n.featured, t.html(n.featured && "[ Feature ]" || "[ Unfeature ]")
            })
        }

        return e
    }()
}).call(this);


/* app/views/read2/sidebar/_document_list.coffee @ 1384554002 */
(function () {
    Scribd.SidebarDocumentList = function () {
        function e(e) {
            var t = this;
            this.container = $(e), this.container.dispatch("click", "read2:sidebar_list", {next_page_btn: function () {
                return t.move_page(1)
            }, prev_page_btn: function () {
                return t.move_page(-1)
            }})
        }

        return e.prototype.current_page = 1, e.prototype.move_page = function (e) {
            return e == null && (e = 0), this.pages || (this.pages = this.container.find(".sidebar_doc_page")), this.current_page_label || (this.current_page_label = this.container.find(".current_page")), this.current_page += e, this.current_page = Math.max(1, Math.min(this.pages.length, this.current_page)), this.current_page_label.text(this.current_page), this.container.toggleClass("first_page", this.current_page === 1).toggleClass("last_page", this.current_page === this.pages.length), this.pages.hide().eq(this.current_page - 1).show()
        }, e
    }()
}).call(this);


/* app/views/read2/sidebar/_find_panel.coffee @ 1384554002 */
(function () {
    var e;
    e = Scribd.make_tracker("read2:find_panel"), Scribd.Highlighter = {highlight_class: "fourgen_highlight", clear: function () {
        var e, t, n, r, i, s;
        i = $("." + this.highlight_class), s = [];
        for (n = 0, r = i.length; n < r; n++)e = i[n], t = e.parentNode, t.replaceChild(e.firstChild, e), s.push(t.normalize());
        return s
    }, highlight: function (e, t) {
        var n, r, i, s = this;
        return i = RegExp(Scribd.escape_regexp(t), "i"), r = 0, n = function (e) {
            var o, u, a, f, l, c, h;
            l = 0;
            if (e.nodeType === 3)f = e.data.search(i), f >= 0 && (c = $("<span>"), c.addClass(s.highlight_class), o = e.splitText(f), o.splitText(t.length), u = $(o).clone(), c.append(u), $(o).replaceWith(c), l = 1, r++); else if (e.nodeType === 1 && e.childNodes && !/(script|style)/i.test(e.tagName))if (((h = e.className) != null ? h.indexOf(s.highlight_class) : void 0) < 0 || e.className == null) {
                a = 0;
                while (a < e.childNodes.length)a += n(e.childNodes[a]), ++a
            } else r++;
            return l
        }, n(e), r
    }}, Scribd.ToolbarFindPanel = function () {
        function t(e, t) {
            var n = this;
            this.toolbar = t, this.container = $(e), this.input = this.container.find(".find_input"), this.input.on("keydown", function (e) {
                if (e.keyCode === 27)return n.toolbar.toggle_panel("find"), !1
            }), this.form = this.container.find(".search_form").on("ajax:success", function (e, t) {
                n.render_results(t);
                if (t.length)return n.goto_result(0)
            }), this.form.on("submit", function () {
                return n.form.addClass("loading"), n.last_query = n.input.val()
            }), this.form.on("ajax:complete", function (e) {
                return n.form.removeClass("loading")
            }), this.container.dispatch("click", "read2:find_panel", {clear_search_btn: function (e) {
                if (n.input.val() === "" && !n.container.hasClass(".has_results")) {
                    n.toolbar.toggle_panel("find");
                    return
                }
                return Scribd.Highlighter.clear(), n.container.removeClass("has_results").trigger("scribd:update_panel"), n.input.val("")
            }, search_result: function (e) {
                var t;
                return t = e.data("result_id"), n.goto_result(t)
            }}), docManager.addEvent("pageLoaded", function (e) {
                var t, r, i;
                t = parseInt(e.id.match(/\d+$/)[0], 10);
                if ((r = n._to_highlight) != null ? r[t] : void 0) {
                    n.highlight_page($(e)), delete n._to_highlight[t];
                    if ((i = n._selected_result) != null ? i.page_num = t : void 0)return n.select_result(n._selected_result.result_id)
                }
            }), this.add_inline_auth(), this.setup_sections()
        }

        return t.prototype.setup_sections = function () {
            var t = this;
            return this.container.on("click", "[data-first_page]", function (n) {
                var r, i;
                return e("click", "goto_section"), i = $(n.currentTarget), r = i.data("first_page"), document.location.hash = "#page=" + r, t.toolbar.with_lock(function () {
                    return docManager.gotoPage(r)
                }), !1
            })
        }, t.prototype.add_inline_auth = function (e) {
            var t, n, r;
            e == null && (e = Scribd.current_doc.inline_search_auth);
            if (!e)return;
            return t = function () {
                var t;
                t = [];
                for (n in e)r = e[n], t.push($('<input type="hidden" />').attr("name", n).val(r));
                return t
            }(), $('<div class="auth_inputs"></div>').append(t).appendTo(this.form.find(".auth_inputs").remove().end())
        }, t.prototype.render_results = function (e) {
            var t, n, r, i, s, o;
            return this.last_results != null && Scribd.Highlighter.clear(), this.last_results = e, this._results || (this._results = this.container.find(".search_results")), this._results.find(".max_result").text(e.length).end().find(".search_query").text(this.last_query), this._result_template || (this._result_template = _.template($("#tpl_search_result").html())), n = new RegExp(Scribd.escape_regexp(this.last_query), "i"), r = function () {
                var r, o, u;
                u = [];
                for (t = r = 0, o = e.length; r < o; t = ++r)i = e[t], s = _.escape(i.snippet).replace(n, "<strong>$&</strong>"), u.push(this._result_template({page_number: i.pageNum, result_text: s, result_id: t}));
                return u
            }.call(this), this._results.find(".results_list").html(r.join("")), this.container.addClass("has_results").toggleClass("empty_results", !e.length).trigger("scribd:update_panel"), (o = this._curr_result) != null && o.text("0"), this._all_results = this._results.find(".search_result"), this.highlight_all()
        }, t.prototype.select_result = function (e) {
        }, t.prototype.goto_result = function (e) {
            var t;
            return t = this.last_results[e].pageNum, t !== docManager.currentPageNum() && this.toolbar.with_lock(function () {
                return docManager.gotoPage(t)
            }), this.select_result(e), this._curr_result || (this._curr_result = this.container.find(".curr_result")), this._curr_result.text(e + 1)
        }, t.prototype.select_result = function (e) {
            var t, n, r, i, s, o, u;
            i = this.last_results[e], r = i.pageNum, this._all_results.removeClass("active").eq(e).addClass("active"), n = 0;
            while (((s = this.last_results[e - n - 1]) != null ? s.pageNum : void 0) === r)n++;
            return((o = docManager.pages[r]) != null ? o.innerPageElem : void 0) ? (delete this._selected_result, t = "" + Scribd.Highlighter.highlight_class + "_selected", (u = this._previously_highlighted) != null && u.removeClass(t), this._previously_highlighted = $("#outer_page_" + r).find("." + Scribd.Highlighter.highlight_class + ":eq(" + n + ")").addClass(t)) : this._selected_result = {page_num: r, result_id: e}
        }, t.prototype.highlight_all = function () {
            var e, t, n, r, i, s, o;
            this._to_highlight = {}, i = _.uniq(function () {
                var e, n, r, i;
                r = this.last_results, i = [];
                for (e = 0, n = r.length; e < n; e++)t = r[e], i.push(t.pageNum);
                return i
            }.call(this), !0), o = [];
            for (n = 0, r = i.length; n < r; n++)e = i[n], ((s = docManager.pages[e]) != null ? s.innerPageElem : void 0) ? o.push(this.highlight_page($("#outer_page_" + e))) : o.push(this._to_highlight[e] = !0);
            return o
        }, t.prototype.highlight_page = function (e) {
            return Scribd.Highlighter.highlight(e[0], this.last_query)
        }, t
    }()
}).call(this);
