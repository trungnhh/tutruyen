/* :files, 'spec_javascripts', ... @ (none) */


/* :files, 'spec_javascripts', ... @ (none) */


/* :asset_packager_compatibility, 'config/asset_packages.yml' @ 1385158675 */


/* :asset_packager_compatibility, 'config/asset_packages.yml' @ 1385158675 */


/* :files, 'public/javascripts/shared', ... @ (none) */


/* :files, 'app/views', ... @ (none) */


/* :files, 'app/views', ... @ (none) */


/* :class_inlines, 'app/views', ... @ 1385158675 */
/* app/views/login/_archive_login_lightbox.coffee @ 1385158675 */
(function(){Scribd.ArchiveLoginLightbox=function(){function e(e){var t=this;this.set_page(),$(e).on("click","a.skip_login",function(){return Scribd.track_event("login_lightbox","click",""+t.page+":login_lightbox:skip_login")})}return e.prototype.set_page=function(){var e;this.page=window.location.pathname.split("/")[1],this.page||(this.page="home");if(((e=Scribd.current_doc)!=null?e.signup_context:void 0)!=null)return this.page=""+this.page+"_"+Scribd.current_doc.signup_context},e}()}).call(this);


/* app/views/login/checkout_flow_login.coffee @ 1385158675 */
(function(){Scribd.CheckoutFlowLogin=function(){function e(e,t){var n=this;this.container=$(e),this.next_url=t,this.page_name="checkout_flow_login",this.forgot_pw_lb=null,trackEvent("payments","payments/checkout_flow",this.page_name),this.container.dispatch("click",this.page_name,{default_input:function(e,t){var n;n=$(t.target),n.removeClass("greyed");if(n.attr("value")===n.attr("default"))return n.attr("value","")},forgot_password:function(e,t){return n.forgot_pw_lb=Scribd.Lightbox.open("forgot_password_lb",Scribd.ForgotPasswordLightbox,n),!1}}),this.initialize_input_labels(),this.container.on("Scribd:Facebook:login_success",function(e,t){return window.location.href=n.next_url})}return e.prototype.initialize_input_labels=function(){var e,t,n,r,i;r=$(".default_input"),i=[];for(t=0,n=r.length;t<n;t++)e=r[t],e=$(e),e.attr("value")===""?(e.attr("value",e.attr("default")),i.push(e.addClass("greyed"))):i.push(void 0);return i},e.prototype.show=function(){if(this.forgot_pw_lb)return this.forgot_pw_lb.close()},e}()}).call(this);


/* app/views/login/login.coffee @ 1385158675 */
(function(){var e={}.hasOwnProperty,t=function(t,n){function i(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r]);return i.prototype=n.prototype,t.prototype=new i,t.__super__=n.prototype,t};Scribd.Login=function(e){function r(){return r.__super__.constructor.apply(this,arguments)}var n;return t(r,e),n="/images/login/login.jpg?1385158711",r.prototype.initialize=function(){return $(".login_forms").backstretch(n),$(".user_menu").hide()},r}(Backbone.View)}).call(this);
