/* :files, 'spec_javascripts', ... @ (none) */


/* :files, 'spec_javascripts', ... @ (none) */


/* :asset_packager_compatibility, 'config/asset_packages.yml' @ 1384468448 */
/* public/javascripts/jquery.dotdotdot.js @ 1384468448 */
/*	
 *	jQuery dotdotdot 1.5.6
 *	
 *	Copyright (c) 2013 Fred Heusschen
 *	www.frebsite.nl
 *
 *	Plugin website:
 *	dotdotdot.frebsite.nl
 *
 *	Dual licensed under the MIT and GPL licenses.
 *	http://en.wikipedia.org/wiki/MIT_License
 *	http://en.wikipedia.org/wiki/GNU_General_Public_License
 */
(function (e) {
    function n(e, t, n) {
        var r = e.children(), i = !1;
        e.empty();
        for (var o = 0, u = r.length; o < u; o++) {
            var a = r.eq(o);
            e.append(a), n && e.append(n);
            if (s(e, t)) {
                a.remove(), i = !0;
                break
            }
            n && n.remove()
        }
        return i
    }

    function r(t, n, o, u, a) {
        var f = t.contents(), l = !1;
        t.empty();
        var c = "table, thead, tbody, tfoot, tr, col, colgroup, object, embed, param, ol, ul, dl, select, optgroup, option, textarea, script, style";
        for (var h = 0, p = f.length; h < p; h++) {
            if (l)break;
            var d = f[h], v = e(d);
            if (typeof d == "undefined")continue;
            t.append(v), a && t[t.is(c) ? "after" : "append"](a), d.nodeType == 3 ? s(o, u) && (l = i(v, n, o, u, a)) : l = r(v, n, o, u, a), l || a && a.remove()
        }
        return l
    }

    function i(e, t, n, r, u) {
        var l = !1, c = e[0];
        if (typeof c == "undefined")return!1;
        var h = r.wrap == "letter" ? "" : " ", p = f(c).split(h), d = -1, v = -1, m = 0, g = p.length - 1;
        while (m <= g) {
            var y = Math.floor((m + g) / 2);
            if (y == v)break;
            v = y, a(c, p.slice(0, v + 1).join(h) + r.ellipsis), s(n, r) ? g = v : (d = v, m = v)
        }
        if (d == -1 || p.length == 1 && p[0].length == 0) {
            var w = e.parent();
            e.remove();
            var E = u ? u.length : 0;
            if (w.contents().size() > E) {
                var S = w.contents().eq(-1 - E);
                l = i(S, t, n, r, u)
            } else {
                var c = w.prev().contents().eq(-1)[0];
                if (typeof c != "undefined") {
                    var b = o(f(c), r);
                    a(c, b), w.remove(), l = !0
                }
            }
        } else {
            var b = o(p.slice(0, d + 1).join(h), r);
            l = !0, a(c, b)
        }
        return l
    }

    function s(e, t) {
        return e.innerHeight() > t.maxHeight
    }

    function o(t, n) {
        while (e.inArray(t.slice(-1), n.lastCharacter.remove) > -1)t = t.slice(0, -1);
        return e.inArray(t.slice(-1), n.lastCharacter.noEllipsis) < 0 && (t += n.ellipsis), t
    }

    function u(e) {
        return{width: e.innerWidth(), height: e.innerHeight()}
    }

    function a(e, t) {
        e.innerText ? e.innerText = t : e.nodeValue ? e.nodeValue = t : e.textContent && (e.textContent = t)
    }

    function f(e) {
        return e.innerText ? e.innerText : e.nodeValue ? e.nodeValue : e.textContent ? e.textContent : ""
    }

    function l(t, n) {
        return typeof t == "undefined" ? !1 : t ? typeof t == "string" ? (t = e(t, n), t.length ? t : !1) : typeof t == "object" ? typeof t.jquery == "undefined" ? !1 : t : !1 : !1
    }

    function c(e) {
        var t = e.innerHeight(), n = ["paddingTop", "paddingBottom"];
        for (var r = 0, i = n.length; r < i; r++) {
            var s = parseInt(e.css(n[r]), 10);
            isNaN(s) && (s = 0), t -= s
        }
        return t
    }

    function h(e, t) {
        return e ? (typeof t == "string" ? t = "dotdotdot: " + t : t = ["dotdotdot:", t], typeof window.console != "undefined" && typeof window.console.log != "undefined" && window.console.log(t), !1) : !1
    }

    if (e.fn.dotdotdot)return;
    e.fn.dotdotdot = function (i) {
        if (this.length == 0)return h(!0, 'No element found for "' + this.selector + '".'), this;
        if (this.length > 1)return this.each(function () {
            e(this).dotdotdot(i)
        });
        var o = this;
        o.data("dotdotdot") && o.trigger("destroy.dot"), o.bind_events = function () {
            return o.bind("update.dot",function (t, i) {
                t.preventDefault(), t.stopPropagation(), f.maxHeight = typeof f.height == "number" ? f.height : c(o), f.maxHeight += f.tolerance;
                if (typeof i != "undefined") {
                    if (typeof i == "string" || i instanceof HTMLElement)i = e("<div />").append(i).contents();
                    i instanceof e && (a = i)
                }
                m = o.wrapInner('<div class="dotdotdot" />').children(), m.empty().append(a.clone(!0)).css({height: "auto", width: "auto", border: "none", padding: 0, margin: 0});
                var u = !1, l = !1;
                return p.afterElement && (u = p.afterElement.clone(!0), p.afterElement.remove()), s(m, f) && (f.wrap == "children" ? l = n(m, f, u) : l = r(m, o, m, f, u)), m.replaceWith(m.contents()), m = null, e.isFunction(f.callback) && f.callback.call(o[0], l, a), p.isTruncated = l, l
            }).bind("isTruncated.dot",function (e, t) {
                    return e.preventDefault(), e.stopPropagation(), typeof t == "function" && t.call(o[0], p.isTruncated), p.isTruncated
                }).bind("originalContent.dot",function (e, t) {
                    return e.preventDefault(), e.stopPropagation(), typeof t == "function" && t.call(o[0], a), a
                }).bind("destroy.dot", function (e) {
                    e.preventDefault(), e.stopPropagation(), o.unwatch().unbind_events().empty().append(a).data("dotdotdot", !1)
                }), o
        }, o.unbind_events = function () {
            return o.unbind(".dot"), o
        }, o.watch = function () {
            o.unwatch();
            if (f.watch == "window") {
                var t = e(window), n = t.width(), r = t.height();
                t.bind("resize.dot" + p.dotId, function () {
                    if (n != t.width() || r != t.height() || !f.windowResizeFix)n = t.width(), r = t.height(), v && clearInterval(v), v = setTimeout(function () {
                        o.trigger("update.dot")
                    }, 10)
                })
            } else d = u(o), v = setInterval(function () {
                var e = u(o);
                if (d.width != e.width || d.height != e.height)o.trigger("update.dot"), d = u(o)
            }, 100);
            return o
        }, o.unwatch = function () {
            return e(window).unbind("resize.dot" + p.dotId), v && clearInterval(v), o
        };
        var a = o.contents(), f = e.extend(!0, {}, e.fn.dotdotdot.defaults, i), p = {}, d = {}, v = null, m = null;
        return p.afterElement = l(f.after, o), p.isTruncated = !1, p.dotId = t++, o.data("dotdotdot", !0).bind_events().trigger("update.dot"), f.watch && o.watch(), o
    }, e.fn.dotdotdot.defaults = {ellipsis: "... ", wrap: "word", lastCharacter: {remove: [" ", ",", ";", ".", "!", "?"], noEllipsis: []}, tolerance: 0, callback: null, after: null, height: null, watch: !1, windowResizeFix: !0, debug: !1};
    var t = 1, p = e.fn.html;
    e.fn.html = function (e) {
        if (typeof e != "undefined")return this.data("dotdotdot") && typeof e != "function" ? this.trigger("update", [e]) : p.call(this, e);
        return p.call(this)
    };
    var d = e.fn.text;
    e.fn.text = function (t) {
        if (typeof t != "undefined") {
            if (this.data("dotdotdot")) {
                var n = e("<div />");
                return n.text(t), t = n.html(), n.remove(), this.trigger("update", [t])
            }
            return d.call(this, t)
        }
        return d.call(this)
    }
})(jQuery);


/* public/javascripts/jquery.nanoscroller.js @ 1384468448 */
/*! nanoScrollerJS - v0.7.2
 * http://jamesflorentino.github.com/nanoScrollerJS/
 * Copyright (c) 2013 James Florentino; Licensed MIT */
(function (e, t, n) {
    "use strict";
    var r, i, s, o, u, a, f, l, c, h, p, d, v, m, g, y, b, w, E, S, x;
    S = {paneClass: "pane", sliderClass: "slider", contentClass: "content", iOSNativeScrolling: !1, preventPageScrolling: !1, disableResize: !1, alwaysVisible: !1, flashDelay: 1500, sliderMinHeight: 20, sliderMaxHeight: null}, y = "scrollbar", g = "scroll", l = "mousedown", c = "mousemove", p = "mousewheel", h = "mouseup", m = "resize", u = "drag", w = "up", v = "panedown", s = "DOMMouseScroll", o = "down", E = "wheel", a = "keydown", f = "keyup", b = "touchmove", r = t.navigator.appName === "Microsoft Internet Explorer" && /msie 7./i.test(t.navigator.appVersion) && t.ActiveXObject, i = null, x = function () {
        var e, t, r;
        return e = n.createElement("div"), t = e.style, t.position = "absolute", t.width = "100px", t.height = "100px", t.overflow = g, t.top = "-9999px", n.body.appendChild(e), r = e.offsetWidth - e.clientWidth, n.body.removeChild(e), r
    }, d = function () {
        function a(r, s) {
            this.el = r, this.options = s, i || (i = x()), this.$el = e(this.el), this.doc = e(n), this.win = e(t), this.$content = this.$el.children("." + s.contentClass), this.$content.attr("tabindex", 0), this.content = this.$content[0], this.options.iOSNativeScrolling && this.el.style.WebkitOverflowScrolling != null ? this.nativeScrolling() : this.generate(), this.createEvents(), this.addEvents(), this.reset()
        }

        return a.prototype.preventScrolling = function (e, t) {
            if (!this.isActive)return;
            if (e.type === s)(t === o && e.originalEvent.detail > 0 || t === w && e.originalEvent.detail < 0) && e.preventDefault(); else if (e.type === p) {
                if (!e.originalEvent || !e.originalEvent.wheelDelta)return;
                (t === o && e.originalEvent.wheelDelta < 0 || t === w && e.originalEvent.wheelDelta > 0) && e.preventDefault()
            }
        }, a.prototype.nativeScrolling = function () {
            this.$content.css({WebkitOverflowScrolling: "touch"}), this.iOSNativeScrolling = !0, this.isActive = !0
        }, a.prototype.updateScrollValues = function () {
            var e;
            e = this.content, this.maxScrollTop = e.scrollHeight - e.clientHeight, this.contentScrollTop = e.scrollTop, this.iOSNativeScrolling || (this.maxSliderTop = this.paneHeight - this.sliderHeight, this.sliderTop = this.contentScrollTop * this.maxSliderTop / this.maxScrollTop)
        }, a.prototype.createEvents = function () {
            var e = this;
            this.events = {down: function (t) {
                return e.isBeingDragged = !0, e.offsetY = t.pageY - e.slider.offset().top, e.pane.addClass("active"), e.doc.bind(c, e.events[u]).bind(h, e.events[w]), !1
            }, drag: function (t) {
                return e.sliderY = t.pageY - e.$el.offset().top - e.offsetY, e.scroll(), e.updateScrollValues(), e.contentScrollTop >= e.maxScrollTop ? e.$el.trigger("scrollend") : e.contentScrollTop === 0 && e.$el.trigger("scrolltop"), !1
            }, up: function (t) {
                return e.isBeingDragged = !1, e.pane.removeClass("active"), e.doc.unbind(c, e.events[u]).unbind(h, e.events[w]), !1
            }, resize: function (t) {
                e.reset()
            }, panedown: function (t) {
                return e.sliderY = (t.offsetY || t.originalEvent.layerY) - e.sliderHeight * .5, e.scroll(), e.events.down(t), !1
            }, scroll: function (t) {
                if (e.isBeingDragged)return;
                e.updateScrollValues(), e.iOSNativeScrolling || (e.sliderY = e.sliderTop, e.slider.css({top: e.sliderTop}));
                if (t == null)return;
                e.contentScrollTop >= e.maxScrollTop ? (e.options.preventPageScrolling && e.preventScrolling(t, o), e.$el.trigger("scrollend")) : e.contentScrollTop === 0 && (e.options.preventPageScrolling && e.preventScrolling(t, w), e.$el.trigger("scrolltop"))
            }, wheel: function (t) {
                if (t == null)return;
                return e.sliderY += -t.wheelDeltaY || -t.delta, e.scroll(), !1
            }}
        }, a.prototype.addEvents = function () {
            var e;
            this.removeEvents(), e = this.events, this.options.disableResize || this.win.bind(m, e[m]), this.iOSNativeScrolling || (this.slider.bind(l, e[o]), this.pane.bind(l, e[v]).bind("" + p + " " + s, e[E])), this.$content.bind("" + g + " " + p + " " + s + " " + b, e[g])
        }, a.prototype.removeEvents = function () {
            var e;
            e = this.events, this.win.unbind(m, e[m]), this.iOSNativeScrolling || (this.slider.unbind(), this.pane.unbind()), this.$content.unbind("" + g + " " + p + " " + s + " " + b, e[g])
        }, a.prototype.generate = function () {
            var e, t, n, r, s;
            return n = this.options, r = n.paneClass, s = n.sliderClass, e = n.contentClass, !this.$el.find("" + r).length && !this.$el.find("" + s).length && this.$el.append('<div class="' + r + '"><div class="' + s + '" /></div>'), this.pane = this.$el.children("." + r), this.slider = this.pane.find("." + s), i && (t = this.$el.css("direction") === "rtl" ? {left: -i} : {right: -i}, this.$el.addClass("has-scrollbar")), t != null && this.$content.css(t), this
        }, a.prototype.restore = function () {
            this.stopped = !1, this.pane.show(), this.addEvents()
        }, a.prototype.reset = function () {
            var e, t, n, s, o, u, a, f, l;
            if (this.iOSNativeScrolling) {
                this.contentHeight = this.content.scrollHeight;
                return
            }
            return this.$el.find("." + this.options.paneClass).length || this.generate().stop(), this.stopped && this.restore(), e = this.content, n = e.style, s = n.overflowY, r && this.$content.css({height: this.$content.height()}), t = e.scrollHeight + i, u = this.pane.outerHeight(), f = parseInt(this.pane.css("top"), 10), o = parseInt(this.pane.css("bottom"), 10), a = u + f + o, l = Math.round(a / t * a), l < this.options.sliderMinHeight ? l = this.options.sliderMinHeight : this.options.sliderMaxHeight != null && l > this.options.sliderMaxHeight && (l = this.options.sliderMaxHeight), s === g && n.overflowX !== g && (l += i), this.maxSliderTop = a - l, this.contentHeight = t, this.paneHeight = u, this.paneOuterHeight = a, this.sliderHeight = l, this.slider.height(l), this.events.scroll(), this.pane.show(), this.isActive = !0, e.scrollHeight === e.clientHeight || this.pane.outerHeight(!0) >= e.scrollHeight && s !== g ? (this.pane.hide(), this.isActive = !1) : this.el.clientHeight === e.scrollHeight && s === g ? this.slider.hide() : this.slider.show(), this.pane.css({opacity: this.options.alwaysVisible ? 1 : "", visibility: this.options.alwaysVisible ? "visible" : ""}), this
        }, a.prototype.scroll = function () {
            if (!this.isActive)return;
            return this.sliderY = Math.max(0, this.sliderY), this.sliderY = Math.min(this.maxSliderTop, this.sliderY), this.$content.scrollTop((this.paneHeight - this.contentHeight + i) * this.sliderY / this.maxSliderTop * -1), this.iOSNativeScrolling || this.slider.css({top: this.sliderY}), this
        }, a.prototype.scrollBottom = function (e) {
            if (!this.isActive)return;
            return this.reset(), this.$content.scrollTop(this.contentHeight - this.$content.height() - e).trigger(p), this
        }, a.prototype.scrollTop = function (e) {
            if (!this.isActive)return;
            return this.reset(), this.$content.scrollTop(+e).trigger(p), this
        }, a.prototype.scrollTo = function (t) {
            if (!this.isActive)return;
            return this.reset(), this.scrollTop(e(t).get(0).offsetTop), this
        }, a.prototype.stop = function () {
            return this.stopped = !0, this.removeEvents(), this.pane.hide(), this
        }, a.prototype.flash = function () {
            var e = this;
            if (!this.isActive)return;
            return this.reset(), this.pane.addClass("flashed"), setTimeout(function () {
                e.pane.removeClass("flashed")
            }, this.options.flashDelay), this
        }, a
    }(), e.fn.nanoScroller = function (t) {
        return this.each(function () {
            var n, r;
            (r = this.nanoscroller) || (n = e.extend({}, S, t), this.nanoscroller = r = new d(this, n));
            if (t && typeof t == "object") {
                e.extend(r.options, t);
                if (t.scrollBottom)return r.scrollBottom(t.scrollBottom);
                if (t.scrollTop)return r.scrollTop(t.scrollTop);
                if (t.scrollTo)return r.scrollTo(t.scrollTo);
                if (t.scroll === "bottom")return r.scrollBottom(0);
                if (t.scroll === "top")return r.scrollTop(0);
                if (t.scroll && t.scroll instanceof e)return r.scrollTo(t.scroll);
                if (t.stop)return r.stop();
                if (t.flash)return r.flash()
            }
            return r.reset()
        })
    }
})(jQuery, window, document);


/* public/javascripts/jquery.jcarousel.js @ 1384468448 */
/*! jCarousel - v0.3.0-beta.5 - 2013-07-10
 * http://sorgalla.com/jcarousel
 * Copyright (c) 2013 Jan Sorgalla; Licensed MIT */
(function (e) {
    "use strict";
    var t = e.jCarousel = {};
    t.version = "0.3.0-beta.5";
    var n = /^([+\-]=)?(.+)$/;
    t.parseTarget = function (e) {
        var t = !1, r = typeof e != "object" ? n.exec(e) : null;
        return r ? (e = parseInt(r[2], 10) || 0, r[1] && (t = !0, r[1] === "-=" && (e *= -1))) : typeof e != "object" && (e = parseInt(e, 10) || 0), {target: e, relative: t}
    }, t.detectCarousel = function (e) {
        var t;
        while (e.size() > 0) {
            t = e.filter("[data-jcarousel]");
            if (t.size() > 0)return t;
            t = e.find("[data-jcarousel]");
            if (t.size() > 0)return t;
            e = e.parent()
        }
        return null
    }, t.base = function (n) {
        return{version: t.version, _options: {}, _element: null, _carousel: null, _init: e.noop, _create: e.noop, _destroy: e.noop, _reload: e.noop, create: function () {
            return this._element.attr("data-" + n.toLowerCase(), !0).data(n, this), !1 === this._trigger("create") ? this : (this._create(), this._trigger("createend"), this)
        }, destroy: function () {
            return!1 === this._trigger("destroy") ? this : (this._destroy(), this._trigger("destroyend"), this._element.removeData(n).removeAttr("data-" + n.toLowerCase()), this)
        }, reload: function (e) {
            return!1 === this._trigger("reload") ? this : (e && this.options(e), this._reload(), this._trigger("reloadend"), this)
        }, element: function () {
            return this._element
        }, options: function (t, n) {
            if (arguments.length === 0)return e.extend({}, this._options);
            if (typeof t == "string") {
                if (typeof n == "undefined")return typeof this._options[t] == "undefined" ? null : this._options[t];
                this._options[t] = n
            } else this._options = e.extend({}, this._options, t);
            return this
        }, carousel: function () {
            return this._carousel || (this._carousel = t.detectCarousel(this.options("carousel") || this._element), this._carousel || e.error('Could not detect carousel for plugin "' + n + '"')), this._carousel
        }, _trigger: function (t, r, i) {
            var s, o = !1;
            return i = [this].concat(i || []), (r || this._element).each(function () {
                s = e.Event((t + "." + n).toLowerCase()), e(this).trigger(s, i), s.isDefaultPrevented() && (o = !0)
            }), !o
        }}
    }, t.plugin = function (n, r) {
        var i = e[n] = function (t, n) {
            this._element = e(t), this.options(n), this._init(), this.create()
        };
        return i.fn = i.prototype = e.extend({}, t.base(n), r), e.fn[n] = function (t) {
            var r = Array.prototype.slice.call(arguments, 1), s = this;
            return typeof t == "string" ? this.each(function () {
                var i = e(this).data(n);
                if (!i)return e.error("Cannot call methods on " + n + " prior to initialization; " + 'attempted to call method "' + t + '"');
                if (!e.isFunction(i[t]) || t.charAt(0) === "_")return e.error('No such method "' + t + '" for ' + n + " instance");
                var o = i[t].apply(i, r);
                if (o !== i && typeof o != "undefined")return s = o, !1
            }) : this.each(function () {
                var r = e(this).data(n);
                r instanceof i ? r.reload(t) : new i(this, t)
            }), s
        }, i
    }
})(jQuery), function (e, t) {
    "use strict";
    var n = function (e) {
        return parseFloat(e) || 0
    };
    e.jCarousel.plugin("jcarousel", {animating: !1, tail: 0, inTail: !1, resizeTimer: null, lt: null, vertical: !1, rtl: !1, circular: !1, underflow: !1, _options: {list: function () {
        return this.element().children().eq(0)
    }, items: function () {
        return this.list().children()
    }, animation: 400, transitions: !1, wrap: null, vertical: null, rtl: null, center: !1}, _list: null, _items: null, _target: null, _first: null, _last: null, _visible: null, _fullyvisible: null, _init: function () {
        var e = this;
        return this.onWindowResize = function () {
            e.resizeTimer && clearTimeout(e.resizeTimer), e.resizeTimer = setTimeout(function () {
                e.reload()
            }, 100)
        }, this
    }, _create: function () {
        this._reload(), e(t).on("resize.jcarousel", this.onWindowResize)
    }, _destroy: function () {
        e(t).off("resize.jcarousel", this.onWindowResize)
    }, _reload: function () {
        this.vertical = this.options("vertical"), this.vertical == null && (this.vertical = this.list().height() > this.list().width()), this.rtl = this.options("rtl"), this.rtl == null && (this.rtl = function (t) {
            if (("" + t.attr("dir")).toLowerCase() === "rtl")return!0;
            var n = !1;
            return t.parents("[dir]").each(function () {
                if (/rtl/i.test(e(this).attr("dir")))return n = !0, !1
            }), n
        }(this._element)), this.lt = this.vertical ? "top" : "left", this._items = null;
        var t = this._target && this.index(this._target) >= 0 ? this._target : this.closest();
        this.circular = this.options("wrap") === "circular", this.underflow = !1;
        var n = {left: 0, top: 0};
        return t.size() > 0 && (this._prepare(t), this.list().find("[data-jcarousel-clone]").remove(), this._items = null, this.underflow = this._fullyvisible.size() >= this.items().size(), this.circular = this.circular && !this.underflow, n[this.lt] = this._position(t) + "px"), this.move(n), this
    }, list: function () {
        if (this._list === null) {
            var t = this.options("list");
            this._list = e.isFunction(t) ? t.call(this) : this._element.find(t)
        }
        return this._list
    }, items: function () {
        if (this._items === null) {
            var t = this.options("items");
            this._items = (e.isFunction(t) ? t.call(this) : this.list().find(t)).not("[data-jcarousel-clone]")
        }
        return this._items
    }, index: function (e) {
        return this.items().index(e)
    }, closest: function () {
        var t = this, r = this.list().position()[this.lt], i = e(), s = !1, o = this.vertical ? "bottom" : this.rtl ? "left" : "right", u;
        return this.rtl && !this.vertical && (r = (r + this.list().width() - this.clipping()) * -1), this.items().each(function () {
            i = e(this);
            if (s)return!1;
            var a = t.dimension(i);
            r += a;
            if (r >= 0) {
                u = a - n(i.css("margin-" + o));
                if (!(Math.abs(r) - a + u / 2 <= 0))return!1;
                s = !0
            }
        }), i
    }, target: function () {
        return this._target
    }, first: function () {
        return this._first
    }, last: function () {
        return this._last
    }, visible: function () {
        return this._visible
    }, fullyvisible: function () {
        return this._fullyvisible
    }, hasNext: function () {
        if (!1 === this._trigger("hasnext"))return!0;
        var e = this.options("wrap"), t = this.items().size() - 1;
        return t >= 0 && (e && e !== "first" || this.index(this._last) < t || this.tail && !this.inTail) ? !0 : !1
    }, hasPrev: function () {
        if (!1 === this._trigger("hasprev"))return!0;
        var e = this.options("wrap");
        return this.items().size() > 0 && (e && e !== "last" || this.index(this._first) > 0 || this.tail && this.inTail) ? !0 : !1
    }, clipping: function () {
        return this._element["inner" + (this.vertical ? "Height" : "Width")]()
    }, dimension: function (e) {
        return e["outer" + (this.vertical ? "Height" : "Width")](!0)
    }, scroll: function (t, r, i) {
        if (this.animating)return this;
        if (!1 === this._trigger("scroll", null, [t, r]))return this;
        e.isFunction(r) && (i = r, r = !0);
        var s = e.jCarousel.parseTarget(t);
        if (s.relative) {
            var o = this.items().size() - 1, u = Math.abs(s.target), a = this.options("wrap"), f, l, c, h, p, d, v, m;
            if (s.target > 0) {
                var g = this.index(this._last);
                if (g >= o && this.tail)this.inTail ? a === "both" || a === "last" ? this._scroll(0, r, i) : this._scroll(Math.min(this.index(this._target) + u, o), r, i) : this._scrollTail(r, i); else {
                    f = this.index(this._target);
                    if (this.underflow && f === o && (a === "circular" || a === "both" || a === "last") || !this.underflow && g === o && (a === "both" || a === "last"))this._scroll(0, r, i); else {
                        c = f + u;
                        if (this.circular && c > o) {
                            m = o, p = this.items().get(-1);
                            while (m++ < c)p = this.items().eq(0), d = this._visible.index(p) >= 0, d && p.after(p.clone(!0).attr("data-jcarousel-clone", !0)), this.list().append(p), d || (v = {}, v[this.lt] = this.dimension(p) * (this.rtl ? -1 : 1), this.moveBy(v)), this._items = null;
                            this._scroll(p, r, i)
                        } else this._scroll(Math.min(c, o), r, i)
                    }
                }
            } else if (this.inTail)this._scroll(Math.max(this.index(this._first) - u + 1, 0), r, i); else {
                l = this.index(this._first), f = this.index(this._target), h = this.underflow ? f : l, c = h - u;
                if (h <= 0 && (this.underflow && a === "circular" || a === "both" || a === "first"))this._scroll(o, r, i); else if (this.circular && c < 0) {
                    m = c, p = this.items().get(0);
                    while (m++ < 0) {
                        p = this.items().eq(-1), d = this._visible.index(p) >= 0, d && p.after(p.clone(!0).attr("data-jcarousel-clone", !0)), this.list().prepend(p), this._items = null;
                        var y = n(this.list().position()[this.lt]), b = this.dimension(p);
                        this.rtl && !this.vertical ? y += b : y -= b, v = {}, v[this.lt] = y + "px", this.move(v)
                    }
                    this._scroll(p, r, i)
                } else this._scroll(Math.max(c, 0), r, i)
            }
        } else this._scroll(s.target, r, i);
        return this._trigger("scrollend"), this
    }, moveBy: function (e, t) {
        var r = this.list().position();
        return e.left && (e.left = r.left + n(e.left) + "px"), e.top && (e.top = r.top + n(e.top) + "px"), this.move(e, t)
    }, move: function (t, n) {
        n = n || {};
        var r = this.options("transitions"), i = !!r, s = !!r.transforms, o = !!r.transforms3d, u = n.duration || 0, a = this.list();
        if (!i && u > 0) {
            a.animate(t, n);
            return
        }
        var f = n.complete || e.noop, l = {};
        if (i) {
            var c = a.css(["transitionDuration", "transitionTimingFunction", "transitionProperty"]), h = f;
            f = function () {
                e(this).css(c), h.call(this)
            }, l = {transitionDuration: (u > 0 ? u / 1e3 : 0) + "s", transitionTimingFunction: r.easing || n.easing, transitionProperty: u > 0 ? function () {
                return s || o ? "all" : t.left ? "left" : "top"
            }() : "none", transform: "none"}
        }
        o ? l.transform = "translate3d(" + (t.left || 0) + "," + (t.top || 0) + ",0)" : s ? l.transform = "translate(" + (t.left || 0) + "," + (t.top || 0) + ")" : e.extend(l, t), i && u > 0 && a.one("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd", f), a.css(l), u <= 0 && a.each(function () {
            f.call(this)
        })
    }, _scroll: function (t, r, i) {
        if (this.animating)return e.isFunction(i) && i.call(this, !1), this;
        typeof t != "object" ? t = this.items().eq(t) : typeof t.jquery == "undefined" && (t = e(t));
        if (t.size() === 0)return e.isFunction(i) && i.call(this, !1), this;
        this.inTail = !1, this._prepare(t);
        var s = this._position(t), o = n(this.list().position()[this.lt]);
        if (s === o)return e.isFunction(i) && i.call(this, !1), this;
        var u = {};
        return u[this.lt] = s + "px", this._animate(u, r, i), this
    }, _scrollTail: function (t, n) {
        if (this.animating || !this.tail)return e.isFunction(n) && n.call(this, !1), this;
        var r = this.list().position()[this.lt];
        this.rtl ? r += this.tail : r -= this.tail, this.inTail = !0;
        var i = {};
        return i[this.lt] = r + "px", this._update({target: this._target.next(), fullyvisible: this._fullyvisible.slice(1).add(this._visible.last())}), this._animate(i, t, n), this
    }, _animate: function (t, n, r) {
        r = r || e.noop;
        if (!1 === this._trigger("animate"))return r.call(this, !1), this;
        this.animating = !0;
        var i = this.options("animation"), s = e.proxy(function () {
            this.animating = !1;
            var e = this.list().find("[data-jcarousel-clone]");
            e.size() > 0 && (e.remove(), this._reload()), this._trigger("animateend"), r.call(this, !0)
        }, this), o = typeof i == "object" ? e.extend({}, i) : {duration: i}, u = o.complete || e.noop;
        return n === !1 ? o.duration = 0 : typeof e.fx.speeds[o.duration] != "undefined" && (o.duration = e.fx.speeds[o.duration]), o.complete = function () {
            s(), u.call(this)
        }, this.move(t, o), this
    }, _prepare: function (t) {
        var r = this.index(t), i = r, s = this.dimension(t), o = this.clipping(), u = this.vertical ? "bottom" : this.rtl ? "left" : "right", a = this.options("center"), f = {target: t, first: t, last: t, visible: t, fullyvisible: s <= o ? t : e()}, l, c, h;
        a && (s /= 2, o /= 2);
        if (s < o)for (; ;) {
            l = this.items().eq(++i);
            if (l.size() === 0) {
                if (!this.circular)break;
                l = this.items().eq(0);
                if (t.get(0) === l.get(0))break;
                c = this._visible.index(l) >= 0, c && l.after(l.clone(!0).attr("data-jcarousel-clone", !0)), this.list().append(l);
                if (!c) {
                    var p = {};
                    p[this.lt] = this.dimension(l) * (this.rtl ? -1 : 1), this.moveBy(p)
                }
                this._items = null
            }
            s += this.dimension(l), f.last = l, f.visible = f.visible.add(l), h = n(l.css("margin-" + u)), s - h <= o && (f.fullyvisible = f.fullyvisible.add(l));
            if (s >= o)break
        }
        if (!this.circular && !a && s < o) {
            i = r;
            for (; ;) {
                if (--i < 0)break;
                l = this.items().eq(i);
                if (l.size() === 0)break;
                s += this.dimension(l), f.first = l, f.visible = f.visible.add(l), h = n(l.css("margin-" + u)), s - h <= o && (f.fullyvisible = f.fullyvisible.add(l));
                if (s >= o)break
            }
        }
        return this._update(f), this.tail = 0, !a && this.options("wrap") !== "circular" && this.options("wrap") !== "custom" && this.index(f.last) === this.items().size() - 1 && (s -= n(f.last.css("margin-" + u)), s > o && (this.tail = s - o)), this
    }, _position: function (e) {
        var t = this._first, n = t.position()[this.lt], r = this.options("center"), i = r ? this.clipping() / 2 - this.dimension(t) / 2 : 0;
        return this.rtl && !this.vertical ? (n -= this.clipping() - this.dimension(t), n += i) : n -= i, !r && (this.index(e) > this.index(t) || this.inTail) && this.tail ? (n = this.rtl ? n - this.tail : n + this.tail, this.inTail = !0) : this.inTail = !1, -n
    }, _update: function (t) {
        var n = this, r = {target: this._target || e(), first: this._first || e(), last: this._last || e(), visible: this._visible || e(), fullyvisible: this._fullyvisible || e()}, i = this.index(t.first || r.first) < this.index(r.first), s, o = function (s) {
            var o = [], u = [];
            t[s].each(function () {
                r[s].index(this) < 0 && o.push(this)
            }), r[s].each(function () {
                t[s].index(this) < 0 && u.push(this)
            }), i ? o = o.reverse() : u = u.reverse(), n._trigger("item" + s + "in", e(o)), n._trigger("item" + s + "out", e(u)), n["_" + s] = t[s]
        };
        for (s in t)o(s);
        return this
    }})
}(jQuery, window), function (e) {
    "use strict";
    e.jcarousel.fn.scrollIntoView = function (t, n, r) {
        var i = e.jCarousel.parseTarget(t), s = this.index(this._fullyvisible.first()), o = this.index(this._fullyvisible.last()), u;
        i.relative ? u = i.target < 0 ? Math.max(0, s + i.target) : o + i.target : u = typeof i.target != "object" ? i.target : this.index(i.target);
        if (u < s)return this.scroll(u, n, r);
        if (u >= s && u <= o)return e.isFunction(r) && r.call(this, !1), this;
        var a = this.items(), f = this.clipping(), l = this.vertical ? "bottom" : this.rtl ? "left" : "right", c = 0, h;
        for (; ;) {
            h = a.eq(u);
            if (h.size() === 0)break;
            c += this.dimension(h);
            if (c >= f) {
                var p = parseFloat(h.css("margin-" + l)) || 0;
                c - p !== f && u++;
                break
            }
            if (u <= 0)break;
            u--
        }
        return this.scroll(u, n, r)
    }
}(jQuery), function (e) {
    "use strict";
    e.jCarousel.plugin("jcarouselControl", {_options: {target: "+=1", event: "click", method: "scroll"}, _active: null, _init: function () {
        this.onDestroy = e.proxy(function () {
            this._destroy(), this.carousel().one("createend.jcarousel", e.proxy(this._create, this))
        }, this), this.onReload = e.proxy(this._reload, this), this.onEvent = e.proxy(function (t) {
            t.preventDefault();
            var n = this.options("method");
            e.isFunction(n) ? n.call(this) : this.carousel().jcarousel(this.options("method"), this.options("target"))
        }, this)
    }, _create: function () {
        this.carousel().one("destroy.jcarousel", this.onDestroy).on("reloadend.jcarousel scrollend.jcarousel", this.onReload), this._element.on(this.options("event") + ".jcarouselcontrol", this.onEvent), this._reload()
    }, _destroy: function () {
        this._element.off(".jcarouselcontrol", this.onEvent), this.carousel().off("destroy.jcarousel", this.onDestroy).off("reloadend.jcarousel scrollend.jcarousel", this.onReload)
    }, _reload: function () {
        var t = e.jCarousel.parseTarget(this.options("target")), n = this.carousel(), r;
        if (t.relative)r = n.jcarousel(t.target > 0 ? "hasNext" : "hasPrev"); else {
            var i = typeof t.target != "object" ? n.jcarousel("items").eq(t.target) : t.target;
            r = n.jcarousel("target").index(i) >= 0
        }
        return this._active !== r && (this._trigger(r ? "active" : "inactive"), this._active = r), this
    }})
}(jQuery), function (e) {
    "use strict";
    e.jCarousel.plugin("jcarouselPagination", {_options: {perPage: null, item: function (e) {
        return'<a href="#' + e + '">' + e + "</a>"
    }, event: "click", method: "scroll"}, _pages: {}, _items: {}, _currentPage: null, _init: function () {
        this.onDestroy = e.proxy(function () {
            this._destroy(), this.carousel().one("createend.jcarousel", e.proxy(this._create, this))
        }, this), this.onReload = e.proxy(this._reload, this), this.onScroll = e.proxy(this._update, this)
    }, _create: function () {
        this.carousel().one("destroy.jcarousel", this.onDestroy).on("reloadend.jcarousel", this.onReload).on("scrollend.jcarousel", this.onScroll), this._reload()
    }, _destroy: function () {
        this._clear(), this.carousel().off("destroy.jcarousel", this.onDestroy).off("reloadend.jcarousel", this.onReload).off("scrollend.jcarousel", this.onScroll)
    }, _reload: function () {
        var t = this.options("perPage");
        this._pages = {}, this._items = {}, e.isFunction(t) && (t = t.call(this));
        if (t == null)this._pages = this._calculatePages(); else {
            var n = parseInt(t, 10) || 0, r = this.carousel().jcarousel("items"), i = 1, s = 0, o;
            for (; ;) {
                o = r.eq(s++);
                if (o.size() === 0)break;
                this._pages[i] ? this._pages[i] = this._pages[i].add(o) : this._pages[i] = o, s % n === 0 && i++
            }
        }
        this._clear();
        var u = this, a = this.carousel().data("jcarousel"), f = this._element, l = this.options("item");
        e.each(this._pages, function (t, n) {
            var r = u._items[t] = e(l.call(u, t, n));
            r.on(u.options("event") + ".jcarouselpagination", e.proxy(function () {
                var e = n.eq(0);
                if (a.circular) {
                    var r = a.index(a.target()), i = a.index(e);
                    parseFloat(t) > parseFloat(u._currentPage) ? i < r && (e = "+=" + (a.items().size() - r + i)) : i > r && (e = "-=" + (r + (a.items().size() - i)))
                }
                a[this.options("method")](e)
            }, u)), f.append(r)
        }), this._update()
    }, _update: function () {
        var t = this.carousel().jcarousel("target"), n;
        e.each(this._pages, function (e, r) {
            r.each(function () {
                if (t.is(this))return n = e, !1
            });
            if (n)return!1
        }), this._currentPage !== n && (this._trigger("inactive", this._items[this._currentPage]), this._trigger("active", this._items[n])), this._currentPage = n
    }, items: function () {
        return this._items
    }, _clear: function () {
        this._element.empty(), this._currentPage = null
    }, _calculatePages: function () {
        var e = this.carousel().data("jcarousel"), t = e.items(), n = e.clipping(), r = 0, i = 0, s = 1, o = {}, u;
        for (; ;) {
            u = t.eq(i++);
            if (u.size() === 0)break;
            o[s] ? o[s] = o[s].add(u) : o[s] = u, r += e.dimension(u), r >= n && (s++, r = 0)
        }
        return o
    }})
}(jQuery), function (e) {
    "use strict";
    e.jCarousel.plugin("jcarouselAutoscroll", {_options: {target: "+=1", interval: 3e3, autostart: !0}, _timer: null, _init: function () {
        this.onDestroy = e.proxy(function () {
            this._destroy(), this.carousel().one("createend.jcarousel", e.proxy(this._create, this))
        }, this), this.onAnimateEnd = e.proxy(this.start, this)
    }, _create: function () {
        this.carousel().one("destroy.jcarousel", this.onDestroy), this.options("autostart") && this.start()
    }, _destroy: function () {
        this.stop(), this.carousel().off("destroy.jcarousel", this.onDestroy)
    }, start: function () {
        return this.stop(), this.carousel().one("animateend.jcarousel", this.onAnimateEnd), this._timer = setTimeout(e.proxy(function () {
            this.carousel().jcarousel("scroll", this.options("target"))
        }, this), this.options("interval")), this
    }, stop: function () {
        return this._timer && (this._timer = clearTimeout(this._timer)), this.carousel().off("animateend.jcarousel", this.onAnimateEnd), this
    }})
}(jQuery);


/* public/javascripts/jquery.touchwipe.1.1.1.js @ 1384468448 */
/**
 * jQuery Plugin to obtain touch gestures from iPhone, iPod Touch and iPad, should also work with Android mobile phones (not tested yet!)
 * Common usage: wipe images (left and right to show the previous or next image)
 *
 * @author Andreas Waltl, netCU Internetagentur (http://www.netcu.de)
 * @version 1.1.1 (9th December 2010) - fix bug (older IE's had problems)
 * @version 1.1 (1st September 2010) - support wipe up and wipe down
 * @version 1.0 (15th July 2010)
 */
(function (e) {
    e.fn.touchwipe = function (t) {
        var n = {min_move_x: 20, min_move_y: 20, wipeLeft: function () {
        }, wipeRight: function () {
        }, wipeUp: function () {
        }, wipeDown: function () {
        }, preventDefaultEvents: !0};
        return t && e.extend(n, t), this.each(function () {
            function i() {
                this.removeEventListener("touchmove", s), e = null, r = !1
            }

            function s(s) {
                n.preventDefaultEvents && s.preventDefault();
                if (r) {
                    var o = s.touches[0].pageX, u = s.touches[0].pageY, a = e - o, f = t - u;
                    Math.abs(a) >= n.min_move_x ? (i(), a > 0 ? n.wipeLeft() : n.wipeRight()) : Math.abs(f) >= n.min_move_y && (i(), f > 0 ? n.wipeDown() : n.wipeUp())
                }
            }

            function o(n) {
                n.touches.length == 1 && (e = n.touches[0].pageX, t = n.touches[0].pageY, r = !0, this.addEventListener("touchmove", s, !1))
            }

            var e, t, r = !1;
            try {
                this.addEventListener("touchstart", o, !1)
            } catch (u) {
                console.log("Touch start events are not supported in this Browser/OS")
            }
        }), this
    }
})(jQuery);


/* public/javascripts/jquery.mousewheel.js @ 1384468448 */
/*! Copyright (c) 2013 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.1.3
 *
 * Requires: 1.2.2+
 */
(function (e) {
    typeof define == "function" && define.amd ? define(["jquery"], e) : typeof exports == "object" ? module.exports = e : e(jQuery)
})(function (e) {
    function o(t) {
        var n = t || window.event, s = [].slice.call(arguments, 1), o = 0, u = 0, a = 0, f = 0, l = 0, c;
        t = e.event.fix(n), t.type = "mousewheel", n.wheelDelta && (o = n.wheelDelta), n.detail && (o = n.detail * -1), n.deltaY && (a = n.deltaY * -1, o = a), n.deltaX && (u = n.deltaX, o = u * -1), n.wheelDeltaY !== undefined && (a = n.wheelDeltaY), n.wheelDeltaX !== undefined && (u = n.wheelDeltaX * -1), f = Math.abs(o);
        if (!r || f < r)r = f;
        l = Math.max(Math.abs(a), Math.abs(u));
        if (!i || l < i)i = l;
        return c = o > 0 ? "floor" : "ceil", o = Math[c](o / r), u = Math[c](u / i), a = Math[c](a / i), s.unshift(t, o, u, a), (e.event.dispatch || e.event.handle).apply(this, s)
    }

    var t = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"], n = "onwheel"in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"], r, i;
    if (e.event.fixHooks)for (var s = t.length; s;)e.event.fixHooks[t[--s]] = e.event.mouseHooks;
    e.event.special.mousewheel = {setup: function () {
        if (this.addEventListener)for (var e = n.length; e;)this.addEventListener(n[--e], o, !1); else this.onmousewheel = o
    }, teardown: function () {
        if (this.removeEventListener)for (var e = n.length; e;)this.removeEventListener(n[--e], o, !1); else this.onmousewheel = null
    }}, e.fn.extend({mousewheel: function (e) {
        return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
    }, unmousewheel: function (e) {
        return this.unbind("mousewheel", e)
    }})
});


/* public/javascripts/modernizr.detect_transition.js @ 1384468448 */
/* Modernizr 2.6.2 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-csstransitions-testprop-testallprops-domprefixes
 */
window.Modernizr = function (e, t, n) {
    function r(e) {
        v.cssText = e
    }

    function i(e, t) {
        return r(prefixes.join(e + ";") + (t || ""))
    }

    function s(e, t) {
        return typeof e === t
    }

    function o(e, t) {
        return!!~("" + e).indexOf(t)
    }

    function u(e, t) {
        for (var r in e) {
            var i = e[r];
            if (!o(i, "-") && v[i] !== n)return t == "pfx" ? i : !0
        }
        return!1
    }

    function a(e, t, r) {
        for (var i in e) {
            var o = t[e[i]];
            if (o !== n)return r === !1 ? e[i] : s(o, "function") ? o.bind(r || t) : o
        }
        return!1
    }

    function f(e, t, n) {
        var r = e.charAt(0).toUpperCase() + e.slice(1), i = (e + " " + b.join(r + " ") + r).split(" ");
        return s(t, "string") || s(t, "undefined") ? u(i, t) : (i = (e + " " + w.join(r + " ") + r).split(" "), a(i, t, n))
    }

    var l = "2.6.2", c = {}, h = t.documentElement, p = "modernizr", d = t.createElement(p), v = d.style, m, g = {}.toString, y = "Webkit Moz O ms", b = y.split(" "), w = y.toLowerCase().split(" "), E = {}, S = {}, x = {}, T = [], N = T.slice, C, k = {}.hasOwnProperty, L;
    !s(k, "undefined") && !s(k.call, "undefined") ? L = function (e, t) {
        return k.call(e, t)
    } : L = function (e, t) {
        return t in e && s(e.constructor.prototype[t], "undefined")
    }, Function.prototype.bind || (Function.prototype.bind = function (e) {
        var t = this;
        if (typeof t != "function")throw new TypeError;
        var n = N.call(arguments, 1), r = function () {
            if (this instanceof r) {
                var i = function () {
                };
                i.prototype = t.prototype;
                var s = new i, o = t.apply(s, n.concat(N.call(arguments)));
                return Object(o) === o ? o : s
            }
            return t.apply(e, n.concat(N.call(arguments)))
        };
        return r
    }), E.csstransitions = function () {
        return f("transition")
    };
    for (var A in E)L(E, A) && (C = A.toLowerCase(), c[C] = E[A](), T.push((c[C] ? "" : "no-") + C));
    return c.addTest = function (e, t) {
        if (typeof e == "object")for (var r in e)L(e, r) && c.addTest(r, e[r]); else {
            e = e.toLowerCase();
            if (c[e] !== n)return c;
            t = typeof t == "function" ? t() : t, typeof enableClasses != "undefined" && enableClasses && (h.className += " " + (t ? "" : "no-") + e), c[e] = t
        }
        return c
    }, r(""), d = m = null, c._version = l, c._domPrefixes = w, c._cssomPrefixes = b, c.testProp = function (e) {
        return u([e])
    }, c.testAllProps = f, c
}(this, this.document);


/* :asset_packager_compatibility, 'config/asset_packages.yml' @ 1384468448 */
/* public/javascripts/newhome/sliding.coffee @ 1384468448 */
(function () {
    Scribd.UI || (Scribd.UI = {}), Scribd.UI.PageDots = function () {
        function e(e, t) {
            this.total_pages = e, this.per_page = t != null ? t : 1, this.container = $(this.template).css({width: this.total_pages * this.dot_size * this.per_page + "px"}), this.fill = this.container.find(".fill"), this.current_page = 1, this.update_fill()
        }

        return e.prototype.dot_size = 12, e.prototype.template = '<div class="page_picker">\n  <div class="fill"></div>\n  <div class="dots"></div>\n</div>', e.prototype.update_fill = function () {
            return this.fill.stop().animate({left: (this.current_page - 1) * this.dot_size * this.per_page + "px", width: this.per_page * this.dot_size + "px"}, "fast")
        }, e.prototype.goto_page = function (e) {
            return this.current_page = e, this.update_fill()
        }, e.prototype.is_first = function () {
            return this.current_page === 1
        }, e.prototype.is_last = function () {
            return this.current_page === this.total_pages
        }, e
    }(), Scribd.UI.SlidingList = function () {
        function e(e) {
            this.container = $(e), this.container.data("list", this), this.sliding = this.container.find(".sliding_content"), this.list_content = this.container.find(".list_content"), this.list_content.height(this.sliding.children(":first").height()), this.page = 1, this.calculate_pages(), this.num_pages > 1 && this.PageDots && (this.dots = new this.PageDots(this.num_pages), this.container.find(".list_dots").append(this.dots.container)), this.goto_page(1)
        }

        return e.prototype.speed = "fast", e.prototype.PageDots = Scribd.UI.PageDots, e.prototype.calculate_pages = function () {
            var e, t, n, r, i;
            t = 0, i = this.sliding.children();
            for (n = 0, r = i.length; n < r; n++)e = i[n], t += $(e).outerWidth(!0);
            return this.sliding.width(t), this.page_width = this.sliding.parent().width(), this.num_pages = Math.ceil(t / this.page_width)
        }, e.prototype.page_left = function () {
            return this.goto_page((this.page - 2 + this.num_pages) % this.num_pages + 1)
        }, e.prototype.page_right = function () {
            return this.goto_page(this.page % this.num_pages + 1)
        }, e.prototype.page_offset = function (e) {
            return this.page_width * (e - 1)
        }, e.prototype.goto_page = function (e) {
            var t, n;
            return t = Math.max(1, Math.min(this.num_pages, e)), this.sliding.animate({marginLeft: -this.page_offset(t)}, this.speed), (n = this.dots) != null && n.goto_page(t), this.page = t, this.container.toggleClass("on_first_page", t === 1).toggleClass("on_last_page", t === this.num_pages)
        }, e
    }()
}).call(this);


/* public/javascripts/jquery_global/sticky.coffee @ 1384468448 */
(function () {
    $.fn.sticky_container = function (e) {
        var t, n, r, i, s, o, u, a, f, l, c, h, p, d, v, m, g = this;
        return e == null && (e = {}), f = this.offset(), m = $(window), a = e.padding != null ? e.padding : 0, h = e.sticky_class || "stuck", s = e.bottom_class || "bottomed", c = e.sticky_callback, l = e.static_callback, n = e.before_stick, i = e.before_unstick, t = e.before_bottom, r = e.before_unbottom, e.unstick_after && (v = $(e.unstick_after)), e.dont_override_elem && (u = $(e.dont_override_elem)), p = !1, o = !1, d = function () {
            var e, d, y, b, E, S;
            return S = m.scrollTop(), E = S > f.top - a, v && (E ? (e = v.offset().top + v.height(), S + m.height() > e ? (E = !1, o || (typeof t == "function" && t(g), o = !0, s && g.addClass(s))) : o && (typeof r == "function" && r(g), o = !1, s && g.removeClass(s))) : o && (typeof r == "function" && r(g), o = !1, s && g.removeClass(s))), u && E && (b = u.offset().top, d = S + g.height() - b, d > 0 ? g.css("top", -d + 38) : g.css("top", 38)), y = !1, E ? (p || (typeof n == "function" && n(g), g.addClass(h), p = !0, y = !0), typeof c == "function" && c(g, y)) : (p && (typeof i == "function" && i(g), g.removeClass(h), p = !1, y = !0), typeof l == "function" && l(g, y)), void 0
        }, this.on("Scribd:update_root", function () {
            return f = g.offset()
        }), this.on("Scribd:restick", d), m.on("scroll resize", d), this
    }
}).call(this);


/* public/javascripts/jquery_global/layover.coffee @ 1384468448 */
(function () {
    $.fn.layover = function (e) {
        var t, n, r, i, s, o, u = this;
        e == null && (e = {}), this.data("has_layover") && console.log("Warning!");
        if (this.length === 0) {
            console.log("Layover cannot initialize without an html element");
            return
        }
        return this.data("has_layover", !0), i = $("<div>"), i.addClass("layover"), i.data("object", this.first()), console.log("layover init"), n = 20, e.border_margin != null && (n = e.border_margin), r = $("<div>"), r.addClass("bordered").hide(), i.append(r), $(document.body).append(i), s = (e.layover_margin || 10) + n, (e.title || e.bold_title) && r.append("<div class='layover_title'><span>" + (e.bold_title || "") + "</span>" + e.title + "</div>"), e.content && r.append("<div class='layover_content'>\n  <h1> " + e.content.title + " </h1>\n  <p> " + e.content.text + " </p>\n</div>"), o = {width: this.width() + s, height: this.height() + s, left: this.offset().left - n, top: this.offset().top - s}, e.width && (o.width = e.width), e.height && (o.height = e.height), e.fullscreen_width && (o.width = $(window).width() - 10, o.left = this.offset().left, o.top = Math.floor(this.offset().top + (e.fullscreen_offset_y || 0))), e.effect === "fadeIn" ? (i.hide(), i.height(o.height), i.width(o.width), i.css("top", o.top), i.css("left", o.left), i.fadeIn(function () {
            r.height(o.height - n), r.width(o.width - n), r.fadeIn();
            if (e.animate_callback)return e.animate_callback.apply()
        })) : (i.css("height", 0), i.css("width", 0), i.css("top", this.offset().top + (this.height() - s) / 2), i.css("left", this.offset().left + (this.width() - s) / 2), i.animate(o, function () {
        }), r.height(o.height - n), r.width(o.width - n), r.fadeIn(), e.animate_callback && e.animate_callback.apply()), i.on("click", function () {
            return e.callback ? e.callback.apply() : i.fadeOut(function () {
                return i.remove()
            })
        }), t = function () {
            return e.fullscreen_width && (o.width = $(window).width() - 10, o.left = u.offset().left, o.top = u.offset().top), i.css(o), r.height(o.height - n), r.width(o.width - n)
        }, $(window).on("resize", t), i
    }
}).call(this);


/* public/javascripts/jquery_global/star_ratings.coffee @ 1384468448 */
(function () {
    $.fn.star_ratings = function (e) {
        var t, n, r, i, s, o, u, a, f, l = this;
        e == null && (e = {}), n = ["strong_lit", "light_lit"], this.setValue = function (e, t) {
            return t == null && (t = "light_lit"), l.container.data("value", e), l.container.find("span").removeClass(t), l.container.find("span").slice(0, e).addClass(t)
        }, t = 5, $(this).addClass("star_ratings"), this.container = $(this), this.container.addClass("rating");
        if (e.setValue) {
            if (this.container.data("star_ratings")) {
                this.setValue(e.setValue);
                return
            }
            e.defaultValue = e.setValue
        }
        r = $(this.find(".icon-feature")).length;
        for (s = f = r; r <= t ? f < t : f > t; s = r <= t ? ++f : --f)u = $("<span>").addClass("icon-feature").data("value", s), this.container.append(u);
        return i = e.defaultValue || 0, o = e.setValue || 0, this.setValue(i, "strong_lit"), this.setValue(o), a = this.container.find(".icon-feature"), e.disable_hover || (this.container.on("mouseenter", ".icon-feature", function (e) {
            var n;
            return n = $(e.target).data("value") + 1, a.slice(0, n).addClass("light_lit"), a.slice(n, t).removeClass("light_lit")
        }), this.container.on("mouseleave", function (e) {
            return l.setValue(l.container.data("value"))
        })), this.animation_lock = !1, this.container.on("click", ".icon-feature", function (n) {
            var r;
            return r = function () {
                var e, r, i;
                return r = $(n.target), i = r.data("value") + 1, l.container.data("value", i), r.rotate && !l.animation_lock && ($.browser.chrome || $.browser.safari) && (l.animation_lock = !0, e = r.data("angle") || 0, r.rotate(e + 144), setTimeout(function () {
                    return l.animation_lock = !1
                }, 1e3)), l.container.trigger("Scribd:rating", i), a.removeClass("strong_lit"), a.slice(0, i).addClass("light_lit"), a.slice(i, t).removeClass("light_lit")
            }, e.force_login && !Scribd.logged_in ? Scribd.with_login("rate_document", r) : r()
        }), this.container.data("star_ratings", this), this
    }
}).call(this);

/* app/views/shared/lists/document_list.coffee @ 1384468298 */
(function () {
    var e = {}.hasOwnProperty, t = function (t, n) {
        function i() {
            this.constructor = t
        }

        for (var r in n)e.call(n, r) && (t[r] = n[r]);
        return i.prototype = n.prototype, t.prototype = new i, t.__super__ = n.prototype, t
    };
    Scribd.UI.DocumentList = function (e) {
        function n() {
            var e, t, r, i = this;
            n.__super__.constructor.apply(this, arguments), this.container.data("lazy_images") && (t = this.container.offset().top, r = $(window), e = function () {
                if (r.scrollTop() + r.height() > t)return i.enable_images(), r.off("scroll resize", e)
            }, r.on("scroll resize", e), _.defer(e)), this.container.dispatch("click", {page_left: function () {
                return $(document.body).trigger("Scribd:hide_popups"), i.page_left()
            }, page_right: function () {
                return $(document.body).trigger("Scribd:hide_popups"), i.page_right()
            }})
        }

        return t(n, e), n.setup_lists = function (e) {
            var t, n, r, i, s, o = this;
            if (this.lists) {
                for (i = 0, s = e.length; i < s; i++)n = e[i], t = new this($(n)), t.recalculate(), this.lists.push(t);
                return this.lists
            }
            return this.lists = function () {
                var t, r, i;
                i = [];
                for (t = 0, r = e.length; t < r; t++)n = e[t], i.push(new this($(n)));
                return i
            }.call(this), r = _.debounce(function () {
                var e, t, r, i;
                r = o.lists, i = [];
                for (e = 0, t = r.length; e < t; e++)n = r[e], i.push(n.recalculate());
                return i
            }, 100), $(window).on("resize", r), $(document.body).on("Scribd:recalculate", r), r(), this.lists
        }, n.prototype.items_per_page = 3, n.prototype.PageDots = !1, n.prototype.calculate_pages = function () {
            var e, t, n, r, i, s;
            this.inner_width = 0, s = this.sliding.children();
            for (r = 0, i = s.length; r < i; r++)e = s[r], this.inner_width += $(e).outerWidth(!0);
            return this.sliding.width(this.inner_width), this.outer_width = this.list_content.width(), t = this.sliding.children(":first").outerWidth(!0), this.page_width = t * this.items_per_page, n = this.inner_width - this.outer_width, this.num_pages = n > 0 ? 1 + Math.ceil((this.inner_width - this.outer_width) / this.page_width) : 1
        }, n.prototype.recalculate = function () {
            return this.calculate_pages(), this.container.toggleClass("on_last_page", this.page === this.num_pages), this.show_visible_images()
        }, n.prototype.page_offset = function () {
            var e;
            return e = n.__super__.page_offset.apply(this, arguments), Math.min(e, Math.max(this.inner_width - this.outer_width, 0))
        }, n.prototype.goto_page = function () {
            return n.__super__.goto_page.apply(this, arguments), this.show_visible_images()
        }, n.prototype.show_visible_images = function () {
            var e, t, n, r, i, s, o;
            if (this.cell_offsets) {
                n = this.page_offset(this.page) + this.outer_width;
                for (t = i = s = this.cell_offsets.length - 1; i >= 0; t = i += -1) {
                    o = this.cell_offsets[t], r = o[0], e = o[1];
                    if (!(r < n))break;
                    this.show_image(e), this.cell_offsets.pop()
                }
                if (!this.cell_offsets.length)return delete this.cell_offsets
            }
        }, n.prototype.show_image = function (e) {
            var t, n;
            t = e.find(".thumb");
            if (n = t.data("thumb_url"))return t.removeData("thumb_url"), window.location.hash.match(/\bdebug_images\b/) ? (t.css({backgroundColor: "red"}), setTimeout(function () {
                return t.css({backgroundImage: "url('" + n + "')"})
            }, 500)) : t.css({backgroundImage: "url('" + n + "')"})
        }, n.prototype.enable_images = function () {
            var e;
            if (!this.container.data("lazy_images"))return;
            return this.container.data("lazy_images", !1), this.cell_offsets = function () {
                var t, n, r, i;
                r = this.container.find(".document_cell"), i = [];
                for (t = 0, n = r.length; t < n; t++)e = r[t], i.push(function (e) {
                    return e = $(e), [e.position().left, e]
                }(e));
                return i
            }.call(this), this.cell_offsets.reverse(), this.show_visible_images()
        }, n
    }(Scribd.UI.SlidingList)
}).call(this);


/* app/views/shared/carousels/carousel.coffee @ 1384468298 */
(function () {
    Scribd.UI.Carousel = function () {
        function e(e, t) {
            var n, r, i, s = this;
            this.container = e, t == null && (t = {}), Scribd.UI.Carousel.carousels.push(this), this.container = $(this.container), this.carousel = this.container.find(".carousel"), this.container.delegate_tracking("click", t.page || "carousel"), this.carousel.jcarousel({wrap: "circular"}), this.toggle_carousel_tools(), $(window).on("resize.jcarousel", function () {
                return s.toggle_carousel_tools()
            }), this.container.one("click", ".carousel_prev,.carousel_next", function () {

            }), this.container.find(".carousel_prev").on("click", function () {
                return s.carousel.jcarousel("scroll", "-=3", function () {
                    return s.enable_all_images()
                })
            }), this.container.find(".carousel_next").on("click", function () {
                return s.carousel.jcarousel("scroll", "+=3", function () {
                    return s.enable_all_images()
                })
            }), this.container.on("mouseenter", function () {
                var e, t, n, r;
                r = Scribd.UI.Carousel.carousels;
                for (t = 0, n = r.length; t < n; t++)e = r[t], e.show_paddles(!1);
                return s.show_paddles(!0)
            }), this.carousel.touchwipe({wipeLeft: function () {
                return s.carousel.jcarousel("scroll", "+=3"), s.enable_all_images(), Scribd.track_event("carousel", "wipe", "left")
            }, wipeRight: function () {
                return s.carousel.jcarousel("scroll", "-=3"), s.enable_all_images(), Scribd.track_event("carousel", "wipe", "right")
            }, min_move_x: 20, min_move_y: 20, preventDefaultEvents: !0}), this.animation_lock = !1, this.container.mousewheel(function (e, t, n, r) {
                if (r !== 0)return;
                s.enable_all_images(), e.preventDefault();
                if (s.animation_lock)return;
                return s.animation_lock = !0, setTimeout(function () {
                    return s.animation_lock = !1
                }, 200)
            }), this.container.data("lazy_images") && (r = this.container.offset().top, i = $(window), n = function () {
                if (i.scrollTop() + i.height() > r)return s.enable_visible_images(), i.off("scroll resize", n)
            }, i.on("scroll resize", n), _.defer(n))
        }

        return e.prototype.toggle_carousel_tools = function () {
            return this.carousel.jcarousel("fullyvisible").length === this.carousel.find("li.item").length ? this.container.addClass("fits_on_screen") : this.container.removeClass("fits_on_screen")
        }, e.prototype.enable_visible_images = function () {
            var e, t, n, r, i;
            if (!this.container.data("lazy_images"))return;
            this.container.data("lazy_images", !1), r = this.carousel.jcarousel("visible"), i = [];
            for (t = 0, n = r.length; t < n; t++)e = r[t], i.push(this.show_image($(e)));
            return i
        }, e.prototype.enable_all_images = function () {
            var e, t, n, r, i;
            if (!this.container.data("lazy_horizontal_images"))return;
            this.container.data("lazy_horizontal_images", !1), r = this.carousel.jcarousel("items"), i = [];
            for (t = 0, n = r.length; t < n; t++)e = r[t], i.push(this.show_image($(e)));
            return i
        }, e.prototype.show_image = function (e) {
            var t, n;
            t = e.find(".thumb");
            if (n = t.data("thumb_url"))return t.removeData("thumb_url"), window.location.hash.match(/\bdebug_images\b/) ? (t.css({backgroundColor: "red"}), setTimeout(function () {
                return t.css({backgroundImage: "url('" + n + "')"})
            }, 500)) : t.css({backgroundImage: "url('" + n + "')"})
        }, e.prototype.show_paddles = function (e) {
            return e == null && (e = !0), e ? this.container.find(".paddle").addClass("always_visible") : this.container.find(".paddle").removeClass("always_visible")
        }, e
    }(), Scribd.UI.Carousel.carousels = []
}).call(this);


/* app/views/shared/branding.coffee @ 1384468298 */
(function () {
    var e, t = {}.hasOwnProperty, n = function (e, n) {
        function i() {
            this.constructor = e
        }

        for (var r in n)t.call(n, r) && (e[r] = n[r]);
        return i.prototype = n.prototype, e.prototype = new i, e.__super__ = n.prototype, e
    };
    e = function (e) {
        function t() {
            var e, n = this;
            t.__super__.constructor.apply(this, arguments), e = _.debounce(function () {
                return n.calculate_pages()
            }, 100), $(window).on("resize", e), $(document.body).on("Scribd:recalculate", function () {
                return n.calculate_pages()
            }), e(), this.container.dispatch("click", {page_left: function () {
                return n.page_left()
            }, page_right: function () {
                return n.page_right()
            }})
        }

        return n(t, e), t.prototype.PageDots = function (e) {
            function t() {
                return t.__super__.constructor.apply(this, arguments)
            }

            return n(t, e), t.prototype.dot_size = 19, t
        }(Scribd.UI.PageDots), t.prototype.calculate_pages = function () {
            var e;
            return e = this.container.width(), this.sliding.children().css({width: "" + e + "px"}), t.__super__.calculate_pages.apply(this, arguments)
        }, t
    }(Scribd.UI.SlidingList), Scribd.UI.Branding = function () {
        function t(t) {
            this.container = $(t), this.list = new e(this.container)
        }

        return t
    }()
}).call(this);


/* app/views/home/index.coffee @ 1384468297 */
(function () {
    var e, t, n;
    e = Scribd.make_tracker("home"), t = function (t, n) {
        return typeof $rat != "undefined" && $rat !== null && $rat("show_list", {event_name: "show_list", context: "home", list: t, list_index: n}), e("show_list", t, n, !1)
    }, n = function () {
        var e, n, r, i, s, o, u, a, f, l, c, h;
        n = 1, l = $(".carousel_widget"), h = [];
        for (o = 0, a = l.length; o < a; o++) {
            r = l[o], i = $(r).find(".carousel_title").data("track"), t(i, n), c = $(r).find(".document_cell");
            for (u = 0, f = c.length; u < f; u++)e = c[u], $(e).data("track_list_index", n), s = $(e).data("track_rats_value"), s.list_index = n, $(e).data("track_rats_value", s);
            h.push(n += 1)
        }
        return h
    }, Scribd.Home = function () {
        function t(e, t, n) {
            var r = this;
            this.load_async = t, this.opts = n != null ? n : {}, this.container = $(e), this.setup_lists(), this.setup_popups(), this.load_async && this.fetch_data(), Scribd.hook_tooltips(this.container), this.track_events(this.load_async), Scribd.UI.Carousel.carousels[0].show_paddles(!0), this.container.one("click", ".carousel_widget", function () {
                return Scribd.track_event("home", "click", "carousel:first_page_click")
            })
        }

        return t.prototype.quick_data_url = "/home/quick_ajax_show", t.prototype.slow_data_url = "/home/timeline_ajax_show", t.prototype.more_lists_url = "/home/load_more_document_lists", t.prototype.current_page = 1, t.prototype.tour_guide = function () {
            var e, t, n, r, i, s, o;
            if (!$(".recently_read_limiter").length) {
                console.log("Skip tour. No Recently Read.");
                return
            }
            return o = 1, t = [], e = function () {
                var e, n, r, i;
                if (o === t.length) {
                    for (r = 0, i = t.length; r < i; r++)e = t[r], e.fadeOut();
                    return
                }
                return n = t[o - 1], e = t[o], n.find(".bordered").fadeOut(function () {
                    var t, n;
                    return t = e.offset().top + e.height(), n = $(window).scrollTop() + $(window).height(), t > n && $("html,body").animate({scrollTop: e.offset().top - $(".global_header").height()}, "slow"), e.find(".bordered").fadeIn()
                }), o += 1
            }, n = function () {
                return i.fadeOut(), s && s.fadeOut(), r.fadeOut()
            }, i = $(".autogen_class_views_home_recommended").layover({fullscreen_width: !0, height: 395, fullscreen_offset_y: -10, bold_title: "Recommendations", title: " are powered by the books you rated", effect: "fadeIn", callback: function () {
                return n()
            }}), s = $(".autogen_class_views_shared_carousels_event_carousel").layover({fullscreen_width: !0, height: 444, fullscreen_offset_y: -10, title: "Your Social Feed displays what your friends are sharing", effect: "fadeIn", callback: function () {
                return n()
            }}), r = $(".autogen_class_views_home_composite_recently_read").layover({fullscreen_width: !0, height: 293, title: "Your Recently Read gets you back to the books your were just reading", effect: "fadeIn", content: {text: "Your experience will be continuously updated as you read, rate and follow. The more you use Scribd the more customized your experience will become.", title: "Welcome to your personalized homepage!"}, callback: function () {
                return n()
            }}), r.css("padding-top", "15px")
        }, t.prototype.fetch_data = function () {
            var e, t, r, i = this;
            return $.get(this.quick_data_url).done(function (e) {
                var t, r, i, s, o, u, a;
                e.redirect_url && (window.location.href = e.redirect_url), e.user_menu && (u = $(e.user_menu), $("#user_menu_placeholder").replaceWith(u), u.hide().fadeIn());
                if (e.flash) {
                    a = e.flash;
                    for (o in a) {
                        t = a[o], (new Scribd.Flasher).show(o, t);
                        break
                    }
                }
                return e.recently_read && (i = $(e.recently_read).insertAfter(home_branding), r = home_branding.outerHeight(), s = i.outerHeight()), e.renewal_nag && ($(".renewal_nag_container").html(e.renewal_nag), trackEvent("payments", "topbar_nag", "homepage", null, !1)), n()
            }), t = {}, window.location.hash.match(/\bforce_suggested\b/) && (t.force_suggested = !0), r = this.slow_data_url + "?" + $.param(t), e = function () {
                if (i.opts.show_tour)return i.opts.show_tour = !1, i.tour_guide()
            }, $.get(r).done(function (t) {
                var n;
                return t.timeline ? (i.container.addClass("has_feed"), n = $(t.timeline).addClass("offscreen"), !$.browser.version === "10.0" ? (n.hide(), i.container.find(".future_social_feed").replaceWith(n), n.slideDown(1e3, function () {
                    return e()
                })) : (i.container.find(".future_social_feed").replaceWith(n), e()), n.toggleClass("is_empty", t.num_events === 0), i.setup_feed(t.events_page_data)) : (i.container.removeClass("has_feed"), i.opts.show_tour && (i.opts.show_tour = !1, i.tour_guide())), $(document.body).trigger("Scribd:recalculate")
            })
        }, t.prototype.setup_feed = function (e, t) {
            var n = this;
            t = this.container.find(".home_feed");
            if (t.length)return this.feed = new Scribd.HomeFeed(t, e, this.container), _.defer(function () {
                return t.removeClass("offscreen")
            })
        }, t.prototype.setup_lists = function () {
            var t, n, r = this;
            this.branding = new Scribd.UI.Branding(this.container.find(".home_branding")), this.container.delegate_tracking("click", "home"), t = this.container.find(".document_lists"), n = this.container.find(".load_more");
            if (n.length)return this.loader = new Scribd.UI.LoadMore(n, function (t) {
                var i;
                return r.current_page += 1, e("more_lists", "" + r.current_page), i = $.param({page: r.current_page}), $.get(r.more_lists_url + "?" + i, function (e) {
                    var r, i, s, o;
                    if (e.document_lists) {
                        i = $(e.document_lists).children().insertBefore(n);
                        for (s = 0, o = i.length; s < o; s++)r = i[s], new Scribd.UI.Carousel(r)
                    }
                    return t(e.has_more)
                })
            })
        }, t.prototype.track_events = function (t) {
            typeof $rat != "undefined" && $rat !== null && $rat("show", {event_name: "show", context: "home"}), e("index", "show", 0, !1);
            if (!t)return n()
        }, t.prototype.setup_popups = function () {
            return this.popups = new Scribd.UI.DocumentPopups(this.container.find(".document_popups"), this.container, "home:document_popups")
        }, t
    }()
}).call(this);


/* app/views/home/_promo.coffee @ 1384468297 */
/* 
 author: Gabriel

 This animation mechanism is unreliable and should be reworked. The main issue is that the entire promo was under the same 
 effect, including the title and translucid right column. 
 A rework should only use the transitioning on the image and coordinate the images. Although it requires more orchestrating, it would
 have prevented a number of issues on IE that propagated in the implementation
 */
(function () {
    Scribd.UI.HomePagePromo = function () {
        function e(e, t) {
            var n = this;
            this.container = e, t == null && (t = {}), this.container = $(this.container), this.disable_animation = t.disable_animation, this.container.delegate_tracking("entry", "onboarding"), this.num_promos = this.container.find(".promotion").length, this.current_promo_index = 0, this.dots = this.container.find(".circle"), this.container.find(".list_dots").append(this.dots.container), this.start_interval(), this.container.delegate_tracking("click", "homepage"), t.stop_on_hover !== !1 && this.container.on("mouseover", function () {
                return n.stop_interval()
            }), this.container.on("mouseout", function () {
                return n.start_interval()
            }), this.dots.on("click", function (e) {
                if (n.animation_lock)return;
                return n.switch_promo($(e.target).index())
            }), this.animation_lock = !1
        }

        return e.prototype.start_interval = function () {
            var e = this;
            if (this.disable_animation)return;
            return this.stop_interval(this.interval), this.interval = setInterval(function () {
                return e.switch_promo()
            }, 4500)
        }, e.prototype.stop_interval = function () {
            return clearInterval(this.interval)
        }, e.prototype.eliminate_animation = function () {
            return this.stop_interval(), this.start_interval = function () {
                return console.log("canceled")
            }
        }, e.prototype._track_click = function () {
            return Scribd.track_event("homepage_promo:" + this.current_promo + "_page", "click", "start_trial_button", !0)
        }, e.prototype.current_promo = function () {
            return $(this.container.find(".promo_container")[this.current_promo_index])
        }, e.prototype.switch_promo = function (e) {
            var t, n, r, i = this;
            return t = this.current_promo(), n = this.right_column(this.current_promo_index), $(this.dots.get(this.current_promo_index)).removeClass("selected"), e != null ? this.current_promo_index = e : this.current_promo_index = (this.current_promo_index + 1) % this.num_promos, $(this.dots.get(this.current_promo_index)).addClass("selected"), !Modernizr.csstransitions || $.browser.version === "10.0" ? (this.current_promo().show(), t.find(".promo_title").fadeOut(800), this.current_promo().find(".promo_title").hide(), t.show(), t.fadeOut(1e3, function () {
                return t.hide().removeClass("active"), i.current_promo().addClass("active"), i.current_promo().find(".promo_title").fadeIn(300)
            })) : (t.removeClass("active"), this.current_promo().show().addClass("active")), this.animation_lock = !0, n.fadeOut(1e3), r = function () {
                return n.animate({right: -396}), n.removeClass("active"), i.animation_lock = !1
            }, setTimeout(function () {
                return i.right_column(i.current_promo_index).first().show().animate({right: -1}, function () {
                    return i.right_column(i.current_promo_index).first().addClass("active"), setTimeout(r, 1e3)
                })
            }, 500)
        }, e.prototype.right_column = function (e) {
            return $(this.container.find(".right_column").get(e))
        }, e
    }()
}).call(this);
