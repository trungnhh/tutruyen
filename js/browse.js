/* :files, 'spec_javascripts', ... @ (none) */


/* :files, 'spec_javascripts', ... @ (none) */


/* :asset_packager_compatibility, 'config/asset_packages.yml' @ 1384554040 */
/* public/javascripts/jquery.dotdotdot.js @ 1384554040 */
/*	
 *	jQuery dotdotdot 1.5.6
 *	
 *	Copyright (c) 2013 Fred Heusschen
 *	www.frebsite.nl
 *
 *	Plugin website:
 *	dotdotdot.frebsite.nl
 *
 *	Dual licensed under the MIT and GPL licenses.
 *	http://en.wikipedia.org/wiki/MIT_License
 *	http://en.wikipedia.org/wiki/GNU_General_Public_License
 */
(function (e) {
    function n(e, t, n) {
        var r = e.children(), i = !1;
        e.empty();
        for (var o = 0, u = r.length; o < u; o++) {
            var a = r.eq(o);
            e.append(a), n && e.append(n);
            if (s(e, t)) {
                a.remove(), i = !0;
                break
            }
            n && n.remove()
        }
        return i
    }

    function r(t, n, o, u, a) {
        var f = t.contents(), l = !1;
        t.empty();
        var c = "table, thead, tbody, tfoot, tr, col, colgroup, object, embed, param, ol, ul, dl, select, optgroup, option, textarea, script, style";
        for (var h = 0, p = f.length; h < p; h++) {
            if (l)break;
            var d = f[h], v = e(d);
            if (typeof d == "undefined")continue;
            t.append(v), a && t[t.is(c) ? "after" : "append"](a), d.nodeType == 3 ? s(o, u) && (l = i(v, n, o, u, a)) : l = r(v, n, o, u, a), l || a && a.remove()
        }
        return l
    }

    function i(e, t, n, r, u) {
        var l = !1, c = e[0];
        if (typeof c == "undefined")return!1;
        var h = r.wrap == "letter" ? "" : " ", p = f(c).split(h), d = -1, v = -1, m = 0, g = p.length - 1;
        while (m <= g) {
            var y = Math.floor((m + g) / 2);
            if (y == v)break;
            v = y, a(c, p.slice(0, v + 1).join(h) + r.ellipsis), s(n, r) ? g = v : (d = v, m = v)
        }
        if (d == -1 || p.length == 1 && p[0].length == 0) {
            var w = e.parent();
            e.remove();
            var E = u ? u.length : 0;
            if (w.contents().size() > E) {
                var S = w.contents().eq(-1 - E);
                l = i(S, t, n, r, u)
            } else {
                var c = w.prev().contents().eq(-1)[0];
                if (typeof c != "undefined") {
                    var b = o(f(c), r);
                    a(c, b), w.remove(), l = !0
                }
            }
        } else {
            var b = o(p.slice(0, d + 1).join(h), r);
            l = !0, a(c, b)
        }
        return l
    }

    function s(e, t) {
        return e.innerHeight() > t.maxHeight
    }

    function o(t, n) {
        while (e.inArray(t.slice(-1), n.lastCharacter.remove) > -1)t = t.slice(0, -1);
        return e.inArray(t.slice(-1), n.lastCharacter.noEllipsis) < 0 && (t += n.ellipsis), t
    }

    function u(e) {
        return{width: e.innerWidth(), height: e.innerHeight()}
    }

    function a(e, t) {
        e.innerText ? e.innerText = t : e.nodeValue ? e.nodeValue = t : e.textContent && (e.textContent = t)
    }

    function f(e) {
        return e.innerText ? e.innerText : e.nodeValue ? e.nodeValue : e.textContent ? e.textContent : ""
    }

    function l(t, n) {
        return typeof t == "undefined" ? !1 : t ? typeof t == "string" ? (t = e(t, n), t.length ? t : !1) : typeof t == "object" ? typeof t.jquery == "undefined" ? !1 : t : !1 : !1
    }

    function c(e) {
        var t = e.innerHeight(), n = ["paddingTop", "paddingBottom"];
        for (var r = 0, i = n.length; r < i; r++) {
            var s = parseInt(e.css(n[r]), 10);
            isNaN(s) && (s = 0), t -= s
        }
        return t
    }

    function h(e, t) {
        return e ? (typeof t == "string" ? t = "dotdotdot: " + t : t = ["dotdotdot:", t], typeof window.console != "undefined" && typeof window.console.log != "undefined" && window.console.log(t), !1) : !1
    }

    if (e.fn.dotdotdot)return;
    e.fn.dotdotdot = function (i) {
        if (this.length == 0)return h(!0, 'No element found for "' + this.selector + '".'), this;
        if (this.length > 1)return this.each(function () {
            e(this).dotdotdot(i)
        });
        var o = this;
        o.data("dotdotdot") && o.trigger("destroy.dot"), o.bind_events = function () {
            return o.bind("update.dot",function (t, i) {
                t.preventDefault(), t.stopPropagation(), f.maxHeight = typeof f.height == "number" ? f.height : c(o), f.maxHeight += f.tolerance;
                if (typeof i != "undefined") {
                    if (typeof i == "string" || i instanceof HTMLElement)i = e("<div />").append(i).contents();
                    i instanceof e && (a = i)
                }
                m = o.wrapInner('<div class="dotdotdot" />').children(), m.empty().append(a.clone(!0)).css({height: "auto", width: "auto", border: "none", padding: 0, margin: 0});
                var u = !1, l = !1;
                return p.afterElement && (u = p.afterElement.clone(!0), p.afterElement.remove()), s(m, f) && (f.wrap == "children" ? l = n(m, f, u) : l = r(m, o, m, f, u)), m.replaceWith(m.contents()), m = null, e.isFunction(f.callback) && f.callback.call(o[0], l, a), p.isTruncated = l, l
            }).bind("isTruncated.dot",function (e, t) {
                return e.preventDefault(), e.stopPropagation(), typeof t == "function" && t.call(o[0], p.isTruncated), p.isTruncated
            }).bind("originalContent.dot",function (e, t) {
                return e.preventDefault(), e.stopPropagation(), typeof t == "function" && t.call(o[0], a), a
            }).bind("destroy.dot", function (e) {
                e.preventDefault(), e.stopPropagation(), o.unwatch().unbind_events().empty().append(a).data("dotdotdot", !1)
            }), o
        }, o.unbind_events = function () {
            return o.unbind(".dot"), o
        }, o.watch = function () {
            o.unwatch();
            if (f.watch == "window") {
                var t = e(window), n = t.width(), r = t.height();
                t.bind("resize.dot" + p.dotId, function () {
                    if (n != t.width() || r != t.height() || !f.windowResizeFix)n = t.width(), r = t.height(), v && clearInterval(v), v = setTimeout(function () {
                        o.trigger("update.dot")
                    }, 10)
                })
            } else d = u(o), v = setInterval(function () {
                var e = u(o);
                if (d.width != e.width || d.height != e.height)o.trigger("update.dot"), d = u(o)
            }, 100);
            return o
        }, o.unwatch = function () {
            return e(window).unbind("resize.dot" + p.dotId), v && clearInterval(v), o
        };
        var a = o.contents(), f = e.extend(!0, {}, e.fn.dotdotdot.defaults, i), p = {}, d = {}, v = null, m = null;
        return p.afterElement = l(f.after, o), p.isTruncated = !1, p.dotId = t++, o.data("dotdotdot", !0).bind_events().trigger("update.dot"), f.watch && o.watch(), o
    }, e.fn.dotdotdot.defaults = {ellipsis: "... ", wrap: "word", lastCharacter: {remove: [" ", ",", ";", ".", "!", "?"], noEllipsis: []}, tolerance: 0, callback: null, after: null, height: null, watch: !1, windowResizeFix: !0, debug: !1};
    var t = 1, p = e.fn.html;
    e.fn.html = function (e) {
        if (typeof e != "undefined")return this.data("dotdotdot") && typeof e != "function" ? this.trigger("update", [e]) : p.call(this, e);
        return p.call(this)
    };
    var d = e.fn.text;
    e.fn.text = function (t) {
        if (typeof t != "undefined") {
            if (this.data("dotdotdot")) {
                var n = e("<div />");
                return n.text(t), t = n.html(), n.remove(), this.trigger("update", [t])
            }
            return d.call(this, t)
        }
        return d.call(this)
    }
})(jQuery);


/* :asset_packager_compatibility, 'config/asset_packages.yml' @ 1384554040 */
/* public/javascripts/jquery_global/star_ratings.coffee @ 1384554040 */
(function () {
    $.fn.star_ratings = function (e) {
        var t, n, r, i, s, o, u, a, f, l = this;
        e == null && (e = {}), n = ["strong_lit", "light_lit"], this.setValue = function (e, t) {
            return t == null && (t = "light_lit"), l.container.data("value", e), l.container.find("span").removeClass(t), l.container.find("span").slice(0, e).addClass(t)
        }, t = 5, $(this).addClass("star_ratings"), this.container = $(this), this.container.addClass("rating");
        if (e.setValue) {
            if (this.container.data("star_ratings")) {
                this.setValue(e.setValue);
                return
            }
            e.defaultValue = e.setValue
        }
        r = $(this.find(".icon-feature")).length;
        for (s = f = r; r <= t ? f < t : f > t; s = r <= t ? ++f : --f)u = $("<span>").addClass("icon-feature").data("value", s), this.container.append(u);
        return i = e.defaultValue || 0, o = e.setValue || 0, this.setValue(i, "strong_lit"), this.setValue(o), a = this.container.find(".icon-feature"), e.disable_hover || (this.container.on("mouseenter", ".icon-feature", function (e) {
            var n;
            return n = $(e.target).data("value") + 1, a.slice(0, n).addClass("light_lit"), a.slice(n, t).removeClass("light_lit")
        }), this.container.on("mouseleave", function (e) {
            return l.setValue(l.container.data("value"))
        })), this.animation_lock = !1, this.container.on("click", ".icon-feature", function (n) {
            var r;
            return r = function () {
                var e, r, i;
                return r = $(n.target), i = r.data("value") + 1, l.container.data("value", i), r.rotate && !l.animation_lock && ($.browser.chrome || $.browser.safari) && (l.animation_lock = !0, e = r.data("angle") || 0, r.rotate(e + 144), setTimeout(function () {
                    return l.animation_lock = !1
                }, 1e3)), l.container.trigger("Scribd:rating", i), a.removeClass("strong_lit"), a.slice(0, i).addClass("light_lit"), a.slice(i, t).removeClass("light_lit")
            }, e.force_login && !Scribd.logged_in ? Scribd.with_login("rate_document", r) : r()
        }), this.container.data("star_ratings", this), this
    }
}).call(this);

/* app/views/shared/grids/object_grid.coffee @ 1384554002 */
(function () {
    var e;
    Scribd.UI || (Scribd.UI = {}), e = function () {
        var t, n, r;
        return t = $("<style type='text/css'>    #_nth_child_test {      margin: 0px;    }    #_nth_child_test:nth-child(1) {      margin: 1px;    }  </style>").appendTo("body"), n = $("<div><div id='_nth_child_test'></div></div>").appendTo(document.body).children().css("margin"), r = n === "1px", e = function () {
            return r
        }, r
    }, Scribd.UI.ObjectGrid = function () {
        function t(e) {
            var t = this;
            this.container = $(e), $(window).on("resize", _.debounce(function () {
                return t.fit_to_width()
            }, 10))
        }

        return t.prototype.margin = 45, t.prototype.cell_width = 185, t.prototype.min_spacing = 32, t.prototype.fit_to_width = function (t) {
            var n, r;
            return this._window || (this._window = $(window)), this._head || (this._head = $("head")), r = this._window.width() - this.margin, t = Math.floor(r / (this.cell_width + this.min_spacing)), n = Math.floor((r - this.margin - this.cell_width) / (t - 1) - this.cell_width), (n + this.cell_width) * t > r && !e() && (n = Math.floor(r / t - this.cell_width)), this._css && this._css.remove(), this._css = $("<style type='text/css'>      .object_grid .object_cell {        margin-right: " + n + "px;      }      .object_grid .object_cell:nth-child(" + t + "n) {        margin-right: 0;      }    </script>").appendTo(this._head)
        }, t
    }()
}).call(this);


/* app/views/shared/filtered_document_page.coffee @ 1384554002 */
(function () {
    Scribd.UI || (Scribd.UI = {}), Scribd.UI.FilteredDocumentPage = function () {
        function e(e) {
            var t, n = this;
            this.container = $(e), t = this.container.find(".filter_picker"), t.is(".open") && this.container.find(".toggle_filters_btn").addClass("open"), this.container.on("click", ".toggle_filters_btn", function (e) {
                return $(e.currentTarget).toggleClass("open"), t.slideToggle("fast")
            }), this.container.find(".language_filter_picker").fancy_select(), this.container.on("change", ".language_filter_picker", function (e) {
                return window.location = $(e.currentTarget).val()
            })
        }

        return e
    }()
}).call(this);


/* app/views/shared/pmp_promo_banner.coffee @ 1384554002 */
(function () {
    var e, t, n, r = {}.hasOwnProperty, i = function (e, t) {
        function i() {
            this.constructor = e
        }

        for (var n in t)r.call(t, n) && (e[n] = t[n]);
        return i.prototype = t.prototype, e.prototype = new i, e.__super__ = t.prototype, e
    };
    t = function (t, n) {
        return new e({el: t, page_name: n})
    }, e = function (e) {
        function t() {
            return t.__super__.constructor.apply(this, arguments)
        }

        return i(t, e), t.prototype.events = {"click .start_trial_button": "free_trial_btn_clicked"}, t.prototype.initialize = function () {
            return this._setup_header_text(), this._page_name = this.options.page_name
        }, t.prototype._setup_header_text = function () {
            var e, t, n;
            return e = this.$(".primary_header"), t = e.find(".text"), n = _.debounce(_.bind(this._fit_text, this, e, t), 300), $(window).on("resize", n), this._fit_text(e, t)
        }, t.prototype._fit_text = function (e, t) {
            var n, r, i, s, o;
            o = function () {
                return t.height() < e.height()
            }, s = function () {
                return t.height() > e.height()
            }, r = function () {
                return parseInt(e.css("font-size").replace("px", ""))
            };
            if (s())while (s()) {
                i = n, n = r();
                if (n === i)return;
                e.css("font-size", n - 1 + "px")
            } else if (o()) {
                while (o()) {
                    i = n, n = r();
                    if (n === i)return;
                    e.css("font-size", Math.max(n + 1, 16) + "px")
                }
                return e.css("font-size", n - 1 + "px")
            }
        }, t.prototype._track_click = function () {
            return Scribd.track_event("pmp_promo_banner:" + this._page_name + "_page", "click", "start_trial_button", !0)
        }, t.prototype.free_trial_btn_clicked = function (e) {
            var t, n, r;
            this._track_click(), t = !!Scribd.logged_in, n = {context: "pmp", page: "pmp_landing", action: "start_trial", platform: "web", logged_in: t}, n = encodeURIComponent(JSON.stringify(n)), r = "metadata=" + n;
            if (!t)return Scribd.SignIn.open("subscribe", "/archive/pmp_checkout?" + r), !1
        }, t
    }(Backbone.View), (n = Scribd.Shared) == null && (Scribd.Shared = {}), Scribd.Shared.PmpPromoBanner = {init: t}
}).call(this);


/* app/views/browse/index.coffee @ 1384554002 */
(function () {
    var e = {}.hasOwnProperty, t = function (t, n) {
        function i() {
            this.constructor = t
        }

        for (var r in n)e.call(n, r) && (t[r] = n[r]);
        return i.prototype = n.prototype, t.prototype = new i, t.__super__ = n.prototype, t
    };
    Scribd.Browse || (Scribd.Browse = {}), Scribd.Browse.GridPage = function (e) {
        function n(e) {
            var t, r = this;
            n.__super__.constructor.apply(this, arguments), this.fit_to_width(), Scribd.hook_tooltips(this.container), this.container.find("[data-default_text]").placeholder_text(), new Scribd.UI.FilteredDocumentPage(this.container), this.container.find(".sub_category_select").fancy_select().on("change", function (e) {
                return window.location = $(e.target).val()
            }), this.document_drop = this.container.find(".document_drop"), t = this.container.find(".load_more_btn"), t.length && new Scribd.UI.LoadMore(t, function (e) {
                return r.page += 1, $.ajax({url: window.location, data: {page: r.page}, dataType: "json", type: "post", success: function (t) {
                    return $(t.objects).children().appendTo(r.document_drop), r.fit_to_width(), e(t.has_more)
                }})
            })
        }

        return t(n, e), n.prototype.page = 1, n
    }(Scribd.UI.ObjectGrid)
}).call(this);
